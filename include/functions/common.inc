<?php

/**
 * common_functions.php
 * @author 
 * @copyright 2009
 */

function print_in_textarea($string, $width=600, $height=400)
{
    print "<textarea style='height:$height\px; width:$width\px;'>";
    print_r($string);
    print "</textarea>";
}

function win_to_utf($s)
{
for($i=0, $m=strlen($s); $i<$m; $i++)
{
$c=ord($s[$i]);
if ($c<=127)
{$t.=chr($c); continue; }
if ($c>=192 && $c<=207)
{$t.=chr(208).chr($c-48); continue; }
if ($c>=208 && $c<=239)
{$t.=chr(208).chr($c-48); continue; }
if ($c>=240 && $c<=255)
{$t.=chr(209).chr($c-112); continue; }
if ($c==184) { $t.=chr(209).chr(209);
continue; };
if ($c==168) { $t.=chr(208).chr(129);
continue; };
}
return $t;
}

function alert($message)
{
	print "<font size='2px' color=green>(<i>$message</i>)</font>";
	//print_in_textarea($message);
	//exit();
	print "<script language='JavaScript'> window.alert('$message');</script>";
}

function required_message($field)
{
	alert('Для реєстрації потрібно заповнити поле "'.$field.'"');
}

function GET_parameters_string_despite($exclude_parameters = array())
{
	$GET_parameters = "";
	$parameters_number = 0;
	$parameter_devider = "";
	foreach($_GET as $parameter=>$value)
	{	
		$parameters_number++;
		if(!in_array($parameter, $exclude_parameters) )
		{
			$GET_parameters .= $parameter_devider."$parameter=$value";	
			$parameter_devider = '&';
		}
	}
	return $GET_parameters;	
}

function GET_parameters_despite_c_()
{
	$r = array();
	$i=0;
	foreach($_GET as $key=>$value)
	{
		if(!preg_match("@c_@",$key))
		{
			$r[$key]= $value;
			$i++;
		}
	}
	return $r;
}

function GET_parameters_like_c_()
{
	$r = array();
	$i=0;
	foreach($_GET as $key=>$value)
	{
		if(preg_match("@c_@",$key))
		{
			$r[$key]= $value;
			$i++;
		}
	}
	return $r;
}

function get_link($this_page, $GET_parameters)
{
	return $this_page."?".$GET_parameters;
}

function redirect($link)
{
	if(!headers_sent())
		header("Location:$link");
	print '<script language="JavaScript">
window.location = "'.$link.'"
</script>
';
	exit();
}

function post_array($array)
{
	foreach ($_POST as $key=>$value) 
	{
		if(is_array($key))
			post_array($key);
		elseif($key != 'submit')
			print '<input type="hidden" name="'.$key.'" value="'.$value.'"/> 
		';
	}
}

function error($error, $location, $seconds = 50)
{
    header("Refresh: $seconds; URL=\"$location\"");
    echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"'."\n".
    '"http://www.w3.org/TR/html4/strict.dtd">'."\n\n".
    '<html lang="en">'."\n".
    '    <head>'."\n".
    '        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">'."\n\n".
    '        <link rel="stylesheet" type="text/css" href="includes/styles/file.upload.css">'."\n\n".	
    '    <title>Помилочка</title>'."\n\n".
    '    </head>'."\n\n".
    '    <body>'."\n\n".
    '    <div id="Upload">'."\n\n".
    '        <p>Трапилась помилка: '."\n\n".
    '        <span class="red">' . $error . '...</span>'."\n\n".
    '        За кілька секунд сторінка перезавантажиться</p>'."\n\n".
    '     </div>'."\n\n".
    '</html>';
    error_report($error, $location, time());
    exit;
} // end error handler


function error_report($description = "", $place = "", $time = "")
{
	if(!$time)
		$time = time();
	$time = date("",$time);
	
	$description = mysql_real_escape_string($description);
	$place = mysql_real_escape_string($place);
	$sql= "insert into error_report values('$description', '$place', '$time')";
	
}


function mine_session_is_registered($session_name)
{
	return isset($_SESSION[$session_name]);
} 

function mine_register_session($session_name,$session_value)
{
	return $_SESSION[$session_name]=$session_value;
} 

function mine_session_destroy()
{
	
	unset($_SESSION['login']);
	unset($_SESSION['password']);
}

function print_query_result_to_jscript($var_name, $sql)
{
	$result = tep_db_query($sql);

	$json_string = "";
	
	if($result && mysql_num_rows($result))
	while($row = mysql_fetch_assoc($result))
	{
		$json_row = "";
		foreach($row as $column => $value)
			$json_row .= "\n '$column': '$value',";
		
		$json_row = substr($json_row, 0, strlen($json_row) - 1);//remove last comma
		$json_string .= "{ $json_row },";
	}
	$json_string = substr($json_string, 0, strlen($json_string) - 1);//remove last comma
	$json_string = "{ $json_string }";
	
	print "
	<script language='javascript'>
	<!--
	$var_name = $json_string;
	-->
	</script>
	";
	
}

function print_query_result_indexed_by_ID_to_jscript($var_name, $sql)
{
	$result = tep_db_query($sql);
	
	$id_name = "";	
	/* get column metadata */
	$i = 0;
	while ($i < mysql_num_fields($result)) 
	{
	    $meta = mysql_fetch_field($result, $i);
	    if($meta->primary_key == 1)
			$id_name = $meta->name;			
	    $i++;
	}
	
	$json_string = "";
	
	if($result && mysql_num_rows($result))
	while($row = mysql_fetch_assoc($result))
	{
		$json_row = "";
		foreach($row as $column => $value)
			$json_row .= "\n '$column': '$value',";
		
		$json_row = substr($json_row, 0, strlen($json_row) - 1);//remove last comma
		$json_string .= "'".$row[$id_name]."' : { $json_row },";
	}
	$json_string = substr($json_string, 0, strlen($json_string) - 1);//remove last comma
	$json_string = "{ $json_string }";
	
	print "
	<script language='javascript'>
	//<!--
	$var_name = $json_string;
	//-->
	</script>
	";
	
	//print_in_textarea($json_string);

	
}


?>