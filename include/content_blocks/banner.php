        <!-- Main Content -->
        <div class="breadcrumb"><a href="http://businesslviv.ho.ua/">Головна</a>&nbsp;//&nbsp;<a href="http://businesslviv.ho.ua/?q=node/add">Створити вміст</a></div>        
                <h2>Створити Рекламний банер</h2>        
        
                        <form action="/?q=node/add/banner" accept-charset="UTF-8" method="post" id="node-form">
<div>
<div class="node-form">
  <div class="standard">
<div class="form-item" id="edit-title-wrapper">
 <label for="edit-title">Title: <span class="form-required" title="Обов'язкове поле">*</span></label>
 <input maxlength="255" name="title" id="edit-title" size="60" value="" class="form-text required" type="text">
</div>
<div class="body-field-wrapper">
<div class="teaser-checkbox"><div class="teaser-button-wrapper"><input value="Split summary at cursor" class="teaser-button" type="button"></div><div class="form-item" id="edit-teaser-include-wrapper">
 <label style="display: none;" class="option" for="edit-teaser-include"><input name="teaser_include" id="edit-teaser-include" value="1" checked="checked" class="form-checkbox" type="checkbox"> Показати при повному перегляді</label>
</div>
</div><div class="form-item" id="edit-body-wrapper">
 <label for="edit-body">Body: </label>
 <div class="resizable-textarea"><span style="display: none;"><textarea cols="60" rows="10" name="teaser_js" id="edit-teaser-js" disabled="disabled" class="form-textarea teaser resizable teaser-processed textarea-processed"></textarea><div style="margin-right: -4px;" class="grippie"></div></span></div><div class="resizable-textarea"><span><textarea cols="60" rows="20" name="body" id="edit-body" class="form-textarea resizable textarea-processed"></textarea><div style="margin-right: -4px;" class="grippie"></div></span></div>
</div>
<fieldset class="collapsible collapsed"><legend class="collapse-processed"><a href="#">Формат вводу</a></legend><div class="fieldset-wrapper"><div class="form-item" id="edit-format-1-wrapper">
 <label class="option" for="edit-format-1"><input id="edit-format-1" name="format" value="1" checked="checked" class="form-radio" type="radio"> Filtered HTML</label>
 <div class="description"><ul class="tips"><li>Інтернет адреси і адреси електронної пошти автоматично перетворюються у лінки.</li><li>Дозволені
теги HTML: &lt;a&gt; &lt;em&gt; &lt;strong&gt; &lt;cite&gt;
&lt;code&gt; &lt;ul&gt; &lt;ol&gt; &lt;li&gt; &lt;dl&gt; &lt;dt&gt;
&lt;dd&gt;</li><li>Рядки і параграфи переносяться автоматично.</li></ul></div>
</div><div class="form-item" id="edit-format-2-wrapper">
 <label class="option" for="edit-format-2"><input id="edit-format-2" name="format" value="2" class="form-radio" type="radio"> Full HTML</label>
 <div class="description"><ul class="tips"><li>Інтернет адреси і адреси електронної пошти автоматично перетворюються у лінки.</li><li>Рядки і параграфи переносяться автоматично.</li></ul></div>
</div><div class="form-item" id="edit-format-3-wrapper">
 <label class="option" for="edit-format-3"><input id="edit-format-3" name="format" value="3" class="form-radio" type="radio"> PHP code</label>
 <div class="description"><ul class="tips"><li>Ви можете вводити PHP код. Використовуйте теги &lt;?php ?&gt;.</li></ul></div>
</div><p><a href="http://businesslviv.ho.ua/?q=filter/tips">Більше інформації про опції форматування</a></p></div>


</fieldset>
</div><fieldset class="menu-item-form collapsible collapsed"><legend class="collapse-processed"><a href="#">Налаштування меню</a></legend><div class="fieldset-wrapper"><div class="form-item" id="edit-menu-link-title-wrapper">
 <label for="edit-menu-link-title">Назва в меню: </label>
 <input maxlength="128" name="menu[link_title]" id="edit-menu-link-title" size="60" value="" class="form-text" type="text">
 <div class="description">Текст
посилання відповідно до цього пункту, що має з'явитися в меню. Залиште
порожнім, якщо Ви не бажаєте додати цю публікацію до меню.</div>
</div><div class="form-item" id="edit-menu-parent-wrapper">
 <label for="edit-menu-parent">Батьківський пункт: </label>
 <select name="menu[parent]" class="form-select menu-title-select" id="edit-menu-parent"><option value="navigation:0">&lt;Navigation&gt;</option><option value="navigation:199">-- Glossary</option><option value="navigation:21">-- Мій профіль користувача</option><option value="navigation:9">-- Підказки редактора (відключений)</option><option value="navigation:243">-- Шукати (відключений)</option><option value="navigation:11">-- Створити вміст</option><option value="navigation:106">---- Page</option><option value="navigation:107">---- Story</option><option value="navigation:207">---- Рекламний банер</option><option value="navigation:165">---- Рубрика</option><option value="navigation:122">---- Фірма</option><option value="navigation:2">-- Адмініструвати сайт</option><option value="navigation:10">---- Керування вмістом сайту</option><option value="navigation:245">------ Content templates</option><option value="navigation:43">------ RSS публікування</option><option value="navigation:27">------ Коментарі</option><option value="navigation:28">------ Наповнення</option><option value="navigation:42">------ Настройки публікацій</option><option value="navigation:94">------ Таксономія</option><option value="navigation:29">------ Типи наповнення</option><option value="navigation:17">---- Структура сайту</option><option value="navigation:25">------ Блоки</option><option value="navigation:38">------ Меню</option><option value="navigation:89">-------- Navigation</option><option value="navigation:90">-------- Primary links</option><option value="navigation:91">-------- Secondary links</option><option value="navigation:39">------ Модулі</option><option value="navigation:49">------ Оформлення</option><option value="navigation:180">------ Перегляди</option><option value="navigation:115">------ Переклад інтерфейсу</option><option value="navigation:18">---- Налаштування сайту</option><option value="navigation:244">------ Content Template Settings</option><option value="navigation:35">------ Інструменти зображень</option><option value="navigation:46">------ Інформація сайту</option><option value="navigation:30">------ Дата і час</option><option value="navigation:24">------ Дизайн адміністратора</option><option value="navigation:23">------ Дії</option><option value="navigation:40">------ Ефективність</option><option value="navigation:37">------ ЖУрналювання та попередження</option><option value="navigation:100">-------- Database logging</option><option value="navigation:33">------ Звітування про помилки</option><option value="navigation:114">------ Мови</option><option value="navigation:246">------ Налаштування пошуку</option><option value="navigation:47">------ Ремонт сайту</option><option value="navigation:34">------ Файлова система</option><option value="navigation:36">------ Формати вводу</option><option value="navigation:26">------ Чисті URL адреси</option><option value="navigation:20">---- Керування користувачами сайту</option><option value="navigation:41">------ Дозволи</option><option value="navigation:51">------ Користувачі</option><option value="navigation:50">------ Налаштування користувача</option><option value="navigation:22">------ Правила доступу</option><option value="navigation:45">------ Ролі</option><option value="navigation:16">---- Звіти</option><option value="navigation:93">------ Останні записи в log-файлі </option><option value="navigation:96">------ Найпопулярніші помилки</option><option value="navigation:97">------ Найпопулярніші помилки</option><option value="navigation:247">------ Найпопулярніші фрази пошуку</option><option value="navigation:105">------ Доступні оновлення</option><option value="navigation:48">------ Звіт про стан сайту</option><option value="navigation:171">---- Advanced help</option><option value="navigation:176">---- Example help</option><option value="navigation:15">---- Допомога</option><option value="navigation:4">-- Вийти</option><option value="primary-links:0" selected="selected">&lt;Primary links&gt;</option><option value="primary-links:202">-- Рубрики</option><option value="primary-links:203">-- Додати ПП</option><option value="primary-links:204">-- Розміщення реклами</option><option value="primary-links:205">-- Контакти</option><option value="primary-links:206">-- Про нас</option><option value="secondary-links:0">&lt;Secondary links&gt;</option></select>
 <div class="description">Максимальна
глибина для публікації та всіх підлеглих сторінок встановлена в 9.
Деякі пункти меню можуть бути недоступними, як батьківські, якщо
вложення перевищує цей ліміт.</div>
</div><div class="form-item" id="edit-menu-weight-wrapper">
 <label for="edit-menu-weight">Вага: </label>
 <select name="menu[weight]" class="form-select" id="edit-menu-weight"><option value="-50">-50</option><option value="-49">-49</option><option value="-48">-48</option><option value="-47">-47</option><option value="-46">-46</option><option value="-45">-45</option><option value="-44">-44</option><option value="-43">-43</option><option value="-42">-42</option><option value="-41">-41</option><option value="-40">-40</option><option value="-39">-39</option><option value="-38">-38</option><option value="-37">-37</option><option value="-36">-36</option><option value="-35">-35</option><option value="-34">-34</option><option value="-33">-33</option><option value="-32">-32</option><option value="-31">-31</option><option value="-30">-30</option><option value="-29">-29</option><option value="-28">-28</option><option value="-27">-27</option><option value="-26">-26</option><option value="-25">-25</option><option value="-24">-24</option><option value="-23">-23</option><option value="-22">-22</option><option value="-21">-21</option><option value="-20">-20</option><option value="-19">-19</option><option value="-18">-18</option><option value="-17">-17</option><option value="-16">-16</option><option value="-15">-15</option><option value="-14">-14</option><option value="-13">-13</option><option value="-12">-12</option><option value="-11">-11</option><option value="-10">-10</option><option value="-9">-9</option><option value="-8">-8</option><option value="-7">-7</option><option value="-6">-6</option><option value="-5">-5</option><option value="-4">-4</option><option value="-3">-3</option><option value="-2">-2</option><option value="-1">-1</option><option value="0" selected="selected">0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option></select>
 <div class="description">Не обов’язковий. В меню, "важчі" пункти будуть "тонути", а "легші" піднімуться нагору.</div>
</div></div>


</fieldset>
<div class="form-item" id="edit-field-image-0-value-wrapper">
 <label for="edit-field-image-0-value">Анімація: </label>
 <input name="field_image[0][value]" id="edit-field-image-0-value" size="60" value="" class="form-text text" type="text">
 <div class="description">Посилання на рисунок</div>
</div>
<fieldset class="group-publication"><legend>Публікування</legend><div class="form-item" id="edit-field-publicated-value-wrapper">
 <label for="edit-field-publicated-value">Висить: <span class="form-required" title="Обов'язкове поле">*</span></label>
 <select name="field_publicated[value]" class="form-select required" id="edit-field-publicated-value"><option value="Ні">Ні</option><option value="Так">Так</option></select>
 <div class="description">Чи опубліковано цей баннер</div>
</div>
<div class="form-item" id="edit-field-start-date-0-value-wrapper">
 <label for="edit-field-start-date-0-value">Дата початку: <span class="form-required" title="Обов'язкове поле">*</span></label>
 <input name="field_start_date[0][value]" id="edit-field-start-date-0-value" size="60" value="" class="form-text required text" type="text">
 <div class="description">перший день публікування баннера</div>
</div>
<div class="form-item" id="edit-field-end-date-0-value-wrapper">
 <label for="edit-field-end-date-0-value">Дата завершення: </label>
 <input name="field_end_date[0][value]" id="edit-field-end-date-0-value" size="60" value="" class="form-text text" type="text">
 <div class="description">завершення розміщення банера на сайті</div>
</div>
<div class="form-item" id="edit-field-duration-0-value-wrapper">
 <label for="edit-field-duration-0-value">Тривалість: <span class="form-required" title="Обов'язкове поле">*</span></label>
 <input name="field_duration[0][value]" id="edit-field-duration-0-value" size="60" value="" class="form-text required text" type="text">
 <div class="description">кількість днів для розміщення</div>
</div>
</fieldset>
<input name="changed" id="edit-changed" value="" type="hidden">
<input name="form_build_id" id="form-883251b823b2215750020f4117763029" value="form-883251b823b2215750020f4117763029" type="hidden">
<input name="form_token" id="edit-banner-node-form-form-token" value="4fc2c6758b1c3493ecf3cdad5437fed9" type="hidden">
<input name="form_id" id="edit-banner-node-form" value="banner_node_form" type="hidden">
<div class="form-item" id="edit-field-customer-0-value-wrapper">
 <label for="edit-field-customer-0-value">Замовник: </label>
 <input name="field_customer[0][value]" id="edit-field-customer-0-value" size="60" value="" class="form-text text" type="text">
 <div class="description">Ім'я замовника</div>
</div>
<div class="form-item" id="edit-field-contacts-0-value-wrapper">
 <label for="edit-field-contacts-0-value">Контактна інформація: </label>
 <div class="resizable-textarea"><span><textarea cols="60" rows="5" name="field_contacts[0][value]" id="edit-field-contacts-0-value" class="form-textarea resizable textarea-processed"></textarea><div style="margin-right: -4px;" class="grippie"></div></span></div>
 <div class="description">електронна пошта 
телефон
ICQ
адреса</div>
</div>
<fieldset class="collapsible collapsed"><legend class="collapse-processed"><a href="#">Інформація про зміни</a></legend><div class="fieldset-wrapper"><div class="form-item" id="edit-revision-wrapper">
 <label class="option" for="edit-revision"><input name="revision" id="edit-revision" value="1" class="form-checkbox" type="checkbox"> Створити нову версію</label>
</div><div class="form-item" id="edit-log-wrapper">
 <label for="edit-log">Повідомлення журналу: </label>
 <div class="resizable-textarea"><span><textarea cols="60" rows="2" name="log" id="edit-log" class="form-textarea resizable textarea-processed"></textarea><div style="margin-right: 0px;" class="grippie"></div></span></div>
 <div class="description">Пояснення додатків або доповнень вводяться для того, щоб допомогти іншим авторам зрозуміти Вашу мотивацію.</div>
</div></div>

</fieldset>
<fieldset class="collapsible collapsed"><legend class="collapse-processed"><a href="#">Налаштування коментаря</a></legend><div class="fieldset-wrapper"><div class="form-radios"><div class="form-item" id="edit-comment-0-wrapper">
 <label class="option" for="edit-comment-0"><input id="edit-comment-0" name="comment" value="0" class="form-radio" type="radio"> Відключений</label>
</div>
<div class="form-item" id="edit-comment-1-wrapper">
 <label class="option" for="edit-comment-1"><input id="edit-comment-1" name="comment" value="1" class="form-radio" type="radio"> Лише для читання</label>
</div>
<div class="form-item" id="edit-comment-2-wrapper">
 <label class="option" for="edit-comment-2"><input id="edit-comment-2" name="comment" value="2" checked="checked" class="form-radio" type="radio"> Прочитати/Написати</label>
</div>
</div></div></fieldset>
  </div>
  <div class="admin">
    <div class="authored">
<fieldset class="collapsible collapsed"><legend class="collapse-processed"><a href="#">Інформація про автора</a></legend><div class="fieldset-wrapper"><div class="form-item" id="edit-name-wrapper">
 <label for="edit-name">Автор: </label>
 <input autocomplete="OFF" maxlength="60" name="name" id="edit-name" size="60" value="root" class="form-text form-autocomplete" type="text">
 <div class="description">Залиште пустим для <em>Anonymous</em>.</div>
</div><input class="autocomplete autocomplete-processed" id="edit-name-autocomplete" value="http://businesslviv.ho.ua/?q=user/autocomplete" disabled="disabled" type="hidden"><div class="form-item" id="edit-date-wrapper">
 <label for="edit-date">Написано: </label>
 <input maxlength="25" name="date" id="edit-date" size="60" value="" class="form-text" type="text">
 <div class="description">Формат: <em>2009-03-17 13:01:27 +0200</em>. Залишіть пустим щоб використати час затвердження форми.</div>
</div></div>

</fieldset>
    </div>
    <div class="options">
<fieldset class="collapsible collapsed"><legend class="collapse-processed"><a href="#">Опції публікування</a></legend><div class="fieldset-wrapper"><div class="form-item" id="edit-status-wrapper">
 <label class="option" for="edit-status"><input name="status" id="edit-status" value="1" checked="checked" class="form-checkbox" type="checkbox"> Опубліковано</label>
</div><div class="form-item" id="edit-promote-wrapper">
 <label class="option" for="edit-promote"><input name="promote" id="edit-promote" value="1" checked="checked" class="form-checkbox" type="checkbox"> На головній сторінці</label>
</div><div class="form-item" id="edit-sticky-wrapper">
 <label class="option" for="edit-sticky"><input name="sticky" id="edit-sticky" value="1" class="form-checkbox" type="checkbox"> Приліплено до верху списку</label>
</div></div>


</fieldset>
    </div>
  </div>
<input name="op" id="edit-submit" value="Зберегти" class="form-submit" type="submit">
<input name="op" id="edit-preview" value="Попередній перегляд" class="form-submit" type="submit">
</div>

</div></form>
