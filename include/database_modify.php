﻿<?php

/**
 * @author 
 * @copyright 2009
 */
 
include("db_connect.php");
include_once("functions/common.inc");

	$week_days_eng = array("monday","tuesday","wednesday","thursday","friday","saturday","sunday");
	$week_days_ukr = array("понеділок","вівторок","середа","четвер","пятниця","субота","неділя");


function print_ok($string)
{
	print "<font color=green>$string</font>";
}

function print_error($string)
{
	print "<font color=red>$string</font>";
}

function add_week_fields_to_firm()
{
	global $week_days_eng;
	global $week_days_ukr;
	$all_sql = "";
	$result = "";
	
	for($i=0;$i<7;$i++)
	{
		$day_eng = $week_days_eng[$i];
		$day_ukr = $week_days_ukr[$i];
		
		$sql = "Alter table firm add column `".$day_eng."_from` time DEFAULT NULL COMMENT '".$day_ukr." від';";
		$result = mysql_query($sql);
		
		$all_sql .= $sql."\n";
		
		if($result)
			print_ok("ok for $day_ukr<br>");
		else 
			print_error("in $day_ukr, ".mysql_result($result,0));
		
		//
			
		$sql = "Alter table firm add column `".$day_eng."_till` time DEFAULT NULL COMMENT '".$day_ukr." до';";
		$result = mysql_query($sql);
		
		$all_sql .= $sql."\n";
		
		if($result)
			print_ok("ok for $day_ukr<br>");
		else 
			print_error("in $day_ukr, ".mysql_result($result,0));

		
	}
	
	print_in_textarea($all_sql);

	
}

function add_week_records_to_form_view()
{
	global $week_days_eng;
	global $week_days_ukr;
	$all_sql = "";
	$result = "";
	for($i=0;$i<7;$i++)
	{
		$day_eng = $week_days_eng[$i];
		$day_ukr = $week_days_ukr[$i];
		
		$sql = "Replace into `form_view` SET 
			`table_name` = 'firm',
			`column_name` = '".$day_eng."_from',
			`ordinal_position` = '".(26+2*$i)."',
			`column_comment` = '$day_ukr від',
			`default_value` = '$day_ukr від',
			`required` = '0',
			`input_type` = 'text',
			`input_tag` = 'input',
			`size` = '5' ;";
		$result = mysql_query($sql);

		$all_sql .= $sql."\n\n";		

		if($result)
			print_ok("ok for $day_ukr<br>");
		else 
			print_error("in $day_ukr, ".mysql_result($result,0));

		
		$sql = "Replace into `form_view` SET 
			`table_name` = 'firm',
			`column_name` = '".$day_eng."_till',
			`ordinal_position` = '".(26+2*$i+1)."',
			`column_comment` = '$day_ukr до',
			`default_value` = '$day_ukr до',
			`required` = '0',
			`input_type` = 'text',
			`input_tag` = 'input',
			`size` = '5' 
			;";
		$result = mysql_query($sql);

		
		$all_sql .= $sql."\n\n";
		
		if($result)
			print_ok("ok for $day_ukr<br>");
		else 
			print_error("in $day_ukr, ".mysql_result($result,0));
		
	}
	
	print_in_textarea($all_sql);

	
}

function modify_week_records_to_form_view()
{
	global $week_days_eng;
	global $week_days_ukr;
	$all_sql = "";
	$result = "";	
	for($i=0;$i<7;$i++)
	{
		$day_eng = $week_days_eng[$i];
		$day_ukr = $week_days_ukr[$i];
		
		$sql = "ALTER TABLE `firm` CHANGE `".$day_eng."_from` `".$day_eng."_from` VARCHAR( 8 ) NULL DEFAULT NULL COMMENT '".$day_ukr." від';";
		$result = mysql_query($sql);

		$all_sql .= $sql."\n\n";		

		if($result)
			print_ok("ok for $day_ukr<br>");
		else 
			print_error("in $day_ukr, ".mysql_result($result,0));

		
		$sql = "ALTER TABLE `firm` CHANGE `".$day_eng."_till` `".$day_eng."_till` VARCHAR( 8 ) NULL DEFAULT NULL COMMENT '".$day_ukr." до';";
		$result = mysql_query($sql);

		
		$all_sql .= $sql."\n\n";
		
		if($result)
			print_ok("ok for $day_ukr<br>");
		else 
			print_error("in $day_ukr, ".mysql_result($result,0));
		
	}
	
	print_in_textarea($all_sql);

	
}
//ALTER TABLE `firm` CHANGE `monday_from` `monday_from` VARCHAR( 8 ) NULL DEFAULT NULL COMMENT 'Понеділок від'


add_week_records_to_form_view();

?>