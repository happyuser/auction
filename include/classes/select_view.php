<?php

/**
 * @author Taras Prystavskyj
 * @copyright 2009
 */
include_once("html_dom.php");

class nested_table
{
	var $link = "";
	var $field = "ID";
	function nested_table($link, $filed)
	{
		$this->link = $link;
		$this->field = $filed;
	}
}

class foreign_key
{
	var $child_table;
	var $child_foreign_key;
	var $parent_table;
	var $parent_id;
	var $parent_view_field;
	var $on_change;
	function foreign_key($child_foreign_key, $parent_table, $parent_id, $parent_view_field, $on_change = "")
	{
		$this->child_foreign_key = $child_foreign_key;
		$this->parent_table = $parent_table;
		$this->parent_id = $parent_id;
		$this->parent_view_field = $parent_view_field;
		$this->on_change = $on_change;
	}
	
	public function get_select_from_parent_table($value_of_foreign_key, $ID = "")
	{
			//built categories select
		$sql = "select * from ".$this->parent_table." where 1";
		$result =mysql_query($sql);
				
		$select = "<select id='".$ID."_".$this->child_foreign_key."' name='".$this->child_foreign_key."' onchange='".$this->on_change."'>\n";//html string
		
		//add empty option
		if($value_of_foreign_key == "")
			$selected = "selected='selected'";
		else $selected = "";
			
			$select .= "\t<option $selected value=''></option>\r\n";

		
		if($result && mysql_num_rows($result))
		while($row = mysql_fetch_assoc($result))
		{
			if($value_of_foreign_key == $row[$this->parent_id])
				$selected = "selected='selected'";
			else $selected = "";
			
			$select .= "\t<option $selected value='".$row[$this->parent_id]."'>".$row[$this->parent_view_field]."</option>\r\n";
		}
		$select .= "</select>";
		$select = new content_element(content_element_type::text, $select);
		
		return $select;		
	}
	
	public function get_value_by_key($forein_key)
	{
		$sql = "select * from ".$this->parent_table." where ".$this->parent_id."= '".$forein_key."'" ;
		$result =mysql_query($sql);
		$row = mysql_fetch_assoc($result);
		
		return $row[$this->parent_view_field];
		
	}
}

class select_view extends content_element{
	var $mode = "table";
	
function select_view($table_name = "", $a_foreign_keys =null, $o_nested_table = null, $id_name = "ID", $dir_with_images = '', $add_pagination = true, $readonly = true)
{
	$this->type = content_element_type::tag;
	$this->tag_name = "div";
	
	//get mode change to switch betwen table and form view
	if(isset($_GET['mode']))
		$this->mode = $_GET['mode'];
	else 
		$this->mode = "table";//mode is initialy set to table
	
	//
	switch($this->mode)
	{
		case "table":
		{
			include_once("select_view_with_table_2.php");
			$this->add_content_element(get_content_filter_for_table($table_name, $a_foreign_keys, $o_nested_table, $id_name, $dir_with_images, $add_pagination, $readonly));
			break;			
		}
		case "form":
		{
			include_once("select_view_with_form_2.php");
			$this->add_content_element(get_content_filter_for_table($table_name, $a_foreign_keys, $id_name, $dir_with_images));
			break;
		}
	}
		
}

}//class select view

?>