<script language="JavaScript">

function clear_image_field_for(this_button)
{
	//alert("ця кнопка працюватиме пізніше");
	td = this_button.parentNode;
	hidden = td.childNodes[3];
	hidden.value = "";
	image = td.childNodes[1];
	image.src = "";
	td.removeChild(this_button);
	td.removeChild(image);	
}

</script>

<?php

/**
 * @author Taras Prystavskyj
 * @copyright 2009
 */

include_once("html_dom.php");
include_once("common.inc");

define("FIELD_WIDTH_MULTIPLER","2");

function get_content_filter_for_table($table_name = "", $a_foreign_keys= null, $id_name = "ID", $dir_with_images = '')
{
	if(isset($_POST['command'])) 
		$command = $_POST['command']; 
	elseif(isset($_GET['command']))
		$command = $_GET['command']; 
	else $command = "";
	
	// make a note of the current working directory relative to root.
	$directory_self = str_replace(basename($_SERVER['PHP_SELF']), '', $_SERVER['PHP_SELF']);

	// make a note of the location of the upload handler script
	$uploadHandler = 'includes/upload.processor.php';
	
	// set a max file size for the html upload form
	$max_file_size = 1000000; // size in bytes 
	
	//split it by pages
define("MAX_DISPLAY_SEARCH_RESULTS","1");

define("TEXT_DISPLAY_NUMBER_OF_PRODUCTS",'Відображено від <b>%d</b> по <b>%d</b> (з <b>%d</b> записів)');//

define('TEXT_RESULT_PAGE', 'Cторінки:');//

define('MAX_DISPLAY_PAGE_LINKS', '5');//Number of \'number\' links use for page-sets

	
	if(!$table_name)
		 error("specify table_name for function get_content_filter_for_table", "get_content_filter_for_table");
		 
//GET table fields properties
$sql = "SELECT column_name, column_comment, input_type, input_tag, required, data_type, column_type, size
FROM form_view
WHERE table_name = '$table_name'
ORDER BY ordinal_position
";
if( !($sql_result = mysql_query($sql)) )
	alert("table form_view not found");
	
$group_names = "";
$group_pseudos = "";
$i=0;
while($columns = mysql_fetch_assoc($sql_result))
{
	$group_names["$i"]=$columns['column_name'];
	$group_pseudos[$columns['column_name']]=$columns['column_comment'];
	$group_input_types[$columns['column_name']] = $columns['input_type'];
	$group_input_tags[$columns['column_name']] = $columns['input_tag'];
	$group_input_sizes[$columns['column_name']] = $columns['size'];
	$group_required[$columns['column_name']] = $columns['required'];
	$group_data_types[$columns['column_name']] = $columns['data_type'];
	$group_column_type[$columns['column_name']] = $columns['column_type'];
	$i++;
}

$sort_order = "ASC";
$this_page = "$_SERVER[PHP_SELF]";
$parameter_devider = "&";
$save_link = get_link($this_page, GET_parameters_string_despite(GET_parameters_despite_c_())."$parameter_devider")."command=update";

$GET_parameters = GET_parameters_string_despite(array("command"));


/**
 * 
 */
//modify table
switch ($command) 
{
	   case "update":
	   {
	   		//print_in_textarea($_POST);
	   		
	   		//exit();
	   		$i = 0;
	   		$s_group_pseudos = "";
	   		$values = "";
			   	   		
	   		if(isset($_POST) && isset($_POST[$id_name]) )//$_POST[$table_name]
	   		{
	   			$key_values = array();
	   			
		   		foreach($group_names as $field)//$_POST[$table_name]
		   		{
   					if( array_key_exists($field, $_POST) )
					{	
						if($field != $id_name)
						{
		   					$value = $_POST[$field];
		   					//if(strpos($value,"\""))
		   					//	print_in_textarea($value);
		   					//$value = mysql_real_escape_string($value);
							$value = str_replace('"','\"',$value);
							//$value = utf8_to_cp1251($value);
							$key_values[$field] = $value;
				   			$i++;			   							   			
				   		}
		   			}
		   			elseif( $group_input_types[$field] == "checkbox")
		   			{
		   				//print_in_textarea($field);
						if($field != $id_name)
						{
		   					$value = "0";//default for checkboxes
		   					$key_values[$field] = $value; 
				   			$i++;			   							   			
				   		}			   				
		   			}
		   		}
		   		
		   		$parameters = "`$id_name` = '".$_POST[$id_name]."'";
		   		
		   		tep_db_perform($table_name, $key_values, "update" , $parameters);
		   		
	   		}	   		
			break;   		
	   }
		
   case "видалити":
   		//print_in_textarea($_POST);
   		$sql = "delete from $table_name where $id_name = ".$_GET[$id_name];
   		if(mysql_query($sql))
   		{
   			//print_in_textarea(1);
   			alert("Запис видалено");
   			
   		}
   		else
   			alert("Проблема із виконанням запиту '$sql'");
   		
   		$_GET['command'] = null;
   		break;   	

   	case "insert":
   			$sql = "select max($id_name) as $id_name from $table_name";
   			//print_in_textarea($sql);
   			$result = mysql_query($sql);
   			$row = mysql_fetch_assoc($result);
   			$ID = $row[$id_name] + 1;
	   		$sql = "insert into $table_name ($id_name) values ($ID)";
	   		mysql_query($sql);	   		
	   		//print $ID;
	   		//exit();
	   		
	   		$link = get_link($this_page, 
			   GET_parameters_string_despite(
			   	array(GET_parameters_despite_c_(),"page","command"))."&page=$ID"
				   );
			redirect($link);
   		break;
   		
   	case "publish":
	   		$ID = $_GET['ID'];
	   		$sql = "update $table_name set active='1'";
	   		if(mysql_query($sql))
	   			alert("запис №$ID опібліковано");
	   		else
	   		{
	   			error_report("unable to run query '$sql'","select_view_with_form.php: $page_title: $command: line 119",time());
	   			alert("зверніться до служби підтримки для публікування запису №$ID ");
	   		}
	   		$link = get_link($this_page, 
			   GET_parameters_string_despite(
			   	array(GET_parameters_despite_c_(),"page","command"))
				   );
			redirect($link);

   	break;

}
/**
*
*/


$condition = 1;//where part to all selects
//lets get all selected conditions from _GET
foreach($_GET as $key=>$value)
{
	if(preg_match("@c_.*@", $key))
	{
		$key = preg_replace("@c_(.*)@", "$1", $key);
		$condition.= " and `$key`='$value'";		
	}		
}

//$page = new content_element(content_element_type::tag,"html");
//$header = new content_element(content_element_type::tag, "html");
//$page->add_content_element($header);
//$body = new content_element(content_element_type::tag, "body");

$table_filter = new data_table();
$form = new content_element(content_element_type::tag,"form","");

//add content of table	
$column_names = "*";
$sql = "select $column_names from `$table_name` where $condition 
		";
$test_result = mysql_query($sql);
if(mysql_num_rows($test_result) == 0)
{
	//create new row if table is empty
	redirect(get_link($this_page, GET_parameters_string_despite(array(GET_parameters_despite_c_(),"ID" ))."$parameter_devider")."command=insert");
}

$listing_split = new splitPageResults($sql, MAX_DISPLAY_SEARCH_RESULTS, $id_name);

if(isset($_GET['ID']))
{
	$listing_split->sql_query = preg_replace("@(limit \d*, \d*)@","and ID = $_GET[ID] $1",	 $listing_split->sql_query);
}

$file_column_name = "";
	
$rows = 0;
$sql_row1 = "0"; 
$listing_query = mysql_query($listing_split->sql_query);
while ($sql_row = mysql_fetch_assoc($listing_query)) 
{
	$sql_row1 = $sql_row;
	$rows++;
	$file_column_name = "";
	
	foreach ($sql_row as $column=>$value) {
		//to make possible printing anyware
		$value = stripcslashes($value);
		$value = stripcslashes($value);
		$value = htmlspecialchars($value); 
		$value = str_replace("'","&#039;",$value);
		$html_row = new table_row();
	
		$tag_name = $group_input_tags[$column];			
		$input_type = $group_input_types[$column];
		$size = $group_input_sizes[$column];

		$edit_box = new content_element();	
		$cell = new table_cell();	

		switch($group_data_types["$column"])
		{
			case "enum":
				$edit_box = get_select_for_enum($column, $group_column_type["$column"],$value);
				
				$edit_box = $edit_box->get_html();
			break;

			case "":
			
			$edit_box = new content_element(content_element_type::tag,"$tag_name");
	
			$edit_box->add_parameter("type",$input_type);			
			$edit_box->add_parameter("name",$column);
			$edit_box->add_parameter("size",$size*FIELD_WIDTH_MULTIPLER);//TODO: 
			$edit_box->add_parameter("style","width:".($size*10));
			
			if($tag_name == "input") 
				$edit_box->add_parameter("value", $value);
			elseif( $tag_name == "textarea" )
			{
				$edit_box->add_content_element($value);
				$edit_box->add_parameter("style", "height:200px; width:300px;");
			}
			
			//replace values with foreign keys
			$in_foreign_keys_column = false;
			if(isset($a_foreign_keys))
				foreach($a_foreign_keys as $o_foreign_key)
				{
					if( $o_foreign_key->child_foreign_key == $column)
					{
						$foreign_key_select = $o_foreign_key->get_select_from_parent_table($value, $sql_row[$id_name]);
						$edit_box = $foreign_key_select;
						$in_foreign_keys_column = true;
					}
				}

			if(! $in_foreign_keys_column)
			switch($input_type) 	
			{
				case "file":
					$file_column_name = $column;
					
					if($value != "")
					{
					$image = new content_element(content_element_type::tag, "img");
					$image->add_parameter('src','../images/'.$value); 	
					//$image->add_parameter("height","25");
					$image->add_parameter("width","300");
					$image_link = new content_element(content_element_type::tag, "a","",$image);
					$image_link->add_parameter("href",'../images/'.$value);
					$cell->add_content_element($image_link);
					$cell->add_content_element('<input type="hidden" name="'.$column.'" value="'.$value.'"/>');
					$cell->add_content_element('<button type="button" name="'.$column.'" value="" onclick="clear_image_field_for(this);"  /><img alt="" height="16" width="16" src="images/b_drop.png"/></button>');//TODO: remove image button
					
					$edit_box ->add_parameter("size","4");
					}
					else 
						$edit_box ->add_parameter("size","10");
				break;
				
				case "checkbox":
					unset($edit_box->parameters["value"]); 
					$edit_box->add_parameter("value","1"); 
					if($value==1 || $value=="on") $edit_box ->add_parameter("checked","true");
				break;
			
	
			}//	switch($input_type) 	
						
			break;//data_type switch
			
		}		

		$cell->add_content_element($edit_box);

	    if($column == $id_name )//print ID as text
	    {
			$cell->add_content_element("$value");				
		}

		$required_notification = ($group_required[$column]?'<span class="form-required" title="Обов\'язкове поле">*</span>':"");
		$td1 = new content_element(content_element_type::text,'<td class="name">
 <label for="edit-title">'.$group_pseudos[$column].$required_notification.'</label></td>
');
		$html_row->add_content_element($td1);
		$html_row->add_content_element($cell);	
		$table_filter->add_content_element($html_row);
	}//for each field	
	
	$html_row = new table_row();
	//add update and видалити buttons
	$save_button = new content_element(content_element_type::tag, "input", "", "");
	$save_button->add_parameter("onclick","submit();");
	$save_button->add_parameter("type","submit");
	$save_button->add_parameter("name","command");
	$save_button->add_parameter("title","Зберегти");
	$save_button->add_parameter("value","update");
	$cell = new table_cell($save_button);
	$html_row->add_content_element($cell);
	
//delete button:
 	$delete_link = get_link($this_page, GET_parameters_string_despite( array("command") )."$parameter_devider"."command=видалити&".$id_name."=".$sql_row[$id_name]);
	$delete_button = new content_element(content_element_type::text, "<image height='32' width='32' title='видалити' alt='видалити' onclick='javascript:  		
		answer = confirm(\"Ви справді хочете видалити цей запис?\");
	 	if(answer==true)
	 		window.location = \"$delete_link\";			

' src='images/b_drop.png'/>");
	$cell = new table_cell($delete_button);
	
	if(isset($_GET['page']))
		$page = $_GET['page'];
	else 
		$page = "";
	if($page != "new")$html_row->add_content_element($cell);
	
	//add table to body
	$table_filter->add_content_element($html_row);
	$table_filter->add_parameter("align","left");
	//$body->add_content_element($table_filter);
	//$page->add_content_element( $body);	
	//$table_filter->rows[0]->add_cell("asdf");

	
	$table_filter->add_parameter("border", "0");
	$table_filter->add_parameter("class","curved_all");
	$form->add_content_element($table_filter);
	$form->add_parameter("method","post");
	$form->add_parameter("id",$table_name);
	$form->add_parameter("enctype","multipart/form-data");//for transfering files
	$form->add_parameter("action",$uploadHandler);
	  		
}//while sql_row

	//$pagination = new content_element(content_element_type::text,$s);
	
	
$form->add_content_element('<input type="hidden" name="MAX_FILE_SIZE" value="'.$max_file_size.'"/> 
<input type="hidden" name="upload_form_page" value="../../../'.$save_link.'"/> 
<input type="hidden" name="upload_success_page" value="../../../'.$save_link.'"/> 
<input type="hidden" name="fieldname" value="'.$file_column_name.'"/> 
<input type="hidden" name="uploadsDirectory" value="'."../../images/".$dir_with_images."".'" />
<input name="submit" id="edit-submit" value="Зберегти" class="form-submit" type="hidden"/>
 ');


//add pagination 
if ( ($listing_split->number_of_rows > 0) ) 
{
	$s = '<table border="0" width="100%" cellspacing="0" cellpadding="2">
	<tr>
	<td align=\'right\' class="smallText">'.$listing_split->display_count(TEXT_DISPLAY_NUMBER_OF_PRODUCTS) .'</td></tr>
	<tr>
	<td class="smallText" align="right">'.TEXT_RESULT_PAGE . ' ' . $listing_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('ID','page', 'info', 'x', 'y'))).'</td>
	</tr>
	</table>';
	
	$pagination = new table_cell($s);
}

//finaly create main table
$table_main = new data_table();

//add goto_form_view button
$link = get_link($this_page, GET_parameters_string_despite(array(GET_parameters_like_c_(),$id_name, "mode", "page", "command" )).$parameter_devider)."mode=table&".$id_name."=".$sql_row1[$id_name]."#".$sql_row1[$id_name];


$cell = new table_cell("<input type='button' name='goto_form_view' value='Перейти до табличного вигляду' onclick='window.location=\"".$link."\"'
>");
$html_row = new table_row($cell);
$table_main ->add_content_element($html_row);

//add select_view contents to this table
$new_cell = new table_cell($form);
$new_row = new table_row($new_cell);
$table_main->add_content_element($new_row);


$new_row = new table_row($pagination);
$table_main -> add_content_element($new_row);

$new_row = new table_row("<td><input type='button' name='page' value='Додати новий запис'
onclick='window.location=\"".get_link($this_page, GET_parameters_string_despite(array(GET_parameters_despite_c_(),"ID","command" ))."$parameter_devider")."command=insert"."\"'
 ></td>");
 $table_main->add_content_element($new_row);

return $table_main;

}
?>