<?php

/**
 * @author Taras Prystavskyj
 * @copyright 2009
 */
define("INDENT_SYMBOL"," "); 
 
class content_element_type
{
    const text = "text";
    const tag = "tag";
    const coment = "coment";
}

class content_element //html or object

{
    var $type = content_element_type::text;
    var $html = '';
    var $parameters = array(); //or string list of parameters
    var $content_elements = array(); //or one object or string HTML
    var $tag_name = '';

    public function content_element($type = content_element_type::text, $name = "",
        $parameters = "", $son_content_elements = "" /* or array(content_element)*/ )
    {
        $this->type = $type;
        $this->tag_name = $name;

        if (is_array($parameters))
            $this->parameters = $parameters;
        elseif ($parameters != '')
            $this->parameters = $parameters;

        if (is_array($son_content_elements)) {
            $this->content_elements = $son_content_elements;
        } elseif (is_object($son_content_elements))
            $this->content_elements[] = $son_content_elements;
        elseif (is_string($son_content_elements) && $son_content_elements != '')
            $this->content_elements[] = $son_content_elements;
    }
    public function get_html($indent="")
    {
    	switch($this->type)
    	{
    		case content_element_type::tag: 
		    		        $this->html .= "$indent<" . $this->tag_name . " ";
		        foreach ($this->parameters as $name => $value) {
		            $this->html .= " " . $name . "='" . $value . "'";
		        }
		        $this->html .= ">
		";
		        if (is_array($this->content_elements))
		            foreach ($this->content_elements as $son_content_element) {
		                if ($son_content_element instanceof content_element)
		                    $this->html .= $son_content_element->get_html($indent.INDENT_SYMBOL);
		                else
		                    $this->html .= $son_content_element.'
							';
		            }
		        $this->html .= "$indent</" . $this->tag_name . ">
		";
				break;
				
			case content_element_type::text:
				$this->html = $this->tag_name;
				break;
			case content_element_type::comment:
				$this->html = "<!-- ".$this->tag_name." -->";
				break;
    	}
    	
    	        return $this->html;

    }
    public function close_content_element()
    {
        $this->html .= "</" . $this->tag_name . ">
";
    }
    public function add_parameter($name, $value)
    {
        $this->parameters[$name] = $value;
    }
    public function add_content_element($content_element)
    {
    	if( is_object($content_element) )
	        $this->content_elements[] = $content_element;
	    elseif( is_string($content_element) || is_null($content_element))
	    {
	    	$this->content_elements[] = new content_element(content_element_type::text, $content_element);
	    }
		else
	    {
	    	error("тип нового елемента контенту має бути стрічка або content_element","function add_content_element");
	    }
    }
}

class sort_button extends content_element
{
    function sort_button($value)
    {
        parent::content_element(content_element_type::tag, 'input', array(type =>
            "button", value => $value, style => "height:inherit; width:inherit;"), "");
    }
}

class table_cell extends content_element
{
    function table_cell($content = "")
    {
        parent::content_element(content_element_type::tag, 'td', '', $content);
    }
}

class table_row extends content_element
{
//    var $cells = array(table_cell);
    var $cells = array();
    function table_row($content = "")
    {
        parent::content_element(content_element_type::tag, "tr", '', $content);
        $this->cells[] = new table_cell;
    }

    public function add_cell($content = "")
    {
        $td = new table_cell($content);
        parent::add_content_element($td);
    }
}

class data_table extends content_element
{
//    var $rows = array(table_row);
    var $rows = array();
    function data_table($content = "")
    {
        parent::content_element(content_element_type::tag, "table", '', $content);
        $this->row[] = new table_row;
    }
    public function add_row($content = "")
    {
        $tr = new table_row($content);
        parent::add_content_element($tr);
    }
    public function add_rows($number)
    {
        for ($i = 1; $i <= $number; $i++) {
            $this->add_row();
        }
    }
}

//<select> classes
class option extends content_element
{
    function option($content = "")
    {
        parent::content_element(content_element_type::tag, 'option', '', $content);
    }
}

class select extends content_element
{
//    var $cells = array(table_cell);
    var $cells = array();
    function select($content = "")
    {
        parent::content_element(content_element_type::tag, "select", '', $content);
        $this->cells[] = new table_cell;
    }

    public function add_option($content = "")
    {
        $option = new option($content);
        parent::add_content_element($option);
    }
}

?>