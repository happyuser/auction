<script language="JavaScript">
function clear_image_field_for(this_button)
{
	//alert("ця кнопка працюватиме пізніше");
	td = this_button.parentNode;
	hidden = td.childNodes[3];
	hidden.value = "";
	image = td.childNodes[1];
	image.src = "";
	td.removeChild(this_button);
	td.removeChild(image);
}
</script>
<?php
/**
 * @author Taras Prystavskyj
 * @copyright 2009

27/08/09
На замовлення Володі
додати розбиття на сторінки по 50 на сторінку 
*/

include_once("html_dom.php");
include("common.inc");
define("SELECTED_COLUMN_COLOR","#E0E0E0");
define("CHAR_WIDTH_IN_PIXELS","3");//2.95

function get_content_filter_for_table($table_name = "", $a_foreign_keys= null, $o_nested_table = null, $id_name = "ID", $dir_with_images = '', $add_pagination = false, $readonly = false)
{
	if(isset($_POST['command']))
		$command = $_POST['command'];
	elseif(isset($_GET['command']))	
		$command = $_GET['command'];
	else $command = "";

	//GET table fields properties
	$sql = "SELECT column_name, column_comment, input_type, input_tag, size, data_type, column_type
	FROM form_view
	WHERE table_name = '$table_name'
	ORDER BY ordinal_position";
	if( !($sql_result = mysql_query($sql)) )
		alert("table form_view not found");

	$group_names = "";
	$group_pseudos = "";
	$i=0;
	while($columns = mysql_fetch_assoc($sql_result))
	{
		$group_names[$i]=$columns['column_name'];
		$group_pseudos[$columns['column_name']]=$columns['column_comment'];
		$group_input_types[$columns['column_name']] = $columns['input_type'];
		$group_input_tags[$columns['column_name']] = $columns['input_tag'];
		$group_input_sizes[$columns['column_name']] = $columns['size'];
		$group_data_types[$columns['column_name']] = $columns['data_type'];
		$group_column_type[$columns['column_name']] = $columns['column_type'];
		$i++;
	}

		if ($command == "видалити" )
		{
			//print_in_textarea($_POST);
			$sql = "delete from $table_name where $id_name = ".$_GET[$id_name];
			if (mysql_query($sql)) {
				//print_in_textarea(1);
				alert("Запис видалено");
			} else alert("Проблема із виконанням запиту '$sql'");
		}

		if (($command == "update") || ($command == "insert")) {
			//print_in_textarea($_POST); //TODO: 1
			//exit();
			$i = 0;
			$s_group_pseudos = "";
			$values = "";

			if( isset($_POST) )//$_POST[$table_name]
			{
				$key_values = array();
	   			
		   		foreach($group_names as $field)
		   		{
		   			if( array_key_exists($field, $_POST) )
					{	
						if($field != $id_name)
						{
		   					$value = $_POST[$field];
		   					//if(strpos($value,"\""))
		   					//	print_in_textarea($value);
		   					//$value = mysql_real_escape_string($value);
							$value = str_replace('"','\"',$value);
							//$value = utf8_to_cp1251($value);
							$key_values[$field] = $value; 
				   			$i++;
				   		}
		   			}
		   			elseif( $group_input_types[$field] == "checkbox")
		   			{
		   				//print_in_textarea($field);
						if($field != $id_name)
						{
		   					$value = "0";//default for checkboxes
		   					$key_values[$field] = $value; 
				   			$i++;
				   		}
		   			}
		   		}
		   		
		   		if($command == "update")
		   		{
		   			$parameters = "`$id_name` = '".$_POST[$id_name]."'";
		   		}
		   		else
		   			$parameters = "";

                //mysql_query("SET NAMES cp1251");
                mysql_query("SET NAMES utf8");

		   		tep_db_perform($table_name, $key_values, $command , $parameters);
	   		}
	   }

	// make a note of the current working directory relative to root.
	$directory_self = str_replace(basename($_SERVER['PHP_SELF']), '', $_SERVER['PHP_SELF']);
//print basename($_SERVER['SERVER_NAME']);
	// make a note of the location of the upload handler script
	$uploadHandler = '/include/upload.processor.php';//"http://".$_SERVER['SERVER_NAME'].

    //print  $uploadHandler;
	//exit;
	// set a max file size for the html upload form
	$max_file_size = 1000000; // size in bytes 
	
	if(!$table_name)
		 error("specify table_name for function get_content_filter_for_table", "get_content_filter_for_table");

if(isset($_POST['command']))
	$command = $_POST['command']; 
else $command = "";

$condition = 1;//where part to all selects
//lets get all selected conditions from _GET
foreach($_GET as $key=>$value)
{
	if(preg_match("@c_.*@", $key))
	{
		$key = preg_replace("@c_(.*)@", "$1", $key);
		$condition.= " and `$key`='$value'";
	}
}

$sort_order = "ASC";
$this_page = "$_SERVER[PHP_SELF]";
$parameter_devider = "&";
$save_link = get_link($this_page, GET_parameters_string_despite( array("command") )."$parameter_devider"."command=update");

$GET_parameters = GET_parameters_string_despite(array("command"));

//$page = new content_element(content_element_type::tag,"html");
//$header = new content_element(content_element_type::tag, "html");
//$page->add_content_element($header);
//$body = new content_element(content_element_type::tag, "body");

//Select part
$one_row = new table_row("");

//add operations column
$operations_header = new table_cell("Операції із записами");
$operations_header->add_parameter("style","width: 20%;");
$one_row->add_content_element($operations_header);

foreach($group_names as $group_name) {
	$select = new select("");

	if(isset($group_input_sizes[$group_name]))
	{
		$size = $group_input_sizes[$group_name];
		$style = " width:" . ($size*10+20) ."px;";
	}

	if(array_key_exists("c_".$group_name,$_GET))
	{
		$style .= "background-color:".SELECTED_COLUMN_COLOR.";";
	}

	$select->add_parameter("style",$style);

	if(isset($group_pseudos[$group_name]) )
		$first_cell = new option($group_pseudos[$group_name]);
	else
		$first_cell = new option($group_name);
	$select->add_content_element($first_cell);

//add view all option
	$option = new option("<b>* показати всі</b>");
		$option->add_parameter("onclick","window.location = \"".get_link( $this_page, GET_parameters_string_despite(array('c_'.$group_name)) )."\"");
        $select->add_content_element($option);

	if($group_input_tags[$group_name]!= 'textarea')
	{
		$sql = "select distinct $group_name from $table_name where $condition 
			order by $group_name $sort_order";	
		$sql_result = mysql_query($sql);		
	    while( is_resource($sql_result) && $distinct_value = mysql_fetch_array($sql_result)) 
		{
			$distinct_value[0] = tep_htmlspecialchars($distinct_value[0]);
			$text = $distinct_value[0];
			if(isset($a_foreign_keys))
				foreach($a_foreign_keys as $o_foreign_key)
				{
					if($group_name == $o_foreign_key->child_foreign_key)
						$text = $o_foreign_key->get_value_by_key($distinct_value[0]);
				}

	        $option = new option("$text");
			$option->add_parameter("onclick","window.location = \"".get_link($this_page, $GET_parameters."$parameter_devider"."c_$group_name=$distinct_value[0]"."\"") );
	        $select->add_content_element($option);
	    }
    }

    $cell = new table_cell($select);
    $cell->add_parameter("style", " border-width:1px;");
   	$one_row->add_content_element($cell);    

}

$table_filter = new data_table();$table_filter->add_parameter("border", "0");
$table_filter->add_parameter("class", "main_table");
$cell = new table_cell("");//зберегти
$one_row->add_content_element($cell);
$cell = new table_cell("");//видалити
$one_row->add_content_element($cell);
$one_row->add_parameter("class","header_row");

$table_filter->add_content_element($one_row);

//add content of table
$column_names = "*";
$sql = "select $column_names from `$table_name` where $condition";
//lets add pagination if neaded
if($add_pagination)
{
	define("MAX_DISPLAY_SEARCH_RESULTS","50");
	define('PREV_NEXT_BAR_LOCATION', '2'); //Sets the location of the Prev/Next Navigation Bar (1-top, 2-bottom, 3-both)'
	define("TEXT_DISPLAY_NUMBER_OF_PRODUCTS",'Відображено <b>%d</b>-<b>%d</b> (із <b>%d</b> записів)');//
	define('TEXT_RESULT_PAGE', 'Cторінки:');
	define('MAX_DISPLAY_PAGE_LINKS', '5');//Number of \'number\' links use for page-sets

	$listing_sql = $sql;
	$listing_split = new splitPageResults($listing_sql, MAX_DISPLAY_SEARCH_RESULTS, $id_name, "page");
	$sql = $listing_split->sql_query;
}

$sql_result = mysql_query($sql);
$i = 0;
while($sql_row = mysql_fetch_assoc($sql_result))//rows of table
{
	$i++;//index of row
	$file_column_name = "";	
	$html_row = new table_row();

//add зберегти and видалити buttons
	$cell = new table_cell();
	$cell ->add_content_element("<a name='".$sql_row[$id_name]."'></a>");

	if(!$readonly)//coment only
        {
    $save_button = new content_element(content_element_type::text, '<input type="image" class="icon" height="16" width="16" title="зберегти" alt="зберегти" onclick="submit();" name="command" value="update" src="images/b_save.png"/>&nbsp;&nbsp;');
    	 $cell->add_content_element($save_button);//TODO: організувати зберігання коментаря

    	$form_page = ($listing_split->current_page_number - 1) * $listing_split->number_of_rows_per_page + $i;	 
    //adding goto_form view 
    	$link = get_link($this_page, GET_parameters_string_despite(array(array_keys(GET_parameters_despite_c_()),$id_name, "command" ))."$parameter_devider").$id_name."=".$sql_row[$id_name]."$parameter_devider"."page=".$form_page ."&mode=form"; // goto direct ID
    	$cell->add_content_element("<img height='16' width='16' title='Розгорнути' alt='Розгорнути' src='images/b_browse.png' onclick='javascript: window.location = \"$link\";'
     />&nbsp;&nbsp;");

    //delete button:
     	$delete_link = get_link($this_page, GET_parameters_string_despite( array("command") )."$parameter_devider"."command=видалити&".$id_name."=".$sql_row[$id_name]."#".($sql_row[$id_name]-1) );
    	$delete_button = new content_element(content_element_type::text, "<image height='16' width='16' title='видалити' alt='видалити' onclick='javascript:  		
    		answer = confirm(\"Ви справді хочете видалити цей запис?\");
    	 	if(answer==true)
    	 		window.location = \"$delete_link\";	

    ' src='images/b_drop.png'/>");
    	$cell->add_content_element($delete_button);
    	}

//adding goto nested table (for example subcategory for firm)
	if($o_nested_table && $o_nested_table->link)
	{
		$link = $o_nested_table->link.$sql_row[$o_nested_table->field];
		$cell->add_content_element("<img height='16' width='16' title='Підкатегорії' alt='Підкатегорії' src='images/b_props.png' onclick='javascript: window.location = \"$link\";'
	 />");
	}

	$html_row ->add_content_element($cell);

	$row_color = "#".strtoupper(dechex((255-($i%2+1)*30)%255))."FF99";//.strtoupper(dechex((180+$i%4*4)%255));
	$row_style = "background-color: $row_color;";

	if(isset($_GET['id']) && $_GET['id'] == $sql_row[$id_name] )
	{
		$row_style.= "background-color: white;";
	}
	$html_row ->add_parameter("style",$row_style);
//adding fields
	foreach ($sql_row as $column => $value) {
		$value = tep_htmlspecialchars($value);
		$tag_name = (isset($group_input_tags[$column]) ?
			 $group_input_tags[$column] : '');
		$input_type = (isset($group_input_types[$column]) ?
			$group_input_types[$column] : '');

		$edit_box = new content_element(content_element_type::tag,"$tag_name");

		if(isset($group_input_sizes[$group_name]))
		{
			$size = $group_input_sizes[$group_name];
			$edit_box->add_parameter("size",$size*CHAR_WIDTH_IN_PIXELS);
		}

		switch (isset($group_data_types["$column"]) ? $group_data_types["$column"] : "") {
			case "enum":
				$edit_box = get_select_for_enum($column, $group_column_type["$column"],$value);
				$edit_box = $edit_box->get_html();
				$cell = new table_cell($edit_box);
				break;

			case "":
				$edit_box->add_parameter("type",$input_type);			
				$edit_box->add_parameter("name",$column);
				$edit_box->add_parameter("style","width:".($size*10)."px;");
				if($tag_name == "input") 
					$edit_box->add_parameter("value", $value);
				elseif( $tag_name == "textarea" )
					$edit_box->add_content_element($value);

				//replace values with foreign keys
				if(isset($a_foreign_keys)) foreach($a_foreign_keys as $o_foreign_key)
				{
					if($o_foreign_key->child_foreign_key == $column)
					{
						$foreign_key_select = $o_foreign_key->get_select_from_parent_table($value, $sql_row[$id_name]);
						$edit_box = $foreign_key_select;
					}
				}

				$cell = new table_cell();
				switch($input_type)
				{
					//add image representation
					case "file":
						$file_column_name = $column;
						if($value != "")
						{
							$image = new content_element(content_element_type::tag, "img");
							$image->add_parameter('src','../images/'.$value);			
							$image->add_parameter("height","25");
							$image->add_parameter("width","25");
							$image_link = new content_element(content_element_type::tag, "a","",$image);
							$image_link->add_parameter("href",'../images/'.$value);
							$cell->add_content_element($image_link);
							$cell->add_content_element('<input type="hidden" name="'.$column.'" value="'.$value.'"/>');
							$cell->add_content_element('<button type="button" name="'.$column.'" value="" onclick="clear_image_field_for(this);"  /><img alt="" height="16" width="16" src="images/b_drop.png"/></button>');//TODO: remove image button
							$edit_box ->add_parameter("size","4");
						}
						else 
							$edit_box ->add_parameter("size","10");
						break;

					case "checkbox": 
						unset($edit_box->parameters["value"]); 
						$edit_box->add_parameter("value","1"); 
						if("$value"=="1") $edit_box ->add_parameter("checked","");
						break;
				}

				if($readonly && ($input_type != "hidden" && ($tag_name != "textarea")))
                {
					//if($column != "message")
					$cell->add_content_element("$value");
					$edit_box ->add_parameter("type","hidden");
					//$edit_box ->add_parameter("disabled","");
					$cell->add_content_element($edit_box);
				}
				else
				{
					if($column != "message" && ($tag_name != "textarea"))
					$cell->add_content_element($edit_box);
				}

				if($column == "message")
				{
					$edit_box ->add_parameter("disabled","");
					$cell->add_content_element($edit_box);
					//$cell->add_content_element("$value");
				}
				//TODO: print values
				break;
		}

	    if ($column == $id_name )//print ID as text
	    {
	       $edit_box ->add_parameter("type","hidden");
	       $cell->add_content_element($edit_box);
			//$cell->add_content_element("$value");
		}

		$html_row->add_content_element($cell);
	}//foreach ($sql_row as $column=>$value)

	$form = new content_element(content_element_type::tag,"form","",$html_row);
	$form->add_parameter("method","post");
	$form->add_parameter("enctype","multipart/form-data");//for transfering files
	$form->add_parameter("action",$uploadHandler);
//	if($file_column_name)
		$form->add_content_element('<input type="hidden" name="MAX_FILE_SIZE" value="'.$max_file_size.'"/> 
<input type="hidden" name="upload_form_page" value="../../../'.$save_link.'"/> 
<input type="hidden" name="upload_success_page" value="../../../'.$save_link."#".$sql_row[$id_name].'"/> 
<input type="hidden" name="fieldname" value="'.$file_column_name.'"/> 
<input type="hidden" name="uploadsDirectory" value="'."../../images/".$dir_with_images."".'" />
<input name="submit" id="edit-submit" value="зберегти" class="form-submit" type="hidden"/>
 ');
	$table_filter->add_content_element($form);
	$table_filter->add_parameter("border", "0");
}

//add pagination
  if ( $add_pagination && ($listing_split->number_of_rows > 0) )
  {
		$pages_links = '
	<tr>
		<td COLSPAN="3" style="text-align:left;" align="left">
		<table border="0" width="100%" cellspacing="0" cellpadding="2" align="left">
		<tr>
	    	<td align="left" class="smallText">'.$listing_split->display_count(TEXT_DISPLAY_NUMBER_OF_PRODUCTS).
			'</td>
		</tr>
		<tr>
	    <td class="smallText" align="left">'.
			TEXT_RESULT_PAGE .' '.
			$listing_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info', 'x', 'y', 'command'))).
		'</td>
		  </tr>
		</table>
		</td>
	</tr>';
		$table_filter->add_content_element($pages_links);
	}
//and of pagination

$file_column_name = "";
///////////////////////////////////////////////////
//row to apply posibility to add new row to table
///////////////////////////////////////////////////
$html_row = new table_row();

$save_button = new content_element(content_element_type::text, '<input type="image" class="icon" height="16" width="16" title="зберегти" alt="зберегти" onclick="submit();" name="command" value="insert" src="images/b_save.png"/>');
$cell = new table_cell($save_button);
$html_row->add_content_element($cell);

foreach ($group_names as $column_name)
{
	if(array_key_exists("c_".$column_name,$_GET))
	{
		$value = $_GET["c_".$column_name];
	}
	else
	{
		$value = "";
	}
	$edit_box = new content_element(content_element_type::text,"");

	//replace default values with foreign key selects
	$in_foreign_keys_column = false;
	if(isset($a_foreign_keys))//TODO: o_foreign_key
	{
		foreach($a_foreign_keys as $o_foreign_key)
		{
			if($column_name == $o_foreign_key->child_foreign_key)
			{
				$foreign_key_select = $o_foreign_key->get_select_from_parent_table($value);
				$edit_box = $foreign_key_select->get_html();				
				$in_foreign_keys_column = true;
			}
		}
	}

	if (!$in_foreign_keys_column) {
		switch ($group_data_types["$column_name"]) {
		case "enum":
			$edit_box = get_select_for_enum($column_name, $group_column_type["$column_name"],"");
			$edit_box = $edit_box->get_html();
			break;

		default:
			$tag_name = $group_input_tags[$column_name];
			$input_type = $group_input_types[$column_name];
			$edit_box = new content_element(content_element_type::tag, "$tag_name");
			$edit_box->add_parameter("type","$input_type");
			$edit_box->add_parameter("name",$column_name);
			$edit_box ->add_parameter("size",$group_input_sizes[$column_name]);
			if ($input_type == "file") {
				$file_column_name = $column_name;
				$edit_box ->add_parameter("size",10);
			}
			$edit_box->add_parameter("value", $value);
			$edit_box->add_parameter("disabled", "disabled");
			break;
		}
	}

	$cell = new table_cell($edit_box);
	$html_row->add_content_element($cell);
}

$form = new content_element(content_element_type::tag,"form","",$html_row);
	$form->add_parameter("method","post");
	$form->add_parameter("enctype","multipart/form-data");//for transfering files
	$form->add_parameter("action",$uploadHandler);

	$form->add_content_element('<input type="hidden" name="MAX_FILE_SIZE" value="'.$max_file_size.'"/> 
<input type="hidden" name="upload_form_page" value="../../../'.$save_link.'"/> 
<input type="hidden" name="upload_success_page" value="../../../'.$save_link.'"/> 
<input type="hidden" name="fieldname" value="'.$file_column_name.'"/> 
<input type="hidden" name="uploadsDirectory" value="'."../../images/".$dir_with_images."".'" />
<input name="submit" id="edit-submit" value="зберегти" class="form-submit" type="hidden"/>
 ');
$table_filter->add_content_element($form);

//add table to body
$table_filter->add_parameter("align","left");
//$body->add_content_element($table_filter);
//$page->add_content_element( $body);

//$table_filter->rows[0]->add_cell("asdf");

return $table_filter;
}

function in_foreign_keys_column($a_foreign_keys, $column_name)
{
	if(is_array($a_foreign_keys))
	{
		foreach($a_foreign_keys as $o_foreign_key)
		{
			if(is_a($o_foreign_key, "foreign_key") && $column_name == $o_foreign_key->child_foreign_key)
			{
				return true;
			}
		}
	}
	
	return false;

}


?>