<?php

/**
 * @author 
 * @copyright 2009
 */
 
 ///for test mode
 //include_once("html_dom.php");
// include_once("../../includes/functions/common.inc");
// include_once("../functions/common.inc");
 
function tep_htmlspecialchars($value)
{
		$value = stripcslashes($value);
		$value = htmlspecialchars($value); 
		$value = str_replace("'","&#039;",$value);
	return $value;
}

function get_select_for_enum($name, $string_enum, $curent_value)
{
	$select = new content_element(content_element_type::tag,"select");
	$select->add_parameter("name","$name");
	
	preg_match_all("@enum\((.*)\)@im", $string_enum, $a);//('[^']+',)+
	
	$string_enum = $a[1][0];
	
	$a = preg_split("@[',]+@",$string_enum); 
	foreach($a as $key=>$value)
	{
		if($value == "")
			unset($a[$key]);
	}
	

	
	//build select
	
	//add null option
	$option = new option("");
	$option->add_parameter("value","");
	if($curent_value == "")
		$option->add_parameter("selected","true");
		
	$select->add_content_element($option);

	foreach($a as $key=>$value)
	{
		$option = new option("$value");
		$option->add_parameter("value",$value);
		
		$value = str_ireplace("&","_",$value);
	
		$curent_value = str_ireplace("&amp;","_",$curent_value );
	
		if($curent_value == $value)
		{
			$option->add_parameter("selected","true");
			$found_value = 1;
		}
			
		$select->add_content_element($option);
	}
	return $select;
}

//$select = get_select_for_enum('asdf',"enum('a b','c')",'1');

//print_in_textarea($select->get_html());

?>