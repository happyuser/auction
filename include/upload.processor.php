<?php

/**
 * @author http://www.webdeveloper.com/forum/showthread.php?t=101466
 * @copyright 2009
 */

include_once("functions/common.inc");

?>
<?php

// filename: upload.processor.php

// first let's set some variables
/*
print("POST");
print_in_textarea($_POST);
print("files");
print_in_textarea($_FILES);
exit();
*/

//qq
// make a note of the current working directory, relative to root.
$directory_self = str_replace(basename($_SERVER['PHP_SELF']), '', $_SERVER['PHP_SELF']);

// make a note of the directory that will recieve the uploaded file
if(isset($_POST['uploadsDirectory']))
	$uploadsDirectory = $_POST['uploadsDirectory'];
else 
	$uploadsDirectory = 'uploaded_files/';
$uploadsDirectory = $_SERVER['DOCUMENT_ROOT'] . $directory_self . $uploadsDirectory;

// make a note of the location of the upload form in case we need it
if(isset($_POST['upload_form_page']))
	$uploadForm_page = $_POST['upload_form_page'];
else 
	$uploadForm_page = 'upload.Form.php';
$uploadForm = $uploadForm_page;

// make a note of the location of the success page
if(isset($_POST['upload_success_page']))
	$uploadSuccess_page = $_POST['upload_success_page'];
else 
	$uploadSuccess_page = 'upload.success.php';
$uploadSuccess = 'http://' . $_SERVER['HTTP_HOST'] . $directory_self . $uploadSuccess_page;
//$uploadSuccess = $uploadSuccess_page;

// fieldname used within the file <input> of the HTML form
if(isset($_POST['fieldname']))
	$fieldname = $_POST['fieldname'];
else
	$fieldname = 'file';

// Now let's deal with the upload

// possible PHP upload errors
$errors = array(1 => 'php.ini max file size exceeded',
                2 => 'html form max file size exceeded',
                3 => 'file upload was only partial',
                4 => 'no file was attached');

// check the upload form was actually submitted else print the form
//(isset($_POST['submit']) || isset($_POST['command']))
 //   or error('the upload form is neaded', $uploadForm);
if( isset($_FILES[$fieldname]) && $_POST['command']!='видалити' && $_FILES[$fieldname]['error'] == 0)
{
	// check for PHP's built-in uploading errors
	($_FILES[$fieldname]['error'] == 0)
	    or error($errors[$_FILES[$fieldname]['error']], $uploadForm);
	    
	/*print_in_textarea($_FILES);
	print $fieldname;
	print_in_textarea($_FILES[$fieldname]);
	*/
	// check that the file we are working on really was the subject of an HTTP upload
	@is_uploaded_file($_FILES[$fieldname]['tmp_name'])
	    or error('not an HTTP upload', $uploadForm);
	    
	// validation... since this is an image upload script we should run a check  
	// to make sure the uploaded file is in fact an image. Here is a simple check:
	// getimagesize() returns false if the file tested is not an image.
	@getimagesize($_FILES[$fieldname]['tmp_name'])
	    or error('only image uploads are allowed', $uploadForm);
	    
	// make a unique filename for the uploaded file and check it is not already
	// taken... if it is already taken keep trying until we find a vacant one
	// sample filename: 1140732936-filename.jpg
	$now = time();
	preg_match("@\..*@", $_FILES[$fieldname]['name'], $a);
	$extension = $a[0];
	$Short_filename = $now.$extension;  //.'-'.$_FILES[$fieldname]['name'];  - with this long names not working copiing files and opening in browser
	
	//for suporting spaces in name we should change them to "_"
	//$Short_filename = preg_replace("@ @","_",$Short_filename);
	
	//print_in_textarea($Short_filename);
	
	while(file_exists($uploadFilename = $uploadsDirectory.$Short_filename))
	{
	    $now++;
	}
	
	// now let's move the file to its final location and allocate the new filename to it
	@move_uploaded_file($_FILES[$fieldname]['tmp_name'], $uploadFilename)
	    or error('receiving directory insuffiecient permission', $uploadForm);
}//$_POST[command]!= 'видалити'	    

//repost all data for computing
//print "<body onload='submit();'";
print '<form name="repost" action="'.$uploadSuccess.'" enctype="multipart/form-data" accept-charset="UTF-8" method="post" id="node-form">
';

//post file_name to add it to database
//alert($Short_filename );
//alert($_POST[$_POST['fieldname']]);
if($Short_filename != "" && $Short_filename != null)// if not empty
	{
	//alert("print");
	post_array( array_diff($_POST,array($_POST['fieldname'],'fieldname')) );	
	print  '<input type="hidden" name="'.$_POST['fieldname'].'" value="'.$Short_filename.'"/> 
';
	}
else
{
	post_array( array_diff($_POST,array('fieldname')) );
	//alert($_POST[$_POST['fieldname']]);
}
//must be one non hidden to repost this form:
print "<input style='border:none;' type='text' value=''>";
print("</form>");


// If you got this far, everything has worked and the file has been successfully saved.
// We are now going to redirect the client to a success page.
print "<script language='JavaScript'>  document.repost.submit(); </script>";
//header('Location: ' . $uploadSuccess);

?> 