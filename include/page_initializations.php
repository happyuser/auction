<?php
/**
 * @copyright 2009
 */

// Include application configuration parameters:
//include_once("include/configure.php");
require("include/configure.php");

include_once("include/functions/common.inc");
include_once("include/classes/split_page_results.inc");
include_once("include/functions/database.php");
include_once("include/functions/general.php");
include_once("include/functions/html_output.php");
include_once("include/functions/sessions.php");
session_start();
// Force autentification is disabled.
//if ((!isset($_SESSION['login'])) || (!isset($_SESSION['password']))) redirect("login.php");
if (isset($_GET['command']) && ($_GET['command']=='вийти')) {
	session_destroy();
	mine_session_destroy();
	redirect("index.php");
}
//include_once("include/db_connect.php");
?>