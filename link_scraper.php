<?php

/**
 * @author 
 * @copyright 2014
 */

include_once("configure.php");
include("database.php");
include("model_organic_link_scraper.php");
include("controller_organic_link_scraper.php");



?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="robots" content="noindex, nofollow"/>
<link rel="stylesheet" href="style.css" />

<style type="text/css">
   
    
</style>
</head>
<body onload="$('#page_shader').hide();">
<div id="page_shader" style="width: 100%; height: 100%; position: absolute; top: 0; left: 0; margin: 0; background-color: black; opacity: 0.1; text-align: center; padding-top: 50px;">
    <img src="ajax_loader.gif"/>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<!-- script src="js/swfobject.js" type="text/javascript"></script -->
<!-- script src="js/jquery.clippy.js" type="text/javascript"></script -->

<script type="text/javascript">
var lines_count_limit = <?php print(MAX_COUNT_TERMS); ?>;

function show_client_message(message){
    m = $("#message_box")
    m.html(message);
    m.fadeIn( 400 ).delay( 1800 ).slideUp( 300 );
}

function limit_lines_count()
{
    var text = $("#search_terms").val();
    var lines = text.split("\n");
    //alert(lines.length);
    if(lines.length > lines_count_limit)
    { 
        show_client_message("Not more "+lines_count_limit+" lines please");
        $("#search_terms").val(lines.slice(1,lines_count_limit).join("\n"));
    }
}


$(document).ready( function (){
    $("#message_box").hide();
    $("#search_terms").keyup(limit_lines_count);
    $("#search_terms").mouseup(limit_lines_count);
}
);

    //TODO: JS
function my_mouseOver(column_id) {
            var items=[], options="";
            //alert(column_id);
            //Iterate all td's in second column
            $('#results_container table tr td.td'+column_id+' a').each( function(i){
               //add item to array
               items[i] =  $(this).text();
            });
            //restrict array to unique items
            //items = $.unique( items );
            //items = items.reverse();    

            //iterate unique array and build array of select options
            $.each( items, function(i, item){
                options += item.replace(/\s/g, '') + "\r\n";
            });
            
            //alert('Text copied. Try to paste it now!');
            return options;
        }
        
//$(document).ready(function()
//		{
			/* Clippy location (hosted on Github) */
			//var clippy_swf = "js/clippy.swf";
            /* Set up the clippies! */
		//	$('#copy_column1').clippy({clippy_path: clippy_swf, text: my_mouseOver(1), width:100, height: 50});
			//$('#copy_column2').clippy({clippy_path: clippy_swf, text: my_mouseOver(2), width:100, height: 50});
			
	//	});
                
        <?php 
if(!array_key_exists("number_results", $_POST))
    print "start = 1;";
else 
    print "start = 0;";
        ?>

</script>

<div  id="header"  class="tr">
    <div id="page_title" class="td" >
    <h1><?php echo $_POST['page_name']; ?></h1>
    <h4><?php echo $_POST['page_description']; ?></h4>
    </div>
    <div class="td help"><a target="blank" href="http://www.ppv.im/help">HELP - make page?</a>
    </div>
</div>
<br />
<br />

<div align="center">
<form id="f1" name="Organic_Link_Scraper" action="" method="post">
<div class="table">
<div class="tr">
	<div class="td"><a target="blank" href="https://adwords.google.com/ko/KeywordPlanner/Home">Open Google Keyword Planner</a></div>
	<div class="td">
    <?php
    $a_site_to_scrape = array("Google.com"=>"Google.com ( USA )","Google.co.uk"=>"Google.co.uk ( UK )", "Google.ca"=>"Google.ca ( Canada )", "Google.ru" => "Google.ru ( Russia )",
        "Google.de" => "Google.de ( Germany )", "Google.fr" => "Google.fr ( France )", "Google.es" => "Google.es ( Spain )", "Google.it" => "Google.it ( Italy )", "Google.com.br" => "Google.com.br ( Brazil )", "Google.com.au" => "Google.com.au ( Australia )");
        
    if(array_key_exists("site_to_scrape", $_POST))
    {
        $site_to_scrape = $_POST['site_to_scrape'];
         
    }
    else
        $site_to_scrape = "Google.com";
     ?>
    	<select name="site_to_scrape">
        <?php 
        foreach ($a_site_to_scrape as $key=>$value) {
            $selected = ($site_to_scrape == $key? "selected=\"\"":"");
            print "<option $selected  value=\"$key\"> $value </option>\n";
        }
        ?>
    	</select>
	</div>
</div><br /> <br />
<div class="tr">
	<div class="td">
    <?php 
    if(array_key_exists("number_results", $_POST))
        $number_results = $_POST['number_results'];
    else
        $number_results = "10";

    ?>
		<input type="radio" name="number_results" value="10" <?php echo ($number_results=="10"?'checked="checked"':"")?> />
		<caption>10 Results per term</caption><br />
		<input type="radio" name="number_results" value="20" <?php echo ($number_results=="20"?'checked="checked"':"")?> />
		<caption>20 Results per term</caption><br />
		<input type="checkbox" value="checked" name="remove_duplicates" <?php
        //$_POST = $_GET;
        if(array_key_exists("number_results",$_POST) && !array_key_exists("remove_duplicates",$_POST))
            ;//print none;
        else
            echo 'checked="checked"';
        ?> />
		<caption>Remove duplicates</caption><br />
	</div>
    <div class="td">
        <textarea id="search_terms" name="search_terms" onclick=" if(start){ this.value = ''; start=0; }"><?php echo(array_key_exists("search_terms", $_POST)?$_POST['search_terms']:$_POST['page_terms_description']);?></textarea>
        <br />
        <div id="message_box">message_box</div>
        <br />
        <input id="Extract" type="submit" title="Extract" value="Extract" onclick="$('#page_shader').show();" />
        <a href="#"><button type="button" title="Clear" value="Clear" onclick='location.reload();'>Clear</button></a>
    </div>
    <div class="td">&nbsp;
    </div>
</div><br />
<div class="tr">
    
</div><br />
<div class="tr"></div>
</div><!-- /table -->

<br />
<br />
<br />

<?php 
if(array_key_exists("number_results", $_POST)):


 ?>
<table class="results" style="">
<caption> <h2></h2></caption>
<tr>
    <th class="th1" class="bordered">Url</th>
    <th class="th2 bordered">Domain</th>
    <th class="th3 bordered">Traffic and Demographics</th>
</tr>
</table>
<div id="results_container" style="" >
<table class="results">
<col width="50%"/>
  <col width="20%"/>
    <col width="30%"/>

<?php
function remove_protocol($href)
{
    return preg_replace("@https://|https://www\.|http://www\.|www\.|http://@im","",$href);
}

function Tlink($href="#", $class = "", $text="href", $id="random")
{
    $text = remove_protocol($text=="href"?$href:$text); //substr(remove_protocol($text=="href"?$href:$text), 0,100);
    //if(strlen($text)<strlen($href)) $text.= "...";
    $id = ($id=="random"?rand(111,999):$id);
    return "<a class='$class' target='blank'  href='$href'>$text</a>";
}

//$scraper->links - iterate to table
$i = 0;
if(isset($scraper))
{
foreach ($scraper->links as $link) {
    $color_class = ($i%2?"color1":"color2");  
    $i++;
?>
<tr class="<?php echo $color_class ?>">
    <td class="bordered td1"><input class="img_delete" type="image" src="delete.png" onclick='$(this).closest("tr").remove();' />
    <?php echo Tlink($link->url, "a1", trim($link->url,'/') )?>
    </td>
    <td class="bordered td2">
    <?php echo Tlink($link->domain, "a2");
    $link->domain = trim(remove_protocol($link->domain),'/');
    ?>
    </td>
    <td class="bordered td3"><a target='blank' href="http://www.alexa.com/siteinfo/<?php echo $link->domain?>">Alexa</a>| 
    <a target='blank' href="http://www.similarweb.com/website/<?php echo $link->domain?>">Similarweb</a> |
     <a target='blank' href="https://siteanalytics.compete.com/<?php echo $link->domain?>">Compete</a> |
      <a target='blank' href="https://www.quantcast.com/<?php echo $link->domain?>">Quantcast</a> |
       <a target='blank' href="http://www.semrush.com/info/<?php echo $link->domain?>">Semrush</a> |
       <a target='blank' href="http://www.keywordspy.com/research/search.aspx?q=<?php echo $link->domain?>">Keywordspy</a></td>
</tr>

<?php
}
}
 ?>
 <tr></tr>
<tr>
    <td></td>
    <td></td>
    <td></td>
</tr>
</table>

</div>

</form>

<form id="f2" name="download" action="ajax/save_txt_file.txt.php" method="post" >
    <input id="download_info" name="download_info" type="hidden" value="" />
</form>
    
<table style="text-align: left; width: 90%;">
<tr style="border: none; background-color: none;">
    <td id="td_copy_1" class="td1" style="">
        <button id="btn_copy1"  class="btn_copy" onmouseover="clip1_on()"> Copy </button>
            <input id="btn_download" class="btn_download" type="button" title="Download" value="Download" onclick='format_download_link(1)' />
        
    </td>
    <td id="td_copy_2" class="td2" style="">   
        <button id="btn_copy2" onmouseover="clip2_on();"  class="btn_copy"> Copy</button>
        <input class="btn_download" type="button" title="Download" onclick='format_download_link(2)' value="Download" />
    </td>
    
    <td></td>
</tr>
</table>

<script src="js/ZeroClip.js"></script>
<script>

//TODO: clipp
//data-clipboard-text="Copy me!" 
  var clip1 = new ZeroClipboard( 
  	document.getElementById('btn_copy1'), {
  	moviePath: "js/ZeroClipboard.swf"
    });
   
function clip1_on()
{
    clip1.setText( my_mouseOver(1) );
}  
    clip1_on();
    

  var clip2 = new ZeroClipboard( 
  	document.getElementById('btn_copy2'), {
  	moviePath: "js/ZeroClipboard.swf"
    });
function clip2_on()
{
    clip1.setText( my_mouseOver(2) );
}

    function format_download_link(column){
        var text = my_mouseOver(column);
        text = text.replace(/\r\n/g," ");
        
        var search_terms = $("#search_terms").text();
        //console.log("search_terms = "+search_terms);
        search_terms = search_terms.replace(/\n/,"_"); //TODO: add search_terms to download file_name    
        var file_name = "page_organic_links_"+search_terms; 
        var url = "ajax/save_txt_file.txt.php?file_name="+file_name;
        
       $("#f2").attr("action", url);
       $("#download_info").attr("value", text);
       $("#f2").submit();
       
    }

    $(".th1").width($(".td1").width()-20);
    $(".th2").width($(".td2").width());
    $("#td_copy_1").width($(".td1").width());
    
    
    
 /*    var clip = null;
 
   function $(id) { return document.getElementById(id); }
 
   function init()
   {
      clip = new ZeroClipboard.Client();
      clip.setHandCursor( true );
   }
 
   function move_swf(ee, column)
   {
      copything =  my_mouseOver(column);
      clip.setText(copything);
 
         if (clip.div)
         {   
            clip.receiveEvent('mouseout', null);
            clip.reposition(ee.id);
         }
         else{ clip.glue(ee.id);   }
  
         clip.receiveEvent('mouseover', null);
  
   }    
   */

</script>

<?php 
endif;
 ?>

<div class="table">
<div class="tr">
    
</div>
<div class="tr">
    
    
</div>
</div>

</div>
</body>
</html>