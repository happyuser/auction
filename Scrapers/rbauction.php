<?php
// Modified BY : Taras Prystavskyj
// Date : 09/14/2012

class Rbauction_ScraperController extends Auction_ScraperAbstractController
{
	protected $table_name = "rbauction";

	public function run()
	{
		ob_start();
		$starttime = microtime(true);

		$this->domain = $this->getDomain(); // IMPORTANT
		$did = $this->getDid(); // IMPORTANT

		$outputFilename = "logs/" . $did . "_" . $this->domain . "_" . time() . ".html"; // IMPORTANT
		$csv_out = fopen($outputFilename, "w");
		if ($csv_out === false)
			exit("Unable to open $outputFilename for writing\n");
		print ((php_sapi_name() == 'cli') ? "opened file: $outputFilename\n" :
			"opened file: <a href='/$outputFilename'>/$outputFilename</a>");

		$keywords =  $this->get_keywords();

		foreach ($keywords as $keyword) {
			$this->scrape_keyword($keyword);
		}

		$runtime = microtime(true) - $starttime;
		echo "<br />",sprintf("executed on server in %s seconds<br>", $runtime);

		fwrite($csv_out, ob_get_flush());
	}

	public function scrape_keyword($keyword)
	{
		$per_page = 20;
		$page_limit=100;


		$search_params = array(
			"indexName" => "ci", "locale" => "en", "txtSelectKeywords" => "", "dropSelectIndustry" => "", "dropSelectCategory" => "", "dropSelectMasterCategory" => "", "dropSelectRegion" => "", "dropSelectAuction" => "", "txtSelectMake" => "", "txtSelectModel" => "", "makeDimId" => "", "modelDimId" => "", "txtSelectLotStart" => "", "txtSelectLotEnd" => "", "txtSerialNo" => "", "dropSelectPriceStart" => "", "txtSelectPriceStart" => "", "txtSelectPriceEnd" => "", "dropSelectDate" => "", "txtSelectManuYearStart" => "", "txtSelectManuYearEnd" => ""
		);
		//search by keyword - txtSelectKeywords, search by model - txtSelectModel
		$search_params['txtSelectKeywords'] = $keyword;
		//$search_params['txtSelectModel'] = $keyword;
		$link = "https://www.rbauction.com/rba-api/rbasq/get?"; // // advanced way to get rbasq

		if ($this->debug_mode)
			echo "<div>txtSelectKeywords:'", htmlspecialchars($search_params['txtSelectKeywords']), "', txtSelectModel:'", htmlspecialchars($search_params['txtSelectModel']),"'</div>";

		$search_page = $this->load_search_page($link.http_build_query($search_params));
		$data = json_decode($search_page, true);
		$rbasq = $data['rbasq'];
		if (!$rbasq)
			throw new Exception("Unable to get RBASQ");

		$summ_count = 0;
		$page = 0;
		do {
			$start = $page*$per_page;
			$this->keyword = $keyword;
			$category_link = "https://www.rbauction.com/rba-api/search/results/advanced?rbasq=" .$rbasq. "&offset=$start&count=$per_page&ccb=USD";
			echo "<div>Page: ", $page+1, "</div>";
			echo "<div>url: ", htmlspecialchars($category_link), "</div>";
			echo "<br />";
			$search_page = $this->load_search_page($category_link);
			if (!$search_page)
				throw new Exception("no data retrieved from server [q:$keyword, page:$page]");

			$data = json_decode($search_page, true);

			if (!$data)
				throw new Exception("no data after json_decode [q:$keyword, page:$page]");

			$summ_count += count($data['results']);
			$num_total = (int)$data['total'];
			if ($page==0)
				echo "<div>Total ", $num_total," results found</div>";

			$this->analise_data($data);

			$page++;
		} while(($start+$per_page<$num_total) && $page < $page_limit);

		$this->save_scraping_log();

		echo "<br />total for '$keyword': $summ_count lots<br />";
	}

	private function analise_data($data)
	{
		$a_lots = $data['results']; //вся інформація в json.

		if ($this->debug_mode && $a_lots)
			echo "<table><tr><th>id</th><th>title</th><th>link</th></tr>";

		$need_init = $this->if_need_init_keyword($this->table_name, $this->keyword);
		//echo "need_init:", $need_init? 1 : 0,"<br />";
		foreach ($a_lots as $key => $lot) {
			$id = $lot['id'];
			$lot['link'] = "https://www.rbauction.com".$lot['url'];
			$lot['title'] = $lot['name'];

			if ($this->debug_mode)
				echo "<tr><td>{$lot['id']}</td><td>{$lot['title']}</td><td><a href='{$lot['link']}'>", htmlspecialchars($lot['link']), "</a></td></tr>";

			$this->save_auction_record($this->table_name, $id, json_encode($lot), $need_init);
		}
		if ($this->debug_mode && $a_lots)
			echo "</table>";

		print "<br /> lots count:" . count($a_lots);
		print "<br /> Done scraping $this->domain for key: $this->keyword";
	}
}
