<?php
// Modified BY : Taras Prystavskyj
// Date : 09/14/2012
class Equipmentone_ScraperController extends Auction_ScraperAbstractController
{
	protected $table_name = "equipmentone";

    public function run()
    {
        ob_start();
        $starttime = microtime(true);

        $this->domain = $domain = $this->getDomain(); // IMPORTANT
        $did = $this->getDid(); // IMPORTANT

        $outputFilename = "logs/" . $did . "_" . time() . ".html"; // IMPORTANT

        $csv_out = fopen($outputFilename, "w");
        if ($csv_out === false)
            exit("Unable to open $outputFilename for writing\n");
        print ((php_sapi_name() == 'cli') ? "opened file: $outputFilename\n" :
            "opened file: <a href='$outputFilename'>$outputFilename</a>");

        $keywords =  $this->get_keywords();

        foreach ($keywords as $keyword) {
			$this->scrape_keyword($keyword);
        }

		$endtime = microtime(true);
		$runtime = $endtime - $starttime;
		echo "<br />",sprintf("executed on server in %s seconds<br>", $runtime);
		fwrite($csv_out, ob_get_flush());
    }

    public function scrape_keyword($keyword)
	{
		$summ_count = 0;
		$per_page = 20;
		$page_limit=50;
		$page = 0;
		do {
			$start = $page*$per_page;
			$this->keyword = $keyword;
			$javascript_time = $this->debug_mode? "1473747737618" : time();
			$callback = "jQuery18308619988231730569_" . $javascript_time;
			$category_link = "https://search.equipmentone.com/e1_search?json.wrf=$callback&wt=json&q=$keyword&fq={!tag=si}marketplace:(%22EquipmentOne%22)&fq=auctionEndDateTimeUTC:([NOW%20TO%20*])&sort=siteSort%20asc,%20score%20desc&rows=$per_page&start=$start&_=" . $javascript_time;
			echo "<div>Page: ", $page+1, "</div>";
			echo "<div>url: ", htmlspecialchars($category_link), "</div>";
			$search_page = $this->load_search_page($category_link);
			if (!$search_page)
				throw new Exception("no data retrieved from server [q:$keyword, page:$page]");

			$search_page = substr($search_page, strlen($callback) + 1, -2); // [jQuery18308619988231730569_....(] - [);]
			$data = json_decode($search_page, true);

			if (!$data)
				throw new Exception("no data after json_decode [q:$keyword, page:$page]");
			if ($data['responseHeader']['status']!=0) {
				$msg = '';
				if ($data['error'] && $data['error']['msg'])
					$msg = "(".$data['error']['msg'].")";
				throw new Exception("['responseHeader']['status']!=0 $msg [status:" . $data['responseHeader']['status'] . ",q:$keyword, page:$page]");
			}


			$summ_count += count($data['response']['docs']);
			$num_total = (int)$data['response']['numFound'];
			$this->analise_data($data);

			$page++;
		} while(($start+$per_page<$num_total) && $page < $page_limit);

		echo "<br />".$keyword.": ".$summ_count." lots<br />";

		$this->save_scraping_log();
	}

	private function analise_data($data)
    {
        $a_lots = $data['response']['docs']; //��� ���������� � json.

		if ($this->debug_mode && $a_lots)
			echo "<table><tr><th>id</th><th>title</th><th>link</th></tr>";

        $need_init = $this->if_need_init_keyword($this->table_name, $this->keyword);
		//echo "need_init:", $need_init? 1 : 0,"<br />";
        foreach ($a_lots as $key => $lot) {
            $id = $lot['listingID'];
			$lot['id'] = $id;
			$lot['link'] = "https://www.equipmentone.com/listing?listingid=".$lot['listingID'];

			if ($this->debug_mode)
				echo "<tr><td>{$lot['id']}</td><td>{$lot['title']}</td><td><a href='{$lot['link']}'>", htmlspecialchars($lot['link']), "</a></td></tr>";

            $this->save_auction_record($this->table_name, $id, json_encode($lot), $need_init);
        }
		if ($this->debug_mode && $a_lots)
			echo "</table>";
        print "<br /> lots count:" . count($a_lots);
        print "<br /> Done scraping $this->domain for key: $this->keyword";
    }
}
