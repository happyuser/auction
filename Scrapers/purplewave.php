<?php
// Modified BY : Taras Prystavskyj
// Date : 09/14/2012

class Purplewave_ScraperController extends Auction_ScraperAbstractController {
	protected $table_name = "purplewave";

	public function run()
	{
		ob_start();
		$starttime = microtime(true);

		$this->domain = $domain = $this->getDomain(); // IMPORTANT
		$did = $this->getDid(); // IMPORTANT

		$outputFilename = "logs/" . $did . "_" . time() . ".html"; // IMPORTANT

		$csv_out = fopen($outputFilename, "w");
		if ($csv_out === false)
			exit("Unable to open $outputFilename for writing\n");
		print ((php_sapi_name() == 'cli') ? "opened file: $outputFilename\n" :
			"opened file: <a href='$outputFilename'>$outputFilename</a>");

		$keywords =  $this->get_keywords();

		foreach ($keywords as $search_key) {
			$this->scrape_keyword($search_key);
		}

		$endtime = microtime(true);
		$runtime = $endtime - $starttime;
		printf("Executed on server in %s seconds", $runtime);
		fwrite($csv_out, ob_get_flush());
	}

	public function scrape_keyword($keyword)
	{
		echo "<hr><h2>", $keyword, "</h2><br />";
		$this->keyword = $keyword;
		$category_link = "https://www.purplewave.com/v1/search/search?searchType=all&dateType=upcoming&dateRanges=&auctions=&zipcode=&zipcodeRange=all&startPrice=0&endPrice=0&filters=&showDefault=true&sortBy=current_bid-desc&page=1&perPage=50&grouped=false&viewtype=compressed&=undefined"
			."&searchTerm=$keyword&timestamp=0";
		echo "<div>Page: ", 1, "</div>";
		echo "<div>url: ", htmlspecialchars($category_link), "</div>";
		echo "<br />";
		$search_page = $this->load_search_page($category_link);
		$data = json_decode($search_page, true);
		$this->analise_data($data);

		// check pagination
		$jquery_callback = "jQuery1110039855087762422303_1473413294894";
		$pagination_link = "https://www.purplewave.com/v1/search/pagination?callback=$jquery_callback&=undefined&searchTerm=$keyword&timestamp=0&_=1473413294895";
		$pagination_page = $this->load_search_page($pagination_link);
		$pagination_page = substr($pagination_page, strlen($jquery_callback)+1, -2);
		$pagination_data = json_decode($pagination_page, true);
		if ($pagination_data['pages'])
		{
			// load pages 2+
			for ($i=1;$i<count($pagination_data['pages']);$i++)
			{
				$page = $pagination_data['pages'][$i];
				$category_link = "https://www.purplewave.com/v1/search/search?searchType=all&dateType=upcoming&dateRanges=&auctions=&zipcode=&zipcodeRange=all&startPrice=0&endPrice=0&filters=&showDefault=true&sortBy=current_bid-desc"
					."&page=$page&perPage=50&grouped=false&viewtype=compressed&=undefined"
					."&searchTerm=$keyword&timestamp=0";
				echo "<div>Page: ", $page, "</div>";
				echo "<div>url: ", htmlspecialchars($category_link), "</div>";
				echo "<br />";
				$search_page = $this->load_search_page($category_link);
				$data = json_decode($search_page, true);
				$this->analise_data($data);
			}
		}

		$this->save_scraping_log();
	}

	private function analise_data($data)
	{
		$a_lots = $data; //��� ���������� � json.

		if ($this->debug_mode && $a_lots)
			echo "<table><tr><th>id</th><th>title</th><th>link</th></tr>";

		$need_init = $this->if_need_init_keyword($this->table_name, $this->keyword);
		//echo "need_init:", $need_init? 1 : 0,"<br />";
		foreach ($a_lots as $key => $lot) {
			$id = $lot['id'];
			$lot['title'] = $lot['first_line_description'];
			$lot['link'] = "https://www.purplewave.com/auction/{$lot['auction']}/item/{$lot['item']}";

			if ($this->debug_mode)
				echo "<tr><td>{$lot['id']}</td><td>{$lot['title']}</td><td><a href='{$lot['link']}'>", htmlspecialchars($lot['link']), "</a></td></tr>";

			$this->save_auction_record($this->table_name, $id, json_encode($lot), $need_init);
		}
		if ($this->debug_mode && $a_lots)
			echo "</table>";
		print "<br /> lots count:" . count($a_lots);
		print "<br /> Done scraping $this->domain for key: $this->keyword";
	}
}
