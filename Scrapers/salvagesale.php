<?php
// Modified BY : Taras Prystavskyj
// Date : 09/14/2012

/*
 *CREATE TABLE `salvagesale` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `item_id` INT(11) NOT NULL,
  `json` TEXT COLLATE armscii8_bin NOT NULL,
  `date_scraped` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `need_notify_user` TINYINT(1) NOT NULL DEFAULT '0',
  `search_string_id` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin
 */

class Salvagesale_ScraperController extends Auction_ScraperAbstractController {
	protected $table_name = "salvagesale";

	public function run()
	{
		ob_start();
		$starttime = microtime(true);

		$this->domain = $domain = $this->getDomain(); // IMPORTANT
		$did = $this->getDid(); // IMPORTANT

		$outputFilename = "logs/" . $did . "_" . time() . ".html"; // IMPORTANT

		$csv_out = fopen($outputFilename, "w");
		if ($csv_out === false)
			exit("Unable to open $outputFilename for writing\n");
		print ((php_sapi_name() == 'cli') ? "opened file: $outputFilename\n" :
			"opened file: <a href='$outputFilename'>$outputFilename</a>");

		$keywords =  $this->get_keywords();

		foreach ($keywords as $keyword) {
			$this->scrape_keyword($keyword);
		}

		$runtime = microtime(true) - $starttime;
		echo sprintf("executed on server in %s seconds<br>", $runtime);
		fwrite($csv_out, ob_get_flush());
	}

	public function scrape_keyword($keyword)
	{
		$this->keyword = $keyword;
		echo "<hr><h2>", $keyword, "</h2><br />";

		$items_per_page = 10;
		$pages_limit = 200;
		$page = 1;
		do {
			$category_link = "http://www.salvagesale.com/General/AdvanceSearch/mc/1/l_searchterm/$keyword/l_venue/-1/l_category/-1/l_location/-1/maxrows/$items_per_page/page/$page";
			echo "<div>Page: ", $page, "</div>";
			echo "<div>url: ", $category_link, "</div>";

			$search_page = $this->load_search_page($category_link);

			$data = $this->parse_page($search_page);
//			$debug_cached_filename = "debug_cache/" . $this->domain . "_" . md5($category_link) . "_data.php";
//			file_put_contents($debug_cached_filename, "<?php \$data=" . var_export($data, true));

			echo "Received lots: ", count($data), "<br />";
			$this->analise_data($data);

			$page++;
		} while ($data && $page<$pages_limit);

		$this->save_scraping_log();
	}

	private function parse_page($html_page)
	{
		$dom = str_get_html($html_page);

		$data = array();
		foreach ($dom->find("#lots_paneled .tile") as $item)
		{
			/* @var $item simple_html_dom_node */
			$lot = array();
			$link = $item->find("ul h2 a", 0);
			$lot['id'] = substr($item->find(".lot_panel", 0)->getAttribute("id"), 9);
			$lot['title'] = trim($link->plaintext);
			$lot['link'] = $link->href;
			$lot['description'] = trim($item->find("ul", 0)->plaintext);

			if (!$lot['id'])
				throw new Exception("lot id not found");

			$data[] = $lot;
		}
		return $data;
	}

	private function analise_data(&$data)
	{
		$a_lots = $data; //��� ���������� � json.

		if ($this->debug_mode && $a_lots)
			echo "<table><tr><th>id</th><th>title</th><th>link</th></tr>";

		$need_init = $this->if_need_init_keyword($this->table_name, $this->keyword);
		//echo "need_init:", $need_init? 1 : 0,"<br />";
		foreach ($a_lots as $key => $lot) {
			$id = $lot['id'];

			if ($this->debug_mode)
				echo "<tr><td>{$lot['id']}</td><td>{$lot['title']}</td><td><a href='{$lot['link']}'>", htmlspecialchars($lot['link']), "</a></td></tr>";

			$this->save_auction_record($this->table_name, $id, json_encode($lot), $need_init);
		}
		if ($this->debug_mode && $a_lots)
			echo "</table>";
		print "<br /> lots count:" . count($a_lots);
		print "<br /> Done scraping $this->domain (table:$this->table_name) for key: $this->keyword";
	}
}

