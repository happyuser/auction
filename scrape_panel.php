<?php
// для визуального тестирования скрейперов, сравнить что на сайте и что скрейпер получает

error_reporting(E_ALL);
ini_set('display_errors', 'On');

define('APPLICATION_ENV', 'development');

require "ScraperAbstractController.php";
require "BaseScraperController.php";
require "include/db_connect.php";
require "simple_html_dom.php";


$keyword = "";
$current_scraper = false;
$scrapers = array();
$q = "SELECT * FROM `auction_site` WHERE `php_or_java_script` = 'PHP' and do_scrape = 1";
$res = mysqli_query($mysqli,$q);
while($r = mysqli_fetch_assoc($res)) {
	if (isset($_GET['s']) && $_GET['s']==$r['name'])
		$current_scraper = $r;
	$scrapers[] = $r;
}
if (empty($_GET['s']) && !isset($_GET['keywords']))
	$current_scraper = $scrapers[0];
if (!empty($_GET['q']))
	$keyword = $_GET['q'];

if (isset($_GET['clear_cache']))
{
	$files = glob("./debug_cache/".$current_scraper['url']."_*.html");
	if ($files)
	{
		foreach ($files as $file)
			unlink($file);
	}
	header("Location: scrape_panel.php?s=".$current_scraper['name'].($keyword? "&q=".$keyword : ""), true, 302);
	exit;
}

if (isset($_GET['keywords']))
{
	if ($_GET['keywords']=='list')
	{
		$q = "Select * from `search_string`";
		$res = mysqli_query($mysqli, $q);

		echo "<ul class='keywords-list'>";
		while ($r = mysqli_fetch_assoc($res)) {
			echo "<li data-id='{$r['id']}'>";
			echo "<span class='name'>{$r['search_string']}</span>";
			echo "<span class='delete' title='Delete'>&times;</span>";
			echo "</li>";
		}
		echo "</ul>";
		exit;
	}
	if ($_GET['keywords']=='save' && $_GET['id'] && $_GET['name'])
	{
		$new_name = mysqli_real_escape_string($mysqli, $_GET['name']);
		$id = (int) $_GET['id'];
		$q = "UPDATE `search_string` SET `search_string`='$new_name' WHERE id='$id'";
		mysqli_query($mysqli, $q);
		echo mysqli_affected_rows($mysqli);
		//header("Location: scrape_panel.php?keywords", true, 302);
		exit;
	}
	if ($_GET['keywords']=='delete' && $_GET['id'])
	{
		$id = (int) $_GET['id'];
		$q = "DELETE FROM `search_string` WHERE id='$id'";
		mysqli_query($mysqli, $q);
		echo mysqli_affected_rows($mysqli);
		//header("Location: scrape_panel.php?keywords", true, 302);
		exit;
	}
	if ($_GET['keywords']=='add' && $_GET['name'])
	{
		$new_name = mysqli_real_escape_string($mysqli, $_GET['name']);
		$q = "INSERT INTO `search_string` SET `search_string`='$new_name'";
		mysqli_query($mysqli, $q);
		echo mysqli_affected_rows($mysqli);
		//header("Location: scrape_panel.php?keywords", true, 302);
		exit;
	}
}


?><!DOCTYPE html>
<html><head>
	<meta charset='utf-8' />
	<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<style type="text/css">
		ul.scrapers-list {
			display: inline-block;
			margin-bottom: 10px;
			margin-right: 40px;
			padding: 0;
		}
		ul.scrapers-list li {
			display: inline;
			padding: 0 5px 15px;
		}
		ul.scrapers-list li.active {
			background-color: #eeeeee;
			border-top-left-radius: 5px;
			border-top-right-radius: 5px;
		}
		.keywords-link {
			padding: 0 5px 15px;
			margin: 2px;
		}
		.keywords-link.active {
			background-color: #eeeeee;
			border-top-left-radius: 5px;
			border-top-right-radius: 5px;
		}
		.search-panel {
			background-color: #eee;
			padding: 5px;
			margin-bottom: 5px;
			-webkit-border-radius:5px;
			-moz-border-radius:5px;
			border-radius:5px;
		}
		.search-panel h2 {
			margin-top:0;
		}

		ul.keywords-list {
			list-style: decimal;
		}
		ul.keywords-list li {
		}
		ul.keywords-list li .name {
			text-decoration: underline;
			cursor: pointer;
		}
		ul.keywords-list li .delete {
			margin-left: 10px;
			cursor: pointer;
			font-weight: bold;
			color: red;
		}
	</style>
</head><body><?
echo "<ul class='scrapers-list'>";
foreach ($scrapers as $r) {
	$active = $r['name']==$current_scraper['name']? "class='active'" : "";
	echo "<li $active><a href='scrape_panel.php?s={$r['name']}'>{$r['name']}</a></li>";
}
echo "</ul>";

$active = isset($_GET['keywords'])? "active" : "";
echo "<span class='keywords-link $active'><a href='scrape_panel.php?keywords'>Manage keywords</a></span>";

if (isset($_GET['keywords']))
{
	echo "<div class='search-panel'>";
	echo "<h2>Keywords</h2>";
	echo "<div class='keywords-list-container'></div>";
	echo "<input class='keyword-add' type='button' value='Add' />";
	echo "</div>";
}

if ($current_scraper)
{
	echo "<div class='search-panel'>";
	echo "<h2>{$current_scraper['name']}</h2>";
	echo "<div>
<a target='_blank' href='http://{$current_scraper['url']}'>{$current_scraper['url']}</a>
<form id='search' action='scrape_panel.php' method='get'>
<input type='hidden' name='s' value='{$current_scraper['name']}' />
Search: <input type='text' name='q' value='$keyword' /><input type='submit' />
</form>";
	$q = "Select * from `search_string`";
	$res = mysqli_query($mysqli, $q);

	echo "<select id='search-keywords'>";
	while ($r = mysqli_fetch_assoc($res)) {
		$selected = (isset($_GET['q']) && $r['search_string']==$_GET['q'])? "selected" : "";
		echo "<option $selected>{$r['search_string']}</option>";
	}
	echo "</select>";
	echo "<input id='search-by-keyword' type='button' value='Search by keyword' />";

	echo "</div>";
	echo "</div>"; // panel div

	$files = glob("./debug_cache/".$current_scraper['url']."_*.html");
	if ($files)
	{
		echo "<div>Cached files:", count($files)," <a href='scrape_panel.php?s=", $current_scraper['name'], ($keyword? "&q=".$keyword : ""),"&clear_cache'>delete</a></div>";
	}

	if ($keyword) {
		echo "Scraper output:";
		echo "<div id='scraper-output'>";

		$did = 1234;
		$currentScraper = $current_scraper['name'] . ".php";
		require __DIR__ . "/Scrapers/$currentScraper";
		$scraperClassName = ucfirst($current_scraper['name']) . "_ScraperController";
		$scraper = new $scraperClassName($current_scraper['url'], $did);
		/* @var $scraper Auction_ScraperAbstractController */
		$scraper->debug_mode = true;

		try {
			$scraper->scrape_keyword($keyword);
		} catch (Exception $e) {
			on_critical_error($e->getMessage());
			print "exit line: " . __LINE__ . "\n";
		}
		echo "</div>";
	}
}

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script>
	(function() {
		$("#search-by-keyword").click(function(){
			var q = $("#search-keywords").val();
			$("#search input[name=q]").val(q);
			$("#search").trigger("submit");
		});


		var reload_keywords_list = function(){
			$.ajax({url:"scrape_panel.php", data:{keywords:"list"}, success: function(html){
				$(".keywords-list-container").html(html);
				rebind_keywords_list();
			}});
		};
		var rebind_keywords_list = function() {

			function delete_keyword_click(){
				var id = $(this).closest("li").data('id');
				var name = $(this).html();
				var new_name = window.prompt("Rename", name);
				if (new_name && name != new_name)
				{
					$.ajax({url:"scrape_panel.php", data:{keywords:"save", id:id, name:new_name}, success: function(html){
						reload_keywords_list();
					}});
				}
			}
			$(".keywords-list .name").bind("click", function() {
				var id = $(this).closest("li").data('id');
				var name = $(this).html();
				var new_name = window.prompt("Rename", name);
				if (new_name && name != new_name)
				{
					$.ajax({url:"scrape_panel.php", data:{keywords:"save", id:id, name:new_name}, success: function(html){
						reload_keywords_list();
					}});
				}
			});
			$(".keywords-list .delete").bind("click", function() {
				var id = $(this).closest("li").data('id');
				var name = $(this).closest("li").find('.name').html();
				if (window.confirm("Delete keyword '" +name+ "'"))
				{
					$.ajax({url:"scrape_panel.php", data:{keywords:"delete", id:id}, success: function(html){
						reload_keywords_list();
					}});
				}
			});
		};

		$(".keyword-add").bind("click", function() {
			var new_name = window.prompt("New keyword");
			if (new_name)
			{
				$.ajax({url:"scrape_panel.php", data:{keywords:"add", name:new_name}, success: function(html){
					reload_keywords_list();
				}});
			}
		});


		reload_keywords_list();
	})();
</script>
</body></html>