<?php
abstract class BaseScraperController
{
    private $mysqli;

  	/**
	 * proxy index in array
	 *
	 * @var integer
	 */
	private $current_proxy_index = 0;

    /**
	 * array of proxies in format: 192.40.94.210:39275:vs18us37:8dcn6sur
	 *
	 * @var array
	 */
	private $a_proxies = array();
    
    /**
	 * array of proxy indexes in the file, starting of 0, if they banned
	 *
	 * @var array
	 */
	private $a_proxies_banned = array();
        
	/**
	 * array of proxies in format: 192.40.94.210:39275:vs18us37:8dcn6sur
	 *
	 * @var string
	 */
	private $current_proxy;
    
    private $a_proxy;
    private $proxy_array = array();
    
    private $proxy_ip;
    
    private $proxy_login;
    
	/**
	 * columns of the data we're scraping
	 *
	 * @var array
	 */
	private $csvColumns = array(
		'DID',
		'VIN',
		'MODELCODE',
		'STOCK',
		'NEWORUSED',
		'CERTIFIED',
		'YEAR',
		'MAKE',
		'MODEL',
		'TRIM',
		'TITLE',
		'BODYTYPE',
		'DRIVE',
		'MILEAGE',
		'PRICE',
		'EXTCOLOR',
		'INTCOLOR',
		'ENGINE',
		'TRANSMISSION',
		'IMAGE',
		'URL',
		'EBAYITEM',
		'PHOTOCOUNT',
		'OPTIONCODES');

	/**
	 * domain to be scraped
	 *
	 * @var string
	 */
	protected $domain;
	
	/**
	 * dealer id
	 *
	 * @var int
	 */
	protected $did;
    
	/**
	 * file handler for output
	 *
	 * @var resource
	 */
	private $csvOutput;
	
	/**
	 * file handler for tmp file
	 *
	 * @var resource
	 */
	private $tmpOutput;
	
	/**
	 * car makes
	 *
	 * @var array
	 */
	
	protected $cookieRequests = false;
	
	/**
	 * cookie data sent with post requests
	 *
	 * @var string
	 */
	protected $cookieData;
	
	/* abstract methods that child scrapers should override */
	abstract protected function getInventoryPath($dom);
	abstract protected function getInventoryPageCount($dom);
	abstract protected function getInventoryPageUrl($dom, $pageNo);
	abstract protected function getInventoryPageVehicles($dom);
	
	abstract protected function getVehicleData($dom, $deep = false);
	abstract protected function replaceBadDomains();
	
	abstract protected function getVin($dom, $deep = false);
	abstract protected function getModelCode($dom, $deep = false);
	abstract protected function getStock($dom, $deep = false);
	abstract protected function getNewOrUsed($dom, $deep = false);
	abstract protected function getCertified($dom, $deep = false);
	abstract protected function getYear($dom, $deep = false);
	abstract protected function getMake($dom, $deep = false);
	abstract protected function getModel($dom, $deep = false);
	abstract protected function getTrim($dom, $deep = false);
	abstract protected function getTitle($dom, $deep = false);
	abstract protected function getBodyType($dom, $deep = false);
	abstract protected function getDrive($dom, $deep = false);
	abstract protected function getMileage($dom, $deep = false);
	abstract protected function getPrice($dom, $deep = false);
	abstract protected function getExtColor($dom, $deep = false);
	abstract protected function getIntColor($dom, $deep = false);
	abstract protected function getEngine($dom, $deep = false);
	abstract protected function getTransmission($dom, $deep = false);
	abstract protected function getImage($dom, $deep = false);
	abstract protected function getUrl($dom, $deep = false, $url = null);
	abstract protected function getEbayItem($dom, $deep = false);
	abstract protected function getPhotoCount($dom, $deep = false);
	abstract protected function getOptionCodes($dom, $deep = false);
    
	/**
	 * constructs the class
	 *
	 * @param string $domain
	 * @param int $did
	 * @return void
	 */
	public function __construct($domain, $did)
	{
		// $did has to be a number
		if(!is_numeric($did))
		{
			throw new InvalidArgumentException();
		}
		
		// set vars
		$this->domain = $domain;
		$this->did = $did;
		
		// error codes
		define('ERROR_PATH', 1);
		define('ERROR_PAGECOUNT', 2);
		define('ERROR_VEHICLEURL', 3);
		define('ERROR_NODATA', 4);
		define('ERROR_NODOMS', 5);
	}
	
	/**
	 * runs the scraper
	 *
	 * @return void
	 */
	public function run()
	{
		// load tmp data
		$this->loadTmpData();
	
		// open a data stream to the output csv file
		$this->initializeOutputStreams();
		
		// load make/model csv data into memory
		$this->loadMakeModelCsvData();
		
		// process the used vehicle inventory listings page
		if(!t())$this->processUsedInventoryPage(); //TODO: uncoment
		
		// if there's any vehicle urls to dig deeper into, do so
		$this->processVehicleUrls();
		
		// we should have all the car data - write to csv file
		$this->writeOutput();
	}
	
	/**
	 * writes output to csv file
	 *
	 * @return void
	 */
	protected function writeOutput()
	{
		$csv = '';
        
        if(t())
        {
            print "<textarea style='widht: 300px; height: 300px;'>";
            print "Got data:<br/>";
            print_r($this->data);
            print "</textarea>";
            print "exit line ".__LINE__; exit;
        }
                
		
		if(count($this->data) > 0)
		{
			foreach($this->data as $row)
			{
				foreach($this->csvColumns as $column)
				{
					$column = strtolower($column);
					if(strpos($row[$column], ',') !== false)
					{
						$row[$column] = str_replace(',', '', $row[$column]);
					}
					$csv .= $row[$column] . ',';
				}
				$csv .= "\n";
			}
			
			fwrite($this->csvOutput, $csv);
			
			echo "Success! " . count($this->data) . " vehicles scraped.";
			echo "\r\nBad VINs: " . $this->badVins . "\r\nBad Prices: " . $this->badPrices . "\r\nBad Mileages: " . $this->badMileages . "\r\nBad URLs: " . $this->badUrls;
		}
		else
		{
			// no data found - output error
			$this->throwError(ERROR_NODATA);
		}
	
		// close streams
		
        if(fclose($this->csvOutput)) 
                if(t())
                    print "<h3><font color='red'>Ready csv file:</font></h3><a href='".$this->getOutputFilename()."'>".$this->getOutputFilename()."</a>";
        
        
		fclose($this->tmpOutput);
	}

	/**
	 * echoes an error based on a given code and terminates
	 *
	 * @param int error code
	 * @return void
	 */
	private function throwError($code)
	{
		switch($code)
		{
			case ERROR_PATH:
				echo "ERROR: Couldn't find path!";
				die();
				
			case ERROR_PAGECOUNT:
				echo "ERROR: Couldn't get page count!";
				die();
				
			case ERROR_NODATA:
				echo "ERROR: No data was scraped.\r\nBad VINs: " . $this->badVins . "\r\nBad Prices: " . $this->badPrices . "\r\nBad Mileages: " . $this->badMileages . "\r\nBad URLs: " . $this->badUrls;
				die();
				
			case ERROR_NODOMS:
				echo "ERROR: Couldn't get vehicle DOMs from inventory page!";
				die();
				
			default:
				echo "ERROR: Invalid error code!";
				die();
		}
	}
	
	/**
	 * initializes the data stream
	 *
	 * @return void
	 */
	protected function initializeOutputStreams()
	{
		$fh = fopen($this->getOutputFilename(), 'w');
		
		// write column headers to file
		$csvColumnsHeader = implode(',', $this->csvColumns) . "\n";
		fwrite($fh, $csvColumnsHeader);
		
		$this->csvOutput = $fh;
		
		$fh = fopen("tmp/" . $this->did . ".tmp", 'w');
		$this->tmpOutput = $fh;
		
		// write any existing data into the tmp file
		foreach($this->tmpData as $stock => $tmpData)
		{
			fwrite($this->tmpOutput, $stock . '-' . $tmpData['vin'] . '-' . $tmpData['price'] . '-' . $tmpData['mileage'] . ',');
		}
	}
	
	/**
	 * loads make and model data from csv files into memory
	 *
	 * @return void
	 */
	private function loadMakeModelCsvData()
	{
		$this->carMakes = $this->loadCsvIntoArray('makes.csv');
		$this->carModels = $this->loadCsvIntoArray('models.csv');
		
		$this->carMakes = array_reverse($this->carMakes);
		$this->carModels = array_reverse($this->carModels);
	}
	

	/**
	 * returns the output filename
	 *
	 * @return string
	 */
	private function getOutputFilename()
	{
		return "Feeds/" . $this->did . ".csv";
	}
	
	/**
	 * initializes a new curl session and returns a handle
	 *
	 * @param string $url (optional)
	 * @return resource (success), false (failure)
	*/
	private function getCurlHandle($url = null)
	{
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (compatible; Autobot/1.0");
		curl_setopt($ch, CURLOPT_HEADER, 0);
		
		// check if we're sending post data
		if($this->cookieRequests)
		{
			curl_setopt($ch, CURLOPT_COOKIE, $this->cookieData);
		}
		
		return $ch;
	}
	

	/**
	* cleans up a url
	*
	* @return string
	*/
	protected function cleanUrl($url)
	{
		if($url[0] != '/')
		{
			$url = '/' . $url;
		}
		
		return $url;
	}
	
	/**
	 * cleans a multi-word string
	 *
	 * @param string $str
	 * @return string
	 */
	protected function cleanMultiWordString($str)
	{
		// clean up result
		$pos = stripos($str, '  ');
		return trim(substr($str, 0, $pos));
	}
    
    
}