pjs.addSuite({
    // single URL or array
    url: 'https://www.equipmentone.com/search?kw=man',
    // single function or array, evaluated in the client
    scraper: function() {
        return $('#e1ResultsListContainer').text();
    }
});