
        pjs.config({ 
    // options: 'stdout', 'file' (set in config.logFile) or 'none'
    log: 'stdout',
    // options: 'json' or 'csv'
    format: 'json',
    // options: 'stdout' or 'file' (set in config.outFile)
    writer: 'file',
    outFile: 'pjscrape_out_1473440388.json'
});

        pjs.addSuite({
    // single URL or array
    url: 'https://www.equipmentone.com/search?kw=man',
    // single function or array, evaluated in the client
    scraper: function() {
        return $('#e1ResultsListContainer').text();
    }
});