 <html>
 <head><meta charset="utf-8"/></head>
 <body>
 <?php
 
 
function print_in_textarea($string, $width=600, $height=400)
{
    print "<textarea style='height:$height\px; width:$width\px;'>";
    print_r($string);
    print "</textarea>";
}


 class ScraperController {
 function pullXmlObjBlogExample($siteUrl,$cssSelector){

        //create configuration object containing jquery selector and target site url to pass to the phantom script

        $config = array(
            "selector"=>$cssSelector,
            "url"=>$siteUrl
        );

        //read in the base phantom script and create a copy of it so we don't mess with the original base script

        $templateScript = "phantomJsBlogExample.js";
        $templateFileCopy = "phantomJs-copy-".time().".js";

        if (!copy($templateScript, $templateFileCopy)) {
            echo "failed to copy $templateFileCopy";
            return false;
        }

        //Prepend configuration object onto script

        //$configObj = file_get_contents($templateFileCopy);
        $configObj = "pjs.addSuite({
    // single URL or array
    url: '$siteUrl',
    // single function or array, evaluated in the client
    scraper: function() {
        return $('$cssSelector').text();
    }
});";

        file_put_contents($templateFileCopy,$configObj);
//exit;
        //Run the phantom script with php exec function, redirect output of script to an $output array;

        
        //$output = shell_exec("dir 2>&1 & echo $!"); //

        //phantomjs loadspeed.js http://www.google.com
        //$c = "phantomjs --ignore-ssl-errors=yes --ssl-protocol=tlsv1  pjscrape/pjscrape.js my_config.js";
        $c = "phantomjs.exe 2>&1";
        print "<br /> runing comand: $c<br />";
        print_in_textarea(shell_exec($c)); // $templateFileCopy
        //('phantomjs loadspeed.js http://www.google.com'));
        

        //delete the copied version of the phantom script as we don't need it anymore
        if ( !unlink( $templateFileCopy ) ) {
            echo "failed to delete $templateFileCopy";
            return false;
        }
        
        //get output from file pjscrape_out.json
        $output_file = "pjscrape_out.json";
        $text = file_get_contents($output_file);
        if(is_string($text)) $output = json_decode($text);
        
                //print_in_textarea($output);

        exit;
        // The first element of the output will be message about adding jquery and the last element will be the 'EXIT' message from the script,
        // lets remove those so all we have is the html lines

        //array_shift($output);
        //array_pop($output);

        //remove any whitespace from the array elements and join all the html lines into one string of all the html

        $output= array_map('trim', $output);
        $output = join("",$output);

        //construct an XML element from the html string
		$xmlObj = new \SimpleXMLElement($output);

        return $xmlObj;
    }
	
function scrapePopulationsByState(){

        $cssSelector = "#e1ResultsListContainer";//"table.sk_popcharttable";

        $siteUrl =  "https://www.equipmentone.com/search?kw=man";//"http://www.ipl.org/div/stateknow/popchart.html";

        $tableXmlObject = $this->pullXmlObjBlogExample($siteUrl,$cssSelector);

        $cnt = 0;

        foreach($tableXmlObject->tbody->tr as $tableRow){

            //the first two rows are the header and "All United States" rows so disregard

            if($cnt++ < 2)
                continue;

            //grab the state and population from the corresponding table cell of the row and output!

            $state = (string) $tableRow->td[1]->a;

            $population = (string) $tableRow->td[2];

            echo $state . " has a population of " . $population . "\n";

        }

}
 }
$a = new ScraperController();
$a->scrapePopulationsByState();
?>
</body>
 </html>
