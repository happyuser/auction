
pjs.addSuite({
    title: 'Basic Scraper Suite',
    url: [
        'http://localhost/test_site/index.html',
        'http://localhost/test_site/page1.html',
        'http://localhost/test_site/page2.html'
    ],
    scrapers: [
        function() {
            return $('h1').first().text();
        }
    ]
});