<?php
abstract class Auction_ScraperAbstractController {
    
    protected $keyword;
    protected $table_name;
    protected $mysqli;

	public $debug_mode = false;

	/**
	 * Domain to be scraped
	 *
	 * @var string
	 */
	protected $domain;

	/**
	 * Dealer ID
	 *
	 * @var int
	 */
	private $did;
    
    private $proxy_ip;

	/**
	 * Constructs the class
	 *
	 * @param  string $domain
	 * @param  int    $did
	 */
	public function __construct($domain, $did)
	{
		if (!is_numeric($did))
			throw new InvalidArgumentException();

		$this->domain = $domain;
		$this->did = $did;
		$this->mysqli = db_connect();
	}

	/**
	 * Runs the scraper
	 *
	 * @return void
	 */
	abstract public function run();

	/**
	 * Runs the scraper for keyword
	 *
	 * @param string $keyword
	 * @return void
	 */
	abstract public function scrape_keyword($keyword);

	/**
	 * Returns the current Dealer ID
	 *
	 * @return int
	 */
	protected function getDid()
	{
		return $this->did;
	}


	/**
	 * Returns the domain to be scraped
	 *
	 * @returns string
	 */
	protected function getDomain()
	{
		return $this->domain;
	}

	/**
	 * Returns the output filename
	 *
	 * @return string output CSV filename
	 */
	protected function getOutputFilename()
	{
		$did = $this->getDid();
		return __DIR__."/Feeds/$did".time().".csv";
	}
	
	/**
	 * Initializes a new session and returns a cURL handle for use with the curl_setopt(), curl_exec(), and curl_close() functions 
	 * 
	 * @param  string $url (optional)
	 * @return resource returns a cURL handle on success, FALSE on errors 
	 */
	public function getCurlHandle($url = NULL, $use_proxy = false)
	{
	   //TODO: enable proxy
       //BUG: get $this from instance 
	    
		$ch = curl_init($url);
		//curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (compatible; Autobot/1.0)");
        curl_setopt( $ch, CURLOPT_HEADER, 0 ) ;
		curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        if($use_proxy)
        {
            $proxy = $this->getNextProxyIP();
            //print_for_developer("proxy - ".$proxy.", ");
            $proxyauth = $this->getNextProxyLogin();//'user:password';
            //print "proxyauth  - ".$proxyauth;
            curl_setopt($ch, CURLOPT_PROXY, $proxy);
            curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
        }
        
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if( function_exists("random_user_agent")) curl_setopt($ch, CURLOPT_USERAGENT,random_user_agent());
        //print random_user_agent(); exit; 
		
		return $ch;
	}
	
	/**
	 * Loads the CSV file into an array
	 * 
	 * @param  string datale
	 * @return array  file contents
	 */
	public function loadCsvIntoArray($file)
	{
		$input_csv = fopen($file,"r");
		if (FALSE === $input_csv)
			throw new Exception(sprintf("File not found: '%s'!", $file));
		
		$contents = array();
		while (($columns = fgetcsv($input_csv, 100, ",")) !== FALSE) {
			if (isset($columns[0]))
				array_push($contents, $columns[0]);
		}
	
		fclose($input_csv);
		return $contents;
	}
	
	// ???
	public function simpleLoadURL($url)
	{
		$curl = $this->getCurlHandle();
		curl_setopt($curl, CURLOPT_SSLVERSION,3);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($curl);
		
		return $response;
	}
	
	// izmo (wrong - needs to be cURL)
	function post_request($url, $data, $referer='')
	{
	
		// Convert the data array into URL Parameters like a=b&foo=bar etc.
		$data = http_build_query($data);
	
		// parse the given URL
		$url = parse_url($url);
	
		if ($url['scheme'] != 'http') {
			die('Error: Only HTTP request are supported !');
		}
	
		// extract host and path:
		$host = $url['host'];
		$path = $url['path'];
	
		// open a socket connection on port 80 - timeout: 30 sec
		$fp = fsockopen($host, 80, $errno, $errstr, 30);
	
		if ($fp){
	
			// send the request headers:
			fputs($fp, "POST $path HTTP/1.1\r\n");
			fputs($fp, "Host: $host\r\n");
	
			if ($referer != '')
				fputs($fp, "Referer: $referer\r\n");
	
			fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
			fputs($fp, "Content-length: ". strlen($data) ."\r\n");
			fputs($fp, "Connection: close\r\n\r\n");
			fputs($fp, $data);
	
			$result = '';
			while(!feof($fp)) {
				// receive the results of the request
				$result .= fgets($fp, 128);
			}
		}
		else {
			return array(
					'status' => 'err',
					'error' => "$errstr ($errno)"
			);
		}
	
		// close the socket connection:
		fclose($fp);
	
		// split the result header from the content
		$result = explode("\r\n\r\n", $result, 2);
	
		$header = isset($result[0]) ? $result[0] : '';
		$content = isset($result[1]) ? $result[1] : '';
	
		// return as structured array:
		return array(
				'status' => 'ok',
				'header' => $header,
				'content' => $content
		);
	
	}	
    
    	
    function print_in_textarea2($string, $width=600, $height=400)
    {
        print "<textarea style='height:$height\px; width:$width\px;'>";
        print_r($string);
        print "</textarea>";
    }
    
    function table_empty($table_name)
    {
        	$q = "Select * from `$table_name`";
                print "<br />".$q."<br>";
    			$res = mysqli_query($this->mysqli, $q); 
                
            print '<br />$res->num_rows:'.$res->num_rows;
    		if($res) 
                return !(($res->num_rows) > 0);
    		else 
    		{
    		  return;
    			//print "nothing to add";
    		}
    }
    
    function if_need_notify_user($table_name, $item_id)
    {
    	if($item_id != "")
    	{
    		$q = "Select * from `$table_name` where `item_id` = '$item_id'";
            //print $q."<br>";
            
            //exit;
    		$res = mysqli_query($this->mysqli, $q); 
            if($res) 
                return ($res->num_rows < 1);
                
    	}
    	else 
    	{
    		//print "nothing to add";
    	}
        return 0;
    }
    
    function get_keywords()
    {
        $q = "Select * from `search_string`";
        //print "<br />" . $q . "<br>";
        $res = mysqli_query($this->mysqli, $q);

        $keywords = array();
        while ($r = mysqli_fetch_assoc($res)) {
            $keywords[] = $r['search_string'];
        }

        if (is_array($keywords))
            return $keywords;
        else {
            return;
            //print "nothing to add";
        }

    }
    
    
    
    private function get_search_string_id()
    {
        $q = "SELECT `id` FROM `search_string` WHERE `search_string` = '$this->keyword'";
//        print "<br />" . $q . "<br>";
        $res = mysqli_query($this->mysqli, $q);

//        print '<br />$res->num_rows:' . $res->num_rows;
        $r = (mysqli_fetch_assoc($res));
        $id = $r['id'];
        
        if ($res)
            return ($id);
        else {
            return;
            //print "nothing to add";
        }

    }

    function get_auction_id()
    {
        $q = "SELECT `id` FROM `auction_site` WHERE `name` = '$this->table_name'";
//        print "<br />" . $q . "<br>";
        $res = mysqli_query($this->mysqli, $q);

//        print '<br />$res->num_rows:' . $res->num_rows;
        $r = (mysqli_fetch_assoc($res));
        $id = $r['id'];
        
        if ($res)
            return ($id);
        else {
            return;
            //print "nothing to add";
        }

    }

    function get_notification_items(&$items)
	{
		$q = "Select * from `search_string`";
		$res = mysqli_query($this->mysqli, $q);
		$keywords = array();
		while ($r = mysqli_fetch_assoc($res))
			$keywords[$r['id']] = $r['search_string'];

		$q = "SELECT * FROM `{$this->table_name}` WHERE `need_notify_user`='1' ORDER BY search_string_id, date_scraped DESC";
		$res = mysqli_query($this->mysqli, $q);
		$lots_count = mysqli_num_rows($res);
		if ($this->debug_mode)
			echo "new lots count:", $lots_count, "<br />";
		if (!$lots_count)
			return 0;

		while ($row = mysqli_fetch_assoc($res)) {
			$keyword = $keywords[$row['search_string_id']];
			$lot = json_decode($row['json']);
			if (!isset($items[$keyword]))
				$items[$keyword] = array();
			$items[$keyword][] = array("title" => $lot->title, 'link' => $lot->link, 'site' => $this->domain);
		}
		// notify
		// Auctions@mwisales.com
		$to_name = "mwisales";
		$to_email_address = "";//xianionline@gmail.com
		$from_email_name = "happyuser";
		$from_email_address = "office@happyuser.info";
		$email_subject = $lots_count." new auction" .($lots_count>1? "s" : ""). " for keyword '{$this->keyword}' on website: {$this->domain}";
		$res = tep_mail($to_name, $to_email_address, $email_subject, $email_text, $from_email_name, $from_email_address);

		$to_email_address .= ", "."tarasprystavskyj@gmail.com";
        $to_email_address .= ", "."Auctions@mwisales.com";
		$res = tep_mail($to_name, $to_email_address, $email_subject, $email_text, $from_email_name, $from_email_address);
		if ($res) {
			// clear notify
			$q = "UPDATE `{$this->table_name}` SET `need_notify_user`='0' WHERE `need_notify_user`='1' AND `search_string_id`='$search_string_id'";
			mysqli_query($this->mysqli, $q);
		}
		return $lots_count;
	}
    
    function save_scraping_log()
    {
        $search_string_id = $this->get_search_string_id();
        $auction_id = $this->get_auction_id();
        
   	    if($auction_id != "" && $search_string_id != "")
		{
			$q = "INSERT INTO `scraping_log`(`auction_id`, `search_string_id`) VALUES ('$auction_id','$search_string_id')";
			$res = mysqli_query($this->mysqli, $q); 
		if($res) ;//print "added search string:".$param;
		}
		else 
		{
			//print "nothing to add";
		}
    }
    	
	function save_auction_record($table_name, $item_id, $v, $init = false)
	{
		if($item_id != "")
		{
			$json = $v;
			//print_in_textarea($json);
            
            if($init)
            {
                    $need_notify_user = 0;
            }
            else 
            {                
                if($this->if_need_notify_user($table_name, $item_id ))
                    $need_notify_user = 1;
                else 
                    return; // not insert data to DB
            }
            
//            print '<br />$need_notify_user:'.$need_notify_user;
//            print '<br /> $init:'.$init;
            
            //exit;
            if($this->if_item_id_exists($table_name, $item_id))
            {
//                print "<br />item_id_exists";
                return;// do nothing
            }
            
            $search_string_id = $this->get_search_string_id();
			if (!$search_string_id) {
//                print "<br />not saving lots for unknown keyword ({$this->keyword})";
				return;
			}

            $json = mysqli_real_escape_string($this->mysqli, $json);
			$q = "Insert INTO `$table_name`(`item_id`,`json`, `need_notify_user`, `search_string_id`) VALUES ('$item_id', '$json', '$need_notify_user', '$search_string_id')";
            //print "<br />".$q."<br>";
            //exit;
			$res = mysqli_query($this->mysqli, $q) or die(mysqli_error($this->mysqli)." query:".htmlspecialchars($q));
            if($res) ;//print "added search string:".$param;
		}
		else 
		{
			//print "nothing to add";
		}
	}
    
    function if_item_id_exists($table_name, $item_id)
    {
        $q = "select `id` from $table_name where `item_id` = '$item_id'";
        //print "<br />".$q."<br>";
        //exit;
		$res = mysqli_query($this->mysqli, $q); 
        if($res && ($res->num_rows > 0)) 
            return 1;
        else
            return 0;
    }
    
    function if_need_init_keyword($table_name, $keyword)
	{
		if($table_name != "" && $keyword != "")
		{
			$q = "SELECT * FROM `scraping_log` WHERE 
            `auction_id` in (select `id` from auction_site where `name` = '$table_name') and 
`search_string_id` in (select `id` from `search_string` where `search_string` = '$keyword')";
            //print "<br />".$q."<br>";
			$res = mysqli_query($this->mysqli, $q); 
            //print '<br />if_need_init_keyword:'.($res->num_rows < 1);
            
		if($res) return ($res->num_rows < 1);
		}
		else 
		{
			//print "<br />:".$this->table_name.", ".$this->keyword;
		}
        return 0;
    }
    
    
    private function load_page($url, $convert_to_html_dom = true, $use_proxy = false)
    {
 		$htmlText = curl_exec($this->getCurlHandle($url, $use_proxy));
		if (!isset($htmlText)) {
			if ($_GET['environment'] == "browser")
				print "<br/>can't find <a href='$url'>$url</a>";
            else print "<br/>can't find $url";
			return;
		}
		if ($convert_to_html_dom) return new simple_html_dom($htmlText);
        else return $htmlText;
    }

    private function load_file($url, $use_proxy = false)
    {
        //$html = new simple_html_dom() ;
        $ch = $this->getCurlHandle($url, $use_proxy);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt( $ch, CURLOPT_HEADER, 0);
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		$htmlText = curl_exec($ch);
		if (!isset($htmlText))
		{
		    if ($_GET['environment'] == "browser")
			    print "<br/>can't find <a href='$url'>$url</a>";
            else print "<br/>can't find $url";
			return;
		}
        return $htmlText;
    }
    
    
    private function getNextProxy()
    {
        if (empty($this->a_proxies)) {
            $f_proxies = file_get_contents("proxies.txt");
            $this->a_proxies = explode("\r\n", $f_proxies);
            $this->a_proxies_banned = array();
        }
        $this->current_proxy_index = rand(0,count($this->a_proxies)-1);
        if (in_array($this->current_proxy_index, $this->a_proxies_banned)) {
            if (($this->current_proxy_index+1) < count($this->a_proxies))
                $this->current_proxy_index++;
        }
        //TODO: for testing
        //$this->current_proxy_index++;
        $proxy = $this->a_proxies[$this->current_proxy_index];
        $this->a_proxy = explode( ":", $proxy);
    }

    public function getNextProxyIP()
    {
        $this->getNextProxy();
        $this->proxy_ip = $this->a_proxy[0].":".$this->a_proxy[1];
        return $this->proxy_ip; 
        //192.40.94.210:39275:vs18us37:8dcn6sur
     }

    public function getNextProxyLogin()
    {
        $this->getNextProxy();
        //192.40.94.210:39275:vs18us37:8dcn6sur
        //return "vs18us37:8dcn6sur";
        $this->proxy_login = $this->a_proxy[2].":".$this->a_proxy[3];
        return $this->proxy_login;
    }

    public function report_proxy_fail($proxy_ip, $url)
    {
        if($proxy_ip == "") { w("proxy_ip is empty"); return; }
        //get proxy id from DB
        $q = "select * from proxies where ip like '".$proxy_ip."%'";
        $r = mysql_query($q);
        if ($r) { $r2 = mysql_fetch_array($r); $proxy_id = $r2[0]; }
        else {
			w('proxy ip '.$proxy_ip.' not found in the table q ='.$q.', '.mysql_error($r));
			return;
		}
        $q = "insert into proxy_session(proxy_id, successful, fail_url) values('$proxy_id', 0, '".mysql_real_escape_string($url)."' )";
        $r = mysql_query($q);
        if ($r); // ok
        else { w('cant insert proxy query: "'.$q.'" .');  return; }
        //TODO: get fails count1
        //TODO: get fails count2 and blacklist proxy
        $q = "select count(*) from proxy_session where (date_time > (NOW() - ".(PROXY_2RETRIES_TIME_LIMIT_MINS*60)."))";
        $r = mysql_query($q);
        if ($r) {
            $c = mysql_fetch_array($r);
            if ($c[0] > PROXY_2RETRIES_COUNT)
                mysql_query("update proxies set `blacklisted` = 1 where Id = '$proxy_id'");
        }
    }

	protected function load_search_page($url, $post_data=false, $return_headers = false)
	{
		$debug_cached_filename = "debug_cache/".$this->domain."_".md5($url);
		if ($post_data)
			$debug_cached_filename .= "_".md5(http_build_query($post_data));
		$debug_cached_filename .= ".html";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		if ($post_data) {
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
		}

		// receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		// do not follow "Location" redirects
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);

		if ($return_headers)
		{
			curl_setopt($ch, CURLOPT_HEADER, 1);
		}


		if ($this->debug_mode && file_exists($debug_cached_filename)) {
			$server_output = file_get_contents($debug_cached_filename);
		} else
			$server_output = curl_exec($ch);

		curl_close($ch);

		// further processing ....
		if ($server_output != "") {
			if ($this->debug_mode) {
				file_put_contents($debug_cached_filename, $server_output);
			}
			if ($return_headers)
			{
				$server_output = explode("\r\n\r\n", $server_output);

				$headers = array();
				foreach (explode("\r\n", $server_output[0]) as $i => $line) {
					if ($i === 0)
						$headers['http_code'] = $line;
					else {
						list ($key, $value) = explode(': ', $line);

						$headers[$key] = $value;
					}
				}
				$server_output[0] = $headers;
			}
			return $server_output;
		} else {
		}
		return "";
	}

}
	
