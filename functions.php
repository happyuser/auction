<?php

/*
 * @name functions
 * @author happyuser
 * @copyright 2014
*/

function not_empty_line($var) {
	if (preg_match("@[a-z0-9]@i",$var)) return true; else return false;
}


function t() { // for testing only
    if (array_key_exists('environment',$_GET) && $_GET['environment'] == "browser") 
        return 1;
    else return 0;
}

function testing() { // for testing only
    if (array_key_exists('environment',$_GET) && $_GET['environment'] == "testing") 
        return 1;
    else return 0;
}

function w($message) {
    global $count_warnings;
    global $a_warnings;
    print ((php_sapi_name() == 'cli') ? "WARNING: $message\n" :
		"<br/>WARNING: <span style='color: yellow; background-color: black'>$message</span>");
    $count_warnings++;
    $a_warnings[] = $message;
}

function html_link($href, $name = "") {
    $name = (($name == "") ? $name : $href);
    return "<a href='$href' target='_blanc'>$name</a>";
}

function print_in_textarea($a, $width = 600, $height = 300, $max_objects = 1000) {
    print "<textarea style='width:$width\px; height: $height\px;'>";
    if (count($a, COUNT_RECURSIVE) < $max_objects) print_r($a);
    else { 
        print "count of objects > $max_objects = ".count($a, COUNT_RECURSIVE)."\n";
        print $a;
    }
    print "</textarea>";
}

function print_for_developer($text) {
    $ip = $_SERVER['REMOTE_ADDR']; 
    if ($ip == DEVELOPER_IP) print $text;     
}

function get_link($href, $target) { return "<a href='$href'>$target</a>"; }

// Send email (text/html) using MIME
// This is the central mail function. The SMTP Server should be configured
// correct in php.ini
// Parameters:
// $to_name           The name of the recipient, e.g. "Jan Wildeboer"
// $to_email_address  The eMail address of the recipient,
//                    e.g. jan.wildeboer@gmx.de
// $email_subject     The subject of the eMail
// $email_text        The text of the eMail, may contain HTML entities
// $from_email_name   The name of the sender, e.g. Shop Administration
// $from_email_adress The eMail address of the sender,
//                    e.g. info@mytepshop.com

function tep_mail($to_name, $to_email_address, $email_subject, $email_text, $from_email_name, $from_email_address) {
	if (SEND_EMAILS != 'true') return false;
	$headers = "From: $from_email_name <$from_email_address>\r\n".
		"MIME-Version: 1.0" . "\r\n" .
		"Content-type: text/html; charset=UTF-8" . "\r\n";
	return mail($to_email_address, $email_subject, $email_text, $headers);
}

function on_critical_error($m) {
	w($m);
	before_exit($m);
}

function before_exit($m) {
    // send report to developer
    mail_to_developer("script name=".$_SERVER['REQUEST_URI']." exited on ".date(DATE_RFC822,time()));
}

function mail_to_developer($message) {
    tep_mail("taras", "happyuserster@gmail.com", "report for scraping homeadvisor.com", $message,
		"Taras", "happyuserster@gmail.com");
}


function _isCurl() {
    if (function_exists('curl_version')) {
		//echo "curl is enabled<br />"; // will do an action.
		;
	} else echo "curl is disabled<br />"; // will do another action.
}

?>
