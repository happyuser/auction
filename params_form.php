<?PHP
require "include/db_connect.php";
require "include/auction_functions.php";


//print_r($_POST["param"] );
//print "<br/>";
//print 'array_key_exists("param",$_POST)'.array_key_exists("param",$_POST);
//print "<br/>";
if(array_key_exists("param",$_POST))
{
	//clear table
	$q = "TRUNCATE search_string";
	mysqli_query($mysqli, $q);
	 
	//print "if(array_key_exists(\"param\",\$_POST))";
	foreach($_POST["param"] as $key=>$value)
	{
		//print "$key <br/>";
		save_param($mysqli, $value);
	}
}

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>three.js webgl - trackball controls</title>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
		<link href="css/style.css" rel="stylesheet" type="text/css" />
	</head>

	<body>
	<hr/>
		<script src="./include/jquery-1.6.2.min.js"></script>		
		<label><h3> Search strings</h3> </label>

<form id="f1" name="f1" action="#" method="post">
		
<?php
$for_update = false;
$a_params = get_params($mysqli, $for_update);
if(is_array($a_params))
{
	print "<label> You can edit existing params here </label>";
	foreach($a_params as $key=> $value)
	{
		print '<input name="param[]" value="'.$value.'" type="edit">'."\n";
	}
}
else
{print "<br/><font color='red'>no search strings in DB</font><br/>";}
?>

<hr/>
		<label> Please add new search strings to the list </label>

<input name="param[]" value="" type="edit">
<input name="param[]" value="" type="edit">
<input name="param[]" value="" type="edit">
<input type="submit" caption=" add new search strings to the list">
</form>

		</body>
</html>
