	<?php
include_once("functions.php");
include_once("configure.php");
//include_once("database.php");
set_time_limit(36000);
//defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
define('APPLICATION_ENV', 'development');
//http://localhost:82/_Yossi%20tools/crawl_world/scraper.php?vendor=play_com&domain=play.com&did=1234
require "simple_html_dom.php";
//include("random-user-agent.php");
require "ScraperAbstractController.php";
require "BaseScraperController.php";

require "include/db_connect.php";
/* @var mysqli $mysqli */
		
chdir('/var/www/main/auction');
if (php_sapi_name() != 'cli') print "<!DOCTYPE html>
<html><head><meta charset='utf-8' /></head><body>";

//$_GET["vendor"] = "salvagesale"; 
//$_GET["domain"] = ".com"; $_GET["did"] = "1234";
//$_GET["vendor"] = "google"; $_GET["domain"] = "google.com"; $_GET["did"] = "1234";
//$_GET["vendor"] = "autodealerwebsite"; $_GET["domain"] = "maundimports.com"; $_GET["did"] = "1234";
//$_GET["vendor"] = "autofunds"; $_GET["domain"] = "cantonautoexchange.com"; $_GET["did"] = "1234";
//$_GET["vendor"] = "liquidmotors"; $_GET["domain"] = "hyundaioflongview.com"; $_GET["did"] = "1234";
//$_GET["vendor"] = "liquidmotors"; $_GET["domain"] = "genuinemotorcars.com"; $_GET["did"] = "1234";
// liquidmotors hyundaioflongview.com 1234
/*
if ((!array_key_exists('argc', $_SERVER)) ||
		(($_SERVER['argc'] != 4) || (!is_numeric($_SERVER['argv'][3])))) {
	if (isset($argv)) printf("Usage: ./%s <vendor> <domain> <did>\n", $argv[0]);
	$_GET['environment'] = "browser";

	if (is_array($_GET) && array_key_exists("vendor",$_GET) ) { //&& array_key_exists("domain",$_GET) && array_key_exists("did",$_GET)
		$vendor = $_GET["vendor"];
		$domain = $_GET["domain"];
		$did = (int)$_GET["did"];
	} else exit("No input data.\n");
} else {
	$vendor = $_SERVER['argv'][1];
	$domain = $_SERVER['argv'][2];
	$did = (int)$_SERVER['argv'][3];
}
*/

define("SEND_EMAILS", 'true');

$time_log = array();
$starttime = microtime(true);

$scrapers = scandir("Scrapers");
unset($scrapers[0]);
unset($scrapers[1]);

$q = "SELECT * FROM `auction_site` WHERE `php_or_java_script` = 'PHP' and do_scrape = 1";
$res = mysqli_query($mysqli,$q);

$run_scrapers = array();
while($r = mysqli_fetch_assoc($res))
	$run_scrapers[] = $r;

echo "<div id='scrapers_list'>Scrapers:";
foreach ($run_scrapers as $r)
	echo "<a href='#{$r['name']}'>{$r['name']}</a>, ";
echo "</div>";

$scrapers_items = array();
$new_lots_count = 0;
foreach ($run_scrapers as $r)
{
	$vendor = $r['name'];
	$domain = $r['url'];

	print "<fieldset id='$vendor'>";
	print "<legend>Scraping: $vendor ($domain)</legend>";

	$currentScraper = $vendor . ".php";
	if (FALSE === array_search($currentScraper, $scrapers)) {
		printf("Scraper not found: '%s'.n", $currentScraper);
		printf("Available scrapers:\n%s\n", implode("\n", $scrapers));
		exit;
	}

	$time_log['scraper_'.$vendor] = array( 'start' => microtime(true) );
	require __DIR__ . "/Scrapers/$currentScraper";
	$scraperClassName = ucfirst($vendor)."_ScraperController";
	$scraper = new $scraperClassName($domain, 1234); /* @var $scraper Auction_ScraperAbstractController */
	$scraper->debug_mode = false;

	try {
		$scraper->run();
		$new_lots_count += $scraper->get_notification_items($scraper_items);
	}
	catch(Exception $e) {
		on_critical_error($e->getMessage());
		print "exit line: " . __LINE__ . "\n";
	}
	$time_log['scraper_'.$vendor]['end'] = microtime(true);

	echo "<hr />";
	echo "<a href='#{$r['name']}'>to section start</a> | <a href='#scrapers_list'>to scrapers list</a>";
	print "</fieldset>";
}

if ($new_lots_count) {
// notify
	echo "<hr>Notify user<br>";
	$time_log['notify email'] = array( 'start' => microtime(true) );

	$email_text = "";
	$site = "";
	if (!empty($scraper_items) && is_array($scraper_items))
		foreach ($scraper_items as $keyword => $items) {
			$site = "";
			$keyword_lots_count = count($items);
			$email_text .= "<h3>'$keyword' - " . $keyword_lots_count . " new auction" . ($keyword_lots_count > 1 ? "s" : "") . "</h3>";
			foreach ($items as $item) {
				if ($item['site'] != $site) {
					if ($site)
						$email_text .= "<br />";
					$email_text .= "<div>" . $item['site'] . "</div>";
					$site = $item['site'];
				}
				$email_text .= "<a href='{$item['link']}'>{$item['title']}</a><br />";
			}
		}

//echo "EMAIL!!!!!<hr>";
//echo $new_lots_count. " new auction" .($new_lots_count>1? "s" : "");
//echo "<hr>";
//echo $email_text;
//echo "<hr>";
//exit;

// notify
// Auctions@mwisales.com
	$to_name = "mwisales";
	$to_email_address = "Auctions@mwisales.com";
//	$to_email_address .= "," . "tarasprystavskyj@gmail.com";
//	$to_email_address .= "," . "xianionline@gmail.com";

	$from_email_name = "happyuser";
	$from_email_address = "noreply@188.226.157.20";

	$email_subject = $new_lots_count . " new auction" . ($new_lots_count > 1 ? "s" : "");

	$res = tep_mail($to_name, $to_email_address, $email_subject, $email_text, $from_email_name, $from_email_address);

	$time_log['notify email']['end'] = microtime(true);


	$db_email = mysqli_real_escape_string($mysqli, $email_text);
	$db_res = $res? 1 : 0;
	$q = "INSERT INTO `email_log` SET lots_found=$new_lots_count, email='$db_email', result=$db_res";
	mysqli_query($mysqli, $q);

	if ($res) {
		// clear notify
		// assume we notified about ALL items, so just drop need_notify_user for all
		foreach ($run_scrapers as $r) {
			$table_name = $r['name'];
			$q = "UPDATE `{$table_name}` SET `need_notify_user`='0' WHERE `need_notify_user`='1'";
			mysqli_query($mysqli, $q);
		}
	}
}




$endtime = microtime(true);
$runtime = $endtime - $starttime;
printf("executed on server in %s seconds", $runtime);
echo "<br />";
echo "<br />";
foreach ($time_log as $section => $value) {
	echo round($value['end']-$value['start'], 2), "s ", $section, "<br />";
}

if (php_sapi_name() != 'cli') print "</body></html>";

