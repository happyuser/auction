<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
//ini_set("implicit_flush", "true");
set_time_limit(0);

include_once("functions.php");
include_once("configure.php");
//include_once("database.php");
set_time_limit(36000);
//defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
define('APPLICATION_ENV', 'development');
//http://localhost:82/_Yossi%20tools/crawl_world/scraper.php?vendor=play_com&domain=play.com&did=1234
require "simple_html_dom.php";
//include("random-user-agent.php");
require "BaseScraperController.php";
require "ScraperAbstractController.php";
require "include/db_connect.php";

if (php_sapi_name() != 'cli') print "<!DOCTYPE html>
<html><head><meta charset='utf-8' /></head><body>";

//$_GET["vendor"] = "purplewave2"; $_GET["domain"] = "purplewave.com"; $_GET["did"] = "1234";
//$_GET["vendor"] = "google"; $_GET["domain"] = "google.com"; $_GET["did"] = "1234";
//$_GET["vendor"] = "autodealerwebsite"; $_GET["domain"] = "maundimports.com"; $_GET["did"] = "1234";
//$_GET["vendor"] = "autofunds"; $_GET["domain"] = "cantonautoexchange.com"; $_GET["did"] = "1234";
//$_GET["vendor"] = "liquidmotors"; $_GET["domain"] = "hyundaioflongview.com"; $_GET["did"] = "1234";
//$_GET["vendor"] = "liquidmotors"; $_GET["domain"] = "genuinemotorcars.com"; $_GET["did"] = "1234";
// liquidmotors hyundaioflongview.com 1234

if ((!array_key_exists('argc', $_SERVER)) ||
		(($_SERVER['argc'] != 4) || (!is_numeric($_SERVER['argv'][3])))) {
	if (isset($argv)) printf("Usage: ./%s <vendor> <domain> <did>\n", $argv[0]);
	$_GET['environment'] = "browser";

	if (is_array($_GET) && array_key_exists("vendor",$_GET) &&
			array_key_exists("domain",$_GET) && array_key_exists("did",$_GET)) {
		$vendor = $_GET["vendor"];
		$domain = $_GET["domain"];
		$did = (int)$_GET["did"];
	} else exit("No input data.\n");
} else {
	$vendor = $_SERVER['argv'][1];
	$domain = $_SERVER['argv'][2];
	$did = (int)$_SERVER['argv'][3];
}

$scrapers = scandir("Scrapers");
unset($scrapers[0]);
unset($scrapers[1]);

$currentScraper = $vendor . ".php";

if (FALSE === array_search($currentScraper, $scrapers)) {
	printf("Scraper not found: '%s'.n", $currentScraper);
	printf("Available scrapers:\n%s\n", implode("\n", $scrapers));
	exit;
}

require __DIR__ . "/Scrapers/$currentScraper";
$scraperClassName = ucfirst($vendor)."_ScraperController";
$scraper = new $scraperClassName($domain, $did); /* @var $scraper Auction_ScraperAbstractController */
$scraper->debug_mode = true;

define("SEND_EMAILS", 'true');
printf("Run scraper: '%s'.\n", $currentScraper);
try { $scraper->run(); }
catch(Exception $e) {
	on_critical_error($e->getMessage());
	print "exit line: " . __LINE__ . "\n";
}
if (php_sapi_name() != 'cli') print "</body></html>";

