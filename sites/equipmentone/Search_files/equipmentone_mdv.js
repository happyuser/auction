/*
Template: External DMP
Author: Mediative 
version: 1.05
*/
(function( window, undefined ) {
	
	var MDVDATA = {} || MDVDATA;



	/* Revised function that can be repurposed to grab URI and break in to different parts */ 

	function parseUri(sourceUri){
    var uriPartNames = ["source","protocol","authority","domain","port","path","directoryPath","fileName","query","anchor"];
    var uriParts = new RegExp("^(?:([^:/?#.]+):)?(?://)?(([^:/?#]*)(?::(\\d*))?)?((/(?:[^?#](?![^?#/]*\\.[^?#/.]+(?:[\\?#]|$)))*/?)?([^?#/]*))?(?:\\?([^#]*))?(?:#(.*))?").exec(sourceUri);
    var uri = {};
    
    for(var i = 0; i < 10; i++){
        uri[uriPartNames[i]] = (uriParts[i] ? uriParts[i] : "");
    }
    
    // Always end directoryPath with a trailing backslash if a path was present in the source URI
    // Note that a trailing backslash is NOT automatically inserted within or appended to the "path" key
    if(uri.directoryPath.length > 0){
        uri.directoryPath = uri.directoryPath.replace(/\/?$/, "/");
    }
    
    return uri;
}
	

	
	MDVDATA.fn = MDVDATA.prototype = {	
	    debug:function(){
					this.collections.debug = true;
					return this
					},
	    trace:function(a){
					try{
						if(this.collections.debug == true){
						console.log("MEDIATIVE UTAG LOGS: " + a);
						}
						}catch(e){}
					
					
					},
		
		collections:{
			debug:false,
			mseg:[],
			lang:"en",
			sitename:"Equipmentone"
			
			
			},
			addseg :function(k,v){
if(window.MUTAG_RANDOM == undefined){
window.MUTAG_RANDOM = Math.round(Math.random()*100000000);
}
var mseg = {};
mseg[k]=v;
MDVDATA.collections.mseg.push(mseg);
return this;
},
populate:function(){
	var img = document.createElement('img');
	var tempURL = document.URL;
	//var tempURL = "https://www.equipmentone.com/search?kw=truck";
	var urlParts = parseUri(tempURL);
	var textimg = ""; 
	img.style.display = "none";
	textimg  += "//bcp.crwdcntrl.net/5/c=2726/";
	textimg  += "rand=" + String(Math.floor((Math.random()*100000000)+1));
	textimg  += "/pv=y/";
	textimg  += "/ctax=Advertisers";
	textimg += "^";
	textimg += this.collections.sitename;
	


	
	var countPath = urlParts["path"].split('/');



	var newPath = countPath.slice(1);

	
	//need some URL specific details
	


	if (tempURL == "https://www.equipmentone.com/") {
		//code
		textimg += "^";
		var string = "Home";	



	}	else if (urlParts['directoryPath'].indexOf('search') == 1) {

		
		var search = urlParts['query'];
		var splitSearch = search.split('kw=').slice(1);
		

		textimg += "^";
		textimg += "search";
		textimg += "^";
		var string = splitSearch;

	}	else {

		
		var path = urlParts["path"];
		var string = "en";

		textimg += "^";

		var slashes = path.split("/");
		//slashes=slashes.substring(0, (slashes.length -1));
	

		if (slashes.length <= 2){

			var newSlashes = slashes.slice(1);

			

			//string += "^";
			string += "^";
			string+= newSlashes[0];
		}	
		else {
			var newSlashes = slashes.slice(1);
			
			//string+= newSlashes[0];

			for(i=0;i<newSlashes.length;i++)
		{
			string += "^";
			string += newSlashes[i];
			}
			
		}
		//remove the file extension and only keep the prefix
		string=string.split(".",1);
	
		
	}
	
for(i=0;i<MDVDATA.collections.mseg.length;i++){
 for(k in MDVDATA.collections.mseg[i]){
textimg += String(k) + "=" + String(MDVDATA.collections.mseg[i][k]) + "/";
 }

}
textimg += string;
textimg +="//rt=ifr";
img.src = textimg;
document.getElementsByTagName("body")[0].appendChild(img);
return this;
},

MLoadEvent:function(){	
try{
var section = document.getElementById("rot_ctr1_bod_ctr3_bod_wrp1_blk2_custom").innerHTML;

string=section.substring(section.lastIndexOf("<font color=\"#b9101b\">"),section.lastIndexOf("</font>"));


if (string.indexOf("<br>") > 0){
	var string=string.substring(string.lastIndexOf("<font color=\"#b9101b\">")+22,string.lastIndexOf("<br>"));
}
else{
	var string=string.substring(string.lastIndexOf("<font color=\"#b9101b\">")+22,string.lastIndexOf("</font>"));
}

string = string.replace(/<\/a>/gi,"_");
string = string.replace(/<[^>]*>/g,"");
string = string.replace(/\s/gi,"");
string = string.replace(/_/,"");
string = string.replace(/amp;/,"");
string = string.replace(/Home/gi,"");


//MDVDATA.addseg("seg","language=" + this.collections.sitename + "-" + this.collections.lang + "-" + section);


}
  
catch(e){
MDVDATA.trace(e.message);
}


 try{
window.addEventListener('load', MDVDATA.MLoadEvent, false);
}catch(e){
window.attachEvent('onload', MDVDATA.MLoadEvent);
}}
	
		
		};	
					
		
		
	
		
	
MDVDATA = MDVDATA.fn;
                return (window.MDVDATA = window.$UTAG = MDVDATA);		
	
})(window);

//MDVDATA.MLoadEvent();	

MDVDATA.populate();