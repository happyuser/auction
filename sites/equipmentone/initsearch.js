/*===============================
https://www.equipmentone.com/assetnation/js/storagePolyfill.js
================================================================================*/
;
try {
    if (!window.localStorage || !window.sessionStorage) throw "exception";
    localStorage.setItem('storage_test', 1);
    localStorage.removeItem('storage_test');
} catch (e) {
    (function() {
        var Storage = function(type) {
            function createCookie(name, value, days) {
                var date, expires;
                if (days) {
                    date = new Date();
                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                    expires = "; expires=" + date.toGMTString();
                } else {
                    expires = "";
                }
                document.cookie = name + "=" + value + expires + "; path=/";
            }

            function readCookie(name) {
                var nameEQ = name + "=",
                    ca = document.cookie.split(';'),
                    i, c;
                for (i = 0; i < ca.length; i++) {
                    c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1, c.length);
                        2480
                    }
                    if (c.indexOf(nameEQ) == 0) {
                        return c.substring(nameEQ.length, c.length);
                    }
                }
                return null;
            }

            function setData(data) {
                data = encodeURIComponent(JSON.stringify(data));
                if (type == 'session') {
                    createCookie(getSessionName(), data, 365);
                } else {
                    createCookie('localStorage', data, 365);
                }
            }

            function clearData() {
                if (type == 'session') {
                    createCookie(getSessionName(), '', 365);
                } else {
                    createCookie('localStorage', '', 365);
                }
            }

            function getData() {
                var data = type == 'session' ? readCookie(getSessionName()) : readCookie('localStorage');
                return data ? JSON.parse(decodeURIComponent(data)) : {};
            }

            function getSessionName() {
                if (!window.name) {
                    window.name = new Date().getTime();
                }
                return 'sessionStorage' + window.name;
            }
            var data = getData();
            return {
                length: 0,
                clear: function() {
                    data = {};
                    this.length = 0;
                    clearData();
                },
                getItem: function(key) {
                    return data[key] === undefined ? null : data[key];
                },
                key: function(i) {
                    var ctr = 0;
                    for (var k in data) {
                        if (ctr == i) return k;
                        else ctr++;
                    }
                    return null;
                },
                removeItem: function(key) {
                    delete data[key];
                    this.length--;
                    setData(data);
                },
                setItem: function(key, value) {
                    data[key] = value + '';
                    this.length++;
                    setData(data);
                }
            };
        };
        var localStorage = new Storage('local');
        var sessionStorage = new Storage('session');
        window.localStorage = localStorage;
        window.sessionStorage = sessionStorage;
        window.localStorage.__proto__ = localStorage;
        window.sessionStorage.__proto__ = sessionStorage;
    })();
}

function clearSessionStorageData(type) {
    switch (type) {
        case 'search':
            localStorage.removeItem('scrollPos');
            localStorage.removeItem('EQ1_query');
            localStorage.removeItem('sourceSite');
            break;
        case 'userlistings':
            localStorage.removeItem('e1User_lastUpdate');
            localStorage.removeItem('e1User_watchlist');
            localStorage.removeItem('e1User_noteListings');
            localStorage.removeItem('e1User_activeListings');
            break;
        case 'myonemenu':
            localStorage.removeItem('menu_buying');
            localStorage.removeItem('menu_selling');
            localStorage.removeItem('menu_watchlist');
            localStorage.removeItem('menu_account');
            break;
    }
}
var hasLocalStorage = (function() {
    try {
        localStorage.setItem('storage_test', '1');
        localStorage.removeItem('storage_test');
        return true;
    } catch (exception) {
        return false;
    }
}());

function fncSetE1LocalStorage(key, val) {
    if (hasLocalStorage) {
        localStorage.setItem(key, val);
    }
}

function fncGetE1LocalStorage(key) {
    if (hasLocalStorage) {
        return localStorage.getItem(key);
    }
}

/*===============================
/plugins/system/t3/base-bs3/bootstrap/js/bootstrap.js
================================================================================*/
;
/*!
 * Bootstrap v3.1.1 (http://getbootstrap.com)
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */
if (typeof jQuery === 'undefined') {
    throw new Error('Bootstrap\'s JavaScript requires jQuery')
} + function($) {
    'use strict';

    function transitionEnd() {
        var el = document.createElement('bootstrap')
        var transEndEventNames = {
            'WebkitTransition': 'webkitTransitionEnd',
            'MozTransition': 'transitionend',
            'OTransition': 'oTransitionEnd otransitionend',
            'transition': 'transitionend'
        }
        for (var name in transEndEventNames) {
            if (el.style[name] !== undefined) {
                return {
                    end: transEndEventNames[name]
                }
            }
        }
        return false
    }
    $.fn.emulateTransitionEnd = function(duration) {
        var called = false,
            $el = this
        $(this).one($.support.transition.end, function() {
            called = true
        })
        var callback = function() {
            if (!called) $($el).trigger($.support.transition.end)
        }
        setTimeout(callback, duration)
        return this
    }
    $(function() {
        $.support.transition = transitionEnd()
    })
}(jQuery); + function($) {
    'use strict';
    var dismiss = '[data-dismiss="alert"]'
    var Alert = function(el) {
        $(el).on('click', dismiss, this.close)
    }
    Alert.prototype.close = function(e) {
        var $this = $(this)
        var selector = $this.attr('data-target')
        if (!selector) {
            selector = $this.attr('href')
            selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '')
        }
        var $parent = $(selector)
        if (e) e.preventDefault()
        if (!$parent.length) {
            $parent = $this.hasClass('alert') ? $this : $this.parent()
        }
        $parent.trigger(e = $.Event('close.bs.alert'))
        if (e.isDefaultPrevented()) return
        $parent.removeClass('in')

        function removeElement() {
            $parent.trigger('closed.bs.alert').remove()
        }
        $.support.transition && $parent.hasClass('fade') ? $parent.one($.support.transition.end, removeElement).emulateTransitionEnd(150) : removeElement()
    }
    var old = $.fn.alert
    $.fn.alert = function(option) {
        return this.each(function() {
            var $this = $(this)
            var data = $this.data('bs.alert')
            if (!data) $this.data('bs.alert', (data = new Alert(this)))
            if (typeof option == 'string') data[option].call($this)
        })
    }
    $.fn.alert.Constructor = Alert
    $.fn.alert.noConflict = function() {
        $.fn.alert = old
        return this
    }
    $(document).on('click.bs.alert.data-api', dismiss, Alert.prototype.close)
}(jQuery); + function($) {
    'use strict';
    var Button = function(element, options) {
        this.$element = $(element)
        this.options = $.extend({}, Button.DEFAULTS, options)
        this.isLoading = false
    }
    Button.DEFAULTS = {
        loadingText: 'loading...'
    }
    Button.prototype.setState = function(state) {
        var d = 'disabled'
        var $el = this.$element
        var val = $el.is('input') ? 'val' : 'html'
        var data = $el.data()
        state = state + 'Text'
        if (!data.resetText) $el.data('resetText', $el[val]())
        $el[val](data[state] || this.options[state])
        setTimeout($.proxy(function() {
            if (state == 'loadingText') {
                this.isLoading = true
                $el.addClass(d).attr(d, d)
            } else if (this.isLoading) {
                this.isLoading = false
                $el.removeClass(d).removeAttr(d)
            }
        }, this), 0)
    }
    Button.prototype.toggle = function() {
        var changed = true
        var $parent = this.$element.closest('[data-toggle="buttons"]')
        if ($parent.length) {
            var $input = this.$element.find('input')
            if ($input.prop('type') == 'radio') {
                if ($input.prop('checked') && this.$element.hasClass('active')) changed = false
                else $parent.find('.active').removeClass('active')
            }
            if (changed) $input.prop('checked', !this.$element.hasClass('active')).trigger('change')
        }
        if (changed) this.$element.toggleClass('active')
    }
    var old = $.fn.button
    $.fn.button = function(option) {
        return this.each(function() {
            var $this = $(this)
            var data = $this.data('bs.button')
            var options = typeof option == 'object' && option
            if (!data) $this.data('bs.button', (data = new Button(this, options)))
            if (option == 'toggle') data.toggle()
            else if (option) data.setState(option)
        })
    }
    $.fn.button.Constructor = Button
    $.fn.button.noConflict = function() {
        $.fn.button = old
        return this
    }
    $(document).on('click.bs.button.data-api', '[data-toggle^=button]', function(e) {
        var $btn = $(e.target)
        if (!$btn.hasClass('btn')) $btn = $btn.closest('.btn')
        $btn.button('toggle')
        e.preventDefault()
    })
}(jQuery); + function($) {
    'use strict';
    var Carousel = function(element, options) {
        this.$element = $(element)
        this.$indicators = this.$element.find('.carousel-indicators')
        this.options = options
        this.paused = this.sliding = this.interval = this.$active = this.$items = null
        this.options.pause == 'hover' && this.$element.on('mouseenter', $.proxy(this.pause, this)).on('mouseleave', $.proxy(this.cycle, this))
    }
    Carousel.DEFAULTS = {
        interval: 5000,
        pause: 'hover',
        wrap: true
    }
    Carousel.prototype.cycle = function(e) {
        e || (this.paused = false)
        this.interval && clearInterval(this.interval)
        this.options.interval && !this.paused && (this.interval = setInterval($.proxy(this.next, this), this.options.interval))
        return this
    }
    Carousel.prototype.getActiveIndex = function() {
        this.$active = this.$element.find('.item.active')
        this.$items = this.$active.parent().children()
        return this.$items.index(this.$active)
    }
    Carousel.prototype.to = function(pos) {
        var that = this
        var activeIndex = this.getActiveIndex()
        if (pos > (this.$items.length - 1) || pos < 0) return
        if (this.sliding) return this.$element.one('slid.bs.carousel', function() {
            that.to(pos)
        })
        if (activeIndex == pos) return this.pause().cycle()
        return this.slide(pos > activeIndex ? 'next' : 'prev', $(this.$items[pos]))
    }
    Carousel.prototype.pause = function(e) {
        e || (this.paused = true)
        if (this.$element.find('.next, .prev').length && $.support.transition) {
            this.$element.trigger($.support.transition.end)
            this.cycle(true)
        }
        this.interval = clearInterval(this.interval)
        return this
    }
    Carousel.prototype.next = function() {
        if (this.sliding) return
        return this.slide('next')
    }
    Carousel.prototype.prev = function() {
        if (this.sliding) return
        return this.slide('prev')
    }
    Carousel.prototype.slide = function(type, next) {
        var $active = this.$element.find('.item.active')
        var $next = next || $active[type]()
        var isCycling = this.interval
        var direction = type == 'next' ? 'left' : 'right'
        var fallback = type == 'next' ? 'first' : 'last'
        var that = this
        if (!$next.length) {
            if (!this.options.wrap) return
            $next = this.$element.find('.item')[fallback]()
        }
        if ($next.hasClass('active')) return this.sliding = false
        var e = $.Event('slide.bs.carousel', {
            relatedTarget: $next[0],
            direction: direction
        })
        this.$element.trigger(e)
        if (e.isDefaultPrevented()) return
        this.sliding = true
        isCycling && this.pause()
        if (this.$indicators.length) {
            this.$indicators.find('.active').removeClass('active')
            this.$element.one('slid.bs.carousel', function() {
                var $nextIndicator = $(that.$indicators.children()[that.getActiveIndex()])
                $nextIndicator && $nextIndicator.addClass('active')
            })
        }
        if ($.support.transition && this.$element.hasClass('slide')) {
            $next.addClass(type)
            $next[0].offsetWidth
            $active.addClass(direction)
            $next.addClass(direction)
            $active.one($.support.transition.end, function() {
                $next.removeClass([type, direction].join(' ')).addClass('active')
                $active.removeClass(['active', direction].join(' '))
                that.sliding = false
                setTimeout(function() {
                    that.$element.trigger('slid.bs.carousel')
                }, 0)
            }).emulateTransitionEnd($active.css('transition-duration').slice(0, -1) * 1000)
        } else {
            $active.removeClass('active')
            $next.addClass('active')
            this.sliding = false
            this.$element.trigger('slid.bs.carousel')
        }
        isCycling && this.cycle()
        return this
    }
    var old = $.fn.carousel
    $.fn.carousel = function(option) {
        return this.each(function() {
            var $this = $(this)
            var data = $this.data('bs.carousel')
            var options = $.extend({}, Carousel.DEFAULTS, $this.data(), typeof option == 'object' && option)
            var action = typeof option == 'string' ? option : options.slide
            if (!data) $this.data('bs.carousel', (data = new Carousel(this, options)))
            if (typeof option == 'number') data.to(option)
            else if (action) data[action]()
            else if (options.interval) data.pause().cycle()
        })
    }
    $.fn.carousel.Constructor = Carousel
    $.fn.carousel.noConflict = function() {
        $.fn.carousel = old
        return this
    }
    $(document).on('click.bs.carousel.data-api', '[data-slide], [data-slide-to]', function(e) {
        var $this = $(this),
            href
        var $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, ''))
        var options = $.extend({}, $target.data(), $this.data())
        var slideIndex = $this.attr('data-slide-to')
        if (slideIndex) options.interval = false
        $target.carousel(options)
        if (slideIndex = $this.attr('data-slide-to')) {
            $target.data('bs.carousel').to(slideIndex)
        }
        e.preventDefault()
    })
    $(window).on('load', function() {
        $('[data-ride="carousel"]').each(function() {
            var $carousel = $(this)
            $carousel.carousel($carousel.data())
        })
    })
}(jQuery); + function($) {
    'use strict';
    var Collapse = function(element, options) {
        this.$element = $(element)
        this.options = $.extend({}, Collapse.DEFAULTS, options)
        this.transitioning = null
        if (this.options.parent) this.$parent = $(this.options.parent)
        if (this.options.toggle) this.toggle()
    }
    Collapse.DEFAULTS = {
        toggle: false
    }
    Collapse.prototype.dimension = function() {
        var hasWidth = this.$element.hasClass('width')
        return hasWidth ? 'width' : 'height'
    }
    Collapse.prototype.show = function() {
        if (this.transitioning || this.$element.hasClass('in')) return
        var startEvent = $.Event('show.bs.collapse')
        this.$element.trigger(startEvent)
        if (startEvent.isDefaultPrevented()) return
        var actives = this.$parent && this.$parent.find('> .panel > .in')
        if (actives && actives.length) {
            var hasData = actives.data('bs.collapse')
            if (hasData && hasData.transitioning) return
            actives.collapse('hide')
            hasData || actives.data('bs.collapse', null)
        }
        var dimension = this.dimension()
        this.$element.removeClass('collapse').addClass('collapsing')[dimension](0)
        this.transitioning = 1
        var complete = function() {
            this.$element.removeClass('collapsing').addClass('collapse in')[dimension]('auto')
            this.transitioning = 0
            this.$element.trigger('shown.bs.collapse')
        }
        if (!$.support.transition) return complete.call(this)
        var scrollSize = $.camelCase(['scroll', dimension].join('-'))
        this.$element.one($.support.transition.end, $.proxy(complete, this)).emulateTransitionEnd(350)[dimension](this.$element[0][scrollSize])
    }
    Collapse.prototype.hide = function() {
        if (this.transitioning || !this.$element.hasClass('in')) return
        var startEvent = $.Event('hide.bs.collapse')
        this.$element.trigger(startEvent)
        if (startEvent.isDefaultPrevented()) return
        var dimension = this.dimension()
        this.$element[dimension](this.$element[dimension]())[0].offsetHeight
        this.$element.addClass('collapsing').removeClass('collapse').removeClass('in')
        this.transitioning = 1
        var complete = function() {
            this.transitioning = 0
            this.$element.trigger('hidden.bs.collapse').removeClass('collapsing').addClass('collapse')
        }
        if (!$.support.transition) return complete.call(this)
        this.$element[dimension](0).one($.support.transition.end, $.proxy(complete, this)).emulateTransitionEnd(350)
    }
    Collapse.prototype.toggle = function() {
        this[this.$element.hasClass('in') ? 'hide' : 'show']()
    }
    var old = $.fn.collapse
    $.fn.collapse = function(option) {
        return this.each(function() {
            var $this = $(this)
            var data = $this.data('bs.collapse')
            var options = $.extend({}, Collapse.DEFAULTS, $this.data(), typeof option == 'object' && option)
            if (!data && options.toggle && option == 'show') option = !option
            if (!data) $this.data('bs.collapse', (data = new Collapse(this, options)))
            if (typeof option == 'string') data[option]()
        })
    }
    $.fn.collapse.Constructor = Collapse
    $.fn.collapse.noConflict = function() {
        $.fn.collapse = old
        return this
    }
    $(document).on('click.bs.collapse.data-api', '[data-toggle=collapse]', function(e) {
        var $this = $(this),
            href
        var target = $this.attr('data-target') || e.preventDefault() || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')
        var $target = $(target)
        var data = $target.data('bs.collapse')
        var option = data ? 'toggle' : $this.data()
        var parent = $this.attr('data-parent')
        var $parent = parent && $(parent)
        if (!data || !data.transitioning) {
            if ($parent) $parent.find('[data-toggle=collapse][data-parent="' + parent + '"]').not($this).addClass('collapsed')
            $this[$target.hasClass('in') ? 'addClass' : 'removeClass']('collapsed')
        }
        $target.collapse(option)
    })
}(jQuery); + function($) {
    'use strict';
    var backdrop = '.dropdown-backdrop'
    var toggle = '[data-toggle=dropdown]'
    var Dropdown = function(element) {
        $(element).on('click.bs.dropdown', this.toggle)
    }
    Dropdown.prototype.toggle = function(e) {
        var $this = $(this)
        if ($this.is('.disabled, :disabled')) return
        var $parent = getParent($this)
        var isActive = $parent.hasClass('open')
        clearMenus()
        if (!isActive) {
            if ('ontouchstart' in document.documentElement && !$parent.closest('.navbar-nav').length) {
                $('<div class="dropdown-backdrop"/>').insertAfter($(this)).on('click', clearMenus)
            }
            var relatedTarget = {
                relatedTarget: this
            }
            $parent.trigger(e = $.Event('show.bs.dropdown', relatedTarget))
            if (e.isDefaultPrevented()) return
            $parent.toggleClass('open').trigger('shown.bs.dropdown', relatedTarget)
            $this.focus()
        }
        return false
    }
    Dropdown.prototype.keydown = function(e) {
        if (!/(38|40|27)/.test(e.keyCode)) return
        var $this = $(this)
        e.preventDefault()
        e.stopPropagation()
        if ($this.is('.disabled, :disabled')) return
        var $parent = getParent($this)
        var isActive = $parent.hasClass('open')
        if (!isActive || (isActive && e.keyCode == 27)) {
            if (e.which == 27) $parent.find(toggle).focus()
            return $this.click()
        }
        var desc = ' li:not(.divider):visible a'
        var $items = $parent.find('[role=menu]' + desc + ', [role=listbox]' + desc)
        if (!$items.length) return
        var index = $items.index($items.filter(':focus'))
        if (e.keyCode == 38 && index > 0) index--
            if (e.keyCode == 40 && index < $items.length - 1) index++
                if (!~index) index = 0
        $items.eq(index).focus()
    }

    function clearMenus(e) {
        $(backdrop).remove()
        $(toggle).each(function() {
            var $parent = getParent($(this))
            var relatedTarget = {
                relatedTarget: this
            }
            if (!$parent.hasClass('open')) return
            $parent.trigger(e = $.Event('hide.bs.dropdown', relatedTarget))
            if (e.isDefaultPrevented()) return
            $parent.removeClass('open').trigger('hidden.bs.dropdown', relatedTarget)
        })
    }

    function getParent($this) {
        var selector = $this.attr('data-target')
        if (!selector) {
            selector = $this.attr('href')
            selector = selector && /#[A-Za-z]/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '')
        }
        var $parent = selector && $(selector)
        return $parent && $parent.length ? $parent : $this.parent()
    }
    var old = $.fn.dropdown
    $.fn.dropdown = function(option) {
        return this.each(function() {
            var $this = $(this)
            var data = $this.data('bs.dropdown')
            if (!data) $this.data('bs.dropdown', (data = new Dropdown(this)))
            if (typeof option == 'string') data[option].call($this)
        })
    }
    $.fn.dropdown.Constructor = Dropdown
    $.fn.dropdown.noConflict = function() {
        $.fn.dropdown = old
        return this
    }
    $(document).on('click.bs.dropdown.data-api', clearMenus).on('click.bs.dropdown.data-api', '.dropdown form', function(e) {
        e.stopPropagation()
    }).on('click.bs.dropdown.data-api', toggle, Dropdown.prototype.toggle).on('keydown.bs.dropdown.data-api', toggle + ', [role=menu], [role=listbox]', Dropdown.prototype.keydown)
}(jQuery); + function($) {
    'use strict';
    var Modal = function(element, options) {
        this.options = options
        this.$element = $(element)
        this.$backdrop = this.isShown = null
        if (this.options.remote) {
            this.$element.find('.modal-content').load(this.options.remote, $.proxy(function() {
                this.$element.trigger('loaded.bs.modal')
            }, this))
        }
    }
    Modal.DEFAULTS = {
        backdrop: true,
        keyboard: true,
        show: true
    }
    Modal.prototype.toggle = function(_relatedTarget) {
        return this[!this.isShown ? 'show' : 'hide'](_relatedTarget)
    }
    Modal.prototype.show = function(_relatedTarget) {
        var that = this
        var e = $.Event('show.bs.modal', {
            relatedTarget: _relatedTarget
        })
        this.$element.trigger(e)
        if (this.isShown || e.isDefaultPrevented()) return
        this.isShown = true
        this.escape()
        this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))
        this.backdrop(function() {
            var transition = $.support.transition && that.$element.hasClass('fade')
            if (!that.$element.parent().length) {
                that.$element.appendTo(document.body)
            }
            that.$element.show().scrollTop(0)
            if (transition) {
                that.$element[0].offsetWidth
            }
            that.$element.addClass('in').attr('aria-hidden', false)
            that.enforceFocus()
            var e = $.Event('shown.bs.modal', {
                relatedTarget: _relatedTarget
            })
            transition ? that.$element.find('.modal-dialog').one($.support.transition.end, function() {
                that.$element.focus().trigger(e)
            }).emulateTransitionEnd(300) : that.$element.focus().trigger(e)
        })
    }
    Modal.prototype.hide = function(e) {
        if (e) e.preventDefault()
        e = $.Event('hide.bs.modal')
        this.$element.trigger(e)
        if (!this.isShown || e.isDefaultPrevented()) return
        this.isShown = false
        this.escape()
        $(document).off('focusin.bs.modal')
        this.$element.removeClass('in').attr('aria-hidden', true).off('click.dismiss.bs.modal')
        $.support.transition && this.$element.hasClass('fade') ? this.$element.one($.support.transition.end, $.proxy(this.hideModal, this)).emulateTransitionEnd(300) : this.hideModal()
    }
    Modal.prototype.enforceFocus = function() {
        $(document).off('focusin.bs.modal').on('focusin.bs.modal', $.proxy(function(e) {
            if (this.$element[0] !== e.target && !this.$element.has(e.target).length) {
                this.$element.focus()
            }
        }, this))
    }
    Modal.prototype.escape = function() {
        if (this.isShown && this.options.keyboard) {
            this.$element.on('keyup.dismiss.bs.modal', $.proxy(function(e) {
                e.which == 27 && this.hide()
            }, this))
        } else if (!this.isShown) {
            this.$element.off('keyup.dismiss.bs.modal')
        }
    }
    Modal.prototype.hideModal = function() {
        var that = this
        this.$element.hide()
        this.backdrop(function() {
            that.removeBackdrop()
            that.$element.trigger('hidden.bs.modal')
        })
    }
    Modal.prototype.removeBackdrop = function() {
        this.$backdrop && this.$backdrop.remove()
        this.$backdrop = null
    }
    Modal.prototype.backdrop = function(callback) {
        var animate = this.$element.hasClass('fade') ? 'fade' : ''
        if (this.isShown && this.options.backdrop) {
            var doAnimate = $.support.transition && animate
            this.$backdrop = $('<div class="modal-backdrop ' + animate + '" />').appendTo(document.body)
            this.$element.on('click.dismiss.bs.modal', $.proxy(function(e) {
                if (e.target !== e.currentTarget) return
                this.options.backdrop == 'static' ? this.$element[0].focus.call(this.$element[0]) : this.hide.call(this)
            }, this))
            if (doAnimate) this.$backdrop[0].offsetWidth
            this.$backdrop.addClass('in')
            if (!callback) return
            doAnimate ? this.$backdrop.one($.support.transition.end, callback).emulateTransitionEnd(150) : callback()
        } else if (!this.isShown && this.$backdrop) {
            this.$backdrop.removeClass('in')
            $.support.transition && this.$element.hasClass('fade') ? this.$backdrop.one($.support.transition.end, callback).emulateTransitionEnd(150) : callback()
        } else if (callback) {
            callback()
        }
    }
    var old = $.fn.modal
    $.fn.modal = function(option, _relatedTarget) {
        return this.each(function() {
            var $this = $(this)
            var data = $this.data('bs.modal')
            var options = $.extend({}, Modal.DEFAULTS, $this.data(), typeof option == 'object' && option)
            if (!data) $this.data('bs.modal', (data = new Modal(this, options)))
            if (typeof option == 'string') data[option](_relatedTarget)
            else if (options.show) data.show(_relatedTarget)
        })
    }
    $.fn.modal.Constructor = Modal
    $.fn.modal.noConflict = function() {
        $.fn.modal = old
        return this
    }
    $(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function(e) {
        var $this = $(this)
        var href = $this.attr('href')
        var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, '')))
        var option = $target.data('bs.modal') ? 'toggle' : $.extend({
            remote: !/#/.test(href) && href
        }, $target.data(), $this.data())
        if ($this.is('a')) e.preventDefault()
        $target.modal(option, this).one('hide', function() {
            $this.is(':visible') && $this.focus()
        })
    })
    $(document).on('show.bs.modal', '.modal', function() {
        $(document.body).addClass('modal-open')
    }).on('hidden.bs.modal', '.modal', function() {
        $(document.body).removeClass('modal-open')
    })
}(jQuery); + function($) {
    'use strict';
    var Tooltip = function(element, options) {
        this.type = this.options = this.enabled = this.timeout = this.hoverState = this.$element = null
        this.init('tooltip', element, options)
    }
    Tooltip.DEFAULTS = {
        animation: true,
        placement: 'top',
        selector: false,
        template: '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
        trigger: 'hover focus',
        title: '',
        delay: 0,
        html: false,
        container: false
    }
    Tooltip.prototype.init = function(type, element, options) {
        this.enabled = true
        this.type = type
        this.$element = $(element)
        this.options = this.getOptions(options)
        var triggers = this.options.trigger.split(' ')
        for (var i = triggers.length; i--;) {
            var trigger = triggers[i]
            if (trigger == 'click') {
                this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this))
            } else if (trigger != 'manual') {
                var eventIn = trigger == 'hover' ? 'mouseenter' : 'focusin'
                var eventOut = trigger == 'hover' ? 'mouseleave' : 'focusout'
                this.$element.on(eventIn + '.' + this.type, this.options.selector, $.proxy(this.enter, this))
                this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this))
            }
        }
        this.options.selector ? (this._options = $.extend({}, this.options, {
            trigger: 'manual',
            selector: ''
        })) : this.fixTitle()
    }
    Tooltip.prototype.getDefaults = function() {
        return Tooltip.DEFAULTS
    }
    Tooltip.prototype.getOptions = function(options) {
        options = $.extend({}, this.getDefaults(), this.$element.data(), options)
        if (options.delay && typeof options.delay == 'number') {
            options.delay = {
                show: options.delay,
                hide: options.delay
            }
        }
        return options
    }
    Tooltip.prototype.getDelegateOptions = function() {
        var options = {}
        var defaults = this.getDefaults()
        this._options && $.each(this._options, function(key, value) {
            if (defaults[key] != value) options[key] = value
        })
        return options
    }
    Tooltip.prototype.enter = function(obj) {
        var self = obj instanceof this.constructor ? obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
        clearTimeout(self.timeout)
        self.hoverState = 'in'
        if (!self.options.delay || !self.options.delay.show) return self.show()
        self.timeout = setTimeout(function() {
            if (self.hoverState == 'in') self.show()
        }, self.options.delay.show)
    }
    Tooltip.prototype.leave = function(obj) {
        var self = obj instanceof this.constructor ? obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
        clearTimeout(self.timeout)
        self.hoverState = 'out'
        if (!self.options.delay || !self.options.delay.hide) return self.hide()
        self.timeout = setTimeout(function() {
            if (self.hoverState == 'out') self.hide()
        }, self.options.delay.hide)
    }
    Tooltip.prototype.show = function() {
        var e = $.Event('show.bs.' + this.type)
        if (this.hasContent() && this.enabled) {
            this.$element.trigger(e)
            if (e.isDefaultPrevented()) return
            var that = this;
            var $tip = this.tip()
            this.setContent()
            if (this.options.animation) $tip.addClass('fade')
            var placement = typeof this.options.placement == 'function' ? this.options.placement.call(this, $tip[0], this.$element[0]) : this.options.placement
            var autoToken = /\s?auto?\s?/i
            var autoPlace = autoToken.test(placement)
            if (autoPlace) placement = placement.replace(autoToken, '') || 'top'
            $tip.detach().css({
                top: 0,
                left: 0,
                display: 'block'
            }).addClass(placement)
            this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element)
            var pos = this.getPosition()
            var actualWidth = $tip[0].offsetWidth
            var actualHeight = $tip[0].offsetHeight
            if (autoPlace) {
                var $parent = this.$element.parent()
                var orgPlacement = placement
                var docScroll = document.documentElement.scrollTop || document.body.scrollTop
                var parentWidth = this.options.container == 'body' ? window.innerWidth : $parent.outerWidth()
                var parentHeight = this.options.container == 'body' ? window.innerHeight : $parent.outerHeight()
                var parentLeft = this.options.container == 'body' ? 0 : $parent.offset().left
                placement = placement == 'bottom' && pos.top + pos.height + actualHeight - docScroll > parentHeight ? 'top' : placement == 'top' && pos.top - docScroll - actualHeight < 0 ? 'bottom' : placement == 'right' && pos.right + actualWidth > parentWidth ? 'left' : placement == 'left' && pos.left - actualWidth < parentLeft ? 'right' : placement
                $tip.removeClass(orgPlacement).addClass(placement)
            }
            var calculatedOffset = this.getCalculatedOffset(placement, pos, actualWidth, actualHeight)
            this.applyPlacement(calculatedOffset, placement)
            this.hoverState = null
            var complete = function() {
                that.$element.trigger('shown.bs.' + that.type)
            }
            $.support.transition && this.$tip.hasClass('fade') ? $tip.one($.support.transition.end, complete).emulateTransitionEnd(150) : complete()
        }
    }
    Tooltip.prototype.applyPlacement = function(offset, placement) {
        var replace
        var $tip = this.tip()
        var width = $tip[0].offsetWidth
        var height = $tip[0].offsetHeight
        var marginTop = parseInt($tip.css('margin-top'), 10)
        var marginLeft = parseInt($tip.css('margin-left'), 10)
        if (isNaN(marginTop)) marginTop = 0
        if (isNaN(marginLeft)) marginLeft = 0
        offset.top = offset.top + marginTop
        offset.left = offset.left + marginLeft
        $.offset.setOffset($tip[0], $.extend({
            using: function(props) {
                $tip.css({
                    top: Math.round(props.top),
                    left: Math.round(props.left)
                })
            }
        }, offset), 0)
        $tip.addClass('in')
        var actualWidth = $tip[0].offsetWidth
        var actualHeight = $tip[0].offsetHeight
        if (placement == 'top' && actualHeight != height) {
            replace = true
            offset.top = offset.top + height - actualHeight
        }
        if (/bottom|top/.test(placement)) {
            var delta = 0
            if (offset.left < 0) {
                delta = offset.left * -2
                offset.left = 0
                $tip.offset(offset)
                actualWidth = $tip[0].offsetWidth
                actualHeight = $tip[0].offsetHeight
            }
            this.replaceArrow(delta - width + actualWidth, actualWidth, 'left')
        } else {
            this.replaceArrow(actualHeight - height, actualHeight, 'top')
        }
        if (replace) $tip.offset(offset)
    }
    Tooltip.prototype.replaceArrow = function(delta, dimension, position) {
        this.arrow().css(position, delta ? (50 * (1 - delta / dimension) + '%') : '')
    }
    Tooltip.prototype.setContent = function() {
        var $tip = this.tip()
        var title = this.getTitle()
        $tip.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](title)
        $tip.removeClass('fade in top bottom left right')
    }
    Tooltip.prototype.hide = function() {
        var that = this
        var $tip = this.tip()
        var e = $.Event('hide.bs.' + this.type)

        function complete() {
            if (that.hoverState != 'in') $tip.detach()
            that.$element.trigger('hidden.bs.' + that.type)
        }
        this.$element.trigger(e)
        if (e.isDefaultPrevented()) return
        $tip.removeClass('in')
        $.support.transition && this.$tip.hasClass('fade') ? $tip.one($.support.transition.end, complete).emulateTransitionEnd(150) : complete()
        this.hoverState = null
        return this
    }
    Tooltip.prototype.fixTitle = function() {
        var $e = this.$element
        if ($e.attr('title') || typeof($e.attr('data-original-title')) != 'string') {
            $e.attr('data-original-title', $e.attr('title') || '').attr('title', '')
        }
    }
    Tooltip.prototype.hasContent = function() {
        return this.getTitle()
    }
    Tooltip.prototype.getPosition = function() {
        var el = this.$element[0]
        return $.extend({}, (typeof el.getBoundingClientRect == 'function') ? el.getBoundingClientRect() : {
            width: el.offsetWidth,
            height: el.offsetHeight
        }, this.$element.offset())
    }
    Tooltip.prototype.getCalculatedOffset = function(placement, pos, actualWidth, actualHeight) {
        return placement == 'bottom' ? {
            top: pos.top + pos.height,
            left: pos.left + pos.width / 2 - actualWidth / 2
        } : placement == 'top' ? {
            top: pos.top - actualHeight,
            left: pos.left + pos.width / 2 - actualWidth / 2
        } : placement == 'left' ? {
            top: pos.top + pos.height / 2 - actualHeight / 2,
            left: pos.left - actualWidth
        } : {
            top: pos.top + pos.height / 2 - actualHeight / 2,
            left: pos.left + pos.width
        }
    }
    Tooltip.prototype.getTitle = function() {
        var title
        var $e = this.$element
        var o = this.options
        title = $e.attr('data-original-title') || (typeof o.title == 'function' ? o.title.call($e[0]) : o.title)
        return title
    }
    Tooltip.prototype.tip = function() {
        return this.$tip = this.$tip || $(this.options.template)
    }
    Tooltip.prototype.arrow = function() {
        return this.$arrow = this.$arrow || this.tip().find('.tooltip-arrow')
    }
    Tooltip.prototype.validate = function() {
        if (!this.$element[0].parentNode) {
            this.hide()
            this.$element = null
            this.options = null
        }
    }
    Tooltip.prototype.enable = function() {
        this.enabled = true
    }
    Tooltip.prototype.disable = function() {
        this.enabled = false
    }
    Tooltip.prototype.toggleEnabled = function() {
        this.enabled = !this.enabled
    }
    Tooltip.prototype.toggle = function(e) {
        var self = e ? $(e.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type) : this
        self.tip().hasClass('in') ? self.leave(self) : self.enter(self)
    }
    Tooltip.prototype.destroy = function() {
        clearTimeout(this.timeout)
        this.hide().$element.off('.' + this.type).removeData('bs.' + this.type)
    }
    var old = $.fn.tooltip
    $.fn.tooltip = function(option) {
        return this.each(function() {
            var $this = $(this)
            var data = $this.data('bs.tooltip')
            var options = typeof option == 'object' && option
            if (!data && option == 'destroy') return
            if (!data) $this.data('bs.tooltip', (data = new Tooltip(this, options)))
            if (typeof option == 'string') data[option]()
        })
    }
    $.fn.tooltip.Constructor = Tooltip
    $.fn.tooltip.noConflict = function() {
        $.fn.tooltip = old
        return this
    }
}(jQuery); + function($) {
    'use strict';
    var Popover = function(element, options) {
        this.init('popover', element, options)
    }
    if (!$.fn.tooltip) throw new Error('Popover requires tooltip.js')
    Popover.DEFAULTS = $.extend({}, $.fn.tooltip.Constructor.DEFAULTS, {
        placement: 'right',
        trigger: 'click',
        content: '',
        template: '<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    })
    Popover.prototype = $.extend({}, $.fn.tooltip.Constructor.prototype)
    Popover.prototype.constructor = Popover
    Popover.prototype.getDefaults = function() {
        return Popover.DEFAULTS
    }
    Popover.prototype.setContent = function() {
        var $tip = this.tip()
        var title = this.getTitle()
        var content = this.getContent()
        $tip.find('.popover-title')[this.options.html ? 'html' : 'text'](title)
        $tip.find('.popover-content')[this.options.html ? (typeof content == 'string' ? 'html' : 'append') : 'text'](content)
        $tip.removeClass('fade top bottom left right in')
        if (!$tip.find('.popover-title').html()) $tip.find('.popover-title').hide()
    }
    Popover.prototype.hasContent = function() {
        return this.getTitle() || this.getContent()
    }
    Popover.prototype.getContent = function() {
        var $e = this.$element
        var o = this.options
        return $e.attr('data-content') || (typeof o.content == 'function' ? o.content.call($e[0]) : o.content)
    }
    Popover.prototype.arrow = function() {
        return this.$arrow = this.$arrow || this.tip().find('.arrow')
    }
    Popover.prototype.tip = function() {
        if (!this.$tip) this.$tip = $(this.options.template)
        return this.$tip
    }
    var old = $.fn.popover
    $.fn.popover = function(option) {
        return this.each(function() {
            var $this = $(this)
            var data = $this.data('bs.popover')
            var options = typeof option == 'object' && option
            if (!data && option == 'destroy') return
            if (!data) $this.data('bs.popover', (data = new Popover(this, options)))
            if (typeof option == 'string') data[option]()
        })
    }
    $.fn.popover.Constructor = Popover
    $.fn.popover.noConflict = function() {
        $.fn.popover = old
        return this
    }
}(jQuery); + function($) {
    'use strict';

    function ScrollSpy(element, options) {
        var href
        var process = $.proxy(this.process, this)
        this.$element = $(element).is('body') ? $(window) : $(element)
        this.$body = $('body')
        this.$scrollElement = this.$element.on('scroll.bs.scroll-spy.data-api', process)
        this.options = $.extend({}, ScrollSpy.DEFAULTS, options)
        this.selector = (this.options.target || ((href = $(element).attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) || '') + ' .nav li > a'
        this.offsets = $([])
        this.targets = $([])
        this.activeTarget = null
        this.refresh()
        this.process()
    }
    ScrollSpy.DEFAULTS = {
        offset: 10
    }
    ScrollSpy.prototype.refresh = function() {
        var offsetMethod = this.$element[0] == window ? 'offset' : 'position'
        this.offsets = $([])
        this.targets = $([])
        var self = this
        var $targets = this.$body.find(this.selector).map(function() {
            var $el = $(this)
            var href = $el.data('target') || $el.attr('href')
            var $href = /^#./.test(href) && $(href)
            return ($href && $href.length && $href.is(':visible') && [
                [$href[offsetMethod]().top + (!$.isWindow(self.$scrollElement.get(0)) && self.$scrollElement.scrollTop()), href]
            ]) || null
        }).sort(function(a, b) {
            return a[0] - b[0]
        }).each(function() {
            self.offsets.push(this[0])
            self.targets.push(this[1])
        })
    }
    ScrollSpy.prototype.process = function() {
        var scrollTop = this.$scrollElement.scrollTop() + this.options.offset
        var scrollHeight = this.$scrollElement[0].scrollHeight || this.$body[0].scrollHeight
        var maxScroll = scrollHeight - this.$scrollElement.height()
        var offsets = this.offsets
        var targets = this.targets
        var activeTarget = this.activeTarget
        var i
        if (scrollTop >= maxScroll) {
            return activeTarget != (i = targets.last()[0]) && this.activate(i)
        }
        if (activeTarget && scrollTop <= offsets[0]) {
            return activeTarget != (i = targets[0]) && this.activate(i)
        }
        for (i = offsets.length; i--;) {
            activeTarget != targets[i] && scrollTop >= offsets[i] && (!offsets[i + 1] || scrollTop <= offsets[i + 1]) && this.activate(targets[i])
        }
    }
    ScrollSpy.prototype.activate = function(target) {
        this.activeTarget = target
        $(this.selector).parentsUntil(this.options.target, '.active').removeClass('active')
        var selector = this.selector + '[data-target="' + target + '"],' +
            this.selector + '[href="' + target + '"]'
        var active = $(selector).parents('li').addClass('active')
        if (active.parent('.dropdown-menu').length) {
            active = active.closest('li.dropdown').addClass('active')
        }
        active.trigger('activate.bs.scrollspy')
    }
    var old = $.fn.scrollspy
    $.fn.scrollspy = function(option) {
        return this.each(function() {
            var $this = $(this)
            var data = $this.data('bs.scrollspy')
            var options = typeof option == 'object' && option
            if (!data) $this.data('bs.scrollspy', (data = new ScrollSpy(this, options)))
            if (typeof option == 'string') data[option]()
        })
    }
    $.fn.scrollspy.Constructor = ScrollSpy
    $.fn.scrollspy.noConflict = function() {
        $.fn.scrollspy = old
        return this
    }
    $(window).on('load', function() {
        $('[data-spy="scroll"]').each(function() {
            var $spy = $(this)
            $spy.scrollspy($spy.data())
        })
    })
}(jQuery); + function($) {
    'use strict';
    var Tab = function(element) {
        this.element = $(element)
    }
    Tab.prototype.show = function() {
        var $this = this.element
        var $ul = $this.closest('ul:not(.dropdown-menu)')
        var selector = $this.data('target')
        if (!selector) {
            selector = $this.attr('href')
            selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '')
        }
        if ($this.parent('li').hasClass('active')) return
        var previous = $ul.find('.active:last a')[0]
        var e = $.Event('show.bs.tab', {
            relatedTarget: previous
        })
        $this.trigger(e)
        if (e.isDefaultPrevented()) return
        var $target = $(selector)
        this.activate($this.parent('li'), $ul)
        this.activate($target, $target.parent(), function() {
            $this.trigger({
                type: 'shown.bs.tab',
                relatedTarget: previous
            })
        })
    }
    Tab.prototype.activate = function(element, container, callback) {
        var $active = container.find('> .active')
        var transition = callback && $.support.transition && $active.hasClass('fade')

        function next() {
            $active.removeClass('active').find('> .dropdown-menu > .active').removeClass('active')
            element.addClass('active')
            if (transition) {
                element[0].offsetWidth
                element.addClass('in')
            } else {
                element.removeClass('fade')
            }
            if (element.parent('.dropdown-menu')) {
                element.closest('li.dropdown').addClass('active')
            }
            callback && callback()
        }
        transition ? $active.one($.support.transition.end, next).emulateTransitionEnd(150) : next()
        $active.removeClass('in')
    }
    var old = $.fn.tab
    $.fn.tab = function(option) {
        return this.each(function() {
            var $this = $(this)
            var data = $this.data('bs.tab')
            if (!data) $this.data('bs.tab', (data = new Tab(this)))
            if (typeof option == 'string') data[option]()
        })
    }
    $.fn.tab.Constructor = Tab
    $.fn.tab.noConflict = function() {
        $.fn.tab = old
        return this
    }
    $(document).on('click.bs.tab.data-api', '[data-toggle="tab"], [data-toggle="pill"]', function(e) {
        e.preventDefault()
        $(this).tab('show')
    })
}(jQuery); + function($) {
    'use strict';
    var Affix = function(element, options) {
        this.options = $.extend({}, Affix.DEFAULTS, options)
        this.$window = $(window).on('scroll.bs.affix.data-api', $.proxy(this.checkPosition, this)).on('click.bs.affix.data-api', $.proxy(this.checkPositionWithEventLoop, this))
        this.$element = $(element)
        this.affixed = this.unpin = this.pinnedOffset = null
        this.checkPosition()
    }
    Affix.RESET = 'affix affix-top affix-bottom'
    Affix.DEFAULTS = {
        offset: 0
    }
    Affix.prototype.getPinnedOffset = function() {
        if (this.pinnedOffset) return this.pinnedOffset
        this.$element.removeClass(Affix.RESET).addClass('affix')
        var scrollTop = this.$window.scrollTop()
        var position = this.$element.offset()
        return (this.pinnedOffset = position.top - scrollTop)
    }
    Affix.prototype.checkPositionWithEventLoop = function() {
        setTimeout($.proxy(this.checkPosition, this), 1)
    }
    Affix.prototype.checkPosition = function() {
        if (!this.$element.is(':visible')) return
        var scrollHeight = $(document).height()
        var scrollTop = this.$window.scrollTop()
        var position = this.$element.offset()
        var offset = this.options.offset
        var offsetTop = offset.top
        var offsetBottom = offset.bottom
        if (this.affixed == 'top') position.top += scrollTop
        if (typeof offset != 'object') offsetBottom = offsetTop = offset
        if (typeof offsetTop == 'function') offsetTop = offset.top(this.$element)
        if (typeof offsetBottom == 'function') offsetBottom = offset.bottom(this.$element)
        var affix = this.unpin != null && (scrollTop + this.unpin <= position.top) ? false : offsetBottom != null && (position.top + this.$element.height() >= scrollHeight - offsetBottom) ? 'bottom' : offsetTop != null && (scrollTop <= offsetTop) ? 'top' : false
        if (this.affixed === affix) return
        if (this.unpin) this.$element.css('top', '')
        var affixType = 'affix' + (affix ? '-' + affix : '')
        var e = $.Event(affixType + '.bs.affix')
        this.$element.trigger(e)
        if (e.isDefaultPrevented()) return
        this.affixed = affix
        this.unpin = affix == 'bottom' ? this.getPinnedOffset() : null
        this.$element.removeClass(Affix.RESET).addClass(affixType).trigger($.Event(affixType.replace('affix', 'affixed')))
        if (affix == 'bottom') {
            this.$element.offset({
                top: scrollHeight - offsetBottom - this.$element.height()
            })
        }
    }
    var old = $.fn.affix
    $.fn.affix = function(option) {
        return this.each(function() {
            var $this = $(this)
            var data = $this.data('bs.affix')
            var options = typeof option == 'object' && option
            if (!data) $this.data('bs.affix', (data = new Affix(this, options)))
            if (typeof option == 'string') data[option]()
        })
    }
    $.fn.affix.Constructor = Affix
    $.fn.affix.noConflict = function() {
        $.fn.affix = old
        return this
    }
    $(window).on('load', function() {
        $('[data-spy="affix"]').each(function() {
            var $spy = $(this)
            var data = $spy.data()
            data.offset = data.offset || {}
            if (data.offsetBottom) data.offset.bottom = data.offsetBottom
            if (data.offsetTop) data.offset.top = data.offsetTop
            $spy.affix(data)
        })
    })
}(jQuery);

/*===============================
/plugins/system/t3/base-bs3/js/jquery.tap.min.js
================================================================================*/
;
! function(r, t) {
    "use strict";
    var d = t.support.touch = !!("ontouchstart" in window || window.DocumentTouch && r instanceof DocumentTouch),
        e = "._tap",
        i = "tap",
        f = 40,
        l = 400,
        a = "clientX clientY screenX screenY pageX pageY".split(" "),
        s = function(n, e) {
            var c;
            return c = Array.prototype.indexOf ? n.indexOf(e) : t.inArray(e, n)
        },
        n = {
            $el: null,
            x: 0,
            y: 0,
            count: 0,
            cancel: !1,
            start: 0
        },
        u = function(o, i) {
            var n = i.originalEvent,
                c = t.Event(n),
                r = n.changedTouches ? n.changedTouches[0] : n;
            c.type = o;
            for (var e = 0, u = a.length; u > e; e++) c[a[e]] = r[a[e]];
            return c
        },
        g = function(t) {
            var o = t.originalEvent,
                e = t.changedTouches ? t.changedTouches[0] : o.changedTouches[0],
                i = Math.abs(e.pageX - n.x),
                a = Math.abs(e.pageY - n.y),
                r = Math.max(i, a);
            return Date.now() - n.start < l && f > r && !n.cancel && 1 === n.count && c.isTracking
        },
        c = {
            isEnabled: !1,
            isTracking: !1,
            enable: function() {
                return this.isEnabled ? this : (this.isEnabled = !0, t(r.body).on("touchstart" + e, this.onTouchStart).on("touchend" + e, this.onTouchEnd).on("touchcancel" + e, this.onTouchCancel), this)
            },
            disable: function() {
                return this.isEnabled ? (this.isEnabled = !1, t(r.body).off("touchstart" + e, this.onTouchStart).off("touchend" + e, this.onTouchEnd).off("touchcancel" + e, this.onTouchCancel), this) : this
            },
            onTouchStart: function(e) {
                var o = e.originalEvent.touches;
                if (n.count = o.length, !c.isTracking) {
                    c.isTracking = !0;
                    var i = o[0];
                    n.cancel = !1, n.start = Date.now(), n.$el = t(e.target), n.x = i.pageX, n.y = i.pageY
                }
            },
            onTouchEnd: function(t) {
                g(t) && n.$el.trigger(u(i, t)), c.onTouchCancel(t)
            },
            onTouchCancel: function() {
                c.isTracking = !1, n.cancel = !0
            }
        };
    if (t.event.special[i] = {
            setup: function() {
                c.enable()
            }
        }, !d) {
        var o = [],
            h = function(n) {
                var e = n.originalEvent;
                if (!(n.isTrigger || s(o, e) >= 0)) {
                    o.length > 3 && o.splice(0, o.length - 3), o.push(e);
                    var c = u(i, n);
                    t(n.target).trigger(c)
                }
            };
        t.event.special[i] = {
            setup: function() {
                t(this).on("click" + e, h)
            },
            teardown: function() {
                t(this).off("click" + e, h)
            }
        }
    }
}(document, jQuery);

/*===============================
/plugins/system/t3/base-bs3/js/off-canvas.js
================================================================================*/
;
jQuery(document).ready(function($) {
    if (/MSIE\s([\d.]+)/.test(navigator.userAgent) ? new Number(RegExp.$1) < 10 : false) {
        $('html').addClass('old-ie');
    } else if (/constructor/i.test(window.HTMLElement)) {
        $('html').addClass('safari');
    }
    var $wrapper = $('body'),
        $inner = $('.t3-wrapper'),
        $toggles = $('.off-canvas-toggle'),
        $offcanvas = $('.t3-off-canvas'),
        $close = $('.t3-off-canvas .close'),
        $btn = null,
        $nav = null,
        direction = 'left',
        $fixed = null;
    if (!$wrapper.length) return;
    $toggles.each(function() {
        var $this = $(this),
            $nav = $($this.data('nav')),
            effect = $this.data('effect'),
            direction = ($('html').attr('dir') == 'rtl' && $this.data('pos') != 'right') || ($('html').attr('dir') != 'rtl' && $this.data('pos') == 'right') ? 'right' : 'left';
        $nav.addClass(effect).addClass('off-canvas-' + direction);
        var inside_effect = ['off-canvas-effect-3', 'off-canvas-effect-16', 'off-canvas-effect-7', 'off-canvas-effect-8', 'off-canvas-effect-14'];
        if ($.inArray(effect, inside_effect) == -1) {
            $inner.before($nav);
        } else {
            $inner.prepend($nav);
        }
    });
    $toggles.on('tap', function(e) {
        stopBubble(e);
        if ($wrapper.hasClass('off-canvas-open')) {
            oc_hide(e);
            return false;
        }
        $btn = $(this);
        $nav = $($btn.data('nav'));
        $fixed = $inner.find('*').filter(function() {
            return $(this).css("position") === 'fixed';
        });
        $nav.addClass('off-canvas-current');
        direction = ($('html').attr('dir') == 'rtl' && $btn.data('pos') != 'right') || ($('html').attr('dir') != 'rtl' && $btn.data('pos') == 'right') ? 'right' : 'left';
        $offcanvas.height($(window).height());
        var events = $(window).data('events');
        if (events && events.scroll && events.scroll.length) {
            var handlers = [];
            for (var i = 0; i < events.scroll.length; i++) {
                handlers[i] = events.scroll[i].handler;
            }
            $(window).data('scroll-events', handlers);
            $(window).off('scroll');
        }
        var scrollTop = ($('html').scrollTop()) ? $('html').scrollTop() : $('body').scrollTop();
        $('html').addClass('noscroll').css('top', -scrollTop).data('top', scrollTop);
        $('.t3-off-canvas').css('top', scrollTop);
        $fixed.each(function() {
            var $this = $(this),
                $parent = $this.parent(),
                mtop = 0;
            while (!$parent.is($inner) && $parent.css("position") === 'static') $parent = $parent.parent();
            mtop = -$parent.offset().top;
            $this.css({
                'position': 'absolute',
                'margin-top': mtop
            });
        });
        $wrapper.scrollTop(scrollTop);
        $wrapper[0].className = $wrapper[0].className.replace(/\s*off\-canvas\-effect\-\d+\s*/g, ' ').trim() + ' ' + $btn.data('effect') + ' ' + 'off-canvas-' + direction;
        setTimeout(oc_show, 50);
        return false;
    });
    var oc_show = function() {
        $wrapper.addClass('off-canvas-open');
        $inner.on('click', oc_hide);
        $close.on('click', oc_hide);
        $offcanvas.on('click', stopBubble);
        if ($.browser.msie && $.browser.version < 10) {
            var p1 = {},
                p2 = {};
            p1['padding-' + direction] = $('.t3-off-canvas').width();
            p2[direction] = 0;
            $inner.animate(p1);
            $nav.animate(p2);
        }
    };
    var oc_hide = function() {
        $inner.off('tab', oc_hide);
        $close.off('click', oc_hide);
        $offcanvas.off('click', stopBubble);
        setTimeout(function() {
            $wrapper.removeClass('off-canvas-open');
        }, 100);
        setTimeout(function() {
            $wrapper.removeClass($btn.data('effect')).removeClass('off-canvas-' + direction);
            $wrapper.scrollTop(0);
            $('html').removeClass('noscroll').css('top', '');
            $('html,body').scrollTop($('html').data('top'));
            $nav.removeClass('off-canvas-current');
            $fixed.css({
                'position': '',
                'margin-top': ''
            });
            if ($(window).data('scroll-events')) {
                var handlers = $(window).data('scroll-events');
                for (var i = 0; i < handlers.length; i++) {
                    $(window).on('scroll', handlers[i]);
                }
                $(window).data('scroll-events', null);
            }
        }, 550);
        if ($('html').hasClass('old-ie')) {
            var p1 = {},
                p2 = {};
            p1['padding-' + direction] = 0;
            p2[direction] = -$('.t3-off-canvas').width();
            $inner.animate(p1);
            $nav.animate(p2);
        }
    };
    var stopBubble = function(e) {
        e.stopPropagation();
        return true;
    }
})

/*===============================
/plugins/system/t3/base-bs3/js/script.js
================================================================================*/
;
! function($) {
    if (match = navigator.userAgent.match(/MSIE ([0-9]{1,}[\.0-9]{0,})/) || navigator.userAgent.match(/Trident.*rv:([0-9]{1,}[\.0-9]{0,})/)) {
        $('html').addClass('ie' + parseInt(match[1]));
    }
    $(document).ready(function() {
        if (!window.getComputedStyle) {
            window.getComputedStyle = function(el, pseudo) {
                this.el = el;
                this.getPropertyValue = function(prop) {
                    var re = /(\-([a-z]){1})/g;
                    if (prop == 'float') prop = 'styleFloat';
                    if (re.test(prop)) {
                        prop = prop.replace(re, function() {
                            return arguments[2].toUpperCase();
                        });
                    }
                    return el.currentStyle[prop] ? el.currentStyle[prop] : null;
                }
                return this;
            }
        }
        var fromClass = 'body-data-holder',
            prop = 'content',
            $inspector = $('<div>').css('display', 'none').addClass(fromClass).appendTo($('body'));
        try {
            var attrs = window.getComputedStyle($inspector[0], ':before').getPropertyValue(prop);
            if (attrs) {
                var matches = attrs.match(/([\da-z\-]+)/gi),
                    data = {};
                if (matches && matches.length) {
                    for (var i = 0; i < matches.length; i++) {
                        data[matches[i++]] = i < matches.length ? matches[i] : null;
                    }
                }
                $('body').data(data);
            }
        } finally {
            $inspector.remove();
        }
    });
    (function() {
        $.support.t3transform = (function() {
            var style = document.createElement('div').style,
                vendors = ['t', 'webkitT', 'MozT', 'msT', 'OT'],
                transform, i = 0,
                l = vendors.length;
            for (; i < l; i++) {
                transform = vendors[i] + 'ransform';
                if (transform in style) {
                    return transform;
                }
            }
            return false;
        })();
    })();
    (function() {
        $('html').addClass('ontouchstart' in window ? 'touch' : 'no-touch');
    })();
    $(document).ready(function() {
        (function() {
            if (window.MooTools && window.MooTools.More && Element && Element.implement) {
                var mthide = Element.prototype.hide,
                    mtshow = Element.prototype.show,
                    mtslide = Element.prototype.slide;
                Element.implement({
                    show: function(args) {
                        if (arguments.callee && arguments.callee.caller && arguments.callee.caller.toString().indexOf('isPropagationStopped') !== -1) {
                            return this;
                        }
                        return $.isFunction(mtshow) && mtshow.apply(this, args);
                    },
                    hide: function() {
                        if (arguments.callee && arguments.callee.caller && arguments.callee.caller.toString().indexOf('isPropagationStopped') !== -1) {
                            return this;
                        }
                        return $.isFunction(mthide) && mthide.apply(this, arguments);
                    },
                    slide: function(args) {
                        if (arguments.callee && arguments.callee.caller && arguments.callee.caller.toString().indexOf('isPropagationStopped') !== -1) {
                            return this;
                        }
                        return $.isFunction(mtslide) && mtslide.apply(this, args);
                    }
                })
            }
        })();
        $.fn.tooltip.Constructor && $.fn.tooltip.Constructor.DEFAULTS && ($.fn.tooltip.Constructor.DEFAULTS.html = true);
        $.fn.popover.Constructor && $.fn.popover.Constructor.DEFAULTS && ($.fn.popover.Constructor.DEFAULTS.html = true);
        $.fn.tooltip.defaults && ($.fn.tooltip.defaults.html = true);
        $.fn.popover.defaults && ($.fn.popover.defaults.html = true);
        (function() {
            if (window.jomsQuery && jomsQuery.fn.collapse) {
                $('[data-toggle="collapse"]').on('click', function(e) {
                    $($(this).attr('data-target')).eq(0).collapse('toggle');
                    e.stopPropagation();
                    return false;
                });
                jomsQuery('html, body').off('touchstart.dropdown.data-api');
            }
        })();
        (function() {
            if ($.fn.chosen && $(document.documentElement).attr('dir') == 'rtl') {
                $('select').addClass('chzn-rtl');
            }
        })();
    });
    $(window).load(function() {
        if (!$(document.documentElement).hasClass('off-canvas-ready') && ($('.navbar-collapse-fixed-top').length || $('.navbar-collapse-fixed-bottom').length)) {
            var btn = $('.btn-navbar[data-toggle="collapse"]');
            if (!btn.length) {
                return;
            }
            if (btn.data('target')) {
                var nav = $(btn.data('target'));
                if (!nav.length) {
                    return;
                }
                var fixedtop = nav.closest('.navbar-collapse-fixed-top').length;
                btn.on('click', function() {
                    var wheight = (window.innerHeight || $(window).height());
                    if (!$.support.transition) {
                        nav.parent().css('height', !btn.hasClass('collapsed') && btn.data('t3-clicked') ? '' : wheight);
                        btn.data('t3-clicked', 1);
                    }
                    nav.addClass('animate').css('max-height', wheight -
                        (fixedtop ? (parseFloat(nav.css('top')) || 0) : (parseFloat(nav.css('bottom')) || 0)));
                });
                nav.on('shown hidden', function() {
                    nav.removeClass('animate');
                });
            }
        }
    });
}(jQuery);

/*===============================
/plugins/system/t3/base-bs3/js/menu.js
================================================================================*/
;;
(function($) {
    var T3Menu = function(elm, options) {
        this.$menu = $(elm);
        if (!this.$menu.length) {
            return;
        }
        this.options = $.extend({}, $.fn.t3menu.defaults, options);
        this.child_open = [];
        this.loaded = false;
        this.start();
    };
    T3Menu.prototype = {
        constructor: T3Menu,
        start: function() {
            if (this.loaded) {
                return;
            }
            this.loaded = true;
            var self = this,
                options = this.options,
                $menu = this.$menu;
            this.$items = $menu.find('li');
            this.$items.each(function(idx, li) {
                var $item = $(this),
                    $child = $item.children('.dropdown-menu'),
                    $link = $item.children('a'),
                    item = {
                        $item: $item,
                        child: $child.length,
                        link: $link.length,
                        clickable: !($link.length && $child.length),
                        mega: $item.hasClass('mega'),
                        status: 'close',
                        timer: null,
                        atimer: null
                    };
                $item.data('t3menu.item', item);
                if ($child.length && !options.hover) {
                    $item.on('click', function(e) {
                        e.stopPropagation();
                        if ($item.hasClass('group')) {
                            return;
                        }
                        if (item.status == 'close') {
                            e.preventDefault();
                            self.show(item);
                        }
                    });
                } else {
                    $item.on('click', function(e) {
                        e.stopPropagation()
                    });
                }
                if (options.hover) {
                    $item.on('mouseover', function(e) {
                        if ($item.hasClass('group')) return;
                        var $target = $(e.target);
                        if ($target.data('show-processed')) return;
                        $target.data('show-processed', true);
                        setTimeout(function() {
                            $target.data('show-processed', false);
                        }, 10);
                        self.show(item);
                    }).on('mouseleave', function(e) {
                        if ($item.hasClass('group')) return;
                        var $target = $(e.target);
                        if ($target.data('hide-processed')) return;
                        $target.data('hide-processed', true);
                        setTimeout(function() {
                            $target.data('hide-processed', false);
                        }, 10);
                        self.hide(item);
                    });
                    if ($link.length && $child.length) {
                        $link.on('click', function(e) {
                            return item.clickable;
                        });
                    }
                }
            });
            $(document.body).on('tap hideall.t3menu', function(e) {
                clearTimeout(self.timer);
                self.timer = setTimeout($.proxy(self.hide_alls, self), e.type == 'tap' ? 500 : self.options.hidedelay);
            });
        },
        show: function(item) {
            if ($.inArray(item, this.child_open) < this.child_open.length - 1) {
                this.hide_others(item);
            }
            $(document.body).trigger('hideall.t3menu', [this]);
            clearTimeout(this.timer);
            clearTimeout(item.timer);
            clearTimeout(item.ftimer);
            clearTimeout(item.ctimer);
            if (item.status != 'open' || !item.$item.hasClass('open') || !this.child_open.length) {
                if (item.mega) {
                    clearTimeout(item.astimer);
                    clearTimeout(item.atimer);
                    this.position(item.$item);
                    item.astimer = setTimeout(function() {
                        item.$item.addClass('animating')
                    }, 10);
                    item.atimer = setTimeout(function() {
                        item.$item.removeClass('animating')
                    }, this.options.duration + 50);
                    item.timer = setTimeout(function() {
                        item.$item.addClass('open')
                    }, 100);
                } else {
                    item.$item.addClass('open');
                }
                item.status = 'open';
                if (item.child && $.inArray(item, this.child_open) == -1) {
                    this.child_open.push(item);
                }
            }
            item.ctimer = setTimeout($.proxy(this.clickable, this, item), 300);
        },
        hide: function(item) {
            clearTimeout(this.timer);
            clearTimeout(item.timer);
            clearTimeout(item.astimer);
            clearTimeout(item.atimer);
            clearTimeout(item.ftimer);
            if (item.mega) {
                item.$item.addClass('animating');
                item.atimer = setTimeout(function() {
                    item.$item.removeClass('animating')
                }, this.options.duration);
                item.timer = setTimeout(function() {
                    item.$item.removeClass('open')
                }, 100);
            } else {
                item.$item.removeClass('open');
            }
            item.status = 'close';
            for (var i = this.child_open.length; i--;) {
                if (this.child_open[i] === item) {
                    this.child_open.splice(i, 1);
                }
            }
            item.ftimer = setTimeout($.proxy(this.hidden, this, item), this.options.duration);
            this.timer = setTimeout($.proxy(this.hide_alls, this), this.options.hidedelay);
        },
        hidden: function(item) {
            if (item.status == 'close') {
                item.clickable = false;
            }
        },
        hide_others: function(item) {
            var self = this;
            $.each(this.child_open.slice(), function(idx, open) {
                if (!item || (open != item && !open.$item.has(item.$item).length)) {
                    self.hide(open);
                }
            });
        },
        hide_alls: function(e, inst) {
            if (!e || e.type == 'tap' || (e.type == 'hideall' && this != inst)) {
                var self = this;
                $.each(this.child_open.slice(), function(idx, item) {
                    item && self.hide(item);
                });
            }
        },
        clickable: function(item) {
            item.clickable = true;
        },
        position: function($item) {
            var sub = $item.children('.mega-dropdown-menu'),
                is_show = sub.is(':visible');
            if (!is_show) {
                sub.show();
            }
            var offset = $item.offset(),
                width = $item.outerWidth(),
                screen_width = $(window).width() - this.options.sb_width,
                sub_width = sub.outerWidth(),
                level = $item.data('level');
            if (!is_show) {
                sub.css('display', '');
            }
            sub.css({
                left: '',
                right: ''
            });
            if (level == 1) {
                var align = $item.data('alignsub'),
                    align_offset = 0,
                    align_delta = 0,
                    align_trans = 0;
                if (align == 'justify') {
                    return;
                }
                if (!align) {
                    align = 'left';
                }
                if (align == 'center') {
                    align_offset = offset.left + (width / 2);
                    if (!$.support.t3transform) {
                        align_trans = -sub_width / 2;
                        sub.css(this.options.rtl ? 'right' : 'left', align_trans + width / 2);
                    }
                } else {
                    align_offset = offset.left + ((align == 'left' && this.options.rtl || align == 'right' && !this.options.rtl) ? width : 0);
                }
                if (this.options.rtl) {
                    if (align == 'right') {
                        if (align_offset + sub_width > screen_width) {
                            align_delta = screen_width - align_offset - sub_width;
                            sub.css('left', align_delta);
                            if (screen_width < sub_width) {
                                sub.css('left', align_delta + sub_width - screen_width);
                            }
                        }
                    } else {
                        if (align_offset < (align == 'center' ? sub_width / 2 : sub_width)) {
                            align_delta = align_offset - (align == 'center' ? sub_width / 2 : sub_width);
                            sub.css('right', align_delta + align_trans);
                        }
                        if (align_offset + (align == 'center' ? sub_width / 2 : 0) - align_delta > screen_width) {
                            sub.css('right', align_offset + (align == 'center' ? (sub_width + width) / 2 : 0) + align_trans - screen_width);
                        }
                    }
                } else {
                    if (align == 'right') {
                        if (align_offset < sub_width) {
                            align_delta = align_offset - sub_width;
                            sub.css('right', align_delta);
                            if (sub_width > screen_width) {
                                sub.css('right', sub_width - screen_width + align_delta);
                            }
                        }
                    } else {
                        if (align_offset + (align == 'center' ? sub_width / 2 : sub_width) > screen_width) {
                            align_delta = screen_width - align_offset - (align == 'center' ? sub_width / 2 : sub_width);
                            sub.css('left', align_delta + align_trans);
                        }
                        if (align_offset - (align == 'center' ? sub_width / 2 : 0) + align_delta < 0) {
                            sub.css('left', (align == 'center' ? (sub_width + width) / 2 : 0) + align_trans - align_offset);
                        }
                    }
                }
            } else {
                if (this.options.rtl) {
                    if ($item.closest('.mega-dropdown-menu').parent().hasClass('mega-align-right')) {
                        if (offset.left + width + sub_width > screen_width) {
                            $item.removeClass('mega-align-right');
                            if (offset.left - sub_width < 0) {
                                sub.css('right', offset.left + width - sub_width);
                            }
                        }
                    } else {
                        if (offset.left - sub_width < 0) {
                            $item.removeClass('mega-align-left').addClass('mega-align-right');
                            if (offset.left + width + sub_width > screen_width) {
                                sub.css('left', screen_width - offset.left - sub_width);
                            }
                        }
                    }
                } else {
                    if ($item.closest('.mega-dropdown-menu').parent().hasClass('mega-align-right')) {
                        if (offset.left - sub_width < 0) {
                            $item.removeClass('mega-align-right');
                            if (offset.left + width + sub_width > screen_width) {
                                sub.css('left', screen_width - offset.left - sub_width);
                            }
                        }
                    } else {
                        if (offset.left + width + sub_width > screen_width) {
                            $item.removeClass('mega-align-left').addClass('mega-align-right');
                            if (offset.left - sub_width < 0) {
                                sub.css('right', offset.left + width - sub_width);
                            }
                        }
                    }
                }
            }
        }
    };
    $.fn.t3menu = function(option) {
        return this.each(function() {
            var $this = $(this),
                data = $this.data('megamenu'),
                options = typeof option == 'object' && option;
            if (!data) {
                $this.data('megamenu', (data = new T3Menu(this, options)));
            } else {
                if (typeof option == 'string' && data[option]) {
                    data[option]()
                }
            }
        })
    };
    $.fn.t3menu.defaults = {
        duration: 400,
        timeout: 100,
        hidedelay: 200,
        hover: true,
        sb_width: 20
    };
    $(document).ready(function() {
        var mm_duration = $('.t3-megamenu').data('duration') || 0;
        if (mm_duration) {
            $('<style type="text/css">' + '.t3-megamenu.animate .animating > .mega-dropdown-menu,' + '.t3-megamenu.animate.slide .animating > .mega-dropdown-menu > div {' + 'transition-duration: ' + mm_duration + 'ms !important;' + '-webkit-transition-duration: ' + mm_duration + 'ms !important;' + '}' + '</style>').appendTo('head');
        }
        var mm_timeout = mm_duration ? 100 + mm_duration : 500,
            mm_rtl = $(document.documentElement).attr('dir') == 'rtl',
            mm_trigger = $(document.documentElement).hasClass('mm-hover'),
            sb_width = (function() {
                var parent = $('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo('body'),
                    child = parent.children(),
                    width = child.innerWidth() - child.height(100).innerWidth();
                parent.remove();
                return width;
            })();
        if (!$.support.transition) {
            $('.t3-megamenu').removeClass('animate');
            mm_timeout = 100;
        }
        $('.nav').has('.dropdown-menu').t3menu({
            duration: mm_duration,
            timeout: mm_timeout,
            rtl: mm_rtl,
            sb_width: sb_width,
            hover: mm_trigger
        });
        $(window).load(function() {
            $('.nav').has('.dropdown-menu').t3menu({
                duration: mm_duration,
                timeout: mm_timeout,
                rtl: mm_rtl,
                sb_width: sb_width,
                hover: mm_trigger
            });
        });
    });
})(jQuery);

/*===============================
/templates/ja_playmag/js/jquery.easing.1.3.js
================================================================================*/
;
jQuery.easing['jswing'] = jQuery.easing['swing'];
jQuery.extend(jQuery.easing, {
    def: 'easeOutQuad',
    swing: function(x, t, b, c, d) {
        return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
    },
    easeInQuad: function(x, t, b, c, d) {
        return c * (t /= d) * t + b;
    },
    easeOutQuad: function(x, t, b, c, d) {
        return -c * (t /= d) * (t - 2) + b;
    },
    easeInOutQuad: function(x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t + b;
        return -c / 2 * ((--t) * (t - 2) - 1) + b;
    },
    easeInCubic: function(x, t, b, c, d) {
        return c * (t /= d) * t * t + b;
    },
    easeOutCubic: function(x, t, b, c, d) {
        return c * ((t = t / d - 1) * t * t + 1) + b;
    },
    easeInOutCubic: function(x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t + b;
        return c / 2 * ((t -= 2) * t * t + 2) + b;
    },
    easeInQuart: function(x, t, b, c, d) {
        return c * (t /= d) * t * t * t + b;
    },
    easeOutQuart: function(x, t, b, c, d) {
        return -c * ((t = t / d - 1) * t * t * t - 1) + b;
    },
    easeInOutQuart: function(x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
        return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
    },
    easeInQuint: function(x, t, b, c, d) {
        return c * (t /= d) * t * t * t * t + b;
    },
    easeOutQuint: function(x, t, b, c, d) {
        return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
    },
    easeInOutQuint: function(x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t * t * t + b;
        return c / 2 * ((t -= 2) * t * t * t * t + 2) + b;
    },
    easeInSine: function(x, t, b, c, d) {
        return -c * Math.cos(t / d * (Math.PI / 2)) + c + b;
    },
    easeOutSine: function(x, t, b, c, d) {
        return c * Math.sin(t / d * (Math.PI / 2)) + b;
    },
    easeInOutSine: function(x, t, b, c, d) {
        return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b;
    },
    easeInExpo: function(x, t, b, c, d) {
        return (t == 0) ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
    },
    easeOutExpo: function(x, t, b, c, d) {
        return (t == d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
    },
    easeInOutExpo: function(x, t, b, c, d) {
        if (t == 0) return b;
        if (t == d) return b + c;
        if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
        return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
    },
    easeInCirc: function(x, t, b, c, d) {
        return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b;
    },
    easeOutCirc: function(x, t, b, c, d) {
        return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;
    },
    easeInOutCirc: function(x, t, b, c, d) {
        if ((t /= d / 2) < 1) return -c / 2 * (Math.sqrt(1 - t * t) - 1) + b;
        return c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b;
    },
    easeInElastic: function(x, t, b, c, d) {
        var s = 1.70158;
        var p = 0;
        var a = c;
        if (t == 0) return b;
        if ((t /= d) == 1) return b + c;
        if (!p) p = d * .3;
        if (a < Math.abs(c)) {
            a = c;
            var s = p / 4;
        } else var s = p / (2 * Math.PI) * Math.asin(c / a);
        return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
    },
    easeOutElastic: function(x, t, b, c, d) {
        var s = 1.70158;
        var p = 0;
        var a = c;
        if (t == 0) return b;
        if ((t /= d) == 1) return b + c;
        if (!p) p = d * .3;
        if (a < Math.abs(c)) {
            a = c;
            var s = p / 4;
        } else var s = p / (2 * Math.PI) * Math.asin(c / a);
        return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b;
    },
    easeInOutElastic: function(x, t, b, c, d) {
        var s = 1.70158;
        var p = 0;
        var a = c;
        if (t == 0) return b;
        if ((t /= d / 2) == 2) return b + c;
        if (!p) p = d * (.3 * 1.5);
        if (a < Math.abs(c)) {
            a = c;
            var s = p / 4;
        } else var s = p / (2 * Math.PI) * Math.asin(c / a);
        if (t < 1) return -.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
        return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p) * .5 + c + b;
    },
    easeInBack: function(x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        return c * (t /= d) * t * ((s + 1) * t - s) + b;
    },
    easeOutBack: function(x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
    },
    easeInOutBack: function(x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        if ((t /= d / 2) < 1) return c / 2 * (t * t * (((s *= (1.525)) + 1) * t - s)) + b;
        return c / 2 * ((t -= 2) * t * (((s *= (1.525)) + 1) * t + s) + 2) + b;
    },
    easeInBounce: function(x, t, b, c, d) {
        return c - jQuery.easing.easeOutBounce(x, d - t, 0, c, d) + b;
    },
    easeOutBounce: function(x, t, b, c, d) {
        if ((t /= d) < (1 / 2.75)) {
            return c * (7.5625 * t * t) + b;
        } else if (t < (2 / 2.75)) {
            return c * (7.5625 * (t -= (1.5 / 2.75)) * t + .75) + b;
        } else if (t < (2.5 / 2.75)) {
            return c * (7.5625 * (t -= (2.25 / 2.75)) * t + .9375) + b;
        } else {
            return c * (7.5625 * (t -= (2.625 / 2.75)) * t + .984375) + b;
        }
    },
    easeInOutBounce: function(x, t, b, c, d) {
        if (t < d / 2) return jQuery.easing.easeInBounce(x, t * 2, 0, c, d) * .5 + b;
        return jQuery.easing.easeOutBounce(x, t * 2 - d, 0, c, d) * .5 + c * .5 + b;
    }
});

/*===============================
/templates/ja_playmag/js/script.js
================================================================================*/
;
(function($) {
    $(document).ready(function() {
        if ($('.nav-search').length > 0) {
            if ($('.nav-search').find('.nav-child.dropdown-menu').length > 0) {
                $('.nav-search').find('.nav-child.dropdown-menu').css('width', $('#t3-header').find('.container').width() + parseInt($('#t3-header').find('.container').css('padding-left')) + parseInt($('#t3-header').find('.container').css('padding-right')));
            }
            $(window).bind('orientationchange resize', function() {
                if ($('.nav-search').find('.nav-child.dropdown-menu').length > 0) {
                    $('.nav-search').find('.nav-child.dropdown-menu').css('width', $('#t3-header').find('.container').width() + parseInt($('#t3-header').find('.container').css('padding-left')) + parseInt($('#t3-header').find('.container').css('padding-right')));
                }
            });
        }
        if ($('.tpl_change_layout').length > 0) {
            var elfirst = $('.tpl_change_layout').parent().parent();
            $('.tpl_change_layout span').on('click', function() {
                elfirst.removeClass('grid small-list big-list');
                $('.tpl_change_layout span').removeClass('active');
                elfirst.addClass($(this).html());
                $(this).addClass('active');
            });
        }
        $('.rating-score').click(function(event) {
            event.preventDefault();
            var full_url = $(this).attr('href');
            var parts = full_url.split("#");
            var trgt = parts[1];
            var target_offset = $("." + trgt).offset();
            var target_top = target_offset.top;
            $('html, body').animate({
                scrollTop: target_top - 30
            }, 400, 'easeInSine');
        });
        $(".ja-healineswrap em, .t3-bottomslider .module-title span").html(function(index, old) {
            return old.replace(/(\b\w+)$/, '<span class="last-word">$1</span>');
        });
        $('iframe').each(function() {
            var url = $(this).prop("src").toString();
            if (url.indexOf('youtube.com') > 0) {
                if (url.indexOf('wmode=transparent') < 0) {
                    if (url.indexOf('?') > 0) {
                        $(this).attr('src', url + '&wmode=transparent');
                    } else {
                        $(this).attr('src', url + '?wmode=transparent');
                    }
                }
            }
        });
    });
})(jQuery);
(function($) {
    var scrollLastPos = 0,
        scrollDir = 0,
        scrollTimeout = 0;
    $(window).on('scroll', function(e) {
        var st = $(this).scrollTop();
        if (st < 80) {
            if (scrollDir != 0) {
                scrollDir = 0;
            }
        } else if (st > scrollLastPos) {
            if (scrollDir != 1) {
                scrollDir = 1;
            }
        } else if (st < scrollLastPos) {
            if (scrollDir != -1) {
                scrollDir = -1;
            }
        }
        scrollLastPos = st;
    });
    $('.ja-header').on('hover', function() {
        $('html').removeClass('scrollDown scrollUp').addClass('hover');
        scrollDir = 0;
    })
    scrollToggle = function() {
        $('html').removeClass('hover');
        if (scrollDir == 1) {
            $('html').addClass('scrollDown').removeClass('scrollUp');
        } else if (scrollDir == -1) {
            $('html').addClass('scrollUp').removeClass('scrollDown');
        } else {
            $('html').removeClass('scrollUp scrollDown');
        }
        $('html').addClass('animating');
        setTimeout(function() {
            $('html').removeClass('animating');
        }, 1000);
    }
})(jQuery);

/*===============================
/templates/ja_playmag/js/jquery.infinitescroll.min.js
================================================================================*/
;
(function(e, t, n) {
    "use strict";
    t.infinitescroll = function(n, r, i) {
        this.element = t(i);
        if (!this._create(n, r)) {
            this.failed = true
        }
    };
    t.infinitescroll.defaults = {
        loading: {
            finished: n,
            finishedMsg: "<em>Congratulations, you've reached the end of the internet.</em>",
            img: "data:image/gif;base64,R0lGODlh3AATAPQeAPDy+MnQ6LW/4N3h8MzT6rjC4sTM5r/I5NHX7N7j8c7U6tvg8OLl8uXo9Ojr9b3G5MfP6Ovu9tPZ7PT1+vX2+tbb7vf4+8/W69jd7rC73vn5/O/x+K243ai02////wAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQECgD/ACwAAAAA3AATAAAF/6AnjmRpnmiqrmzrvnAsz3Rt33iu73zv/8CgcEj0BAScpHLJbDqf0Kh0Sq1ar9isdioItAKGw+MAKYMFhbF63CW438f0mg1R2O8EuXj/aOPtaHx7fn96goR4hmuId4qDdX95c4+RBIGCB4yAjpmQhZN0YGYGXitdZBIVGAsLoq4BBKQDswm1CQRkcG6ytrYKubq8vbfAcMK9v7q7EMO1ycrHvsW6zcTKsczNz8HZw9vG3cjTsMIYqQkCLBwHCgsMDQ4RDAYIqfYSFxDxEfz88/X38Onr16+Bp4ADCco7eC8hQYMAEe57yNCew4IVBU7EGNDiRn8Z831cGLHhSIgdFf9chIeBg7oA7gjaWUWTVQAGE3LqBDCTlc9WOHfm7PkTqNCh54rePDqB6M+lR536hCpUqs2gVZM+xbrTqtGoWqdy1emValeXKzggYBBB5y1acFNZmEvXAoN2cGfJrTv3bl69Ffj2xZt3L1+/fw3XRVw4sGDGcR0fJhxZsF3KtBTThZxZ8mLMgC3fRatCbYMNFCzwLEqLgE4NsDWs/tvqdezZf13Hvk2A9Szdu2X3pg18N+68xXn7rh1c+PLksI/Dhe6cuO3ow3NfV92bdArTqC2Ebd3A8vjf5QWfH6Bg7Nz17c2fj69+fnq+8N2Lty+fuP78/eV2X13neIcCeBRwxorbZrA1ANoCDGrgoG8RTshahQ9iSKEEzUmYIYfNWViUhheCGJyIP5E4oom7WWjgCeBFAJNv1DVV01MAdJhhjdkplWNzO/5oXI846njjVEIqR2OS2B1pE5PVscajkxhMycqLJghQSwT40PgfAl4GqNSXYdZXJn5gSkmmmmJu1aZYb14V51do+pTOCmA40AqVCIhG5IJ9PvYnhIFOxmdqhpaI6GeHCtpooisuutmg+Eg62KOMKuqoTaXgicQWoIYq6qiklmoqFV0UoeqqrLbq6quwxirrrLTWauutJ4QAACH5BAUKABwALAcABADOAAsAAAX/IPd0D2dyRCoUp/k8gpHOKtseR9yiSmGbuBykler9XLAhkbDavXTL5k2oqFqNOxzUZPU5YYZd1XsD72rZpBjbeh52mSNnMSC8lwblKZGwi+0QfIJ8CncnCoCDgoVnBHmKfByGJimPkIwtiAeBkH6ZHJaKmCeVnKKTHIihg5KNq4uoqmEtcRUtEREMBggtEr4QDrjCuRC8h7/BwxENeicSF8DKy82pyNLMOxzWygzFmdvD2L3P0dze4+Xh1Arkyepi7dfFvvTtLQkZBC0T/FX3CRgCMOBHsJ+EHYQY7OinAGECgQsB+Lu3AOK+CewcWjwxQeJBihtNGHSoQOE+iQ3//4XkwBBhRZMcUS6YSXOAwIL8PGqEaSJCiYt9SNoCmnJPAgUVLChdaoFBURN8MAzl2PQphwQLfDFd6lTowglHve6rKpbjhK7/pG5VinZP1qkiz1rl4+tr2LRwWU64cFEihwEtZgbgR1UiHaMVvxpOSwBA37kzGz9e8G+B5MIEKLutOGEsAH2ATQwYfTmuX8aETWdGPZmiZcccNSzeTCA1Sw0bdiitC7LBWgu8jQr8HRzqgpK6gX88QbrB14z/kF+ELpwB8eVQj/JkqdylAudji/+ts3039vEEfK8Vz2dlvxZKG0CmbkKDBvllRd6fCzDvBLKBDSCeffhRJEFebFk1k/Mv9jVIoIJZSeBggwUaNeB+Qk34IE0cXlihcfRxkOAJFFhwGmKlmWDiakZhUJtnLBpnWWcnKaAZcxI0piFGGLBm1mc90kajSCveeBVWKeYEoU2wqeaQi0PetoE+rr14EpVC7oAbAUHqhYExbn2XHHsVqbcVew9tx8+XJKk5AZsqqdlddGpqAKdbAYBn1pcczmSTdWvdmZ17c1b3FZ99vnTdCRFM8OEcAhLwm1NdXnWcBBSMRWmfkWZqVlsmLIiAp/o1gGV2vpS4lalGYsUOqXrddcKCmK61aZ8SjEpUpVFVoCpTj4r661Km7kBHjrDyc1RAIQAAIfkEBQoAGwAsBwAEAM4ACwAABf/gtmUCd4goQQgFKj6PYKi0yrrbc8i4ohQt12EHcal+MNSQiCP8gigdz7iCioaCIvUmZLp8QBzW0EN2vSlCuDtFKaq4RyHzQLEKZNdiQDhRDVooCwkbfm59EAmKi4SGIm+AjIsKjhsqB4mSjT2IOIOUnICeCaB/mZKFNTSRmqVpmJqklSqskq6PfYYCDwYHDC4REQwGCBLGxxIQDsHMwhAIX8bKzcENgSLGF9PU1j3Sy9zX2NrgzQziChLk1BHWxcjf7N046tvN82715czn9Pryz6Ilc4ACj4EBOCZM8KEnAYYADBRKnACAYUMFv1wotIhCEcaJCisqwJFgAUSQGyX/kCSVUUTIdKMwJlyo0oXHlhskwrTJciZHEXsgaqS4s6PJiCAr1uzYU8kBBSgnWFqpoMJMUjGtDmUwkmfVmVypakWhEKvXsS4nhLW5wNjVroJIoc05wSzTr0PtiigpYe4EC2vj4iWrFu5euWIMRBhacaVJhYQBEFjA9jHjyQ0xEABwGceGAZYjY0YBOrRLCxUp29QM+bRkx5s7ZyYgVbTqwwti2ybJ+vLtDYpycyZbYOlptxdx0kV+V7lC5iJAyyRrwYKxAdiz82ng0/jnAdMJFz0cPi104Ec1Vj9/M6F173vKL/feXv156dw11tlqeMMnv4V5Ap53GmjQQH97nFfg+IFiucfgRX5Z8KAgbUlQ4IULIlghhhdOSB6AgX0IVn8eReghen3NRIBsRgnH4l4LuEidZBjwRpt6NM5WGwoW0KSjCwX6yJSMab2GwwAPDXfaBCtWpluRTQqC5JM5oUZAjUNS+VeOLWpJEQ7VYQANW0INJSZVDFSnZphjSikfmzE5N4EEbQI1QJmnWXCmHulRp2edwDXF43txukenJwvI9xyg9Q26Z3MzGUcBYFEChZh6DVTq34AU8Iflh51Sd+CnKFYQ6mmZkhqfBKfSxZWqA9DZanWjxmhrWwi0qtCrt/43K6WqVjjpmhIqgEGvculaGKklKstAACEAACH5BAUKABwALAcABADOAAsAAAX/ICdyQmaMYyAUqPgIBiHPxNpy79kqRXH8wAPsRmDdXpAWgWdEIYm2llCHqjVHU+jjJkwqBTecwItShMXkEfNWSh8e1NGAcLgpDGlRgk7EJ/6Ae3VKfoF/fDuFhohVeDeCfXkcCQqDVQcQhn+VNDOYmpSWaoqBlUSfmowjEA+iEAEGDRGztAwGCDcXEA60tXEiCrq8vREMEBLIyRLCxMWSHMzExnbRvQ2Sy7vN0zvVtNfU2tLY3rPgLdnDvca4VQS/Cpk3ABwSLQkYAQwT/P309vcI7OvXr94jBQMJ/nskkGA/BQBRLNDncAIAiDcG6LsxAWOLiQzmeURBKWSLCQbv/1F0eDGinJUKR47YY1IEgQASKk7Yc7ACRwZm7mHweRJoz59BJUogisKCUaFMR0x4SlJBVBFTk8pZivTR0K73rN5wqlXEAq5Fy3IYgHbEzQ0nLy4QSoCjXLoom96VOJEeCosK5n4kkFfqXjl94wa+l1gvAcGICbewAOAxY8l/Ky/QhAGz4cUkGxu2HNozhwMGBnCUqUdBg9UuW9eUynqSwLHIBujePef1ZGQZXcM+OFuEBeBhi3OYgLyqcuaxbT9vLkf4SeqyWxSQpKGB2gQpm1KdWbu72rPRzR9Ne2Nu9Kzr/1Jqj0yD/fvqP4aXOt5sW/5qsXXVcv1Nsp8IBUAmgswGF3llGgeU1YVXXKTN1FlhWFXW3gIE+DVChApysACHHo7Q4A35lLichh+ROBmLKAzgYmYEYDAhCgxKGOOMn4WR4kkDaoBBOxJtdNKQxFmg5JIWIBnQc07GaORfUY4AEkdV6jHlCEISSZ5yTXpp1pbGZbkWmcuZmQCaE6iJ0FhjMaDjTMsgZaNEHFRAQVp3bqXnZED1qYcECOz5V6BhSWCoVJQIKuKQi2KFKEkEFAqoAo7uYSmO3jk61wUUMKmknJ4SGimBmAa0qVQBhAAAIfkEBQoAGwAsBwAEAM4ACwAABf/gJm5FmRlEqhJC+bywgK5pO4rHI0D3pii22+Mg6/0Ej96weCMAk7cDkXf7lZTTnrMl7eaYoy10JN0ZFdco0XAuvKI6qkgVFJXYNwjkIBcNBgR8TQoGfRsJCRuCYYQQiI+ICosiCoGOkIiKfSl8mJkHZ4U9kZMbKaI3pKGXmJKrngmug4WwkhA0lrCBWgYFCCMQFwoQDRHGxwwGCBLMzRLEx8iGzMMO0cYNeCMKzBDW19lnF9DXDIY/48Xg093f0Q3s1dcR8OLe8+Y91OTv5wrj7o7B+7VNQqABIoRVCMBggsOHE36kSoCBIcSH3EbFangxogJYFi8CkJhqQciLJEf/LDDJEeJIBT0GsOwYUYJGBS0fjpQAMidGmyVP6sx4Y6VQhzs9VUwkwqaCCh0tmKoFtSMDmBOf9phg4SrVrROuasRQAaxXpVUhdsU6IsECZlvX3kwLUWzRt0BHOLTbNlbZG3vZinArge5Dvn7wbqtQkSYAAgtKmnSsYKVKo2AfW048uaPmG386i4Q8EQMBAIAnfB7xBxBqvapJ9zX9WgRS2YMpnvYMGdPK3aMjt/3dUcNI4blpj7iwkMFWDXDvSmgAlijrt9RTR78+PS6z1uAJZIe93Q8g5zcsWCi/4Y+C8bah5zUv3vv89uft30QP23punGCx5954oBBwnwYaNCDY/wYrsYeggnM9B2Fpf8GG2CEUVWhbWAtGouEGDy7Y4IEJVrbSiXghqGKIo7z1IVcXIkKWWR361QOLWWnIhwERpLaaCCee5iMBGJQmJGyPFTnbkfHVZGRtIGrg5HALEJAZbu39BuUEUmq1JJQIPtZilY5hGeSWsSk52G9XqsmgljdIcABytq13HyIM6RcUA+r1qZ4EBF3WHWB29tBgAzRhEGhig8KmqKFv8SeCeo+mgsF7YFXa1qWSbkDpom/mqR1PmHCqJ3fwNRVXjC7S6CZhFVCQ2lWvZiirhQq42SACt25IK2hv8TprriUV1usGgeka7LFcNmCldMLi6qZMgFLgpw16Cipb7bC1knXsBiEAACH5BAUKABsALAcABADOAAsAAAX/4FZsJPkUmUGsLCEUTywXglFuSg7fW1xAvNWLF6sFFcPb42C8EZCj24EJdCp2yoegWsolS0Uu6fmamg8n8YYcLU2bXSiRaXMGvqV6/KAeJAh8VgZqCX+BexCFioWAYgqNi4qAR4ORhRuHY408jAeUhAmYYiuVlpiflqGZa5CWkzc5fKmbbhIpsAoQDRG8vQwQCBLCwxK6vb5qwhfGxxENahvCEA7NzskSy7vNzzzK09W/PNHF1NvX2dXcN8K55cfh69Luveol3vO8zwi4Yhj+AQwmCBw4IYclDAAJDlQggVOChAoLKkgFkSCAHDwWLKhIEOONARsDKryogFPIiAUb/95gJNIiw4wnI778GFPhzBKFOAq8qLJEhQpiNArjMcHCmlTCUDIouTKBhApELSxFWiGiVKY4E2CAekPgUphDu0742nRrVLJZnyrFSqKQ2ohoSYAMW6IoDpNJ4bLdILTnAj8KUF7UeENjAKuDyxIgOuGiOI0EBBMgLNew5AUrDTMGsFixwBIaNCQuAXJB57qNJ2OWm2Aj4skwCQCIyNkhhtMkdsIuodE0AN4LJDRgfLPtn5YDLdBlraAByuUbBgxQwICxMOnYpVOPej074OFdlfc0TqC62OIbcppHjV4o+LrieWhfT8JC/I/T6W8oCl29vQ0XjLdBaA3s1RcPBO7lFvpX8BVoG4O5jTXRQRDuJ6FDTzEWF1/BCZhgbyAKE9qICYLloQYOFtahVRsWYlZ4KQJHlwHS/IYaZ6sZd9tmu5HQm2xi1UaTbzxYwJk/wBF5g5EEYOBZeEfGZmNdFyFZmZIR4jikbLThlh5kUUVJGmRT7sekkziRWUIACABk3T4qCsedgO4xhgGcY7q5pHJ4klBBTQRJ0CeHcoYHHUh6wgfdn9uJdSdMiebGJ0zUPTcoS286FCkrZxnYoYYKWLkBowhQoBeaOlZAgVhLidrXqg2GiqpQpZ4apwSwRtjqrB3muoF9BboaXKmshlqWqsWiGt2wphJkQbAU5hoCACH5BAUKABsALAcABADOAAsAAAX/oGFw2WZuT5oZROsSQnGaKjRvilI893MItlNOJ5v5gDcFrHhKIWcEYu/xFEqNv6B1N62aclysF7fsZYe5aOx2yL5aAUGSaT1oTYMBwQ5VGCAJgYIJCnx1gIOBhXdwiIl7d0p2iYGQUAQBjoOFSQR/lIQHnZ+Ue6OagqYzSqSJi5eTpTxGcjcSChANEbu8DBAIEsHBChe5vL13G7fFuscRDcnKuM3H0La3EA7Oz8kKEsXazr7Cw9/Gztar5uHHvte47MjktznZ2w0G1+D3BgirAqJmJMAQgMGEgwgn5Ei0gKDBhBMALGRYEOJBb5QcWlQo4cbAihZz3GgIMqFEBSM1/4ZEOWPAgpIIJXYU+PIhRG8ja1qU6VHlzZknJNQ6UanCjQkWCIGSUGEjAwVLjc44+DTqUQtPPS5gejUrTa5TJ3g9sWCr1BNUWZI161StiQUDmLYdGfesibQ3XMq1OPYthrwuA2yU2LBs2cBHIypYQPPlYAKFD5cVvNPtW8eVGbdcQADATsiNO4cFAPkvHpedPzc8kUcPgNGgZ5RNDZG05reoE9s2vSEP79MEGiQGy1qP8LA4ZcdtsJE48ONoLTBtTV0B9LsTnPceoIDBDQvS7W7vfjVY3q3eZ4A339J4eaAmKqU/sV58HvJh2RcnIBsDUw0ABqhBA5aV5V9XUFGiHfVeAiWwoFgJJrIXRH1tEMiDFV4oHoAEGlaWhgIGSGBO2nFomYY3mKjVglidaNYJGJDkWW2xxTfbjCbVaOGNqoX2GloR8ZeTaECS9pthRGJH2g0b3Agbk6hNANtteHD2GJUucfajCQBy5OOTQ25ZgUPvaVVQmbKh9510/qQpwXx3SQdfk8tZJOd5b6JJFplT3ZnmmX3qd5l1eg5q00HrtUkUn0AKaiGjClSAgKLYZcgWXwocGRcCFGCKwSB6ceqphwmYRUFYT/1WKlOdUpipmxW0mlCqHjYkAaeoZlqrqZ4qd+upQKaapn/AmgAegZ8KUtYtFAQQAgAh+QQFCgAbACwHAAQAzgALAAAF/+C2PUcmiCiZGUTrEkKBis8jQEquKwU5HyXIbEPgyX7BYa5wTNmEMwWsSXsqFbEh8DYs9mrgGjdK6GkPY5GOeU6ryz7UFopSQEzygOGhJBjoIgMDBAcBM0V/CYqLCQqFOwobiYyKjn2TlI6GKC2YjJZknouaZAcQlJUHl6eooJwKooobqoewrJSEmyKdt59NhRKFMxLEEA4RyMkMEAjDEhfGycqAG8TQx9IRDRDE3d3R2ctD1RLg0ttKEnbY5wZD3+zJ6M7X2RHi9Oby7u/r9g38UFjTh2xZJBEBMDAboogAgwkQI07IMUORwocSJwCgWDFBAIwZOaJIsOBjRogKJP8wTODw5ESVHVtm3AhzpEeQElOuNDlTZ0ycEUWKWFASqEahGwYUPbnxoAgEdlYSqDBkgoUNClAlIHbSAoOsqCRQnQHxq1axVb06FWFxLIqyaze0Tft1JVqyE+pWXMD1pF6bYl3+HTqAWNW8cRUFzmih0ZAAB2oGKukSAAGGRHWJgLiR6AylBLpuHKKUMlMCngMpDSAa9QIUggZVVvDaJobLeC3XZpvgNgCmtPcuwP3WgmXSq4do0DC6o2/guzcseECtUoO0hmcsGKDgOt7ssBd07wqesAIGZC1YIBa7PQHvb1+SFo+++HrJSQfB33xfav3i5eX3Hnb4CTJgegEq8tH/YQEOcIJzbm2G2EoYRLgBXFpVmFYDcREV4HIcnmUhiGBRouEMJGJGzHIspqgdXxK0yCKHRNXoIX4uorCdTyjkyNtdPWrA4Up82EbAbzMRxxZRR54WXVLDIRmRcag5d2R6ugl3ZXzNhTecchpMhIGVAKAYpgJjjsSklBEd99maZoo535ZvdamjBEpusJyctg3h4X8XqodBMx0tiNeg/oGJaKGABpogS40KSqiaEgBqlQWLUtqoVQnytekEjzo0hHqhRorppOZt2p923M2AAV+oBtpAnnPNoB6HaU6mAAIU+IXmi3j2mtFXuUoHKwXpzVrsjcgGOauKEjQrwq157hitGq2NoWmjh7z6Wmxb0m5w66+2VRAuXN/yFUAIACH5BAUKABsALAcABADOAAsAAAX/4CZuRiaM45MZqBgIRbs9AqTcuFLE7VHLOh7KB5ERdjJaEaU4ClO/lgKWjKKcMiJQ8KgumcieVdQMD8cbBeuAkkC6LYLhOxoQ2PF5Ys9PKPBMen17f0CCg4VSh32JV4t8jSNqEIOEgJKPlkYBlJWRInKdiJdkmQlvKAsLBxdABA4RsbIMBggtEhcQsLKxDBC2TAS6vLENdJLDxMZAubu8vjIbzcQRtMzJz79S08oQEt/guNiyy7fcvMbh4OezdAvGrakLAQwyABsELQkY9BP+//ckyPDD4J9BfAMh1GsBoImMeQUN+lMgUJ9CiRMa5msxoB9Gh/o8GmxYMZXIgxtR/yQ46S/gQAURR0pDwYDfywoyLPip5AdnCwsMFPBU4BPFhKBDi444quCmDKZOfwZ9KEGpCKgcN1jdALSpPqIYsabS+nSqvqplvYqQYAeDPgwKwjaMtiDl0oaqUAyo+3TuWwUAMPpVCfee0cEjVBGQq2ABx7oTWmQk4FglZMGN9fGVDMCuiH2AOVOu/PmyxM630gwM0CCn6q8LjVJ8GXvpa5Uwn95OTC/nNxkda1/dLSK475IjCD6dHbK1ZOa4hXP9DXs5chJ00UpVm5xo2qRpoxptwF2E4/IbJpB/SDz9+q9b1aNfQH08+p4a8uvX8B53fLP+ycAfemjsRUBgp1H20K+BghHgVgt1GXZXZpZ5lt4ECjxYR4ScUWiShEtZqBiIInRGWnERNnjiBglw+JyGnxUmGowsyiiZg189lNtPGACjV2+S9UjbU0JWF6SPvEk3QZEqsZYTk3UAaRSUnznJI5LmESCdBVSyaOWUWLK4I5gDUYVeV1T9l+FZClCAUVA09uSmRHBCKAECFEhW51ht6rnmWBXkaR+NjuHpJ40D3DmnQXt2F+ihZxlqVKOfQRACACH5BAUKABwALAcABADOAAsAAAX/ICdyUCkUo/g8mUG8MCGkKgspeC6j6XEIEBpBUeCNfECaglBcOVfJFK7YQwZHQ6JRZBUqTrSuVEuD3nI45pYjFuWKvjjSkCoRaBUMWxkwBGgJCXspQ36Bh4EEB0oKhoiBgyNLjo8Ki4QElIiWfJqHnISNEI+Ql5J9o6SgkqKkgqYihamPkW6oNBgSfiMMDQkGCBLCwxIQDhHIyQwQCGMKxsnKVyPCF9DREQ3MxMPX0cu4wt7J2uHWx9jlKd3o39MiuefYEcvNkuLt5O8c1ePI2tyELXGQwoGDAQf+iEC2xByDCRAjTlAgIUWCBRgCPJQ4AQBFXAs0coT40WLIjRxL/47AcHLkxIomRXL0CHPERZkpa4q4iVKiyp0tR/7kwHMkTUBBJR5dOCEBAVcKKtCAyOHpowXCpk7goABqBZdcvWploACpBKkpIJI1q5OD2rIWE0R1uTZu1LFwbWL9OlKuWb4c6+o9i3dEgw0RCGDUG9KlRw56gDY2qmCByZBaASi+TACA0TucAaTteCcy0ZuOK3N2vJlx58+LRQyY3Xm0ZsgjZg+oPQLi7dUcNXi0LOJw1pgNtB7XG6CBy+U75SYfPTSQAgZTNUDnQHt67wnbZyvwLgKiMN3oCZB3C76tdewpLFgIP2C88rbi4Y+QT3+8S5USMICZXWj1pkEDeUU3lOYGB3alSoEiMIjgX4WlgNF2EibIwQIXauWXSRg2SAOHIU5IIIMoZkhhWiJaiFVbKo6AQEgQXrTAazO1JhkBrBG3Y2Y6EsUhaGn95hprSN0oWpFE7rhkeaQBchGOEWnwEmc0uKWZj0LeuNV3W4Y2lZHFlQCSRjTIl8uZ+kG5HU/3sRlnTG2ytyadytnD3HrmuRcSn+0h1dycexIK1KCjYaCnjCCVqOFFJTZ5GkUUjESWaUIKU2lgCmAKKQIUjHapXRKE+t2og1VgankNYnohqKJ2CmKplso6GKz7WYCgqxeuyoF8u9IQAgA7",
            msg: null,
            msgText: "<em>Loading the next set of posts...</em>",
            selector: null,
            speed: "fast",
            start: n
        },
        state: {
            isDuringAjax: false,
            isInvalidPage: false,
            isDestroyed: false,
            isDone: false,
            isPaused: false,
            isBeyondMaxPage: false,
            currPage: 1
        },
        debug: false,
        behavior: n,
        binder: t(e),
        nextSelector: "div.navigation a:first",
        navSelector: "div.navigation",
        contentSelector: null,
        extraScrollPx: 150,
        itemSelector: "div.post",
        animate: false,
        pathParse: n,
        dataType: "html",
        appendCallback: true,
        bufferPx: 40,
        errorCallback: function() {},
        infid: 0,
        pixelsFromNavToBottom: n,
        path: n,
        prefill: false,
        maxPage: n
    };
    t.infinitescroll.prototype = {
        _binding: function(t) {
            var r = this,
                i = r.options;
            i.v = "2.0b2.120520";
            if (!!i.behavior && this["_binding_" + i.behavior] !== n) {
                this["_binding_" + i.behavior].call(this);
                return
            }
            if (t !== "bind" && t !== "unbind") {
                this._debug("Binding value  " + t + " not valid");
                return false
            }
            if (t === "unbind") {
                this.options.binder.unbind("smartscroll.infscr." + r.options.infid)
            } else {
                this.options.binder[t]("smartscroll.infscr." + r.options.infid, function() {
                    r.scroll()
                })
            }
            this._debug("Binding", t)
        },
        _create: function(i, s) {
            var o = t.extend(true, {}, t.infinitescroll.defaults, i);
            this.options = o;
            var u = t(e);
            var a = this;
            if (!a._validate(i)) {
                return false
            }
            var f = t(o.nextSelector).attr("href");
            if (!f) {
                this._debug("Navigation selector not found");
                return false
            }
            o.path = o.path || this._determinepath(f);
            o.contentSelector = o.contentSelector || this.element;
            o.loading.selector = o.loading.selector || o.contentSelector;
            o.loading.msg = o.loading.msg || t('<div id="infscr-loading"><img alt="Loading..." src="' + o.loading.img + '" /><div>' + o.loading.msgText + "</div></div>");
            (new Image).src = o.loading.img;
            if (o.pixelsFromNavToBottom === n) {
                o.pixelsFromNavToBottom = t(document).height() - t(o.navSelector).offset().top;
                this._debug("pixelsFromNavToBottom: " + o.pixelsFromNavToBottom)
            }
            var l = this;
            o.loading.start = o.loading.start || function() {
                t(o.navSelector).hide();
                o.loading.msg.appendTo(o.loading.selector).show(o.loading.speed, t.proxy(function() {
                    this.beginAjax(o)
                }, l))
            };
            o.loading.finished = o.loading.finished || function() {
                if (!o.state.isBeyondMaxPage) o.loading.msg.fadeOut(o.loading.speed)
            };
            o.callback = function(e, r, i) {
                if (!!o.behavior && e["_callback_" + o.behavior] !== n) {
                    e["_callback_" + o.behavior].call(t(o.contentSelector)[0], r, i)
                }
                if (s) {
                    s.call(t(o.contentSelector)[0], r, o, i)
                }
                if (o.prefill) {
                    u.bind("resize.infinite-scroll", e._prefill)
                }
            };
            if (i.debug) {
                if (Function.prototype.bind && (typeof console === "object" || typeof console === "function") && typeof console.log === "object") {
                    ["log", "info", "warn", "error", "assert", "dir", "clear", "profile", "profileEnd"].forEach(function(e) {
                        console[e] = this.call(console[e], console)
                    }, Function.prototype.bind)
                }
            }
            this._setup();
            if (o.prefill) {
                this._prefill()
            }
            return true
        },
        _prefill: function() {
            function s() {
                return r.options.contentSelector.height() <= i.height()
            }
            var r = this;
            var i = t(e);
            this._prefill = function() {
                if (s()) {
                    r.scroll()
                }
                i.bind("resize.infinite-scroll", function() {
                    if (s()) {
                        i.unbind("resize.infinite-scroll");
                        r.scroll()
                    }
                })
            };
            this._prefill()
        },
        _debug: function() {
            if (true !== this.options.debug) {
                return
            }
            if (typeof console !== "undefined" && typeof console.log === "function") {
                if (Array.prototype.slice.call(arguments).length === 1 && typeof Array.prototype.slice.call(arguments)[0] === "string") {
                    console.log(Array.prototype.slice.call(arguments).toString())
                } else {
                    console.log(Array.prototype.slice.call(arguments))
                }
            } else if (!Function.prototype.bind && typeof console !== "undefined" && typeof console.log === "object") {
                Function.prototype.call.call(console.log, console, Array.prototype.slice.call(arguments))
            }
        },
        _determinepath: function(t) {
            var r = this.options;
            if (!!r.behavior && this["_determinepath_" + r.behavior] !== n) {
                return this["_determinepath_" + r.behavior].call(this, t)
            }
            if (!!r.pathParse) {
                this._debug("pathParse manual");
                return r.pathParse(t, this.options.state.currPage + 1)
            } else if (t.match(/^(.*?)\b2\b(.*?$)/)) {
                t = t.match(/^(.*?)\b2\b(.*?$)/).slice(1)
            } else if (t.match(/^(.*?)2(.*?$)/)) {
                if (t.match(/^(.*?page=)2(\/.*|$)/)) {
                    t = t.match(/^(.*?page=)2(\/.*|$)/).slice(1);
                    return t
                }
                t = t.match(/^(.*?)2(.*?$)/).slice(1)
            } else {
                if (t.match(/^(.*?page=)1(\/.*|$)/)) {
                    t = t.match(/^(.*?page=)1(\/.*|$)/).slice(1);
                    return t
                } else {
                    this._debug("Sorry, we couldn't parse your Next (Previous Posts) URL. Verify your the css selector points to the correct A tag. If you still get this error: yell, scream, and kindly ask for help at infinite-scroll.com.");
                    r.state.isInvalidPage = true
                }
            }
            this._debug("determinePath", t);
            return t
        },
        _error: function(t) {
            var r = this.options;
            if (!!r.behavior && this["_error_" + r.behavior] !== n) {
                this["_error_" + r.behavior].call(this, t);
                return
            }
            if (t !== "destroy" && t !== "end") {
                t = "unknown"
            }
            this._debug("Error", t);
            if (t === "end" || r.state.isBeyondMaxPage) {
                this._showdonemsg()
            }
            r.state.isDone = true;
            r.state.currPage = 1;
            r.state.isPaused = false;
            r.state.isBeyondMaxPage = false;
            this._binding("unbind")
        },
        _loadcallback: function(i, s, o) {
            var u = this.options,
                a = this.options.callback,
                f = u.state.isDone ? "done" : !u.appendCallback ? "no-append" : "append",
                l;
            if (!!u.behavior && this["_loadcallback_" + u.behavior] !== n) {
                this["_loadcallback_" + u.behavior].call(this, i, s);
                return
            }
            switch (f) {
                case "done":
                    this._showdonemsg();
                    return false;
                case "no-append":
                    if (u.dataType === "html") {
                        s = "<div>" + s + "</div>";
                        s = t(s).find(u.itemSelector)
                    }
                    break;
                case "append":
                    var c = i.children();
                    if (c.length === 0) {
                        return this._error("end")
                    }
                    l = document.createDocumentFragment();
                    while (i[0].firstChild) {
                        l.appendChild(i[0].firstChild)
                    }
                    this._debug("contentSelector", t(u.contentSelector)[0]);
                    t(u.contentSelector)[0].appendChild(l);
                    s = c.get();
                    break
            }
            u.loading.finished.call(t(u.contentSelector)[0], u);
            if (u.animate) {
                var h = t(e).scrollTop() + t(u.loading.msg).height() + u.extraScrollPx + "px";
                t("html,body").animate({
                    scrollTop: h
                }, 800, function() {
                    u.state.isDuringAjax = false
                })
            }
            if (!u.animate) {
                u.state.isDuringAjax = false
            }
            a(this, s, o);
            if (u.prefill) {
                this._prefill()
            }
        },
        _nearbottom: function() {
            var i = this.options,
                s = 0 + t(document).height() - i.binder.scrollTop() - t(e).height();
            if (!!i.behavior && this["_nearbottom_" + i.behavior] !== n) {
                return this["_nearbottom_" + i.behavior].call(this)
            }
            this._debug("math:", s, i.pixelsFromNavToBottom);
            return s - i.bufferPx < i.pixelsFromNavToBottom
        },
        _pausing: function(t) {
            var r = this.options;
            if (!!r.behavior && this["_pausing_" + r.behavior] !== n) {
                this["_pausing_" + r.behavior].call(this, t);
                return
            }
            if (t !== "pause" && t !== "resume" && t !== null) {
                this._debug("Invalid argument. Toggling pause value instead")
            }
            t = t && (t === "pause" || t === "resume") ? t : "toggle";
            switch (t) {
                case "pause":
                    r.state.isPaused = true;
                    break;
                case "resume":
                    r.state.isPaused = false;
                    break;
                case "toggle":
                    r.state.isPaused = !r.state.isPaused;
                    break
            }
            this._debug("Paused", r.state.isPaused);
            return false
        },
        _setup: function() {
            var t = this.options;
            if (!!t.behavior && this["_setup_" + t.behavior] !== n) {
                this["_setup_" + t.behavior].call(this);
                return
            }
            this._binding("bind");
            return false
        },
        _showdonemsg: function() {
            var r = this.options;
            if (!!r.behavior && this["_showdonemsg_" + r.behavior] !== n) {
                this["_showdonemsg_" + r.behavior].call(this);
                return
            }
            r.loading.msg.find("img").hide().parent().find("div").html(r.loading.finishedMsg).animate({
                opacity: 1
            }, 2e3, function() {
                t(this).parent().fadeOut(r.loading.speed)
            });
            r.errorCallback.call(t(r.contentSelector)[0], "done")
        },
        _validate: function(n) {
            for (var r in n) {
                if (r.indexOf && r.indexOf("Selector") > -1 && t(n[r]).length === 0) {
                    this._debug("Your " + r + " found no elements.");
                    return false
                }
            }
            return true
        },
        bind: function() {
            this._binding("bind")
        },
        destroy: function() {
            this.options.state.isDestroyed = true;
            this.options.loading.finished();
            return this._error("destroy")
        },
        pause: function() {
            this._pausing("pause")
        },
        resume: function() {
            this._pausing("resume")
        },
        beginAjax: function(r) {
            var i = this,
                s = r.path,
                o, u, a, f;
            r.state.currPage++;
            if (r.maxPage != n && r.state.currPage > r.maxPage) {
                r.state.isBeyondMaxPage = true;
                this.destroy();
                return
            }
            o = t(r.contentSelector).is("table, tbody") ? t("<tbody/>") : t("<div/>");
            u = typeof s === "function" ? s(r.state.currPage) : s.join(r.state.currPage);
            i._debug("heading into ajax", u);
            a = r.dataType === "html" || r.dataType === "json" ? r.dataType : "html+callback";
            if (r.appendCallback && r.dataType === "html") {
                a += "+callback"
            }
            switch (a) {
                case "html+callback":
                    i._debug("Using HTML via .load() method");
                    o.load(u + " " + r.itemSelector, n, function(t) {
                        i._loadcallback(o, t, u)
                    });
                    break;
                case "html":
                    i._debug("Using " + a.toUpperCase() + " via $.ajax() method");
                    t.ajax({
                        url: u,
                        dataType: r.dataType,
                        complete: function(t, n) {
                            f = typeof t.isResolved !== "undefined" ? t.isResolved() : n === "success" || n === "notmodified";
                            if (f) {
                                i._loadcallback(o, t.responseText, u)
                            } else {
                                i._error("end")
                            }
                        }
                    });
                    break;
                case "json":
                    i._debug("Using " + a.toUpperCase() + " via $.ajax() method");
                    t.ajax({
                        dataType: "json",
                        type: "GET",
                        url: u,
                        success: function(e, t, s) {
                            f = typeof s.isResolved !== "undefined" ? s.isResolved() : t === "success" || t === "notmodified";
                            if (r.appendCallback) {
                                if (r.template !== n) {
                                    var a = r.template(e);
                                    o.append(a);
                                    if (f) {
                                        i._loadcallback(o, a)
                                    } else {
                                        i._error("end")
                                    }
                                } else {
                                    i._debug("template must be defined.");
                                    i._error("end")
                                }
                            } else {
                                if (f) {
                                    i._loadcallback(o, e, u)
                                } else {
                                    i._error("end")
                                }
                            }
                        },
                        error: function() {
                            i._debug("JSON ajax request failed.");
                            i._error("end")
                        }
                    });
                    break
            }
        },
        retrieve: function(r) {
            r = r || null;
            var i = this,
                s = i.options;
            if (!!s.behavior && this["retrieve_" + s.behavior] !== n) {
                this["retrieve_" + s.behavior].call(this, r);
                return
            }
            if (s.state.isDestroyed) {
                this._debug("Instance is destroyed");
                return false
            }
            s.state.isDuringAjax = true;
            s.loading.start.call(t(s.contentSelector)[0], s)
        },
        scroll: function() {
            var t = this.options,
                r = t.state;
            if (!!t.behavior && this["scroll_" + t.behavior] !== n) {
                this["scroll_" + t.behavior].call(this);
                return
            }
            if (r.isDuringAjax || r.isInvalidPage || r.isDone || r.isDestroyed || r.isPaused) {
                return
            }
            if (!this._nearbottom()) {
                return
            }
            this.retrieve()
        },
        toggle: function() {
            this._pausing()
        },
        unbind: function() {
            this._binding("unbind")
        },
        update: function(n) {
            if (t.isPlainObject(n)) {
                this.options = t.extend(true, this.options, n)
            }
        }
    };
    t.fn.infinitescroll = function(n, r) {
        var i = typeof n;
        switch (i) {
            case "string":
                var s = Array.prototype.slice.call(arguments, 1);
                this.each(function() {
                    var e = t.data(this, "infinitescroll");
                    if (!e) {
                        return false
                    }
                    if (!t.isFunction(e[n]) || n.charAt(0) === "_") {
                        return false
                    }
                    e[n].apply(e, s)
                });
                break;
            case "object":
                this.each(function() {
                    var e = t.data(this, "infinitescroll");
                    if (e) {
                        e.update(n)
                    } else {
                        e = new t.infinitescroll(n, r, this);
                        if (!e.failed) {
                            t.data(this, "infinitescroll", e)
                        }
                    }
                });
                break
        }
        return this
    };
    var r = t.event,
        i;
    r.special.smartscroll = {
        setup: function() {
            t(this).bind("scroll", r.special.smartscroll.handler)
        },
        teardown: function() {
            t(this).unbind("scroll", r.special.smartscroll.handler)
        },
        handler: function(e, n) {
            var r = this,
                s = arguments;
            e.type = "smartscroll";
            if (i) {
                clearTimeout(i)
            }
            i = setTimeout(function() {
                t(r).trigger("smartscroll", s)
            }, n === "execAsap" ? 0 : 100)
        }
    };
    t.fn.smartscroll = function(e) {
        return e ? this.bind("smartscroll", e) : this.trigger("smartscroll", ["execAsap"])
    }
})(window, jQuery)

/*===============================
/templates/ja_playmag/js/jquery.metisMenu.js
================================================================================*/
;;
(function($, window, document, undefined) {
    var pluginName = "metisMenu",
        defaults = {
            toggle: true
        };

    function Plugin(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }
    Plugin.prototype = {
        init: function() {
            var $this = $(this.element),
                $toggle = this.settings.toggle;
            $this.find('li.active').has('ul').children('ul').addClass('collapse in');
            $this.find('li').not('.active').has('ul').children('ul').addClass('collapse');
            $this.find('li').has('ul').children('a').on('click', function(e) {
                e.preventDefault();
                $(this).parent('li').toggleClass('active').children('ul').collapse('toggle');
                if ($toggle) {
                    $(this).parent('li').siblings().removeClass('active').children('ul.in').collapse('hide');
                }
            });
        }
    };
    $.fn[pluginName] = function(options) {
        if (options == 'destroy') {
            this.each(function() {
                $.data(this, "plugin_" + pluginName, false);
            });
            return;
        }
        return this.each(function() {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            }
        });
    };
})(jQuery, window, document);

/*===============================
/templates/ja_playmag/js/custom-javascript.js
================================================================================*/
;
$(document).ready(function() {
    if (isTouchSupported() == false) {
        $(".mega .dropdown-toggle").click(function() {
            window.location = this.href;
        });
    }
    if ($('#jsHomeBanners').length) {
        $("#jsMarketingBanner2 img, .e1BannerThumb img").addClass('responsiveimg');
        $('#jsMarketingBanner2 p :first').addClass('active');
        $(".e1BannerThumb a").attr('href', 'javascript:void(0)');
        var bannerTimer = setInterval(function() {
            if (isTouchSupported() == true || $('#jsMarketingBanner2').is(":hover") == false) {
                fncRotateBanners();
            }
        }, 5000);
        $(".e1BannerThumb").click(function() {
            clearInterval(bannerTimer);
            fncRotateBanners(this.id);
            bannerTimer = setInterval(function() {
                if (isTouchSupported() == true || $('#jsMarketingBanner2').is(":hover") == false) {
                    fncRotateBanners();
                }
            }, 5000);
        });
    }
    $('#menu').metisMenu();
    $('.myone-menu').metisMenu('destroy');
    $('.myone-menu').metisMenu({
        toggle: false
    });
    $('li.nav-heading').click(function() {
        var cntrl = $(this).children('ul');
        if (cntrl.is(":visible")) {
            cntrl.slideUp();
        } else {
            cntrl.slideDown();
            cntrl.prev().parent().siblings().children('ul').slideUp();
        }
    });
    var left_height = $("#sidebar-wrapper").height();
    var right_height = $("#page-content-wrapper").height();
    var total_height = left_height;
    if (right_height > left_height) {
        total_height = right_height;
    }
    $("#sidebar-wrapper").css('height', total_height);
    $("#page-content-wrapper").css('height', total_height);
    if ($(".com_e1home").length > 0) {
        $('body').addClass('home-page');
    }
});
$(window).load(function() {
    $('img.responsiveimg').each(function() {
        var imgWidth = $(this).width();
        $(this).hide();
        if ($(this).parent().width() < imgWidth) {
            $(this).css('width', '100%');
        }
        $(this).show();
    });
    $('a.collapselink').click(function() {
        if ($('#collapseExample').hasClass('in')) {
            $(this).text('More');
        } else {
            $(this).text('Less');
        }
    });
    $('a.collapseread').click(function() {
        if ($('#collapseread').hasClass('in')) {
            $(this).text('Read More');
        } else {
            $(this).text('Read Less');
        }
    });
});

function isTouchSupported() {
    var msTouchEnabled = window.navigator.msMaxTouchPoints;
    var generalTouchEnabled = "ontouchstart" in document.createElement("div");
    if (msTouchEnabled || generalTouchEnabled) {
        return true;
    }
    return false;
}

function fncRotateBanners(currentId) {
    var $active = $('#jsMarketingBanner2 .active');
    if (currentId != undefined) {
        var arrActiveThumNum = currentId.split('-');
        $('.e1BannerThumb').removeClass('activeBannerThumb');
        $('#jsE1Banner-' + arrActiveThumNum[1]).addClass('activeBannerThumb');
        $next = $("#jsMarketingBanner2 p:nth-child(" + arrActiveThumNum[1] + ")");
        $next.css('z-index', 2);
        $active.fadeOut(200, function() {
            $active.css('z-index', 1).show().removeClass('active');
            $next.css('z-index', 3).addClass('active');
        });
    } else {
        var $next = ($active.next().length > 0) ? $active.next() : $('#jsMarketingBanner2 p :first');
        $next.css('z-index', 2);
        var activeThumNum = $next.index() + 1;
        $('.e1BannerThumb').removeClass('activeBannerThumb');
        $('#jsE1Banner-' + activeThumNum).addClass('activeBannerThumb');
        $active.fadeOut(1500, function() {
            $active.css('z-index', 1).show().removeClass('active');
            $next.css('z-index', 3).addClass('active');
        });
    }
}

/*===============================
/templates/ja_playmag/js/bootstrap-typeahead.js
================================================================================*/
;
/*!
 * typeahead.js 0.9.3
 * https://github.com/twitter/typeahead
 * Copyright 2013 Twitter, Inc. and other contributors; Licensed MIT
 */
(function($) {
    var VERSION = "0.9.3";
    var utils = {
        isMsie: function() {
            var match = /(msie) ([\w.]+)/i.exec(navigator.userAgent);
            return match ? parseInt(match[2], 10) : false;
        },
        isBlankString: function(str) {
            return !str || /^\s*$/.test(str);
        },
        escapeRegExChars: function(str) {
            return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
        },
        isString: function(obj) {
            return typeof obj === "string";
        },
        isNumber: function(obj) {
            return typeof obj === "number";
        },
        isArray: $.isArray,
        isFunction: $.isFunction,
        isObject: $.isPlainObject,
        isUndefined: function(obj) {
            return typeof obj === "undefined";
        },
        bind: $.proxy,
        bindAll: function(obj) {
            var val;
            for (var key in obj) {
                $.isFunction(val = obj[key]) && (obj[key] = $.proxy(val, obj));
            }
        },
        indexOf: function(haystack, needle) {
            for (var i = 0; i < haystack.length; i++) {
                if (haystack[i] === needle) {
                    return i;
                }
            }
            return -1;
        },
        each: $.each,
        map: $.map,
        filter: $.grep,
        every: function(obj, test) {
            var result = true;
            if (!obj) {
                return result;
            }
            $.each(obj, function(key, val) {
                if (!(result = test.call(null, val, key, obj))) {
                    return false;
                }
            });
            return !!result;
        },
        some: function(obj, test) {
            var result = false;
            if (!obj) {
                return result;
            }
            $.each(obj, function(key, val) {
                if (result = test.call(null, val, key, obj)) {
                    return false;
                }
            });
            return !!result;
        },
        mixin: $.extend,
        getUniqueId: function() {
            var counter = 0;
            return function() {
                return counter++;
            };
        }(),
        defer: function(fn) {
            setTimeout(fn, 0);
        },
        debounce: function(func, wait, immediate) {
            var timeout, result;
            return function() {
                var context = this,
                    args = arguments,
                    later, callNow;
                later = function() {
                    timeout = null;
                    if (!immediate) {
                        result = func.apply(context, args);
                    }
                };
                callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow) {
                    result = func.apply(context, args);
                }
                return result;
            };
        },
        throttle: function(func, wait) {
            var context, args, timeout, result, previous, later;
            previous = 0;
            later = function() {
                previous = new Date();
                timeout = null;
                result = func.apply(context, args);
            };
            return function() {
                var now = new Date(),
                    remaining = wait - (now - previous);
                context = this;
                args = arguments;
                if (remaining <= 0) {
                    clearTimeout(timeout);
                    timeout = null;
                    previous = now;
                    result = func.apply(context, args);
                } else if (!timeout) {
                    timeout = setTimeout(later, remaining);
                }
                return result;
            };
        },
        tokenizeQuery: function(str) {
            return $.trim(str).toLowerCase().split(/[\s]+/);
        },
        tokenizeText: function(str) {
            return $.trim(str).toLowerCase().split(/[\s\-_]+/);
        },
        getProtocol: function() {
            return location.protocol;
        },
        noop: function() {}
    };
    var EventTarget = function() {
        var eventSplitter = /\s+/;
        return {
            on: function(events, callback) {
                var event;
                if (!callback) {
                    return this;
                }
                this._callbacks = this._callbacks || {};
                events = events.split(eventSplitter);
                while (event = events.shift()) {
                    this._callbacks[event] = this._callbacks[event] || [];
                    this._callbacks[event].push(callback);
                }
                return this;
            },
            trigger: function(events, data) {
                var event, callbacks;
                if (!this._callbacks) {
                    return this;
                }
                events = events.split(eventSplitter);
                while (event = events.shift()) {
                    if (callbacks = this._callbacks[event]) {
                        for (var i = 0; i < callbacks.length; i += 1) {
                            callbacks[i].call(this, {
                                type: event,
                                data: data
                            });
                        }
                    }
                }
                return this;
            }
        };
    }();
    var EventBus = function() {
        var namespace = "typeahead:";

        function EventBus(o) {
            if (!o || !o.el) {
                $.error("EventBus initialized without el");
            }
            this.$el = $(o.el);
        }
        utils.mixin(EventBus.prototype, {
            trigger: function(type) {
                var args = [].slice.call(arguments, 1);
                this.$el.trigger(namespace + type, args);
            }
        });
        return EventBus;
    }();
    var PersistentStorage = function() {
        var ls, methods;
        try {
            ls = window.localStorage;
            ls.setItem("~~~", "!");
            ls.removeItem("~~~");
        } catch (err) {
            ls = null;
        }

        function PersistentStorage(namespace) {
            this.prefix = ["__", namespace, "__"].join("");
            this.ttlKey = "__ttl__";
            this.keyMatcher = new RegExp("^" + this.prefix);
        }
        if (ls && window.JSON) {
            methods = {
                _prefix: function(key) {
                    return this.prefix + key;
                },
                _ttlKey: function(key) {
                    return this._prefix(key) + this.ttlKey;
                },
                get: function(key) {
                    if (this.isExpired(key)) {
                        this.remove(key);
                    }
                    return decode(ls.getItem(this._prefix(key)));
                },
                set: function(key, val, ttl) {
                    if (utils.isNumber(ttl)) {
                        ls.setItem(this._ttlKey(key), encode(now() + ttl));
                    } else {
                        ls.removeItem(this._ttlKey(key));
                    }
                    return ls.setItem(this._prefix(key), encode(val));
                },
                remove: function(key) {
                    ls.removeItem(this._ttlKey(key));
                    ls.removeItem(this._prefix(key));
                    return this;
                },
                clear: function() {
                    var i, key, keys = [],
                        len = ls.length;
                    for (i = 0; i < len; i++) {
                        if ((key = ls.key(i)).match(this.keyMatcher)) {
                            keys.push(key.replace(this.keyMatcher, ""));
                        }
                    }
                    for (i = keys.length; i--;) {
                        this.remove(keys[i]);
                    }
                    return this;
                },
                isExpired: function(key) {
                    var ttl = decode(ls.getItem(this._ttlKey(key)));
                    return utils.isNumber(ttl) && now() > ttl ? true : false;
                }
            };
        } else {
            methods = {
                get: utils.noop,
                set: utils.noop,
                remove: utils.noop,
                clear: utils.noop,
                isExpired: utils.noop
            };
        }
        utils.mixin(PersistentStorage.prototype, methods);
        return PersistentStorage;

        function now() {
            return new Date().getTime();
        }

        function encode(val) {
            return JSON.stringify(utils.isUndefined(val) ? null : val);
        }

        function decode(val) {
            return JSON.parse(val);
        }
    }();
    var RequestCache = function() {
        function RequestCache(o) {
            utils.bindAll(this);
            o = o || {};
            this.sizeLimit = o.sizeLimit || 10;
            this.cache = {};
            this.cachedKeysByAge = [];
        }
        utils.mixin(RequestCache.prototype, {
            get: function(url) {
                return this.cache[url];
            },
            set: function(url, resp) {
                var requestToEvict;
                if (this.cachedKeysByAge.length === this.sizeLimit) {
                    requestToEvict = this.cachedKeysByAge.shift();
                    delete this.cache[requestToEvict];
                }
                this.cache[url] = resp;
                this.cachedKeysByAge.push(url);
            }
        });
        return RequestCache;
    }();
    var Transport = function() {
        var pendingRequestsCount = 0,
            pendingRequests = {},
            maxPendingRequests, requestCache;

        function Transport(o) {
            utils.bindAll(this);
            o = utils.isString(o) ? {
                url: o
            } : o;
            requestCache = requestCache || new RequestCache();
            maxPendingRequests = utils.isNumber(o.maxParallelRequests) ? o.maxParallelRequests : maxPendingRequests || 6;
            this.url = o.url;
            this.wildcard = o.wildcard || "%QUERY";
            this.filter = o.filter;
            this.replace = o.replace;
            this.ajaxSettings = {
                type: "get",
                cache: o.cache,
                timeout: o.timeout,
                dataType: o.dataType || "json",
                beforeSend: o.beforeSend
            };
            this._get = (/^throttle$/i.test(o.rateLimitFn) ? utils.throttle : utils.debounce)(this._get, o.rateLimitWait || 300);
        }
        utils.mixin(Transport.prototype, {
            _get: function(url, cb) {
                var that = this;
                if (belowPendingRequestsThreshold()) {
                    this._sendRequest(url).done(done);
                } else {
                    this.onDeckRequestArgs = [].slice.call(arguments, 0);
                }

                function done(resp) {
                    var data = that.filter ? that.filter(resp) : resp;
                    cb && cb(data);
                    requestCache.set(url, resp);
                }
            },
            _sendRequest: function(url) {
                var that = this,
                    jqXhr = pendingRequests[url];
                if (!jqXhr) {
                    incrementPendingRequests();
                    jqXhr = pendingRequests[url] = $.ajax(url, this.ajaxSettings).always(always);
                }
                return jqXhr;

                function always() {
                    decrementPendingRequests();
                    pendingRequests[url] = null;
                    if (that.onDeckRequestArgs) {
                        that._get.apply(that, that.onDeckRequestArgs);
                        that.onDeckRequestArgs = null;
                    }
                }
            },
            get: function(query, cb) {
                var that = this,
                    encodedQuery = encodeURIComponent(query || ""),
                    url, resp;
                cb = cb || utils.noop;
                url = this.replace ? this.replace(this.url, encodedQuery) : this.url.replace(this.wildcard, encodedQuery);
                if (resp = requestCache.get(url)) {
                    utils.defer(function() {
                        cb(that.filter ? that.filter(resp) : resp);
                    });
                } else {
                    this._get(url, cb);
                }
                return !!resp;
            }
        });
        return Transport;

        function incrementPendingRequests() {
            pendingRequestsCount++;
        }

        function decrementPendingRequests() {
            pendingRequestsCount--;
        }

        function belowPendingRequestsThreshold() {
            return pendingRequestsCount < maxPendingRequests;
        }
    }();
    var Dataset = function() {
        var keys = {
            thumbprint: "thumbprint",
            protocol: "protocol",
            itemHash: "itemHash",
            adjacencyList: "adjacencyList"
        };

        function Dataset(o) {
            utils.bindAll(this);
            if (utils.isString(o.template) && !o.engine) {
                $.error("no template engine specified");
            }
            if (!o.local && !o.prefetch && !o.remote) {
                $.error("one of local, prefetch, or remote is required");
            }
            this.name = o.name || utils.getUniqueId();
            this.limit = o.limit || 10;
            this.minLength = o.minLength || 1;
            this.header = o.header;
            this.footer = o.footer;
            this.valueKey = o.valueKey || "value";
            this.template = compileTemplate(o.template, o.engine, this.valueKey);
            this.local = o.local;
            this.prefetch = o.prefetch;
            this.remote = o.remote;
            this.itemHash = {};
            this.adjacencyList = {};
            this.storage = o.name ? new PersistentStorage(o.name) : null;
        }
        utils.mixin(Dataset.prototype, {
            _processLocalData: function(data) {
                this._mergeProcessedData(this._processData(data));
            },
            _loadPrefetchData: function(o) {
                var that = this,
                    thumbprint = VERSION + (o.thumbprint || ""),
                    storedThumbprint, storedProtocol, storedItemHash, storedAdjacencyList, isExpired, deferred;
                if (this.storage) {
                    storedThumbprint = this.storage.get(keys.thumbprint);
                    storedProtocol = this.storage.get(keys.protocol);
                    storedItemHash = this.storage.get(keys.itemHash);
                    storedAdjacencyList = this.storage.get(keys.adjacencyList);
                }
                isExpired = storedThumbprint !== thumbprint || storedProtocol !== utils.getProtocol();
                o = utils.isString(o) ? {
                    url: o
                } : o;
                o.ttl = utils.isNumber(o.ttl) ? o.ttl : 24 * 60 * 60 * 1e3;
                if (storedItemHash && storedAdjacencyList && !isExpired) {
                    this._mergeProcessedData({
                        itemHash: storedItemHash,
                        adjacencyList: storedAdjacencyList
                    });
                    deferred = $.Deferred().resolve();
                } else {
                    deferred = $.getJSON(o.url).done(processPrefetchData);
                }
                return deferred;

                function processPrefetchData(data) {
                    var filteredData = o.filter ? o.filter(data) : data,
                        processedData = that._processData(filteredData),
                        itemHash = processedData.itemHash,
                        adjacencyList = processedData.adjacencyList;
                    if (that.storage) {
                        that.storage.set(keys.itemHash, itemHash, o.ttl);
                        that.storage.set(keys.adjacencyList, adjacencyList, o.ttl);
                        that.storage.set(keys.thumbprint, thumbprint, o.ttl);
                        that.storage.set(keys.protocol, utils.getProtocol(), o.ttl);
                    }
                    that._mergeProcessedData(processedData);
                }
            },
            _transformDatum: function(datum) {
                var value = utils.isString(datum) ? datum : datum[this.valueKey],
                    tokens = datum.tokens || utils.tokenizeText(value),
                    item = {
                        value: value,
                        tokens: tokens
                    };
                if (utils.isString(datum)) {
                    item.datum = {};
                    item.datum[this.valueKey] = datum;
                } else {
                    item.datum = datum;
                }
                item.tokens = utils.filter(item.tokens, function(token) {
                    return !utils.isBlankString(token);
                });
                item.tokens = utils.map(item.tokens, function(token) {
                    return token.toLowerCase();
                });
                return item;
            },
            _processData: function(data) {
                var that = this,
                    itemHash = {},
                    adjacencyList = {};
                utils.each(data, function(i, datum) {
                    var item = that._transformDatum(datum),
                        id = utils.getUniqueId(item.value);
                    itemHash[id] = item;
                    utils.each(item.tokens, function(i, token) {
                        var character = token.charAt(0),
                            adjacency = adjacencyList[character] || (adjacencyList[character] = [id]);
                        !~utils.indexOf(adjacency, id) && adjacency.push(id);
                    });
                });
                return {
                    itemHash: itemHash,
                    adjacencyList: adjacencyList
                };
            },
            _mergeProcessedData: function(processedData) {
                var that = this;
                utils.mixin(this.itemHash, processedData.itemHash);
                utils.each(processedData.adjacencyList, function(character, adjacency) {
                    var masterAdjacency = that.adjacencyList[character];
                    that.adjacencyList[character] = masterAdjacency ? masterAdjacency.concat(adjacency) : adjacency;
                });
            },
            _getLocalSuggestions: function(terms) {
                var that = this,
                    firstChars = [],
                    lists = [],
                    shortestList, suggestions = [];
                utils.each(terms, function(i, term) {
                    var firstChar = term.charAt(0);
                    !~utils.indexOf(firstChars, firstChar) && firstChars.push(firstChar);
                });
                utils.each(firstChars, function(i, firstChar) {
                    var list = that.adjacencyList[firstChar];
                    if (!list) {
                        return false;
                    }
                    lists.push(list);
                    if (!shortestList || list.length < shortestList.length) {
                        shortestList = list;
                    }
                });
                if (lists.length < firstChars.length) {
                    return [];
                }
                utils.each(shortestList, function(i, id) {
                    var item = that.itemHash[id],
                        isCandidate, isMatch;
                    isCandidate = utils.every(lists, function(list) {
                        return ~utils.indexOf(list, id);
                    });
                    isMatch = isCandidate && utils.every(terms, function(term) {
                        return utils.some(item.tokens, function(token) {
                            return token.indexOf(term) === 0;
                        });
                    });
                    isMatch && suggestions.push(item);
                });
                return suggestions;
            },
            initialize: function() {
                var deferred;
                this.local && this._processLocalData(this.local);
                this.transport = this.remote ? new Transport(this.remote) : null;
                deferred = this.prefetch ? this._loadPrefetchData(this.prefetch) : $.Deferred().resolve();
                this.local = this.prefetch = this.remote = null;
                this.initialize = function() {
                    return deferred;
                };
                return deferred;
            },
            getSuggestions: function(query, cb) {
                var that = this,
                    terms, suggestions, cacheHit = false;
                if (query.length < this.minLength) {
                    return;
                }
                terms = utils.tokenizeQuery(query);
                suggestions = this._getLocalSuggestions(terms).slice(0, this.limit);
                if (suggestions.length < this.limit && this.transport) {
                    cacheHit = this.transport.get(query, processRemoteData);
                }!cacheHit && cb && cb(suggestions);

                function processRemoteData(data) {
                    suggestions = suggestions.slice(0);
                    utils.each(data, function(i, datum) {
                        var item = that._transformDatum(datum),
                            isDuplicate;
                        isDuplicate = utils.some(suggestions, function(suggestion) {
                            return item.value === suggestion.value;
                        });
                        !isDuplicate && suggestions.push(item);
                        return suggestions.length < that.limit;
                    });
                    cb && cb(suggestions);
                }
            }
        });
        return Dataset;

        function compileTemplate(template, engine, valueKey) {
            var renderFn, compiledTemplate;
            if (utils.isFunction(template)) {
                renderFn = template;
            } else if (utils.isString(template)) {
                compiledTemplate = engine.compile(template);
                renderFn = utils.bind(compiledTemplate.render, compiledTemplate);
            } else {
                renderFn = function(context) {
                    return "<p>" + context[valueKey] + "</p>";
                };
            }
            return renderFn;
        }
    }();
    var InputView = function() {
        function InputView(o) {
            var that = this;
            utils.bindAll(this);
            this.specialKeyCodeMap = {
                9: "tab",
                27: "esc",
                37: "left",
                39: "right",
                13: "enter",
                38: "up",
                40: "down"
            };
            this.$hint = $(o.hint);
            this.$input = $(o.input).on("blur.tt", this._handleBlur).on("focus.tt", this._handleFocus).on("keydown.tt", this._handleSpecialKeyEvent);
            if (!utils.isMsie()) {
                this.$input.on("input.tt", this._compareQueryToInputValue);
            } else {
                this.$input.on("keydown.tt keypress.tt cut.tt paste.tt", function($e) {
                    if (that.specialKeyCodeMap[$e.which || $e.keyCode]) {
                        return;
                    }
                    utils.defer(that._compareQueryToInputValue);
                });
            }
            this.query = this.$input.val();
            this.$overflowHelper = buildOverflowHelper(this.$input);
        }
        utils.mixin(InputView.prototype, EventTarget, {
            _handleFocus: function() {
                this.trigger("focused");
            },
            _handleBlur: function() {
                this.trigger("blured");
            },
            _handleSpecialKeyEvent: function($e) {
                var keyName = this.specialKeyCodeMap[$e.which || $e.keyCode];
                keyName && this.trigger(keyName + "Keyed", $e);
            },
            _compareQueryToInputValue: function() {
                var inputValue = this.getInputValue(),
                    isSameQuery = compareQueries(this.query, inputValue),
                    isSameQueryExceptWhitespace = isSameQuery ? this.query.length !== inputValue.length : false;
                if (isSameQueryExceptWhitespace) {
                    this.trigger("whitespaceChanged", {
                        value: this.query
                    });
                } else if (!isSameQuery) {
                    this.trigger("queryChanged", {
                        value: this.query = inputValue
                    });
                }
            },
            destroy: function() {
                this.$hint.off(".tt");
                this.$input.off(".tt");
                this.$hint = this.$input = this.$overflowHelper = null;
            },
            focus: function() {
                this.$input.focus();
            },
            blur: function() {
                this.$input.blur();
            },
            getQuery: function() {
                return this.query;
            },
            setQuery: function(query) {
                this.query = query;
            },
            getInputValue: function() {
                return this.$input.val();
            },
            setInputValue: function(value, silent) {
                this.$input.val(value);
                !silent && this._compareQueryToInputValue();
            },
            getHintValue: function() {
                return this.$hint.val();
            },
            setHintValue: function(value) {
                this.$hint.val(value);
            },
            getLanguageDirection: function() {
                return (this.$input.css("direction") || "ltr").toLowerCase();
            },
            isOverflow: function() {
                this.$overflowHelper.text(this.getInputValue());
                return this.$overflowHelper.width() > this.$input.width();
            },
            isCursorAtEnd: function() {
                var valueLength = this.$input.val().length,
                    selectionStart = this.$input[0].selectionStart,
                    range;
                if (utils.isNumber(selectionStart)) {
                    return selectionStart === valueLength;
                } else if (document.selection) {
                    range = document.selection.createRange();
                    range.moveStart("character", -valueLength);
                    return valueLength === range.text.length;
                }
                return true;
            }
        });
        return InputView;

        function buildOverflowHelper($input) {
            return $("<span></span>").css({
                position: "absolute",
                left: "-9999px",
                visibility: "hidden",
                whiteSpace: "nowrap",
                fontFamily: $input.css("font-family"),
                fontSize: $input.css("font-size"),
                fontStyle: $input.css("font-style"),
                fontVariant: $input.css("font-variant"),
                fontWeight: $input.css("font-weight"),
                wordSpacing: $input.css("word-spacing"),
                letterSpacing: $input.css("letter-spacing"),
                textIndent: $input.css("text-indent"),
                textRendering: $input.css("text-rendering"),
                textTransform: $input.css("text-transform")
            }).insertAfter($input);
        }

        function compareQueries(a, b) {
            a = (a || "").replace(/^\s*/g, "").replace(/\s{2,}/g, " ");
            b = (b || "").replace(/^\s*/g, "").replace(/\s{2,}/g, " ");
            return a === b;
        }
    }();
    var DropdownView = function() {
        var html = {
                suggestionsList: '<span class="tt-suggestions"></span>'
            },
            css = {
                suggestionsList: {
                    display: "block"
                },
                suggestion: {
                    whiteSpace: "nowrap",
                    cursor: "pointer"
                },
                suggestionChild: {
                    whiteSpace: "normal"
                }
            };

        function DropdownView(o) {
            utils.bindAll(this);
            this.isOpen = false;
            this.isEmpty = true;
            this.isMouseOverDropdown = false;
            this.$menu = $(o.menu).on("mouseenter.tt", this._handleMouseenter).on("mouseleave.tt", this._handleMouseleave).on("click.tt", ".tt-suggestion", this._handleSelection).on("mouseover.tt", ".tt-suggestion", this._handleMouseover);
        }
        utils.mixin(DropdownView.prototype, EventTarget, {
            _handleMouseenter: function() {
                this.isMouseOverDropdown = true;
            },
            _handleMouseleave: function() {
                this.isMouseOverDropdown = false;
            },
            _handleMouseover: function($e) {
                var $suggestion = $($e.currentTarget);
                this._getSuggestions().removeClass("tt-is-under-cursor");
                $suggestion.addClass("tt-is-under-cursor");
            },
            _handleSelection: function($e) {
                var $suggestion = $($e.currentTarget);
                this.trigger("suggestionSelected", extractSuggestion($suggestion));
            },
            _show: function() {
                this.$menu.css("display", "block");
            },
            _hide: function() {
                this.$menu.hide();
            },
            _moveCursor: function(increment) {
                var $suggestions, $cur, nextIndex, $underCursor;
                if (!this.isVisible()) {
                    return;
                }
                $suggestions = this._getSuggestions();
                $cur = $suggestions.filter(".tt-is-under-cursor");
                $cur.removeClass("tt-is-under-cursor");
                nextIndex = $suggestions.index($cur) + increment;
                nextIndex = (nextIndex + 1) % ($suggestions.length + 1) - 1;
                if (nextIndex === -1) {
                    this.trigger("cursorRemoved");
                    return;
                } else if (nextIndex < -1) {
                    nextIndex = $suggestions.length - 1;
                }
                $underCursor = $suggestions.eq(nextIndex).addClass("tt-is-under-cursor");
                this._ensureVisibility($underCursor);
                this.trigger("cursorMoved", extractSuggestion($underCursor));
            },
            _getSuggestions: function() {
                return this.$menu.find(".tt-suggestions > .tt-suggestion");
            },
            _ensureVisibility: function($el) {
                var menuHeight = this.$menu.height() + parseInt(this.$menu.css("paddingTop"), 10) + parseInt(this.$menu.css("paddingBottom"), 10),
                    menuScrollTop = this.$menu.scrollTop(),
                    elTop = $el.position().top,
                    elBottom = elTop + $el.outerHeight(true);
                if (elTop < 0) {
                    this.$menu.scrollTop(menuScrollTop + elTop);
                } else if (menuHeight < elBottom) {
                    this.$menu.scrollTop(menuScrollTop + (elBottom - menuHeight));
                }
            },
            destroy: function() {
                this.$menu.off(".tt");
                this.$menu = null;
            },
            isVisible: function() {
                return this.isOpen && !this.isEmpty;
            },
            closeUnlessMouseIsOverDropdown: function() {
                if (!this.isMouseOverDropdown) {
                    this.close();
                }
            },
            close: function() {
                if (this.isOpen) {
                    this.isOpen = false;
                    this.isMouseOverDropdown = false;
                    this._hide();
                    this.$menu.find(".tt-suggestions > .tt-suggestion").removeClass("tt-is-under-cursor");
                    this.trigger("closed");
                }
            },
            open: function() {
                if (!this.isOpen) {
                    this.isOpen = true;
                    !this.isEmpty && this._show();
                    this.trigger("opened");
                }
            },
            setLanguageDirection: function(dir) {
                var ltrCss = {
                        left: "0",
                        right: "auto"
                    },
                    rtlCss = {
                        left: "auto",
                        right: " 0"
                    };
                dir === "ltr" ? this.$menu.css(ltrCss) : this.$menu.css(rtlCss);
            },
            moveCursorUp: function() {
                this._moveCursor(-1);
            },
            moveCursorDown: function() {
                this._moveCursor(+1);
            },
            getSuggestionUnderCursor: function() {
                var $suggestion = this._getSuggestions().filter(".tt-is-under-cursor").first();
                return $suggestion.length > 0 ? extractSuggestion($suggestion) : null;
            },
            getFirstSuggestion: function() {
                var $suggestion = this._getSuggestions().first();
                return $suggestion.length > 0 ? extractSuggestion($suggestion) : null;
            },
            renderSuggestions: function(dataset, suggestions) {
                var datasetClassName = "tt-dataset-" + dataset.name,
                    wrapper = '<div class="tt-suggestion">%body</div>',
                    compiledHtml, $suggestionsList, $dataset = this.$menu.find("." + datasetClassName),
                    elBuilder, fragment, $el;
                if ($dataset.length === 0) {
                    $suggestionsList = $(html.suggestionsList).css(css.suggestionsList);
                    $dataset = $("<div></div>").addClass(datasetClassName).append(dataset.header).append($suggestionsList).append(dataset.footer).appendTo(this.$menu);
                }
                if (suggestions.length > 0) {
                    this.isEmpty = false;
                    this.isOpen && this._show();
                    elBuilder = document.createElement("div");
                    fragment = document.createDocumentFragment();
                    utils.each(suggestions, function(i, suggestion) {
                        suggestion.dataset = dataset.name;
                        compiledHtml = dataset.template(suggestion.datum);
                        elBuilder.innerHTML = wrapper.replace("%body", compiledHtml);
                        $el = $(elBuilder.firstChild).css(css.suggestion).data("suggestion", suggestion);
                        $el.children().each(function() {
                            $(this).css(css.suggestionChild);
                        });
                        fragment.appendChild($el[0]);
                    });
                    $dataset.show().find(".tt-suggestions").html(fragment);
                } else {
                    this.clearSuggestions(dataset.name);
                }
                this.trigger("suggestionsRendered");
            },
            clearSuggestions: function(datasetName) {
                var $datasets = datasetName ? this.$menu.find(".tt-dataset-" + datasetName) : this.$menu.find('[class^="tt-dataset-"]'),
                    $suggestions = $datasets.find(".tt-suggestions");
                $datasets.hide();
                $suggestions.empty();
                if (this._getSuggestions().length === 0) {
                    this.isEmpty = true;
                    this._hide();
                }
            }
        });
        return DropdownView;

        function extractSuggestion($el) {
            return $el.data("suggestion");
        }
    }();
    var TypeaheadView = function() {
        var html = {
                wrapper: '<span class="twitter-typeahead"></span>',
                hint: '<input class="tt-hint" type="text" autocomplete="off" spellcheck="off" disabled>',
                dropdown: '<span class="tt-dropdown-menu"></span>'
            },
            css = {
                wrapper: {
                    position: "relative",
                    display: "inline-block"
                },
                hint: {
                    position: "absolute",
                    top: "0",
                    left: "0",
                    borderColor: "transparent",
                    boxShadow: "none"
                },
                query: {
                    position: "relative",
                    verticalAlign: "top",
                    backgroundColor: "transparent"
                },
                dropdown: {
                    position: "absolute",
                    top: "100%",
                    left: "0",
                    zIndex: "100",
                    display: "none"
                }
            };
        if (utils.isMsie()) {
            utils.mixin(css.query, {
                backgroundImage: "url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)"
            });
        }
        if (utils.isMsie() && utils.isMsie() <= 7) {
            utils.mixin(css.wrapper, {
                display: "inline",
                zoom: "1"
            });
            utils.mixin(css.query, {
                marginTop: "-1px"
            });
        }

        function TypeaheadView(o) {
            var $menu, $input, $hint;
            utils.bindAll(this);
            this.$node = buildDomStructure(o.input);
            this.datasets = o.datasets;
            this.dir = null;
            this.eventBus = o.eventBus;
            $menu = this.$node.find(".tt-dropdown-menu");
            $input = this.$node.find(".tt-query");
            $hint = this.$node.find(".tt-hint");
            this.dropdownView = new DropdownView({
                menu: $menu
            }).on("suggestionSelected", this._handleSelection).on("cursorMoved", this._clearHint).on("cursorMoved", this._setInputValueToSuggestionUnderCursor).on("cursorRemoved", this._setInputValueToQuery).on("cursorRemoved", this._updateHint).on("suggestionsRendered", this._updateHint).on("opened", this._updateHint).on("closed", this._clearHint).on("opened closed", this._propagateEvent);
            this.inputView = new InputView({
                input: $input,
                hint: $hint
            }).on("focused", this._openDropdown).on("blured", this._closeDropdown).on("blured", this._setInputValueToQuery).on("enterKeyed tabKeyed", this._handleSelection).on("queryChanged", this._clearHint).on("queryChanged", this._clearSuggestions).on("queryChanged", this._getSuggestions).on("whitespaceChanged", this._updateHint).on("queryChanged whitespaceChanged", this._openDropdown).on("queryChanged whitespaceChanged", this._setLanguageDirection).on("escKeyed", this._closeDropdown).on("escKeyed", this._setInputValueToQuery).on("tabKeyed upKeyed downKeyed", this._managePreventDefault).on("upKeyed downKeyed", this._moveDropdownCursor).on("upKeyed downKeyed", this._openDropdown).on("tabKeyed leftKeyed rightKeyed", this._autocomplete);
        }
        utils.mixin(TypeaheadView.prototype, EventTarget, {
            _managePreventDefault: function(e) {
                var $e = e.data,
                    hint, inputValue, preventDefault = false;
                switch (e.type) {
                    case "tabKeyed":
                        hint = this.inputView.getHintValue();
                        inputValue = this.inputView.getInputValue();
                        preventDefault = hint && hint !== inputValue;
                        break;
                    case "upKeyed":
                    case "downKeyed":
                        preventDefault = !$e.shiftKey && !$e.ctrlKey && !$e.metaKey;
                        break;
                }
                preventDefault && $e.preventDefault();
            },
            _setLanguageDirection: function() {
                var dir = this.inputView.getLanguageDirection();
                if (dir !== this.dir) {
                    this.dir = dir;
                    this.$node.css("direction", dir);
                    this.dropdownView.setLanguageDirection(dir);
                }
            },
            _updateHint: function() {
                var suggestion = this.dropdownView.getFirstSuggestion(),
                    hint = suggestion ? suggestion.value : null,
                    dropdownIsVisible = this.dropdownView.isVisible(),
                    inputHasOverflow = this.inputView.isOverflow(),
                    inputValue, query, escapedQuery, beginsWithQuery, match;
                if (hint && dropdownIsVisible && !inputHasOverflow) {
                    inputValue = this.inputView.getInputValue();
                    query = inputValue.replace(/\s{2,}/g, " ").replace(/^\s+/g, "");
                    escapedQuery = utils.escapeRegExChars(query);
                    beginsWithQuery = new RegExp("^(?:" + escapedQuery + ")(.*$)", "i");
                    match = beginsWithQuery.exec(hint);
                    this.inputView.setHintValue(inputValue + (match ? match[1] : ""));
                }
            },
            _clearHint: function() {
                this.inputView.setHintValue("");
            },
            _clearSuggestions: function() {
                this.dropdownView.clearSuggestions();
            },
            _setInputValueToQuery: function() {
                this.inputView.setInputValue(this.inputView.getQuery());
            },
            _setInputValueToSuggestionUnderCursor: function(e) {
                var suggestion = e.data;
                this.inputView.setInputValue(suggestion.value, true);
            },
            _openDropdown: function() {
                this.dropdownView.open();
            },
            _closeDropdown: function(e) {
                this.dropdownView[e.type === "blured" ? "closeUnlessMouseIsOverDropdown" : "close"]();
            },
            _moveDropdownCursor: function(e) {
                var $e = e.data;
                if (!$e.shiftKey && !$e.ctrlKey && !$e.metaKey) {
                    this.dropdownView[e.type === "upKeyed" ? "moveCursorUp" : "moveCursorDown"]();
                }
            },
            _handleSelection: function(e) {
                var byClick = e.type === "suggestionSelected",
                    suggestion = byClick ? e.data : this.dropdownView.getSuggestionUnderCursor();
                if (suggestion) {
                    this.inputView.setInputValue(suggestion.value);
                    byClick ? this.inputView.focus() : e.data.preventDefault();
                    byClick && utils.isMsie() ? utils.defer(this.dropdownView.close) : this.dropdownView.close();
                    this.eventBus.trigger("selected", suggestion.datum, suggestion.dataset);
                }
            },
            _getSuggestions: function() {
                var that = this,
                    query = this.inputView.getQuery();
                if (utils.isBlankString(query)) {
                    return;
                }
                utils.each(this.datasets, function(i, dataset) {
                    dataset.getSuggestions(query, function(suggestions) {
                        if (query === that.inputView.getQuery()) {
                            that.dropdownView.renderSuggestions(dataset, suggestions);
                        }
                    });
                });
            },
            _autocomplete: function(e) {
                var isCursorAtEnd, ignoreEvent, query, hint, suggestion;
                if (e.type === "rightKeyed" || e.type === "leftKeyed") {
                    isCursorAtEnd = this.inputView.isCursorAtEnd();
                    ignoreEvent = this.inputView.getLanguageDirection() === "ltr" ? e.type === "leftKeyed" : e.type === "rightKeyed";
                    if (!isCursorAtEnd || ignoreEvent) {
                        return;
                    }
                }
                query = this.inputView.getQuery();
                hint = this.inputView.getHintValue();
                if (hint !== "" && query !== hint) {
                    suggestion = this.dropdownView.getFirstSuggestion();
                    this.inputView.setInputValue(suggestion.value);
                    this.eventBus.trigger("autocompleted", suggestion.datum, suggestion.dataset);
                }
            },
            _propagateEvent: function(e) {
                this.eventBus.trigger(e.type);
            },
            destroy: function() {
                this.inputView.destroy();
                this.dropdownView.destroy();
                destroyDomStructure(this.$node);
                this.$node = null;
            },
            setQuery: function(query) {
                this.inputView.setQuery(query);
                this.inputView.setInputValue(query);
                this._clearHint();
                this._clearSuggestions();
                this._getSuggestions();
            }
        });
        return TypeaheadView;

        function buildDomStructure(input) {
            var $wrapper = $(html.wrapper),
                $dropdown = $(html.dropdown),
                $input = $(input),
                $hint = $(html.hint);
            $wrapper = $wrapper.css(css.wrapper);
            $dropdown = $dropdown.css(css.dropdown);
            $hint.css(css.hint).css({
                backgroundAttachment: $input.css("background-attachment"),
                backgroundClip: $input.css("background-clip"),
                backgroundColor: $input.css("background-color"),
                backgroundImage: $input.css("background-image"),
                backgroundOrigin: $input.css("background-origin"),
                backgroundPosition: $input.css("background-position"),
                backgroundRepeat: $input.css("background-repeat"),
                backgroundSize: $input.css("background-size")
            });
            $input.data("ttAttrs", {
                dir: $input.attr("dir"),
                autocomplete: $input.attr("autocomplete"),
                spellcheck: $input.attr("spellcheck"),
                style: $input.attr("style")
            });
            $input.addClass("tt-query").attr({
                autocomplete: "off",
                spellcheck: false
            }).css(css.query);
            try {
                !$input.attr("dir") && $input.attr("dir", "auto");
            } catch (e) {}
            return $input.wrap($wrapper).parent().prepend($hint).append($dropdown);
        }

        function destroyDomStructure($node) {
            var $input = $node.find(".tt-query");
            utils.each($input.data("ttAttrs"), function(key, val) {
                utils.isUndefined(val) ? $input.removeAttr(key) : $input.attr(key, val);
            });
            $input.detach().removeData("ttAttrs").removeClass("tt-query").insertAfter($node);
            $node.remove();
        }
    }();
    (function() {
        var cache = {},
            viewKey = "ttView",
            methods;
        methods = {
            initialize: function(datasetDefs) {
                var datasets;
                datasetDefs = utils.isArray(datasetDefs) ? datasetDefs : [datasetDefs];
                if (datasetDefs.length === 0) {
                    $.error("no datasets provided");
                }
                datasets = utils.map(datasetDefs, function(o) {
                    var dataset = cache[o.name] ? cache[o.name] : new Dataset(o);
                    if (o.name) {
                        cache[o.name] = dataset;
                    }
                    return dataset;
                });
                return this.each(initialize);

                function initialize() {
                    var $input = $(this),
                        deferreds, eventBus = new EventBus({
                            el: $input
                        });
                    deferreds = utils.map(datasets, function(dataset) {
                        return dataset.initialize();
                    });
                    $input.data(viewKey, new TypeaheadView({
                        input: $input,
                        eventBus: eventBus = new EventBus({
                            el: $input
                        }),
                        datasets: datasets
                    }));
                    $.when.apply($, deferreds).always(function() {
                        utils.defer(function() {
                            eventBus.trigger("initialized");
                        });
                    });
                }
            },
            destroy: function() {
                return this.each(destroy);

                function destroy() {
                    var $this = $(this),
                        view = $this.data(viewKey);
                    if (view) {
                        view.destroy();
                        $this.removeData(viewKey);
                    }
                }
            },
            setQuery: function(query) {
                return this.each(setQuery);

                function setQuery() {
                    var view = $(this).data(viewKey);
                    view && view.setQuery(query);
                }
            }
        };
        jQuery.fn.typeahead = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, [].slice.call(arguments, 1));
            } else {
                return methods.initialize.apply(this, arguments);
            }
        };
    })();
})(window.jQuery);

/*===============================
/templates/ja_playmag/js/hogan-2.0.0.js
================================================================================*/
;
var Hogan = {};
(function(Hogan, useArrayBuffer) {
    Hogan.Template = function(renderFunc, text, compiler, options) {
        this.r = renderFunc || this.r;
        this.c = compiler;
        this.options = options;
        this.text = text || '';
        this.buf = (useArrayBuffer) ? [] : '';
    }
    Hogan.Template.prototype = {
        r: function(context, partials, indent) {
            return '';
        },
        v: hoganEscape,
        t: coerceToString,
        render: function render(context, partials, indent) {
            return this.ri([context], partials || {}, indent);
        },
        ri: function(context, partials, indent) {
            return this.r(context, partials, indent);
        },
        rp: function(name, context, partials, indent) {
            var partial = partials[name];
            if (!partial) {
                return '';
            }
            if (this.c && typeof partial == 'string') {
                partial = this.c.compile(partial, this.options);
            }
            return partial.ri(context, partials, indent);
        },
        rs: function(context, partials, section) {
            var tail = context[context.length - 1];
            if (!isArray(tail)) {
                section(context, partials, this);
                return;
            }
            for (var i = 0; i < tail.length; i++) {
                context.push(tail[i]);
                section(context, partials, this);
                context.pop();
            }
        },
        s: function(val, ctx, partials, inverted, start, end, tags) {
            var pass;
            if (isArray(val) && val.length === 0) {
                return false;
            }
            if (typeof val == 'function') {
                val = this.ls(val, ctx, partials, inverted, start, end, tags);
            }
            pass = (val === '') || !!val;
            if (!inverted && pass && ctx) {
                ctx.push((typeof val == 'object') ? val : ctx[ctx.length - 1]);
            }
            return pass;
        },
        d: function(key, ctx, partials, returnFound) {
            var names = key.split('.'),
                val = this.f(names[0], ctx, partials, returnFound),
                cx = null;
            if (key === '.' && isArray(ctx[ctx.length - 2])) {
                return ctx[ctx.length - 1];
            }
            for (var i = 1; i < names.length; i++) {
                if (val && typeof val == 'object' && names[i] in val) {
                    cx = val;
                    val = val[names[i]];
                } else {
                    val = '';
                }
            }
            if (returnFound && !val) {
                return false;
            }
            if (!returnFound && typeof val == 'function') {
                ctx.push(cx);
                val = this.lv(val, ctx, partials);
                ctx.pop();
            }
            return val;
        },
        f: function(key, ctx, partials, returnFound) {
            var val = false,
                v = null,
                found = false;
            for (var i = ctx.length - 1; i >= 0; i--) {
                v = ctx[i];
                if (v && typeof v == 'object' && key in v) {
                    val = v[key];
                    found = true;
                    break;
                }
            }
            if (!found) {
                return (returnFound) ? false : "";
            }
            if (!returnFound && typeof val == 'function') {
                val = this.lv(val, ctx, partials);
            }
            return val;
        },
        ho: function(val, cx, partials, text, tags) {
            var compiler = this.c;
            var options = this.options;
            options.delimiters = tags;
            var text = val.call(cx, text);
            text = (text == null) ? String(text) : text.toString();
            this.b(compiler.compile(text, options).render(cx, partials));
            return false;
        },
        b: (useArrayBuffer) ? function(s) {
            this.buf.push(s);
        } : function(s) {
            this.buf += s;
        },
        fl: (useArrayBuffer) ? function() {
            var r = this.buf.join('');
            this.buf = [];
            return r;
        } : function() {
            var r = this.buf;
            this.buf = '';
            return r;
        },
        ls: function(val, ctx, partials, inverted, start, end, tags) {
            var cx = ctx[ctx.length - 1],
                t = null;
            if (!inverted && this.c && val.length > 0) {
                return this.ho(val, cx, partials, this.text.substring(start, end), tags);
            }
            t = val.call(cx);
            if (typeof t == 'function') {
                if (inverted) {
                    return true;
                } else if (this.c) {
                    return this.ho(t, cx, partials, this.text.substring(start, end), tags);
                }
            }
            return t;
        },
        lv: function(val, ctx, partials) {
            var cx = ctx[ctx.length - 1];
            var result = val.call(cx);
            if (typeof result == 'function') {
                result = coerceToString(result.call(cx));
                if (this.c && ~result.indexOf("{\u007B")) {
                    return this.c.compile(result, this.options).render(cx, partials);
                }
            }
            return coerceToString(result);
        }
    };
    var rAmp = /&/g,
        rLt = /</g,
        rGt = />/g,
        rApos = /\'/g,
        rQuot = /\"/g,
        hChars = /[&<>\"\']/;

    function coerceToString(val) {
        return String((val === null || val === undefined) ? '' : val);
    }

    function hoganEscape(str) {
        str = coerceToString(str);
        return hChars.test(str) ? str.replace(rAmp, '&amp;').replace(rLt, '&lt;').replace(rGt, '&gt;').replace(rApos, '&#39;').replace(rQuot, '&quot;') : str;
    }
    var isArray = Array.isArray || function(a) {
        return Object.prototype.toString.call(a) === '[object Array]';
    };
})(typeof exports !== 'undefined' ? exports : Hogan);
(function(Hogan) {
    var rIsWhitespace = /\S/,
        rQuot = /\"/g,
        rNewline = /\n/g,
        rCr = /\r/g,
        rSlash = /\\/g,
        tagTypes = {
            '#': 1,
            '^': 2,
            '/': 3,
            '!': 4,
            '>': 5,
            '<': 6,
            '=': 7,
            '_v': 8,
            '{': 9,
            '&': 10
        };
    Hogan.scan = function scan(text, delimiters) {
        var len = text.length,
            IN_TEXT = 0,
            IN_TAG_TYPE = 1,
            IN_TAG = 2,
            state = IN_TEXT,
            tagType = null,
            tag = null,
            buf = '',
            tokens = [],
            seenTag = false,
            i = 0,
            lineStart = 0,
            otag = '{{',
            ctag = '}}';

        function addBuf() {
            if (buf.length > 0) {
                tokens.push(new String(buf));
                buf = '';
            }
        }

        function lineIsWhitespace() {
            var isAllWhitespace = true;
            for (var j = lineStart; j < tokens.length; j++) {
                isAllWhitespace = (tokens[j].tag && tagTypes[tokens[j].tag] < tagTypes['_v']) || (!tokens[j].tag && tokens[j].match(rIsWhitespace) === null);
                if (!isAllWhitespace) {
                    return false;
                }
            }
            return isAllWhitespace;
        }

        function filterLine(haveSeenTag, noNewLine) {
            addBuf();
            if (haveSeenTag && lineIsWhitespace()) {
                for (var j = lineStart, next; j < tokens.length; j++) {
                    if (!tokens[j].tag) {
                        if ((next = tokens[j + 1]) && next.tag == '>') {
                            next.indent = tokens[j].toString()
                        }
                        tokens.splice(j, 1);
                    }
                }
            } else if (!noNewLine) {
                tokens.push({
                    tag: '\n'
                });
            }
            seenTag = false;
            lineStart = tokens.length;
        }

        function changeDelimiters(text, index) {
            var close = '=' + ctag,
                closeIndex = text.indexOf(close, index),
                delimiters = trim(text.substring(text.indexOf('=', index) + 1, closeIndex)).split(' ');
            otag = delimiters[0];
            ctag = delimiters[1];
            return closeIndex + close.length - 1;
        }
        if (delimiters) {
            delimiters = delimiters.split(' ');
            otag = delimiters[0];
            ctag = delimiters[1];
        }
        for (i = 0; i < len; i++) {
            if (state == IN_TEXT) {
                if (tagChange(otag, text, i)) {
                    --i;
                    addBuf();
                    state = IN_TAG_TYPE;
                } else {
                    if (text.charAt(i) == '\n') {
                        filterLine(seenTag);
                    } else {
                        buf += text.charAt(i);
                    }
                }
            } else if (state == IN_TAG_TYPE) {
                i += otag.length - 1;
                tag = tagTypes[text.charAt(i + 1)];
                tagType = tag ? text.charAt(i + 1) : '_v';
                if (tagType == '=') {
                    i = changeDelimiters(text, i);
                    state = IN_TEXT;
                } else {
                    if (tag) {
                        i++;
                    }
                    state = IN_TAG;
                }
                seenTag = i;
            } else {
                if (tagChange(ctag, text, i)) {
                    tokens.push({
                        tag: tagType,
                        n: trim(buf),
                        otag: otag,
                        ctag: ctag,
                        i: (tagType == '/') ? seenTag - ctag.length : i + otag.length
                    });
                    buf = '';
                    i += ctag.length - 1;
                    state = IN_TEXT;
                    if (tagType == '{') {
                        if (ctag == '}}') {
                            i++;
                        } else {
                            cleanTripleStache(tokens[tokens.length - 1]);
                        }
                    }
                } else {
                    buf += text.charAt(i);
                }
            }
        }
        filterLine(seenTag, true);
        return tokens;
    }

    function cleanTripleStache(token) {
        if (token.n.substr(token.n.length - 1) === '}') {
            token.n = token.n.substring(0, token.n.length - 1);
        }
    }

    function trim(s) {
        if (s.trim) {
            return s.trim();
        }
        return s.replace(/^\s*|\s*$/g, '');
    }

    function tagChange(tag, text, index) {
        if (text.charAt(index) != tag.charAt(0)) {
            return false;
        }
        for (var i = 1, l = tag.length; i < l; i++) {
            if (text.charAt(index + i) != tag.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    function buildTree(tokens, kind, stack, customTags) {
        var instructions = [],
            opener = null,
            token = null;
        while (tokens.length > 0) {
            token = tokens.shift();
            if (token.tag == '#' || token.tag == '^' || isOpener(token, customTags)) {
                stack.push(token);
                token.nodes = buildTree(tokens, token.tag, stack, customTags);
                instructions.push(token);
            } else if (token.tag == '/') {
                if (stack.length === 0) {
                    throw new Error('Closing tag without opener: /' + token.n);
                }
                opener = stack.pop();
                if (token.n != opener.n && !isCloser(token.n, opener.n, customTags)) {
                    throw new Error('Nesting error: ' + opener.n + ' vs. ' + token.n);
                }
                opener.end = token.i;
                return instructions;
            } else {
                instructions.push(token);
            }
        }
        if (stack.length > 0) {
            throw new Error('missing closing tag: ' + stack.pop().n);
        }
        return instructions;
    }

    function isOpener(token, tags) {
        for (var i = 0, l = tags.length; i < l; i++) {
            if (tags[i].o == token.n) {
                token.tag = '#';
                return true;
            }
        }
    }

    function isCloser(close, open, tags) {
        for (var i = 0, l = tags.length; i < l; i++) {
            if (tags[i].c == close && tags[i].o == open) {
                return true;
            }
        }
    }
    Hogan.generate = function(tree, text, options) {
        var code = 'var _=this;_.b(i=i||"");' + walk(tree) + 'return _.fl();';
        if (options.asString) {
            return 'function(c,p,i){' + code + ';}';
        }
        return new Hogan.Template(new Function('c', 'p', 'i', code), text, Hogan, options);
    }

    function esc(s) {
        return s.replace(rSlash, '\\\\').replace(rQuot, '\\\"').replace(rNewline, '\\n').replace(rCr, '\\r');
    }

    function chooseMethod(s) {
        return (~s.indexOf('.')) ? 'd' : 'f';
    }

    function walk(tree) {
        var code = '';
        for (var i = 0, l = tree.length; i < l; i++) {
            var tag = tree[i].tag;
            if (tag == '#') {
                code += section(tree[i].nodes, tree[i].n, chooseMethod(tree[i].n), tree[i].i, tree[i].end, tree[i].otag + " " + tree[i].ctag);
            } else if (tag == '^') {
                code += invertedSection(tree[i].nodes, tree[i].n, chooseMethod(tree[i].n));
            } else if (tag == '<' || tag == '>') {
                code += partial(tree[i]);
            } else if (tag == '{' || tag == '&') {
                code += tripleStache(tree[i].n, chooseMethod(tree[i].n));
            } else if (tag == '\n') {
                code += text('"\\n"' + (tree.length - 1 == i ? '' : ' + i'));
            } else if (tag == '_v') {
                code += variable(tree[i].n, chooseMethod(tree[i].n));
            } else if (tag === undefined) {
                code += text('"' + esc(tree[i]) + '"');
            }
        }
        return code;
    }

    function section(nodes, id, method, start, end, tags) {
        return 'if(_.s(_.' + method + '("' + esc(id) + '",c,p,1),' + 'c,p,0,' + start + ',' + end + ',"' + tags + '")){' + '_.rs(c,p,' + 'function(c,p,_){' +
            walk(nodes) + '});c.pop();}';
    }

    function invertedSection(nodes, id, method) {
        return 'if(!_.s(_.' + method + '("' + esc(id) + '",c,p,1),c,p,1,0,0,"")){' +
            walk(nodes) + '};';
    }

    function partial(tok) {
        return '_.b(_.rp("' + esc(tok.n) + '",c,p,"' + (tok.indent || '') + '"));';
    }

    function tripleStache(id, method) {
        return '_.b(_.t(_.' + method + '("' + esc(id) + '",c,p,0)));';
    }

    function variable(id, method) {
        return '_.b(_.v(_.' + method + '("' + esc(id) + '",c,p,0)));';
    }

    function text(id) {
        return '_.b(' + id + ');';
    }
    Hogan.parse = function(tokens, text, options) {
        options = options || {};
        return buildTree(tokens, '', [], options.sectionTags || []);
    }, Hogan.cache = {};
    Hogan.compile = function(text, options) {
        options = options || {};
        var key = text + '||' + !!options.asString;
        var t = this.cache[key];
        if (t) {
            return t;
        }
        t = this.generate(this.parse(this.scan(text, options.delimiters), text, options), text, options);
        return this.cache[key] = t;
    };
})(typeof exports !== 'undefined' ? exports : Hogan);

/*===============================
https://www.equipmentone.com/e1_2/js/jQueryUtils.js
================================================================================*/
;
jQuery.extend({
    keys: function(obj) {
        var arrKeys = [];
        $.each(obj, function(key) {
            arrKeys.push(key)
        });
        return arrKeys;
    },
    sortByKeys: function(obj) {
        var objReturn = {};
        var objKeyMap = {};
        var arrKeys = $.keys(obj);
        $.each(arrKeys, function(index, key) {
            arrKeys[index] = key.replace(/^[a-z]/, function(m) {
                return m.toUpperCase()
            });
            objKeyMap[arrKeys[index]] = key;
        });
        arrKeys.sort();
        $.each(arrKeys, function(index, key) {
            objReturn[key] = {
                searchKey: objKeyMap[key],
                count: obj[objKeyMap[key]]
            };
        });
        return objReturn;
    },
    numToOrd: function(obj) {
        var s = String(obj),
            len = s.length,
            end = s.substr(len - 1, 1),
            teen = len > 1 && s.substr(len - 2, 1) === "1",
            ord = "th";
        if (end === "1" && !teen) {
            ord = "st";
        } else if (end === "2" && !teen) {
            ord = "nd";
        } else if (end === "3" && !teen) {
            ord = "rd";
        }
        return obj + ord;
    }
});

/*===============================
https://www.equipmentone.com/e1_2/js/ajaxUtils.js
================================================================================*/
;
AJAX_ERROR = -1;
AJAX_EMPTY = null;
AJAX_ERROR_LIST = [];

function fncAjax(data) {
    if (data.data == undefined) {
        data.data = {};
    }
    if (typeof data.timeout == undefined) {
        data.timeout = 5000;
    }
    if (typeof data.url == undefined) {
        data.url = '/e1_2/ajax/MyOne.php';
    }
    $.ajax(data);
};

function fncAjaxResponse($response, params) {
    var source = params.source;
    var loginRequired = params.loginRequired;
    var parseJson = params.parseJson;
    if (loginRequired == undefined) {
        loginRequired = false;
    }
    if (parseJson == false) {
        return $response;
    }
    if ($response == null || $response == false) {
        return AJAX_ERROR;
    }
    if (typeof $response == 'string') {
        $response = $.parseJSON($response);
    }
    if ($response.e1_status == 'success') {
        $('#errorMessage').hide();
        if ($response.e1_content == null || $response.e1_content == false) {
            return AJAX_ERROR;
        }
        return $response.e1_content;
    } else {
        $('#errorMessage').show();
        AJAX_ERROR_LIST[AJAX_ERROR_LIST.length] = [params.source, $response.e1_error];
    }
    if (loginRequired && $response.e1_visitor == true) {
        window.location = '/login?timeout=true';
    }
    return AJAX_ERROR;
}

/*===============================
https://www.equipmentone.com/e1_2/js/pubnub-3.7.1.min.js
================================================================================*/
;
// Version: 3.7.1
(function() {
    var aa = void 0,
        v = !0,
        z = null,
        A = !1;

    function C() {
        return function() {}
    }
    window.JSON && window.JSON.stringify || function() {
        function a() {
            try {
                return this.valueOf()
            } catch (a) {
                return z
            }
        }

        function d(a) {
            c.lastIndex = 0;
            return c.test(a) ? '"' + a.replace(c, function(a) {
                var b = q[a];
                return "string" === typeof b ? b : "\\u" + ("0000" + a.charCodeAt(0).toString(16)).slice(-4)
            }) + '"' : '"' + a + '"'
        }

        function b(c, q) {
            var s, r, f, h, j, m = e,
                l = q[c];
            l && "object" === typeof l && (l = a.call(l));
            "function" === typeof g && (l = g.call(q, c, l));
            switch (typeof l) {
                case "string":
                    return d(l);
                case "number":
                    return isFinite(l) ? String(l) : "null";
                case "boolean":
                case "null":
                    return String(l);
                case "object":
                    if (!l) return "null";
                    e += p;
                    j = [];
                    if ("[object Array]" === Object.prototype.toString.apply(l)) {
                        h = l.length;
                        for (s = 0; s < h; s += 1) j[s] = b(s, l) || "null";
                        f = 0 === j.length ? "[]" : e ? "[\n" + e + j.join(",\n" + e) + "\n" + m + "]" : "[" + j.join(",") + "]";
                        e = m;
                        return f
                    }
                    if (g && "object" === typeof g) {
                        h = g.length;
                        for (s = 0; s < h; s += 1) r = g[s], "string" === typeof r && (f = b(r, l)) && j.push(d(r) + (e ? ": " : ":") + f)
                    } else
                        for (r in l) Object.hasOwnProperty.call(l, r) && (f = b(r, l)) && j.push(d(r) + (e ? ": " : ":") + f);
                    f = 0 === j.length ? "{}" : e ? "{\n" + e + j.join(",\n" + e) + "\n" +
                        m + "}" : "{" + j.join(",") + "}";
                    e = m;
                    return f
            }
        }
        window.JSON || (window.JSON = {});
        var c = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
            e, p, q = {
                "\b": "\\b",
                "\t": "\\t",
                "\n": "\\n",
                "\f": "\\f",
                "\r": "\\r",
                '"': '\\"',
                "\\": "\\\\"
            },
            g;
        "function" !== typeof JSON.stringify && (JSON.stringify = function(a, c, d) {
            var q;
            p = e = "";
            if ("number" === typeof d)
                for (q = 0; q < d; q += 1) p += " ";
            else "string" === typeof d && (p = d);
            if ((g = c) && "function" !== typeof c && ("object" !== typeof c || "number" !==
                    typeof c.length)) throw Error("JSON.stringify");
            return b("", {
                "": a
            })
        });
        "function" !== typeof JSON.parse && (JSON.parse = function(a) {
            return eval("(" + a + ")")
        })
    }();
    var ba = 1,
        ea = A,
        ha = [],
        ia = "-pnpres",
        D = 1E3,
        ja = "/",
        ka = "&",
        la = /{([\w\-]+)}/g;

    function ma() {
        return "x" + ++ba + "" + +new Date
    }

    function E() {
        return +new Date
    }
    var na, pa = Math.floor(20 * Math.random());
    na = function(a, d) {
        return 0 < a.indexOf("pubsub.") && a.replace("pubsub", "ps" + (d ? qa().split("-")[0] : 20 > ++pa ? pa : pa = 1)) || a
    };

    function ra(a, d) {
        var b = a.join(ja),
            c = [];
        if (!d) return b;
        H(d, function(a, b) {
            var d = "object" == typeof b ? JSON.stringify(b) : b;
            "undefined" != typeof b && (b != z && 0 < encodeURIComponent(d).length) && c.push(a + "=" + encodeURIComponent(d))
        });
        return b += "?" + c.join(ka)
    }

    function sa(a, d) {
        function b() {
            e + d > E() ? (clearTimeout(c), c = setTimeout(b, d)) : (e = E(), a())
        }
        var c, e = 0;
        return b
    }

    function ta(a, d) {
        var b = [];
        H(a || [], function(a) {
            d(a) && b.push(a)
        });
        return b
    }

    function ua(a, d) {
        return a.replace(la, function(a, c) {
            return d[c] || a
        })
    }

    function qa(a) {
        var d = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(a) {
            var c = 16 * Math.random() | 0;
            return ("x" == a ? c : c & 3 | 8).toString(16)
        });
        a && a(d);
        return d
    }

    function H(a, d) {
        if (a && d)
            if (a && Array.isArray && Array.isArray(a))
                for (var b = 0, c = a.length; b < c;) d.call(a[b], a[b], b++);
            else
                for (b in a) a.hasOwnProperty && a.hasOwnProperty(b) && d.call(a[b], b, a[b])
    }

    function va(a, d) {
        var b = [];
        H(a || [], function(a, e) {
            b.push(d(a, e))
        });
        return b
    }

    function wa(a, d) {
        var b = [];
        H(a, function(a, e) {
            d ? 0 > a.search("-pnpres") && e.h && b.push(a) : e.h && b.push(a)
        });
        return b.sort()
    }

    function xa(a, d) {
        var b = [];
        H(a, function(a, e) {
            d ? 0 > channel.search("-pnpres") && e.h && b.push(a) : e.h && b.push(a)
        });
        return b.sort()
    }

    function ya() {
        setTimeout(function() {
            ea || (ea = 1, H(ha, function(a) {
                a()
            }))
        }, D)
    }
    var L, R = 14,
        S = 8,
        za = A;

    function Ba(a, d) {
        var b = "",
            c, e;
        if (d) {
            c = a[15];
            if (16 < c) throw "Decryption error: Maybe bad key";
            if (16 == c) return "";
            for (e = 0; e < 16 - c; e++) b += String.fromCharCode(a[e])
        } else
            for (e = 0; 16 > e; e++) b += String.fromCharCode(a[e]);
        return b
    }

    function Ca(a, d) {
        var b = [],
            c;
        if (!d) try {
            a = unescape(encodeURIComponent(a))
        } catch (e) {
            throw "Error on UTF-8 encode";
        }
        for (c = 0; c < a.length; c++) b[c] = a.charCodeAt(c);
        return b
    }

    function Da(a, d) {
        var b = 12 <= R ? 3 : 2,
            c = [],
            e = [],
            c = [],
            e = [],
            p = a.concat(d),
            q;
        c[0] = GibberishAES.Q.S(p);
        e = c[0];
        for (q = 1; q < b; q++) c[q] = GibberishAES.Q.S(c[q - 1].concat(p)), e = e.concat(c[q]);
        c = e.slice(0, 4 * S);
        e = e.slice(4 * S, 4 * S + 16);
        return {
            key: c,
            J: e
        }
    }

    function Ea(a, d, b) {
        var d = Fa(d),
            c = Math.ceil(a.length / 16),
            e = [],
            p, q = [];
        for (p = 0; p < c; p++) {
            var g = e,
                t = p,
                x = a.slice(16 * p, 16 * p + 16),
                s = [],
                r = aa,
                r = aa;
            16 > x.length && (r = 16 - x.length, s = [r, r, r, r, r, r, r, r, r, r, r, r, r, r, r, r]);
            for (r = 0; r < x.length; r++) s[r] = x[r];
            g[t] = s
        }
        0 === a.length % 16 && e.push([16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16]);
        for (p = 0; p < e.length; p++) e[p] = 0 === p ? Ga(e[p], b) : Ga(e[p], q[p - 1]), q[p] = Ha(e[p], d);
        return q
    }

    function Ia(a, d, b, c) {
        var d = Fa(d),
            e = a.length / 16,
            p = [],
            q, g = [],
            t = "";
        for (q = 0; q < e; q++) p.push(a.slice(16 * q, 16 * (q + 1)));
        for (q = p.length - 1; 0 <= q; q--) g[q] = Ja(p[q], d), g[q] = 0 === q ? Ga(g[q], b) : Ga(g[q], p[q - 1]);
        for (q = 0; q < e - 1; q++) t += Ba(g[q]);
        var t = t + Ba(g[q], v),
            x;
        if (c) x = t;
        else try {
            x = decodeURIComponent(escape(t))
        } catch (s) {
            throw "Bad Key";
        }
        return x
    }

    function Ha(a, d) {
        za = A;
        var b = Ka(a, d, 0),
            c;
        for (c = 1; c < R + 1; c++) b = La(b), b = Ma(b), c < R && (b = Na(b)), b = Ka(b, d, c);
        return b
    }

    function Ja(a, d) {
        za = v;
        var b = Ka(a, d, R),
            c;
        for (c = R - 1; - 1 < c; c--) b = Ma(b), b = La(b), b = Ka(b, d, c), 0 < c && (b = Na(b));
        return b
    }

    function La(a) {
        var d = za ? Oa : Pa,
            b = [],
            c;
        for (c = 0; 16 > c; c++) b[c] = d[a[c]];
        return b
    }

    function Ma(a) {
        var d = [],
            b = za ? [0, 13, 10, 7, 4, 1, 14, 11, 8, 5, 2, 15, 12, 9, 6, 3] : [0, 5, 10, 15, 4, 9, 14, 3, 8, 13, 2, 7, 12, 1, 6, 11],
            c;
        for (c = 0; 16 > c; c++) d[c] = a[b[c]];
        return d
    }

    function Na(a) {
        var d = [],
            b;
        if (za)
            for (b = 0; 4 > b; b++) d[4 * b] = Qa[a[4 * b]] ^ Sa[a[1 + 4 * b]] ^ Ta[a[2 + 4 * b]] ^ Ua[a[3 + 4 * b]], d[1 + 4 * b] = Ua[a[4 * b]] ^ Qa[a[1 + 4 * b]] ^ Sa[a[2 + 4 * b]] ^ Ta[a[3 + 4 * b]], d[2 + 4 * b] = Ta[a[4 * b]] ^ Ua[a[1 + 4 * b]] ^ Qa[a[2 + 4 * b]] ^ Sa[a[3 + 4 * b]], d[3 + 4 * b] = Sa[a[4 * b]] ^ Ta[a[1 + 4 * b]] ^ Ua[a[2 + 4 * b]] ^ Qa[a[3 + 4 * b]];
        else
            for (b = 0; 4 > b; b++) d[4 * b] = Va[a[4 * b]] ^ bb[a[1 + 4 * b]] ^ a[2 + 4 * b] ^ a[3 + 4 * b], d[1 + 4 * b] = a[4 * b] ^ Va[a[1 + 4 * b]] ^ bb[a[2 + 4 * b]] ^ a[3 + 4 * b], d[2 + 4 * b] = a[4 * b] ^ a[1 + 4 * b] ^ Va[a[2 + 4 * b]] ^ bb[a[3 + 4 * b]], d[3 + 4 * b] = bb[a[4 * b]] ^ a[1 + 4 * b] ^ a[2 + 4 * b] ^ Va[a[3 + 4 *
                b]];
        return d
    }

    function Ka(a, d, b) {
        var c = [],
            e;
        for (e = 0; 16 > e; e++) c[e] = a[e] ^ d[b][e];
        return c
    }

    function Ga(a, d) {
        var b = [],
            c;
        for (c = 0; 16 > c; c++) b[c] = a[c] ^ d[c];
        return b
    }

    function Fa(a) {
        var d = [],
            b = [],
            c, e, p = [];
        for (c = 0; c < S; c++) e = [a[4 * c], a[4 * c + 1], a[4 * c + 2], a[4 * c + 3]], d[c] = e;
        for (c = S; c < 4 * (R + 1); c++) {
            d[c] = [];
            for (a = 0; 4 > a; a++) b[a] = d[c - 1][a];
            if (0 === c % S) {
                a = b[0];
                e = aa;
                for (e = 0; 4 > e; e++) b[e] = b[e + 1];
                b[3] = a;
                b = cb(b);
                b[0] ^= db[c / S - 1]
            } else 6 < S && 4 == c % S && (b = cb(b));
            for (a = 0; 4 > a; a++) d[c][a] = d[c - S][a] ^ b[a]
        }
        for (c = 0; c < R + 1; c++) {
            p[c] = [];
            for (b = 0; 4 > b; b++) p[c].push(d[4 * c + b][0], d[4 * c + b][1], d[4 * c + b][2], d[4 * c + b][3])
        }
        return p
    }

    function cb(a) {
        for (var d = 0; 4 > d; d++) a[d] = Pa[a[d]];
        return a
    }

    function eb(a, d) {
        var b = [];
        for (i = 0; i < a.length; i += d) b[i / d] = parseInt(a.substr(i, d), 16);
        return b
    }

    function fb(a) {
        for (var d = [], b = 0; 256 > b; b++) {
            for (var c = a, e = b, p = aa, q = aa, p = q = 0; 8 > p; p++) q = 1 == (e & 1) ? q ^ c : q, c = 127 < c ? 283 ^ c << 1 : c << 1, e >>>= 1;
            d[b] = q
        }
        return d
    }
    var Pa = eb("637c777bf26b6fc53001672bfed7ab76ca82c97dfa5947f0add4a2af9ca472c0b7fd9326363ff7cc34a5e5f171d8311504c723c31896059a071280e2eb27b27509832c1a1b6e5aa0523bd6b329e32f8453d100ed20fcb15b6acbbe394a4c58cfd0efaafb434d338545f9027f503c9fa851a3408f929d38f5bcb6da2110fff3d2cd0c13ec5f974417c4a77e3d645d197360814fdc222a908846eeb814de5e0bdbe0323a0a4906245cc2d3ac629195e479e7c8376d8dd54ea96c56f4ea657aae08ba78252e1ca6b4c6e8dd741f4bbd8b8a703eb5664803f60e613557b986c11d9ee1f8981169d98e949b1e87e9ce5528df8ca1890dbfe6426841992d0fb054bb16", 2),
        Oa, gb = Pa,
        hb = [];
    for (i = 0; i < gb.length; i++) hb[gb[i]] = i;
    Oa = hb;
    var db = eb("01020408102040801b366cd8ab4d9a2f5ebc63c697356ad4b37dfaefc591", 2),
        Va = fb(2),
        bb = fb(3),
        Ua = fb(9),
        Sa = fb(11),
        Ta = fb(13),
        Qa = fb(14),
        ib, jb = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
        lb = jb.split("");
    "function" === typeof Array.indexOf && (jb = lb);
    ib = {
        encode: function(a) {
            var d = [],
                b = "",
                c;
            for (c = 0; c < 16 * a.length; c++) d.push(a[Math.floor(c / 16)][c % 16]);
            for (c = 0; c < d.length; c += 3) b += lb[d[c] >> 2], b += lb[(d[c] & 3) << 4 | d[c + 1] >> 4], b = d[c + 1] !== aa ? b + lb[(d[c + 1] & 15) << 2 | d[c + 2] >> 6] : b + "=", b = d[c + 2] !== aa ? b + lb[d[c + 2] & 63] : b + "=";
            a = b.slice(0, 64);
            for (c = 1; c < Math.ceil(b.length / 64); c++) a += b.slice(64 * c, 64 * c + 64) + (Math.ceil(b.length / 64) == c + 1 ? "" : "\n");
            return a
        },
        decode: function(a) {
            var a = a.replace(/\n/g, ""),
                d = [],
                b = [],
                c = [],
                e;
            for (e = 0; e < a.length; e += 4) b[0] = jb.indexOf(a.charAt(e)), b[1] = jb.indexOf(a.charAt(e +
                1)), b[2] = jb.indexOf(a.charAt(e + 2)), b[3] = jb.indexOf(a.charAt(e + 3)), c[0] = b[0] << 2 | b[1] >> 4, c[1] = (b[1] & 15) << 4 | b[2] >> 2, c[2] = (b[2] & 3) << 6 | b[3], d.push(c[0], c[1], c[2]);
            return d = d.slice(0, d.length - d.length % 16)
        }
    };
    L = {
        size: function(a) {
            switch (a) {
                case 128:
                    R = 10;
                    S = 4;
                    break;
                case 192:
                    R = 12;
                    S = 6;
                    break;
                case 256:
                    R = 14;
                    S = 8;
                    break;
                default:
                    throw "Invalid Key Size Specified:" + a;
            }
        },
        h2a: function(a) {
            var d = [];
            a.replace(/(..)/g, function(a) {
                d.push(parseInt(a, 16))
            });
            return d
        },
        expandKey: Fa,
        encryptBlock: Ha,
        decryptBlock: Ja,
        Decrypt: za,
        s2a: Ca,
        rawEncrypt: Ea,
        rawDecrypt: Ia,
        dec: function(a, d, b) {
            var a = ib.ja(a),
                c = a.slice(8, 16),
                c = Da(Ca(d, b), c),
                d = c.key,
                c = c.J,
                a = a.slice(16, a.length);
            return a = Ia(a, d, c, b)
        },
        openSSLKey: Da,
        a2h: function(a) {
            var d = "",
                b;
            for (b =
                0; b < a.length; b++) d += (16 > a[b] ? "0" : "") + a[b].toString(16);
            return d
        },
        enc: function(a, d, b) {
            var c;
            c = [];
            var e;
            for (e = 0; 8 > e; e++) c = c.concat(Math.floor(256 * Math.random()));
            e = Da(Ca(d, b), c);
            d = e.key;
            e = e.J;
            c = [
                [83, 97, 108, 116, 101, 100, 95, 95].concat(c)
            ];
            a = Ca(a, b);
            a = Ea(a, d, e);
            a = c.concat(a);
            return ib.ka(a)
        },
        Hash: {
            MD5: function(a) {
                function d(a, b) {
                    var d, c, e, f, g;
                    e = a & 2147483648;
                    f = b & 2147483648;
                    d = a & 1073741824;
                    c = b & 1073741824;
                    g = (a & 1073741823) + (b & 1073741823);
                    return d & c ? g ^ 2147483648 ^ e ^ f : d | c ? g & 1073741824 ? g ^ 3221225472 ^ e ^ f : g ^ 1073741824 ^
                        e ^ f : g ^ e ^ f
                }

                function b(a, b, c, e, f, g, l) {
                    a = d(a, d(d(b & c | ~b & e, f), l));
                    return d(a << g | a >>> 32 - g, b)
                }

                function c(a, b, c, e, f, g, l) {
                    a = d(a, d(d(b & e | c & ~e, f), l));
                    return d(a << g | a >>> 32 - g, b)
                }

                function e(a, b, c, e, g, f, l) {
                    a = d(a, d(d(b ^ c ^ e, g), l));
                    return d(a << f | a >>> 32 - f, b)
                }

                function p(a, b, c, e, f, g, l) {
                    a = d(a, d(d(c ^ (b | ~e), f), l));
                    return d(a << g | a >>> 32 - g, b)
                }

                function q(a) {
                    var b, d, c = [];
                    for (d = 0; 3 >= d; d++) b = a >>> 8 * d & 255, c = c.concat(b);
                    return c
                }
                var g = [],
                    t, x, s, r, f, h, j, m, l = eb("67452301efcdab8998badcfe10325476d76aa478e8c7b756242070dbc1bdceeef57c0faf4787c62aa8304613fd469501698098d88b44f7afffff5bb1895cd7be6b901122fd987193a679438e49b40821f61e2562c040b340265e5a51e9b6c7aad62f105d02441453d8a1e681e7d3fbc821e1cde6c33707d6f4d50d87455a14eda9e3e905fcefa3f8676f02d98d2a4c8afffa39428771f6816d9d6122fde5380ca4beea444bdecfa9f6bb4b60bebfbc70289b7ec6eaa127fad4ef308504881d05d9d4d039e6db99e51fa27cf8c4ac5665f4292244432aff97ab9423a7fc93a039655b59c38f0ccc92ffeff47d85845dd16fa87e4ffe2ce6e0a30143144e0811a1f7537e82bd3af2352ad7d2bbeb86d391",
                        8),
                    g = a.length;
                t = g + 8;
                x = 16 * ((t - t % 64) / 64 + 1);
                s = [];
                for (f = r = 0; f < g;) t = (f - f % 4) / 4, r = 8 * (f % 4), s[t] |= a[f] << r, f++;
                t = (f - f % 4) / 4;
                s[t] |= 128 << 8 * (f % 4);
                s[x - 2] = g << 3;
                s[x - 1] = g >>> 29;
                g = s;
                f = l[0];
                h = l[1];
                j = l[2];
                m = l[3];
                for (a = 0; a < g.length; a += 16) t = f, x = h, s = j, r = m, f = b(f, h, j, m, g[a + 0], 7, l[4]), m = b(m, f, h, j, g[a + 1], 12, l[5]), j = b(j, m, f, h, g[a + 2], 17, l[6]), h = b(h, j, m, f, g[a + 3], 22, l[7]), f = b(f, h, j, m, g[a + 4], 7, l[8]), m = b(m, f, h, j, g[a + 5], 12, l[9]), j = b(j, m, f, h, g[a + 6], 17, l[10]), h = b(h, j, m, f, g[a + 7], 22, l[11]), f = b(f, h, j, m, g[a + 8], 7, l[12]), m = b(m, f, h, j, g[a + 9],
                        12, l[13]), j = b(j, m, f, h, g[a + 10], 17, l[14]), h = b(h, j, m, f, g[a + 11], 22, l[15]), f = b(f, h, j, m, g[a + 12], 7, l[16]), m = b(m, f, h, j, g[a + 13], 12, l[17]), j = b(j, m, f, h, g[a + 14], 17, l[18]), h = b(h, j, m, f, g[a + 15], 22, l[19]), f = c(f, h, j, m, g[a + 1], 5, l[20]), m = c(m, f, h, j, g[a + 6], 9, l[21]), j = c(j, m, f, h, g[a + 11], 14, l[22]), h = c(h, j, m, f, g[a + 0], 20, l[23]), f = c(f, h, j, m, g[a + 5], 5, l[24]), m = c(m, f, h, j, g[a + 10], 9, l[25]), j = c(j, m, f, h, g[a + 15], 14, l[26]), h = c(h, j, m, f, g[a + 4], 20, l[27]), f = c(f, h, j, m, g[a + 9], 5, l[28]), m = c(m, f, h, j, g[a + 14], 9, l[29]), j = c(j, m, f, h, g[a + 3], 14, l[30]),
                    h = c(h, j, m, f, g[a + 8], 20, l[31]), f = c(f, h, j, m, g[a + 13], 5, l[32]), m = c(m, f, h, j, g[a + 2], 9, l[33]), j = c(j, m, f, h, g[a + 7], 14, l[34]), h = c(h, j, m, f, g[a + 12], 20, l[35]), f = e(f, h, j, m, g[a + 5], 4, l[36]), m = e(m, f, h, j, g[a + 8], 11, l[37]), j = e(j, m, f, h, g[a + 11], 16, l[38]), h = e(h, j, m, f, g[a + 14], 23, l[39]), f = e(f, h, j, m, g[a + 1], 4, l[40]), m = e(m, f, h, j, g[a + 4], 11, l[41]), j = e(j, m, f, h, g[a + 7], 16, l[42]), h = e(h, j, m, f, g[a + 10], 23, l[43]), f = e(f, h, j, m, g[a + 13], 4, l[44]), m = e(m, f, h, j, g[a + 0], 11, l[45]), j = e(j, m, f, h, g[a + 3], 16, l[46]), h = e(h, j, m, f, g[a + 6], 23, l[47]), f = e(f, h,
                        j, m, g[a + 9], 4, l[48]), m = e(m, f, h, j, g[a + 12], 11, l[49]), j = e(j, m, f, h, g[a + 15], 16, l[50]), h = e(h, j, m, f, g[a + 2], 23, l[51]), f = p(f, h, j, m, g[a + 0], 6, l[52]), m = p(m, f, h, j, g[a + 7], 10, l[53]), j = p(j, m, f, h, g[a + 14], 15, l[54]), h = p(h, j, m, f, g[a + 5], 21, l[55]), f = p(f, h, j, m, g[a + 12], 6, l[56]), m = p(m, f, h, j, g[a + 3], 10, l[57]), j = p(j, m, f, h, g[a + 10], 15, l[58]), h = p(h, j, m, f, g[a + 1], 21, l[59]), f = p(f, h, j, m, g[a + 8], 6, l[60]), m = p(m, f, h, j, g[a + 15], 10, l[61]), j = p(j, m, f, h, g[a + 6], 15, l[62]), h = p(h, j, m, f, g[a + 13], 21, l[63]), f = p(f, h, j, m, g[a + 4], 6, l[64]), m = p(m, f, h, j, g[a +
                        11], 10, l[65]), j = p(j, m, f, h, g[a + 2], 15, l[66]), h = p(h, j, m, f, g[a + 9], 21, l[67]), f = d(f, t), h = d(h, x), j = d(j, s), m = d(m, r);
                return q(f).concat(q(h), q(j), q(m))
            }
        },
        Base64: ib
    };
    if (!window.PUBNUB) {
        var mb = function(a, d) {
                return V.HmacSHA256(a, d).toString(V.enc.Base64)
            },
            tb = function(a) {
                return document.getElementById(a)
            },
            ub = function(a) {
                console.error(a)
            },
            vb = function(a, d) {
                var b = [];
                H(a.split(/\s+/), function(a) {
                    H((d || document).getElementsByTagName(a), function(a) {
                        b.push(a)
                    })
                });
                return b
            },
            wb = function(a, d, b) {
                H(a.split(","), function(a) {
                    function e(a) {
                        a || (a = window.event);
                        b(a) || (a.cancelBubble = v, a.preventDefault && a.preventDefault(), a.stopPropagation && a.stopPropagation())
                    }
                    d.addEventListener ?
                        d.addEventListener(a, e, A) : d.attachEvent ? d.attachEvent("on" + a, e) : d["on" + a] = e
                })
            },
            xb = function() {
                return vb("head")[0]
            },
            yb = function(a, d, b) {
                if (b) a.setAttribute(d, b);
                else return a && a.getAttribute && a.getAttribute(d)
            },
            zb = function(a, d) {
                for (var b in d)
                    if (d.hasOwnProperty(b)) try {
                        a.style[b] = d[b] + (0 < "|width|height|top|left|".indexOf(b) && "number" == typeof d[b] ? "px" : "")
                    } catch (c) {}
            },
            Ab = function(a) {
                return document.createElement(a)
            },
            Cb = function() {
                return Bb || X() ? 0 : ma()
            },
            Db = function(a) {
                function d(a, b) {
                    W || (W = 1, l.onerror =
                        z, clearTimeout(T), a || !b || Ra(b), setTimeout(function() {
                            a && kb();
                            var b = tb(y),
                                d = b && b.parentNode;
                            d && d.removeChild(b)
                        }, D))
                }
                if (Bb || X()) {
                    a: {
                        var b, c, e = function() {
                                if (!q) {
                                    q = 1;
                                    clearTimeout(t);
                                    try {
                                        c = JSON.parse(b.responseText)
                                    } catch (a) {
                                        return h(1)
                                    }
                                    p = 1;
                                    r(c)
                                }
                            },
                            p = 0,
                            q = 0,
                            g = a.timeout || 1E4,
                            t = setTimeout(function() {
                                h(1, {
                                    message: "timeout"
                                })
                            }, g),
                            x = a.d || C(),
                            s = a.data || {},
                            r = a.e || C(),
                            f = !a.D,
                            h = function(a, d) {
                                p || (p = 1, clearTimeout(t), b && (b.onerror = b.onload = z, b.abort && b.abort(), b = z), a && x(d))
                            };
                        try {
                            b = X() || window.XDomainRequest && new XDomainRequest ||
                                new XMLHttpRequest;
                            b.onerror = b.onabort = function() {
                                h(1, b.responseText || {
                                    error: "Network Connection Error"
                                })
                            };
                            b.onload = b.onloadend = e;
                            b.onreadystatechange = function() {
                                if (b && 4 == b.readyState) switch (b.status) {
                                    case 401:
                                    case 402:
                                    case 403:
                                        try {
                                            c = JSON.parse(b.responseText), h(1, c)
                                        } catch (a) {
                                            return h(1, b.responseText)
                                        }
                                }
                            };
                            var j = ra(a.url, s);
                            b.open("GET", j, f);
                            f && (b.timeout = g);
                            b.send()
                        } catch (m) {
                            h(0);
                            Bb = 0;
                            a = Db(a);
                            break a
                        }
                        a = h
                    }
                    return a
                }
                var l = Ab("script"),
                    e = a.c,
                    y = ma(),
                    W = 0,
                    T = setTimeout(function() {
                            d(1, {
                                message: "timeout"
                            })
                        },
                        a.timeout || 1E4),
                    kb = a.d || C(),
                    g = a.data || {},
                    Ra = a.e || C();
                window[e] = function(a) {
                    d(0, a)
                };
                a.D || (l[Eb] = Eb);
                l.onerror = function() {
                    d(1)
                };
                l.src = ra(a.url, g);
                yb(l, "id", y);
                xb().appendChild(l);
                return d
            },
            Fb = function() {
                return !("onLine" in navigator) ? 1 : navigator.onLine
            },
            X = function() {
                if (!Gb || !Gb.get) return 0;
                var a = {
                    id: X.id++,
                    send: C(),
                    abort: function() {
                        a.id = {}
                    },
                    open: function(d, b) {
                        X[a.id] = a;
                        Gb.get(a.id, b)
                    }
                };
                return a
            },
            Eb = "async",
            Bb = -1 == navigator.userAgent.indexOf("MSIE 6");
        window.console || (window.console = window.console || {});
        console.log || (console.log = console.error = (window.opera || {}).postError || C());
        var Hb, Ib = {},
            Jb = A;
        try {
            Jb = window.localStorage
        } catch (Kb) {}
        var Lb = function(a) {
                return -1 == document.cookie.indexOf(a) ? z : ((document.cookie || "").match(RegExp(a + "=([^;]+)")) || [])[1] || z
            },
            Mb = function(a, d) {
                document.cookie = a + "=" + d + "; expires=Thu, 1 Aug 2030 20:00:00 UTC; path=/"
            },
            Nb;
        try {
            Mb("pnctest", "1"), Nb = "1" === Lb("pnctest")
        } catch (Ob) {
            Nb = A
        }
        Hb = {
            get: function(a) {
                try {
                    return Jb ? Jb.getItem(a) : Nb ? Lb(a) : Ib[a]
                } catch (d) {
                    return Ib[a]
                }
            },
            set: function(a,
                d) {
                try {
                    if (Jb) return Jb.setItem(a, d) && 0;
                    Nb && Mb(a, d);
                    Ib[a] = d
                } catch (b) {
                    Ib[a] = d
                }
            }
        };
        var Pb = {
                list: {},
                unbind: function(a) {
                    Pb.list[a] = []
                },
                bind: function(a, d) {
                    (Pb.list[a] = Pb.list[a] || []).push(d)
                },
                fire: function(a, d) {
                    H(Pb.list[a] || [], function(a) {
                        a(d)
                    })
                }
            },
            Qb = tb("pubnub") || 0,
            Sb = function(a) {
                function d() {}

                function b(a, b) {
                    function d(b) {
                        b && (nb = E() - (b / 1E4 + (E() - c) / 2), a && a(nb))
                    }
                    var c = E();
                    b && d(b) || B.time(d)
                }

                function c(a, b) {
                    Wa && Wa(a, b);
                    Wa = z;
                    clearTimeout(ca);
                    clearTimeout(Y)
                }

                function e() {
                    B.time(function(a) {
                        b(C(), a);
                        a || c(1, {
                            error: "Heartbeat failed to connect to Pubnub Servers.Please check your network settings."
                        });
                        Y && clearTimeout(Y);
                        Y = setTimeout(e, Rb)
                    })
                }

                function p() {
                    sc() || c(1, {
                        error: "Offline. Please check your network settings. "
                    });
                    ca && clearTimeout(ca);
                    ca = setTimeout(p, D)
                }

                function q(a, b, d, c) {
                    var b = a.callback || b,
                        e = a.error || u,
                        a = I(),
                        f = [J, "v1", "channel-registration", "sub-key", w];
                    f.push.apply(f, d);
                    G({
                        c: a,
                        data: l(c),
                        e: function(a) {
                            t(a, b, e)
                        },
                        d: function(a) {
                            g(a, e)
                        },
                        url: f
                    })
                }

                function g(a, b) {
                    "object" == typeof a && a.error && a.message &&
                        a.payload ? b({
                            message: a.message,
                            payload: a.payload
                        }) : b(a)
                }

                function t(a, b, d) {
                    if ("object" == typeof a) {
                        if (a.error && a.message && a.payload) {
                            d({
                                message: a.message,
                                payload: a.payload
                            });
                            return
                        }
                        if (a.payload) {
                            b(a.payload);
                            return
                        }
                    }
                    b(a)
                }

                function x(a) {
                    var b = 0;
                    H(wa(F), function(d) {
                        if (d = F[d]) b++, (a || C())(d)
                    });
                    return b
                }

                function s(a) {
                    if (tc) {
                        if (!Z.length) return
                    } else {
                        a && (Z.L = 0);
                        if (Z.L || !Z.length) return;
                        Z.L = 1
                    }
                    G(Z.shift())
                }

                function r() {
                    !ob && f()
                }

                function f() {
                    clearTimeout(Xa);
                    !M || 500 <= M || 1 > M || !wa(F, v).length ? ob = A : (ob = v, B.presence_heartbeat({
                        callback: function() {
                            Xa =
                                setTimeout(f, M * D)
                        },
                        error: function(a) {
                            u && u("Presence Heartbeat unable to reach Pubnub servers." + JSON.stringify(a));
                            Xa = setTimeout(f, M * D)
                        }
                    }))
                }

                function h(a, b) {
                    return Ya.decrypt(a, b || N) || Ya.decrypt(a, N) || a
                }

                function j(a, b, d) {
                    var c = A;
                    if ("number" === typeof a) c = 5 < a || 0 == a ? A : v;
                    else {
                        if ("boolean" === typeof a) return a ? 30 : 0;
                        c = v
                    }
                    return c ? (d && d("Presence Heartbeat value invalid. Valid range ( x > 5 or x = 0). Current Value : " + (b || 5)), b || 5) : a
                }

                function m(a) {
                    var b = "",
                        d = [];
                    H(a, function(a) {
                        d.push(a)
                    });
                    var c = d.sort(),
                        e;
                    for (e in c) {
                        var f = c[e],
                            b = b + (f + "=" + encodeURIComponent(a[f]));
                        e != c.length - 1 && (b += "&")
                    }
                    return b
                }

                function l(a) {
                    a || (a = {});
                    H($, function(b, d) {
                        b in a || (a[b] = d)
                    });
                    return a
                }

                function y(a) {
                    return Sb(a)
                }

                function W(a) {
                    function b(a, d) {
                        var c = (a & 65535) + (d & 65535);
                        return (a >> 16) + (d >> 16) + (c >> 16) << 16 | c & 65535
                    }

                    function d(a, b) {
                        return a >>> b | a << 32 - b
                    }
                    var c;
                    c = a.replace(/\r\n/g, "\n");
                    for (var a = "", e = 0; e < c.length; e++) {
                        var f = c.charCodeAt(e);
                        128 > f ? a += String.fromCharCode(f) : (127 < f && 2048 > f ? a += String.fromCharCode(f >> 6 | 192) : (a += String.fromCharCode(f >>
                            12 | 224), a += String.fromCharCode(f >> 6 & 63 | 128)), a += String.fromCharCode(f & 63 | 128))
                    }
                    e = a;
                    c = [];
                    for (f = 0; f < 8 * e.length; f += 8) c[f >> 5] |= (e.charCodeAt(f / 8) & 255) << 24 - f % 32;
                    var g = 8 * a.length,
                        e = [1116352408, 1899447441, 3049323471, 3921009573, 961987163, 1508970993, 2453635748, 2870763221, 3624381080, 310598401, 607225278, 1426881987, 1925078388, 2162078206, 2614888103, 3248222580, 3835390401, 4022224774, 264347078, 604807628, 770255983, 1249150122, 1555081692, 1996064986, 2554220882, 2821834349, 2952996808, 3210313671, 3336571891, 3584528711, 113926993,
                            338241895, 666307205, 773529912, 1294757372, 1396182291, 1695183700, 1986661051, 2177026350, 2456956037, 2730485921, 2820302411, 3259730800, 3345764771, 3516065817, 3600352804, 4094571909, 275423344, 430227734, 506948616, 659060556, 883997877, 958139571, 1322822218, 1537002063, 1747873779, 1955562222, 2024104815, 2227730452, 2361852424, 2428436474, 2756734187, 3204031479, 3329325298
                        ],
                        a = [1779033703, 3144134277, 1013904242, 2773480762, 1359893119, 2600822924, 528734635, 1541459225],
                        f = Array(64),
                        l, h, j, m, p, q, r, t, u, s, w;
                    c[g >> 5] |= 128 << 24 - g % 32;
                    c[(g + 64 >> 9 << 4) + 15] = g;
                    for (t = 0; t < c.length; t += 16) {
                        g = a[0];
                        l = a[1];
                        h = a[2];
                        j = a[3];
                        m = a[4];
                        p = a[5];
                        q = a[6];
                        r = a[7];
                        for (u = 0; 64 > u; u++) f[u] = 16 > u ? c[u + t] : b(b(b(d(f[u - 2], 17) ^ d(f[u - 2], 19) ^ f[u - 2] >>> 10, f[u - 7]), d(f[u - 15], 7) ^ d(f[u - 15], 18) ^ f[u - 15] >>> 3), f[u - 16]), s = b(b(b(b(r, d(m, 6) ^ d(m, 11) ^ d(m, 25)), m & p ^ ~m & q), e[u]), f[u]), w = b(d(g, 2) ^ d(g, 13) ^ d(g, 22), g & l ^ g & h ^ l & h), r = q, q = p, p = m, m = b(j, s), j = h, h = l, l = g, g = b(s, w);
                        a[0] = b(g, a[0]);
                        a[1] = b(l, a[1]);
                        a[2] = b(h, a[2]);
                        a[3] = b(j, a[3]);
                        a[4] = b(m, a[4]);
                        a[5] = b(p, a[5]);
                        a[6] = b(q, a[6]);
                        a[7] = b(r, a[7])
                    }
                    c = "";
                    for (e = 0; e < 4 * a.length; e++) c += "0123456789abcdef".charAt(a[e >> 2] >> 8 * (3 - e % 4) + 4 & 15) + "0123456789abcdef".charAt(a[e >> 2] >> 8 * (3 - e % 4) & 15);
                    return c
                }
                a.jsonp && (Bb = 0);
                var T = a.subscribe_key || "";
                a.uuid || Hb.get(T + "uuid");
                var kb = a.leave_on_unload || 0;
                a.xdr = Db;
                a.db = Hb;
                a.error = a.error || ub;
                a._is_online = Fb;
                a.jsonp_cb = Cb;
                a.hmac_SHA256 = mb;
                L.size(256);
                var Ra = L.s2a("0123456789012345");
                a.crypto_obj = {
                    encrypt: function(a, b) {
                        if (!b) return a;
                        var d = L.s2a(W(b).slice(0, 32)),
                            c = L.s2a(JSON.stringify(a)),
                            d = L.rawEncrypt(c, d, Ra);
                        return L.Base64.encode(d) ||
                            a
                    },
                    decrypt: function(a, b) {
                        if (!b) return a;
                        var d = L.s2a(W(b).slice(0, 32));
                        try {
                            var c = L.Base64.decode(a),
                                e = L.rawDecrypt(c, d, Ra, A);
                            return JSON.parse(e)
                        } catch (f) {}
                    }
                };
                a.params = {
                    pnsdk: "PubNub-JS-Web/3.7.1"
                };
                var lc = +a.windowing || 10,
                    mc = (+a.timeout || 310) * D,
                    Rb = (+a.keepalive || 60) * D,
                    qc = a.noleave || 0,
                    O = a.publish_key || "demo",
                    w = a.subscribe_key || "demo",
                    P = a.auth_key || "",
                    Za = a.secret_key || "",
                    Wb = a.hmac_SHA256,
                    pb = a.ssl ? "s" : "",
                    Aa = "http" + pb + "://" + (a.origin || "pubsub.pubnub.com"),
                    J = na(Aa),
                    Xb = na(Aa),
                    Z = [],
                    qb = v,
                    nb = 0,
                    rb = 0,
                    Yb = 0,
                    Wa =
                    0,
                    $a = a.restore || 0,
                    fa = 0,
                    sb = A,
                    F = {},
                    da = {},
                    Q = {},
                    Xa = z,
                    U = j(a.heartbeat || a.pnexpires || 0, a.error),
                    M = a.heartbeat_interval || U - 3,
                    ob = A,
                    tc = a.no_wait_for_pending,
                    uc = a["compatible_3.5"] || A,
                    G = a.xdr,
                    $ = a.params || {},
                    u = a.error || C(),
                    sc = a._is_online || function() {
                        return 1
                    },
                    I = a.jsonp_cb || function() {
                        return 0
                    },
                    ga = a.db || {
                        get: C(),
                        set: C()
                    },
                    N = a.cipher_key,
                    K = a.uuid || ga && ga.get(w + "uuid") || "",
                    ca, Y, Ya = a.crypto_obj || {
                        encrypt: function(a) {
                            return a
                        },
                        decrypt: function(a) {
                            return a
                        }
                    },
                    B = {
                        LEAVE: function(a, b, d, c) {
                            var e = {
                                    uuid: K,
                                    auth: P
                                },
                                f = na(Aa),
                                d = d || C(),
                                h = c || C(),
                                c = I();
                            if (0 < a.indexOf(ia)) return v;
                            if (uc && (!pb || "0" == c) || qc) return A;
                            "0" != c && (e.callback = c);
                            G({
                                D: b || pb,
                                timeout: 2E3,
                                c: c,
                                data: l(e),
                                e: function(a) {
                                    t(a, d, h)
                                },
                                d: function(a) {
                                    g(a, h)
                                },
                                url: [f, "v2", "presence", "sub_key", w, "channel", encodeURIComponent(a), "leave"]
                            });
                            return v
                        },
                        set_resumed: function(a) {
                            sb = a
                        },
                        get_cipher_key: function() {
                            return N
                        },
                        set_cipher_key: function(a) {
                            N = a
                        },
                        raw_encrypt: function(a, b) {
                            return Ya.encrypt(a, b || N) || a
                        },
                        raw_decrypt: function(a, b) {
                            return h(a, b)
                        },
                        get_heartbeat: function() {
                            return U
                        },
                        set_heartbeat: function(a) {
                            U = j(a, M, u);
                            M = 1 <= U - 3 ? U - 3 : 1;
                            d();
                            f()
                        },
                        get_heartbeat_interval: function() {
                            return M
                        },
                        set_heartbeat_interval: function(a) {
                            M = a;
                            f()
                        },
                        get_version: function() {
                            return "3.7.1"
                        },
                        getGcmMessageObject: function(a) {
                            return {
                                data: a
                            }
                        },
                        getApnsMessageObject: function(a) {
                            var b = {
                                aps: {
                                    badge: 1,
                                    alert: ""
                                }
                            };
                            for (k in a) k[b] = a[k];
                            return b
                        },
                        newPnMessage: function() {
                            var a = {};
                            gcm && (a.pn_gcm = gcm);
                            apns && (a.pn_apns = apns);
                            for (k in n) a[k] = n[k];
                            return a
                        },
                        _add_param: function(a, b) {
                            $[a] = b
                        },
                        channel_group: function(a, b) {
                            var d =
                                a.channel_group,
                                c = a.channels || a.channel,
                                e = a.cloak,
                                f, g, l = [],
                                h = {},
                                j = a.mode || "add";
                            d && (d = d.split(":"), 1 < d.length ? (f = "*" === d[0] ? z : d[0], g = d[1]) : g = d[0]);
                            f && l.push("namespace") && l.push(encodeURIComponent(f));
                            l.push("channel-group");
                            g && "*" !== g && l.push(g);
                            c ? (c && (Array.isArray && Array.isArray(c)) && (c = c.join(",")), h[j] = c, h.cloak = qb ? "true" : "false") : "remove" === j && l.push("remove");
                            "undefined" != typeof e && (h.cloak = e ? "true" : "false");
                            q(a, b, l, h)
                        },
                        channel_group_list_groups: function(a, b) {
                            var d;
                            (d = a.namespace || a.ns || a.channel_group ||
                                z) && (a.channel_group = d + ":*");
                            B.channel_group(a, b)
                        },
                        channel_group_list_channels: function(a, b) {
                            if (!a.channel_group) return u("Missing Channel Group");
                            B.channel_group(a, b)
                        },
                        channel_group_remove_channel: function(a, b) {
                            if (!a.channel_group) return u("Missing Channel Group");
                            if (!a.channel && !a.channels) return u("Missing Channel");
                            a.mode = "remove";
                            B.channel_group(a, b)
                        },
                        channel_group_remove_group: function(a, b) {
                            if (!a.channel_group) return u("Missing Channel Group");
                            if (a.channel) return u("Use channel_group_remove_channel if you want to remove a channel from a group.");
                            a.mode = "remove";
                            B.channel_group(a, b)
                        },
                        channel_group_add_channel: function(a, b) {
                            if (!a.channel_group) return u("Missing Channel Group");
                            if (!a.channel && !a.channels) return u("Missing Channel");
                            B.channel_group(a, b)
                        },
                        channel_group_cloak: function(a, b) {
                            "undefined" == typeof a.cloak ? b(qb) : (qb = a.cloak, B.channel_group(a, b))
                        },
                        channel_group_list_namespaces: function(a, b) {
                            q(a, b, ["namespace"])
                        },
                        channel_group_remove_namespace: function(a, b) {
                            q(a, b, ["namespace", a.namespace, "remove"])
                        },
                        history: function(a, b) {
                            var b = a.callback ||
                                b,
                                d = a.count || a.limit || 100,
                                c = a.reverse || "false",
                                e = a.error || C(),
                                f = a.auth_key || P,
                                j = a.cipher_key,
                                m = a.channel,
                                p = a.channel_group,
                                q = a.start,
                                r = a.end,
                                t = a.include_token,
                                s = {},
                                x = I();
                            if (!m && !p) return u("Missing Channel");
                            if (!b) return u("Missing Callback");
                            if (!w) return u("Missing Subscribe Key");
                            s.stringtoken = "true";
                            s.count = d;
                            s.reverse = c;
                            s.auth = f;
                            p && (s["channel-group"] = p, m || (m = ","));
                            x && (s.callback = x);
                            q && (s.start = q);
                            r && (s.end = r);
                            t && (s.include_token = "true");
                            G({
                                c: x,
                                data: l(s),
                                e: function(a) {
                                    if ("object" == typeof a && a.error) e({
                                        message: a.message,
                                        payload: a.payload
                                    });
                                    else {
                                        for (var d = a[0], c = [], f = 0; f < d.length; f++) {
                                            var g = h(d[f], j);
                                            try {
                                                c.push(JSON.parse(g))
                                            } catch (l) {
                                                c.push(g)
                                            }
                                        }
                                        b([c, a[1], a[2]])
                                    }
                                },
                                d: function(a) {
                                    g(a, e)
                                },
                                url: [J, "v2", "history", "sub-key", w, "channel", encodeURIComponent(m)]
                            })
                        },
                        replay: function(a, b) {
                            var b = b || a.callback || C(),
                                d = a.auth_key || P,
                                c = a.source,
                                e = a.destination,
                                f = a.stop,
                                g = a.start,
                                h = a.end,
                                j = a.reverse,
                                m = a.limit,
                                p = I(),
                                q = {};
                            if (!c) return u("Missing Source Channel");
                            if (!e) return u("Missing Destination Channel");
                            if (!O) return u("Missing Publish Key");
                            if (!w) return u("Missing Subscribe Key");
                            "0" != p && (q.callback = p);
                            f && (q.stop = "all");
                            j && (q.reverse = "true");
                            g && (q.start = g);
                            h && (q.end = h);
                            m && (q.count = m);
                            q.auth = d;
                            G({
                                c: p,
                                e: function(a) {
                                    t(a, b, err)
                                },
                                d: function() {
                                    b([0, "Disconnected"])
                                },
                                url: [J, "v1", "replay", O, w, c, e],
                                data: l(q)
                            })
                        },
                        auth: function(a) {
                            P = a;
                            d()
                        },
                        time: function(a) {
                            var b = I();
                            G({
                                c: b,
                                data: l({
                                    uuid: K,
                                    auth: P
                                }),
                                timeout: 5 * D,
                                url: [J, "time", b],
                                e: function(b) {
                                    a(b[0])
                                },
                                d: function() {
                                    a(0)
                                }
                            })
                        },
                        publish: function(a, b) {
                            var d = a.message;
                            if (!d) return u("Missing Message");
                            var b =
                                b || a.callback || d.callback || C(),
                                c = a.channel || d.channel,
                                e = a.auth_key || P,
                                f = a.cipher_key,
                                h = a.error || d.error || C(),
                                j = a.post || A,
                                m = "store_in_history" in a ? a.store_in_history : v,
                                p = I(),
                                q = "push";
                            a.prepend && (q = "unshift");
                            if (!c) return u("Missing Channel");
                            if (!O) return u("Missing Publish Key");
                            if (!w) return u("Missing Subscribe Key");
                            d.getPubnubMessage && (d = d.getPubnubMessage());
                            d = JSON.stringify(Ya.encrypt(d, f || N) || d);
                            d = [J, "publish", O, w, 0, encodeURIComponent(c), p, encodeURIComponent(d)];
                            $ = {
                                uuid: K,
                                auth: e
                            };
                            m || ($.store =
                                "0");
                            Z[q]({
                                c: p,
                                timeout: 5 * D,
                                url: d,
                                data: l($),
                                d: function(a) {
                                    g(a, h);
                                    s(1)
                                },
                                e: function(a) {
                                    t(a, b, h);
                                    s(1)
                                },
                                mode: j ? "POST" : "GET"
                            });
                            s()
                        },
                        unsubscribe: function(a, b) {
                            var c = a.channel,
                                e = a.channel_group,
                                b = b || a.callback || C(),
                                f = a.error || C();
                            fa = 0;
                            c && (c = va((c.join ? c.join(",") : "" + c).split(","), function(a) {
                                if (F[a]) return a + "," + a + ia
                            }).join(","), H(c.split(","), function(a) {
                                var d = v;
                                a && (ea && (d = B.LEAVE(a, 0, b, f)), d || b({
                                    action: "leave"
                                }), F[a] = 0, a in Q && delete Q[a])
                            }));
                            e && (e = va((e.join ? e.join(",") : "" + e).split(","), function(a) {
                                if (da[a]) return a +
                                    "," + a + ia
                            }).join(","), H(e.split(","), function() {
                                var a = v;
                                e && (ea && (a = B.LEAVE(e, 0, b, f)), a || b({
                                    action: "leave"
                                }), da[e] = 0, e in Q && delete Q[e])
                            }));
                            d()
                        },
                        subscribe: function(a, b) {
                            function e(a) {
                                a ? setTimeout(d, D) : (J = na(Aa, 1), Xb = na(Aa, 1), setTimeout(function() {
                                    B.time(e)
                                }, D));
                                x(function(b) {
                                    if (a && b.i) return b.i = 0, b.K(b.name);
                                    !a && !b.i && (b.i = 1, b.H(b.name))
                                })
                            }

                            function f() {
                                var a = I(),
                                    b = wa(F).join(","),
                                    j = xa(da).join(",");
                                if (b || j) {
                                    b || (b = ",");
                                    c();
                                    var m = l({
                                        uuid: K,
                                        auth: p
                                    });
                                    j && (m["channel-group"] = j);
                                    2 < JSON.stringify(Q).length &&
                                        (m.state = JSON.stringify(Q));
                                    U && (m.heartbeat = U);
                                    r();
                                    Wa = G({
                                        timeout: ca,
                                        c: a,
                                        d: function(a) {
                                            g(a, y);
                                            B.time(e)
                                        },
                                        data: l(m),
                                        url: [Xb, "subscribe", w, encodeURIComponent(b), a, fa],
                                        e: function(a) {
                                            if (!a || "object" == typeof a && "error" in a && a.error) return y(a.error), setTimeout(d, D);
                                            M(a[1]);
                                            fa = !fa && $a && ga.get(w) || a[1];
                                            x(function(a) {
                                                a.l || (a.l = 1, a.G(a.name))
                                            });
                                            if (sb && !$a) fa = 0, sb = A, ga.set(w, 0);
                                            else {
                                                T && (fa = 1E4, T = 0);
                                                ga.set(w, a[1]);
                                                var b, c = "",
                                                    c = 3 < a.length ? a[3] : 2 < a.length ? a[2] : va(wa(F), function(b) {
                                                        return va(Array(a[0].length).join(",").split(","),
                                                            function() {
                                                                return b
                                                            })
                                                    }).join(","),
                                                    e = c.split(",");
                                                b = function() {
                                                    var a = e.shift() || Yb;
                                                    return [(F[a] || {}).c || rb, a.split(ia)[0]]
                                                };
                                                var g = E() - nb - +a[1] / 1E4;
                                                H(a[0], function(d) {
                                                    var c = b(),
                                                        d = h(d, F[c[1]] ? F[c[1]].cipher_key : z);
                                                    c[0](d, a, c[2] || c[1], g)
                                                })
                                            }
                                            setTimeout(f, Y)
                                        }
                                    })
                                }
                            }
                            var j = a.channel,
                                m = a.channel_group,
                                b = (b = b || a.callback) || a.message,
                                p = a.auth_key || P,
                                q = a.connect || C(),
                                t = a.reconnect || C(),
                                s = a.disconnect || C(),
                                y = a.error || C(),
                                M = a.idle || C(),
                                oa = a.presence || 0,
                                O = a.noheresync || 0,
                                T = a.backfill || 0,
                                Z = a.timetoken || 0,
                                ca = a.timeout ||
                                mc,
                                Y = a.windowing || lc,
                                N = a.state,
                                W = a.heartbeat || a.pnexpires,
                                $ = a.restore || $a;
                            $a = $;
                            fa = Z;
                            if (!j && !m) return u("Missing Channel");
                            if (!b) return u("Missing Callback");
                            if (!w) return u("Missing Subscribe Key");
                            (W || 0 === W) && B.set_heartbeat(W);
                            j && H((j.join ? j.join(",") : "" + j).split(","), function(d) {
                                var c = F[d] || {};
                                F[Yb = d] = {
                                    name: d,
                                    l: c.l,
                                    i: c.i,
                                    h: 1,
                                    c: rb = b,
                                    cipher_key: a.cipher_key,
                                    G: q,
                                    H: s,
                                    K: t
                                };
                                N && (Q[d] = d in N ? N[d] : N);
                                oa && (B.subscribe({
                                    channel: d + ia,
                                    callback: oa,
                                    restore: $
                                }), !c.h && !O && B.here_now({
                                    channel: d,
                                    callback: function(a) {
                                        H("uuids" in
                                            a ? a.uuids : [],
                                            function(b) {
                                                oa({
                                                    action: "join",
                                                    uuid: b,
                                                    timestamp: Math.floor(E() / 1E3),
                                                    occupancy: a.occupancy || 1
                                                }, a, d)
                                            })
                                    }
                                }))
                            });
                            m && H((m.join ? m.join(",") : "" + m).split(","), function(d) {
                                var c = da[d] || {};
                                da[d] = {
                                    name: d,
                                    l: c.l,
                                    i: c.i,
                                    h: 1,
                                    c: rb = b,
                                    cipher_key: a.cipher_key,
                                    G: q,
                                    H: s,
                                    K: t
                                };
                                oa && (B.subscribe({
                                    channel_group: d + ia,
                                    callback: oa,
                                    restore: $
                                }), !c.h && !O && B.here_now({
                                    channel_group: d,
                                    callback: function(a) {
                                        H("uuids" in a ? a.uuids : [], function(b) {
                                            oa({
                                                    action: "join",
                                                    uuid: b,
                                                    timestamp: Math.floor(E() / 1E3),
                                                    occupancy: a.occupancy || 1
                                                },
                                                a, d)
                                        })
                                    }
                                }))
                            });
                            d = function() {
                                c();
                                setTimeout(f, Y)
                            };
                            if (!ea) return ha.push(d);
                            d()
                        },
                        here_now: function(a, b) {
                            var b = a.callback || b,
                                d = a.error || C(),
                                c = a.auth_key || P,
                                e = a.channel,
                                f = a.channel_group,
                                h = I(),
                                j = a.state,
                                c = {
                                    uuid: K,
                                    auth: c
                                };
                            if (!("uuids" in a ? a.uuids : 1)) c.disable_uuids = 1;
                            j && (c.state = 1);
                            if (!b) return u("Missing Callback");
                            if (!w) return u("Missing Subscribe Key");
                            j = [J, "v2", "presence", "sub_key", w];
                            e && j.push("channel") && j.push(encodeURIComponent(e));
                            "0" != h && (c.callback = h);
                            f && (c["channel-group"] = f, !e && j.push("channel") &&
                                j.push(","));
                            G({
                                c: h,
                                data: l(c),
                                e: function(a) {
                                    t(a, b, d)
                                },
                                d: function(a) {
                                    g(a, d)
                                },
                                url: j
                            })
                        },
                        where_now: function(a, b) {
                            var b = a.callback || b,
                                d = a.error || C(),
                                c = a.auth_key || P,
                                e = I(),
                                f = a.uuid || K,
                                c = {
                                    auth: c
                                };
                            if (!b) return u("Missing Callback");
                            if (!w) return u("Missing Subscribe Key");
                            "0" != e && (c.callback = e);
                            G({
                                c: e,
                                data: l(c),
                                e: function(a) {
                                    t(a, b, d)
                                },
                                d: function(a) {
                                    g(a, d)
                                },
                                url: [J, "v2", "presence", "sub_key", w, "uuid", encodeURIComponent(f)]
                            })
                        },
                        state: function(a, b) {
                            var b = a.callback || b || C(),
                                d = a.error || C(),
                                c = a.auth_key || P,
                                e = I(),
                                f =
                                a.state,
                                h = a.uuid || K,
                                j = a.channel,
                                m = a.channel_group,
                                c = l({
                                    auth: c
                                });
                            if (!w) return u("Missing Subscribe Key");
                            if (!h) return u("Missing UUID");
                            if (!j && !m) return u("Missing Channel");
                            "0" != e && (c.callback = e);
                            "undefined" != typeof j && F[j] && F[j].h && f && (Q[j] = f);
                            "undefined" != typeof m && (da[m] && da[m].h) && (f && (Q[m] = f), c["channel-group"] = m, j || (j = ","));
                            c.state = JSON.stringify(f);
                            f = f ? [J, "v2", "presence", "sub-key", w, "channel", j, "uuid", h, "data"] : [J, "v2", "presence", "sub-key", w, "channel", j, "uuid", encodeURIComponent(h)];
                            G({
                                c: e,
                                data: l(c),
                                e: function(a) {
                                    t(a, b, d)
                                },
                                d: function(a) {
                                    g(a, d)
                                },
                                url: f
                            })
                        },
                        grant: function(a, b) {
                            var b = a.callback || b,
                                d = a.error || C(),
                                c = a.channel,
                                e = a.channel_group,
                                f = I(),
                                j = a.ttl,
                                h = a.read ? "1" : "0",
                                p = a.write ? "1" : "0",
                                q = a.manage ? "1" : "0",
                                r = a.auth_key;
                            if (!b) return u("Missing Callback");
                            if (!w) return u("Missing Subscribe Key");
                            if (!O) return u("Missing Publish Key");
                            if (!Za) return u("Missing Secret Key");
                            var s = w + "\n" + O + "\ngrant\n",
                                h = {
                                    w: p,
                                    r: h,
                                    timestamp: Math.floor((new Date).getTime() / 1E3)
                                };
                            a.manage && (h.m = q);
                            "undefined" != typeof c &&
                                (c != z && 0 < c.length) && (h.channel = c);
                            "undefined" != typeof e && (e != z && 0 < e.length) && (h["channel-group"] = e);
                            "0" != f && (h.callback = f);
                            if (j || 0 === j) h.ttl = j;
                            r && (h.auth = r);
                            h = l(h);
                            r || delete h.auth;
                            s += m(h);
                            c = Wb(s, Za);
                            c = c.replace(/\+/g, "-");
                            c = c.replace(/\//g, "_");
                            h.signature = c;
                            G({
                                c: f,
                                data: h,
                                e: function(a) {
                                    t(a, b, d)
                                },
                                d: function(a) {
                                    g(a, d)
                                },
                                url: [J, "v1", "auth", "grant", "sub-key", w]
                            })
                        },
                        audit: function(a, b) {
                            var b = a.callback || b,
                                d = a.error || C(),
                                c = a.channel,
                                e = a.channel_group,
                                f = a.auth_key,
                                h = I();
                            if (!b) return u("Missing Callback");
                            if (!w) return u("Missing Subscribe Key");
                            if (!O) return u("Missing Publish Key");
                            if (!Za) return u("Missing Secret Key");
                            var j = w + "\n" + O + "\naudit\n",
                                p = {
                                    timestamp: Math.floor((new Date).getTime() / 1E3)
                                };
                            "0" != h && (p.callback = h);
                            "undefined" != typeof c && (c != z && 0 < c.length) && (p.channel = c);
                            "undefined" != typeof e && (e != z && 0 < e.length) && (p["channel-group"] = e);
                            f && (p.auth = f);
                            p = l(p);
                            f || delete p.auth;
                            j += m(p);
                            c = Wb(j, Za);
                            c = c.replace(/\+/g, "-");
                            c = c.replace(/\//g, "_");
                            p.signature = c;
                            G({
                                c: h,
                                data: p,
                                e: function(a) {
                                    t(a, b, d)
                                },
                                d: function(a) {
                                    g(a,
                                        d)
                                },
                                url: [J, "v1", "auth", "audit", "sub-key", w]
                            })
                        },
                        revoke: function(a, b) {
                            a.read = A;
                            a.write = A;
                            B.grant(a, b)
                        },
                        set_uuid: function(a) {
                            K = a;
                            d()
                        },
                        get_uuid: function() {
                            return K
                        },
                        presence_heartbeat: function(a) {
                            var b = a.callback || C(),
                                d = a.error || C(),
                                a = I(),
                                c = {
                                    uuid: K,
                                    auth: P
                                };
                            2 < JSON.stringify(Q).length && (c.state = JSON.stringify(Q));
                            0 < U && 320 > U && (c.heartbeat = U);
                            "0" != a && (c.callback = a);
                            var e;
                            e = wa(F, v).join(",");
                            e = encodeURIComponent(e);
                            var f = xa(da, v).join(",");
                            e || (e = ",");
                            f && (c["channel-group"] = f);
                            G({
                                c: a,
                                data: l(c),
                                timeout: 5 * D,
                                url: [J, "v2", "presence", "sub-key", w, "channel", e, "heartbeat"],
                                e: function(a) {
                                    t(a, b, d)
                                },
                                d: function(a) {
                                    g(a, d)
                                }
                            })
                        },
                        stop_timers: function() {
                            clearTimeout(ca);
                            clearTimeout(Y)
                        },
                        xdr: G,
                        ready: ya,
                        db: ga,
                        uuid: qa,
                        map: va,
                        each: H,
                        "each-channel": x,
                        grep: ta,
                        offline: function() {
                            c(1, {
                                message: "Offline. Please check your network settings."
                            })
                        },
                        supplant: ua,
                        now: E,
                        unique: ma,
                        updater: sa
                    };
                K || (K = B.uuid());
                ga.set(w + "uuid", K);
                ca = setTimeout(p, D);
                Y = setTimeout(e, Rb);
                Xa = setTimeout(r, (M - 3) * D);
                b();
                var T = B,
                    ab;
                for (ab in T) T.hasOwnProperty(ab) &&
                    (y[ab] = T[ab]);
                y.css = zb;
                y.$ = tb;
                y.create = Ab;
                y.bind = wb;
                y.head = xb;
                y.search = vb;
                y.attr = yb;
                y.events = Pb;
                y.init = y;
                y.secure = y;
                wb("beforeunload", window, function() {
                    if (kb) y["each-channel"](function(a) {
                        y.LEAVE(a.name, 0)
                    });
                    return v
                });
                if (a.notest) return y;
                wb("offline", window, y.offline);
                wb("offline", document, y.offline);
                return y
            };
        Sb.init = Sb;
        Sb.secure = Sb;
        "complete" === document.readyState ? setTimeout(ya, 0) : wb("load", window, function() {
            setTimeout(ya, 0)
        });
        var Tb = Qb || {};
        PUBNUB = Sb({
            notest: 1,
            publish_key: yb(Tb, "pub-key"),
            subscribe_key: yb(Tb,
                "sub-key"),
            ssl: !document.location.href.indexOf("https") || "on" == yb(Tb, "ssl"),
            origin: yb(Tb, "origin"),
            uuid: yb(Tb, "uuid")
        });
        window.jQuery && (window.jQuery.PUBNUB = Sb);
        "undefined" !== typeof module && (module.exports = PUBNUB) && ya();
        var Gb = tb("pubnubs") || 0;
        if (Qb) {
            zb(Qb, {
                position: "absolute",
                top: -D
            });
            if ("opera" in window || yb(Qb, "flash")) Qb.innerHTML = "<object id=pubnubs data=https://pubnub.a.ssl.fastly.net/pubnub.swf><param name=movie value=https://pubnub.a.ssl.fastly.net/pubnub.swf><param name=allowscriptaccess value=always></object>";
            PUBNUB.rdx = function(a, d) {
                if (!d) return X[a].onerror();
                X[a].responseText = unescape(d);
                X[a].onload()
            };
            X.id = D
        }
    }
    var Ub = PUBNUB.ws = function(a, d) {
        if (!(this instanceof Ub)) return new Ub(a, d);
        var b = this,
            a = b.url = a || "";
        b.protocol = d || "Sec-WebSocket-Protocol";
        var c = a.split("/"),
            c = {
                ssl: "wss:" === c[0],
                origin: c[2],
                publish_key: c[3],
                subscribe_key: c[4],
                channel: c[5]
            };
        b.CONNECTING = 0;
        b.OPEN = 1;
        b.CLOSING = 2;
        b.CLOSED = 3;
        b.CLOSE_NORMAL = 1E3;
        b.CLOSE_GOING_AWAY = 1001;
        b.CLOSE_PROTOCOL_ERROR = 1002;
        b.CLOSE_UNSUPPORTED = 1003;
        b.CLOSE_TOO_LARGE = 1004;
        b.CLOSE_NO_STATUS = 1005;
        b.CLOSE_ABNORMAL = 1006;
        b.onclose = b.onerror = b.onmessage = b.onopen = b.onsend =
            C();
        b.binaryType = "";
        b.extensions = "";
        b.bufferedAmount = 0;
        b.trasnmitting = A;
        b.buffer = [];
        b.readyState = b.CONNECTING;
        if (!a) return b.readyState = b.CLOSED, b.onclose({
            code: b.CLOSE_ABNORMAL,
            reason: "Missing URL",
            wasClean: v
        }), b;
        b.o = PUBNUB.init(c);
        b.o.M = c;
        b.M = c;
        b.o.subscribe({
            restore: A,
            channel: c.channel,
            disconnect: b.onerror,
            reconnect: b.onopen,
            error: function() {
                b.onclose({
                    code: b.CLOSE_ABNORMAL,
                    reason: "Missing URL",
                    wasClean: A
                })
            },
            callback: function(a) {
                b.onmessage({
                    data: a
                })
            },
            connect: function() {
                b.readyState = b.OPEN;
                b.onopen()
            }
        })
    };
    Ub.prototype.send = function(a) {
        var d = this;
        d.o.publish({
            channel: d.o.M.channel,
            message: a,
            callback: function(a) {
                d.onsend({
                    data: a
                })
            }
        })
    };
    var Vb;
    if (!(Vb = V)) {
        var Zb = Math,
            $b = {},
            ac = $b.t = {},
            bc = C(),
            cc = ac.O = {
                extend: function(a) {
                    bc.prototype = this;
                    var d = new bc;
                    a && d.da(a);
                    d.hasOwnProperty("init") || (d.a = function() {
                        d.N.a.apply(this, arguments)
                    });
                    d.a.prototype = d;
                    d.N = this;
                    return d
                },
                create: function() {
                    var a = this.extend();
                    a.a.apply(a, arguments);
                    return a
                },
                a: C(),
                da: function(a) {
                    for (var d in a) a.hasOwnProperty(d) && (this[d] = a[d]);
                    a.hasOwnProperty("toString") && (this.toString = a.toString)
                },
                g: function() {
                    return this.a.prototype.extend(this)
                }
            },
            ec = ac.u = cc.extend({
                a: function(a,
                    d) {
                    a = this.f = a || [];
                    this.b = d != aa ? d : 4 * a.length
                },
                toString: function(a) {
                    return (a || dc).stringify(this)
                },
                concat: function(a) {
                    var d = this.f,
                        b = a.f,
                        c = this.b,
                        a = a.b;
                    this.s();
                    if (c % 4)
                        for (var e = 0; e < a; e++) d[c + e >>> 2] |= (b[e >>> 2] >>> 24 - 8 * (e % 4) & 255) << 24 - 8 * ((c + e) % 4);
                    else if (65535 < b.length)
                        for (e = 0; e < a; e += 4) d[c + e >>> 2] = b[e >>> 2];
                    else d.push.apply(d, b);
                    this.b += a;
                    return this
                },
                s: function() {
                    var a = this.f,
                        d = this.b;
                    a[d >>> 2] &= 4294967295 << 32 - 8 * (d % 4);
                    a.length = Zb.ceil(d / 4)
                },
                g: function() {
                    var a = cc.g.call(this);
                    a.f = this.f.slice(0);
                    return a
                },
                random: function(a) {
                    for (var d = [], b = 0; b < a; b += 4) d.push(4294967296 * Zb.random() | 0);
                    return new ec.a(d, a)
                }
            }),
            fc = $b.I = {},
            dc = fc.ga = {
                stringify: function(a) {
                    for (var d = a.f, a = a.b, b = [], c = 0; c < a; c++) {
                        var e = d[c >>> 2] >>> 24 - 8 * (c % 4) & 255;
                        b.push((e >>> 4).toString(16));
                        b.push((e & 15).toString(16))
                    }
                    return b.join("")
                },
                parse: function(a) {
                    for (var d = a.length, b = [], c = 0; c < d; c += 2) b[c >>> 3] |= parseInt(a.substr(c, 2), 16) << 24 - 4 * (c % 8);
                    return new ec.a(b, d / 2)
                }
            },
            gc = fc.ia = {
                stringify: function(a) {
                    for (var d = a.f, a = a.b, b = [], c = 0; c < a; c++) b.push(String.fromCharCode(d[c >>>
                        2] >>> 24 - 8 * (c % 4) & 255));
                    return b.join("")
                },
                parse: function(a) {
                    for (var d = a.length, b = [], c = 0; c < d; c++) b[c >>> 2] |= (a.charCodeAt(c) & 255) << 24 - 8 * (c % 4);
                    return new ec.a(b, d)
                }
            },
            hc = fc.U = {
                stringify: function(a) {
                    try {
                        return decodeURIComponent(escape(gc.stringify(a)))
                    } catch (d) {
                        throw Error("Malformed UTF-8 data");
                    }
                },
                parse: function(a) {
                    return gc.parse(unescape(encodeURIComponent(a)))
                }
            },
            ic = ac.fa = cc.extend({
                reset: function() {
                    this.k = new ec.a;
                    this.q = 0
                },
                v: function(a) {
                    "string" == typeof a && (a = hc.parse(a));
                    this.k.concat(a);
                    this.q +=
                        a.b
                },
                A: function(a) {
                    var d = this.k,
                        b = d.f,
                        c = d.b,
                        e = this.C,
                        p = c / (4 * e),
                        p = a ? Zb.ceil(p) : Zb.max((p | 0) - this.ba, 0),
                        a = p * e,
                        c = Zb.min(4 * a, c);
                    if (a) {
                        for (var q = 0; q < a; q += e) this.Y(b, q);
                        q = b.splice(0, a);
                        d.b -= c
                    }
                    return new ec.a(q, c)
                },
                g: function() {
                    var a = cc.g.call(this);
                    a.k = this.k.g();
                    return a
                },
                ba: 0
            });
        ac.R = ic.extend({
            F: cc.extend(),
            a: function(a) {
                this.F = this.F.extend(a);
                this.reset()
            },
            reset: function() {
                ic.reset.call(this);
                this.Z()
            },
            update: function(a) {
                this.v(a);
                this.A();
                return this
            },
            j: function(a) {
                a && this.v(a);
                return this.X()
            },
            C: 16,
            V: function(a) {
                return function(d, b) {
                    return (new a.a(b)).j(d)
                }
            },
            W: function(a) {
                return function(d, b) {
                    return (new jc.P.a(a, b)).j(d)
                }
            }
        });
        var jc = $b.B = {};
        Vb = $b
    }
    for (var V = Vb, kc = Math, nc = V.t, oc = nc.u, pc = nc.R, nc = V.B, rc = [], vc = [], wc = 2, xc = 0; 64 > xc;) {
        var yc;
        a: {
            yc = wc;
            for (var zc = kc.sqrt(yc), Ac = 2; Ac <= zc; Ac++)
                if (!(yc % Ac)) {
                    yc = A;
                    break a
                }
            yc = v
        }
        yc && (8 > xc && (rc[xc] = 4294967296 * (kc.pow(wc, 0.5) - (kc.pow(wc, 0.5) | 0)) | 0), vc[xc] = 4294967296 * (kc.pow(wc, 1 / 3) - (kc.pow(wc, 1 / 3) | 0)) | 0, xc++);
        wc++
    }
    var Bc = [],
        nc = nc.T = pc.extend({
            Z: function() {
                this.n = new oc.a(rc.slice(0))
            },
            Y: function(a, d) {
                for (var b = this.n.f, c = b[0], e = b[1], p = b[2], q = b[3], g = b[4], t = b[5], x = b[6], s = b[7], r = 0; 64 > r; r++) {
                    if (16 > r) Bc[r] = a[d + r] | 0;
                    else {
                        var f = Bc[r - 15],
                            h = Bc[r - 2];
                        Bc[r] = ((f << 25 | f >>> 7) ^ (f << 14 | f >>> 18) ^ f >>> 3) + Bc[r - 7] + ((h << 15 | h >>> 17) ^ (h << 13 | h >>> 19) ^ h >>> 10) + Bc[r - 16]
                    }
                    f = s + ((g << 26 | g >>> 6) ^ (g << 21 | g >>> 11) ^ (g << 7 | g >>> 25)) + (g & t ^ ~g & x) + vc[r] + Bc[r];
                    h = ((c << 30 | c >>> 2) ^ (c << 19 | c >>> 13) ^ (c << 10 | c >>> 22)) + (c & e ^ c & p ^ e & p);
                    s = x;
                    x = t;
                    t = g;
                    g = q + f | 0;
                    q = p;
                    p = e;
                    e = c;
                    c = f + h |
                        0
                }
                b[0] = b[0] + c | 0;
                b[1] = b[1] + e | 0;
                b[2] = b[2] + p | 0;
                b[3] = b[3] + q | 0;
                b[4] = b[4] + g | 0;
                b[5] = b[5] + t | 0;
                b[6] = b[6] + x | 0;
                b[7] = b[7] + s | 0
            },
            X: function() {
                var a = this.k,
                    d = a.f,
                    b = 8 * this.q,
                    c = 8 * a.b;
                d[c >>> 5] |= 128 << 24 - c % 32;
                d[(c + 64 >>> 9 << 4) + 14] = kc.floor(b / 4294967296);
                d[(c + 64 >>> 9 << 4) + 15] = b;
                a.b = 4 * d.length;
                this.A();
                return this.n
            },
            g: function() {
                var a = pc.g.call(this);
                a.n = this.n.g();
                return a
            }
        });
    V.T = pc.V(nc);
    V.ha = pc.W(nc);
    var Cc = V.I.U;
    V.B.P = V.t.O.extend({
        a: function(a, d) {
            a = this.p = new a.a;
            "string" == typeof d && (d = Cc.parse(d));
            var b = a.C,
                c = 4 * b;
            d.b > c && (d = a.j(d));
            d.s();
            for (var e = this.ca = d.g(), p = this.aa = d.g(), q = e.f, g = p.f, t = 0; t < b; t++) q[t] ^= 1549556828, g[t] ^= 909522486;
            e.b = p.b = c;
            this.reset()
        },
        reset: function() {
            var a = this.p;
            a.reset();
            a.update(this.aa)
        },
        update: function(a) {
            this.p.update(a);
            return this
        },
        j: function(a) {
            var d = this.p,
                a = d.j(a);
            d.reset();
            return d.j(this.ca.g().concat(a))
        }
    });
    var Dc = V.t.u;
    V.I.ea = {
        stringify: function(a) {
            var d = a.f,
                b = a.b,
                c = this.z;
            a.s();
            for (var a = [], e = 0; e < b; e += 3)
                for (var p = (d[e >>> 2] >>> 24 - 8 * (e % 4) & 255) << 16 | (d[e + 1 >>> 2] >>> 24 - 8 * ((e + 1) % 4) & 255) << 8 | d[e + 2 >>> 2] >>> 24 - 8 * ((e + 2) % 4) & 255, q = 0; 4 > q && e + 0.75 * q < b; q++) a.push(c.charAt(p >>> 6 * (3 - q) & 63));
            if (d = c.charAt(64))
                for (; a.length % 4;) a.push(d);
            return a.join("")
        },
        parse: function(a) {
            var d = a.length,
                b = this.z,
                c = b.charAt(64);
            c && (c = a.indexOf(c), -1 != c && (d = c));
            for (var c = [], e = 0, p = 0; p < d; p++) p % 4 && (c[e >>> 2] |= (b.indexOf(a.charAt(p - 1)) << 2 * (p % 4) | b.indexOf(a.charAt(p)) >>>
                6 - 2 * (p % 4)) << 24 - 8 * (e % 4), e++);
            return Dc.create(c, e)
        },
        z: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
    };
})();

/*===============================
https://www.equipmentone.com/e1_2/js/jquery.viewport.mini.js
================================================================================*/
;
(function($) {
    $.belowthefold = function(element, settings) {
        var fold = $(window).height() + $(window).scrollTop();
        return fold <= $(element).offset().top - settings.threshold;
    };
    $.abovethetop = function(element, settings) {
        var top = $(window).scrollTop();
        return top >= $(element).offset().top + $(element).height() - settings.threshold;
    };
    $.rightofscreen = function(element, settings) {
        var fold = $(window).width() + $(window).scrollLeft();
        return fold <= $(element).offset().left - settings.threshold;
    };
    $.leftofscreen = function(element, settings) {
        var left = $(window).scrollLeft();
        return left >= $(element).offset().left + $(element).width() - settings.threshold;
    };
    $.inviewport = function(element, settings) {
        return !$.rightofscreen(element, settings) && !$.leftofscreen(element, settings) && !$.belowthefold(element, settings) && !$.abovethetop(element, settings);
    };
    $.extend($.expr[':'], {
        "below-the-fold": function(a, i, m) {
            return $.belowthefold(a, {
                threshold: 0
            });
        },
        "above-the-top": function(a, i, m) {
            return $.abovethetop(a, {
                threshold: 0
            });
        },
        "left-of-screen": function(a, i, m) {
            return $.leftofscreen(a, {
                threshold: 0
            });
        },
        "right-of-screen": function(a, i, m) {
            return $.rightofscreen(a, {
                threshold: 0
            });
        },
        "in-viewport": function(a, i, m) {
            return $.inviewport(a, {
                threshold: 0
            });
        }
    });
})(jQuery);

/*===============================
https://www.equipmentone.com/e1_2/js/date.js
================================================================================*/
;
Date.CultureInfo = {
    name: "en-US",
    englishName: "English (United States)",
    nativeName: "English (United States)",
    dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    abbreviatedDayNames: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    shortestDayNames: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
    firstLetterDayNames: ["S", "M", "T", "W", "T", "F", "S"],
    monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    abbreviatedMonthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    amDesignator: "AM",
    pmDesignator: "PM",
    firstDayOfWeek: 0,
    twoDigitYearMax: 2029,
    dateElementOrder: "mdy",
    formatPatterns: {
        shortDate: "M/d/yyyy",
        longDate: "dddd, MMMM dd, yyyy",
        shortTime: "h:mm tt",
        longTime: "h:mm:ss tt",
        fullDateTime: "dddd, MMMM dd, yyyy h:mm:ss tt",
        sortableDateTime: "yyyy-MM-ddTHH:mm:ss",
        universalSortableDateTime: "yyyy-MM-dd HH:mm:ssZ",
        rfc1123: "ddd, dd MMM yyyy HH:mm:ss GMT",
        monthDay: "MMMM dd",
        yearMonth: "MMMM, yyyy"
    },
    regexPatterns: {
        jan: /^jan(uary)?/i,
        feb: /^feb(ruary)?/i,
        mar: /^mar(ch)?/i,
        apr: /^apr(il)?/i,
        may: /^may/i,
        jun: /^jun(e)?/i,
        jul: /^jul(y)?/i,
        aug: /^aug(ust)?/i,
        sep: /^sep(t(ember)?)?/i,
        oct: /^oct(ober)?/i,
        nov: /^nov(ember)?/i,
        dec: /^dec(ember)?/i,
        sun: /^su(n(day)?)?/i,
        mon: /^mo(n(day)?)?/i,
        tue: /^tu(e(s(day)?)?)?/i,
        wed: /^we(d(nesday)?)?/i,
        thu: /^th(u(r(s(day)?)?)?)?/i,
        fri: /^fr(i(day)?)?/i,
        sat: /^sa(t(urday)?)?/i,
        future: /^next/i,
        past: /^last|past|prev(ious)?/i,
        add: /^(\+|after|from)/i,
        subtract: /^(\-|before|ago)/i,
        yesterday: /^yesterday/i,
        today: /^t(oday)?/i,
        tomorrow: /^tomorrow/i,
        now: /^n(ow)?/i,
        millisecond: /^ms|milli(second)?s?/i,
        second: /^sec(ond)?s?/i,
        minute: /^min(ute)?s?/i,
        hour: /^h(ou)?rs?/i,
        week: /^w(ee)?k/i,
        month: /^m(o(nth)?s?)?/i,
        day: /^d(ays?)?/i,
        year: /^y((ea)?rs?)?/i,
        shortMeridian: /^(a|p)/i,
        longMeridian: /^(a\.?m?\.?|p\.?m?\.?)/i,
        timezone: /^((e(s|d)t|c(s|d)t|m(s|d)t|p(s|d)t)|((gmt)?\s*(\+|\-)\s*\d\d\d\d?)|gmt)/i,
        ordinalSuffix: /^\s*(st|nd|rd|th)/i,
        timeContext: /^\s*(\:|a|p)/i
    },
    abbreviatedTimeZoneStandard: {
        GMT: "-000",
        EST: "-0400",
        CST: "-0500",
        MST: "-0600",
        PST: "-0700"
    },
    abbreviatedTimeZoneDST: {
        GMT: "-000",
        EDT: "-0500",
        CDT: "-0600",
        MDT: "-0700",
        PDT: "-0800"
    }
};
Date.getMonthNumberFromName = function(name) {
    var n = Date.CultureInfo.monthNames,
        m = Date.CultureInfo.abbreviatedMonthNames,
        s = name.toLowerCase();
    for (var i = 0; i < n.length; i++) {
        if (n[i].toLowerCase() == s || m[i].toLowerCase() == s) {
            return i;
        }
    }
    return -1;
};
Date.getDayNumberFromName = function(name) {
    var n = Date.CultureInfo.dayNames,
        m = Date.CultureInfo.abbreviatedDayNames,
        o = Date.CultureInfo.shortestDayNames,
        s = name.toLowerCase();
    for (var i = 0; i < n.length; i++) {
        if (n[i].toLowerCase() == s || m[i].toLowerCase() == s) {
            return i;
        }
    }
    return -1;
};
Date.isLeapYear = function(year) {
    return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
};
Date.getDaysInMonth = function(year, month) {
    return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
};
Date.getTimezoneOffset = function(s, dst) {
    return (dst || false) ? Date.CultureInfo.abbreviatedTimeZoneDST[s.toUpperCase()] : Date.CultureInfo.abbreviatedTimeZoneStandard[s.toUpperCase()];
};
Date.getTimezoneAbbreviation = function(offset, dst) {
    var n = (dst || false) ? Date.CultureInfo.abbreviatedTimeZoneDST : Date.CultureInfo.abbreviatedTimeZoneStandard,
        p;
    for (p in n) {
        if (n[p] === offset) {
            return p;
        }
    }
    return null;
};
Date.prototype.clone = function() {
    return new Date(this.getTime());
};
Date.prototype.compareTo = function(date) {
    if (isNaN(this)) {
        throw new Error(this);
    }
    if (date instanceof Date && !isNaN(date)) {
        return (this > date) ? 1 : (this < date) ? -1 : 0;
    } else {
        throw new TypeError(date);
    }
};
Date.prototype.equals = function(date) {
    return (this.compareTo(date) === 0);
};
Date.prototype.between = function(start, end) {
    var t = this.getTime();
    return t >= start.getTime() && t <= end.getTime();
};
Date.prototype.addMilliseconds = function(value) {
    this.setMilliseconds(this.getMilliseconds() + value);
    return this;
};
Date.prototype.addSeconds = function(value) {
    return this.addMilliseconds(value * 1000);
};
Date.prototype.addMinutes = function(value) {
    return this.addMilliseconds(value * 60000);
};
Date.prototype.addHours = function(value) {
    return this.addMilliseconds(value * 3600000);
};
Date.prototype.addDays = function(value) {
    return this.addMilliseconds(value * 86400000);
};
Date.prototype.addWeeks = function(value) {
    return this.addMilliseconds(value * 604800000);
};
Date.prototype.addMonths = function(value) {
    var n = this.getDate();
    this.setDate(1);
    this.setMonth(this.getMonth() + value);
    this.setDate(Math.min(n, this.getDaysInMonth()));
    return this;
};
Date.prototype.addYears = function(value) {
    return this.addMonths(value * 12);
};
Date.prototype.add = function(config) {
    if (typeof config == "number") {
        this._orient = config;
        return this;
    }
    var x = config;
    if (x.millisecond || x.milliseconds) {
        this.addMilliseconds(x.millisecond || x.milliseconds);
    }
    if (x.second || x.seconds) {
        this.addSeconds(x.second || x.seconds);
    }
    if (x.minute || x.minutes) {
        this.addMinutes(x.minute || x.minutes);
    }
    if (x.hour || x.hours) {
        this.addHours(x.hour || x.hours);
    }
    if (x.month || x.months) {
        this.addMonths(x.month || x.months);
    }
    if (x.year || x.years) {
        this.addYears(x.year || x.years);
    }
    if (x.day || x.days) {
        this.addDays(x.day || x.days);
    }
    return this;
};
Date._validate = function(value, min, max, name) {
    if (typeof value != "number") {
        throw new TypeError(value + " is not a Number.");
    } else if (value < min || value > max) {
        throw new RangeError(value + " is not a valid value for " + name + ".");
    }
    return true;
};
Date.validateMillisecond = function(n) {
    return Date._validate(n, 0, 999, "milliseconds");
};
Date.validateSecond = function(n) {
    return Date._validate(n, 0, 59, "seconds");
};
Date.validateMinute = function(n) {
    return Date._validate(n, 0, 59, "minutes");
};
Date.validateHour = function(n) {
    return Date._validate(n, 0, 23, "hours");
};
Date.validateDay = function(n, year, month) {
    return Date._validate(n, 1, Date.getDaysInMonth(year, month), "days");
};
Date.validateMonth = function(n) {
    return Date._validate(n, 0, 11, "months");
};
Date.validateYear = function(n) {
    return Date._validate(n, 1, 9999, "seconds");
};
Date.prototype.set = function(config) {
    var x = config;
    if (!x.millisecond && x.millisecond !== 0) {
        x.millisecond = -1;
    }
    if (!x.second && x.second !== 0) {
        x.second = -1;
    }
    if (!x.minute && x.minute !== 0) {
        x.minute = -1;
    }
    if (!x.hour && x.hour !== 0) {
        x.hour = -1;
    }
    if (!x.day && x.day !== 0) {
        x.day = -1;
    }
    if (!x.month && x.month !== 0) {
        x.month = -1;
    }
    if (!x.year && x.year !== 0) {
        x.year = -1;
    }
    if (x.millisecond != -1 && Date.validateMillisecond(x.millisecond)) {
        this.addMilliseconds(x.millisecond - this.getMilliseconds());
    }
    if (x.second != -1 && Date.validateSecond(x.second)) {
        this.addSeconds(x.second - this.getSeconds());
    }
    if (x.minute != -1 && Date.validateMinute(x.minute)) {
        this.addMinutes(x.minute - this.getMinutes());
    }
    if (x.hour != -1 && Date.validateHour(x.hour)) {
        this.addHours(x.hour - this.getHours());
    }
    if (x.month !== -1 && Date.validateMonth(x.month)) {
        this.addMonths(x.month - this.getMonth());
    }
    if (x.year != -1 && Date.validateYear(x.year)) {
        this.addYears(x.year - this.getFullYear());
    }
    if (x.day != -1 && Date.validateDay(x.day, this.getFullYear(), this.getMonth())) {
        this.addDays(x.day - this.getDate());
    }
    if (x.timezone) {
        this.setTimezone(x.timezone);
    }
    if (x.timezoneOffset) {
        this.setTimezoneOffset(x.timezoneOffset);
    }
    return this;
};
Date.prototype.clearTime = function() {
    this.setHours(0);
    this.setMinutes(0);
    this.setSeconds(0);
    this.setMilliseconds(0);
    return this;
};
Date.prototype.isLeapYear = function() {
    var y = this.getFullYear();
    return (((y % 4 === 0) && (y % 100 !== 0)) || (y % 400 === 0));
};
Date.prototype.isWeekday = function() {
    return !(this.is().sat() || this.is().sun());
};
Date.prototype.getDaysInMonth = function() {
    return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
};
Date.prototype.moveToFirstDayOfMonth = function() {
    return this.set({
        day: 1
    });
};
Date.prototype.moveToLastDayOfMonth = function() {
    return this.set({
        day: this.getDaysInMonth()
    });
};
Date.prototype.moveToDayOfWeek = function(day, orient) {
    var diff = (day - this.getDay() + 7 * (orient || +1)) % 7;
    return this.addDays((diff === 0) ? diff += 7 * (orient || +1) : diff);
};
Date.prototype.moveToMonth = function(month, orient) {
    var diff = (month - this.getMonth() + 12 * (orient || +1)) % 12;
    return this.addMonths((diff === 0) ? diff += 12 * (orient || +1) : diff);
};
Date.prototype.getDayOfYear = function() {
    return Math.floor((this - new Date(this.getFullYear(), 0, 1)) / 86400000);
};
Date.prototype.getWeekOfYear = function(firstDayOfWeek) {
    var y = this.getFullYear(),
        m = this.getMonth(),
        d = this.getDate();
    var dow = firstDayOfWeek || Date.CultureInfo.firstDayOfWeek;
    var offset = 7 + 1 - new Date(y, 0, 1).getDay();
    if (offset == 8) {
        offset = 1;
    }
    var daynum = ((Date.UTC(y, m, d, 0, 0, 0) - Date.UTC(y, 0, 1, 0, 0, 0)) / 86400000) + 1;
    var w = Math.floor((daynum - offset + 7) / 7);
    if (w === dow) {
        y--;
        var prevOffset = 7 + 1 - new Date(y, 0, 1).getDay();
        if (prevOffset == 2 || prevOffset == 8) {
            w = 53;
        } else {
            w = 52;
        }
    }
    return w;
};
Date.prototype.isDST = function() {
    console.log('isDST');
    return this.toString().match(/(E|C|M|P)(S|D)T/)[2] == "D";
};
Date.prototype.getTimezone = function() {
    return Date.getTimezoneAbbreviation(this.getUTCOffset, this.isDST());
};
Date.prototype.setTimezoneOffset = function(s) {
    var here = this.getTimezoneOffset(),
        there = Number(s) * -6 / 10;
    this.addMinutes(there - here);
    return this;
};
Date.prototype.setTimezone = function(s) {
    return this.setTimezoneOffset(Date.getTimezoneOffset(s));
};
Date.prototype.getUTCOffset = function() {
    var n = this.getTimezoneOffset() * -10 / 6,
        r;
    if (n < 0) {
        r = (n - 10000).toString();
        return r[0] + r.substr(2);
    } else {
        r = (n + 10000).toString();
        return "+" + r.substr(1);
    }
};
Date.prototype.getDayName = function(abbrev) {
    return abbrev ? Date.CultureInfo.abbreviatedDayNames[this.getDay()] : Date.CultureInfo.dayNames[this.getDay()];
};
Date.prototype.getMonthName = function(abbrev) {
    return abbrev ? Date.CultureInfo.abbreviatedMonthNames[this.getMonth()] : Date.CultureInfo.monthNames[this.getMonth()];
};
Date.prototype._toString = Date.prototype.toString;
Date.prototype.toString = function(format) {
    var self = this;
    var p = function p(s) {
        return (s.toString().length == 1) ? "0" + s : s;
    };
    return format ? format.replace(/dd?d?d?|MM?M?M?|yy?y?y?|hh?|HH?|mm?|ss?|tt?|zz?z?/g, function(format) {
        switch (format) {
            case "hh":
                return p(self.getHours() < 13 ? self.getHours() : (self.getHours() - 12));
            case "h":
                return self.getHours() < 13 ? self.getHours() : (self.getHours() - 12);
            case "HH":
                return p(self.getHours());
            case "H":
                return self.getHours();
            case "mm":
                return p(self.getMinutes());
            case "m":
                return self.getMinutes();
            case "ss":
                return p(self.getSeconds());
            case "s":
                return self.getSeconds();
            case "yyyy":
                return self.getFullYear();
            case "yy":
                return self.getFullYear().toString().substring(2, 4);
            case "dddd":
                return self.getDayName();
            case "ddd":
                return self.getDayName(true);
            case "dd":
                return p(self.getDate());
            case "d":
                return self.getDate().toString();
            case "MMMM":
                return self.getMonthName();
            case "MMM":
                return self.getMonthName(true);
            case "MM":
                return p((self.getMonth() + 1));
            case "M":
                return self.getMonth() + 1;
            case "t":
                return self.getHours() < 12 ? Date.CultureInfo.amDesignator.substring(0, 1) : Date.CultureInfo.pmDesignator.substring(0, 1);
            case "tt":
                return self.getHours() < 12 ? Date.CultureInfo.amDesignator : Date.CultureInfo.pmDesignator;
            case "zzz":
            case "zz":
            case "z":
                return "";
        }
    }) : this._toString();
};
Date.now = function() {
    return new Date();
};
Date.today = function() {
    return Date.now().clearTime();
};
Date.prototype._orient = +1;
Date.prototype.next = function() {
    this._orient = +1;
    return this;
};
Date.prototype.last = Date.prototype.prev = Date.prototype.previous = function() {
    this._orient = -1;
    return this;
};
Date.prototype._is = false;
Date.prototype.is = function() {
    this._is = true;
    return this;
};
Number.prototype._dateElement = "day";
Number.prototype.fromNow = function() {
    var c = {};
    c[this._dateElement] = this;
    return Date.now().add(c);
};
Number.prototype.ago = function() {
    var c = {};
    c[this._dateElement] = this * -1;
    return Date.now().add(c);
};
(function() {
    var $D = Date.prototype,
        $N = Number.prototype;
    var dx = ("sunday monday tuesday wednesday thursday friday saturday").split(/\s/),
        mx = ("january february march april may june july august september october november december").split(/\s/),
        px = ("Millisecond Second Minute Hour Day Week Month Year").split(/\s/),
        de;
    var df = function(n) {
        return function() {
            if (this._is) {
                this._is = false;
                return this.getDay() == n;
            }
            return this.moveToDayOfWeek(n, this._orient);
        };
    };
    for (var i = 0; i < dx.length; i++) {
        $D[dx[i]] = $D[dx[i].substring(0, 3)] = df(i);
    }
    var mf = function(n) {
        return function() {
            if (this._is) {
                this._is = false;
                return this.getMonth() === n;
            }
            return this.moveToMonth(n, this._orient);
        };
    };
    for (var j = 0; j < mx.length; j++) {
        $D[mx[j]] = $D[mx[j].substring(0, 3)] = mf(j);
    }
    var ef = function(j) {
        return function() {
            if (j.substring(j.length - 1) != "s") {
                j += "s";
            }
            return this["add" + j](this._orient);
        };
    };
    var nf = function(n) {
        return function() {
            this._dateElement = n;
            return this;
        };
    };
    for (var k = 0; k < px.length; k++) {
        de = px[k].toLowerCase();
        $D[de] = $D[de + "s"] = ef(px[k]);
        $N[de] = $N[de + "s"] = nf(de);
    }
}());
Date.prototype.toJSONString = function() {
    return this.toString("yyyy-MM-ddThh:mm:ssZ");
};
Date.prototype.toShortDateString = function() {
    return this.toString(Date.CultureInfo.formatPatterns.shortDatePattern);
};
Date.prototype.toLongDateString = function() {
    return this.toString(Date.CultureInfo.formatPatterns.longDatePattern);
};
Date.prototype.toShortTimeString = function() {
    return this.toString(Date.CultureInfo.formatPatterns.shortTimePattern);
};
Date.prototype.toLongTimeString = function() {
    return this.toString(Date.CultureInfo.formatPatterns.longTimePattern);
};
Date.prototype.getOrdinal = function() {
    switch (this.getDate()) {
        case 1:
        case 21:
        case 31:
            return "st";
        case 2:
        case 22:
            return "nd";
        case 3:
        case 23:
            return "rd";
        default:
            return "th";
    }
};
(function() {
    Date.Parsing = {
        Exception: function(s) {
            this.message = "Parse error at '" + s.substring(0, 10) + " ...'";
        }
    };
    var $P = Date.Parsing;
    var _ = $P.Operators = {
        rtoken: function(r) {
            return function(s) {
                var mx = s.match(r);
                if (mx) {
                    return ([mx[0], s.substring(mx[0].length)]);
                } else {
                    throw new $P.Exception(s);
                }
            };
        },
        token: function(s) {
            return function(s) {
                return _.rtoken(new RegExp("^\s*" + s + "\s*"))(s);
            };
        },
        stoken: function(s) {
            return _.rtoken(new RegExp("^" + s));
        },
        until: function(p) {
            return function(s) {
                var qx = [],
                    rx = null;
                while (s.length) {
                    try {
                        rx = p.call(this, s);
                    } catch (e) {
                        qx.push(rx[0]);
                        s = rx[1];
                        continue;
                    }
                    break;
                }
                return [qx, s];
            };
        },
        many: function(p) {
            return function(s) {
                var rx = [],
                    r = null;
                while (s.length) {
                    try {
                        r = p.call(this, s);
                    } catch (e) {
                        return [rx, s];
                    }
                    rx.push(r[0]);
                    s = r[1];
                }
                return [rx, s];
            };
        },
        optional: function(p) {
            return function(s) {
                var r = null;
                try {
                    r = p.call(this, s);
                } catch (e) {
                    return [null, s];
                }
                return [r[0], r[1]];
            };
        },
        not: function(p) {
            return function(s) {
                try {
                    p.call(this, s);
                } catch (e) {
                    return [null, s];
                }
                throw new $P.Exception(s);
            };
        },
        ignore: function(p) {
            return p ? function(s) {
                var r = null;
                r = p.call(this, s);
                return [null, r[1]];
            } : null;
        },
        product: function() {
            var px = arguments[0],
                qx = Array.prototype.slice.call(arguments, 1),
                rx = [];
            for (var i = 0; i < px.length; i++) {
                rx.push(_.each(px[i], qx));
            }
            return rx;
        },
        cache: function(rule) {
            var cache = {},
                r = null;
            return function(s) {
                try {
                    r = cache[s] = (cache[s] || rule.call(this, s));
                } catch (e) {
                    r = cache[s] = e;
                }
                if (r instanceof $P.Exception) {
                    throw r;
                } else {
                    return r;
                }
            };
        },
        any: function() {
            var px = arguments;
            return function(s) {
                var r = null;
                for (var i = 0; i < px.length; i++) {
                    if (px[i] == null) {
                        continue;
                    }
                    try {
                        r = (px[i].call(this, s));
                    } catch (e) {
                        r = null;
                    }
                    if (r) {
                        return r;
                    }
                }
                throw new $P.Exception(s);
            };
        },
        each: function() {
            var px = arguments;
            return function(s) {
                var rx = [],
                    r = null;
                for (var i = 0; i < px.length; i++) {
                    if (px[i] == null) {
                        continue;
                    }
                    try {
                        r = (px[i].call(this, s));
                    } catch (e) {
                        throw new $P.Exception(s);
                    }
                    rx.push(r[0]);
                    s = r[1];
                }
                return [rx, s];
            };
        },
        all: function() {
            var px = arguments,
                _ = _;
            return _.each(_.optional(px));
        },
        sequence: function(px, d, c) {
            d = d || _.rtoken(/^\s*/);
            c = c || null;
            if (px.length == 1) {
                return px[0];
            }
            return function(s) {
                var r = null,
                    q = null;
                var rx = [];
                for (var i = 0; i < px.length; i++) {
                    try {
                        r = px[i].call(this, s);
                    } catch (e) {
                        break;
                    }
                    rx.push(r[0]);
                    try {
                        q = d.call(this, r[1]);
                    } catch (ex) {
                        q = null;
                        break;
                    }
                    s = q[1];
                }
                if (!r) {
                    throw new $P.Exception(s);
                }
                if (q) {
                    throw new $P.Exception(q[1]);
                }
                if (c) {
                    try {
                        r = c.call(this, r[1]);
                    } catch (ey) {
                        throw new $P.Exception(r[1]);
                    }
                }
                return [rx, (r ? r[1] : s)];
            };
        },
        between: function(d1, p, d2) {
            d2 = d2 || d1;
            var _fn = _.each(_.ignore(d1), p, _.ignore(d2));
            return function(s) {
                var rx = _fn.call(this, s);
                return [
                    [rx[0][0], r[0][2]], rx[1]
                ];
            };
        },
        list: function(p, d, c) {
            d = d || _.rtoken(/^\s*/);
            c = c || null;
            return (p instanceof Array ? _.each(_.product(p.slice(0, -1), _.ignore(d)), p.slice(-1), _.ignore(c)) : _.each(_.many(_.each(p, _.ignore(d))), px, _.ignore(c)));
        },
        set: function(px, d, c) {
            d = d || _.rtoken(/^\s*/);
            c = c || null;
            return function(s) {
                var r = null,
                    p = null,
                    q = null,
                    rx = null,
                    best = [
                        [], s
                    ],
                    last = false;
                for (var i = 0; i < px.length; i++) {
                    q = null;
                    p = null;
                    r = null;
                    last = (px.length == 1);
                    try {
                        r = px[i].call(this, s);
                    } catch (e) {
                        continue;
                    }
                    rx = [
                        [r[0]], r[1]
                    ];
                    if (r[1].length > 0 && !last) {
                        try {
                            q = d.call(this, r[1]);
                        } catch (ex) {
                            last = true;
                        }
                    } else {
                        last = true;
                    }
                    if (!last && q[1].length === 0) {
                        last = true;
                    }
                    if (!last) {
                        var qx = [];
                        for (var j = 0; j < px.length; j++) {
                            if (i != j) {
                                qx.push(px[j]);
                            }
                        }
                        p = _.set(qx, d).call(this, q[1]);
                        if (p[0].length > 0) {
                            rx[0] = rx[0].concat(p[0]);
                            rx[1] = p[1];
                        }
                    }
                    if (rx[1].length < best[1].length) {
                        best = rx;
                    }
                    if (best[1].length === 0) {
                        break;
                    }
                }
                if (best[0].length === 0) {
                    return best;
                }
                if (c) {
                    try {
                        q = c.call(this, best[1]);
                    } catch (ey) {
                        throw new $P.Exception(best[1]);
                    }
                    best[1] = q[1];
                }
                return best;
            };
        },
        forward: function(gr, fname) {
            return function(s) {
                return gr[fname].call(this, s);
            };
        },
        replace: function(rule, repl) {
            return function(s) {
                var r = rule.call(this, s);
                return [repl, r[1]];
            };
        },
        process: function(rule, fn) {
            return function(s) {
                var r = rule.call(this, s);
                return [fn.call(this, r[0]), r[1]];
            };
        },
        min: function(min, rule) {
            return function(s) {
                var rx = rule.call(this, s);
                if (rx[0].length < min) {
                    throw new $P.Exception(s);
                }
                return rx;
            };
        }
    };
    var _generator = function(op) {
        return function() {
            var args = null,
                rx = [];
            if (arguments.length > 1) {
                args = Array.prototype.slice.call(arguments);
            } else if (arguments[0] instanceof Array) {
                args = arguments[0];
            }
            if (args) {
                for (var i = 0, px = args.shift(); i < px.length; i++) {
                    args.unshift(px[i]);
                    rx.push(op.apply(null, args));
                    args.shift();
                    return rx;
                }
            } else {
                return op.apply(null, arguments);
            }
        };
    };
    var gx = "optional not ignore cache".split(/\s/);
    for (var i = 0; i < gx.length; i++) {
        _[gx[i]] = _generator(_[gx[i]]);
    }
    var _vector = function(op) {
        return function() {
            if (arguments[0] instanceof Array) {
                return op.apply(null, arguments[0]);
            } else {
                return op.apply(null, arguments);
            }
        };
    };
    var vx = "each any all".split(/\s/);
    for (var j = 0; j < vx.length; j++) {
        _[vx[j]] = _vector(_[vx[j]]);
    }
}());
(function() {
    var flattenAndCompact = function(ax) {
        var rx = [];
        for (var i = 0; i < ax.length; i++) {
            if (ax[i] instanceof Array) {
                rx = rx.concat(flattenAndCompact(ax[i]));
            } else {
                if (ax[i]) {
                    rx.push(ax[i]);
                }
            }
        }
        return rx;
    };
    Date.Grammar = {};
    Date.Translator = {
        hour: function(s) {
            return function() {
                this.hour = Number(s);
            };
        },
        minute: function(s) {
            return function() {
                this.minute = Number(s);
            };
        },
        second: function(s) {
            return function() {
                this.second = Number(s);
            };
        },
        meridian: function(s) {
            return function() {
                this.meridian = s.slice(0, 1).toLowerCase();
            };
        },
        timezone: function(s) {
            return function() {
                var n = s.replace(/[^\d\+\-]/g, "");
                if (n.length) {
                    this.timezoneOffset = Number(n);
                } else {
                    this.timezone = s.toLowerCase();
                }
            };
        },
        day: function(x) {
            var s = x[0];
            return function() {
                this.day = Number(s.match(/\d+/)[0]);
            };
        },
        month: function(s) {
            return function() {
                this.month = ((s.length == 3) ? Date.getMonthNumberFromName(s) : (Number(s) - 1));
            };
        },
        year: function(s) {
            return function() {
                var n = Number(s);
                this.year = ((s.length > 2) ? n : (n + (((n + 2000) < Date.CultureInfo.twoDigitYearMax) ? 2000 : 1900)));
            };
        },
        rday: function(s) {
            return function() {
                switch (s) {
                    case "yesterday":
                        this.days = -1;
                        break;
                    case "tomorrow":
                        this.days = 1;
                        break;
                    case "today":
                        this.days = 0;
                        break;
                    case "now":
                        this.days = 0;
                        this.now = true;
                        break;
                }
            };
        },
        finishExact: function(x) {
            x = (x instanceof Array) ? x : [x];
            var now = new Date();
            this.year = now.getFullYear();
            this.month = now.getMonth();
            this.day = 1;
            this.hour = 0;
            this.minute = 0;
            this.second = 0;
            for (var i = 0; i < x.length; i++) {
                if (x[i]) {
                    x[i].call(this);
                }
            }
            this.hour = (this.meridian == "p" && this.hour < 13) ? this.hour + 12 : this.hour;
            if (this.day > Date.getDaysInMonth(this.year, this.month)) {
                throw new RangeError(this.day + " is not a valid value for days.");
            }
            var r = new Date(this.year, this.month, this.day, this.hour, this.minute, this.second);
            if (this.timezone) {
                r.set({
                    timezone: this.timezone
                });
            } else if (this.timezoneOffset) {
                r.set({
                    timezoneOffset: this.timezoneOffset
                });
            }
            return r;
        },
        finish: function(x) {
            x = (x instanceof Array) ? flattenAndCompact(x) : [x];
            if (x.length === 0) {
                return null;
            }
            for (var i = 0; i < x.length; i++) {
                if (typeof x[i] == "function") {
                    x[i].call(this);
                }
            }
            if (this.now) {
                return new Date();
            }
            var today = Date.today();
            var method = null;
            var expression = !!(this.days != null || this.orient || this.operator);
            if (expression) {
                var gap, mod, orient;
                orient = ((this.orient == "past" || this.operator == "subtract") ? -1 : 1);
                if (this.weekday) {
                    this.unit = "day";
                    gap = (Date.getDayNumberFromName(this.weekday) - today.getDay());
                    mod = 7;
                    this.days = gap ? ((gap + (orient * mod)) % mod) : (orient * mod);
                }
                if (this.month) {
                    this.unit = "month";
                    gap = (this.month - today.getMonth());
                    mod = 12;
                    this.months = gap ? ((gap + (orient * mod)) % mod) : (orient * mod);
                    this.month = null;
                }
                if (!this.unit) {
                    this.unit = "day";
                }
                if (this[this.unit + "s"] == null || this.operator != null) {
                    if (!this.value) {
                        this.value = 1;
                    }
                    if (this.unit == "week") {
                        this.unit = "day";
                        this.value = this.value * 7;
                    }
                    this[this.unit + "s"] = this.value * orient;
                }
                return today.add(this);
            } else {
                if (this.meridian && this.hour) {
                    this.hour = (this.hour < 13 && this.meridian == "p") ? this.hour + 12 : this.hour;
                }
                if (this.weekday && !this.day) {
                    this.day = (today.addDays((Date.getDayNumberFromName(this.weekday) - today.getDay()))).getDate();
                }
                if (this.month && !this.day) {
                    this.day = 1;
                }
                return today.set(this);
            }
        }
    };
    var _ = Date.Parsing.Operators,
        g = Date.Grammar,
        t = Date.Translator,
        _fn;
    g.datePartDelimiter = _.rtoken(/^([\s\-\.\,\/\x27]+)/);
    g.timePartDelimiter = _.stoken(":");
    g.whiteSpace = _.rtoken(/^\s*/);
    g.generalDelimiter = _.rtoken(/^(([\s\,]|at|on)+)/);
    var _C = {};
    g.ctoken = function(keys) {
        var fn = _C[keys];
        if (!fn) {
            var c = Date.CultureInfo.regexPatterns;
            var kx = keys.split(/\s+/),
                px = [];
            for (var i = 0; i < kx.length; i++) {
                px.push(_.replace(_.rtoken(c[kx[i]]), kx[i]));
            }
            fn = _C[keys] = _.any.apply(null, px);
        }
        return fn;
    };
    g.ctoken2 = function(key) {
        return _.rtoken(Date.CultureInfo.regexPatterns[key]);
    };
    g.h = _.cache(_.process(_.rtoken(/^(0[0-9]|1[0-2]|[1-9])/), t.hour));
    g.hh = _.cache(_.process(_.rtoken(/^(0[0-9]|1[0-2])/), t.hour));
    g.H = _.cache(_.process(_.rtoken(/^([0-1][0-9]|2[0-3]|[0-9])/), t.hour));
    g.HH = _.cache(_.process(_.rtoken(/^([0-1][0-9]|2[0-3])/), t.hour));
    g.m = _.cache(_.process(_.rtoken(/^([0-5][0-9]|[0-9])/), t.minute));
    g.mm = _.cache(_.process(_.rtoken(/^[0-5][0-9]/), t.minute));
    g.s = _.cache(_.process(_.rtoken(/^([0-5][0-9]|[0-9])/), t.second));
    g.ss = _.cache(_.process(_.rtoken(/^[0-5][0-9]/), t.second));
    g.hms = _.cache(_.sequence([g.H, g.mm, g.ss], g.timePartDelimiter));
    g.t = _.cache(_.process(g.ctoken2("shortMeridian"), t.meridian));
    g.tt = _.cache(_.process(g.ctoken2("longMeridian"), t.meridian));
    g.z = _.cache(_.process(_.rtoken(/^(\+|\-)?\s*\d\d\d\d?/), t.timezone));
    g.zz = _.cache(_.process(_.rtoken(/^(\+|\-)\s*\d\d\d\d/), t.timezone));
    g.zzz = _.cache(_.process(g.ctoken2("timezone"), t.timezone));
    g.timeSuffix = _.each(_.ignore(g.whiteSpace), _.set([g.tt, g.zzz]));
    g.time = _.each(_.optional(_.ignore(_.stoken("T"))), g.hms, g.timeSuffix);
    g.d = _.cache(_.process(_.each(_.rtoken(/^([0-2]\d|3[0-1]|\d)/), _.optional(g.ctoken2("ordinalSuffix"))), t.day));
    g.dd = _.cache(_.process(_.each(_.rtoken(/^([0-2]\d|3[0-1])/), _.optional(g.ctoken2("ordinalSuffix"))), t.day));
    g.ddd = g.dddd = _.cache(_.process(g.ctoken("sun mon tue wed thu fri sat"), function(s) {
        return function() {
            this.weekday = s;
        };
    }));
    g.M = _.cache(_.process(_.rtoken(/^(1[0-2]|0\d|\d)/), t.month));
    g.MM = _.cache(_.process(_.rtoken(/^(1[0-2]|0\d)/), t.month));
    g.MMM = g.MMMM = _.cache(_.process(g.ctoken("jan feb mar apr may jun jul aug sep oct nov dec"), t.month));
    g.y = _.cache(_.process(_.rtoken(/^(\d\d?)/), t.year));
    g.yy = _.cache(_.process(_.rtoken(/^(\d\d)/), t.year));
    g.yyy = _.cache(_.process(_.rtoken(/^(\d\d?\d?\d?)/), t.year));
    g.yyyy = _.cache(_.process(_.rtoken(/^(\d\d\d\d)/), t.year));
    _fn = function() {
        return _.each(_.any.apply(null, arguments), _.not(g.ctoken2("timeContext")));
    };
    g.day = _fn(g.d, g.dd);
    g.month = _fn(g.M, g.MMM);
    g.year = _fn(g.yyyy, g.yy);
    g.orientation = _.process(g.ctoken("past future"), function(s) {
        return function() {
            this.orient = s;
        };
    });
    g.operator = _.process(g.ctoken("add subtract"), function(s) {
        return function() {
            this.operator = s;
        };
    });
    g.rday = _.process(g.ctoken("yesterday tomorrow today now"), t.rday);
    g.unit = _.process(g.ctoken("minute hour day week month year"), function(s) {
        return function() {
            this.unit = s;
        };
    });
    g.value = _.process(_.rtoken(/^\d\d?(st|nd|rd|th)?/), function(s) {
        return function() {
            this.value = s.replace(/\D/g, "");
        };
    });
    g.expression = _.set([g.rday, g.operator, g.value, g.unit, g.orientation, g.ddd, g.MMM]);
    _fn = function() {
        return _.set(arguments, g.datePartDelimiter);
    };
    g.mdy = _fn(g.ddd, g.month, g.day, g.year);
    g.ymd = _fn(g.ddd, g.year, g.month, g.day);
    g.dmy = _fn(g.ddd, g.day, g.month, g.year);
    g.date = function(s) {
        return ((g[Date.CultureInfo.dateElementOrder] || g.mdy).call(this, s));
    };
    g.format = _.process(_.many(_.any(_.process(_.rtoken(/^(dd?d?d?|MM?M?M?|yy?y?y?|hh?|HH?|mm?|ss?|tt?|zz?z?)/), function(fmt) {
        if (g[fmt]) {
            return g[fmt];
        } else {
            throw Date.Parsing.Exception(fmt);
        }
    }), _.process(_.rtoken(/^[^dMyhHmstz]+/), function(s) {
        return _.ignore(_.stoken(s));
    }))), function(rules) {
        return _.process(_.each.apply(null, rules), t.finishExact);
    });
    var _F = {};
    var _get = function(f) {
        return _F[f] = (_F[f] || g.format(f)[0]);
    };
    g.formats = function(fx) {
        if (fx instanceof Array) {
            var rx = [];
            for (var i = 0; i < fx.length; i++) {
                rx.push(_get(fx[i]));
            }
            return _.any.apply(null, rx);
        } else {
            return _get(fx);
        }
    };
    g._formats = g.formats(["yyyy-MM-ddTHH:mm:ss", "ddd, MMM dd, yyyy H:mm:ss tt", "ddd MMM d yyyy HH:mm:ss zzz", "d"]);
    g._start = _.process(_.set([g.date, g.time, g.expression], g.generalDelimiter, g.whiteSpace), t.finish);
    g.start = function(s) {
        try {
            var r = g._formats.call({}, s);
            if (r[1].length === 0) {
                return r;
            }
        } catch (e) {}
        return g._start.call({}, s);
    };
}());
Date._parse = Date.parse;
Date.parse = function(s) {
    var r = null;
    if (!s) {
        return null;
    }
    try {
        r = Date.Grammar.start.call({}, s);
    } catch (e) {
        return null;
    }
    return ((r[1].length === 0) ? r[0] : null);
};
Date.getParseFunction = function(fx) {
    var fn = Date.Grammar.formats(fx);
    return function(s) {
        var r = null;
        try {
            r = fn.call({}, s);
        } catch (e) {
            return null;
        }
        return ((r[1].length === 0) ? r[0] : null);
    };
};
Date.parseExact = function(s, fx) {
    return Date.getParseFunction(fx)(s);
};

/*===============================
https://www.equipmentone.com/e1_2/js/jquery.countdown.js
================================================================================*/
;
/*!
 * The Final Countdown for jQuery v2.0.4 (http://hilios.github.io/jQuery.countdown/)
 * Copyright (c) 2014 Edson Hilios
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
(function(factory) {
    "use strict";
    if (typeof define === "function" && define.amd) {
        define(["jquery"], factory);
    } else {
        factory(jQuery);
    }
})(function($) {
    "use strict";
    var PRECISION = 1000;
    var instances = [],
        matchers = [];
    matchers.push(/^[0-9]*$/.source);
    matchers.push(/([0-9]{1,2}\/){2}[0-9]{4}( [0-9]{1,2}(:[0-9]{2}){2})?/.source);
    matchers.push(/[0-9]{4}([\/\-][0-9]{1,2}){2}( [0-9]{1,2}(:[0-9]{2}){2})?/.source);
    matchers = new RegExp(matchers.join("|"));

    function parseDateString(dateString) {
        if (dateString instanceof Date) {
            return dateString;
        }
        if (String(dateString).match(matchers)) {
            if (String(dateString).match(/^[0-9]*$/)) {
                dateString = Number(dateString);
            }
            if (String(dateString).match(/\-/)) {
                dateString = String(dateString).replace(/\-/g, "/");
            }
            return new Date(dateString);
        } else {
            throw new Error("Couldn't cast `" + dateString + "` to a date object.");
        }
    }
    var DIRECTIVE_KEY_MAP = {
        Y: "years",
        m: "months",
        w: "weeks",
        d: "days",
        D: "totalDays",
        H: "hours",
        M: "minutes",
        S: "seconds"
    };

    function strftime(offsetObject) {
        return function(format) {
            var directives = format.match(/%(-|!)?[A-Z]{1}(:[^;]+;)?/gi);
            if (directives) {
                for (var i = 0, len = directives.length; i < len; ++i) {
                    var directive = directives[i].match(/%(-|!)?([a-zA-Z]{1})(:[^;]+;)?/),
                        regexp = new RegExp(directive[0]),
                        modifier = directive[1] || "",
                        plural = directive[3] || "",
                        value = null;
                    directive = directive[2];
                    if (DIRECTIVE_KEY_MAP.hasOwnProperty(directive)) {
                        value = DIRECTIVE_KEY_MAP[directive];
                        value = Number(offsetObject[value]);
                    }
                    if (value !== null) {
                        if (modifier === "!") {
                            value = pluralize(plural, value);
                        }
                        if (modifier === "") {
                            if (value < 10) {
                                value = "0" + value.toString();
                            }
                        }
                        format = format.replace(regexp, value.toString());
                    }
                }
            }
            format = format.replace(/%%/, "%");
            return format;
        };
    }

    function pluralize(format, count) {
        var plural = "s",
            singular = "";
        if (format) {
            format = format.replace(/(:|;|\s)/gi, "").split(/\,/);
            if (format.length === 1) {
                plural = format[0];
            } else {
                singular = format[0];
                plural = format[1];
            }
        }
        if (Math.abs(count) === 1) {
            return singular;
        } else {
            return plural;
        }
    }
    var Countdown = function(el, finalDate, callback) {
        this.el = el;
        this.$el = $(el);
        this.interval = null;
        this.offset = {};
        this.instanceNumber = instances.length;
        instances.push(this);
        this.$el.data("countdown-instance", this.instanceNumber);
        if (callback) {
            this.$el.on("update.countdown", callback);
            this.$el.on("stoped.countdown", callback);
            this.$el.on("finish.countdown", callback);
        }
        this.setFinalDate(finalDate);
        this.start();
    };
    $.extend(Countdown.prototype, {
        start: function() {
            if (this.interval !== null) {
                clearInterval(this.interval);
            }
            var self = this;
            this.update();
            this.interval = setInterval(function() {
                self.update.call(self);
            }, PRECISION);
        },
        stop: function() {
            clearInterval(this.interval);
            this.interval = null;
            this.dispatchEvent("stoped");
        },
        pause: function() {
            this.stop.call(this);
        },
        resume: function() {
            this.start.call(this);
        },
        remove: function() {
            this.stop();
            instances[this.instanceNumber] = null;
            delete this.$el.data().countdownInstance;
        },
        setFinalDate: function(value) {
            this.finalDate = parseDateString(value);
        },
        update: function() {
            if (this.$el.closest("html").length === 0) {
                this.remove();
                return;
            }
            this.totalSecsLeft = this.finalDate.getTime() - new Date().add({
                seconds: timeOffsetSecs
            }).getTime();
            this.totalSecsLeft = Math.ceil(this.totalSecsLeft / 1e3);
            this.totalSecsLeft = this.totalSecsLeft < 0 ? 0 : this.totalSecsLeft;
            this.offset = {
                seconds: this.totalSecsLeft % 60,
                minutes: Math.floor(this.totalSecsLeft / 60) % 60,
                hours: Math.floor(this.totalSecsLeft / 60 / 60) % 24,
                days: Math.floor(this.totalSecsLeft / 60 / 60 / 24) % 7,
                totalDays: Math.floor(this.totalSecsLeft / 60 / 60 / 24),
                weeks: Math.floor(this.totalSecsLeft / 60 / 60 / 24 / 7),
                months: Math.floor(this.totalSecsLeft / 60 / 60 / 24 / 30),
                years: Math.floor(this.totalSecsLeft / 60 / 60 / 24 / 365)
            };
            if (this.totalSecsLeft === 0) {
                this.stop();
                this.dispatchEvent("finish");
            } else {
                this.dispatchEvent("update");
            }
        },
        dispatchEvent: function(eventName) {
            var event = $.Event(eventName + ".countdown");
            event.finalDate = this.finalDate;
            event.offset = $.extend({}, this.offset);
            event.strftime = strftime(this.offset);
            this.$el.trigger(event);
        }
    });
    $.fn.countdown = function() {
        var argumentsArray = Array.prototype.slice.call(arguments, 0);
        return this.each(function() {
            var instanceNumber = $(this).data("countdown-instance");
            if (instanceNumber !== undefined) {
                var instance = instances[instanceNumber],
                    method = argumentsArray[0];
                if (Countdown.prototype.hasOwnProperty(method)) {
                    instance[method].apply(instance, argumentsArray.slice(1));
                } else if (String(method).match(/^[$A-Z_][0-9A-Z_$]*$/i) === null) {
                    instance.setFinalDate.call(instance, method);
                    instance.start();
                } else {
                    $.error("Method %s does not exist on jQuery.countdown".replace(/\%s/gi, method));
                }
            } else {
                new Countdown(this, argumentsArray[0], argumentsArray[1]);
            }
        });
    };
});

/*===============================
https://www.equipmentone.com/e1_2/js/moment.js
================================================================================*/
;
(function(undefined) {
    var moment, VERSION = '2.9.0',
        globalScope = (typeof global !== 'undefined' && (typeof window === 'undefined' || window === global.window)) ? global : this,
        oldGlobalMoment, round = Math.round,
        hasOwnProperty = Object.prototype.hasOwnProperty,
        i, YEAR = 0,
        MONTH = 1,
        DATE = 2,
        HOUR = 3,
        MINUTE = 4,
        SECOND = 5,
        MILLISECOND = 6,
        locales = {},
        momentProperties = [],
        hasModule = (typeof module !== 'undefined' && module && module.exports),
        aspNetJsonRegex = /^\/?Date\((\-?\d+)/i,
        aspNetTimeSpanJsonRegex = /(\-)?(?:(\d*)\.)?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?)?/,
        isoDurationRegex = /^(-)?P(?:(?:([0-9,.]*)Y)?(?:([0-9,.]*)M)?(?:([0-9,.]*)D)?(?:T(?:([0-9,.]*)H)?(?:([0-9,.]*)M)?(?:([0-9,.]*)S)?)?|([0-9,.]*)W)$/,
        formattingTokens = /(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Q|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|S{1,4}|x|X|zz?|ZZ?|.)/g,
        localFormattingTokens = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g,
        parseTokenOneOrTwoDigits = /\d\d?/,
        parseTokenOneToThreeDigits = /\d{1,3}/,
        parseTokenOneToFourDigits = /\d{1,4}/,
        parseTokenOneToSixDigits = /[+\-]?\d{1,6}/,
        parseTokenDigits = /\d+/,
        parseTokenWord = /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i,
        parseTokenTimezone = /Z|[\+\-]\d\d:?\d\d/gi,
        parseTokenT = /T/i,
        parseTokenOffsetMs = /[\+\-]?\d+/,
        parseTokenTimestampMs = /[\+\-]?\d+(\.\d{1,3})?/,
        parseTokenOneDigit = /\d/,
        parseTokenTwoDigits = /\d\d/,
        parseTokenThreeDigits = /\d{3}/,
        parseTokenFourDigits = /\d{4}/,
        parseTokenSixDigits = /[+-]?\d{6}/,
        parseTokenSignedNumber = /[+-]?\d+/,
        isoRegex = /^\s*(?:[+-]\d{6}|\d{4})-(?:(\d\d-\d\d)|(W\d\d$)|(W\d\d-\d)|(\d\d\d))((T| )(\d\d(:\d\d(:\d\d(\.\d+)?)?)?)?([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/,
        isoFormat = 'YYYY-MM-DDTHH:mm:ssZ',
        isoDates = [
            ['YYYYYY-MM-DD', /[+-]\d{6}-\d{2}-\d{2}/],
            ['YYYY-MM-DD', /\d{4}-\d{2}-\d{2}/],
            ['GGGG-[W]WW-E', /\d{4}-W\d{2}-\d/],
            ['GGGG-[W]WW', /\d{4}-W\d{2}/],
            ['YYYY-DDD', /\d{4}-\d{3}/]
        ],
        isoTimes = [
            ['HH:mm:ss.SSSS', /(T| )\d\d:\d\d:\d\d\.\d+/],
            ['HH:mm:ss', /(T| )\d\d:\d\d:\d\d/],
            ['HH:mm', /(T| )\d\d:\d\d/],
            ['HH', /(T| )\d\d/]
        ],
        parseTimezoneChunker = /([\+\-]|\d\d)/gi,
        proxyGettersAndSetters = 'Date|Hours|Minutes|Seconds|Milliseconds'.split('|'),
        unitMillisecondFactors = {
            'Milliseconds': 1,
            'Seconds': 1e3,
            'Minutes': 6e4,
            'Hours': 36e5,
            'Days': 864e5,
            'Months': 2592e6,
            'Years': 31536e6
        },
        unitAliases = {
            ms: 'millisecond',
            s: 'second',
            m: 'minute',
            h: 'hour',
            d: 'day',
            D: 'date',
            w: 'week',
            W: 'isoWeek',
            M: 'month',
            Q: 'quarter',
            y: 'year',
            DDD: 'dayOfYear',
            e: 'weekday',
            E: 'isoWeekday',
            gg: 'weekYear',
            GG: 'isoWeekYear'
        },
        camelFunctions = {
            dayofyear: 'dayOfYear',
            isoweekday: 'isoWeekday',
            isoweek: 'isoWeek',
            weekyear: 'weekYear',
            isoweekyear: 'isoWeekYear'
        },
        formatFunctions = {},
        relativeTimeThresholds = {
            s: 45,
            m: 45,
            h: 22,
            d: 26,
            M: 11
        },
        ordinalizeTokens = 'DDD w W M D d'.split(' '),
        paddedTokens = 'M D H h m s w W'.split(' '),
        formatTokenFunctions = {
            M: function() {
                return this.month() + 1;
            },
            MMM: function(format) {
                return this.localeData().monthsShort(this, format);
            },
            MMMM: function(format) {
                return this.localeData().months(this, format);
            },
            D: function() {
                return this.date();
            },
            DDD: function() {
                return this.dayOfYear();
            },
            d: function() {
                return this.day();
            },
            dd: function(format) {
                return this.localeData().weekdaysMin(this, format);
            },
            ddd: function(format) {
                return this.localeData().weekdaysShort(this, format);
            },
            dddd: function(format) {
                return this.localeData().weekdays(this, format);
            },
            w: function() {
                return this.week();
            },
            W: function() {
                return this.isoWeek();
            },
            YY: function() {
                return leftZeroFill(this.year() % 100, 2);
            },
            YYYY: function() {
                return leftZeroFill(this.year(), 4);
            },
            YYYYY: function() {
                return leftZeroFill(this.year(), 5);
            },
            YYYYYY: function() {
                var y = this.year(),
                    sign = y >= 0 ? '+' : '-';
                return sign + leftZeroFill(Math.abs(y), 6);
            },
            gg: function() {
                return leftZeroFill(this.weekYear() % 100, 2);
            },
            gggg: function() {
                return leftZeroFill(this.weekYear(), 4);
            },
            ggggg: function() {
                return leftZeroFill(this.weekYear(), 5);
            },
            GG: function() {
                return leftZeroFill(this.isoWeekYear() % 100, 2);
            },
            GGGG: function() {
                return leftZeroFill(this.isoWeekYear(), 4);
            },
            GGGGG: function() {
                return leftZeroFill(this.isoWeekYear(), 5);
            },
            e: function() {
                return this.weekday();
            },
            E: function() {
                return this.isoWeekday();
            },
            a: function() {
                return this.localeData().meridiem(this.hours(), this.minutes(), true);
            },
            A: function() {
                return this.localeData().meridiem(this.hours(), this.minutes(), false);
            },
            H: function() {
                return this.hours();
            },
            h: function() {
                return this.hours() % 12 || 12;
            },
            m: function() {
                return this.minutes();
            },
            s: function() {
                return this.seconds();
            },
            S: function() {
                return toInt(this.milliseconds() / 100);
            },
            SS: function() {
                return leftZeroFill(toInt(this.milliseconds() / 10), 2);
            },
            SSS: function() {
                return leftZeroFill(this.milliseconds(), 3);
            },
            SSSS: function() {
                return leftZeroFill(this.milliseconds(), 3);
            },
            Z: function() {
                var a = this.utcOffset(),
                    b = '+';
                if (a < 0) {
                    a = -a;
                    b = '-';
                }
                return b + leftZeroFill(toInt(a / 60), 2) + ':' + leftZeroFill(toInt(a) % 60, 2);
            },
            ZZ: function() {
                var a = this.utcOffset(),
                    b = '+';
                if (a < 0) {
                    a = -a;
                    b = '-';
                }
                return b + leftZeroFill(toInt(a / 60), 2) + leftZeroFill(toInt(a) % 60, 2);
            },
            z: function() {
                return this.zoneAbbr();
            },
            zz: function() {
                return this.zoneName();
            },
            x: function() {
                return this.valueOf();
            },
            X: function() {
                return this.unix();
            },
            Q: function() {
                return this.quarter();
            }
        },
        deprecations = {},
        lists = ['months', 'monthsShort', 'weekdays', 'weekdaysShort', 'weekdaysMin'],
        updateInProgress = false;

    function dfl(a, b, c) {
        switch (arguments.length) {
            case 2:
                return a != null ? a : b;
            case 3:
                return a != null ? a : b != null ? b : c;
            default:
                throw new Error('Implement me');
        }
    }

    function hasOwnProp(a, b) {
        return hasOwnProperty.call(a, b);
    }

    function defaultParsingFlags() {
        return {
            empty: false,
            unusedTokens: [],
            unusedInput: [],
            overflow: -2,
            charsLeftOver: 0,
            nullInput: false,
            invalidMonth: null,
            invalidFormat: false,
            userInvalidated: false,
            iso: false
        };
    }

    function printMsg(msg) {
        if (moment.suppressDeprecationWarnings === false && typeof console !== 'undefined' && console.warn) {
            console.warn('Deprecation warning: ' + msg);
        }
    }

    function deprecate(msg, fn) {
        var firstTime = true;
        return extend(function() {
            if (firstTime) {
                printMsg(msg);
                firstTime = false;
            }
            return fn.apply(this, arguments);
        }, fn);
    }

    function deprecateSimple(name, msg) {
        if (!deprecations[name]) {
            printMsg(msg);
            deprecations[name] = true;
        }
    }

    function padToken(func, count) {
        return function(a) {
            return leftZeroFill(func.call(this, a), count);
        };
    }

    function ordinalizeToken(func, period) {
        return function(a) {
            return this.localeData().ordinal(func.call(this, a), period);
        };
    }

    function monthDiff(a, b) {
        var wholeMonthDiff = ((b.year() - a.year()) * 12) + (b.month() - a.month()),
            anchor = a.clone().add(wholeMonthDiff, 'months'),
            anchor2, adjust;
        if (b - anchor < 0) {
            anchor2 = a.clone().add(wholeMonthDiff - 1, 'months');
            adjust = (b - anchor) / (anchor - anchor2);
        } else {
            anchor2 = a.clone().add(wholeMonthDiff + 1, 'months');
            adjust = (b - anchor) / (anchor2 - anchor);
        }
        return -(wholeMonthDiff + adjust);
    }
    while (ordinalizeTokens.length) {
        i = ordinalizeTokens.pop();
        formatTokenFunctions[i + 'o'] = ordinalizeToken(formatTokenFunctions[i], i);
    }
    while (paddedTokens.length) {
        i = paddedTokens.pop();
        formatTokenFunctions[i + i] = padToken(formatTokenFunctions[i], 2);
    }
    formatTokenFunctions.DDDD = padToken(formatTokenFunctions.DDD, 3);

    function meridiemFixWrap(locale, hour, meridiem) {
        var isPm;
        if (meridiem == null) {
            return hour;
        }
        if (locale.meridiemHour != null) {
            return locale.meridiemHour(hour, meridiem);
        } else if (locale.isPM != null) {
            isPm = locale.isPM(meridiem);
            if (isPm && hour < 12) {
                hour += 12;
            }
            if (!isPm && hour === 12) {
                hour = 0;
            }
            return hour;
        } else {
            return hour;
        }
    }

    function Locale() {}

    function Moment(config, skipOverflow) {
        if (skipOverflow !== false) {
            checkOverflow(config);
        }
        copyConfig(this, config);
        this._d = new Date(+config._d);
        if (updateInProgress === false) {
            updateInProgress = true;
            moment.updateOffset(this);
            updateInProgress = false;
        }
    }

    function Duration(duration) {
        var normalizedInput = normalizeObjectUnits(duration),
            years = normalizedInput.year || 0,
            quarters = normalizedInput.quarter || 0,
            months = normalizedInput.month || 0,
            weeks = normalizedInput.week || 0,
            days = normalizedInput.day || 0,
            hours = normalizedInput.hour || 0,
            minutes = normalizedInput.minute || 0,
            seconds = normalizedInput.second || 0,
            milliseconds = normalizedInput.millisecond || 0;
        this._milliseconds = +milliseconds +
            seconds * 1e3 +
            minutes * 6e4 +
            hours * 36e5;
        this._days = +days +
            weeks * 7;
        this._months = +months +
            quarters * 3 +
            years * 12;
        this._data = {};
        this._locale = moment.localeData();
        this._bubble();
    }

    function extend(a, b) {
        for (var i in b) {
            if (hasOwnProp(b, i)) {
                a[i] = b[i];
            }
        }
        if (hasOwnProp(b, 'toString')) {
            a.toString = b.toString;
        }
        if (hasOwnProp(b, 'valueOf')) {
            a.valueOf = b.valueOf;
        }
        return a;
    }

    function copyConfig(to, from) {
        var i, prop, val;
        if (typeof from._isAMomentObject !== 'undefined') {
            to._isAMomentObject = from._isAMomentObject;
        }
        if (typeof from._i !== 'undefined') {
            to._i = from._i;
        }
        if (typeof from._f !== 'undefined') {
            to._f = from._f;
        }
        if (typeof from._l !== 'undefined') {
            to._l = from._l;
        }
        if (typeof from._strict !== 'undefined') {
            to._strict = from._strict;
        }
        if (typeof from._tzm !== 'undefined') {
            to._tzm = from._tzm;
        }
        if (typeof from._isUTC !== 'undefined') {
            to._isUTC = from._isUTC;
        }
        if (typeof from._offset !== 'undefined') {
            to._offset = from._offset;
        }
        if (typeof from._pf !== 'undefined') {
            to._pf = from._pf;
        }
        if (typeof from._locale !== 'undefined') {
            to._locale = from._locale;
        }
        if (momentProperties.length > 0) {
            for (i in momentProperties) {
                prop = momentProperties[i];
                val = from[prop];
                if (typeof val !== 'undefined') {
                    to[prop] = val;
                }
            }
        }
        return to;
    }

    function absRound(number) {
        if (number < 0) {
            return Math.ceil(number);
        } else {
            return Math.floor(number);
        }
    }

    function leftZeroFill(number, targetLength, forceSign) {
        var output = '' + Math.abs(number),
            sign = number >= 0;
        while (output.length < targetLength) {
            output = '0' + output;
        }
        return (sign ? (forceSign ? '+' : '') : '-') + output;
    }

    function positiveMomentsDifference(base, other) {
        var res = {
            milliseconds: 0,
            months: 0
        };
        res.months = other.month() - base.month() +
            (other.year() - base.year()) * 12;
        if (base.clone().add(res.months, 'M').isAfter(other)) {
            --res.months;
        }
        res.milliseconds = +other - +(base.clone().add(res.months, 'M'));
        return res;
    }

    function momentsDifference(base, other) {
        var res;
        other = makeAs(other, base);
        if (base.isBefore(other)) {
            res = positiveMomentsDifference(base, other);
        } else {
            res = positiveMomentsDifference(other, base);
            res.milliseconds = -res.milliseconds;
            res.months = -res.months;
        }
        return res;
    }

    function createAdder(direction, name) {
        return function(val, period) {
            var dur, tmp;
            if (period !== null && !isNaN(+period)) {
                deprecateSimple(name, 'moment().' + name + '(period, number) is deprecated. Please use moment().' + name + '(number, period).');
                tmp = val;
                val = period;
                period = tmp;
            }
            val = typeof val === 'string' ? +val : val;
            dur = moment.duration(val, period);
            addOrSubtractDurationFromMoment(this, dur, direction);
            return this;
        };
    }

    function addOrSubtractDurationFromMoment(mom, duration, isAdding, updateOffset) {
        var milliseconds = duration._milliseconds,
            days = duration._days,
            months = duration._months;
        updateOffset = updateOffset == null ? true : updateOffset;
        if (milliseconds) {
            mom._d.setTime(+mom._d + milliseconds * isAdding);
        }
        if (days) {
            rawSetter(mom, 'Date', rawGetter(mom, 'Date') + days * isAdding);
        }
        if (months) {
            rawMonthSetter(mom, rawGetter(mom, 'Month') + months * isAdding);
        }
        if (updateOffset) {
            moment.updateOffset(mom, days || months);
        }
    }

    function isArray(input) {
        return Object.prototype.toString.call(input) === '[object Array]';
    }

    function isDate(input) {
        return Object.prototype.toString.call(input) === '[object Date]' || input instanceof Date;
    }

    function compareArrays(array1, array2, dontConvert) {
        var len = Math.min(array1.length, array2.length),
            lengthDiff = Math.abs(array1.length - array2.length),
            diffs = 0,
            i;
        for (i = 0; i < len; i++) {
            if ((dontConvert && array1[i] !== array2[i]) || (!dontConvert && toInt(array1[i]) !== toInt(array2[i]))) {
                diffs++;
            }
        }
        return diffs + lengthDiff;
    }

    function normalizeUnits(units) {
        if (units) {
            var lowered = units.toLowerCase().replace(/(.)s$/, '$1');
            units = unitAliases[units] || camelFunctions[lowered] || lowered;
        }
        return units;
    }

    function normalizeObjectUnits(inputObject) {
        var normalizedInput = {},
            normalizedProp, prop;
        for (prop in inputObject) {
            if (hasOwnProp(inputObject, prop)) {
                normalizedProp = normalizeUnits(prop);
                if (normalizedProp) {
                    normalizedInput[normalizedProp] = inputObject[prop];
                }
            }
        }
        return normalizedInput;
    }

    function makeList(field) {
        var count, setter;
        if (field.indexOf('week') === 0) {
            count = 7;
            setter = 'day';
        } else if (field.indexOf('month') === 0) {
            count = 12;
            setter = 'month';
        } else {
            return;
        }
        moment[field] = function(format, index) {
            var i, getter, method = moment._locale[field],
                results = [];
            if (typeof format === 'number') {
                index = format;
                format = undefined;
            }
            getter = function(i) {
                var m = moment().utc().set(setter, i);
                return method.call(moment._locale, m, format || '');
            };
            if (index != null) {
                return getter(index);
            } else {
                for (i = 0; i < count; i++) {
                    results.push(getter(i));
                }
                return results;
            }
        };
    }

    function toInt(argumentForCoercion) {
        var coercedNumber = +argumentForCoercion,
            value = 0;
        if (coercedNumber !== 0 && isFinite(coercedNumber)) {
            if (coercedNumber >= 0) {
                value = Math.floor(coercedNumber);
            } else {
                value = Math.ceil(coercedNumber);
            }
        }
        return value;
    }

    function daysInMonth(year, month) {
        return new Date(Date.UTC(year, month + 1, 0)).getUTCDate();
    }

    function weeksInYear(year, dow, doy) {
        return weekOfYear(moment([year, 11, 31 + dow - doy]), dow, doy).week;
    }

    function daysInYear(year) {
        return isLeapYear(year) ? 366 : 365;
    }

    function isLeapYear(year) {
        return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
    }

    function checkOverflow(m) {
        var overflow;
        if (m._a && m._pf.overflow === -2) {
            overflow = m._a[MONTH] < 0 || m._a[MONTH] > 11 ? MONTH : m._a[DATE] < 1 || m._a[DATE] > daysInMonth(m._a[YEAR], m._a[MONTH]) ? DATE : m._a[HOUR] < 0 || m._a[HOUR] > 24 || (m._a[HOUR] === 24 && (m._a[MINUTE] !== 0 || m._a[SECOND] !== 0 || m._a[MILLISECOND] !== 0)) ? HOUR : m._a[MINUTE] < 0 || m._a[MINUTE] > 59 ? MINUTE : m._a[SECOND] < 0 || m._a[SECOND] > 59 ? SECOND : m._a[MILLISECOND] < 0 || m._a[MILLISECOND] > 999 ? MILLISECOND : -1;
            if (m._pf._overflowDayOfYear && (overflow < YEAR || overflow > DATE)) {
                overflow = DATE;
            }
            m._pf.overflow = overflow;
        }
    }

    function isValid(m) {
        if (m._isValid == null) {
            m._isValid = !isNaN(m._d.getTime()) && m._pf.overflow < 0 && !m._pf.empty && !m._pf.invalidMonth && !m._pf.nullInput && !m._pf.invalidFormat && !m._pf.userInvalidated;
            if (m._strict) {
                m._isValid = m._isValid && m._pf.charsLeftOver === 0 && m._pf.unusedTokens.length === 0 && m._pf.bigHour === undefined;
            }
        }
        return m._isValid;
    }

    function normalizeLocale(key) {
        return key ? key.toLowerCase().replace('_', '-') : key;
    }

    function chooseLocale(names) {
        var i = 0,
            j, next, locale, split;
        while (i < names.length) {
            split = normalizeLocale(names[i]).split('-');
            j = split.length;
            next = normalizeLocale(names[i + 1]);
            next = next ? next.split('-') : null;
            while (j > 0) {
                locale = loadLocale(split.slice(0, j).join('-'));
                if (locale) {
                    return locale;
                }
                if (next && next.length >= j && compareArrays(split, next, true) >= j - 1) {
                    break;
                }
                j--;
            }
            i++;
        }
        return null;
    }

    function loadLocale(name) {
        var oldLocale = null;
        if (!locales[name] && hasModule) {
            try {
                oldLocale = moment.locale();
                require('./locale/' + name);
                moment.locale(oldLocale);
            } catch (e) {}
        }
        return locales[name];
    }

    function makeAs(input, model) {
        var res, diff;
        if (model._isUTC) {
            res = model.clone();
            diff = (moment.isMoment(input) || isDate(input) ? +input : +moment(input)) - (+res);
            res._d.setTime(+res._d + diff);
            moment.updateOffset(res, false);
            return res;
        } else {
            return moment(input).local();
        }
    }
    extend(Locale.prototype, {
        set: function(config) {
            var prop, i;
            for (i in config) {
                prop = config[i];
                if (typeof prop === 'function') {
                    this[i] = prop;
                } else {
                    this['_' + i] = prop;
                }
            }
            this._ordinalParseLenient = new RegExp(this._ordinalParse.source + '|' + /\d{1,2}/.source);
        },
        _months: 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_'),
        months: function(m) {
            return this._months[m.month()];
        },
        _monthsShort: 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_'),
        monthsShort: function(m) {
            return this._monthsShort[m.month()];
        },
        monthsParse: function(monthName, format, strict) {
            var i, mom, regex;
            if (!this._monthsParse) {
                this._monthsParse = [];
                this._longMonthsParse = [];
                this._shortMonthsParse = [];
            }
            for (i = 0; i < 12; i++) {
                mom = moment.utc([2000, i]);
                if (strict && !this._longMonthsParse[i]) {
                    this._longMonthsParse[i] = new RegExp('^' + this.months(mom, '').replace('.', '') + '$', 'i');
                    this._shortMonthsParse[i] = new RegExp('^' + this.monthsShort(mom, '').replace('.', '') + '$', 'i');
                }
                if (!strict && !this._monthsParse[i]) {
                    regex = '^' + this.months(mom, '') + '|^' + this.monthsShort(mom, '');
                    this._monthsParse[i] = new RegExp(regex.replace('.', ''), 'i');
                }
                if (strict && format === 'MMMM' && this._longMonthsParse[i].test(monthName)) {
                    return i;
                } else if (strict && format === 'MMM' && this._shortMonthsParse[i].test(monthName)) {
                    return i;
                } else if (!strict && this._monthsParse[i].test(monthName)) {
                    return i;
                }
            }
        },
        _weekdays: 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_'),
        weekdays: function(m) {
            return this._weekdays[m.day()];
        },
        _weekdaysShort: 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_'),
        weekdaysShort: function(m) {
            return this._weekdaysShort[m.day()];
        },
        _weekdaysMin: 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_'),
        weekdaysMin: function(m) {
            return this._weekdaysMin[m.day()];
        },
        weekdaysParse: function(weekdayName) {
            var i, mom, regex;
            if (!this._weekdaysParse) {
                this._weekdaysParse = [];
            }
            for (i = 0; i < 7; i++) {
                if (!this._weekdaysParse[i]) {
                    mom = moment([2000, 1]).day(i);
                    regex = '^' + this.weekdays(mom, '') + '|^' + this.weekdaysShort(mom, '') + '|^' + this.weekdaysMin(mom, '');
                    this._weekdaysParse[i] = new RegExp(regex.replace('.', ''), 'i');
                }
                if (this._weekdaysParse[i].test(weekdayName)) {
                    return i;
                }
            }
        },
        _longDateFormat: {
            LTS: 'h:mm:ss A',
            LT: 'h:mm A',
            L: 'MM/DD/YYYY',
            LL: 'MMMM D, YYYY',
            LLL: 'MMMM D, YYYY LT',
            LLLL: 'dddd, MMMM D, YYYY LT'
        },
        longDateFormat: function(key) {
            var output = this._longDateFormat[key];
            if (!output && this._longDateFormat[key.toUpperCase()]) {
                output = this._longDateFormat[key.toUpperCase()].replace(/MMMM|MM|DD|dddd/g, function(val) {
                    return val.slice(1);
                });
                this._longDateFormat[key] = output;
            }
            return output;
        },
        isPM: function(input) {
            return ((input + '').toLowerCase().charAt(0) === 'p');
        },
        _meridiemParse: /[ap]\.?m?\.?/i,
        meridiem: function(hours, minutes, isLower) {
            if (hours > 11) {
                return isLower ? 'pm' : 'PM';
            } else {
                return isLower ? 'am' : 'AM';
            }
        },
        _calendar: {
            sameDay: '[Today at] LT',
            nextDay: '[Tomorrow at] LT',
            nextWeek: 'dddd [at] LT',
            lastDay: '[Yesterday at] LT',
            lastWeek: '[Last] dddd [at] LT',
            sameElse: 'L'
        },
        calendar: function(key, mom, now) {
            var output = this._calendar[key];
            return typeof output === 'function' ? output.apply(mom, [now]) : output;
        },
        _relativeTime: {
            future: 'in %s',
            past: '%s ago',
            s: 'a few seconds',
            m: 'a minute',
            mm: '%d minutes',
            h: 'an hour',
            hh: '%d hours',
            d: 'a day',
            dd: '%d days',
            M: 'a month',
            MM: '%d months',
            y: 'a year',
            yy: '%d years'
        },
        relativeTime: function(number, withoutSuffix, string, isFuture) {
            var output = this._relativeTime[string];
            return (typeof output === 'function') ? output(number, withoutSuffix, string, isFuture) : output.replace(/%d/i, number);
        },
        pastFuture: function(diff, output) {
            var format = this._relativeTime[diff > 0 ? 'future' : 'past'];
            return typeof format === 'function' ? format(output) : format.replace(/%s/i, output);
        },
        ordinal: function(number) {
            return this._ordinal.replace('%d', number);
        },
        _ordinal: '%d',
        _ordinalParse: /\d{1,2}/,
        preparse: function(string) {
            return string;
        },
        postformat: function(string) {
            return string;
        },
        week: function(mom) {
            return weekOfYear(mom, this._week.dow, this._week.doy).week;
        },
        _week: {
            dow: 0,
            doy: 6
        },
        firstDayOfWeek: function() {
            return this._week.dow;
        },
        firstDayOfYear: function() {
            return this._week.doy;
        },
        _invalidDate: 'Invalid date',
        invalidDate: function() {
            return this._invalidDate;
        }
    });

    function removeFormattingTokens(input) {
        if (input.match(/\[[\s\S]/)) {
            return input.replace(/^\[|\]$/g, '');
        }
        return input.replace(/\\/g, '');
    }

    function makeFormatFunction(format) {
        var array = format.match(formattingTokens),
            i, length;
        for (i = 0, length = array.length; i < length; i++) {
            if (formatTokenFunctions[array[i]]) {
                array[i] = formatTokenFunctions[array[i]];
            } else {
                array[i] = removeFormattingTokens(array[i]);
            }
        }
        return function(mom) {
            var output = '';
            for (i = 0; i < length; i++) {
                output += array[i] instanceof Function ? array[i].call(mom, format) : array[i];
            }
            return output;
        };
    }

    function formatMoment(m, format) {
        if (!m.isValid()) {
            return m.localeData().invalidDate();
        }
        format = expandFormat(format, m.localeData());
        if (!formatFunctions[format]) {
            formatFunctions[format] = makeFormatFunction(format);
        }
        return formatFunctions[format](m);
    }

    function expandFormat(format, locale) {
        var i = 5;

        function replaceLongDateFormatTokens(input) {
            return locale.longDateFormat(input) || input;
        }
        localFormattingTokens.lastIndex = 0;
        while (i >= 0 && localFormattingTokens.test(format)) {
            format = format.replace(localFormattingTokens, replaceLongDateFormatTokens);
            localFormattingTokens.lastIndex = 0;
            i -= 1;
        }
        return format;
    }

    function getParseRegexForToken(token, config) {
        var a, strict = config._strict;
        switch (token) {
            case 'Q':
                return parseTokenOneDigit;
            case 'DDDD':
                return parseTokenThreeDigits;
            case 'YYYY':
            case 'GGGG':
            case 'gggg':
                return strict ? parseTokenFourDigits : parseTokenOneToFourDigits;
            case 'Y':
            case 'G':
            case 'g':
                return parseTokenSignedNumber;
            case 'YYYYYY':
            case 'YYYYY':
            case 'GGGGG':
            case 'ggggg':
                return strict ? parseTokenSixDigits : parseTokenOneToSixDigits;
            case 'S':
                if (strict) {
                    return parseTokenOneDigit;
                }
            case 'SS':
                if (strict) {
                    return parseTokenTwoDigits;
                }
            case 'SSS':
                if (strict) {
                    return parseTokenThreeDigits;
                }
            case 'DDD':
                return parseTokenOneToThreeDigits;
            case 'MMM':
            case 'MMMM':
            case 'dd':
            case 'ddd':
            case 'dddd':
                return parseTokenWord;
            case 'a':
            case 'A':
                return config._locale._meridiemParse;
            case 'x':
                return parseTokenOffsetMs;
            case 'X':
                return parseTokenTimestampMs;
            case 'Z':
            case 'ZZ':
                return parseTokenTimezone;
            case 'T':
                return parseTokenT;
            case 'SSSS':
                return parseTokenDigits;
            case 'MM':
            case 'DD':
            case 'YY':
            case 'GG':
            case 'gg':
            case 'HH':
            case 'hh':
            case 'mm':
            case 'ss':
            case 'ww':
            case 'WW':
                return strict ? parseTokenTwoDigits : parseTokenOneOrTwoDigits;
            case 'M':
            case 'D':
            case 'd':
            case 'H':
            case 'h':
            case 'm':
            case 's':
            case 'w':
            case 'W':
            case 'e':
            case 'E':
                return parseTokenOneOrTwoDigits;
            case 'Do':
                return strict ? config._locale._ordinalParse : config._locale._ordinalParseLenient;
            default:
                a = new RegExp(regexpEscape(unescapeFormat(token.replace('\\', '')), 'i'));
                return a;
        }
    }

    function utcOffsetFromString(string) {
        string = string || '';
        var possibleTzMatches = (string.match(parseTokenTimezone) || []),
            tzChunk = possibleTzMatches[possibleTzMatches.length - 1] || [],
            parts = (tzChunk + '').match(parseTimezoneChunker) || ['-', 0, 0],
            minutes = +(parts[1] * 60) + toInt(parts[2]);
        return parts[0] === '+' ? minutes : -minutes;
    }

    function addTimeToArrayFromToken(token, input, config) {
        var a, datePartArray = config._a;
        switch (token) {
            case 'Q':
                if (input != null) {
                    datePartArray[MONTH] = (toInt(input) - 1) * 3;
                }
                break;
            case 'M':
            case 'MM':
                if (input != null) {
                    datePartArray[MONTH] = toInt(input) - 1;
                }
                break;
            case 'MMM':
            case 'MMMM':
                a = config._locale.monthsParse(input, token, config._strict);
                if (a != null) {
                    datePartArray[MONTH] = a;
                } else {
                    config._pf.invalidMonth = input;
                }
                break;
            case 'D':
            case 'DD':
                if (input != null) {
                    datePartArray[DATE] = toInt(input);
                }
                break;
            case 'Do':
                if (input != null) {
                    datePartArray[DATE] = toInt(parseInt(input.match(/\d{1,2}/)[0], 10));
                }
                break;
            case 'DDD':
            case 'DDDD':
                if (input != null) {
                    config._dayOfYear = toInt(input);
                }
                break;
            case 'YY':
                datePartArray[YEAR] = moment.parseTwoDigitYear(input);
                break;
            case 'YYYY':
            case 'YYYYY':
            case 'YYYYYY':
                datePartArray[YEAR] = toInt(input);
                break;
            case 'a':
            case 'A':
                config._meridiem = input;
                break;
            case 'h':
            case 'hh':
                config._pf.bigHour = true;
            case 'H':
            case 'HH':
                datePartArray[HOUR] = toInt(input);
                break;
            case 'm':
            case 'mm':
                datePartArray[MINUTE] = toInt(input);
                break;
            case 's':
            case 'ss':
                datePartArray[SECOND] = toInt(input);
                break;
            case 'S':
            case 'SS':
            case 'SSS':
            case 'SSSS':
                datePartArray[MILLISECOND] = toInt(('0.' + input) * 1000);
                break;
            case 'x':
                config._d = new Date(toInt(input));
                break;
            case 'X':
                config._d = new Date(parseFloat(input) * 1000);
                break;
            case 'Z':
            case 'ZZ':
                config._useUTC = true;
                config._tzm = utcOffsetFromString(input);
                break;
            case 'dd':
            case 'ddd':
            case 'dddd':
                a = config._locale.weekdaysParse(input);
                if (a != null) {
                    config._w = config._w || {};
                    config._w['d'] = a;
                } else {
                    config._pf.invalidWeekday = input;
                }
                break;
            case 'w':
            case 'ww':
            case 'W':
            case 'WW':
            case 'd':
            case 'e':
            case 'E':
                token = token.substr(0, 1);
            case 'gggg':
            case 'GGGG':
            case 'GGGGG':
                token = token.substr(0, 2);
                if (input) {
                    config._w = config._w || {};
                    config._w[token] = toInt(input);
                }
                break;
            case 'gg':
            case 'GG':
                config._w = config._w || {};
                config._w[token] = moment.parseTwoDigitYear(input);
        }
    }

    function dayOfYearFromWeekInfo(config) {
        var w, weekYear, week, weekday, dow, doy, temp;
        w = config._w;
        if (w.GG != null || w.W != null || w.E != null) {
            dow = 1;
            doy = 4;
            weekYear = dfl(w.GG, config._a[YEAR], weekOfYear(moment(), 1, 4).year);
            week = dfl(w.W, 1);
            weekday = dfl(w.E, 1);
        } else {
            dow = config._locale._week.dow;
            doy = config._locale._week.doy;
            weekYear = dfl(w.gg, config._a[YEAR], weekOfYear(moment(), dow, doy).year);
            week = dfl(w.w, 1);
            if (w.d != null) {
                weekday = w.d;
                if (weekday < dow) {
                    ++week;
                }
            } else if (w.e != null) {
                weekday = w.e + dow;
            } else {
                weekday = dow;
            }
        }
        temp = dayOfYearFromWeeks(weekYear, week, weekday, doy, dow);
        config._a[YEAR] = temp.year;
        config._dayOfYear = temp.dayOfYear;
    }

    function dateFromConfig(config) {
        var i, date, input = [],
            currentDate, yearToUse;
        if (config._d) {
            return;
        }
        currentDate = currentDateArray(config);
        if (config._w && config._a[DATE] == null && config._a[MONTH] == null) {
            dayOfYearFromWeekInfo(config);
        }
        if (config._dayOfYear) {
            yearToUse = dfl(config._a[YEAR], currentDate[YEAR]);
            if (config._dayOfYear > daysInYear(yearToUse)) {
                config._pf._overflowDayOfYear = true;
            }
            date = makeUTCDate(yearToUse, 0, config._dayOfYear);
            config._a[MONTH] = date.getUTCMonth();
            config._a[DATE] = date.getUTCDate();
        }
        for (i = 0; i < 3 && config._a[i] == null; ++i) {
            config._a[i] = input[i] = currentDate[i];
        }
        for (; i < 7; i++) {
            config._a[i] = input[i] = (config._a[i] == null) ? (i === 2 ? 1 : 0) : config._a[i];
        }
        if (config._a[HOUR] === 24 && config._a[MINUTE] === 0 && config._a[SECOND] === 0 && config._a[MILLISECOND] === 0) {
            config._nextDay = true;
            config._a[HOUR] = 0;
        }
        config._d = (config._useUTC ? makeUTCDate : makeDate).apply(null, input);
        if (config._tzm != null) {
            config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);
        }
        if (config._nextDay) {
            config._a[HOUR] = 24;
        }
    }

    function dateFromObject(config) {
        var normalizedInput;
        if (config._d) {
            return;
        }
        normalizedInput = normalizeObjectUnits(config._i);
        config._a = [normalizedInput.year, normalizedInput.month, normalizedInput.day || normalizedInput.date, normalizedInput.hour, normalizedInput.minute, normalizedInput.second, normalizedInput.millisecond];
        dateFromConfig(config);
    }

    function currentDateArray(config) {
        var now = new Date();
        if (config._useUTC) {
            return [now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate()];
        } else {
            return [now.getFullYear(), now.getMonth(), now.getDate()];
        }
    }

    function makeDateFromStringAndFormat(config) {
        if (config._f === moment.ISO_8601) {
            parseISO(config);
            return;
        }
        config._a = [];
        config._pf.empty = true;
        var string = '' + config._i,
            i, parsedInput, tokens, token, skipped, stringLength = string.length,
            totalParsedInputLength = 0;
        tokens = expandFormat(config._f, config._locale).match(formattingTokens) || [];
        for (i = 0; i < tokens.length; i++) {
            token = tokens[i];
            parsedInput = (string.match(getParseRegexForToken(token, config)) || [])[0];
            if (parsedInput) {
                skipped = string.substr(0, string.indexOf(parsedInput));
                if (skipped.length > 0) {
                    config._pf.unusedInput.push(skipped);
                }
                string = string.slice(string.indexOf(parsedInput) + parsedInput.length);
                totalParsedInputLength += parsedInput.length;
            }
            if (formatTokenFunctions[token]) {
                if (parsedInput) {
                    config._pf.empty = false;
                } else {
                    config._pf.unusedTokens.push(token);
                }
                addTimeToArrayFromToken(token, parsedInput, config);
            } else if (config._strict && !parsedInput) {
                config._pf.unusedTokens.push(token);
            }
        }
        config._pf.charsLeftOver = stringLength - totalParsedInputLength;
        if (string.length > 0) {
            config._pf.unusedInput.push(string);
        }
        if (config._pf.bigHour === true && config._a[HOUR] <= 12) {
            config._pf.bigHour = undefined;
        }
        config._a[HOUR] = meridiemFixWrap(config._locale, config._a[HOUR], config._meridiem);
        dateFromConfig(config);
        checkOverflow(config);
    }

    function unescapeFormat(s) {
        return s.replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function(matched, p1, p2, p3, p4) {
            return p1 || p2 || p3 || p4;
        });
    }

    function regexpEscape(s) {
        return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    }

    function makeDateFromStringAndArray(config) {
        var tempConfig, bestMoment, scoreToBeat, i, currentScore;
        if (config._f.length === 0) {
            config._pf.invalidFormat = true;
            config._d = new Date(NaN);
            return;
        }
        for (i = 0; i < config._f.length; i++) {
            currentScore = 0;
            tempConfig = copyConfig({}, config);
            if (config._useUTC != null) {
                tempConfig._useUTC = config._useUTC;
            }
            tempConfig._pf = defaultParsingFlags();
            tempConfig._f = config._f[i];
            makeDateFromStringAndFormat(tempConfig);
            if (!isValid(tempConfig)) {
                continue;
            }
            currentScore += tempConfig._pf.charsLeftOver;
            currentScore += tempConfig._pf.unusedTokens.length * 10;
            tempConfig._pf.score = currentScore;
            if (scoreToBeat == null || currentScore < scoreToBeat) {
                scoreToBeat = currentScore;
                bestMoment = tempConfig;
            }
        }
        extend(config, bestMoment || tempConfig);
    }

    function parseISO(config) {
        var i, l, string = config._i,
            match = isoRegex.exec(string);
        if (match) {
            config._pf.iso = true;
            for (i = 0, l = isoDates.length; i < l; i++) {
                if (isoDates[i][1].exec(string)) {
                    config._f = isoDates[i][0] + (match[6] || ' ');
                    break;
                }
            }
            for (i = 0, l = isoTimes.length; i < l; i++) {
                if (isoTimes[i][1].exec(string)) {
                    config._f += isoTimes[i][0];
                    break;
                }
            }
            if (string.match(parseTokenTimezone)) {
                config._f += 'Z';
            }
            makeDateFromStringAndFormat(config);
        } else {
            config._isValid = false;
        }
    }

    function makeDateFromString(config) {
        parseISO(config);
        if (config._isValid === false) {
            delete config._isValid;
            moment.createFromInputFallback(config);
        }
    }

    function map(arr, fn) {
        var res = [],
            i;
        for (i = 0; i < arr.length; ++i) {
            res.push(fn(arr[i], i));
        }
        return res;
    }

    function makeDateFromInput(config) {
        var input = config._i,
            matched;
        if (input === undefined) {
            config._d = new Date();
        } else if (isDate(input)) {
            config._d = new Date(+input);
        } else if ((matched = aspNetJsonRegex.exec(input)) !== null) {
            config._d = new Date(+matched[1]);
        } else if (typeof input === 'string') {
            makeDateFromString(config);
        } else if (isArray(input)) {
            config._a = map(input.slice(0), function(obj) {
                return parseInt(obj, 10);
            });
            dateFromConfig(config);
        } else if (typeof(input) === 'object') {
            dateFromObject(config);
        } else if (typeof(input) === 'number') {
            config._d = new Date(input);
        } else {
            moment.createFromInputFallback(config);
        }
    }

    function makeDate(y, m, d, h, M, s, ms) {
        var date = new Date(y, m, d, h, M, s, ms);
        if (y < 1970) {
            date.setFullYear(y);
        }
        return date;
    }

    function makeUTCDate(y) {
        var date = new Date(Date.UTC.apply(null, arguments));
        if (y < 1970) {
            date.setUTCFullYear(y);
        }
        return date;
    }

    function parseWeekday(input, locale) {
        if (typeof input === 'string') {
            if (!isNaN(input)) {
                input = parseInt(input, 10);
            } else {
                input = locale.weekdaysParse(input);
                if (typeof input !== 'number') {
                    return null;
                }
            }
        }
        return input;
    }

    function substituteTimeAgo(string, number, withoutSuffix, isFuture, locale) {
        return locale.relativeTime(number || 1, !!withoutSuffix, string, isFuture);
    }

    function relativeTime(posNegDuration, withoutSuffix, locale) {
        var duration = moment.duration(posNegDuration).abs(),
            seconds = round(duration.as('s')),
            minutes = round(duration.as('m')),
            hours = round(duration.as('h')),
            days = round(duration.as('d')),
            months = round(duration.as('M')),
            years = round(duration.as('y')),
            args = seconds < relativeTimeThresholds.s && ['s', seconds] || minutes === 1 && ['m'] || minutes < relativeTimeThresholds.m && ['mm', minutes] || hours === 1 && ['h'] || hours < relativeTimeThresholds.h && ['hh', hours] || days === 1 && ['d'] || days < relativeTimeThresholds.d && ['dd', days] || months === 1 && ['M'] || months < relativeTimeThresholds.M && ['MM', months] || years === 1 && ['y'] || ['yy', years];
        args[2] = withoutSuffix;
        args[3] = +posNegDuration > 0;
        args[4] = locale;
        return substituteTimeAgo.apply({}, args);
    }

    function weekOfYear(mom, firstDayOfWeek, firstDayOfWeekOfYear) {
        var end = firstDayOfWeekOfYear - firstDayOfWeek,
            daysToDayOfWeek = firstDayOfWeekOfYear - mom.day(),
            adjustedMoment;
        if (daysToDayOfWeek > end) {
            daysToDayOfWeek -= 7;
        }
        if (daysToDayOfWeek < end - 7) {
            daysToDayOfWeek += 7;
        }
        adjustedMoment = moment(mom).add(daysToDayOfWeek, 'd');
        return {
            week: Math.ceil(adjustedMoment.dayOfYear() / 7),
            year: adjustedMoment.year()
        };
    }

    function dayOfYearFromWeeks(year, week, weekday, firstDayOfWeekOfYear, firstDayOfWeek) {
        var d = makeUTCDate(year, 0, 1).getUTCDay(),
            daysToAdd, dayOfYear;
        d = d === 0 ? 7 : d;
        weekday = weekday != null ? weekday : firstDayOfWeek;
        daysToAdd = firstDayOfWeek - d + (d > firstDayOfWeekOfYear ? 7 : 0) - (d < firstDayOfWeek ? 7 : 0);
        dayOfYear = 7 * (week - 1) + (weekday - firstDayOfWeek) + daysToAdd + 1;
        return {
            year: dayOfYear > 0 ? year : year - 1,
            dayOfYear: dayOfYear > 0 ? dayOfYear : daysInYear(year - 1) + dayOfYear
        };
    }

    function makeMoment(config) {
        var input = config._i,
            format = config._f,
            res;
        config._locale = config._locale || moment.localeData(config._l);
        if (input === null || (format === undefined && input === '')) {
            return moment.invalid({
                nullInput: true
            });
        }
        if (typeof input === 'string') {
            config._i = input = config._locale.preparse(input);
        }
        if (moment.isMoment(input)) {
            return new Moment(input, true);
        } else if (format) {
            if (isArray(format)) {
                makeDateFromStringAndArray(config);
            } else {
                makeDateFromStringAndFormat(config);
            }
        } else {
            makeDateFromInput(config);
        }
        res = new Moment(config);
        if (res._nextDay) {
            res.add(1, 'd');
            res._nextDay = undefined;
        }
        return res;
    }
    moment = function(input, format, locale, strict) {
        var c;
        if (typeof(locale) === 'boolean') {
            strict = locale;
            locale = undefined;
        }
        c = {};
        c._isAMomentObject = true;
        c._i = input;
        c._f = format;
        c._l = locale;
        c._strict = strict;
        c._isUTC = false;
        c._pf = defaultParsingFlags();
        return makeMoment(c);
    };
    moment.suppressDeprecationWarnings = false;
    moment.createFromInputFallback = deprecate('moment construction falls back to js Date. This is ' + 'discouraged and will be removed in upcoming major ' + 'release. Please refer to ' + 'https://github.com/moment/moment/issues/1407 for more info.', function(config) {
        config._d = new Date(config._i + (config._useUTC ? ' UTC' : ''));
    });

    function pickBy(fn, moments) {
        var res, i;
        if (moments.length === 1 && isArray(moments[0])) {
            moments = moments[0];
        }
        if (!moments.length) {
            return moment();
        }
        res = moments[0];
        for (i = 1; i < moments.length; ++i) {
            if (moments[i][fn](res)) {
                res = moments[i];
            }
        }
        return res;
    }
    moment.min = function() {
        var args = [].slice.call(arguments, 0);
        return pickBy('isBefore', args);
    };
    moment.max = function() {
        var args = [].slice.call(arguments, 0);
        return pickBy('isAfter', args);
    };
    moment.utc = function(input, format, locale, strict) {
        var c;
        if (typeof(locale) === 'boolean') {
            strict = locale;
            locale = undefined;
        }
        c = {};
        c._isAMomentObject = true;
        c._useUTC = true;
        c._isUTC = true;
        c._l = locale;
        c._i = input;
        c._f = format;
        c._strict = strict;
        c._pf = defaultParsingFlags();
        return makeMoment(c).utc();
    };
    moment.unix = function(input) {
        return moment(input * 1000);
    };
    moment.duration = function(input, key) {
        var duration = input,
            match = null,
            sign, ret, parseIso, diffRes;
        if (moment.isDuration(input)) {
            duration = {
                ms: input._milliseconds,
                d: input._days,
                M: input._months
            };
        } else if (typeof input === 'number') {
            duration = {};
            if (key) {
                duration[key] = input;
            } else {
                duration.milliseconds = input;
            }
        } else if (!!(match = aspNetTimeSpanJsonRegex.exec(input))) {
            sign = (match[1] === '-') ? -1 : 1;
            duration = {
                y: 0,
                d: toInt(match[DATE]) * sign,
                h: toInt(match[HOUR]) * sign,
                m: toInt(match[MINUTE]) * sign,
                s: toInt(match[SECOND]) * sign,
                ms: toInt(match[MILLISECOND]) * sign
            };
        } else if (!!(match = isoDurationRegex.exec(input))) {
            sign = (match[1] === '-') ? -1 : 1;
            parseIso = function(inp) {
                var res = inp && parseFloat(inp.replace(',', '.'));
                return (isNaN(res) ? 0 : res) * sign;
            };
            duration = {
                y: parseIso(match[2]),
                M: parseIso(match[3]),
                d: parseIso(match[4]),
                h: parseIso(match[5]),
                m: parseIso(match[6]),
                s: parseIso(match[7]),
                w: parseIso(match[8])
            };
        } else if (duration == null) {
            duration = {};
        } else if (typeof duration === 'object' && ('from' in duration || 'to' in duration)) {
            diffRes = momentsDifference(moment(duration.from), moment(duration.to));
            duration = {};
            duration.ms = diffRes.milliseconds;
            duration.M = diffRes.months;
        }
        ret = new Duration(duration);
        if (moment.isDuration(input) && hasOwnProp(input, '_locale')) {
            ret._locale = input._locale;
        }
        return ret;
    };
    moment.version = VERSION;
    moment.defaultFormat = isoFormat;
    moment.ISO_8601 = function() {};
    moment.momentProperties = momentProperties;
    moment.updateOffset = function() {};
    moment.relativeTimeThreshold = function(threshold, limit) {
        if (relativeTimeThresholds[threshold] === undefined) {
            return false;
        }
        if (limit === undefined) {
            return relativeTimeThresholds[threshold];
        }
        relativeTimeThresholds[threshold] = limit;
        return true;
    };
    moment.lang = deprecate('moment.lang is deprecated. Use moment.locale instead.', function(key, value) {
        return moment.locale(key, value);
    });
    moment.locale = function(key, values) {
        var data;
        if (key) {
            if (typeof(values) !== 'undefined') {
                data = moment.defineLocale(key, values);
            } else {
                data = moment.localeData(key);
            }
            if (data) {
                moment.duration._locale = moment._locale = data;
            }
        }
        return moment._locale._abbr;
    };
    moment.defineLocale = function(name, values) {
        if (values !== null) {
            values.abbr = name;
            if (!locales[name]) {
                locales[name] = new Locale();
            }
            locales[name].set(values);
            moment.locale(name);
            return locales[name];
        } else {
            delete locales[name];
            return null;
        }
    };
    moment.langData = deprecate('moment.langData is deprecated. Use moment.localeData instead.', function(key) {
        return moment.localeData(key);
    });
    moment.localeData = function(key) {
        var locale;
        if (key && key._locale && key._locale._abbr) {
            key = key._locale._abbr;
        }
        if (!key) {
            return moment._locale;
        }
        if (!isArray(key)) {
            locale = loadLocale(key);
            if (locale) {
                return locale;
            }
            key = [key];
        }
        return chooseLocale(key);
    };
    moment.isMoment = function(obj) {
        return obj instanceof Moment || (obj != null && hasOwnProp(obj, '_isAMomentObject'));
    };
    moment.isDuration = function(obj) {
        return obj instanceof Duration;
    };
    for (i = lists.length - 1; i >= 0; --i) {
        makeList(lists[i]);
    }
    moment.normalizeUnits = function(units) {
        return normalizeUnits(units);
    };
    moment.invalid = function(flags) {
        var m = moment.utc(NaN);
        if (flags != null) {
            extend(m._pf, flags);
        } else {
            m._pf.userInvalidated = true;
        }
        return m;
    };
    moment.parseZone = function() {
        return moment.apply(null, arguments).parseZone();
    };
    moment.parseTwoDigitYear = function(input) {
        return toInt(input) + (toInt(input) > 68 ? 1900 : 2000);
    };
    moment.isDate = isDate;
    extend(moment.fn = Moment.prototype, {
        clone: function() {
            return moment(this);
        },
        valueOf: function() {
            return +this._d - ((this._offset || 0) * 60000);
        },
        unix: function() {
            return Math.floor(+this / 1000);
        },
        toString: function() {
            return this.clone().locale('en').format('ddd MMM DD YYYY HH:mm:ss [GMT]ZZ');
        },
        toDate: function() {
            return this._offset ? new Date(+this) : this._d;
        },
        toISOString: function() {
            var m = moment(this).utc();
            if (0 < m.year() && m.year() <= 9999) {
                if ('function' === typeof Date.prototype.toISOString) {
                    return this.toDate().toISOString();
                } else {
                    return formatMoment(m, 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
                }
            } else {
                return formatMoment(m, 'YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
            }
        },
        toArray: function() {
            var m = this;
            return [m.year(), m.month(), m.date(), m.hours(), m.minutes(), m.seconds(), m.milliseconds()];
        },
        isValid: function() {
            return isValid(this);
        },
        isDSTShifted: function() {
            if (this._a) {
                return this.isValid() && compareArrays(this._a, (this._isUTC ? moment.utc(this._a) : moment(this._a)).toArray()) > 0;
            }
            return false;
        },
        parsingFlags: function() {
            return extend({}, this._pf);
        },
        invalidAt: function() {
            return this._pf.overflow;
        },
        utc: function(keepLocalTime) {
            return this.utcOffset(0, keepLocalTime);
        },
        local: function(keepLocalTime) {
            if (this._isUTC) {
                this.utcOffset(0, keepLocalTime);
                this._isUTC = false;
                if (keepLocalTime) {
                    this.subtract(this._dateUtcOffset(), 'm');
                }
            }
            return this;
        },
        format: function(inputString) {
            var output = formatMoment(this, inputString || moment.defaultFormat);
            return this.localeData().postformat(output);
        },
        add: createAdder(1, 'add'),
        subtract: createAdder(-1, 'subtract'),
        diff: function(input, units, asFloat) {
            var that = makeAs(input, this),
                zoneDiff = (that.utcOffset() - this.utcOffset()) * 6e4,
                anchor, diff, output, daysAdjust;
            units = normalizeUnits(units);
            if (units === 'year' || units === 'month' || units === 'quarter') {
                output = monthDiff(this, that);
                if (units === 'quarter') {
                    output = output / 3;
                } else if (units === 'year') {
                    output = output / 12;
                }
            } else {
                diff = this - that;
                output = units === 'second' ? diff / 1e3 : units === 'minute' ? diff / 6e4 : units === 'hour' ? diff / 36e5 : units === 'day' ? (diff - zoneDiff) / 864e5 : units === 'week' ? (diff - zoneDiff) / 6048e5 : diff;
            }
            return asFloat ? output : absRound(output);
        },
        from: function(time, withoutSuffix) {
            return moment.duration({
                to: this,
                from: time
            }).locale(this.locale()).humanize(!withoutSuffix);
        },
        fromNow: function(withoutSuffix) {
            return this.from(moment(), withoutSuffix);
        },
        calendar: function(time) {
            var now = time || moment(),
                sod = makeAs(now, this).startOf('day'),
                diff = this.diff(sod, 'days', true),
                format = diff < -6 ? 'sameElse' : diff < -1 ? 'lastWeek' : diff < 0 ? 'lastDay' : diff < 1 ? 'sameDay' : diff < 2 ? 'nextDay' : diff < 7 ? 'nextWeek' : 'sameElse';
            return this.format(this.localeData().calendar(format, this, moment(now)));
        },
        isLeapYear: function() {
            return isLeapYear(this.year());
        },
        isDST: function() {
            return (this.utcOffset() > this.clone().month(0).utcOffset() || this.utcOffset() > this.clone().month(5).utcOffset());
        },
        day: function(input) {
            var day = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
            if (input != null) {
                input = parseWeekday(input, this.localeData());
                return this.add(input - day, 'd');
            } else {
                return day;
            }
        },
        month: makeAccessor('Month', true),
        startOf: function(units) {
            units = normalizeUnits(units);
            switch (units) {
                case 'year':
                    this.month(0);
                case 'quarter':
                case 'month':
                    this.date(1);
                case 'week':
                case 'isoWeek':
                case 'day':
                    this.hours(0);
                case 'hour':
                    this.minutes(0);
                case 'minute':
                    this.seconds(0);
                case 'second':
                    this.milliseconds(0);
            }
            if (units === 'week') {
                this.weekday(0);
            } else if (units === 'isoWeek') {
                this.isoWeekday(1);
            }
            if (units === 'quarter') {
                this.month(Math.floor(this.month() / 3) * 3);
            }
            return this;
        },
        endOf: function(units) {
            units = normalizeUnits(units);
            if (units === undefined || units === 'millisecond') {
                return this;
            }
            return this.startOf(units).add(1, (units === 'isoWeek' ? 'week' : units)).subtract(1, 'ms');
        },
        isAfter: function(input, units) {
            var inputMs;
            units = normalizeUnits(typeof units !== 'undefined' ? units : 'millisecond');
            if (units === 'millisecond') {
                input = moment.isMoment(input) ? input : moment(input);
                return +this > +input;
            } else {
                inputMs = moment.isMoment(input) ? +input : +moment(input);
                return inputMs < +this.clone().startOf(units);
            }
        },
        isBefore: function(input, units) {
            var inputMs;
            units = normalizeUnits(typeof units !== 'undefined' ? units : 'millisecond');
            if (units === 'millisecond') {
                input = moment.isMoment(input) ? input : moment(input);
                return +this < +input;
            } else {
                inputMs = moment.isMoment(input) ? +input : +moment(input);
                return +this.clone().endOf(units) < inputMs;
            }
        },
        isBetween: function(from, to, units) {
            return this.isAfter(from, units) && this.isBefore(to, units);
        },
        isSame: function(input, units) {
            var inputMs;
            units = normalizeUnits(units || 'millisecond');
            if (units === 'millisecond') {
                input = moment.isMoment(input) ? input : moment(input);
                return +this === +input;
            } else {
                inputMs = +moment(input);
                return +(this.clone().startOf(units)) <= inputMs && inputMs <= +(this.clone().endOf(units));
            }
        },
        min: deprecate('moment().min is deprecated, use moment.min instead. https://github.com/moment/moment/issues/1548', function(other) {
            other = moment.apply(null, arguments);
            return other < this ? this : other;
        }),
        max: deprecate('moment().max is deprecated, use moment.max instead. https://github.com/moment/moment/issues/1548', function(other) {
            other = moment.apply(null, arguments);
            return other > this ? this : other;
        }),
        zone: deprecate('moment().zone is deprecated, use moment().utcOffset instead. ' + 'https://github.com/moment/moment/issues/1779', function(input, keepLocalTime) {
            if (input != null) {
                if (typeof input !== 'string') {
                    input = -input;
                }
                this.utcOffset(input, keepLocalTime);
                return this;
            } else {
                return -this.utcOffset();
            }
        }),
        utcOffset: function(input, keepLocalTime) {
            var offset = this._offset || 0,
                localAdjust;
            if (input != null) {
                if (typeof input === 'string') {
                    input = utcOffsetFromString(input);
                }
                if (Math.abs(input) < 16) {
                    input = input * 60;
                }
                if (!this._isUTC && keepLocalTime) {
                    localAdjust = this._dateUtcOffset();
                }
                this._offset = input;
                this._isUTC = true;
                if (localAdjust != null) {
                    this.add(localAdjust, 'm');
                }
                if (offset !== input) {
                    if (!keepLocalTime || this._changeInProgress) {
                        addOrSubtractDurationFromMoment(this, moment.duration(input - offset, 'm'), 1, false);
                    } else if (!this._changeInProgress) {
                        this._changeInProgress = true;
                        moment.updateOffset(this, true);
                        this._changeInProgress = null;
                    }
                }
                return this;
            } else {
                return this._isUTC ? offset : this._dateUtcOffset();
            }
        },
        isLocal: function() {
            return !this._isUTC;
        },
        isUtcOffset: function() {
            return this._isUTC;
        },
        isUtc: function() {
            return this._isUTC && this._offset === 0;
        },
        zoneAbbr: function() {
            return this._isUTC ? 'UTC' : '';
        },
        zoneName: function() {
            return this._isUTC ? 'Coordinated Universal Time' : '';
        },
        parseZone: function() {
            if (this._tzm) {
                this.utcOffset(this._tzm);
            } else if (typeof this._i === 'string') {
                this.utcOffset(utcOffsetFromString(this._i));
            }
            return this;
        },
        hasAlignedHourOffset: function(input) {
            if (!input) {
                input = 0;
            } else {
                input = moment(input).utcOffset();
            }
            return (this.utcOffset() - input) % 60 === 0;
        },
        daysInMonth: function() {
            return daysInMonth(this.year(), this.month());
        },
        dayOfYear: function(input) {
            var dayOfYear = round((moment(this).startOf('day') - moment(this).startOf('year')) / 864e5) + 1;
            return input == null ? dayOfYear : this.add((input - dayOfYear), 'd');
        },
        quarter: function(input) {
            return input == null ? Math.ceil((this.month() + 1) / 3) : this.month((input - 1) * 3 + this.month() % 3);
        },
        weekYear: function(input) {
            var year = weekOfYear(this, this.localeData()._week.dow, this.localeData()._week.doy).year;
            return input == null ? year : this.add((input - year), 'y');
        },
        isoWeekYear: function(input) {
            var year = weekOfYear(this, 1, 4).year;
            return input == null ? year : this.add((input - year), 'y');
        },
        week: function(input) {
            var week = this.localeData().week(this);
            return input == null ? week : this.add((input - week) * 7, 'd');
        },
        isoWeek: function(input) {
            var week = weekOfYear(this, 1, 4).week;
            return input == null ? week : this.add((input - week) * 7, 'd');
        },
        weekday: function(input) {
            var weekday = (this.day() + 7 - this.localeData()._week.dow) % 7;
            return input == null ? weekday : this.add(input - weekday, 'd');
        },
        isoWeekday: function(input) {
            return input == null ? this.day() || 7 : this.day(this.day() % 7 ? input : input - 7);
        },
        isoWeeksInYear: function() {
            return weeksInYear(this.year(), 1, 4);
        },
        weeksInYear: function() {
            var weekInfo = this.localeData()._week;
            return weeksInYear(this.year(), weekInfo.dow, weekInfo.doy);
        },
        get: function(units) {
            units = normalizeUnits(units);
            return this[units]();
        },
        set: function(units, value) {
            var unit;
            if (typeof units === 'object') {
                for (unit in units) {
                    this.set(unit, units[unit]);
                }
            } else {
                units = normalizeUnits(units);
                if (typeof this[units] === 'function') {
                    this[units](value);
                }
            }
            return this;
        },
        locale: function(key) {
            var newLocaleData;
            if (key === undefined) {
                return this._locale._abbr;
            } else {
                newLocaleData = moment.localeData(key);
                if (newLocaleData != null) {
                    this._locale = newLocaleData;
                }
                return this;
            }
        },
        lang: deprecate('moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.', function(key) {
            if (key === undefined) {
                return this.localeData();
            } else {
                return this.locale(key);
            }
        }),
        localeData: function() {
            return this._locale;
        },
        _dateUtcOffset: function() {
            return -Math.round(this._d.getTimezoneOffset() / 15) * 15;
        }
    });

    function rawMonthSetter(mom, value) {
        var dayOfMonth;
        if (typeof value === 'string') {
            value = mom.localeData().monthsParse(value);
            if (typeof value !== 'number') {
                return mom;
            }
        }
        dayOfMonth = Math.min(mom.date(), daysInMonth(mom.year(), value));
        mom._d['set' + (mom._isUTC ? 'UTC' : '') + 'Month'](value, dayOfMonth);
        return mom;
    }

    function rawGetter(mom, unit) {
        return mom._d['get' + (mom._isUTC ? 'UTC' : '') + unit]();
    }

    function rawSetter(mom, unit, value) {
        if (unit === 'Month') {
            return rawMonthSetter(mom, value);
        } else {
            return mom._d['set' + (mom._isUTC ? 'UTC' : '') + unit](value);
        }
    }

    function makeAccessor(unit, keepTime) {
        return function(value) {
            if (value != null) {
                rawSetter(this, unit, value);
                moment.updateOffset(this, keepTime);
                return this;
            } else {
                return rawGetter(this, unit);
            }
        };
    }
    moment.fn.millisecond = moment.fn.milliseconds = makeAccessor('Milliseconds', false);
    moment.fn.second = moment.fn.seconds = makeAccessor('Seconds', false);
    moment.fn.minute = moment.fn.minutes = makeAccessor('Minutes', false);
    moment.fn.hour = moment.fn.hours = makeAccessor('Hours', true);
    moment.fn.date = makeAccessor('Date', true);
    moment.fn.dates = deprecate('dates accessor is deprecated. Use date instead.', makeAccessor('Date', true));
    moment.fn.year = makeAccessor('FullYear', true);
    moment.fn.years = deprecate('years accessor is deprecated. Use year instead.', makeAccessor('FullYear', true));
    moment.fn.days = moment.fn.day;
    moment.fn.months = moment.fn.month;
    moment.fn.weeks = moment.fn.week;
    moment.fn.isoWeeks = moment.fn.isoWeek;
    moment.fn.quarters = moment.fn.quarter;
    moment.fn.toJSON = moment.fn.toISOString;
    moment.fn.isUTC = moment.fn.isUtc;

    function daysToYears(days) {
        return days * 400 / 146097;
    }

    function yearsToDays(years) {
        return years * 146097 / 400;
    }
    extend(moment.duration.fn = Duration.prototype, {
        _bubble: function() {
            var milliseconds = this._milliseconds,
                days = this._days,
                months = this._months,
                data = this._data,
                seconds, minutes, hours, years = 0;
            data.milliseconds = milliseconds % 1000;
            seconds = absRound(milliseconds / 1000);
            data.seconds = seconds % 60;
            minutes = absRound(seconds / 60);
            data.minutes = minutes % 60;
            hours = absRound(minutes / 60);
            data.hours = hours % 24;
            days += absRound(hours / 24);
            years = absRound(daysToYears(days));
            days -= absRound(yearsToDays(years));
            months += absRound(days / 30);
            days %= 30;
            years += absRound(months / 12);
            months %= 12;
            data.days = days;
            data.months = months;
            data.years = years;
        },
        abs: function() {
            this._milliseconds = Math.abs(this._milliseconds);
            this._days = Math.abs(this._days);
            this._months = Math.abs(this._months);
            this._data.milliseconds = Math.abs(this._data.milliseconds);
            this._data.seconds = Math.abs(this._data.seconds);
            this._data.minutes = Math.abs(this._data.minutes);
            this._data.hours = Math.abs(this._data.hours);
            this._data.months = Math.abs(this._data.months);
            this._data.years = Math.abs(this._data.years);
            return this;
        },
        weeks: function() {
            return absRound(this.days() / 7);
        },
        valueOf: function() {
            return this._milliseconds +
                this._days * 864e5 +
                (this._months % 12) * 2592e6 +
                toInt(this._months / 12) * 31536e6;
        },
        humanize: function(withSuffix) {
            var output = relativeTime(this, !withSuffix, this.localeData());
            if (withSuffix) {
                output = this.localeData().pastFuture(+this, output);
            }
            return this.localeData().postformat(output);
        },
        add: function(input, val) {
            var dur = moment.duration(input, val);
            this._milliseconds += dur._milliseconds;
            this._days += dur._days;
            this._months += dur._months;
            this._bubble();
            return this;
        },
        subtract: function(input, val) {
            var dur = moment.duration(input, val);
            this._milliseconds -= dur._milliseconds;
            this._days -= dur._days;
            this._months -= dur._months;
            this._bubble();
            return this;
        },
        get: function(units) {
            units = normalizeUnits(units);
            return this[units.toLowerCase() + 's']();
        },
        as: function(units) {
            var days, months;
            units = normalizeUnits(units);
            if (units === 'month' || units === 'year') {
                days = this._days + this._milliseconds / 864e5;
                months = this._months + daysToYears(days) * 12;
                return units === 'month' ? months : months / 12;
            } else {
                days = this._days + Math.round(yearsToDays(this._months / 12));
                switch (units) {
                    case 'week':
                        return days / 7 + this._milliseconds / 6048e5;
                    case 'day':
                        return days + this._milliseconds / 864e5;
                    case 'hour':
                        return days * 24 + this._milliseconds / 36e5;
                    case 'minute':
                        return days * 24 * 60 + this._milliseconds / 6e4;
                    case 'second':
                        return days * 24 * 60 * 60 + this._milliseconds / 1000;
                    case 'millisecond':
                        return Math.floor(days * 24 * 60 * 60 * 1000) + this._milliseconds;
                    default:
                        throw new Error('Unknown unit ' + units);
                }
            }
        },
        lang: moment.fn.lang,
        locale: moment.fn.locale,
        toIsoString: deprecate('toIsoString() is deprecated. Please use toISOString() instead ' + '(notice the capitals)', function() {
            return this.toISOString();
        }),
        toISOString: function() {
            var years = Math.abs(this.years()),
                months = Math.abs(this.months()),
                days = Math.abs(this.days()),
                hours = Math.abs(this.hours()),
                minutes = Math.abs(this.minutes()),
                seconds = Math.abs(this.seconds() + this.milliseconds() / 1000);
            if (!this.asSeconds()) {
                return 'P0D';
            }
            return (this.asSeconds() < 0 ? '-' : '') + 'P' +
                (years ? years + 'Y' : '') +
                (months ? months + 'M' : '') +
                (days ? days + 'D' : '') +
                ((hours || minutes || seconds) ? 'T' : '') +
                (hours ? hours + 'H' : '') +
                (minutes ? minutes + 'M' : '') +
                (seconds ? seconds + 'S' : '');
        },
        localeData: function() {
            return this._locale;
        },
        toJSON: function() {
            return this.toISOString();
        }
    });
    moment.duration.fn.toString = moment.duration.fn.toISOString;

    function makeDurationGetter(name) {
        moment.duration.fn[name] = function() {
            return this._data[name];
        };
    }
    for (i in unitMillisecondFactors) {
        if (hasOwnProp(unitMillisecondFactors, i)) {
            makeDurationGetter(i.toLowerCase());
        }
    }
    moment.duration.fn.asMilliseconds = function() {
        return this.as('ms');
    };
    moment.duration.fn.asSeconds = function() {
        return this.as('s');
    };
    moment.duration.fn.asMinutes = function() {
        return this.as('m');
    };
    moment.duration.fn.asHours = function() {
        return this.as('h');
    };
    moment.duration.fn.asDays = function() {
        return this.as('d');
    };
    moment.duration.fn.asWeeks = function() {
        return this.as('weeks');
    };
    moment.duration.fn.asMonths = function() {
        return this.as('M');
    };
    moment.duration.fn.asYears = function() {
        return this.as('y');
    };
    moment.locale('en', {
        ordinalParse: /\d{1,2}(th|st|nd|rd)/,
        ordinal: function(number) {
            var b = number % 10,
                output = (toInt(number % 100 / 10) === 1) ? 'th' : (b === 1) ? 'st' : (b === 2) ? 'nd' : (b === 3) ? 'rd' : 'th';
            return number + output;
        }
    });

    function makeGlobal(shouldDeprecate) {
        if (typeof ender !== 'undefined') {
            return;
        }
        oldGlobalMoment = globalScope.moment;
        if (shouldDeprecate) {
            globalScope.moment = deprecate('Accessing Moment through the global scope is ' + 'deprecated, and will be removed in an upcoming ' + 'release.', moment);
        } else {
            globalScope.moment = moment;
        }
    }
    if (hasModule) {
        module.exports = moment;
    } else if (typeof define === 'function' && define.amd) {
        define(function(require, exports, module) {
            if (module.config && module.config() && module.config().noGlobal === true) {
                globalScope.moment = oldGlobalMoment;
            }
            return moment;
        });
        makeGlobal(true);
    } else {
        makeGlobal();
    }
}).call(this);

/*===============================
https://www.equipmentone.com/assetnation/js/autoNumeric.js
================================================================================*/
;?
(function($) {
    "use strict";

    function getElementSelection(that) {
        var position = {};
        if (that.selectionStart === undefined) {
            that.focus();
            var select = document.selection.createRange();
            position.length = select.text.length;
            select.moveStart('character', -that.value.length);
            position.end = select.text.length;
            position.start = position.end - position.length;
        } else {
            position.start = that.selectionStart;
            position.end = that.selectionEnd;
            position.length = position.end - position.start;
        }
        return position;
    }

    function setElementSelection(that, start, end) {
        if (that.selectionStart === undefined) {
            that.focus();
            var r = that.createTextRange();
            r.collapse(true);
            r.moveEnd('character', end);
            r.moveStart('character', start);
            r.select();
        } else {
            that.selectionStart = start;
            that.selectionEnd = end;
        }
    }

    function runCallbacks($this, settings) {
        $.each(settings, function(k, val) {
            if (typeof val === 'function') {
                settings[k] = val($this, settings, k);
            } else if (typeof $this.autoNumeric[val] === 'function') {
                settings[k] = $this.autoNumeric[val]($this, settings, k);
            }
        });
    }

    function convertKeyToNumber(settings, key) {
        if (typeof(settings[key]) === 'string') {
            settings[key] *= 1;
        }
    }

    function autoCode($this, settings) {
        runCallbacks($this, settings);
        settings.oEvent = null;
        settings.tagList = ['B', 'CAPTION', 'CITE', 'CODE', 'DD', 'DEL', 'DIV', 'DFN', 'DT', 'EM', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'INS', 'KDB', 'LABEL', 'LI', 'OUTPUT', 'P', 'Q', 'S', 'SAMPLE', 'SPAN', 'STRONG', 'TD', 'TH', 'U', 'VAR'];
        var vmax = settings.vMax.toString().split('.'),
            vmin = (!settings.vMin && settings.vMin !== 0) ? [] : settings.vMin.toString().split('.');
        convertKeyToNumber(settings, 'vMax');
        convertKeyToNumber(settings, 'vMin');
        convertKeyToNumber(settings, 'mDec');
        settings.mDec = (settings.mRound === 'CHF') ? '2' : settings.mDec;
        settings.allowLeading = true;
        settings.aNeg = settings.vMin < 0 ? '-' : '';
        vmax[0] = vmax[0].replace('-', '');
        vmin[0] = vmin[0].replace('-', '');
        settings.mInt = Math.max(vmax[0].length, vmin[0].length, 1);
        if (settings.mDec === null) {
            var vmaxLength = 0,
                vminLength = 0;
            if (vmax[1]) {
                vmaxLength = vmax[1].length;
            }
            if (vmin[1]) {
                vminLength = vmin[1].length;
            }
            settings.mDec = Math.max(vmaxLength, vminLength);
        }
        if (settings.altDec === null && settings.mDec > 0) {
            if (settings.aDec === '.' && settings.aSep !== ',') {
                settings.altDec = ',';
            } else if (settings.aDec === ',' && settings.aSep !== '.') {
                settings.altDec = '.';
            }
        }
        var aNegReg = settings.aNeg ? '([-\\' + settings.aNeg + ']?)' : '(-?)';
        settings.aNegRegAutoStrip = aNegReg;
        settings.skipFirstAutoStrip = new RegExp(aNegReg + '[^-' + (settings.aNeg ? '\\' + settings.aNeg : '') + '\\' + settings.aDec + '\\d]' + '.*?(\\d|\\' + settings.aDec + '\\d)');
        settings.skipLastAutoStrip = new RegExp('(\\d\\' + settings.aDec + '?)[^\\' + settings.aDec + '\\d]\\D*$');
        var allowed = '-' + settings.aNum + '\\' + settings.aDec;
        settings.allowedAutoStrip = new RegExp('[^' + allowed + ']', 'gi');
        settings.numRegAutoStrip = new RegExp(aNegReg + '(?:\\' + settings.aDec + '?(\\d+\\' + settings.aDec + '\\d+)|(\\d*(?:\\' + settings.aDec + '\\d*)?))');
        return settings;
    }

    function autoStrip(s, settings, strip_zero) {
        if (settings.aSign) {
            while (s.indexOf(settings.aSign) > -1) {
                s = s.replace(settings.aSign, '');
            }
        }
        s = s.replace(settings.skipFirstAutoStrip, '$1$2');
        s = s.replace(settings.skipLastAutoStrip, '$1');
        s = s.replace(settings.allowedAutoStrip, '');
        if (settings.altDec) {
            s = s.replace(settings.altDec, settings.aDec);
        }
        var m = s.match(settings.numRegAutoStrip);
        s = m ? [m[1], m[2], m[3]].join('') : '';
        if ((settings.lZero === 'allow' || settings.lZero === 'keep') && strip_zero !== 'strip') {
            var parts = [],
                nSign = '';
            parts = s.split(settings.aDec);
            if (parts[0].indexOf('-') !== -1) {
                nSign = '-';
                parts[0] = parts[0].replace('-', '');
            }
            if (parts[0].length > settings.mInt && parts[0].charAt(0) === '0') {
                parts[0] = parts[0].slice(1);
            }
            s = nSign + parts.join(settings.aDec);
        }
        if ((strip_zero && settings.lZero === 'deny') || (strip_zero && settings.lZero === 'allow' && settings.allowLeading === false)) {
            var strip_reg = '^' + settings.aNegRegAutoStrip + '0*(\\d' + (strip_zero === 'leading' ? ')' : '|$)');
            strip_reg = new RegExp(strip_reg);
            s = s.replace(strip_reg, '$1$2');
        }
        return s;
    }

    function negativeBracket(s, nBracket, oEvent) {
        nBracket = nBracket.split(',');
        if (oEvent === 'set' || oEvent === 'focusout') {
            s = s.replace('-', '');
            s = nBracket[0] + s + nBracket[1];
        } else if ((oEvent === 'get' || oEvent === 'focusin' || oEvent === 'pageLoad') && s.charAt(0) === nBracket[0]) {
            s = s.replace(nBracket[0], '-');
            s = s.replace(nBracket[1], '');
        }
        return s;
    }

    function truncateDecimal(s, aDec, mDec) {
        if (aDec && mDec) {
            var parts = s.split(aDec);
            if (parts[1] && parts[1].length > mDec) {
                if (mDec > 0) {
                    parts[1] = parts[1].substring(0, mDec);
                    s = parts.join(aDec);
                } else {
                    s = parts[0];
                }
            }
        }
        return s;
    }

    function fixNumber(s, aDec, aNeg) {
        if (aDec && aDec !== '.') {
            s = s.replace(aDec, '.');
        }
        if (aNeg && aNeg !== '-') {
            s = s.replace(aNeg, '-');
        }
        if (!s.match(/\d/)) {
            s += '0';
        }
        return s;
    }

    function checkValue(value, settings) {
        if (value) {
            var checkSmall = +value;
            if (checkSmall < 0.000001 && checkSmall > -1) {
                value = +value;
                if (value < 0.000001 && value > 0) {
                    value = (value + 10).toString();
                    value = value.substring(1);
                }
                if (value < 0 && value > -1) {
                    value = (value - 10).toString();
                    value = '-' + value.substring(2);
                }
                value = value.toString();
            } else {
                var parts = value.split('.');
                if (parts[1] !== undefined) {
                    if (+parts[1] === 0) {
                        value = parts[0];
                    } else {
                        parts[1] = parts[1].replace(/0*$/, '');
                        value = parts.join('.');
                    }
                }
            }
        }
        return (settings.lZero === 'keep') ? value : value.replace(/^0*(\d)/, '$1');
    }

    function presentNumber(s, aDec, aNeg) {
        if (aNeg && aNeg !== '-') {
            s = s.replace('-', aNeg);
        }
        if (aDec && aDec !== '.') {
            s = s.replace('.', aDec);
        }
        return s;
    }

    function autoCheck(s, settings) {
        s = autoStrip(s, settings);
        s = truncateDecimal(s, settings.aDec, settings.mDec);
        s = fixNumber(s, settings.aDec, settings.aNeg);
        var value = +s;
        if (settings.oEvent === 'set' && (value < settings.vMin || value > settings.vMax)) {
            $.error("The value (" + value + ") from the 'set' method falls outside of the vMin / vMax range");
        }
        return value >= settings.vMin && value <= settings.vMax;
    }

    function checkEmpty(iv, settings, signOnEmpty) {
        if (iv === '' || iv === settings.aNeg) {
            if (settings.wEmpty === 'zero') {
                return iv + '0';
            }
            if (settings.wEmpty === 'sign' || signOnEmpty) {
                return iv + settings.aSign;
            }
            return iv;
        }
        return null;
    }

    function autoGroup(iv, settings) {
        iv = autoStrip(iv, settings);
        var testNeg = iv.replace(',', '.'),
            empty = checkEmpty(iv, settings, true);
        if (empty !== null) {
            return empty;
        }
        var digitalGroup = '';
        if (settings.dGroup === 2) {
            digitalGroup = /(\d)((\d)(\d{2}?)+)$/;
        } else if (settings.dGroup === 4) {
            digitalGroup = /(\d)((\d{4}?)+)$/;
        } else {
            digitalGroup = /(\d)((\d{3}?)+)$/;
        }
        var ivSplit = iv.split(settings.aDec);
        if (settings.altDec && ivSplit.length === 1) {
            ivSplit = iv.split(settings.altDec);
        }
        var s = ivSplit[0];
        if (settings.aSep) {
            while (digitalGroup.test(s)) {
                s = s.replace(digitalGroup, '$1' + settings.aSep + '$2');
            }
        }
        if (settings.mDec !== 0 && ivSplit.length > 1) {
            if (ivSplit[1].length > settings.mDec) {
                ivSplit[1] = ivSplit[1].substring(0, settings.mDec);
            }
            iv = s + settings.aDec + ivSplit[1];
        } else {
            if (iv > 0 && settings.zCent) {
                iv = s + settings.aDec + '00';
            } else if (settings.zCent) {
                iv = '';
            } else {
                iv = s;
            }
        }
        if (settings.aSign) {
            var has_aNeg = iv.indexOf(settings.aNeg) !== -1;
            iv = iv.replace(settings.aNeg, '');
            iv = settings.pSign === 'p' ? settings.aSign + iv : iv + settings.aSign;
            if (has_aNeg) {
                iv = settings.aNeg + iv;
            }
        }
        if (settings.oEvent === 'set' && testNeg < 0 && settings.nBracket !== null) {
            iv = negativeBracket(iv, settings.nBracket, settings.oEvent);
        }
        return iv;
    }

    function autoRound(iv, settings) {
        iv = (iv === '') ? '0' : iv.toString();
        convertKeyToNumber(settings, 'mDec');
        if (settings.mRound === 'CHF') {
            iv = (Math.round(iv * 20) / 20).toString();
        }
        var ivRounded = '',
            i = 0,
            nSign = '',
            rDec = (typeof(settings.aPad) === 'boolean' || settings.aPad === null) ? (settings.aPad ? settings.mDec : 0) : +settings.aPad;
        var truncateZeros = function(ivRounded) {
            var regex = (rDec === 0) ? (/(\.(?:\d*[1-9])?)0*$/) : rDec === 1 ? (/(\.\d(?:\d*[1-9])?)0*$/) : new RegExp('(\\.\\d{' + rDec + '}(?:\\d*[1-9])?)0*$');
            ivRounded = ivRounded.replace(regex, '$1');
            if (rDec === 0) {
                ivRounded = ivRounded.replace(/\.$/, '');
            }
            return ivRounded;
        };
        if (iv.charAt(0) === '-') {
            nSign = '-';
            iv = iv.replace('-', '');
        }
        if (!iv.match(/^\d/)) {
            iv = '0' + iv;
        }
        if (nSign === '-' && +iv === 0) {
            nSign = '';
        }
        if ((+iv > 0 && settings.lZero !== 'keep') || (iv.length > 0 && settings.lZero === 'allow')) {
            iv = iv.replace(/^0*(\d)/, '$1');
        }
        var dPos = iv.lastIndexOf('.'),
            vdPos = (dPos === -1) ? iv.length - 1 : dPos,
            cDec = (iv.length - 1) - vdPos;
        if (cDec <= settings.mDec) {
            ivRounded = iv;
            if (cDec < rDec) {
                if (dPos === -1) {
                    ivRounded += '.';
                }
                var zeros = '000000';
                while (cDec < rDec) {
                    zeros = zeros.substring(0, rDec - cDec);
                    ivRounded += zeros;
                    cDec += zeros.length;
                }
            } else if (cDec > rDec) {
                ivRounded = truncateZeros(ivRounded);
            } else if (cDec === 0 && rDec === 0) {
                ivRounded = ivRounded.replace(/\.$/, '');
            }
            if (settings.mRound !== 'CHF') {
                return (+ivRounded === 0) ? ivRounded : nSign + ivRounded;
            }
            if (settings.mRound === 'CHF') {
                dPos = ivRounded.lastIndexOf('.');
                iv = ivRounded;
            }
        }
        var rLength = dPos + settings.mDec,
            tRound = +iv.charAt(rLength + 1),
            ivArray = iv.substring(0, rLength + 1).split(''),
            odd = (iv.charAt(rLength) === '.') ? (iv.charAt(rLength - 1) % 2) : (iv.charAt(rLength) % 2),
            onePass = true;
        odd = (odd === 0 && (iv.substring(rLength + 2, iv.length) > 0)) ? 1 : 0;
        if ((tRound > 4 && settings.mRound === 'S') || (tRound > 4 && settings.mRound === 'A' && nSign === '') || (tRound > 5 && settings.mRound === 'A' && nSign === '-') || (tRound > 5 && settings.mRound === 's') || (tRound > 5 && settings.mRound === 'a' && nSign === '') || (tRound > 4 && settings.mRound === 'a' && nSign === '-') || (tRound > 5 && settings.mRound === 'B') || (tRound === 5 && settings.mRound === 'B' && odd === 1) || (tRound > 0 && settings.mRound === 'C' && nSign === '') || (tRound > 0 && settings.mRound === 'F' && nSign === '-') || (tRound > 0 && settings.mRound === 'U') || (settings.mRound === 'CHF')) {
            for (i = (ivArray.length - 1); i >= 0; i -= 1) {
                if (ivArray[i] !== '.') {
                    if (settings.mRound === 'CHF' && ivArray[i] <= 2 && onePass) {
                        ivArray[i] = 0;
                        onePass = false;
                        break;
                    }
                    if (settings.mRound === 'CHF' && ivArray[i] <= 7 && onePass) {
                        ivArray[i] = 5;
                        onePass = false;
                        break;
                    }
                    if (settings.mRound === 'CHF' && onePass) {
                        ivArray[i] = 10;
                        onePass = false;
                    } else {
                        ivArray[i] = +ivArray[i] + 1;
                    }
                    if (ivArray[i] < 10) {
                        break;
                    }
                    if (i > 0) {
                        ivArray[i] = '0';
                    }
                }
            }
        }
        ivArray = ivArray.slice(0, rLength + 1);
        ivRounded = truncateZeros(ivArray.join(''));
        return (+ivRounded === 0) ? ivRounded : nSign + ivRounded;
    }

    function AutoNumericHolder(that, settings) {
        this.settings = settings;
        this.that = that;
        this.$that = $(that);
        this.formatted = false;
        this.settingsClone = autoCode(this.$that, this.settings);
        this.value = that.value;
    }
    AutoNumericHolder.prototype = {
        init: function(e) {
            this.value = this.that.value;
            this.settingsClone = autoCode(this.$that, this.settings);
            this.ctrlKey = e.ctrlKey;
            this.cmdKey = e.metaKey;
            this.shiftKey = e.shiftKey;
            this.selection = getElementSelection(this.that);
            if (e.type === 'keydown' || e.type === 'keyup') {
                this.kdCode = e.keyCode;
            }
            this.which = e.which;
            this.processed = false;
            this.formatted = false;
        },
        setSelection: function(start, end, setReal) {
            start = Math.max(start, 0);
            end = Math.min(end, this.that.value.length);
            this.selection = {
                start: start,
                end: end,
                length: end - start
            };
            if (setReal === undefined || setReal) {
                setElementSelection(this.that, start, end);
            }
        },
        setPosition: function(pos, setReal) {
            this.setSelection(pos, pos, setReal);
        },
        getBeforeAfter: function() {
            var value = this.value,
                left = value.substring(0, this.selection.start),
                right = value.substring(this.selection.end, value.length);
            return [left, right];
        },
        getBeforeAfterStriped: function() {
            var parts = this.getBeforeAfter();
            parts[0] = autoStrip(parts[0], this.settingsClone);
            parts[1] = autoStrip(parts[1], this.settingsClone);
            return parts;
        },
        normalizeParts: function(left, right) {
            var settingsClone = this.settingsClone;
            right = autoStrip(right, settingsClone);
            var strip = right.match(/^\d/) ? true : 'leading';
            left = autoStrip(left, settingsClone, strip);
            if ((left === '' || left === settingsClone.aNeg) && settingsClone.lZero === 'deny') {
                if (right > '') {
                    right = right.replace(/^0*(\d)/, '$1');
                }
            }
            var new_value = left + right;
            if (settingsClone.aDec) {
                var m = new_value.match(new RegExp('^' + settingsClone.aNegRegAutoStrip + '\\' + settingsClone.aDec));
                if (m) {
                    left = left.replace(m[1], m[1] + '0');
                    new_value = left + right;
                }
            }
            if (settingsClone.wEmpty === 'zero' && (new_value === settingsClone.aNeg || new_value === '')) {
                left += '0';
            }
            return [left, right];
        },
        setValueParts: function(left, right) {
            var settingsClone = this.settingsClone,
                parts = this.normalizeParts(left, right),
                new_value = parts.join(''),
                position = parts[0].length;
            if (autoCheck(new_value, settingsClone)) {
                new_value = truncateDecimal(new_value, settingsClone.aDec, settingsClone.mDec);
                if (position > new_value.length) {
                    position = new_value.length;
                }
                this.value = new_value;
                this.setPosition(position, false);
                return true;
            }
            return false;
        },
        signPosition: function() {
            var settingsClone = this.settingsClone,
                aSign = settingsClone.aSign,
                that = this.that;
            if (aSign) {
                var aSignLen = aSign.length;
                if (settingsClone.pSign === 'p') {
                    var hasNeg = settingsClone.aNeg && that.value && that.value.charAt(0) === settingsClone.aNeg;
                    return hasNeg ? [1, aSignLen + 1] : [0, aSignLen];
                }
                var valueLen = that.value.length;
                return [valueLen - aSignLen, valueLen];
            }
            return [1000, -1];
        },
        expandSelectionOnSign: function(setReal) {
            var sign_position = this.signPosition(),
                selection = this.selection;
            if (selection.start < sign_position[1] && selection.end > sign_position[0]) {
                if ((selection.start < sign_position[0] || selection.end > sign_position[1]) && this.value.substring(Math.max(selection.start, sign_position[0]), Math.min(selection.end, sign_position[1])).match(/^\s*$/)) {
                    if (selection.start < sign_position[0]) {
                        this.setSelection(selection.start, sign_position[0], setReal);
                    } else {
                        this.setSelection(sign_position[1], selection.end, setReal);
                    }
                } else {
                    this.setSelection(Math.min(selection.start, sign_position[0]), Math.max(selection.end, sign_position[1]), setReal);
                }
            }
        },
        checkPaste: function() {
            if (this.valuePartsBeforePaste !== undefined) {
                var parts = this.getBeforeAfter(),
                    oldParts = this.valuePartsBeforePaste;
                delete this.valuePartsBeforePaste;
                parts[0] = parts[0].substr(0, oldParts[0].length) + autoStrip(parts[0].substr(oldParts[0].length), this.settingsClone);
                if (!this.setValueParts(parts[0], parts[1])) {
                    this.value = oldParts.join('');
                    this.setPosition(oldParts[0].length, false);
                }
            }
        },
        skipAllways: function(e) {
            var kdCode = this.kdCode,
                which = this.which,
                ctrlKey = this.ctrlKey,
                cmdKey = this.cmdKey,
                shiftKey = this.shiftKey;
            if (((ctrlKey || cmdKey) && e.type === 'keyup' && this.valuePartsBeforePaste !== undefined) || (shiftKey && kdCode === 45)) {
                this.checkPaste();
                return false;
            }
            if ((kdCode >= 112 && kdCode <= 123) || (kdCode >= 91 && kdCode <= 93) || (kdCode >= 9 && kdCode <= 31) || (kdCode < 8 && (which === 0 || which === kdCode)) || kdCode === 144 || kdCode === 145 || kdCode === 45) {
                return true;
            }
            if ((ctrlKey || cmdKey) && kdCode === 65) {
                return true;
            }
            if ((ctrlKey || cmdKey) && (kdCode === 67 || kdCode === 86 || kdCode === 88)) {
                if (e.type === 'keydown') {
                    this.expandSelectionOnSign();
                }
                if (kdCode === 86 || kdCode === 45) {
                    if (e.type === 'keydown' || e.type === 'keypress') {
                        if (this.valuePartsBeforePaste === undefined) {
                            this.valuePartsBeforePaste = this.getBeforeAfter();
                        }
                    } else {
                        this.checkPaste();
                    }
                }
                return e.type === 'keydown' || e.type === 'keypress' || kdCode === 67;
            }
            if (ctrlKey || cmdKey) {
                return true;
            }
            if (kdCode === 37 || kdCode === 39) {
                var aSep = this.settingsClone.aSep,
                    start = this.selection.start,
                    value = this.that.value;
                if (e.type === 'keydown' && aSep && !this.shiftKey) {
                    if (kdCode === 37 && value.charAt(start - 2) === aSep) {
                        this.setPosition(start - 1);
                    } else if (kdCode === 39 && value.charAt(start + 1) === aSep) {
                        this.setPosition(start + 1);
                    }
                }
                return true;
            }
            if (kdCode >= 34 && kdCode <= 40) {
                return true;
            }
            return false;
        },
        processAllways: function() {
            var parts;
            if (this.kdCode === 8 || this.kdCode === 46) {
                if (!this.selection.length) {
                    parts = this.getBeforeAfterStriped();
                    if (this.kdCode === 8) {
                        parts[0] = parts[0].substring(0, parts[0].length - 1);
                    } else {
                        parts[1] = parts[1].substring(1, parts[1].length);
                    }
                    this.setValueParts(parts[0], parts[1]);
                } else {
                    this.expandSelectionOnSign(false);
                    parts = this.getBeforeAfterStriped();
                    this.setValueParts(parts[0], parts[1]);
                }
                return true;
            }
            return false;
        },
        processKeypress: function() {
            var settingsClone = this.settingsClone,
                cCode = String.fromCharCode(this.which),
                parts = this.getBeforeAfterStriped(),
                left = parts[0],
                right = parts[1];
            if (cCode === settingsClone.aDec || (settingsClone.altDec && cCode === settingsClone.altDec) || ((cCode === '.' || cCode === ',') && this.kdCode === 110)) {
                if (!settingsClone.mDec || !settingsClone.aDec) {
                    return true;
                }
                if (settingsClone.aNeg && right.indexOf(settingsClone.aNeg) > -1) {
                    return true;
                }
                if (left.indexOf(settingsClone.aDec) > -1) {
                    return true;
                }
                if (right.indexOf(settingsClone.aDec) > 0) {
                    return true;
                }
                if (right.indexOf(settingsClone.aDec) === 0) {
                    right = right.substr(1);
                }
                this.setValueParts(left + settingsClone.aDec, right);
                return true;
            }
            if (cCode === '-' || cCode === '+') {
                if (!settingsClone.aNeg) {
                    return true;
                }
                if (left === '' && right.indexOf(settingsClone.aNeg) > -1) {
                    left = settingsClone.aNeg;
                    right = right.substring(1, right.length);
                }
                if (left.charAt(0) === settingsClone.aNeg) {
                    left = left.substring(1, left.length);
                } else {
                    left = (cCode === '-') ? settingsClone.aNeg + left : left;
                }
                this.setValueParts(left, right);
                return true;
            }
            if (cCode >= '0' && cCode <= '9') {
                if (settingsClone.aNeg && left === '' && right.indexOf(settingsClone.aNeg) > -1) {
                    left = settingsClone.aNeg;
                    right = right.substring(1, right.length);
                }
                if (settingsClone.vMax <= 0 && settingsClone.vMin < settingsClone.vMax && this.value.indexOf(settingsClone.aNeg) === -1 && cCode !== '0') {
                    left = settingsClone.aNeg + left;
                }
                this.setValueParts(left + cCode, right);
                return true;
            }
            return true;
        },
        formatQuick: function() {
            var settingsClone = this.settingsClone,
                parts = this.getBeforeAfterStriped(),
                leftLength = this.value;
            if ((settingsClone.aSep === '' || (settingsClone.aSep !== '' && leftLength.indexOf(settingsClone.aSep) === -1)) && (settingsClone.aSign === '' || (settingsClone.aSign !== '' && leftLength.indexOf(settingsClone.aSign) === -1))) {
                var subParts = [],
                    nSign = '';
                subParts = leftLength.split(settingsClone.aDec);
                if (subParts[0].indexOf('-') > -1) {
                    nSign = '-';
                    subParts[0] = subParts[0].replace('-', '');
                    parts[0] = parts[0].replace('-', '');
                }
                if (subParts[0].length > settingsClone.mInt && parts[0].charAt(0) === '0') {
                    parts[0] = parts[0].slice(1);
                }
                parts[0] = nSign + parts[0];
            }
            var value = autoGroup(this.value, this.settingsClone),
                position = value.length;
            if (value) {
                var left_ar = parts[0].split(''),
                    i = 0;
                for (i; i < left_ar.length; i += 1) {
                    if (!left_ar[i].match('\\d')) {
                        left_ar[i] = '\\' + left_ar[i];
                    }
                }
                var leftReg = new RegExp('^.*?' + left_ar.join('.*?'));
                var newLeft = value.match(leftReg);
                if (newLeft) {
                    position = newLeft[0].length;
                    if (((position === 0 && value.charAt(0) !== settingsClone.aNeg) || (position === 1 && value.charAt(0) === settingsClone.aNeg)) && settingsClone.aSign && settingsClone.pSign === 'p') {
                        position = this.settingsClone.aSign.length + (value.charAt(0) === '-' ? 1 : 0);
                    }
                } else if (settingsClone.aSign && settingsClone.pSign === 's') {
                    position -= settingsClone.aSign.length;
                }
            }
            this.that.value = value;
            this.setPosition(position);
            this.formatted = true;
        }
    };

    function autoGet(obj) {
        if (typeof obj === 'string') {
            obj = obj.replace(/\[/g, "\\[").replace(/\]/g, "\\]");
            obj = '#' + obj.replace(/(:|\.)/g, '\\$1');
        }
        return $(obj);
    }

    function getHolder($that, settings, update) {
        var data = $that.data('autoNumeric');
        if (!data) {
            data = {};
            $that.data('autoNumeric', data);
        }
        var holder = data.holder;
        if ((holder === undefined && settings) || update) {
            holder = new AutoNumericHolder($that.get(0), settings);
            data.holder = holder;
        }
        return holder;
    }
    var methods = {
        init: function(options) {
            return this.each(function() {
                var $this = $(this),
                    settings = $this.data('autoNumeric'),
                    tagData = $this.data();
                if (typeof settings !== 'object') {
                    var defaults = {
                        aNum: '0123456789',
                        aSep: ',',
                        dGroup: '3',
                        aDec: '.',
                        altDec: null,
                        aSign: '',
                        pSign: 'p',
                        vMax: '9999999999999.99',
                        vMin: '0.00',
                        mDec: null,
                        mRound: 'S',
                        aPad: true,
                        nBracket: null,
                        wEmpty: 'empty',
                        lZero: 'allow',
                        aForm: true,
                        onSomeEvent: function() {}
                    };
                    settings = $.extend({}, defaults, tagData, options);
                    if (settings.aDec === settings.aSep) {
                        $.error("autoNumeric will not function properly when the decimal character aDec: '" + settings.aDec + "' and thousand separator aSep: '" + settings.aSep + "' are the same character");
                        return this;
                    }
                    $this.data('autoNumeric', settings);
                } else {
                    return this;
                }
                settings.runOnce = false;
                var holder = getHolder($this, settings);
                if ($.inArray($this.prop('tagName'), settings.tagList) === -1 && $this.prop('tagName') !== 'INPUT') {
                    $.error("The <" + $this.prop('tagName') + "> is not supported by autoNumeric()");
                    return this;
                }
                if (settings.runOnce === false && settings.aForm) {
                    if ($this.is('input[type=text], input[type=hidden], input[type=tel], input:not([type])')) {
                        var setValue = true;
                        if ($this[0].value === '' && settings.wEmpty === 'empty') {
                            $this[0].value = '';
                            setValue = false;
                        }
                        if ($this[0].value === '' && settings.wEmpty === 'sign') {
                            $this[0].value = settings.aSign;
                            setValue = false;
                        }
                        if (setValue) {
                            $this.autoNumeric('set', $this.val());
                        }
                    }
                    if ($.inArray($this.prop('tagName'), settings.tagList) !== -1 && $this.text() !== '') {
                        $this.autoNumeric('set', $this.text());
                    }
                }
                settings.runOnce = true;
                if ($this.is('input[type=text], input[type=hidden], input[type=tel], input:not([type])')) {
                    $this.on('keydown.autoNumeric', function(e) {
                        holder = getHolder($this);
                        if (holder.settings.aDec === holder.settings.aSep) {
                            $.error("autoNumeric will not function properly when the decimal character aDec: '" + holder.settings.aDec + "' and thousand separator aSep: '" + holder.settings.aSep + "' are the same character");
                            return this;
                        }
                        if (holder.that.readOnly) {
                            holder.processed = true;
                            return true;
                        }
                        holder.init(e);
                        holder.settings.oEvent = 'keydown';
                        if (holder.skipAllways(e)) {
                            holder.processed = true;
                            return true;
                        }
                        if (holder.processAllways()) {
                            holder.processed = true;
                            holder.formatQuick();
                            e.preventDefault();
                            return false;
                        }
                        holder.formatted = false;
                        return true;
                    });
                    $this.on('keypress.autoNumeric', function(e) {
                        var holder = getHolder($this),
                            processed = holder.processed;
                        holder.init(e);
                        holder.settings.oEvent = 'keypress';
                        if (holder.skipAllways(e)) {
                            return true;
                        }
                        if (processed) {
                            e.preventDefault();
                            return false;
                        }
                        if (holder.processAllways() || holder.processKeypress()) {
                            holder.formatQuick();
                            e.preventDefault();
                            return false;
                        }
                        holder.formatted = false;
                    });
                    $this.on('keyup.autoNumeric', function(e) {
                        var holder = getHolder($this);
                        holder.init(e);
                        holder.settings.oEvent = 'keyup';
                        var skip = holder.skipAllways(e);
                        holder.kdCode = 0;
                        delete holder.valuePartsBeforePaste;
                        if ($this[0].value === holder.settings.aSign) {
                            if (holder.settings.pSign === 's') {
                                setElementSelection(this, 0, 0);
                            } else {
                                setElementSelection(this, holder.settings.aSign.length, holder.settings.aSign.length);
                            }
                        }
                        if (skip) {
                            return true;
                        }
                        if (this.value === '') {
                            return true;
                        }
                        if (!holder.formatted) {
                            holder.formatQuick();
                        }
                    });
                    $this.on('focusin.autoNumeric', function() {
                        var holder = getHolder($this);
                        holder.settingsClone.oEvent = 'focusin';
                        if (holder.settingsClone.nBracket !== null) {
                            var checkVal = $this.val();
                            $this.val(negativeBracket(checkVal, holder.settingsClone.nBracket, holder.settingsClone.oEvent));
                        }
                        holder.inVal = $this.val();
                        var onempty = checkEmpty(holder.inVal, holder.settingsClone, true);
                        if (onempty !== null) {
                            $this.val(onempty);
                            if (holder.settings.pSign === 's') {
                                setElementSelection(this, 0, 0);
                            } else {
                                setElementSelection(this, holder.settings.aSign.length, holder.settings.aSign.length);
                            }
                        }
                    });
                    $this.on('focusout.autoNumeric', function() {
                        var holder = getHolder($this),
                            settingsClone = holder.settingsClone,
                            value = $this.val(),
                            origValue = value;
                        holder.settingsClone.oEvent = 'focusout';
                        var strip_zero = '';
                        if (settingsClone.lZero === 'allow') {
                            settingsClone.allowLeading = false;
                            strip_zero = 'leading';
                        }
                        if (value !== '') {
                            value = autoStrip(value, settingsClone, strip_zero);
                            if (checkEmpty(value, settingsClone) === null && autoCheck(value, settingsClone, $this[0])) {
                                value = fixNumber(value, settingsClone.aDec, settingsClone.aNeg);
                                value = autoRound(value, settingsClone);
                                value = presentNumber(value, settingsClone.aDec, settingsClone.aNeg);
                            } else {
                                value = '';
                            }
                        }
                        var groupedValue = checkEmpty(value, settingsClone, false);
                        if (groupedValue === null) {
                            groupedValue = autoGroup(value, settingsClone);
                        }
                        if (groupedValue !== origValue) {
                            $this.val(groupedValue);
                        }
                        if (groupedValue !== holder.inVal) {
                            $this.change();
                            delete holder.inVal;
                        }
                        if (settingsClone.nBracket !== null && $this.autoNumeric('get') < 0) {
                            holder.settingsClone.oEvent = 'focusout';
                            $this.val(negativeBracket($this.val(), settingsClone.nBracket, settingsClone.oEvent));
                        }
                    });
                }
            });
        },
        destroy: function() {
            return $(this).each(function() {
                var $this = $(this);
                $this.off('.autoNumeric');
                $this.removeData('autoNumeric');
            });
        },
        update: function(options) {
            return $(this).each(function() {
                var $this = autoGet($(this)),
                    settings = $this.data('autoNumeric');
                if (typeof settings !== 'object') {
                    $.error("You must initialize autoNumeric('init', {options}) prior to calling the 'update' method");
                    return this;
                }
                var strip = $this.autoNumeric('get');
                settings = $.extend(settings, options);
                getHolder($this, settings, true);
                if (settings.aDec === settings.aSep) {
                    $.error("autoNumeric will not function properly when the decimal character aDec: '" + settings.aDec + "' and thousand separator aSep: '" + settings.aSep + "' are the same character");
                    return this;
                }
                $this.data('autoNumeric', settings);
                if ($this.val() !== '' || $this.text() !== '') {
                    return $this.autoNumeric('set', strip);
                }
                return;
            });
        },
        set: function(valueIn) {
            return $(this).each(function() {
                var $this = autoGet($(this)),
                    settings = $this.data('autoNumeric'),
                    value = (valueIn != undefined) ? valueIn.toString() : '',
                    testValue = (valueIn != undefined) ? valueIn.toString() : '';
                if (typeof settings !== 'object') {
                    $.error("You must initialize autoNumeric('init', {options}) prior to calling the 'set' method");
                    return this;
                }
                if (testValue !== $this.attr('value') && $this.prop('tagName') === 'INPUT' && settings.runOnce === false) {
                    value = (settings.nBracket !== null) ? negativeBracket($this.val(), settings.nBracket, 'pageLoad') : value;
                    value = autoStrip(value, settings);
                }
                if ((testValue === $this.attr('value') || testValue === $this.text()) && settings.runOnce === false) {
                    value = value.replace(',', '.');
                }
                if (!$.isNumeric(+value)) {
                    return '';
                }
                value = checkValue(value, settings);
                settings.oEvent = 'set';
                value.toString();
                if (value !== '') {
                    value = autoRound(value, settings);
                }
                value = presentNumber(value, settings.aDec, settings.aNeg);
                if (!autoCheck(value, settings)) {
                    value = autoRound('', settings);
                }
                value = autoGroup(value, settings);
                if ($this.is('input[type=text], input[type=hidden], input[type=tel], input:not([type])')) {
                    return $this.val(value);
                }
                if ($.inArray($this.prop('tagName'), settings.tagList) !== -1) {
                    return $this.text(value);
                }
                $.error("The <" + $this.prop('tagName') + "> is not supported by autoNumeric()");
                return false;
            });
        },
        get: function() {
            var $this = autoGet($(this)),
                settings = $this.data('autoNumeric');
            if (typeof settings !== 'object') {
                $.error("You must initialize autoNumeric('init', {options}) prior to calling the 'get' method");
                return this;
            }
            settings.oEvent = 'get';
            var getValue = '';
            if ($this.is('input[type=text], input[type=hidden], input[type=tel], input:not([type])')) {
                getValue = $this.eq(0).val();
            } else if ($.inArray($this.prop('tagName'), settings.tagList) !== -1) {
                getValue = $this.eq(0).text();
            } else {
                $.error("The <" + $this.prop('tagName') + "> is not supported by autoNumeric()");
                return false;
            }
            if ((getValue === '' && settings.wEmpty === 'empty') || (getValue === settings.aSign && (settings.wEmpty === 'sign' || settings.wEmpty === 'empty'))) {
                return '';
            }
            if (settings.nBracket !== null && getValue !== '') {
                getValue = negativeBracket(getValue, settings.nBracket, settings.oEvent);
            }
            if (settings.runOnce || settings.aForm === false) {
                getValue = autoStrip(getValue, settings);
            }
            getValue = fixNumber(getValue, settings.aDec, settings.aNeg);
            if (+getValue === 0 && settings.lZero !== 'keep') {
                getValue = '0';
            }
            if (settings.lZero === 'keep') {
                return getValue;
            }
            getValue = checkValue(getValue, settings);
            return getValue;
        },
        getString: function() {
            var isAutoNumeric = false,
                $this = autoGet($(this)),
                str = $this.serialize(),
                parts = str.split('&'),
                i = 0;
            for (i; i < parts.length; i += 1) {
                var miniParts = parts[i].split('=');
                var settings = $('*[name="' + decodeURIComponent(miniParts[0]) + '"]').data('autoNumeric');
                if (typeof settings === 'object') {
                    if (miniParts[1] !== null && $('*[name="' + decodeURIComponent(miniParts[0]) + '"]').data('autoNumeric') !== undefined) {
                        miniParts[1] = $('input[name="' + decodeURIComponent(miniParts[0]) + '"]').autoNumeric('get');
                        parts[i] = miniParts.join('=');
                        isAutoNumeric = true;
                    }
                }
            }
            if (isAutoNumeric === true) {
                return parts.join('&');
            }
            $.error("You must initialize autoNumeric('init', {options}) prior to calling the 'getString' method");
            return this;
        },
        getArray: function() {
            var isAutoNumeric = false,
                $this = autoGet($(this)),
                formFields = $this.serializeArray();
            $.each(formFields, function(i, field) {
                var settings = $('*[name="' + decodeURIComponent(field.name) + '"]').data('autoNumeric');
                if (typeof settings === 'object') {
                    if (field.value !== '' && $('*[name="' + decodeURIComponent(field.name) + '"]').data('autoNumeric') !== undefined) {
                        field.value = $('input[name="' + decodeURIComponent(field.name) + '"]').autoNumeric('get').toString();
                    }
                    isAutoNumeric = true;
                }
            });
            if (isAutoNumeric === true) {
                return formFields;
            }
            $.error("You must initialize autoNumeric('init', {options}) prior to calling the 'getArray' method");
            return this;
        },
        getSettings: function() {
            var $this = autoGet($(this));
            return $this.eq(0).data('autoNumeric');
        }
    };
    $.fn.autoNumeric = function(method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        }
        if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        }
        $.error('Method "' + method + '" is not supported by autoNumeric()');
    };
}(jQuery));

/*===============================
https://www.equipmentone.com/assetnation/js/e1menubar.js
================================================================================*/
;

function checkLogin(fromurl) {
    if (fromurl == 'search') {
        setCookie('IS_SEARCHPAGE', 'true', 1, false);
    } else {
        setCookie('IS_SEARCHPAGE', 'false', 1, false);
    }
    setCookie('E1_HARD_LOGIN_PERFORMED', 'true', 1, false);
    if ($('#j_rememberme').is(":checked") === true)
        setCookie('E1_REMEMBERME_SELECTED', 'true', 1, false);
}

function setCookie(c_name, value, exdays, isHttp) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    if (isHttp)
        document.cookie = c_name + "=" + c_value + "; HttpOnly";
    else
        document.cookie = c_name + "=" + c_value;
}

function ReadCookie(cookieName) {
    var theCookie = " " + document.cookie;
    var ind = theCookie.indexOf(" " + cookieName + "=");
    if (ind == -1) ind = theCookie.indexOf(";" + cookieName + "=");
    if (ind == -1 || cookieName == "") return "";
    var ind1 = theCookie.indexOf(";", ind + 1);
    if (ind1 == -1) ind1 = theCookie.length;
    return unescape(theCookie.substring(ind + cookieName.length + 2, ind1));
}

/*===============================
https://www.equipmentone.com/assetnation/js/ANUtils.js
================================================================================*/
;

function ANAjax() {}
ANAjax.exec = function(url, data) {
    if (data.data == undefined) {
        data.data = {};
    }
    data.data.rand = Math.random();
    data.data.timeout = 20000;
    $.ajax(url, data);
};
ANAjax.makeRestCall = function(mdata, successCallback, failureCallback) {
    var url = '/assetnation/controllers/RestController.php';
    var data = {
        type: 'POST',
        data: mdata,
        success: function(response) {
            var $response = $.parseJSON(response);
            var service = '';
            for (service in $response) {
                if ($response[service] != null) {
                    successCallback.call(this, service, $.parseJSON($response[service]));
                }
            }
        },
        failure: failureCallback
    };
    ANAjax.exec(url, data);
};
Function.prototype.subclass = function(base) {
    var c = Function.prototype.subclass.nonconstructor;
    c.prototype = base.prototype;
    this.prototype = new c();
};
Function.prototype.subclass.nonconstructor = function() {};

function ANUtils() {}
ANUtils.ERROR = -1;
ANUtils.parseAPIResponse = function($response, apiCall, alwaysSuppress, loginRequired) {
    if (loginRequired == undefined) {
        loginRequired = false;
    }
    if (alwaysSuppress == undefined) {
        alwaysSuppress = false;
    }
    if (typeof $response == 'string') {
        $response = $.parseJSON($response);
    }
    if ($response == false) {
        return ANUtils.ERROR;
    }
    if ($response.e1_status == 'success') {
        if ($response.e1_content == null || $response.e1_content == false) {
            return ANUtils.ERROR;
        }
        return $response.e1_content;
    } else {
        $('#error_information').append('<div data-api="' + apiCall + '"" data-error="' + $response.e1_error + '"" ></div>');
    }
    var displayError = true;
    if (alwaysSuppress == true) {
        displayError = false;
    }
    if (loginRequired && $response.e1_visitor == true) {
        window.location = '/login?timeout=true';
    }
    switch ($response.e1_error_code) {
        case 28:
            displayError = false;
            break;
        case 22:
            displayError = false;
            break;
    }
    if (displayError == true) {
        var config = {
            message: apiCall + ": " + $response.e1_error,
            id: apiCall,
            level: E1Alert.level.error
        };
        E1Alert.render(config);
    }
    return ANUtils.ERROR;
};
ANUtils.pulse = function(selector, params) {
    $(selector).effect("pulsate", params, 2000);
};
Number.prototype.formatMoney = function(c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "," : d,
        t = t == undefined ? "." : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
ANUtils.launchBidBox = function(lotId) {
    if (E1_MOBILE == 'true') {
        window.location.href = "/mobile-listing?view=bidbox&listingid=" + lotId;
    } else {
        window.location.href = "/listing?listingid=" + lotId;
    }
};
ANUtils.defaultImage = function(img) {
    img.onerror = "";
    img.src = '/images/na.png';
};
ANUtils.loadAddthisResource = function(jsfile) {
    $.getScript(jsfile).done(function(script, textStatus) {
        $('#shareArea').show();
        initAddThis();
    }).fail(function(jqxhr, settings, exception) {});
};
ANUtils.redirectToJoinNow = function() {
    window.location.href = "/registration";
};

/*===============================
https://www.equipmentone.com/e1_2/js/bootstrap-datetimepicker.js
================================================================================*/
;
/*! version : 4.7.14
 =========================================================
 bootstrap-datetimejs
 https://github.com/Eonasdan/bootstrap-datetimepicker
 Copyright (c) 2015 Jonathan Peterson
 =========================================================
 */
(function(factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['jquery', 'moment'], factory);
    } else if (typeof exports === 'object') {
        factory(require('jquery'), require('moment'));
    } else {
        if (typeof jQuery === 'undefined') {
            throw 'bootstrap-datetimepicker requires jQuery to be loaded first';
        }
        if (typeof moment === 'undefined') {
            throw 'bootstrap-datetimepicker requires Moment.js to be loaded first';
        }
        factory(jQuery, moment);
    }
}(function($, moment) {
    'use strict';
    if (!moment) {
        throw new Error('bootstrap-datetimepicker requires Moment.js to be loaded first');
    }
    var dateTimePicker = function(element, options) {
        var picker = {},
            date = moment().startOf('d'),
            viewDate = date.clone(),
            unset = true,
            input, component = false,
            widget = false,
            use24Hours, minViewModeNumber = 0,
            actualFormat, parseFormats, currentViewMode, datePickerModes = [{
                clsName: 'days',
                navFnc: 'M',
                navStep: 1
            }, {
                clsName: 'months',
                navFnc: 'y',
                navStep: 1
            }, {
                clsName: 'years',
                navFnc: 'y',
                navStep: 10
            }],
            viewModes = ['days', 'months', 'years'],
            verticalModes = ['top', 'bottom', 'auto'],
            horizontalModes = ['left', 'right', 'auto'],
            toolbarPlacements = ['default', 'top', 'bottom'],
            keyMap = {
                'up': 38,
                38: 'up',
                'down': 40,
                40: 'down',
                'left': 37,
                37: 'left',
                'right': 39,
                39: 'right',
                'tab': 9,
                9: 'tab',
                'escape': 27,
                27: 'escape',
                'enter': 13,
                13: 'enter',
                'pageUp': 33,
                33: 'pageUp',
                'pageDown': 34,
                34: 'pageDown',
                'shift': 16,
                16: 'shift',
                'control': 17,
                17: 'control',
                'space': 32,
                32: 'space',
                't': 84,
                84: 't',
                'delete': 46,
                46: 'delete'
            },
            keyState = {},
            isEnabled = function(granularity) {
                if (typeof granularity !== 'string' || granularity.length > 1) {
                    throw new TypeError('isEnabled expects a single character string parameter');
                }
                switch (granularity) {
                    case 'y':
                        return actualFormat.indexOf('Y') !== -1;
                    case 'M':
                        return actualFormat.indexOf('M') !== -1;
                    case 'd':
                        return actualFormat.toLowerCase().indexOf('d') !== -1;
                    case 'h':
                    case 'H':
                        return actualFormat.toLowerCase().indexOf('h') !== -1;
                    case 'm':
                        return actualFormat.indexOf('m') !== -1;
                    case 's':
                        return actualFormat.indexOf('s') !== -1;
                    default:
                        return false;
                }
            },
            hasTime = function() {
                return (isEnabled('h') || isEnabled('m') || isEnabled('s'));
            },
            hasDate = function() {
                return (isEnabled('y') || isEnabled('M') || isEnabled('d'));
            },
            getDatePickerTemplate = function() {
                var headTemplate = $('<thead>').append($('<tr>').append($('<th>').addClass('prev').attr('data-action', 'previous').append($('<span>').addClass(options.icons.previous))).append($('<th>').addClass('picker-switch').attr('data-action', 'pickerSwitch').attr('colspan', (options.calendarWeeks ? '6' : '5'))).append($('<th>').addClass('next').attr('data-action', 'next').append($('<span>').addClass(options.icons.next)))),
                    contTemplate = $('<tbody>').append($('<tr>').append($('<td>').attr('colspan', (options.calendarWeeks ? '8' : '7'))));
                return [$('<div>').addClass('datepicker-days').append($('<table>').addClass('table-condensed').append(headTemplate).append($('<tbody>'))), $('<div>').addClass('datepicker-months').append($('<table>').addClass('table-condensed').append(headTemplate.clone()).append(contTemplate.clone())), $('<div>').addClass('datepicker-years').append($('<table>').addClass('table-condensed').append(headTemplate.clone()).append(contTemplate.clone()))];
            },
            getTimePickerMainTemplate = function() {
                var topRow = $('<tr>'),
                    middleRow = $('<tr>'),
                    bottomRow = $('<tr>');
                if (isEnabled('h')) {
                    topRow.append($('<td>').append($('<a>').attr({
                        href: '#',
                        tabindex: '-1'
                    }).addClass('btn').attr('data-action', 'incrementHours').append($('<span>').addClass(options.icons.up))));
                    middleRow.append($('<td>').append($('<span>').addClass('timepicker-hour').attr('data-time-component', 'hours').attr('data-action', 'showHours')));
                    bottomRow.append($('<td>').append($('<a>').attr({
                        href: '#',
                        tabindex: '-1'
                    }).addClass('btn').attr('data-action', 'decrementHours').append($('<span>').addClass(options.icons.down))));
                }
                if (isEnabled('m')) {
                    if (isEnabled('h')) {
                        topRow.append($('<td>').addClass('separator'));
                        middleRow.append($('<td>').addClass('separator').html(':'));
                        bottomRow.append($('<td>').addClass('separator'));
                    }
                    topRow.append($('<td>').append($('<a>').attr({
                        href: '#',
                        tabindex: '-1'
                    }).addClass('btn').attr('data-action', 'incrementMinutes').append($('<span>').addClass(options.icons.up))));
                    middleRow.append($('<td>').append($('<span>').addClass('timepicker-minute').attr('data-time-component', 'minutes').attr('data-action', 'showMinutes')));
                    bottomRow.append($('<td>').append($('<a>').attr({
                        href: '#',
                        tabindex: '-1'
                    }).addClass('btn').attr('data-action', 'decrementMinutes').append($('<span>').addClass(options.icons.down))));
                }
                if (isEnabled('s')) {
                    if (isEnabled('m')) {
                        topRow.append($('<td>').addClass('separator'));
                        middleRow.append($('<td>').addClass('separator').html(':'));
                        bottomRow.append($('<td>').addClass('separator'));
                    }
                    topRow.append($('<td>').append($('<a>').attr({
                        href: '#',
                        tabindex: '-1'
                    }).addClass('btn').attr('data-action', 'incrementSeconds').append($('<span>').addClass(options.icons.up))));
                    middleRow.append($('<td>').append($('<span>').addClass('timepicker-second').attr('data-time-component', 'seconds').attr('data-action', 'showSeconds')));
                    bottomRow.append($('<td>').append($('<a>').attr({
                        href: '#',
                        tabindex: '-1'
                    }).addClass('btn').attr('data-action', 'decrementSeconds').append($('<span>').addClass(options.icons.down))));
                }
                if (!use24Hours) {
                    topRow.append($('<td>').addClass('separator'));
                    middleRow.append($('<td>').append($('<button>').addClass('btn btn-primary').attr('data-action', 'togglePeriod')));
                    bottomRow.append($('<td>').addClass('separator'));
                }
                return $('<div>').addClass('timepicker-picker').append($('<table>').addClass('table-condensed').append([topRow, middleRow, bottomRow]));
            },
            getTimePickerTemplate = function() {
                var hoursView = $('<div>').addClass('timepicker-hours').append($('<table>').addClass('table-condensed')),
                    minutesView = $('<div>').addClass('timepicker-minutes').append($('<table>').addClass('table-condensed')),
                    secondsView = $('<div>').addClass('timepicker-seconds').append($('<table>').addClass('table-condensed')),
                    ret = [getTimePickerMainTemplate()];
                if (isEnabled('h')) {
                    ret.push(hoursView);
                }
                if (isEnabled('m')) {
                    ret.push(minutesView);
                }
                if (isEnabled('s')) {
                    ret.push(secondsView);
                }
                return ret;
            },
            getToolbar = function() {
                var row = [];
                if (options.showTodayButton) {
                    row.push($('<td>').append($('<a>').attr('data-action', 'today').append($('<span>').addClass(options.icons.today))));
                }
                if (!options.sideBySide && hasDate() && hasTime()) {
                    row.push($('<td>').append($('<a>').attr('data-action', 'togglePicker').append($('<span>').addClass(options.icons.time))));
                }
                if (options.showClear) {
                    row.push($('<td>').append($('<a>').attr('data-action', 'clear').append($('<span>').addClass(options.icons.clear))));
                }
                if (options.showClose) {
                    row.push($('<td>').append($('<a>').attr('data-action', 'close').append($('<span>').addClass(options.icons.close))));
                }
                return $('<table>').addClass('table-condensed').append($('<tbody>').append($('<tr>').append(row)));
            },
            getTemplate = function() {
                var template = $('<div>').addClass('bootstrap-datetimepicker-widget dropdown-menu'),
                    dateView = $('<div>').addClass('datepicker').append(getDatePickerTemplate()),
                    timeView = $('<div>').addClass('timepicker').append(getTimePickerTemplate()),
                    content = $('<ul>').addClass('list-unstyled'),
                    toolbar = $('<li>').addClass('picker-switch' + (options.collapse ? ' accordion-toggle' : '')).append(getToolbar());
                if (options.inline) {
                    template.removeClass('dropdown-menu');
                }
                if (use24Hours) {
                    template.addClass('usetwentyfour');
                }
                if (options.sideBySide && hasDate() && hasTime()) {
                    template.addClass('timepicker-sbs');
                    template.append($('<div>').addClass('row').append(dateView.addClass('col-sm-6')).append(timeView.addClass('col-sm-6')));
                    template.append(toolbar);
                    return template;
                }
                if (options.toolbarPlacement === 'top') {
                    content.append(toolbar);
                }
                if (hasDate()) {
                    content.append($('<li>').addClass((options.collapse && hasTime() ? 'collapse in' : '')).append(dateView));
                }
                if (options.toolbarPlacement === 'default') {
                    content.append(toolbar);
                }
                if (hasTime()) {
                    content.append($('<li>').addClass((options.collapse && hasDate() ? 'collapse' : '')).append(timeView));
                }
                if (options.toolbarPlacement === 'bottom') {
                    content.append(toolbar);
                }
                return template.append(content);
            },
            dataToOptions = function() {
                var eData, dataOptions = {};
                if (element.is('input') || options.inline) {
                    eData = element.data();
                } else {
                    eData = element.find('input').data();
                }
                if (eData.dateOptions && eData.dateOptions instanceof Object) {
                    dataOptions = $.extend(true, dataOptions, eData.dateOptions);
                }
                $.each(options, function(key) {
                    var attributeName = 'date' + key.charAt(0).toUpperCase() + key.slice(1);
                    if (eData[attributeName] !== undefined) {
                        dataOptions[key] = eData[attributeName];
                    }
                });
                return dataOptions;
            },
            place = function() {
                var position = (component || element).position(),
                    offset = (component || element).offset(),
                    vertical = options.widgetPositioning.vertical,
                    horizontal = options.widgetPositioning.horizontal,
                    parent;
                if (options.widgetParent) {
                    parent = options.widgetParent.append(widget);
                } else if (element.is('input')) {
                    parent = element.parent().append(widget);
                } else if (options.inline) {
                    parent = element.append(widget);
                    return;
                } else {
                    parent = element;
                    element.children().first().after(widget);
                }
                if (vertical === 'auto') {
                    if (offset.top + widget.height() * 1.5 >= $(window).height() + $(window).scrollTop() && widget.height() + element.outerHeight() < offset.top) {
                        vertical = 'top';
                    } else {
                        vertical = 'bottom';
                    }
                }
                if (horizontal === 'auto') {
                    if (parent.width() < offset.left + widget.outerWidth() / 2 && offset.left + widget.outerWidth() > $(window).width()) {
                        horizontal = 'right';
                    } else {
                        horizontal = 'left';
                    }
                }
                if (vertical === 'top') {
                    widget.addClass('top').removeClass('bottom');
                } else {
                    widget.addClass('bottom').removeClass('top');
                }
                if (horizontal === 'right') {
                    widget.addClass('pull-right');
                } else {
                    widget.removeClass('pull-right');
                }
                if (parent.css('position') !== 'relative') {
                    parent = parent.parents().filter(function() {
                        return $(this).css('position') === 'relative';
                    }).first();
                }
                if (parent.length === 0) {
                    throw new Error('datetimepicker component should be placed within a relative positioned container');
                }
                widget.css({
                    top: vertical === 'top' ? 'auto' : position.top + element.outerHeight(),
                    bottom: vertical === 'top' ? position.top + element.outerHeight() : 'auto',
                    left: horizontal === 'left' ? parent.css('padding-left') : 'auto',
                    right: horizontal === 'left' ? 'auto' : parent.width() - element.outerWidth()
                });
            },
            notifyEvent = function(e) {
                if (e.type === 'dp.change' && ((e.date && e.date.isSame(e.oldDate)) || (!e.date && !e.oldDate))) {
                    return;
                }
                element.trigger(e);
            },
            showMode = function(dir) {
                if (!widget) {
                    return;
                }
                if (dir) {
                    currentViewMode = Math.max(minViewModeNumber, Math.min(2, currentViewMode + dir));
                }
                widget.find('.datepicker > div').hide().filter('.datepicker-' + datePickerModes[currentViewMode].clsName).show();
            },
            fillDow = function() {
                var row = $('<tr>'),
                    currentDate = viewDate.clone().startOf('w');
                if (options.calendarWeeks === true) {
                    row.append($('<th>').addClass('cw').text('#'));
                }
                while (currentDate.isBefore(viewDate.clone().endOf('w'))) {
                    row.append($('<th>').addClass('dow').text(currentDate.format('dd')));
                    currentDate.add(1, 'd');
                }
                widget.find('.datepicker-days thead').append(row);
            },
            isInDisabledDates = function(testDate) {
                return options.disabledDates[testDate.format('YYYY-MM-DD')] === true;
            },
            isInEnabledDates = function(testDate) {
                return options.enabledDates[testDate.format('YYYY-MM-DD')] === true;
            },
            isValid = function(targetMoment, granularity) {
                if (!targetMoment.isValid()) {
                    return false;
                }
                if (options.disabledDates && isInDisabledDates(targetMoment) && granularity !== 'M') {
                    return false;
                }
                if (options.enabledDates && !isInEnabledDates(targetMoment) && granularity !== 'M') {
                    return false;
                }
                if (options.minDate && targetMoment.isBefore(options.minDate, granularity)) {
                    return false;
                }
                if (options.maxDate && targetMoment.isAfter(options.maxDate, granularity)) {
                    return false;
                }
                if (granularity === 'd' && options.daysOfWeekDisabled.indexOf(targetMoment.day()) !== -1) {
                    return false;
                }
                return true;
            },
            fillMonths = function() {
                var spans = [],
                    monthsShort = viewDate.clone().startOf('y').hour(12);
                while (monthsShort.isSame(viewDate, 'y')) {
                    spans.push($('<span>').attr('data-action', 'selectMonth').addClass('month').text(monthsShort.format('MMM')));
                    monthsShort.add(1, 'M');
                }
                widget.find('.datepicker-months td').empty().append(spans);
            },
            updateMonths = function() {
                var monthsView = widget.find('.datepicker-months'),
                    monthsViewHeader = monthsView.find('th'),
                    months = monthsView.find('tbody').find('span');
                monthsView.find('.disabled').removeClass('disabled');
                if (!isValid(viewDate.clone().subtract(1, 'y'), 'y')) {
                    monthsViewHeader.eq(0).addClass('disabled');
                }
                monthsViewHeader.eq(1).text(viewDate.year());
                if (!isValid(viewDate.clone().add(1, 'y'), 'y')) {
                    monthsViewHeader.eq(2).addClass('disabled');
                }
                months.removeClass('active');
                if (date.isSame(viewDate, 'y')) {
                    months.eq(date.month()).addClass('active');
                }
                months.each(function(index) {
                    if (!isValid(viewDate.clone().month(index), 'M')) {
                        $(this).addClass('disabled');
                    }
                });
            },
            updateYears = function() {
                var yearsView = widget.find('.datepicker-years'),
                    yearsViewHeader = yearsView.find('th'),
                    startYear = viewDate.clone().subtract(5, 'y'),
                    endYear = viewDate.clone().add(6, 'y'),
                    html = '';
                yearsView.find('.disabled').removeClass('disabled');
                if (options.minDate && options.minDate.isAfter(startYear, 'y')) {
                    yearsViewHeader.eq(0).addClass('disabled');
                }
                yearsViewHeader.eq(1).text(startYear.year() + '-' + endYear.year());
                if (options.maxDate && options.maxDate.isBefore(endYear, 'y')) {
                    yearsViewHeader.eq(2).addClass('disabled');
                }
                while (!startYear.isAfter(endYear, 'y')) {
                    html += '<span data-action="selectYear" class="year' + (startYear.isSame(date, 'y') ? ' active' : '') + (!isValid(startYear, 'y') ? ' disabled' : '') + '">' + startYear.year() + '</span>';
                    startYear.add(1, 'y');
                }
                yearsView.find('td').html(html);
            },
            fillDate = function() {
                var daysView = widget.find('.datepicker-days'),
                    daysViewHeader = daysView.find('th'),
                    currentDate, html = [],
                    row, clsName;
                if (!hasDate()) {
                    return;
                }
                daysView.find('.disabled').removeClass('disabled');
                daysViewHeader.eq(1).text(viewDate.format(options.dayViewHeaderFormat));
                if (!isValid(viewDate.clone().subtract(1, 'M'), 'M')) {
                    daysViewHeader.eq(0).addClass('disabled');
                }
                if (!isValid(viewDate.clone().add(1, 'M'), 'M')) {
                    daysViewHeader.eq(2).addClass('disabled');
                }
                currentDate = viewDate.clone().startOf('M').startOf('week');
                while (!viewDate.clone().endOf('M').endOf('w').isBefore(currentDate, 'd')) {
                    if (currentDate.weekday() === 0) {
                        row = $('<tr>');
                        if (options.calendarWeeks) {
                            row.append('<td class="cw">' + currentDate.week() + '</td>');
                        }
                        html.push(row);
                    }
                    clsName = '';
                    if (currentDate.isBefore(viewDate, 'M')) {
                        clsName += ' old';
                    }
                    if (currentDate.isAfter(viewDate, 'M')) {
                        clsName += ' new';
                    }
                    if (currentDate.isSame(date, 'd') && !unset) {
                        clsName += ' active';
                    }
                    if (!isValid(currentDate, 'd')) {
                        clsName += ' disabled';
                    }
                    if (currentDate.isSame(moment(), 'd')) {
                        clsName += ' today';
                    }
                    if (currentDate.day() === 0 || currentDate.day() === 6) {
                        clsName += ' weekend';
                    }
                    row.append('<td data-action="selectDay" class="day' + clsName + '">' + currentDate.date() + '</td>');
                    currentDate.add(1, 'd');
                }
                daysView.find('tbody').empty().append(html);
                updateMonths();
                updateYears();
            },
            fillHours = function() {
                var table = widget.find('.timepicker-hours table'),
                    currentHour = viewDate.clone().startOf('d'),
                    html = [],
                    row = $('<tr>');
                if (viewDate.hour() > 11 && !use24Hours) {
                    currentHour.hour(12);
                }
                while (currentHour.isSame(viewDate, 'd') && (use24Hours || (viewDate.hour() < 12 && currentHour.hour() < 12) || viewDate.hour() > 11)) {
                    if (currentHour.hour() % 4 === 0) {
                        row = $('<tr>');
                        html.push(row);
                    }
                    row.append('<td data-action="selectHour" class="hour' + (!isValid(currentHour, 'h') ? ' disabled' : '') + '">' + currentHour.format(use24Hours ? 'HH' : 'hh') + '</td>');
                    currentHour.add(1, 'h');
                }
                table.empty().append(html);
            },
            fillMinutes = function() {
                var table = widget.find('.timepicker-minutes table'),
                    currentMinute = viewDate.clone().startOf('h'),
                    html = [],
                    row = $('<tr>'),
                    step = options.stepping === 1 ? 5 : options.stepping;
                while (viewDate.isSame(currentMinute, 'h')) {
                    if (currentMinute.minute() % (step * 4) === 0) {
                        row = $('<tr>');
                        html.push(row);
                    }
                    row.append('<td data-action="selectMinute" class="minute' + (!isValid(currentMinute, 'm') ? ' disabled' : '') + '">' + currentMinute.format('mm') + '</td>');
                    currentMinute.add(step, 'm');
                }
                table.empty().append(html);
            },
            fillSeconds = function() {
                var table = widget.find('.timepicker-seconds table'),
                    currentSecond = viewDate.clone().startOf('m'),
                    html = [],
                    row = $('<tr>');
                while (viewDate.isSame(currentSecond, 'm')) {
                    if (currentSecond.second() % 20 === 0) {
                        row = $('<tr>');
                        html.push(row);
                    }
                    row.append('<td data-action="selectSecond" class="second' + (!isValid(currentSecond, 's') ? ' disabled' : '') + '">' + currentSecond.format('ss') + '</td>');
                    currentSecond.add(5, 's');
                }
                table.empty().append(html);
            },
            fillTime = function() {
                var timeComponents = widget.find('.timepicker span[data-time-component]');
                if (!use24Hours) {
                    widget.find('.timepicker [data-action=togglePeriod]').text(date.format('A'));
                }
                timeComponents.filter('[data-time-component=hours]').text(date.format(use24Hours ? 'HH' : 'hh'));
                timeComponents.filter('[data-time-component=minutes]').text(date.format('mm'));
                timeComponents.filter('[data-time-component=seconds]').text(date.format('ss'));
                fillHours();
                fillMinutes();
                fillSeconds();
            },
            update = function() {
                if (!widget) {
                    return;
                }
                fillDate();
                fillTime();
            },
            setValue = function(targetMoment) {
                var oldDate = unset ? null : date;
                if (!targetMoment) {
                    unset = true;
                    input.val('');
                    element.data('date', '');
                    notifyEvent({
                        type: 'dp.change',
                        date: null,
                        oldDate: oldDate
                    });
                    update();
                    return;
                }
                targetMoment = targetMoment.clone().locale(options.locale);
                if (options.stepping !== 1) {
                    targetMoment.minutes((Math.round(targetMoment.minutes() / options.stepping) * options.stepping) % 60).seconds(0);
                }
                if (isValid(targetMoment)) {
                    date = targetMoment;
                    viewDate = date.clone();
                    input.val(date.format(actualFormat));
                    element.data('date', date.format(actualFormat));
                    update();
                    unset = false;
                    notifyEvent({
                        type: 'dp.change',
                        date: date.clone(),
                        oldDate: oldDate
                    });
                } else {
                    if (!options.keepInvalid) {
                        input.val(unset ? '' : date.format(actualFormat));
                    }
                    notifyEvent({
                        type: 'dp.error',
                        date: targetMoment
                    });
                }
            },
            hide = function() {
                var transitioning = false;
                if (!widget) {
                    return picker;
                }
                widget.find('.collapse').each(function() {
                    var collapseData = $(this).data('collapse');
                    if (collapseData && collapseData.transitioning) {
                        transitioning = true;
                        return false;
                    }
                    return true;
                });
                if (transitioning) {
                    return picker;
                }
                if (component && component.hasClass('btn')) {
                    component.toggleClass('active');
                }
                widget.hide();
                $(window).off('resize', place);
                widget.off('click', '[data-action]');
                widget.off('mousedown', false);
                widget.remove();
                widget = false;
                notifyEvent({
                    type: 'dp.hide',
                    date: date.clone()
                });
                return picker;
            },
            clear = function() {
                setValue(null);
            },
            actions = {
                next: function() {
                    viewDate.add(datePickerModes[currentViewMode].navStep, datePickerModes[currentViewMode].navFnc);
                    fillDate();
                },
                previous: function() {
                    viewDate.subtract(datePickerModes[currentViewMode].navStep, datePickerModes[currentViewMode].navFnc);
                    fillDate();
                },
                pickerSwitch: function() {
                    showMode(1);
                },
                selectMonth: function(e) {
                    var month = $(e.target).closest('tbody').find('span').index($(e.target));
                    viewDate.month(month);
                    if (currentViewMode === minViewModeNumber) {
                        setValue(date.clone().year(viewDate.year()).month(viewDate.month()));
                        if (!options.inline) {
                            hide();
                        }
                    } else {
                        showMode(-1);
                        fillDate();
                    }
                },
                selectYear: function(e) {
                    var year = parseInt($(e.target).text(), 10) || 0;
                    viewDate.year(year);
                    if (currentViewMode === minViewModeNumber) {
                        setValue(date.clone().year(viewDate.year()));
                        if (!options.inline) {
                            hide();
                        }
                    } else {
                        showMode(-1);
                        fillDate();
                    }
                },
                selectDay: function(e) {
                    var day = viewDate.clone();
                    if ($(e.target).is('.old')) {
                        day.subtract(1, 'M');
                    }
                    if ($(e.target).is('.new')) {
                        day.add(1, 'M');
                    }
                    setValue(day.date(parseInt($(e.target).text(), 10)));
                    if (!hasTime() && !options.keepOpen && !options.inline) {
                        hide();
                    }
                },
                incrementHours: function() {
                    setValue(date.clone().add(1, 'h'));
                },
                incrementMinutes: function() {
                    setValue(date.clone().add(options.stepping, 'm'));
                },
                incrementSeconds: function() {
                    setValue(date.clone().add(1, 's'));
                },
                decrementHours: function() {
                    setValue(date.clone().subtract(1, 'h'));
                },
                decrementMinutes: function() {
                    setValue(date.clone().subtract(options.stepping, 'm'));
                },
                decrementSeconds: function() {
                    setValue(date.clone().subtract(1, 's'));
                },
                togglePeriod: function() {
                    setValue(date.clone().add((date.hours() >= 12) ? -12 : 12, 'h'));
                },
                togglePicker: function(e) {
                    var $this = $(e.target),
                        $parent = $this.closest('ul'),
                        expanded = $parent.find('.in'),
                        closed = $parent.find('.collapse:not(.in)'),
                        collapseData;
                    if (expanded && expanded.length) {
                        collapseData = expanded.data('collapse');
                        if (collapseData && collapseData.transitioning) {
                            return;
                        }
                        if (expanded.collapse) {
                            expanded.collapse('hide');
                            closed.collapse('show');
                        } else {
                            expanded.removeClass('in');
                            closed.addClass('in');
                        }
                        if ($this.is('span')) {
                            $this.toggleClass(options.icons.time + ' ' + options.icons.date);
                        } else {
                            $this.find('span').toggleClass(options.icons.time + ' ' + options.icons.date);
                        }
                    }
                },
                showPicker: function() {
                    widget.find('.timepicker > div:not(.timepicker-picker)').hide();
                    widget.find('.timepicker .timepicker-picker').show();
                },
                showHours: function() {
                    widget.find('.timepicker .timepicker-picker').hide();
                    widget.find('.timepicker .timepicker-hours').show();
                },
                showMinutes: function() {
                    widget.find('.timepicker .timepicker-picker').hide();
                    widget.find('.timepicker .timepicker-minutes').show();
                },
                showSeconds: function() {
                    widget.find('.timepicker .timepicker-picker').hide();
                    widget.find('.timepicker .timepicker-seconds').show();
                },
                selectHour: function(e) {
                    var hour = parseInt($(e.target).text(), 10);
                    if (!use24Hours) {
                        if (date.hours() >= 12) {
                            if (hour !== 12) {
                                hour += 12;
                            }
                        } else {
                            if (hour === 12) {
                                hour = 0;
                            }
                        }
                    }
                    setValue(date.clone().hours(hour));
                    actions.showPicker.call(picker);
                },
                selectMinute: function(e) {
                    setValue(date.clone().minutes(parseInt($(e.target).text(), 10)));
                    actions.showPicker.call(picker);
                },
                selectSecond: function(e) {
                    setValue(date.clone().seconds(parseInt($(e.target).text(), 10)));
                    actions.showPicker.call(picker);
                },
                clear: clear,
                today: function() {
                    setValue(moment());
                },
                close: hide
            },
            doAction = function(e) {
                if ($(e.currentTarget).is('.disabled')) {
                    return false;
                }
                actions[$(e.currentTarget).data('action')].apply(picker, arguments);
                return false;
            },
            show = function() {
                var currentMoment, useCurrentGranularity = {
                    'year': function(m) {
                        return m.month(0).date(1).hours(0).seconds(0).minutes(0);
                    },
                    'month': function(m) {
                        return m.date(1).hours(0).seconds(0).minutes(0);
                    },
                    'day': function(m) {
                        return m.hours(0).seconds(0).minutes(0);
                    },
                    'hour': function(m) {
                        return m.seconds(0).minutes(0);
                    },
                    'minute': function(m) {
                        return m.seconds(0);
                    }
                };
                if (input.prop('disabled') || (!options.ignoreReadonly && input.prop('readonly')) || widget) {
                    return picker;
                }
                if (options.useCurrent && unset && ((input.is('input') && input.val().trim().length === 0) || options.inline)) {
                    currentMoment = moment();
                    if (typeof options.useCurrent === 'string') {
                        currentMoment = useCurrentGranularity[options.useCurrent](currentMoment);
                    }
                    setValue(currentMoment);
                }
                widget = getTemplate();
                fillDow();
                fillMonths();
                widget.find('.timepicker-hours').hide();
                widget.find('.timepicker-minutes').hide();
                widget.find('.timepicker-seconds').hide();
                update();
                showMode();
                $(window).on('resize', place);
                widget.on('click', '[data-action]', doAction);
                widget.on('mousedown', false);
                if (component && component.hasClass('btn')) {
                    component.toggleClass('active');
                }
                widget.show();
                place();
                if (!input.is(':focus')) {
                    input.focus();
                }
                notifyEvent({
                    type: 'dp.show'
                });
                return picker;
            },
            toggle = function() {
                return (widget ? hide() : show());
            },
            parseInputDate = function(inputDate) {
                if (moment.isMoment(inputDate) || inputDate instanceof Date) {
                    inputDate = moment(inputDate);
                } else {
                    inputDate = moment(inputDate, parseFormats, options.useStrict);
                }
                inputDate.locale(options.locale);
                return inputDate;
            },
            keydown = function(e) {
                var handler = null,
                    index, index2, pressedKeys = [],
                    pressedModifiers = {},
                    currentKey = e.which,
                    keyBindKeys, allModifiersPressed, pressed = 'p';
                keyState[currentKey] = pressed;
                for (index in keyState) {
                    if (keyState.hasOwnProperty(index) && keyState[index] === pressed) {
                        pressedKeys.push(index);
                        if (parseInt(index, 10) !== currentKey) {
                            pressedModifiers[index] = true;
                        }
                    }
                }
                for (index in options.keyBinds) {
                    if (options.keyBinds.hasOwnProperty(index) && typeof(options.keyBinds[index]) === 'function') {
                        keyBindKeys = index.split(' ');
                        if (keyBindKeys.length === pressedKeys.length && keyMap[currentKey] === keyBindKeys[keyBindKeys.length - 1]) {
                            allModifiersPressed = true;
                            for (index2 = keyBindKeys.length - 2; index2 >= 0; index2--) {
                                if (!(keyMap[keyBindKeys[index2]] in pressedModifiers)) {
                                    allModifiersPressed = false;
                                    break;
                                }
                            }
                            if (allModifiersPressed) {
                                handler = options.keyBinds[index];
                                break;
                            }
                        }
                    }
                }
                if (handler) {
                    handler.call(picker, widget);
                    e.stopPropagation();
                    e.preventDefault();
                }
            },
            keyup = function(e) {
                keyState[e.which] = 'r';
                e.stopPropagation();
                e.preventDefault();
            },
            change = function(e) {
                var val = $(e.target).val().trim(),
                    parsedDate = val ? parseInputDate(val) : null;
                setValue(parsedDate);
                e.stopImmediatePropagation();
                return false;
            },
            attachDatePickerElementEvents = function() {
                input.on({
                    'change': change,
                    'blur': options.debug ? '' : hide,
                    'keydown': keydown,
                    'keyup': keyup
                });
                if (element.is('input')) {
                    input.on({
                        'focus': show
                    });
                } else if (component) {
                    component.on('click', toggle);
                    component.on('mousedown', false);
                }
            },
            detachDatePickerElementEvents = function() {
                input.off({
                    'change': change,
                    'blur': hide,
                    'keydown': keydown,
                    'keyup': keyup
                });
                if (element.is('input')) {
                    input.off({
                        'focus': show
                    });
                } else if (component) {
                    component.off('click', toggle);
                    component.off('mousedown', false);
                }
            },
            indexGivenDates = function(givenDatesArray) {
                var givenDatesIndexed = {};
                $.each(givenDatesArray, function() {
                    var dDate = parseInputDate(this);
                    if (dDate.isValid()) {
                        givenDatesIndexed[dDate.format('YYYY-MM-DD')] = true;
                    }
                });
                return (Object.keys(givenDatesIndexed).length) ? givenDatesIndexed : false;
            },
            initFormatting = function() {
                var format = options.format || 'L LT';
                actualFormat = format.replace(/(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g, function(formatInput) {
                    var newinput = date.localeData().longDateFormat(formatInput) || formatInput;
                    return newinput.replace(/(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g, function(formatInput2) {
                        return date.localeData().longDateFormat(formatInput2) || formatInput2;
                    });
                });
                parseFormats = options.extraFormats ? options.extraFormats.slice() : [];
                if (parseFormats.indexOf(format) < 0 && parseFormats.indexOf(actualFormat) < 0) {
                    parseFormats.push(actualFormat);
                }
                use24Hours = (actualFormat.toLowerCase().indexOf('a') < 1 && actualFormat.indexOf('h') < 1);
                if (isEnabled('y')) {
                    minViewModeNumber = 2;
                }
                if (isEnabled('M')) {
                    minViewModeNumber = 1;
                }
                if (isEnabled('d')) {
                    minViewModeNumber = 0;
                }
                currentViewMode = Math.max(minViewModeNumber, currentViewMode);
                if (!unset) {
                    setValue(date);
                }
            };
        picker.destroy = function() {
            hide();
            detachDatePickerElementEvents();
            element.removeData('DateTimePicker');
            element.removeData('date');
        };
        picker.toggle = toggle;
        picker.show = show;
        picker.hide = hide;
        picker.disable = function() {
            hide();
            if (component && component.hasClass('btn')) {
                component.addClass('disabled');
            }
            input.prop('disabled', true);
            return picker;
        };
        picker.enable = function() {
            if (component && component.hasClass('btn')) {
                component.removeClass('disabled');
            }
            input.prop('disabled', false);
            return picker;
        };
        picker.ignoreReadonly = function(ignoreReadonly) {
            if (arguments.length === 0) {
                return options.ignoreReadonly;
            }
            if (typeof ignoreReadonly !== 'boolean') {
                throw new TypeError('ignoreReadonly () expects a boolean parameter');
            }
            options.ignoreReadonly = ignoreReadonly;
            return picker;
        };
        picker.options = function(newOptions) {
            if (arguments.length === 0) {
                return $.extend(true, {}, options);
            }
            if (!(newOptions instanceof Object)) {
                throw new TypeError('options() options parameter should be an object');
            }
            $.extend(true, options, newOptions);
            $.each(options, function(key, value) {
                if (picker[key] !== undefined) {
                    picker[key](value);
                } else {
                    throw new TypeError('option ' + key + ' is not recognized!');
                }
            });
            return picker;
        };
        picker.date = function(newDate) {
            if (arguments.length === 0) {
                if (unset) {
                    return null;
                }
                return date.clone();
            }
            if (newDate !== null && typeof newDate !== 'string' && !moment.isMoment(newDate) && !(newDate instanceof Date)) {
                throw new TypeError('date() parameter must be one of [null, string, moment or Date]');
            }
            setValue(newDate === null ? null : parseInputDate(newDate));
            return picker;
        };
        picker.format = function(newFormat) {
            if (arguments.length === 0) {
                return options.format;
            }
            if ((typeof newFormat !== 'string') && ((typeof newFormat !== 'boolean') || (newFormat !== false))) {
                throw new TypeError('format() expects a sting or boolean:false parameter ' + newFormat);
            }
            options.format = newFormat;
            if (actualFormat) {
                initFormatting();
            }
            return picker;
        };
        picker.dayViewHeaderFormat = function(newFormat) {
            if (arguments.length === 0) {
                return options.dayViewHeaderFormat;
            }
            if (typeof newFormat !== 'string') {
                throw new TypeError('dayViewHeaderFormat() expects a string parameter');
            }
            options.dayViewHeaderFormat = newFormat;
            return picker;
        };
        picker.extraFormats = function(formats) {
            if (arguments.length === 0) {
                return options.extraFormats;
            }
            if (formats !== false && !(formats instanceof Array)) {
                throw new TypeError('extraFormats() expects an array or false parameter');
            }
            options.extraFormats = formats;
            if (parseFormats) {
                initFormatting();
            }
            return picker;
        };
        picker.disabledDates = function(dates) {
            if (arguments.length === 0) {
                return (options.disabledDates ? $.extend({}, options.disabledDates) : options.disabledDates);
            }
            if (!dates) {
                options.disabledDates = false;
                update();
                return picker;
            }
            if (!(dates instanceof Array)) {
                throw new TypeError('disabledDates() expects an array parameter');
            }
            options.disabledDates = indexGivenDates(dates);
            options.enabledDates = false;
            update();
            return picker;
        };
        picker.enabledDates = function(dates) {
            if (arguments.length === 0) {
                return (options.enabledDates ? $.extend({}, options.enabledDates) : options.enabledDates);
            }
            if (!dates) {
                options.enabledDates = false;
                update();
                return picker;
            }
            if (!(dates instanceof Array)) {
                throw new TypeError('enabledDates() expects an array parameter');
            }
            options.enabledDates = indexGivenDates(dates);
            options.disabledDates = false;
            update();
            return picker;
        };
        picker.daysOfWeekDisabled = function(daysOfWeekDisabled) {
            if (arguments.length === 0) {
                return options.daysOfWeekDisabled.splice(0);
            }
            if (!(daysOfWeekDisabled instanceof Array)) {
                throw new TypeError('daysOfWeekDisabled() expects an array parameter');
            }
            options.daysOfWeekDisabled = daysOfWeekDisabled.reduce(function(previousValue, currentValue) {
                currentValue = parseInt(currentValue, 10);
                if (currentValue > 6 || currentValue < 0 || isNaN(currentValue)) {
                    return previousValue;
                }
                if (previousValue.indexOf(currentValue) === -1) {
                    previousValue.push(currentValue);
                }
                return previousValue;
            }, []).sort();
            update();
            return picker;
        };
        picker.maxDate = function(maxDate) {
            if (arguments.length === 0) {
                return options.maxDate ? options.maxDate.clone() : options.maxDate;
            }
            if ((typeof maxDate === 'boolean') && maxDate === false) {
                options.maxDate = false;
                update();
                return picker;
            }
            if (typeof maxDate === 'string') {
                if (maxDate === 'now' || maxDate === 'moment') {
                    maxDate = moment();
                }
            }
            var parsedDate = parseInputDate(maxDate);
            if (!parsedDate.isValid()) {
                throw new TypeError('maxDate() Could not parse date parameter: ' + maxDate);
            }
            if (options.minDate && parsedDate.isBefore(options.minDate)) {
                throw new TypeError('maxDate() date parameter is before options.minDate: ' + parsedDate.format(actualFormat));
            }
            options.maxDate = parsedDate;
            if (options.maxDate.isBefore(maxDate)) {
                setValue(options.maxDate);
            }
            if (viewDate.isAfter(parsedDate)) {
                viewDate = parsedDate.clone();
            }
            update();
            return picker;
        };
        picker.minDate = function(minDate) {
            if (arguments.length === 0) {
                return options.minDate ? options.minDate.clone() : options.minDate;
            }
            if ((typeof minDate === 'boolean') && minDate === false) {
                options.minDate = false;
                update();
                return picker;
            }
            if (typeof minDate === 'string') {
                if (minDate === 'now' || minDate === 'moment') {
                    minDate = moment();
                }
            }
            var parsedDate = parseInputDate(minDate);
            if (!parsedDate.isValid()) {
                throw new TypeError('minDate() Could not parse date parameter: ' + minDate);
            }
            if (options.maxDate && parsedDate.isAfter(options.maxDate)) {
                throw new TypeError('minDate() date parameter is after options.maxDate: ' + parsedDate.format(actualFormat));
            }
            options.minDate = parsedDate;
            if (options.minDate.isAfter(minDate)) {
                setValue(options.minDate);
            }
            if (viewDate.isBefore(parsedDate)) {
                viewDate = parsedDate.clone();
            }
            update();
            return picker;
        };
        picker.defaultDate = function(defaultDate) {
            if (arguments.length === 0) {
                return options.defaultDate ? options.defaultDate.clone() : options.defaultDate;
            }
            if (!defaultDate) {
                options.defaultDate = false;
                return picker;
            }
            if (typeof defaultDate === 'string') {
                if (defaultDate === 'now' || defaultDate === 'moment') {
                    defaultDate = moment();
                }
            }
            var parsedDate = parseInputDate(defaultDate);
            if (!parsedDate.isValid()) {
                throw new TypeError('defaultDate() Could not parse date parameter: ' + defaultDate);
            }
            if (!isValid(parsedDate)) {
                throw new TypeError('defaultDate() date passed is invalid according to component setup validations');
            }
            options.defaultDate = parsedDate;
            if (options.defaultDate && input.val().trim() === '' && input.attr('placeholder') === undefined) {
                setValue(options.defaultDate);
            }
            return picker;
        };
        picker.locale = function(locale) {
            if (arguments.length === 0) {
                return options.locale;
            }
            if (!moment.localeData(locale)) {
                throw new TypeError('locale() locale ' + locale + ' is not loaded from moment locales!');
            }
            options.locale = locale;
            date.locale(options.locale);
            viewDate.locale(options.locale);
            if (actualFormat) {
                initFormatting();
            }
            if (widget) {
                hide();
                show();
            }
            return picker;
        };
        picker.stepping = function(stepping) {
            if (arguments.length === 0) {
                return options.stepping;
            }
            stepping = parseInt(stepping, 10);
            if (isNaN(stepping) || stepping < 1) {
                stepping = 1;
            }
            options.stepping = stepping;
            return picker;
        };
        picker.useCurrent = function(useCurrent) {
            var useCurrentOptions = ['year', 'month', 'day', 'hour', 'minute'];
            if (arguments.length === 0) {
                return options.useCurrent;
            }
            if ((typeof useCurrent !== 'boolean') && (typeof useCurrent !== 'string')) {
                throw new TypeError('useCurrent() expects a boolean or string parameter');
            }
            if (typeof useCurrent === 'string' && useCurrentOptions.indexOf(useCurrent.toLowerCase()) === -1) {
                throw new TypeError('useCurrent() expects a string parameter of ' + useCurrentOptions.join(', '));
            }
            options.useCurrent = useCurrent;
            return picker;
        };
        picker.collapse = function(collapse) {
            if (arguments.length === 0) {
                return options.collapse;
            }
            if (typeof collapse !== 'boolean') {
                throw new TypeError('collapse() expects a boolean parameter');
            }
            if (options.collapse === collapse) {
                return picker;
            }
            options.collapse = collapse;
            if (widget) {
                hide();
                show();
            }
            return picker;
        };
        picker.icons = function(icons) {
            if (arguments.length === 0) {
                return $.extend({}, options.icons);
            }
            if (!(icons instanceof Object)) {
                throw new TypeError('icons() expects parameter to be an Object');
            }
            $.extend(options.icons, icons);
            if (widget) {
                hide();
                show();
            }
            return picker;
        };
        picker.useStrict = function(useStrict) {
            if (arguments.length === 0) {
                return options.useStrict;
            }
            if (typeof useStrict !== 'boolean') {
                throw new TypeError('useStrict() expects a boolean parameter');
            }
            options.useStrict = useStrict;
            return picker;
        };
        picker.sideBySide = function(sideBySide) {
            if (arguments.length === 0) {
                return options.sideBySide;
            }
            if (typeof sideBySide !== 'boolean') {
                throw new TypeError('sideBySide() expects a boolean parameter');
            }
            options.sideBySide = sideBySide;
            if (widget) {
                hide();
                show();
            }
            return picker;
        };
        picker.viewMode = function(viewMode) {
            if (arguments.length === 0) {
                return options.viewMode;
            }
            if (typeof viewMode !== 'string') {
                throw new TypeError('viewMode() expects a string parameter');
            }
            if (viewModes.indexOf(viewMode) === -1) {
                throw new TypeError('viewMode() parameter must be one of (' + viewModes.join(', ') + ') value');
            }
            options.viewMode = viewMode;
            currentViewMode = Math.max(viewModes.indexOf(viewMode), minViewModeNumber);
            showMode();
            return picker;
        };
        picker.toolbarPlacement = function(toolbarPlacement) {
            if (arguments.length === 0) {
                return options.toolbarPlacement;
            }
            if (typeof toolbarPlacement !== 'string') {
                throw new TypeError('toolbarPlacement() expects a string parameter');
            }
            if (toolbarPlacements.indexOf(toolbarPlacement) === -1) {
                throw new TypeError('toolbarPlacement() parameter must be one of (' + toolbarPlacements.join(', ') + ') value');
            }
            options.toolbarPlacement = toolbarPlacement;
            if (widget) {
                hide();
                show();
            }
            return picker;
        };
        picker.widgetPositioning = function(widgetPositioning) {
            if (arguments.length === 0) {
                return $.extend({}, options.widgetPositioning);
            }
            if (({}).toString.call(widgetPositioning) !== '[object Object]') {
                throw new TypeError('widgetPositioning() expects an object variable');
            }
            if (widgetPositioning.horizontal) {
                if (typeof widgetPositioning.horizontal !== 'string') {
                    throw new TypeError('widgetPositioning() horizontal variable must be a string');
                }
                widgetPositioning.horizontal = widgetPositioning.horizontal.toLowerCase();
                if (horizontalModes.indexOf(widgetPositioning.horizontal) === -1) {
                    throw new TypeError('widgetPositioning() expects horizontal parameter to be one of (' + horizontalModes.join(', ') + ')');
                }
                options.widgetPositioning.horizontal = widgetPositioning.horizontal;
            }
            if (widgetPositioning.vertical) {
                if (typeof widgetPositioning.vertical !== 'string') {
                    throw new TypeError('widgetPositioning() vertical variable must be a string');
                }
                widgetPositioning.vertical = widgetPositioning.vertical.toLowerCase();
                if (verticalModes.indexOf(widgetPositioning.vertical) === -1) {
                    throw new TypeError('widgetPositioning() expects vertical parameter to be one of (' + verticalModes.join(', ') + ')');
                }
                options.widgetPositioning.vertical = widgetPositioning.vertical;
            }
            update();
            return picker;
        };
        picker.calendarWeeks = function(calendarWeeks) {
            if (arguments.length === 0) {
                return options.calendarWeeks;
            }
            if (typeof calendarWeeks !== 'boolean') {
                throw new TypeError('calendarWeeks() expects parameter to be a boolean value');
            }
            options.calendarWeeks = calendarWeeks;
            update();
            return picker;
        };
        picker.showTodayButton = function(showTodayButton) {
            if (arguments.length === 0) {
                return options.showTodayButton;
            }
            if (typeof showTodayButton !== 'boolean') {
                throw new TypeError('showTodayButton() expects a boolean parameter');
            }
            options.showTodayButton = showTodayButton;
            if (widget) {
                hide();
                show();
            }
            return picker;
        };
        picker.showClear = function(showClear) {
            if (arguments.length === 0) {
                return options.showClear;
            }
            if (typeof showClear !== 'boolean') {
                throw new TypeError('showClear() expects a boolean parameter');
            }
            options.showClear = showClear;
            if (widget) {
                hide();
                show();
            }
            return picker;
        };
        picker.widgetParent = function(widgetParent) {
            if (arguments.length === 0) {
                return options.widgetParent;
            }
            if (typeof widgetParent === 'string') {
                widgetParent = $(widgetParent);
            }
            if (widgetParent !== null && (typeof widgetParent !== 'string' && !(widgetParent instanceof $))) {
                throw new TypeError('widgetParent() expects a string or a jQuery object parameter');
            }
            options.widgetParent = widgetParent;
            if (widget) {
                hide();
                show();
            }
            return picker;
        };
        picker.keepOpen = function(keepOpen) {
            if (arguments.length === 0) {
                return options.keepOpen;
            }
            if (typeof keepOpen !== 'boolean') {
                throw new TypeError('keepOpen() expects a boolean parameter');
            }
            options.keepOpen = keepOpen;
            return picker;
        };
        picker.inline = function(inline) {
            if (arguments.length === 0) {
                return options.inline;
            }
            if (typeof inline !== 'boolean') {
                throw new TypeError('inline() expects a boolean parameter');
            }
            options.inline = inline;
            return picker;
        };
        picker.clear = function() {
            clear();
            return picker;
        };
        picker.keyBinds = function(keyBinds) {
            options.keyBinds = keyBinds;
            return picker;
        };
        picker.debug = function(debug) {
            if (typeof debug !== 'boolean') {
                throw new TypeError('debug() expects a boolean parameter');
            }
            options.debug = debug;
            return picker;
        };
        picker.showClose = function(showClose) {
            if (arguments.length === 0) {
                return options.showClose;
            }
            if (typeof showClose !== 'boolean') {
                throw new TypeError('showClose() expects a boolean parameter');
            }
            options.showClose = showClose;
            return picker;
        };
        picker.keepInvalid = function(keepInvalid) {
            if (arguments.length === 0) {
                return options.keepInvalid;
            }
            if (typeof keepInvalid !== 'boolean') {
                throw new TypeError('keepInvalid() expects a boolean parameter');
            }
            options.keepInvalid = keepInvalid;
            return picker;
        };
        picker.datepickerInput = function(datepickerInput) {
            if (arguments.length === 0) {
                return options.datepickerInput;
            }
            if (typeof datepickerInput !== 'string') {
                throw new TypeError('datepickerInput() expects a string parameter');
            }
            options.datepickerInput = datepickerInput;
            return picker;
        };
        if (element.is('input')) {
            input = element;
        } else {
            input = element.find(options.datepickerInput);
            if (input.size() === 0) {
                input = element.find('input');
            } else if (!input.is('input')) {
                throw new Error('CSS class "' + options.datepickerInput + '" cannot be applied to non input element');
            }
        }
        if (element.hasClass('input-group')) {
            if (element.find('.datepickerbutton').size() === 0) {
                component = element.find('[class^="input-group-"]');
            } else {
                component = element.find('.datepickerbutton');
            }
        }
        if (!options.inline && !input.is('input')) {
            throw new Error('Could not initialize DateTimePicker without an input element');
        }
        $.extend(true, options, dataToOptions());
        picker.options(options);
        initFormatting();
        attachDatePickerElementEvents();
        if (input.prop('disabled')) {
            picker.disable();
        }
        if (input.is('input') && input.val().trim().length !== 0) {
            setValue(parseInputDate(input.val().trim()));
        } else if (options.defaultDate && input.attr('placeholder') === undefined) {
            setValue(options.defaultDate);
        }
        if (options.inline) {
            show();
        }
        return picker;
    };
    $.fn.datetimepicker = function(options) {
        return this.each(function() {
            var $this = $(this);
            if (!$this.data('DateTimePicker')) {
                options = $.extend(true, {}, $.fn.datetimepicker.defaults, options);
                $this.data('DateTimePicker', dateTimePicker($this, options));
            }
        });
    };
    $.fn.datetimepicker.defaults = {
        format: false,
        dayViewHeaderFormat: 'MMMM YYYY',
        extraFormats: false,
        stepping: 1,
        minDate: false,
        maxDate: false,
        useCurrent: true,
        collapse: true,
        locale: moment.locale(),
        defaultDate: false,
        disabledDates: false,
        enabledDates: false,
        icons: {
            time: 'glyphicon glyphicon-time',
            date: 'glyphicon glyphicon-calendar',
            up: 'glyphicon glyphicon-chevron-up',
            down: 'glyphicon glyphicon-chevron-down',
            previous: 'glyphicon glyphicon-chevron-left',
            next: 'glyphicon glyphicon-chevron-right',
            today: 'glyphicon glyphicon-screenshot',
            clear: 'glyphicon glyphicon-trash',
            close: 'glyphicon glyphicon-remove'
        },
        useStrict: false,
        sideBySide: false,
        daysOfWeekDisabled: [],
        calendarWeeks: false,
        viewMode: 'days',
        toolbarPlacement: 'default',
        showTodayButton: false,
        showClear: false,
        showClose: false,
        widgetPositioning: {
            horizontal: 'auto',
            vertical: 'auto'
        },
        widgetParent: null,
        ignoreReadonly: false,
        keepOpen: false,
        inline: false,
        keepInvalid: false,
        datepickerInput: '.datepickerinput',
        keyBinds: {
            up: function(widget) {
                if (!widget) {
                    return;
                }
                var d = this.date() || moment();
                if (widget.find('.datepicker').is(':visible')) {
                    this.date(d.clone().subtract(7, 'd'));
                } else {
                    this.date(d.clone().add(1, 'm'));
                }
            },
            down: function(widget) {
                if (!widget) {
                    this.show();
                    return;
                }
                var d = this.date() || moment();
                if (widget.find('.datepicker').is(':visible')) {
                    this.date(d.clone().add(7, 'd'));
                } else {
                    this.date(d.clone().subtract(1, 'm'));
                }
            },
            'control up': function(widget) {
                if (!widget) {
                    return;
                }
                var d = this.date() || moment();
                if (widget.find('.datepicker').is(':visible')) {
                    this.date(d.clone().subtract(1, 'y'));
                } else {
                    this.date(d.clone().add(1, 'h'));
                }
            },
            'control down': function(widget) {
                if (!widget) {
                    return;
                }
                var d = this.date() || moment();
                if (widget.find('.datepicker').is(':visible')) {
                    this.date(d.clone().add(1, 'y'));
                } else {
                    this.date(d.clone().subtract(1, 'h'));
                }
            },
            left: function(widget) {
                if (!widget) {
                    return;
                }
                var d = this.date() || moment();
                if (widget.find('.datepicker').is(':visible')) {
                    this.date(d.clone().subtract(1, 'd'));
                }
            },
            right: function(widget) {
                if (!widget) {
                    return;
                }
                var d = this.date() || moment();
                if (widget.find('.datepicker').is(':visible')) {
                    this.date(d.clone().add(1, 'd'));
                }
            },
            pageUp: function(widget) {
                if (!widget) {
                    return;
                }
                var d = this.date() || moment();
                if (widget.find('.datepicker').is(':visible')) {
                    this.date(d.clone().subtract(1, 'M'));
                }
            },
            pageDown: function(widget) {
                if (!widget) {
                    return;
                }
                var d = this.date() || moment();
                if (widget.find('.datepicker').is(':visible')) {
                    this.date(d.clone().add(1, 'M'));
                }
            },
            enter: function() {
                this.hide();
            },
            escape: function() {
                this.hide();
            },
            'control space': function(widget) {
                if (widget.find('.timepicker').is(':visible')) {
                    widget.find('.btn[data-action="togglePeriod"]').click();
                }
            },
            t: function() {
                this.date(moment());
            },
            'delete': function() {
                this.clear();
            }
        },
        debug: false
    };
}));

/*===============================
https://www.equipmentone.com/e1_2/js/toolbarUtils.js
================================================================================*/
;
$(window).on('load', function() {
    fncUpdateToolbarCounts();
});

function fncUpdateToolbarCounts() {
    if (E1_USERID == '') {
        return;
    }
    var params = {};
    params.data = {};
    params.data.curl = '/myone/:userID/counts/venueID/:venueID';
    params.url = '/home?task=ajax';;
    params.success = fncRenderToolbarCounts;
    fncAjax(params);
}

function fncRenderToolbarCounts(response) {
    $toolbar = $('#e1toolbar');
    var params = {};
    params.source = 'fncRenderListings';
    response = fncAjaxResponse(response, params);
    intBuyingCount = 0;
    intSellingCount = 0;
    intWatchingCount = 0;
    var buyingArray = VENUE['toolBarBuyingCountList'];
    var sellingArray = ['COUNTSELLERQUESTIONS', 'COUNTAPPROVEOFFERS', 'COUNTMYSALES', 'COUNTPAYMENTSTATUS', 'COUNTREMOVALSTATUS', 'COUNTDRAFTS', 'COUNTPENDING', 'COUNTPENDINGINFONOTAWAITINGSELLER', 'COUNTPENDINGSELLERAPPROVAL'];
    var watchingArray = ['COUNTWATCHING', 'COUNTPRIVATESALES'];
    $.each(buyingArray, function(index, value) {
        intBuyingCount += response[value];
    });
    $.each(sellingArray, function(index, value) {
        intSellingCount += response[value];
    });
    $.each(watchingArray, function(index, value) {
        intWatchingCount += response[value];
    });
    if (intBuyingCount > 0) {
        $toolbar.find('#toolbar_buy_count > .toolbar_cnt_txt').html(intBuyingCount);
        $toolbar.find('.tb-btn-buying').removeClass('e1Hidden');
    } else {
        $toolbar.find('.tb-btn-buying').addClass('e1Hidden');
    }
    if (intSellingCount > 0) {
        $toolbar.find('#toolbar_sel_count > .toolbar_cnt_txt').html(intSellingCount);
        $toolbar.find('.tb-btn-selling').removeClass('e1Hidden');
    } else {
        $toolbar.find('.tb-btn-selling').addClass('e1Hidden');
    }
    if (intWatchingCount > 0) {
        $toolbar.find('#toolbar_watch_count > .toolbar_cnt_txt').html(intWatchingCount);
        $toolbar.find('.tb-btn-watchlist').removeClass('e1Hidden');
    } else {
        $toolbar.find('.tb-btn-watchlist').addClass('e1Hidden');
    }
    if (response['COUNTSAVEDSEARCHES'] > 0) {
        $toolbar.find('#toolbar_search_count > .toolbar_cnt_txt').html(response['COUNTSAVEDSEARCHES']);
        $toolbar.find('.tb-btn-search').removeClass('e1Hidden');
    } else {
        $toolbar.find('.tb-btn-search').addClass('e1Hidden');
    }
}

/*===============================
https://www.equipmentone.com/e1_2/js/UserListingsUtils.js
================================================================================*/
;

function UserListings() {}
var noteListingData = [];
UserListings.fncInitUserListingData = function() {
    var lastUpdteTime = localStorage.getItem("e1User_lastUpdate");
    var timeNow = Math.floor(Date.now() / 1000);
    if (loggedIn == true) {
        UserListings.fncInitWatchItems();
        UserListings.fncInitNotetems();
        if (localStorage.getItem("e1User_lastUpdate") == null || timeNow - localStorage.getItem("e1User_lastUpdate") > 900) {
            UserListings.fncGetUserListingsData();
        } else {
            UserListings.fncGetUserListingsDataFromLocalStorage();
            UserListings.fncGetUserNotesDataFromLocalStorage();
            UserListings.fncGetUserActiveBidListsFromLocalStorage();
        }
    }
};
UserListings.fncGetMyMaxBid = function(listingId) {
    if (localStorage.getItem("e1User_lastUpdate") == null) {
        UserListings.fncGetUserListingsData();
    }
    var userActiveListings = localStorage.getItem("e1User_activeListings");
    userActiveListings = JSON.parse(userActiveListings);
    var myMaxBid = 0;
    $.each(userActiveListings, function(index, list) {
        if (list.LISTINGID == listingId) {
            myMaxBid = list.MYMAXBID;
        }
    });
    return myMaxBid;
};
UserListings.fncInitWatchItems = function() {
    $('.listing-item').each(function() {
        if ($(this).attr('id') != undefined) {
            var $listing = $(this).attr('id');
            var arrListing = $listing.split('-');
            var params = {};
            var watchListName = VENUE['lotNumberPrefix'];
            params.LISTINGID = arrListing[1];
            var $listing = $('#listing-' + arrListing[1]);
            if ($listing.find('.jsLotIDContainer').css('display') != 'none') {
                var strLotId = $listing.find('.jsLotID').html();
                var strLotType = strLotId.substring(0, 2);
                if (strLotType.toUpperCase() == watchListName) {
                    $listing.find('.jsWatchlistToggle').show();
                    fncDisplayAddToWatchlist($listing, params);
                }
            }
        }
        $('.jsWatchMsg').hide();
    });
};
UserListings.fncGetUserListingsDataFromLocalStorage = function() {
    var userWatchedListings = localStorage.getItem("e1User_watchlist");
    if (userWatchedListings != null) {
        userWatchedListings = uniqueArr(userWatchedListings.split(','));
        UserListings.fncUpdateWatchedStatus(userWatchedListings);
    }
};
UserListings.fncGetUserNotesDataFromLocalStorage = function() {
    var userNoteListings = localStorage.getItem("e1User_noteListings");
    if (userNoteListings != null) {
        userNoteListings = uniqueArr(userNoteListings.split(','));
        UserListings.fncUpdateNote(userNoteListings);
    }
};
UserListings.fncGetUserActiveBidListsFromLocalStorage = function() {
    var userActiveListings = localStorage.getItem("e1User_activeListings");
    if (userActiveListings != null) {
        userActiveListings = JSON.parse(userActiveListings);
        UserListings.fncUpdateBuyingActive(userActiveListings);
    }
};
UserListings.fncInitNotetems = function() {
    $('.listing-item').each(function() {
        if ($(this).attr('id') != undefined) {
            var $listing = $(this).attr('id');
            var arrListing = $listing.split('-');
            var params = {};
            params.LISTINGID = arrListing[1];
            var $listing = $('#listing-' + arrListing[1]);
            if ($listing.find('.jsLotIDContainer').css('display') != 'none') {
                var strLotId = $listing.find('.jsLotID').html();
                var strLotType = strLotId.substring(0, 2);
                var lotNumberPrefix = VENUE['lotNumberPrefix'];
                if (strLotType.toUpperCase() == lotNumberPrefix) {
                    $listing.find('.listing-notes').show();
                    $listing.find('.jsAddNote').show();
                    $listing.find('.jsNoteContent').val('');
                    $listing.find('.jsNoteId').val('');
                    $listing.find('.jsNoteFields').hide();
                    fncDisplayAddNotes($listing, params);
                    fncInitNotesConsole('#listing-' + params.LISTINGID + ' .listing-notes');
                    fncDisplayDeleteNotes($listing, params);
                }
            }
        }
    });
};
UserListings.fncGetUserListingsData = function() {
    var params = {};
    var postfields = {};
    params.data = {};
    params.data.curl = '/user/:userID/lists/venueID/:venueID';
    params.url = '/home?task=ajax';
    params.data.method = 'GET';
    postfields.userID = ':userID';
    params.data.version = '2';
    params.data.postfields = JSON.stringify(postfields);
    params.success = UserListings.fncRenderUserListingsData;
    fncAjax(params);
};
UserListings.fncRenderUserListingsData = function(responseText) {
    localStorage.setItem("e1User_lastUpdate", Math.floor(Date.now() / 1000));
    if (responseText['e1_content'] != null) {
        var userListings = responseText['e1_content']['USERLISTS'];
        $.each(userListings, function(index, list) {
            var listingType = list.LISTNAME;
            listingType = listingType.toUpperCase();
            switch (listingType) {
                case "WATCHLIST":
                    if (list.LISTINGIDS != 'undefined') {
                        localStorage.setItem("e1User_watchlist", list.LISTINGIDS);
                        UserListings.fncUpdateWatchedStatus(list.LISTINGIDS);
                    }
                    break;
                case "NOTELIST":
                    if (list.LISTINGIDS != 'undefined') {
                        localStorage.setItem("e1User_noteListings", list.LISTINGIDS);
                        UserListings.fncUpdateNote(list.LISTINGIDS);
                    }
                    break;
                case "BUYINGACTIVE":
                    if (list.LISTINGIDS != 'undefined') {
                        localStorage.setItem("e1User_activeListings", JSON.stringify(list.LISTINGIDS));
                        UserListings.fncUpdateBuyingActive(list.LISTINGIDS);
                        var highBidder;
                        var rank;
                        for (var i in list.LISTINGIDS) {
                            $listing = $('#listing-' + list.LISTINGIDS[i].LISTINGID);
                            fncSetMaxBid($listing, list.LISTINGIDS[i].MYMAXBID, list.LISTINGIDS[i].CURRENCYLOCALESHORT);
                            highBidder = $listing.data('highbidhash');
                            rank = UserListings.fncGetUserRank(highBidder);
                            fncSetColor($listing, rank);
                        }
                    }
                    break;
            }
        });
    }
}
UserListings.fncGetUserRank = function(highBidUser) {
    var userRank = 0;
    if (highBidUser == E1_USERID_HASH || (E1_CHILDUSERS_HASH.length > 0 && E1_CHILDUSERS_HASH.indexOf(highBidUser) > -1)) {
        userRank = 1;
    }
    return userRank;
}
UserListings.fncUpdateWatchedStatus = function(watchedListings) {
    var watchedCount = watchedListings.length;
    if (watchedCount > 0) {
        for (w = 0; w < watchedCount; w++) {
            var watchedListing = watchedListings[w];
            var params = {};
            params.LISTINGID = watchedListing;
            var $listing = $('#listing-' + watchedListing);
            fncDisplayRemoveFromWatchlist($listing, params);
        }
    }
};
UserListings.fncUpdateNote = function(noteListings) {
    var notesCount = noteListings.length;
    var params = {};
    if (typeof pageConfig != 'undefined' && notesCount > 0) {
        var params = {};
        var postfields = {};
        params.data = {};
        pageConfig.defaultAjaxController = '/home?task=ajax';
        params.data.curl = '/myone/:userID/offerNotes/listing/:listingID';
        params.url = pageConfig.defaultAjaxController;
        params.data.method = 'GET';
        postfields.userID = ':userID';
        params.data.listingID = noteListings.toString();
        params.data.version = '2';
        params.success = UserListings.fncRenderListingNotes;
        fncAjax(params);
    }
};
UserListings.fncRenderListingNotes = function(listingNotes) {
    if (typeof listingNotes['e1_content'] != 'undefined')
        var offerNotes = listingNotes['e1_content']['OFFERNOTES'];
    if (offerNotes != undefined) {
        noteListingData = offerNotes;
        $('.listing-item').each(function() {
            if ($(this).attr('id') != undefined) {
                var $listing = $(this).attr('id');
                var arrListing = $listing.split('-');
                var $listing = $('#listing-' + arrListing[1]);
                if ($listing.find('.jsLotIDContainer').css('display') != 'none') {
                    var noteContent = '';
                    var noteId = '';
                    $.each(noteListingData, function(i, item) {
                        if (noteListingData[i].LISTINGID == arrListing[1]) {
                            noteContent = noteListingData[i].NOTE;
                            noteId = noteListingData[i].ID;
                        }
                    });
                    if (noteContent != '' && noteId != '') {
                        var noteContent = noteContent.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
                        $listing.find('.jsNoteContent').val(noteContent);
                        $listing.find('.jsListingNote').html(noteContent);
                        $listing.find('.jsNoteId').val(noteId);
                        $listing.find('.jsAddNote').hide();
                        $listing.find('.jsNoteFields').show();
                        var params = {};
                        var postfields = {};
                        params.LISTINGID = arrListing[1];
                        fncDisplayEditNotes($listing, params);
                    }
                }
            }
        });
    }
};
UserListings.fncGetNoteContent = function(listingId) {
    $.each(noteListingData, function(i, item) {
        return 'abc' + item.LISTINGID;
        if (item.LISTINGID == listingId) {
            return 'test' + listingId;
        }
    });
};
UserListings.fncUpdateBuyingActive = function(activeListings) {
    var activeCount = activeListings.length;
    for (a = 0; a < activeCount; a++) {
        var activeListing = activeListings[a].LISTINGID;
        var myMaxBid = activeListings[a].MYMAXBID;
        UserListings.fncDisplayActiveListingStatus(activeListing, myMaxBid);
    }
};
UserListings.getList = function(listName) {
    return localStorage.getItem(listName);
};
UserListings.fncGetBuyingActive = function() {
    return $.parseJSON(UserListings.getList("e1User_activeListings"));
};
UserListings.fncGetWatchlist = function() {
    return $.parseJSON("[" + UserListings.getList("e1User_watchlist") + "]");
};
UserListings.fncDisplayActiveListingStatus = function(activeListing, myMaxBid) {
    var $listing = $('#listing-' + activeListing);
    fncSetMaxBid($listing, myMaxBid);
};
UserListings.fncAddListing = function(listingId, listType, maxBid) {
    switch (listType) {
        case "WATCHLIST":
            var userWatchedListings = localStorage.getItem("e1User_watchlist");
            if (userWatchedListings != null && userWatchedListings != "") {
                userWatchedListings = uniqueArr(userWatchedListings.split(','));
            } else {
                userWatchedListings = [];
            }
            if (userWatchedListings.indexOf(listingId) == -1) {
                userWatchedListings.push(listingId);
                localStorage.setItem("e1User_watchlist", userWatchedListings);
            }
            break;
        case "NOTESLIST":
            var userNoteListings = localStorage.getItem("e1User_noteListings");
            if (userNoteListings != null && userWatchedListings != "") {
                userNoteListings = uniqueArr(userNoteListings.split(','));
            } else {
                userNoteListings = [];
            }
            if (userNoteListings.indexOf(listingId) == -1) {
                userNoteListings.push(listingId);
                localStorage.setItem("e1User_noteListings", userNoteListings);
            }
            break;
        case "BUYINGACTIVE":
            var userActiveListings = localStorage.getItem("e1User_activeListings");
            userActiveListings = JSON.parse(userActiveListings);
            var alreadyBid = 0;
            $.each(userActiveListings, function(index, list) {
                if (list.LISTINGID == listingId) {
                    alreadyBid = 1;
                    userActiveListings[index].MYMAXBID = maxBid;
                }
            });
            if (alreadyBid == 0) {
                userActiveListings.push({
                    LISTINGID: listingId,
                    MYMAXBID: maxBid
                });
            }
            localStorage.setItem("e1User_activeListings", JSON.stringify(userActiveListings));
            break;
    }
};
UserListings.fncRemoveListing = function(listingId, listType) {
    switch (listType) {
        case "WATCHLIST":
            var userWatchedListings = localStorage.getItem("e1User_watchlist");
            userWatchedListings = uniqueArr(userWatchedListings.split(','));
            var listingIndex = userWatchedListings.indexOf(listingId);
            if (listingIndex != -1) {
                userWatchedListings.splice(listingIndex, 1);
                localStorage.setItem("e1User_watchlist", userWatchedListings);
            }
            break;
        case "NOTESLIST":
            var listingIndex = userNoteListings.indexOf(listingId);
            if (listingIndex != -1) {
                userNoteListings.splice(listingIndex, 1);
            }
            break;
        case "BUYINGACTIVE":
            var listingIndex = activeListings.indexOf(listingId);
            if (listingIndex != -1) {
                activeListings.splice(listingIndex, 1);
            }
            break;
    }
};
var uniqueArr = function(origArr) {
    var newArr = [],
        origLen = origArr.length,
        found, x, y;
    for (x = 0; x < origLen; x++) {
        found = undefined;
        for (y = 0; y < newArr.length; y++) {
            if (origArr[x] === newArr[y]) {
                found = true;
                break;
            }
        }
        if (!found) {
            newArr.push(origArr[x]);
        }
    }
    return newArr;
}

/*===============================
https://www.equipmentone.com/e1_2/js/localeUtils.js
================================================================================*/
;

function fncCurrencyText(symbol, currenylocal, leadOfferPrice) {
    if (currenylocal != undefined && leadOfferPrice != undefined) {
        leadOfferPrice = parseFloat(leadOfferPrice).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        if (leadOfferPrice[0] == '$')
            return leadOfferPrice + ' ' + currenylocal;
        else
            return symbol + leadOfferPrice + ' ' + currenylocal;
    }
}

function fncFormatCurrency(symbol, amount, locale) {
    if (amount[0] == '$') {
        amount = amount.substring(1);
    }
    amount = parseFloat(amount).toFixed(2);
    return symbol + amount + ' (' + locale + ')';
}

/*===============================
https://www.equipmentone.com/e1_2/js/minify/e122a2f6.js
================================================================================*/
;

/*===============================
https://www.equipmentone.com/e1_2/js/send2Monetate.js
================================================================================*/
;

function send2Monetate() {
    this.arrProducts = [];
    this.objConversion = {};
    this.pushProduct = function(LISTINGID) {
        this.arrProducts.push(String(LISTINGID));
    }
    this.addProducts = function() {
        try {
            window.monetateQ.push(["setPageType", this.pageType]);
            window.monetateQ.push(["addProducts", this.arrProducts]);
            window.monetateQ.push(["trackData"]);
        } catch (e) {
            Raygun.send(e);
        }
        this.arrProducts = [];
    }
    this.addProductDetails = function() {
        try {
            window.monetateQ.push(["setPageType", this.pageType]);
            window.monetateQ.push(["addProductDetails", this.arrProducts]);
            window.monetateQ.push(["trackData"]);
        } catch (e) {
            Raygun.send(e);
        }
        this.arrProducts = [];
    }
    this.addCartRow = function(data, params) {
        productData = {
            'productId': params.LISTINGID,
            'quantity': "1",
            "unitPrice": data.find('.jsMinOffer').text().replace(/[^0-9\.]+/g, ""),
            "currency": data.find('.locale_short').val()
        };
        try {
            window.monetateQ.push(["setPageType", 'cart']);
            window.monetateQ.push(["addCartRows", [productData]]);
            window.monetateQ.push(["trackData"]);
        } catch (e) {
            Raygun.send(e);
        }
    }
    this.addCategory = function(data) {
        this.pageType = 'category';
        try {
            window.monetateQ.push(["setPageType", this.pageType]);
            window.monetateQ.push(["addCategories", data]);
            window.monetateQ.push(["trackData"]);
        } catch (e) {
            Raygun.send(e);
        }
    }
    this.addConversionRows = function() {
        try {
            window.monetateQ.push(["setPageType", this.pageType]);
            window.monetateQ.push(["addConversionRows", [this.objConversion]]);
            window.monetateQ.push(["trackData"]);
        } catch (e) {
            Raygun.send(e);
        }
    }
    this.pageView = function() {
        try {
            window.monetateQ.push(["setPageType", this.pageType]);
            window.monetateQ.push(["trackData"]);
        } catch (e) {
            Raygun.send(e);
        }
    }
}
var send2Monetate = new send2Monetate();

/*===============================
/plugins/system/t3/base-bs3/js/nav-collapse.js
================================================================================*/
;
jQuery(document).ready(function($) {
    $('.t3-navbar').each(function() {
        var $navwrapper = $(this),
            $menu = null,
            $placeholder = null;
        if ($navwrapper.find('.t3-megamenu').length) {
            $menu = $navwrapper.find('ul.level0').clone(), $placeholder = $navwrapper.prev('.navbar-collapse');
            if (!$placeholder.length) {
                $placeholder = $navwrapper.closest('.container, .t3-mainnav').find('.navbar-collapse:empty');
            }
            var lis = $menu.find('li[data-id]'),
                liactive = lis.filter('.current');
            lis.removeClass('mega dropdown mega-align-left mega-align-right mega-align-center mega-align-adjust');
            lis.each(function() {
                var $li = $(this),
                    $child = $li.find('>:first-child');
                if ($child[0].nodeName == 'DIV') {
                    $child.find('>:first-child').prependTo($li);
                    $child.remove();
                }
                if ($li.data('hidewcol')) {
                    $child.find('.caret').remove();
                    $child.nextAll().remove();
                    return;
                }
                var subul = $li.find('ul.level' + $li.data('level'));
                if (subul.length) {
                    $ul = $('<ul class="level' + $li.data('level') + ' dropdown-menu">');
                    subul.each(function() {
                        if ($(this).parents('.mega-col-nav').data('hidewcol')) return;
                        $(this).find('>li').appendTo($ul);
                    });
                    if ($ul.children().length) {
                        $ul.appendTo($li);
                    }
                }
                $li.find('>div').remove();
                if (!$li.children('ul').length) {
                    $child.find('.caret').remove();
                }
                var divider = $li.hasClass('divider');
                for (var x in $li.data()) {
                    $li.removeAttr('data-' + x)
                }
                $child.removeAttr('class');
                for (var x in $child.data()) {
                    $child.removeAttr('data-' + x)
                }
                if (divider) {
                    $li.addClass('divider');
                }
            });
            liactive.addClass('current active');
        } else {
            $menu = $navwrapper.find('ul.nav').clone();
            $placeholder = $('.t3-navbar-collapse:empty, .navbar-collapse:empty').eq(0);
        }
        $menu.find('a[data-toggle="dropdown"]').removeAttr('data-toggle').removeAttr('data-target');
        $menu.find('> li > ul.dropdown-menu').prev('a').attr('data-toggle', 'dropdown').attr('data-target', '#').parent('li').addClass(function() {
            return 'dropdown' + ($(this).data('level') > 1 ? ' dropdown-submenu' : '');
        });
        $menu.appendTo($placeholder);
    });
});

/*===============================
https://www.equipmentone.com/assetnation/js/e1footer.js
================================================================================*/
;

function E1Footer() {}
$(document).on('ready', function() {
    E1Footer.onDocumentReady();
});
E1Footer.onDocumentReady = function() {
    first_name = "firstName";
    last_name = "lastName";
    email = "emailAddr";
    e1source = "EquipmentOne-Footer";
    marketingEmailPreference = "E1-Unconfirmed";
    var defaultFname = "First Name";
    var defaultLname = "Last Name";
    var defaultEmail = "Email";
    defaultVals = ["First Name", "Last Name", "Email", "First Name is required", "Last Name is required", "Email is required", "Valid Email address is required", ""];
    if ($('#e2footer').val() != '1') {
        $("#" + first_name).val(defaultFname);
        $("#" + last_name).val(defaultLname);
        $("#" + email).val(defaultEmail);
    }
    $(".input_text").attr('autocomplete', 'off');
    $('ul.footer_submenu').on('click', 'li', function() {
        clearSessionStorageData('search');
    });
    $("#btn-subscribe").on('click', function() {
        submitClick();
    });
    $(".input_text").on('focus', function() {
        removeDuplicates();
        if ($.inArray($.trim($(this).val()), defaultVals) != -1) {
            $(this).val('');
            $(this).addClass('black_text');
            $(this).removeClass('input_err');
        }
    });
    $(".input_text").keyup(function(e) {
        if (e.keyCode == 13) {
            submitClick();
        }
    });
    $(".input_text").on('blur', function() {
        if ($.trim($(this).val()) == '' && $('#e2footer').val() != '1') {
            switch ($(this).attr('id')) {
                case first_name:
                    $(this).val(defaultFname);
                    break;
                case last_name:
                    $(this).val(defaultLname);
                    break;
                case email:
                    $(this).val(defaultEmail);
                    break;
            }
            $(this).removeClass('black_text');
        }
    });
};

function submitClick() {
    $(":focus").blur();
    removeDuplicates();
    var fieldsAreValid = true;
    firstName = $.trim($("#" + first_name + ".input_text").val());
    lastName = $.trim($("#" + last_name + ".input_text").val());
    emailAddr = $.trim($("#" + email + ".input_text").val());
    $(".input_text").each(function() {
        var textValue = $.trim($(this).val());
        if ($.inArray(textValue, defaultVals) != -1) {
            switch ($(this).attr('id')) {
                case first_name:
                    $(this).val("First Name is required");
                    break;
                case last_name:
                    $(this).val("Last Name is required");
                    break;
                case email:
                    $(this).val("Email is required");
                    break;
            }
            $(this).addClass('input_err');
            fieldsAreValid = false;
        }
    });
    if ($.inArray(emailAddr, defaultVals) == -1) {
        if (validEmail(emailAddr) == false) {
            $("#" + email + ".input_text").addClass('input_err');
            $("#" + email + ".input_text").removeClass('black_text');
            $("#" + email + ".input_text").val('Valid Email address is required');
            fieldsAreValid = false;
        }
    }
    if (fieldsAreValid) {
        loading();
        encodedEmailAddr = encodeURIComponent(emailAddr);
        var formData = "FirstName=" + firstName + "&LastName=" + lastName + "&Email=" + encodedEmailAddr + "&E1Source=" + e1source + "&MarketingEmailPreference=" + marketingEmailPreference;
        $.ajax({
            url: "/index.php?option=com_e1registration&task=emailRegister",
            type: "POST",
            data: formData,
            dataType: "text",
            complete: function(returnData) {
                switch (returnData.responseText) {
                    case "created":
                        submitSuccess();
                        break;
                    case "skipped":
                        isSubscribed();
                        break;
                    default:
                        submitFailure();
                        break;
                }
            },
            error: function(e) {
                submitFailure();
            }
        });
    }
};

function submitSuccess() {
    $(".signup_container").css("display", "none");
    $(".confirm_text").css("display", "block");
    $(".confirm_text").html("\
  <p>Thank you for signing up!</p>\
  <p>A confirmation email will be sent to </p><span>" + emailAddr + "</span><br>\
  <p>Click the link in email to confirm your subscription.</p>");
}

function isSubscribed() {
    $(".signup_container *").css("display", "block");
    $(".signup_container").removeClass("loading_spinner");
    $(".error_text").css("display:block");
    $(".error_text").html("This email address is already subscribed.");
}

function submitFailure() {
    $(".signup_container *").css("display", "block");
    $(".signup_container").removeClass("loading_spinner");
    $(".error_text").css("display:block");
    $(".error_text").html("A problem occurred. Please try again.");
}

function loading() {
    $(".signup_container *").css("display", "none");
    $(".signup_container").addClass("loading_spinner");
}

function validEmail(email) {
    var emailRE = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRE.test(email);
}

function removeDuplicates() {
    $(".jsHiddenAjax .input_text").removeClass('input_text');
}

/*===============================
https://www.equipmentone.com/assetnation/js/jquery.base64.js
================================================================================*/
;;
/*!
 * jquery.base64.js 0.1 - https://github.com/yckart/jquery.base64.js
 * Makes Base64 en & -decoding simpler as it is.
 *
 * Based upon: https://gist.github.com/Yaffle/1284012
 *
 * Copyright (c) 2012 Yannick Albert (http://yckart.com)
 * Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php).
 * 2013/02/10
 **/
(function($) {
    var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
        a256 = '',
        r64 = [256],
        r256 = [256],
        i = 0;
    var UTF8 = {
        encode: function(strUni) {
            var strUtf = strUni.replace(/[\u0080-\u07ff]/g, function(c) {
                var cc = c.charCodeAt(0);
                return String.fromCharCode(0xc0 | cc >> 6, 0x80 | cc & 0x3f);
            }).replace(/[\u0800-\uffff]/g, function(c) {
                var cc = c.charCodeAt(0);
                return String.fromCharCode(0xe0 | cc >> 12, 0x80 | cc >> 6 & 0x3F, 0x80 | cc & 0x3f);
            });
            return strUtf;
        },
        decode: function(strUtf) {
            var strUni = strUtf.replace(/[\u00e0-\u00ef][\u0080-\u00bf][\u0080-\u00bf]/g, function(c) {
                var cc = ((c.charCodeAt(0) & 0x0f) << 12) | ((c.charCodeAt(1) & 0x3f) << 6) | (c.charCodeAt(2) & 0x3f);
                return String.fromCharCode(cc);
            }).replace(/[\u00c0-\u00df][\u0080-\u00bf]/g, function(c) {
                var cc = (c.charCodeAt(0) & 0x1f) << 6 | c.charCodeAt(1) & 0x3f;
                return String.fromCharCode(cc);
            });
            return strUni;
        }
    };
    while (i < 256) {
        var c = String.fromCharCode(i);
        a256 += c;
        r256[i] = i;
        r64[i] = b64.indexOf(c);
        ++i;
    }

    function code(s, discard, alpha, beta, w1, w2) {
        s = String(s);
        var buffer = 0,
            i = 0,
            length = s.length,
            result = '',
            bitsInBuffer = 0;
        while (i < length) {
            var c = s.charCodeAt(i);
            c = c < 256 ? alpha[c] : -1;
            buffer = (buffer << w1) + c;
            bitsInBuffer += w1;
            while (bitsInBuffer >= w2) {
                bitsInBuffer -= w2;
                var tmp = buffer >> bitsInBuffer;
                result += beta.charAt(tmp);
                buffer ^= tmp << bitsInBuffer;
            }
            ++i;
        }
        if (!discard && bitsInBuffer > 0) result += beta.charAt(buffer << (w2 - bitsInBuffer));
        return result;
    }
    var Plugin = $.base64 = function(dir, input, encode) {
        return input ? Plugin[dir](input, encode) : dir ? null : this;
    };
    Plugin.btoa = Plugin.encode = function(plain, utf8encode) {
        plain = Plugin.raw === false || Plugin.utf8encode || utf8encode ? UTF8.encode(plain) : plain;
        plain = code(plain, false, r256, b64, 8, 6);
        return plain + '===='.slice((plain.length % 4) || 4);
    };
    Plugin.atob = Plugin.decode = function(coded, utf8decode) {
        coded = String(coded).split('=');
        var i = coded.length;
        do {
            --i;
            coded[i] = code(coded[i], true, r64, a256, 6, 8);
        } while (i > 0);
        coded = coded.join('');
        return Plugin.raw === false || Plugin.utf8decode || utf8decode ? UTF8.decode(coded) : coded;
    };
}(jQuery));

/*===============================
https://www.equipmentone.com/templates/ja_playmag/js/bootstrap-slider.js
================================================================================*/
;
/*! =========================================================
 * bootstrap-slider.js
 *
 * Maintainers:
 *  Kyle Kemp
 *   - Twitter: @seiyria
 *   - Github:  seiyria
 *  Rohit Kalkur
 *   - Twitter: @Rovolutionary
 *   - Github:  rovolution
 *
 * =========================================================
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================= */
(function(root, factory) {
    if (typeof define === "function" && define.amd) {
        define(["jquery"], factory);
    } else if (typeof module === "object" && module.exports) {
        var jQuery;
        try {
            jQuery = require("jquery");
        } catch (err) {
            jQuery = null;
        }
        module.exports = factory(jQuery);
    } else {
        root.Slider = factory(root.jQuery);
    }
}(this, function($) {
    var Slider;
    (function($) {
        'use strict';
        var slice = Array.prototype.slice;

        function noop() {}

        function defineBridget($) {
            if (!$) {
                return;
            }

            function addOptionMethod(PluginClass) {
                if (PluginClass.prototype.option) {
                    return;
                }
                PluginClass.prototype.option = function(opts) {
                    if (!$.isPlainObject(opts)) {
                        return;
                    }
                    this.options = $.extend(true, this.options, opts);
                };
            }
            var logError = typeof console === 'undefined' ? noop : function(message) {
                console.error(message);
            };

            function bridge(namespace, PluginClass) {
                $.fn[namespace] = function(options) {
                    if (typeof options === 'string') {
                        var args = slice.call(arguments, 1);
                        for (var i = 0, len = this.length; i < len; i++) {
                            var elem = this[i];
                            var instance = $.data(elem, namespace);
                            if (!instance) {
                                logError("cannot call methods on " + namespace + " prior to initialization; " + "attempted to call '" + options + "'");
                                continue;
                            }
                            if (!$.isFunction(instance[options]) || options.charAt(0) === '_') {
                                logError("no such method '" + options + "' for " + namespace + " instance");
                                continue;
                            }
                            var returnValue = instance[options].apply(instance, args);
                            if (returnValue !== undefined && returnValue !== instance) {
                                return returnValue;
                            }
                        }
                        return this;
                    } else {
                        var objects = this.map(function() {
                            var instance = $.data(this, namespace);
                            if (instance) {
                                instance.option(options);
                                instance._init();
                            } else {
                                instance = new PluginClass(this, options);
                                $.data(this, namespace, instance);
                            }
                            return $(this);
                        });
                        if (!objects || objects.length > 1) {
                            return objects;
                        } else {
                            return objects[0];
                        }
                    }
                };
            }
            $.bridget = function(namespace, PluginClass) {
                addOptionMethod(PluginClass);
                bridge(namespace, PluginClass);
            };
            return $.bridget;
        }
        defineBridget($);
    })($);
    (function($) {
        var ErrorMsgs = {
            formatInvalidInputErrorMsg: function(input) {
                return "Invalid input value '" + input + "' passed in";
            },
            callingContextNotSliderInstance: "Calling context element does not have instance of Slider bound to it. Check your code to make sure the JQuery object returned from the call to the slider() initializer is calling the method"
        };
        var SliderScale = {
            linear: {
                toValue: function(percentage) {
                    var rawValue = percentage / 100 * (this.options.max - this.options.min);
                    if (this.options.ticks_positions.length > 0) {
                        var minv, maxv, minp, maxp = 0;
                        for (var i = 0; i < this.options.ticks_positions.length; i++) {
                            if (percentage <= this.options.ticks_positions[i]) {
                                minv = (i > 0) ? this.options.ticks[i - 1] : 0;
                                minp = (i > 0) ? this.options.ticks_positions[i - 1] : 0;
                                maxv = this.options.ticks[i];
                                maxp = this.options.ticks_positions[i];
                                break;
                            }
                        }
                        if (i > 0) {
                            var partialPercentage = (percentage - minp) / (maxp - minp);
                            rawValue = minv + partialPercentage * (maxv - minv);
                        }
                    }
                    var value = this.options.min + Math.round(rawValue / this.options.step) * this.options.step;
                    if (value < this.options.min) {
                        return this.options.min;
                    } else if (value > this.options.max) {
                        return this.options.max;
                    } else {
                        return value;
                    }
                },
                toPercentage: function(value) {
                    if (this.options.max === this.options.min) {
                        return 0;
                    }
                    if (this.options.ticks_positions.length > 0) {
                        var minv, maxv, minp, maxp = 0;
                        for (var i = 0; i < this.options.ticks.length; i++) {
                            if (value <= this.options.ticks[i]) {
                                minv = (i > 0) ? this.options.ticks[i - 1] : 0;
                                minp = (i > 0) ? this.options.ticks_positions[i - 1] : 0;
                                maxv = this.options.ticks[i];
                                maxp = this.options.ticks_positions[i];
                                break;
                            }
                        }
                        if (i > 0) {
                            var partialPercentage = (value - minv) / (maxv - minv);
                            return minp + partialPercentage * (maxp - minp);
                        }
                    }
                    return 100 * (value - this.options.min) / (this.options.max - this.options.min);
                }
            },
            logarithmic: {
                toValue: function(percentage) {
                    var min = (this.options.min === 0) ? 0 : Math.log(this.options.min);
                    var max = Math.log(this.options.max);
                    var value = Math.exp(min + (max - min) * percentage / 100);
                    value = this.options.min + Math.round((value - this.options.min) / this.options.step) * this.options.step;
                    if (value < this.options.min) {
                        return this.options.min;
                    } else if (value > this.options.max) {
                        return this.options.max;
                    } else {
                        return value;
                    }
                },
                toPercentage: function(value) {
                    if (this.options.max === this.options.min) {
                        return 0;
                    } else {
                        var max = Math.log(this.options.max);
                        var min = this.options.min === 0 ? 0 : Math.log(this.options.min);
                        var v = value === 0 ? 0 : Math.log(value);
                        return 100 * (v - min) / (max - min);
                    }
                }
            }
        };
        Slider = function(element, options) {
            createNewSlider.call(this, element, options);
            return this;
        };

        function createNewSlider(element, options) {
            if (typeof element === "string") {
                this.element = document.querySelector(element);
            } else if (element instanceof HTMLElement) {
                this.element = element;
            }
            options = options ? options : {};
            var optionTypes = Object.keys(this.defaultOptions);
            for (var i = 0; i < optionTypes.length; i++) {
                var optName = optionTypes[i];
                var val = options[optName];
                val = (typeof val !== 'undefined') ? val : getDataAttrib(this.element, optName);
                val = (val !== null) ? val : this.defaultOptions[optName];
                if (!this.options) {
                    this.options = {};
                }
                this.options[optName] = val;
            }

            function getDataAttrib(element, optName) {
                var dataName = "data-slider-" + optName.replace(/_/g, '-');
                var dataValString = element.getAttribute(dataName);
                try {
                    return JSON.parse(dataValString);
                } catch (err) {
                    return dataValString;
                }
            }
            var origWidth = this.element.style.width;
            var updateSlider = false;
            var parent = this.element.parentNode;
            var sliderTrackSelection;
            var sliderTrackLow, sliderTrackHigh;
            var sliderMinHandle;
            var sliderMaxHandle;
            if (this.sliderElem) {
                updateSlider = true;
            } else {
                this.sliderElem = document.createElement("div");
                this.sliderElem.className = "slider";
                var sliderTrack = document.createElement("div");
                sliderTrack.className = "slider-track";
                sliderTrackLow = document.createElement("div");
                sliderTrackLow.className = "slider-track-low";
                sliderTrackSelection = document.createElement("div");
                sliderTrackSelection.className = "slider-selection";
                sliderTrackHigh = document.createElement("div");
                sliderTrackHigh.className = "slider-track-high";
                sliderMinHandle = document.createElement("div");
                sliderMinHandle.className = "slider-handle min-slider-handle";
                sliderMaxHandle = document.createElement("div");
                sliderMaxHandle.className = "slider-handle max-slider-handle";
                sliderTrack.appendChild(sliderTrackLow);
                sliderTrack.appendChild(sliderTrackSelection);
                sliderTrack.appendChild(sliderTrackHigh);
                this.ticks = [];
                if (Array.isArray(this.options.ticks) && this.options.ticks.length > 0) {
                    for (i = 0; i < this.options.ticks.length; i++) {
                        var tick = document.createElement('div');
                        tick.className = 'slider-tick';
                        this.ticks.push(tick);
                        sliderTrack.appendChild(tick);
                    }
                    sliderTrackSelection.className += " tick-slider-selection";
                }
                sliderTrack.appendChild(sliderMinHandle);
                sliderTrack.appendChild(sliderMaxHandle);
                this.tickLabels = [];
                if (Array.isArray(this.options.ticks_labels) && this.options.ticks_labels.length > 0) {
                    this.tickLabelContainer = document.createElement('div');
                    this.tickLabelContainer.className = 'slider-tick-label-container';
                    for (i = 0; i < this.options.ticks_labels.length; i++) {
                        var label = document.createElement('div');
                        label.className = 'slider-tick-label';
                        label.innerHTML = this.options.ticks_labels[i];
                        this.tickLabels.push(label);
                        this.tickLabelContainer.appendChild(label);
                    }
                }
                var createAndAppendTooltipSubElements = function(tooltipElem) {
                    var arrow = document.createElement("div");
                    arrow.className = "tooltip-arrow";
                    var inner = document.createElement("div");
                    inner.className = "tooltip-inner";
                    tooltipElem.appendChild(arrow);
                    tooltipElem.appendChild(inner);
                };
                var sliderTooltip = document.createElement("div");
                sliderTooltip.className = "tooltip tooltip-main";
                createAndAppendTooltipSubElements(sliderTooltip);
                var sliderTooltipMin = document.createElement("div");
                sliderTooltipMin.className = "tooltip tooltip-min";
                createAndAppendTooltipSubElements(sliderTooltipMin);
                var sliderTooltipMax = document.createElement("div");
                sliderTooltipMax.className = "tooltip tooltip-max";
                createAndAppendTooltipSubElements(sliderTooltipMax);
                this.sliderElem.appendChild(sliderTrack);
                this.sliderElem.appendChild(sliderTooltip);
                this.sliderElem.appendChild(sliderTooltipMin);
                this.sliderElem.appendChild(sliderTooltipMax);
                if (this.tickLabelContainer) {
                    this.sliderElem.appendChild(this.tickLabelContainer);
                }
                parent.insertBefore(this.sliderElem, this.element);
                this.element.style.display = "none";
            }
            if ($) {
                this.$element = $(this.element);
                this.$sliderElem = $(this.sliderElem);
            }
            this.eventToCallbackMap = {};
            this.sliderElem.id = this.options.id;
            this.touchCapable = 'ontouchstart' in window || (window.DocumentTouch && document instanceof window.DocumentTouch);
            this.tooltip = this.sliderElem.querySelector('.tooltip-main');
            this.tooltipInner = this.tooltip.querySelector('.tooltip-inner');
            this.tooltip_min = this.sliderElem.querySelector('.tooltip-min');
            this.tooltipInner_min = this.tooltip_min.querySelector('.tooltip-inner');
            this.tooltip_max = this.sliderElem.querySelector('.tooltip-max');
            this.tooltipInner_max = this.tooltip_max.querySelector('.tooltip-inner');
            if (SliderScale[this.options.scale]) {
                this.options.scale = SliderScale[this.options.scale];
            }
            if (updateSlider === true) {
                this._removeClass(this.sliderElem, 'slider-horizontal');
                this._removeClass(this.sliderElem, 'slider-vertical');
                this._removeClass(this.tooltip, 'hide');
                this._removeClass(this.tooltip_min, 'hide');
                this._removeClass(this.tooltip_max, 'hide');
                ["left", "top", "width", "height"].forEach(function(prop) {
                    this._removeProperty(this.trackLow, prop);
                    this._removeProperty(this.trackSelection, prop);
                    this._removeProperty(this.trackHigh, prop);
                }, this);
                [this.handle1, this.handle2].forEach(function(handle) {
                    this._removeProperty(handle, 'left');
                    this._removeProperty(handle, 'top');
                }, this);
                [this.tooltip, this.tooltip_min, this.tooltip_max].forEach(function(tooltip) {
                    this._removeProperty(tooltip, 'left');
                    this._removeProperty(tooltip, 'top');
                    this._removeProperty(tooltip, 'margin-left');
                    this._removeProperty(tooltip, 'margin-top');
                    this._removeClass(tooltip, 'right');
                    this._removeClass(tooltip, 'top');
                }, this);
            }
            if (this.options.orientation === 'vertical') {
                this._addClass(this.sliderElem, 'slider-vertical');
                this.stylePos = 'top';
                this.mousePos = 'pageY';
                this.sizePos = 'offsetHeight';
                this._addClass(this.tooltip, 'right');
                this.tooltip.style.left = '100%';
                this._addClass(this.tooltip_min, 'right');
                this.tooltip_min.style.left = '100%';
                this._addClass(this.tooltip_max, 'right');
                this.tooltip_max.style.left = '100%';
            } else {
                this._addClass(this.sliderElem, 'slider-horizontal');
                this.sliderElem.style.width = origWidth;
                this.options.orientation = 'horizontal';
                this.stylePos = 'left';
                this.mousePos = 'pageX';
                this.sizePos = 'offsetWidth';
                this._addClass(this.tooltip, 'top');
                this.tooltip.style.top = -this.tooltip.outerHeight - 14 + 'px';
                this._addClass(this.tooltip_min, 'top');
                this.tooltip_min.style.top = -this.tooltip_min.outerHeight - 14 + 'px';
                this._addClass(this.tooltip_max, 'top');
                this.tooltip_max.style.top = -this.tooltip_max.outerHeight - 14 + 'px';
            }
            if (Array.isArray(this.options.ticks) && this.options.ticks.length > 0) {
                this.options.max = Math.max.apply(Math, this.options.ticks);
                this.options.min = Math.min.apply(Math, this.options.ticks);
            }
            if (Array.isArray(this.options.value)) {
                this.options.range = true;
            } else if (this.options.range) {
                this.options.value = [this.options.value, this.options.max];
            }
            this.trackLow = sliderTrackLow || this.trackLow;
            this.trackSelection = sliderTrackSelection || this.trackSelection;
            this.trackHigh = sliderTrackHigh || this.trackHigh;
            if (this.options.selection === 'none') {
                this._addClass(this.trackLow, 'hide');
                this._addClass(this.trackSelection, 'hide');
                this._addClass(this.trackHigh, 'hide');
            }
            this.handle1 = sliderMinHandle || this.handle1;
            this.handle2 = sliderMaxHandle || this.handle2;
            if (updateSlider === true) {
                this._removeClass(this.handle1, 'round triangle');
                this._removeClass(this.handle2, 'round triangle hide');
                for (i = 0; i < this.ticks.length; i++) {
                    this._removeClass(this.ticks[i], 'round triangle hide');
                }
            }
            var availableHandleModifiers = ['round', 'triangle', 'custom'];
            var isValidHandleType = availableHandleModifiers.indexOf(this.options.handle) !== -1;
            if (isValidHandleType) {
                this._addClass(this.handle1, this.options.handle);
                this._addClass(this.handle2, this.options.handle);
                for (i = 0; i < this.ticks.length; i++) {
                    this._addClass(this.ticks[i], this.options.handle);
                }
            }
            this.offset = this._offset(this.sliderElem);
            this.size = this.sliderElem[this.sizePos];
            this.setValue(this.options.value);
            this.handle1Keydown = this._keydown.bind(this, 0);
            this.handle1.addEventListener("keydown", this.handle1Keydown, false);
            this.handle2Keydown = this._keydown.bind(this, 1);
            this.handle2.addEventListener("keydown", this.handle2Keydown, false);
            this.mousedown = this._mousedown.bind(this);
            if (this.touchCapable) {
                this.sliderElem.addEventListener("touchstart", this.mousedown, false);
            }
            this.sliderElem.addEventListener("mousedown", this.mousedown, false);
            if (this.options.tooltip === 'hide') {
                this._addClass(this.tooltip, 'hide');
                this._addClass(this.tooltip_min, 'hide');
                this._addClass(this.tooltip_max, 'hide');
            } else if (this.options.tooltip === 'always') {
                this._showTooltip();
                this._alwaysShowTooltip = true;
            } else {
                this.showTooltip = this._showTooltip.bind(this);
                this.hideTooltip = this._hideTooltip.bind(this);
                this.sliderElem.addEventListener("mouseenter", this.showTooltip, false);
                this.sliderElem.addEventListener("mouseleave", this.hideTooltip, false);
                this.handle1.addEventListener("focus", this.showTooltip, false);
                this.handle1.addEventListener("blur", this.hideTooltip, false);
                this.handle2.addEventListener("focus", this.showTooltip, false);
                this.handle2.addEventListener("blur", this.hideTooltip, false);
            }
            if (this.options.enabled) {
                this.enable();
            } else {
                this.disable();
            }
        }
        Slider.prototype = {
            _init: function() {},
            constructor: Slider,
            defaultOptions: {
                id: "",
                min: 0,
                max: 10,
                step: 1,
                precision: 0,
                orientation: 'horizontal',
                value: 5,
                range: false,
                selection: 'before',
                tooltip: 'show',
                tooltip_split: false,
                handle: 'round',
                reversed: false,
                enabled: true,
                formatter: function(val) {
                    if (Array.isArray(val)) {
                        return val[0] + " : " + val[1];
                    } else {
                        return val;
                    }
                },
                natural_arrow_keys: false,
                ticks: [],
                ticks_positions: [],
                ticks_labels: [],
                ticks_snap_bounds: 0,
                scale: 'linear',
                focus: false
            },
            over: false,
            inDrag: false,
            getValue: function() {
                if (this.options.range) {
                    return this.options.value;
                }
                return this.options.value[0];
            },
            setValue: function(val, triggerSlideEvent, triggerChangeEvent) {
                if (!val) {
                    val = 0;
                }
                var oldValue = this.getValue();
                this.options.value = this._validateInputValue(val);
                var applyPrecision = this._applyPrecision.bind(this);
                if (this.options.range) {
                    this.options.value[0] = applyPrecision(this.options.value[0]);
                    this.options.value[1] = applyPrecision(this.options.value[1]);
                    this.options.value[0] = Math.max(this.options.min, Math.min(this.options.max, this.options.value[0]));
                    this.options.value[1] = Math.max(this.options.min, Math.min(this.options.max, this.options.value[1]));
                } else {
                    this.options.value = applyPrecision(this.options.value);
                    this.options.value = [Math.max(this.options.min, Math.min(this.options.max, this.options.value))];
                    this._addClass(this.handle2, 'hide');
                    if (this.options.selection === 'after') {
                        this.options.value[1] = this.options.max;
                    } else {
                        this.options.value[1] = this.options.min;
                    }
                }
                if (this.options.max > this.options.min) {
                    this.percentage = [this._toPercentage(this.options.value[0]), this._toPercentage(this.options.value[1]), this.options.step * 100 / (this.options.max - this.options.min)];
                } else {
                    this.percentage = [0, 0, 100];
                }
                this._layout();
                var newValue = this.options.range ? this.options.value : this.options.value[0];
                if (triggerSlideEvent === true) {
                    this._trigger('slide', newValue);
                }
                if ((oldValue !== newValue) && (triggerChangeEvent === true)) {
                    this._trigger('change', {
                        oldValue: oldValue,
                        newValue: newValue
                    });
                }
                this._setDataVal(newValue);
                return this;
            },
            destroy: function() {
                this._removeSliderEventHandlers();
                this.sliderElem.parentNode.removeChild(this.sliderElem);
                this.element.style.display = "";
                this._cleanUpEventCallbacksMap();
                this.element.removeAttribute("data");
                if ($) {
                    this._unbindJQueryEventHandlers();
                    this.$element.removeData('slider');
                }
            },
            disable: function() {
                this.options.enabled = false;
                this.handle1.removeAttribute("tabindex");
                this.handle2.removeAttribute("tabindex");
                this._addClass(this.sliderElem, 'slider-disabled');
                this._trigger('slideDisabled');
                return this;
            },
            enable: function() {
                this.options.enabled = true;
                this.handle1.setAttribute("tabindex", 0);
                this.handle2.setAttribute("tabindex", 0);
                this._removeClass(this.sliderElem, 'slider-disabled');
                this._trigger('slideEnabled');
                return this;
            },
            toggle: function() {
                if (this.options.enabled) {
                    this.disable();
                } else {
                    this.enable();
                }
                return this;
            },
            isEnabled: function() {
                return this.options.enabled;
            },
            on: function(evt, callback) {
                this._bindNonQueryEventHandler(evt, callback);
                return this;
            },
            getAttribute: function(attribute) {
                if (attribute) {
                    return this.options[attribute];
                } else {
                    return this.options;
                }
            },
            setAttribute: function(attribute, value) {
                this.options[attribute] = value;
                return this;
            },
            refresh: function() {
                this._removeSliderEventHandlers();
                createNewSlider.call(this, this.element, this.options);
                if ($) {
                    $.data(this.element, 'slider', this);
                }
                return this;
            },
            relayout: function() {
                this._layout();
                return this;
            },
            _removeSliderEventHandlers: function() {
                this.handle1.removeEventListener("keydown", this.handle1Keydown, false);
                this.handle1.removeEventListener("focus", this.showTooltip, false);
                this.handle1.removeEventListener("blur", this.hideTooltip, false);
                this.handle2.removeEventListener("keydown", this.handle2Keydown, false);
                this.handle2.removeEventListener("focus", this.handle2Keydown, false);
                this.handle2.removeEventListener("blur", this.handle2Keydown, false);
                this.sliderElem.removeEventListener("mouseenter", this.showTooltip, false);
                this.sliderElem.removeEventListener("mouseleave", this.hideTooltip, false);
                this.sliderElem.removeEventListener("touchstart", this.mousedown, false);
                this.sliderElem.removeEventListener("mousedown", this.mousedown, false);
            },
            _bindNonQueryEventHandler: function(evt, callback) {
                if (this.eventToCallbackMap[evt] === undefined) {
                    this.eventToCallbackMap[evt] = [];
                }
                this.eventToCallbackMap[evt].push(callback);
            },
            _cleanUpEventCallbacksMap: function() {
                var eventNames = Object.keys(this.eventToCallbackMap);
                for (var i = 0; i < eventNames.length; i++) {
                    var eventName = eventNames[i];
                    this.eventToCallbackMap[eventName] = null;
                }
            },
            _showTooltip: function() {
                if (this.options.tooltip_split === false) {
                    this._addClass(this.tooltip, 'in');
                } else {
                    this._addClass(this.tooltip_min, 'in');
                    this._addClass(this.tooltip_max, 'in');
                }
                this.over = true;
            },
            _hideTooltip: function() {
                if (this.inDrag === false && this.alwaysShowTooltip !== true) {
                    this._removeClass(this.tooltip, 'in');
                    this._removeClass(this.tooltip_min, 'in');
                    this._removeClass(this.tooltip_max, 'in');
                }
                this.over = false;
            },
            _layout: function() {
                var positionPercentages;
                if (this.options.reversed) {
                    positionPercentages = [100 - this.percentage[0], this.percentage[1]];
                } else {
                    positionPercentages = [this.percentage[0], this.percentage[1]];
                }
                this.handle1.style[this.stylePos] = positionPercentages[0] + '%';
                this.handle2.style[this.stylePos] = positionPercentages[1] + '%';
                if (Array.isArray(this.options.ticks) && this.options.ticks.length > 0) {
                    var maxTickValue = Math.max.apply(Math, this.options.ticks);
                    var minTickValue = Math.min.apply(Math, this.options.ticks);
                    var styleSize = this.options.orientation === 'vertical' ? 'height' : 'width';
                    var styleMargin = this.options.orientation === 'vertical' ? 'marginTop' : 'marginLeft';
                    var labelSize = this.size / (this.options.ticks.length - 1);
                    if (this.tickLabelContainer) {
                        var extraMargin = 0;
                        if (this.options.ticks_positions.length === 0) {
                            this.tickLabelContainer.style[styleMargin] = -labelSize / 2 + 'px';
                            extraMargin = this.tickLabelContainer.offsetHeight;
                        } else {
                            for (i = 0; i < this.tickLabelContainer.childNodes.length; i++) {
                                if (this.tickLabelContainer.childNodes[i].offsetHeight > extraMargin) {
                                    extraMargin = this.tickLabelContainer.childNodes[i].offsetHeight;
                                }
                            }
                        }
                        if (this.options.orientation === 'horizontal') {
                            this.sliderElem.style.marginBottom = extraMargin + 'px';
                        }
                    }
                    for (var i = 0; i < this.options.ticks.length; i++) {
                        var percentage = this.options.ticks_positions[i] || 100 * (this.options.ticks[i] - minTickValue) / (maxTickValue - minTickValue);
                        this.ticks[i].style[this.stylePos] = percentage + '%';
                        this._removeClass(this.ticks[i], 'in-selection');
                        if (!this.options.range) {
                            if (this.options.selection === 'after' && percentage >= positionPercentages[0]) {
                                this._addClass(this.ticks[i], 'in-selection');
                            } else if (this.options.selection === 'before' && percentage <= positionPercentages[0]) {
                                this._addClass(this.ticks[i], 'in-selection');
                            }
                        } else if (percentage >= positionPercentages[0] && percentage <= positionPercentages[1]) {
                            this._addClass(this.ticks[i], 'in-selection');
                        }
                        if (this.tickLabels[i]) {
                            this.tickLabels[i].style[styleSize] = labelSize + 'px';
                            if (this.options.ticks_positions[i] !== undefined) {
                                this.tickLabels[i].style.position = 'absolute';
                                this.tickLabels[i].style[this.stylePos] = this.options.ticks_positions[i] + '%';
                                this.tickLabels[i].style[styleMargin] = -labelSize / 2 + 'px';
                            }
                        }
                    }
                }
                if (this.options.orientation === 'vertical') {
                    this.trackLow.style.top = '0';
                    this.trackLow.style.height = Math.min(positionPercentages[0], positionPercentages[1]) + '%';
                    this.trackSelection.style.top = Math.min(positionPercentages[0], positionPercentages[1]) + '%';
                    this.trackSelection.style.height = Math.abs(positionPercentages[0] - positionPercentages[1]) + '%';
                    this.trackHigh.style.bottom = '0';
                    this.trackHigh.style.height = (100 - Math.min(positionPercentages[0], positionPercentages[1]) - Math.abs(positionPercentages[0] - positionPercentages[1])) + '%';
                } else {
                    this.trackLow.style.left = '0';
                    this.trackLow.style.width = Math.min(positionPercentages[0], positionPercentages[1]) + '%';
                    this.trackSelection.style.left = Math.min(positionPercentages[0], positionPercentages[1]) + '%';
                    this.trackSelection.style.width = Math.abs(positionPercentages[0] - positionPercentages[1]) + '%';
                    this.trackHigh.style.right = '0';
                    this.trackHigh.style.width = (100 - Math.min(positionPercentages[0], positionPercentages[1]) - Math.abs(positionPercentages[0] - positionPercentages[1])) + '%';
                    var offset_min = this.tooltip_min.getBoundingClientRect();
                    var offset_max = this.tooltip_max.getBoundingClientRect();
                    if (offset_min.right > offset_max.left) {
                        this._removeClass(this.tooltip_max, 'top');
                        this._addClass(this.tooltip_max, 'bottom');
                        this.tooltip_max.style.top = 18 + 'px';
                    } else {
                        this._removeClass(this.tooltip_max, 'bottom');
                        this._addClass(this.tooltip_max, 'top');
                        this.tooltip_max.style.top = this.tooltip_min.style.top;
                    }
                }
                var formattedTooltipVal;
                if (this.options.range) {
                    formattedTooltipVal = this.options.formatter(this.options.value);
                    this._setText(this.tooltipInner, formattedTooltipVal);
                    this.tooltip.style[this.stylePos] = (positionPercentages[1] + positionPercentages[0]) / 2 + '%';
                    if (this.options.orientation === 'vertical') {
                        this._css(this.tooltip, 'margin-top', -this.tooltip.offsetHeight / 2 + 'px');
                    } else {
                        this._css(this.tooltip, 'margin-left', -this.tooltip.offsetWidth / 2 + 'px');
                    }
                    if (this.options.orientation === 'vertical') {
                        this._css(this.tooltip, 'margin-top', -this.tooltip.offsetHeight / 2 + 'px');
                    } else {
                        this._css(this.tooltip, 'margin-left', -this.tooltip.offsetWidth / 2 + 'px');
                    }
                    var innerTooltipMinText = this.options.formatter(this.options.value[0]);
                    this._setText(this.tooltipInner_min, innerTooltipMinText);
                    var innerTooltipMaxText = this.options.formatter(this.options.value[1]);
                    this._setText(this.tooltipInner_max, innerTooltipMaxText);
                    this.tooltip_min.style[this.stylePos] = positionPercentages[0] + '%';
                    if (this.options.orientation === 'vertical') {
                        this._css(this.tooltip_min, 'margin-top', -this.tooltip_min.offsetHeight / 2 + 'px');
                    } else {
                        this._css(this.tooltip_min, 'margin-left', -this.tooltip_min.offsetWidth / 2 + 'px');
                    }
                    this.tooltip_max.style[this.stylePos] = positionPercentages[1] + '%';
                    if (this.options.orientation === 'vertical') {
                        this._css(this.tooltip_max, 'margin-top', -this.tooltip_max.offsetHeight / 2 + 'px');
                    } else {
                        this._css(this.tooltip_max, 'margin-left', -this.tooltip_max.offsetWidth / 2 + 'px');
                    }
                } else {
                    formattedTooltipVal = this.options.formatter(this.options.value[0]);
                    this._setText(this.tooltipInner, formattedTooltipVal);
                    this.tooltip.style[this.stylePos] = positionPercentages[0] + '%';
                    if (this.options.orientation === 'vertical') {
                        this._css(this.tooltip, 'margin-top', -this.tooltip.offsetHeight / 2 + 'px');
                    } else {
                        this._css(this.tooltip, 'margin-left', -this.tooltip.offsetWidth / 2 + 'px');
                    }
                }
            },
            _removeProperty: function(element, prop) {
                if (element.style.removeProperty) {
                    element.style.removeProperty(prop);
                } else {
                    element.style.removeAttribute(prop);
                }
            },
            _mousedown: function(ev) {
                if (!this.options.enabled) {
                    return false;
                }
                this.offset = this._offset(this.sliderElem);
                this.size = this.sliderElem[this.sizePos];
                var percentage = this._getPercentage(ev);
                if (this.options.range) {
                    var diff1 = Math.abs(this.percentage[0] - percentage);
                    var diff2 = Math.abs(this.percentage[1] - percentage);
                    this.dragged = (diff1 < diff2) ? 0 : 1;
                } else {
                    this.dragged = 0;
                }
                this.percentage[this.dragged] = this.options.reversed ? 100 - percentage : percentage;
                this._layout();
                if (this.touchCapable) {
                    document.removeEventListener("touchmove", this.mousemove, false);
                    document.removeEventListener("touchend", this.mouseup, false);
                }
                if (this.mousemove) {
                    document.removeEventListener("mousemove", this.mousemove, false);
                }
                if (this.mouseup) {
                    document.removeEventListener("mouseup", this.mouseup, false);
                }
                this.mousemove = this._mousemove.bind(this);
                this.mouseup = this._mouseup.bind(this);
                if (this.touchCapable) {
                    document.addEventListener("touchmove", this.mousemove, false);
                    document.addEventListener("touchend", this.mouseup, false);
                }
                document.addEventListener("mousemove", this.mousemove, false);
                document.addEventListener("mouseup", this.mouseup, false);
                this.inDrag = true;
                var newValue = this._calculateValue();
                this._trigger('slideStart', newValue);
                this._setDataVal(newValue);
                this.setValue(newValue, false, true);
                this._pauseEvent(ev);
                if (this.options.focus) {
                    this._triggerFocusOnHandle(this.dragged);
                }
                return true;
            },
            _triggerFocusOnHandle: function(handleIdx) {
                if (handleIdx === 0) {
                    this.handle1.focus();
                }
                if (handleIdx === 1) {
                    this.handle2.focus();
                }
            },
            _keydown: function(handleIdx, ev) {
                if (!this.options.enabled) {
                    return false;
                }
                var dir;
                switch (ev.keyCode) {
                    case 37:
                    case 40:
                        dir = -1;
                        break;
                    case 39:
                    case 38:
                        dir = 1;
                        break;
                }
                if (!dir) {
                    return;
                }
                if (this.options.natural_arrow_keys) {
                    var ifVerticalAndNotReversed = (this.options.orientation === 'vertical' && !this.options.reversed);
                    var ifHorizontalAndReversed = (this.options.orientation === 'horizontal' && this.options.reversed);
                    if (ifVerticalAndNotReversed || ifHorizontalAndReversed) {
                        dir = -dir;
                    }
                }
                var val = this.options.value[handleIdx] + dir * this.options.step;
                if (this.options.range) {
                    val = [(!handleIdx) ? val : this.options.value[0], (handleIdx) ? val : this.options.value[1]];
                }
                this._trigger('slideStart', val);
                this._setDataVal(val);
                this.setValue(val, true, true);
                this._trigger('slideStop', val);
                this._setDataVal(val);
                this._layout();
                this._pauseEvent(ev);
                return false;
            },
            _pauseEvent: function(ev) {
                if (ev.stopPropagation) {
                    ev.stopPropagation();
                }
                if (ev.preventDefault) {
                    ev.preventDefault();
                }
                ev.cancelBubble = true;
                ev.returnValue = false;
            },
            _mousemove: function(ev) {
                if (!this.options.enabled) {
                    return false;
                }
                var percentage = this._getPercentage(ev);
                this._adjustPercentageForRangeSliders(percentage);
                this.percentage[this.dragged] = this.options.reversed ? 100 - percentage : percentage;
                this._layout();
                var val = this._calculateValue(true);
                this.setValue(val, true, true);
                return false;
            },
            _adjustPercentageForRangeSliders: function(percentage) {
                if (this.options.range) {
                    if (this.dragged === 0 && this.percentage[1] < percentage) {
                        this.percentage[0] = this.percentage[1];
                        this.dragged = 1;
                    } else if (this.dragged === 1 && this.percentage[0] > percentage) {
                        this.percentage[1] = this.percentage[0];
                        this.dragged = 0;
                    }
                }
            },
            _mouseup: function() {
                if (!this.options.enabled) {
                    return false;
                }
                if (this.touchCapable) {
                    document.removeEventListener("touchmove", this.mousemove, false);
                    document.removeEventListener("touchend", this.mouseup, false);
                }
                document.removeEventListener("mousemove", this.mousemove, false);
                document.removeEventListener("mouseup", this.mouseup, false);
                this.inDrag = false;
                if (this.over === false) {
                    this._hideTooltip();
                }
                var val = this._calculateValue(true);
                this._layout();
                this._trigger('slideStop', val);
                this._setDataVal(val);
                return false;
            },
            _calculateValue: function(snapToClosestTick) {
                var val;
                if (this.options.range) {
                    val = [this.options.min, this.options.max];
                    if (this.percentage[0] !== 0) {
                        val[0] = this._toValue(this.percentage[0]);
                        val[0] = this._applyPrecision(val[0]);
                    }
                    if (this.percentage[1] !== 100) {
                        val[1] = this._toValue(this.percentage[1]);
                        val[1] = this._applyPrecision(val[1]);
                    }
                } else {
                    val = this._toValue(this.percentage[0]);
                    val = parseFloat(val);
                    val = this._applyPrecision(val);
                }
                if (snapToClosestTick) {
                    var min = [val, Infinity];
                    for (var i = 0; i < this.options.ticks.length; i++) {
                        var diff = Math.abs(this.options.ticks[i] - val);
                        if (diff <= min[1]) {
                            min = [this.options.ticks[i], diff];
                        }
                    }
                    if (min[1] <= this.options.ticks_snap_bounds) {
                        return min[0];
                    }
                }
                return val;
            },
            _applyPrecision: function(val) {
                var precision = this.options.precision || this._getNumDigitsAfterDecimalPlace(this.options.step);
                return this._applyToFixedAndParseFloat(val, precision);
            },
            _getNumDigitsAfterDecimalPlace: function(num) {
                var match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if (!match) {
                    return 0;
                }
                return Math.max(0, (match[1] ? match[1].length : 0) - (match[2] ? +match[2] : 0));
            },
            _applyToFixedAndParseFloat: function(num, toFixedInput) {
                var truncatedNum = num.toFixed(toFixedInput);
                return parseFloat(truncatedNum);
            },
            _getPercentage: function(ev) {
                if (this.touchCapable && (ev.type === 'touchstart' || ev.type === 'touchmove')) {
                    ev = ev.touches[0];
                }
                var eventPosition = ev[this.mousePos];
                var sliderOffset = this.offset[this.stylePos];
                var distanceToSlide = eventPosition - sliderOffset;
                var percentage = (distanceToSlide / this.size) * 100;
                percentage = Math.round(percentage / this.percentage[2]) * this.percentage[2];
                return Math.max(0, Math.min(100, percentage));
            },
            _validateInputValue: function(val) {
                if (typeof val === 'number') {
                    return val;
                } else if (Array.isArray(val)) {
                    this._validateArray(val);
                    return val;
                } else {
                    throw new Error(ErrorMsgs.formatInvalidInputErrorMsg(val));
                }
            },
            _validateArray: function(val) {
                for (var i = 0; i < val.length; i++) {
                    var input = val[i];
                    if (typeof input !== 'number') {
                        throw new Error(ErrorMsgs.formatInvalidInputErrorMsg(input));
                    }
                }
            },
            _setDataVal: function(val) {
                var value = "value: '" + val + "'";
                this.element.setAttribute('data', value);
                this.element.setAttribute('value', val);
                this.element.value = val;
            },
            _trigger: function(evt, val) {
                val = (val || val === 0) ? val : undefined;
                var callbackFnArray = this.eventToCallbackMap[evt];
                if (callbackFnArray && callbackFnArray.length) {
                    for (var i = 0; i < callbackFnArray.length; i++) {
                        var callbackFn = callbackFnArray[i];
                        callbackFn(val);
                    }
                }
                if ($) {
                    this._triggerJQueryEvent(evt, val);
                }
            },
            _triggerJQueryEvent: function(evt, val) {
                var eventData = {
                    type: evt,
                    value: val
                };
                this.$element.trigger(eventData);
                this.$sliderElem.trigger(eventData);
            },
            _unbindJQueryEventHandlers: function() {
                this.$element.off();
                this.$sliderElem.off();
            },
            _setText: function(element, text) {
                if (typeof element.innerText !== "undefined") {
                    element.innerText = text;
                } else if (typeof element.textContent !== "undefined") {
                    element.textContent = text;
                }
            },
            _removeClass: function(element, classString) {
                var classes = classString.split(" ");
                var newClasses = element.className;
                for (var i = 0; i < classes.length; i++) {
                    var classTag = classes[i];
                    var regex = new RegExp("(?:\\s|^)" + classTag + "(?:\\s|$)");
                    newClasses = newClasses.replace(regex, " ");
                }
                element.className = newClasses.trim();
            },
            _addClass: function(element, classString) {
                var classes = classString.split(" ");
                var newClasses = element.className;
                for (var i = 0; i < classes.length; i++) {
                    var classTag = classes[i];
                    var regex = new RegExp("(?:\\s|^)" + classTag + "(?:\\s|$)");
                    var ifClassExists = regex.test(newClasses);
                    if (!ifClassExists) {
                        newClasses += " " + classTag;
                    }
                }
                element.className = newClasses.trim();
            },
            _offsetLeft: function(obj) {
                var offsetLeft = obj.offsetLeft;
                while ((obj = obj.offsetParent) && !isNaN(obj.offsetLeft)) {
                    offsetLeft += obj.offsetLeft;
                }
                return offsetLeft;
            },
            _offsetTop: function(obj) {
                var offsetTop = obj.offsetTop;
                while ((obj = obj.offsetParent) && !isNaN(obj.offsetTop)) {
                    offsetTop += obj.offsetTop;
                }
                return offsetTop;
            },
            _offset: function(obj) {
                return {
                    left: this._offsetLeft(obj),
                    top: this._offsetTop(obj)
                };
            },
            _css: function(elementRef, styleName, value) {
                if ($) {
                    $.style(elementRef, styleName, value);
                } else {
                    var style = styleName.replace(/^-ms-/, "ms-").replace(/-([\da-z])/gi, function(all, letter) {
                        return letter.toUpperCase();
                    });
                    elementRef.style[style] = value;
                }
            },
            _toValue: function(percentage) {
                return this.options.scale.toValue.apply(this, [percentage]);
            },
            _toPercentage: function(value) {
                return this.options.scale.toPercentage.apply(this, [value]);
            }
        };
        if ($) {
            var namespace = $.fn.slider ? 'bootstrapSlider' : 'slider';
            $.bridget(namespace, Slider);
        }
    })($);
    return Slider;
}));

/*===============================
https://www.equipmentone.com/e1_2/js/jquery.jcarousel.min.js
================================================================================*/
;
/*! jCarousel - v0.3.3 - 2015-02-28
 * http://sorgalla.com/jcarousel/
 * Copyright (c) 2006-2015 Jan Sorgalla; Licensed MIT */
(function(t) {
    "use strict";
    var i = t.jCarousel = {};
    i.version = "0.3.3";
    var s = /^([+\-]=)?(.+)$/;
    i.parseTarget = function(t) {
        var i = !1,
            e = "object" != typeof t ? s.exec(t) : null;
        return e ? (t = parseInt(e[2], 10) || 0, e[1] && (i = !0, "-=" === e[1] && (t *= -1))) : "object" != typeof t && (t = parseInt(t, 10) || 0), {
            target: t,
            relative: i
        }
    }, i.detectCarousel = function(t) {
        for (var i; t.length > 0;) {
            if (i = t.filter("[data-jcarousel]"), i.length > 0) return i;
            if (i = t.find("[data-jcarousel]"), i.length > 0) return i;
            t = t.parent()
        }
        return null
    }, i.base = function(s) {
        return {
            version: i.version,
            _options: {},
            _element: null,
            _carousel: null,
            _init: t.noop,
            _create: t.noop,
            _destroy: t.noop,
            _reload: t.noop,
            create: function() {
                return this._element.attr("data-" + s.toLowerCase(), !0).data(s, this), !1 === this._trigger("create") ? this : (this._create(), this._trigger("createend"), this)
            },
            destroy: function() {
                return !1 === this._trigger("destroy") ? this : (this._destroy(), this._trigger("destroyend"), this._element.removeData(s).removeAttr("data-" + s.toLowerCase()), this)
            },
            reload: function(t) {
                return !1 === this._trigger("reload") ? this : (t && this.options(t), this._reload(), this._trigger("reloadend"), this)
            },
            element: function() {
                return this._element
            },
            options: function(i, s) {
                if (0 === arguments.length) return t.extend({}, this._options);
                if ("string" == typeof i) {
                    if (s === void 0) return this._options[i] === void 0 ? null : this._options[i];
                    this._options[i] = s
                } else this._options = t.extend({}, this._options, i);
                return this
            },
            carousel: function() {
                return this._carousel || (this._carousel = i.detectCarousel(this.options("carousel") || this._element), this._carousel || t.error('Could not detect carousel for plugin "' + s + '"')), this._carousel
            },
            _trigger: function(i, e, r) {
                var n, o = !1;
                return r = [this].concat(r || []), (e || this._element).each(function() {
                    n = t.Event((s + ":" + i).toLowerCase()), t(this).trigger(n, r), n.isDefaultPrevented() && (o = !0)
                }), !o
            }
        }
    }, i.plugin = function(s, e) {
        var r = t[s] = function(i, s) {
            this._element = t(i), this.options(s), this._init(), this.create()
        };
        return r.fn = r.prototype = t.extend({}, i.base(s), e), t.fn[s] = function(i) {
            var e = Array.prototype.slice.call(arguments, 1),
                n = this;
            return "string" == typeof i ? this.each(function() {
                var r = t(this).data(s);
                if (!r) return t.error("Cannot call methods on " + s + " prior to initialization; " + 'attempted to call method "' + i + '"');
                if (!t.isFunction(r[i]) || "_" === i.charAt(0)) return t.error('No such method "' + i + '" for ' + s + " instance");
                var o = r[i].apply(r, e);
                return o !== r && o !== void 0 ? (n = o, !1) : void 0
            }) : this.each(function() {
                var e = t(this).data(s);
                e instanceof r ? e.reload(i) : new r(this, i)
            }), n
        }, r
    }
})(jQuery),
function(t, i) {
    "use strict";
    var s = function(t) {
        return parseFloat(t) || 0
    };
    t.jCarousel.plugin("jcarousel", {
        animating: !1,
        tail: 0,
        inTail: !1,
        resizeTimer: null,
        lt: null,
        vertical: !1,
        rtl: !1,
        circular: !1,
        underflow: !1,
        relative: !1,
        _options: {
            list: function() {
                return this.element().children().eq(0)
            },
            items: function() {
                return this.list().children()
            },
            animation: 400,
            transitions: !1,
            wrap: null,
            vertical: null,
            rtl: null,
            center: !1
        },
        _list: null,
        _items: null,
        _target: t(),
        _first: t(),
        _last: t(),
        _visible: t(),
        _fullyvisible: t(),
        _init: function() {
            var t = this;
            return this.onWindowResize = function() {
                t.resizeTimer && clearTimeout(t.resizeTimer), t.resizeTimer = setTimeout(function() {
                    t.reload()
                }, 100)
            }, this
        },
        _create: function() {
            this._reload(), t(i).on("resize.jcarousel", this.onWindowResize)
        },
        _destroy: function() {
            t(i).off("resize.jcarousel", this.onWindowResize)
        },
        _reload: function() {
            this.vertical = this.options("vertical"), null == this.vertical && (this.vertical = this.list().height() > this.list().width()), this.rtl = this.options("rtl"), null == this.rtl && (this.rtl = function(i) {
                if ("rtl" === ("" + i.attr("dir")).toLowerCase()) return !0;
                var s = !1;
                return i.parents("[dir]").each(function() {
                    return /rtl/i.test(t(this).attr("dir")) ? (s = !0, !1) : void 0
                }), s
            }(this._element)), this.lt = this.vertical ? "top" : "left", this.relative = "relative" === this.list().css("position"), this._list = null, this._items = null;
            var i = this.index(this._target) >= 0 ? this._target : this.closest();
            this.circular = "circular" === this.options("wrap"), this.underflow = !1;
            var s = {
                left: 0,
                top: 0
            };
            return i.length > 0 && (this._prepare(i), this.list().find("[data-jcarousel-clone]").remove(), this._items = null, this.underflow = this._fullyvisible.length >= this.items().length, this.circular = this.circular && !this.underflow, s[this.lt] = this._position(i) + "px"), this.move(s), this
        },
        list: function() {
            if (null === this._list) {
                var i = this.options("list");
                this._list = t.isFunction(i) ? i.call(this) : this._element.find(i)
            }
            return this._list
        },
        items: function() {
            if (null === this._items) {
                var i = this.options("items");
                this._items = (t.isFunction(i) ? i.call(this) : this.list().find(i)).not("[data-jcarousel-clone]")
            }
            return this._items
        },
        index: function(t) {
            return this.items().index(t)
        },
        closest: function() {
            var i, e = this,
                r = this.list().position()[this.lt],
                n = t(),
                o = !1,
                l = this.vertical ? "bottom" : this.rtl && !this.relative ? "left" : "right";
            return this.rtl && this.relative && !this.vertical && (r += this.list().width() - this.clipping()), this.items().each(function() {
                if (n = t(this), o) return !1;
                var a = e.dimension(n);
                if (r += a, r >= 0) {
                    if (i = a - s(n.css("margin-" + l)), !(0 >= Math.abs(r) - a + i / 2)) return !1;
                    o = !0
                }
            }), n
        },
        target: function() {
            return this._target
        },
        first: function() {
            return this._first
        },
        last: function() {
            return this._last
        },
        visible: function() {
            return this._visible
        },
        fullyvisible: function() {
            return this._fullyvisible
        },
        hasNext: function() {
            if (!1 === this._trigger("hasnext")) return !0;
            var t = this.options("wrap"),
                i = this.items().length - 1,
                s = this.options("center") ? this._target : this._last;
            return i >= 0 && !this.underflow && (t && "first" !== t || i > this.index(s) || this.tail && !this.inTail) ? !0 : !1
        },
        hasPrev: function() {
            if (!1 === this._trigger("hasprev")) return !0;
            var t = this.options("wrap");
            return this.items().length > 0 && !this.underflow && (t && "last" !== t || this.index(this._first) > 0 || this.tail && this.inTail) ? !0 : !1
        },
        clipping: function() {
            return this._element["inner" + (this.vertical ? "Height" : "Width")]()
        },
        dimension: function(t) {
            return t["outer" + (this.vertical ? "Height" : "Width")](!0)
        },
        scroll: function(i, s, e) {
            if (this.animating) return this;
            if (!1 === this._trigger("scroll", null, [i, s])) return this;
            t.isFunction(s) && (e = s, s = !0);
            var r = t.jCarousel.parseTarget(i);
            if (r.relative) {
                var n, o, l, a, h, u, c, f, d = this.items().length - 1,
                    _ = Math.abs(r.target),
                    p = this.options("wrap");
                if (r.target > 0) {
                    var g = this.index(this._last);
                    if (g >= d && this.tail) this.inTail ? "both" === p || "last" === p ? this._scroll(0, s, e) : t.isFunction(e) && e.call(this, !1) : this._scrollTail(s, e);
                    else if (n = this.index(this._target), this.underflow && n === d && ("circular" === p || "both" === p || "last" === p) || !this.underflow && g === d && ("both" === p || "last" === p)) this._scroll(0, s, e);
                    else if (l = n + _, this.circular && l > d) {
                        for (f = d, h = this.items().get(-1); l > f++;) h = this.items().eq(0), u = this._visible.index(h) >= 0, u && h.after(h.clone(!0).attr("data-jcarousel-clone", !0)), this.list().append(h), u || (c = {}, c[this.lt] = this.dimension(h), this.moveBy(c)), this._items = null;
                        this._scroll(h, s, e)
                    } else this._scroll(Math.min(l, d), s, e)
                } else if (this.inTail) this._scroll(Math.max(this.index(this._first) - _ + 1, 0), s, e);
                else if (o = this.index(this._first), n = this.index(this._target), a = this.underflow ? n : o, l = a - _, 0 >= a && (this.underflow && "circular" === p || "both" === p || "first" === p)) this._scroll(d, s, e);
                else if (this.circular && 0 > l) {
                    for (f = l, h = this.items().get(0); 0 > f++;) {
                        h = this.items().eq(-1), u = this._visible.index(h) >= 0, u && h.after(h.clone(!0).attr("data-jcarousel-clone", !0)), this.list().prepend(h), this._items = null;
                        var v = this.dimension(h);
                        c = {}, c[this.lt] = -v, this.moveBy(c)
                    }
                    this._scroll(h, s, e)
                } else this._scroll(Math.max(l, 0), s, e)
            } else this._scroll(r.target, s, e);
            return this._trigger("scrollend"), this
        },
        moveBy: function(t, i) {
            var e = this.list().position(),
                r = 1,
                n = 0;
            return this.rtl && !this.vertical && (r = -1, this.relative && (n = this.list().width() - this.clipping())), t.left && (t.left = e.left + n + s(t.left) * r + "px"), t.top && (t.top = e.top + n + s(t.top) * r + "px"), this.move(t, i)
        },
        move: function(i, s) {
            s = s || {};
            var e = this.options("transitions"),
                r = !!e,
                n = !!e.transforms,
                o = !!e.transforms3d,
                l = s.duration || 0,
                a = this.list();
            if (!r && l > 0) return a.animate(i, s), void 0;
            var h = s.complete || t.noop,
                u = {};
            if (r) {
                var c = {
                        transitionDuration: a.css("transitionDuration"),
                        transitionTimingFunction: a.css("transitionTimingFunction"),
                        transitionProperty: a.css("transitionProperty")
                    },
                    f = h;
                h = function() {
                    t(this).css(c), f.call(this)
                }, u = {
                    transitionDuration: (l > 0 ? l / 1e3 : 0) + "s",
                    transitionTimingFunction: e.easing || s.easing,
                    transitionProperty: l > 0 ? function() {
                        return n || o ? "all" : i.left ? "left" : "top"
                    }() : "none",
                    transform: "none"
                }
            }
            o ? u.transform = "translate3d(" + (i.left || 0) + "," + (i.top || 0) + ",0)" : n ? u.transform = "translate(" + (i.left || 0) + "," + (i.top || 0) + ")" : t.extend(u, i), r && l > 0 && a.one("transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd", h), a.css(u), 0 >= l && a.each(function() {
                h.call(this)
            })
        },
        _scroll: function(i, s, e) {
            if (this.animating) return t.isFunction(e) && e.call(this, !1), this;
            if ("object" != typeof i ? i = this.items().eq(i) : i.jquery === void 0 && (i = t(i)), 0 === i.length) return t.isFunction(e) && e.call(this, !1), this;
            this.inTail = !1, this._prepare(i);
            var r = this._position(i),
                n = this.list().position()[this.lt];
            if (r === n) return t.isFunction(e) && e.call(this, !1), this;
            var o = {};
            return o[this.lt] = r + "px", this._animate(o, s, e), this
        },
        _scrollTail: function(i, s) {
            if (this.animating || !this.tail) return t.isFunction(s) && s.call(this, !1), this;
            var e = this.list().position()[this.lt];
            this.rtl && this.relative && !this.vertical && (e += this.list().width() - this.clipping()), this.rtl && !this.vertical ? e += this.tail : e -= this.tail, this.inTail = !0;
            var r = {};
            return r[this.lt] = e + "px", this._update({
                target: this._target.next(),
                fullyvisible: this._fullyvisible.slice(1).add(this._visible.last())
            }), this._animate(r, i, s), this
        },
        _animate: function(i, s, e) {
            if (e = e || t.noop, !1 === this._trigger("animate")) return e.call(this, !1), this;
            this.animating = !0;
            var r = this.options("animation"),
                n = t.proxy(function() {
                    this.animating = !1;
                    var t = this.list().find("[data-jcarousel-clone]");
                    t.length > 0 && (t.remove(), this._reload()), this._trigger("animateend"), e.call(this, !0)
                }, this),
                o = "object" == typeof r ? t.extend({}, r) : {
                    duration: r
                },
                l = o.complete || t.noop;
            return s === !1 ? o.duration = 0 : t.fx.speeds[o.duration] !== void 0 && (o.duration = t.fx.speeds[o.duration]), o.complete = function() {
                n(), l.call(this)
            }, this.move(i, o), this
        },
        _prepare: function(i) {
            var e, r, n, o, l = this.index(i),
                a = l,
                h = this.dimension(i),
                u = this.clipping(),
                c = this.vertical ? "bottom" : this.rtl ? "left" : "right",
                f = this.options("center"),
                d = {
                    target: i,
                    first: i,
                    last: i,
                    visible: i,
                    fullyvisible: u >= h ? i : t()
                };
            if (f && (h /= 2, u /= 2), u > h)
                for (;;) {
                    if (e = this.items().eq(++a), 0 === e.length) {
                        if (!this.circular) break;
                        if (e = this.items().eq(0), i.get(0) === e.get(0)) break;
                        if (r = this._visible.index(e) >= 0, r && e.after(e.clone(!0).attr("data-jcarousel-clone", !0)), this.list().append(e), !r) {
                            var _ = {};
                            _[this.lt] = this.dimension(e), this.moveBy(_)
                        }
                        this._items = null
                    }
                    if (o = this.dimension(e), 0 === o) break;
                    if (h += o, d.last = e, d.visible = d.visible.add(e), n = s(e.css("margin-" + c)), u >= h - n && (d.fullyvisible = d.fullyvisible.add(e)), h >= u) break
                }
            if (!this.circular && !f && u > h)
                for (a = l;;) {
                    if (0 > --a) break;
                    if (e = this.items().eq(a), 0 === e.length) break;
                    if (o = this.dimension(e), 0 === o) break;
                    if (h += o, d.first = e, d.visible = d.visible.add(e), n = s(e.css("margin-" + c)), u >= h - n && (d.fullyvisible = d.fullyvisible.add(e)), h >= u) break
                }
            return this._update(d), this.tail = 0, f || "circular" === this.options("wrap") || "custom" === this.options("wrap") || this.index(d.last) !== this.items().length - 1 || (h -= s(d.last.css("margin-" + c)), h > u && (this.tail = h - u)), this
        },
        _position: function(t) {
            var i = this._first,
                s = i.position()[this.lt],
                e = this.options("center"),
                r = e ? this.clipping() / 2 - this.dimension(i) / 2 : 0;
            return this.rtl && !this.vertical ? (s -= this.relative ? this.list().width() - this.dimension(i) : this.clipping() - this.dimension(i), s += r) : s -= r, !e && (this.index(t) > this.index(i) || this.inTail) && this.tail ? (s = this.rtl && !this.vertical ? s - this.tail : s + this.tail, this.inTail = !0) : this.inTail = !1, -s
        },
        _update: function(i) {
            var s, e = this,
                r = {
                    target: this._target,
                    first: this._first,
                    last: this._last,
                    visible: this._visible,
                    fullyvisible: this._fullyvisible
                },
                n = this.index(i.first || r.first) < this.index(r.first),
                o = function(s) {
                    var o = [],
                        l = [];
                    i[s].each(function() {
                        0 > r[s].index(this) && o.push(this)
                    }), r[s].each(function() {
                        0 > i[s].index(this) && l.push(this)
                    }), n ? o = o.reverse() : l = l.reverse(), e._trigger(s + "in", t(o)), e._trigger(s + "out", t(l)), e["_" + s] = i[s]
                };
            for (s in i) o(s);
            return this
        }
    })
}(jQuery, window),
function(t) {
    "use strict";
    t.jcarousel.fn.scrollIntoView = function(i, s, e) {
        var r, n = t.jCarousel.parseTarget(i),
            o = this.index(this._fullyvisible.first()),
            l = this.index(this._fullyvisible.last());
        if (r = n.relative ? 0 > n.target ? Math.max(0, o + n.target) : l + n.target : "object" != typeof n.target ? n.target : this.index(n.target), o > r) return this.scroll(r, s, e);
        if (r >= o && l >= r) return t.isFunction(e) && e.call(this, !1), this;
        for (var a, h = this.items(), u = this.clipping(), c = this.vertical ? "bottom" : this.rtl ? "left" : "right", f = 0;;) {
            if (a = h.eq(r), 0 === a.length) break;
            if (f += this.dimension(a), f >= u) {
                var d = parseFloat(a.css("margin-" + c)) || 0;
                f - d !== u && r++;
                break
            }
            if (0 >= r) break;
            r--
        }
        return this.scroll(r, s, e)
    }
}(jQuery),
function(t) {
    "use strict";
    t.jCarousel.plugin("jcarouselControl", {
        _options: {
            target: "+=1",
            event: "click",
            method: "scroll"
        },
        _active: null,
        _init: function() {
            this.onDestroy = t.proxy(function() {
                this._destroy(), this.carousel().one("jcarousel:createend", t.proxy(this._create, this))
            }, this), this.onReload = t.proxy(this._reload, this), this.onEvent = t.proxy(function(i) {
                i.preventDefault();
                var s = this.options("method");
                t.isFunction(s) ? s.call(this) : this.carousel().jcarousel(this.options("method"), this.options("target"))
            }, this)
        },
        _create: function() {
            this.carousel().one("jcarousel:destroy", this.onDestroy).on("jcarousel:reloadend jcarousel:scrollend", this.onReload), this._element.on(this.options("event") + ".jcarouselcontrol", this.onEvent), this._reload()
        },
        _destroy: function() {
            this._element.off(".jcarouselcontrol", this.onEvent), this.carousel().off("jcarousel:destroy", this.onDestroy).off("jcarousel:reloadend jcarousel:scrollend", this.onReload)
        },
        _reload: function() {
            var i, s = t.jCarousel.parseTarget(this.options("target")),
                e = this.carousel();
            if (s.relative) i = e.jcarousel(s.target > 0 ? "hasNext" : "hasPrev");
            else {
                var r = "object" != typeof s.target ? e.jcarousel("items").eq(s.target) : s.target;
                i = e.jcarousel("target").index(r) >= 0
            }
            return this._active !== i && (this._trigger(i ? "active" : "inactive"), this._active = i), this
        }
    })
}(jQuery),
function(t) {
    "use strict";
    t.jCarousel.plugin("jcarouselPagination", {
        _options: {
            perPage: null,
            item: function(t) {
                return '<a href="#' + t + '">' + t + "</a>"
            },
            event: "click",
            method: "scroll"
        },
        _carouselItems: null,
        _pages: {},
        _items: {},
        _currentPage: null,
        _init: function() {
            this.onDestroy = t.proxy(function() {
                this._destroy(), this.carousel().one("jcarousel:createend", t.proxy(this._create, this))
            }, this), this.onReload = t.proxy(this._reload, this), this.onScroll = t.proxy(this._update, this)
        },
        _create: function() {
            this.carousel().one("jcarousel:destroy", this.onDestroy).on("jcarousel:reloadend", this.onReload).on("jcarousel:scrollend", this.onScroll), this._reload()
        },
        _destroy: function() {
            this._clear(), this.carousel().off("jcarousel:destroy", this.onDestroy).off("jcarousel:reloadend", this.onReload).off("jcarousel:scrollend", this.onScroll), this._carouselItems = null
        },
        _reload: function() {
            var i = this.options("perPage");
            if (this._pages = {}, this._items = {}, t.isFunction(i) && (i = i.call(this)), null == i) this._pages = this._calculatePages();
            else
                for (var s, e = parseInt(i, 10) || 0, r = this._getCarouselItems(), n = 1, o = 0;;) {
                    if (s = r.eq(o++), 0 === s.length) break;
                    this._pages[n] = this._pages[n] ? this._pages[n].add(s) : s, 0 === o % e && n++
                }
            this._clear();
            var l = this,
                a = this.carousel().data("jcarousel"),
                h = this._element,
                u = this.options("item"),
                c = this._getCarouselItems().length;
            t.each(this._pages, function(i, s) {
                var e = l._items[i] = t(u.call(l, i, s));
                e.on(l.options("event") + ".jcarouselpagination", t.proxy(function() {
                    var t = s.eq(0);
                    if (a.circular) {
                        var e = a.index(a.target()),
                            r = a.index(t);
                        parseFloat(i) > parseFloat(l._currentPage) ? e > r && (t = "+=" + (c - e + r)) : r > e && (t = "-=" + (e + (c - r)))
                    }
                    a[this.options("method")](t)
                }, l)), h.append(e)
            }), this._update()
        },
        _update: function() {
            var i, s = this.carousel().jcarousel("target");
            t.each(this._pages, function(t, e) {
                return e.each(function() {
                    return s.is(this) ? (i = t, !1) : void 0
                }), i ? !1 : void 0
            }), this._currentPage !== i && (this._trigger("inactive", this._items[this._currentPage]), this._trigger("active", this._items[i])), this._currentPage = i
        },
        items: function() {
            return this._items
        },
        reloadCarouselItems: function() {
            return this._carouselItems = null, this
        },
        _clear: function() {
            this._element.empty(), this._currentPage = null
        },
        _calculatePages: function() {
            for (var t, i, s = this.carousel().data("jcarousel"), e = this._getCarouselItems(), r = s.clipping(), n = 0, o = 0, l = 1, a = {};;) {
                if (t = e.eq(o++), 0 === t.length) break;
                i = s.dimension(t), n + i > r && (l++, n = 0), n += i, a[l] = a[l] ? a[l].add(t) : t
            }
            return a
        },
        _getCarouselItems: function() {
            return this._carouselItems || (this._carouselItems = this.carousel().jcarousel("items")), this._carouselItems
        }
    })
}(jQuery),
function(t) {
    "use strict";
    t.jCarousel.plugin("jcarouselAutoscroll", {
        _options: {
            target: "+=1",
            interval: 3e3,
            autostart: !0
        },
        _timer: null,
        _init: function() {
            this.onDestroy = t.proxy(function() {
                this._destroy(), this.carousel().one("jcarousel:createend", t.proxy(this._create, this))
            }, this), this.onAnimateEnd = t.proxy(this.start, this)
        },
        _create: function() {
            this.carousel().one("jcarousel:destroy", this.onDestroy), this.options("autostart") && this.start()
        },
        _destroy: function() {
            this.stop(), this.carousel().off("jcarousel:destroy", this.onDestroy)
        },
        start: function() {
            return this.stop(), this.carousel().one("jcarousel:animateend", this.onAnimateEnd), this._timer = setTimeout(t.proxy(function() {
                this.carousel().jcarousel("scroll", this.options("target"))
            }, this), this.options("interval")), this
        },
        stop: function() {
            return this._timer && (this._timer = clearTimeout(this._timer)), this.carousel().off("jcarousel:animateend", this.onAnimateEnd), this
        }
    })
}(jQuery);

/*===============================
https://www.equipmentone.com/e1_2/js/listingUtils.js
================================================================================*/
;
var pubNubError = true;
var pubnub = PUBNUB.init({
    subscribe_key: pubnubSubKey,
    ssl: pubnubSSL
});

function trim_str(s) {
    return s.replace(/^\s+|\s+$/, '');
}
pubnub.subscribe({
    channel: pubnubChannels,
    callback: function(data) {
        try {
            var page = $('#jsMyonePageName').val();
            if (page == 'PaymentCreditCard' || page == 'PaymentECheck') {
                return;
            }
            if (page == 'ListingDetails') {
                fncUpdateUserListings(data);
                fncUpdateOfferBoard(data);
                fncUpdatePlpViewed(data);
                fncUpdateListingIteration(data);
            }
            if (pubnubLogData) {
                console.log("Response: " + JSON.stringify(data));
            }
            fncCheckPubNubPayload(data, page);
            if (fncIsListingRendered(data.LISTINGID)) {
                if ($('#listing-' + data.LISTINGID).hasClass('jsSystemException')) {
                    $('.jsErrorReload').hide();
                    $('#listing-' + data.LISTINGID).removeClass('jsSystemException');
                }
                fncInitListingConfig();
                fncConfigureListing(page);
                fncUpdateListing(data);
                $('#listing-' + data.LISTINGID + ':in-viewport').find('.e1OfferConsole').growBox(10, 5, 600);
                $('#listing-' + data.LISTINGID + ':in-viewport').find('.jsRibbonText').growText(600);
            }
        } catch (e) {
            if (fncIsListingRendered(data.LISTINGID)) {
                $('.jsErrorReload').show();
                $('#listing-' + data.LISTINGID).addClass('jsSystemException');
            }
            console.log("Pubnub Call System Exception :" + e.message);
            fncGeneratePubNubErrorLog(data, e, page);
        }
    },
    connect: function(data) {
        if (pubnubLogData) {
            console.log("Connected To: " + data);
        }
        if (typeof searchInterval != 'undefined') {
            clearInterval(searchInterval);
        }
    },
    error: function(err) {
        if (pubnubLogData) {
            console.log("Error: " + JSON.stringify(err));
        }
        if (pubNubError == false) {
            $('.networkReload').show();
        }
    },
    disconnect: function(data) {
        console.log("Pubnub Connection Lost : " + data);
        if (pubNubError != false) {
            $('.networkReload').show();
        }
    },
    reconnect: function(data) {
        console.log("Pubnub Reconnected To : " + data);
        $('.networkReload').hide();
    }
});
$(window).on('beforeunload', function() {
    pubNubError = false;
    pubnub.unsubscribe({
        channel: pubnubChannels
    });
});

function fncUpdateUserListings(data) {
    var pubNubListingId = data.LISTINGID;
    var currentListingId = listingDetails.LISTINGID;
    if (pubNubListingId != currentListingId) {
        return;
    }
    var rank = UserListings.fncGetUserRank(data.HIGHBIDUSERID);
    if (rank == 1)
        UserListings.fncAddListing(currentListingId, 'BUYINGACTIVE', data.LEADOFFERPRICE);
}

function fncUpdateListingIteration(data) {
    var pubNubListingId = data.LISTINGID;
    var currentListingId = listingDetails.LISTINGID;
    var listingid = listingDetails.LISTINGID;
    if (pubNubListingId != currentListingId) {
        return;
    }
    $('.jsAuctionPeriodText span').removeClass('green-text');
    $('.jsAuctionPeriodText span').removeClass('orange-text');
    $('.jsAuctionPeriodText span').removeClass('red');
    $('.jsAuctionPeriodCount').html(data.LISTINGCLOSEITERATION);
    switch (data.LISTINGCLOSEITERATION) {
        case 0:
        case 1:
            $('.jsAuctionPeriodText span').addClass('green-text');
            break;
        case 2:
            $('.jsAuctionPeriodText span').addClass('orange-text');
            break;
        case 3:
            $('.jsAuctionPeriodText span').addClass('red');
            break;
    }
}
jQuery.fn.growBox = function(intHeight, intBorderWidth, intDuration) {
    intOriginalBW = $(this).css('borderWidth') != '' ? parseInt($(this).css('borderWidth')) : parseInt($(this).css('border-top-width'));
    $(this).delay(intDuration).animate({
        borderWidth: intBorderWidth
    }, intDuration, 'linear', function() {
        $(this).delay(intDuration / 2).animate({
            borderWidth: intOriginalBW
        }, intDuration, 'linear')
    });
    return this;
};
jQuery.fn.growText = function(intDuration) {
    intFontSize = parseInt($(this).css('font-size'));
    $(this).delay(intDuration).animate({
        fontSize: intFontSize * 1.2
    }, intDuration, 'linear', function() {
        $(this).delay(intDuration / 2).animate({
            fontSize: intFontSize
        }, intDuration, 'linear')
    })
    return this;
}

function fncCheckPubNubPayload(data, page) {
    if (data.LISTINGID == '' || data.ALLOWEDCOMMERCEMETHODS == '') {
        var e = {};
        e.message = 'Incomplete payload from pubnub';
        e.stack = "";
        fncGeneratePubNubErrorLog(data, e, page);
    }
}

function fncGeneratePubNubErrorLog(pubnubPayload, e, page) {
    var pageConfig = {};
    pageConfig.defaultAjaxController = '/home?task=ajax';
    var params = {};
    params.data = {};
    params.data.log = "\n ***** PUBNUB PAYLOAD PROCESSING EXCEPTION **** \n " + "Pubnub  PayLoad : " + JSON.stringify(pubnubPayload) + "\n  " + "Error Message :" + e.message + " \n  " + "Exception Stack :" + e.stack + "\n " + "User Agent :" + JSON.stringify($.browser) + "\n  " + "Error Occured Page :" + page + "\n" + "***** PUBNUB PAYLOAD  EXCEPTION END ****";
    params.data.version = 1;
    params.data.curl = '/log';
    params.url = pageConfig.defaultAjaxController;
    fncAjax(params);
}

function fncInitListing(params, alterbg, $container, selfservice) {
    fncValidateConfiguration();
    if (selfservice == true) {
        fncRenderSelfserviceListing(params, alterbg, $container);
    }
    if (fncIsListingRendered(params.LISTINGID)) {
        fncUpdateListing(params);
    } else {
        fncRenderListing(params, alterbg, $container);
    }
}

function fncIsListingRendered(listingId) {
    var count = $('#listing-' + listingId).length;
    if (count > 0) {
        return true;
    } else {
        return false;
    }
}

function fncUpdateListing(params) {
    var $listing = $('#listing-' + params.LISTINGID);
    var update = true;
    $listing.find('.salesStatus').val(params.SALESTATUS);
    $listing.find('.auctionEndDateUtc').val(params.AUCTIONENDDATETIMEUTC);
    for (property in listingConfig) {
        if (listingConfig[property] == true) {
            fncDisplayProperty(property, 'fncConfigureListing_' + property, $listing, params, update);
        }
    }
    var lastUpdated = +new Date;
}

function fncRenderSelfserviceListing(params, alterbg, $container) {
    listingConfig.displayRelativeImage = false;
    var $listing = $('div.jsListingTemplate').clone();
    var update = false;
    $listing.removeClass('jsListingTemplate');
    $listing.addClass('jsListingRendered');
    if (alterbg % 2 == 1) {
        $listing.addClass('alter-bg');
    }
    $listing.attr('id', 'listing-' + params.LISTINGID);
    $listing.find('.jsListingNumber').find('.e1-label').html('Asset Number');
    $listing.find('.jsLotID').html(params.LISTINGID);
    $listing.find('.jsTitle').html(params.TITLE);
    $listing.find('.jsTitle').attr('href', '/createlisting?task=listing_edit&listingId=' + params.LISTINGID);
    $listing.find('.jsImageLink').attr('href', '/createlisting?task=listing_edit&listingId=' + params.LISTINGID);
    if (params.FEATUREDIMAGEURL != null) {
        $listing.find('.jsListingImage').attr('src', params.FEATUREDIMAGEURL);
    } else {
        $listing.find('.jsListingImage').attr('src', CDN_BASE_URL + '/images/na.png');
        $listing.find('.jsListingImage').addClass('img-responsive');
    }
    if (params.LOCATION == null || params.LOCATION == '') {
        params.LOCATION = 'N/A';
    }
    if (params.UNITNUMBER == null | params.UNITNUMBER == '') {
        params.UNITNUMBER = 'N/A';
    }
    if (params.SERIALNUMBER == null || params.SERIALNUMBER == '') {
        params.SERIALNUMBER = 'N/A';
    }
    if (params.TITLE == null | params.TITLE == '') {
        if (params.CATEGORYCODE == -1) {
            params.TITLE = 'Material';
        } else {
            params.TITLE = params.FAIMINDUSTRY + " " + params.FAIMCATEGORY;
        }
    }
    $listing.find('.jsMarketPlace').html('<img alt="" class="img-responsive" src="' + CDN_BASE_URL + '/images/u1.png">');
    if (params.STATUS == 'submitted') {
        $listing.find('.jsTitle').on('click', function() {
            return false;
        });
        $listing.find('.jsListingImage').on('click', function() {
            return false;
        });
        $('.jsTitle').addClass('jsCursorDefault');
        $('.jsListingImage').addClass('jsCursorDefault');
    }
    $container.append($listing);
    for (property in listingConfig) {
        if (listingConfig[property] == true) {
            fncDisplayProperty(property, 'fncConfigureListing_' + property, $listing, params, update);
        }
    }
    $listing.show();
}

function fncRenderListingDetails(params, $container) {
    var $listing = $container;
    var update = false;
    $listing.attr('id', 'listing-' + params.LISTINGID);
    send2Monetate.pageType = 'detail';
    send2Monetate.pushProduct(params.LISTINGID);
    send2Monetate.addProductDetails();
    $listing.find('.jsTitle').html(params.TITLE);
    $listing.find('.jsTitle').attr('href', "/listing?listingid=" + params.LISTINGID);
    if (displayVideo == false) {
        if (params.FEATUREDIMAGEURL != '' && params.FEATUREDIMAGEURL != undefined) {
            $listing.find('.jsListingImage').attr('src', params.FEATUREDIMAGEURL.replace('small', 'large'));
        } else {
            $listing.find('.jsListingImage').attr('src', '/images/na.png');
        }
    }
    for (property in listingConfig) {
        if (listingConfig[property] == true) {
            fncDisplayProperty(property, 'fncConfigureListing_' + property, $listing, params, update);
        }
    }
    fncdisplayDescription($listing, params);
    fncdisplayCondition($listing, params);
}

function fncdisplayDescription($listing, params) {
    if (params.DESCRIPTION == undefined || params.DESCRIPTION == '') {
        params.DESCRIPTION = '';
    }
    if (params.DESCRIPTION == '')
        $listing.find('.lotDiscription').addClass('e1Hidden');
    else
        $listing.find('.jsDescription').html(params.DESCRIPTION);
}

function fncdisplayCondition($listing, params) {
    if (params.CONDITION == undefined || params.CONDITION == '')
        $listing.find('.e1LotCondition').addClass('e1Hidden');
}

function fncRenderListing(params, alterbg, $container) {
    var $listing = $('div.jsListingTemplate').clone();
    var update = false;
    $listing.removeClass('jsListingTemplate');
    $listing.addClass('jsListingRendered');
    if (alterbg % 2 == 1) {
        $listing.addClass('alter-bg');
    }
    $listing.attr('id', 'listing-' + params.LISTINGID);
    send2Monetate.pushProduct(params.LISTINGID);
    var marketPlace = VENUE['allMarketPlaces'];
    if (params.MARKETPLACE != "" && params.MARKETPLACE != undefined) {
        for (var i = 0; i < (marketPlace).length; i++) {
            marketPlace[i] = marketPlace[i].replace(" ", '-');
            var paramsMarketplace = (params.MARKETPLACE).replace(" ", '-');
            if (marketPlace[i] == paramsMarketplace) {
                $listing.find('.jsMarketPlace').html('<span class="refinevalue filter' + marketPlace[i] + '"></span>');
            }
        }
    }
    $listing.find('.jsTitle').html(params.TITLE);
    $listing.find('.jsTitle').attr('href', "/listing?listingid=" + params.LISTINGID);
    if (params.FEATUREDIMAGEURL != '' && params.FEATUREDIMAGEURL != undefined) {
        $listing.find('.jsListingImage').attr('src', CDN_IMAGES_BASE + '/' + params.FEATUREDIMAGEURL);
        $listing.find('.jsListingImage').addClass('img-responsive');
    } else if (params.FEATUREDIMAGE != '' && params.FEATUREDIMAGE != undefined) {
        if (params.EVENTID != undefined) {
            var eventId = params.EVENTID;
        } else {
            var strLot = params.LOTNUMBER;
            var arrLot = strLot.split('-');
            var eventId = arrLot[0].replace('EQ', '');
        }
        if (eventId != undefined)
            $listing.find('.jsListingImage').attr('src', CDN_IMAGES_BASE + "/" + eventId + "/" + params.LISTINGID + "/large/" + params.FEATUREDIMAGE);
        else {
            $listing.find('.jsListingImage').attr('src', CDN_BASE_URL + '/images/na.png');
            $listing.find('.jsListingImage').addClass('img-responsive');
        }
    } else {
        $listing.find('.jsListingImage').attr('src', CDN_BASE_URL + '/images/na.png');
        $listing.find('.jsListingImage').addClass('img-responsive');
    }
    $listing.find('.jsTitle').html(params.TITLE);
    $container.append($listing);
    for (property in listingConfig) {
        if (listingConfig[property] == true) {
            fncDisplayProperty(property, 'fncConfigureListing_' + property, $listing, params, update);
        }
    }
    if ((params.MARKETPLACE == 'RBA') || (params.MARKETPLACE == 'SS')) {
        $listing.find('.jsDefiniteSaleBuyer').addClass('e1Hidden');
    }
    $listing.show();
    if ($listing.find('.jsMarketPlace').html() == '') {
        $listing.find('.jsMarketPlace').hide();
    }
}

function fncDisplayProperty(property, initMethod, $listing, params, update) {
    var fn = window[initMethod];
    if (typeof fn == 'function') {
        fn($listing, params, update);
    }
    return false;
}

function fncInitEvent(params) {
    fncValidateConfiguration();
    var currentEvent = listingConfig.eventId;
    var $container = $('#event-' + params.EVENTID);
    if ($container.length > 0) {
        return;
    }
    console.log(params);
    listingConfig.eventId = params.EVENTID;
    var $header = $('.jsEventTemplate').clone();
    $header.removeClass('jsEventTemplate');
    $header.addClass('jsEventRendered');
    if (typeof params.EVENTPOB != 'undefined') {
        var total = 0;
        for (i in params.LISTINGS) {
            total = fncCurrencyText('$', params.LISTINGS[i].CURRENCYLOCALESHORT, params.LISTINGS[i].LEADOFFERPRICE);
        }
        $header.find('.jsEventTotal').html(total);
    } else {
        var EventTotalAmount = fncCurrencyText('$', params.LISTINGS[0].CURRENCYLOCALESHORT, params.EVENTINVOICEAMOUNT);
        $header.find('.jsEventTotal').html(EventTotalAmount);
    }
    $header.find('.jsEventTotal').autoNumeric('init', {
        aSign: '$',
        aSep: ',',
        aDec: '.',
        mDec: '2',
        mRound: 'D',
        lZero: 'deny'
    });
    if (listingConfig.displayEventPaymentOptions == true) {
        $header.find('.jsPaymentOptions').show();
        if (params.EVENTINVOICEAMOUNT < 7500) {
            if (params.PAYMENT_CREDIT == 1) {
                $header.find('.jsCreditCardButton input:submit').show();
                $header.find('.jsCreditCardButton').attr('action', '/myone?view=payment_creditcard');
            }
            if (params.PAYMENT_ECHECK == 1) {
                $header.find('.jsECheckButton input:submit').show();
                $header.find('.jsECheckButton').attr('action', '/myone?view=payment_echeck');
            }
        }
        if (params.PAYMENT_WIRE == 1) {
            $header.find('.jsWireButton input:submit').show();
            $header.find('.jsWireButton').attr('action', '/myone?view=payment_wire');
        }
        $header.find('.jsPaymentPrice').val(params.EVENTINVOICEAMOUNT);
        $header.find('.jsCurrencyShort').val(params.LISTINGS[0].CURRENCYLOCALESHORT);
        $header.find('.jsWireEventId').val(params.EVENTID);
        $header.find('.jsWirePage').val(pageConfig.page);
        $header.find('.jsWireMaxResults').val(pageConfig.maxresults);
        var invoiceIds = '';
        for (k in params.EVENTINVOICEIDS) {
            invoiceIds += params.EVENTINVOICEIDS[k];
            invoiceIds += ',';
        }
        invoiceIds = invoiceIds.substring(0, invoiceIds.length - 1);
        $header.find('.jsInvoiceIds').val(invoiceIds);
    }
    if (listingConfig.displayRemovalEventInfo == true) {
        $header.find('.jsReleaseDoc').show();
        $header.find('.jsSellerSA').show();
        if (params.INVOICE != undefined) {
            $header.find('.jsRemovalLabel').html('I REMOVED THESE');
        } else {
            $header.find('.jsRemovalLabel').html('EQUIPMENT REMOVED');
        }
        $header.find('.jsRemovalCheckbox').show();
    }
    if (listingConfig.displayEventInvoice == true) {
        $header.find('.jsEventInvoice').show();
    }
    if (listingConfig.displayEventPrice == true) {
        $header.find('.jsEventPrice').show();
    }
    $header.attr('id', 'event-' + listingConfig.eventId);
    $header.show();
    $header.find('.jsEventId h4').html("EQ" + listingConfig.eventId);
    $('#listingContainer').append($header);
}

function fncRenderPagination(response) {
    var numPerPage = 5;
    var intStart = 1;
    var totalListings = response.LISTINGCOUNT;
    var numPages = Math.ceil(totalListings / numPerPage);
    var currentPage = pageConfig.page;
    var previousPage = ((intStart - numPerPage) <= 0) ? 0 : intStart - numPerPage;
    var nextPage = currentPage + 1;
    var lastPage = Math.ceil(totalListings / numPerPage);
    var paginationText = $('<li>' + currentPage + '</li>');
    $('.jsPaginationRendered').remove();
    var paginationControls = $('.jsPaginationTemplate2 .e1SearchPagination').clone(true);
    paginationControls.empty();
    paginationControls.addClass('jsPaginationRendered')
    var includePreviousElipses = false;
    var includeNextElipses = false;
    paginationControls.append('<div class="col-md-24"><div class="row"></div></div>');
    if (numPages > 3) {
        if (currentPage < lastPage - 2) {
            includeNextElipses = true;
        }
        if (currentPage > 3) {
            includePreviousElipses = true;
        }
    }
    if (numPages > 1) {
        $('.page-section').removeClass('e1Hidden')
        paginationControls.find('.row').append('<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3 previous-page pagePrev" ></div>');
        if (currentPage > 1) {
            var pageStart = ((currentPage - 1) * numPerPage) - numPerPage;
            var previousLink = $('<a href="#" class="pagePrev" data-startVal="' + pageStart + '"><i class="fa fa-arrow-left">&nbsp;</i> <span class="hidden-xs">Previous Page</span></a>');
            paginationControls.find('.previous-page').append(previousLink);
        }
        paginationControls.find('.row').append('<div class="col-lg-12 col-md-12 col-sm-12 col-xs-18 text-center page-number"></div>');
        if (currentPage != 1) {
            paginationControls.find('.text-center').append('<a href="#" class="pageFirst" data-startVal="' + 0 + '"">1</a> ');
        }
        if (includePreviousElipses == true) {
            paginationControls.find('.text-center').append('<span class="page-dots">...</span> ');
        }
        if (currentPage > 2) {
            paginationControls.find('.text-center').append('<a href="#" class="pagePrev" data-startVal="' + ((currentPage - 2) * numPerPage) + '">' + (currentPage - 1) + '</a> ');
        }
        paginationControls.find('.text-center').append('<a href="javascript:void(0)" class="page-active" data-startVal="' + ((currentPage - 1) * numPerPage) + '">' + currentPage + '</a> ');
        if (currentPage < (lastPage - 1)) {
            paginationControls.find('.text-center').append('<a href="#" class="pageNext" data-startVal="' + ((currentPage) * numPerPage) + '">' + (currentPage + 1) + '</a> ');
        }
        if (includeNextElipses == true) {
            paginationControls.find('.text-center').append('<span class="page-dots">...</span> ');
        }
        if (currentPage != lastPage) {
            paginationControls.find('.text-center').append('<a href="javascript:void(0)" class="pageLast" data-startVal="' + ((lastPage - 1) * numPerPage) + '">' + lastPage + '</a> ');
        }
        if (currentPage != lastPage) {
            var nextPageLink = $('<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3 pull-right text-right next-page pageNext" ><a href="javascript:void(0)" class="pageNext" data-startVal="' + ((currentPage) * numPerPage) + '"><span class="hidden-xs">Next Page</span> <i class="fa fa-arrow-right">&nbsp;</i></a></div>');
            paginationControls.find('.row').append(nextPageLink);
        }
        paginationControls.find('.pageFirst').on('click', function() {
            fncInitMyone(1);
            return false;
        });
        paginationControls.find('.pagePrev').on('click', function() {
            fncInitMyone(currentPage - 1);
            return false;
        });
        paginationControls.find('.pageNext').on('click', function() {
            fncInitMyone(currentPage + 1);
            return false;
        });
        paginationControls.find('.pageLast').on('click', function() {
            fncInitMyone(numPages);
            return false;
        });
    } else {
        $('.page-section').addClass('e1Hidden')
    }
    if (numPages > 1) {
        $('.jsTopPagination').removeClass('e1Hidden');
        $('.jsTopPagination').prepend(paginationControls);
    }
    var lowerPaginationControls = $(paginationControls).clone(true);
    lowerPaginationControls.find('a').on('click', function() {
        if ($('#e1CompareList').length)
            $('body,html').scrollTop($('#e1CompareList').offset().top);
        if ($('.jsListingCount').length)
            $('body,html').scrollTop($('.jsListingCount').offset().top);
    });
    if (numPages > 1) {
        $('.jsBottomPagination').removeClass('e1Hidden');
        $('.jsBottomPagination').prepend(lowerPaginationControls);
    }
}

function fncRenderListings(response) {
    var params = {};
    var i;
    var j;
    var currentEvent;
    $('.jsListingRendered').remove();
    $('.jsEventRendered').remove();
    params.source = 'fncRenderListings';
    $('.jsLoader').hide();
    fncTrimUnusedFragments(listingConfig);
    response = fncAjaxResponse(response, params);
    if (response == AJAX_ERROR) {
        return AJAX_ERROR;
    }
    fncRenderPagination(response);
    var $totalPaymentRendered = $('.jsEventTemplateRendered');
    if (listingConfig.displayEventPaymentOptions == true && response.LISTINGCOUNT > 0 && $totalPaymentRendered.length == 0) {
        var $header = $('.jsEventTemplate').clone();
        $header.removeClass('jsEventTemplate');
        $header.addClass('jsEventTemplateRendered');
        $header.find('.row').hide();
        $header.find('.jsBuyerPayments').show();
        $header.show();
        $('#listingContainer').append($header);
    }
    var $renderedListings = $('.jsListingRendered');
    if (typeof response.LISTINGS == 'object') {
        for (i in response.LISTINGS) {
            var $container = $('#listingContainer');
            arrUpdateIDs.push(response.LISTINGS[i].LISTINGID);
            fncInitListing(response.LISTINGS[i], i, $container);
        }
        $.each($renderedListings, function(i, val) {
            var id = $(val).attr('id');
            var remove = true;
            id = id.split('-');
            id = id[1];
            for (i in response.LISTINGS) {
                if (response.LISTINGS[i].LISTINGID == id) {
                    remove = false;
                }
            }
            if (remove == true) {
                $(val).remove();
            }
        });
    } else if (typeof response.EVENTS == 'object') {
        if (response.TOTALINVOICEAMOUNT != undefined) {
            var bankData;
            var bankName = '';
            var flag = '';
            var amount = '';
            var symbol = '$';
            var events;
            $('.renderedBankTemplate').remove();
            for (var i in response.TOTALINVOICEAMOUNT) {
                bankData = response.TOTALINVOICEAMOUNT[i];
                bankName = bankData['BANKNAME'];
                currency = bankData['CURRENCYLOCALESHORT'];
                events = bankData['EVENTS'];
                amount = bankData['TOTAL'];
                $bankHtml = $('.jsBankTemplate').clone();
                $bankHtml.removeClass('jsBankTemplate');
                $bankHtml.removeClass('e1Hidden');
                $bankHtml.addClass('renderedBankTemplate');
                $bankHtml.find('.jsBankName').html(bankName);
                $bankHtml.find('.jsWireEventId').val(events);
                $bankHtml.find('.jsBankAmount').html(amount);
                $bankHtml.find('.jsAmountCurrency').val(amount);
                $bankHtml.find('.jsTotalDueAmount').val(amount);
                $bankHtml.find('.jsBankCurrency').html(currency);
                $bankHtml.find('.jsCurrencyShort').val(currency);
                $bankHtml.find('.jsBankAmount').autoNumeric('init', {
                    aSign: symbol,
                    aSep: ',',
                    aDec: '.',
                    mDec: '2',
                    mRound: 'D',
                    lZero: 'deny'
                });
                $bankHtml.find('.jsBankAmount').autoNumeric('set', amount);
                $('.jsEventTemplateRendered .jsTotalAmountDueContainer').append($bankHtml);
            }
        }
        for (j in response.EVENTS) {
            currentEvent = response.EVENTS[j];
            for (i in currentEvent.LISTINGS) {
                fncInitEvent(currentEvent);
                var $container = $('#event-' + currentEvent.EVENTID);
                arrUpdateIDs.push(currentEvent.LISTINGS[i].LISTINGID);
                fncInitListing(currentEvent.LISTINGS[i], i, $container);
            }
        }
    }
    if (response.LISTINGCOUNT == 0) {
        $('.jsNoListingsReturnedMessage').show();
    } else {
        $('.jsNoListingsReturnedMessage').hide();
    }
    $('.jsListingCount').html(response.LISTINGCOUNT);
}

function fncGetActiveBidding() {
    $.ajax({
        url: '/index.php?option=com_e1home&format=raw&task=active_bidding_status',
        type: "POST",
        data: null,
        success: function(userBidData) {
            fncDisplayPriceData(userBidData, false);
        },
        error: function(bidData) {
            fncDisplayPriceData(null, false);
        },
        dataType: 'json'
    });
}

function fncRenderSelfserviceListings(response) {
    var params = {};
    var i, j;
    var currentEvent;
    params.source = 'fncRenderSelfserviceListings';
    $('.jsLoader').hide();
    fncTrimUnusedFragments(listingConfig);
    response = fncAjaxResponse(response, params);
    if (response == AJAX_ERROR) {
        return AJAX_ERROR;
    }
    fncRenderPagination(response);
    var $container = $('#listingContainer');
    if (typeof response.LISTINGS == 'object') {
        for (i in response.LISTINGS) {
            fncInitListing(response.LISTINGS[i], i, $container, true);
        }
    }
    if (response.LISTINGS.length == 0) {
        $('.jsNoListingsReturnedMessage').show();
    } else {
        $('.jsNoListingsReturnedMessage').hide();
    }
    $('.jsListingCount').html(response.LISTINGCOUNT);
}

function fncValidateConfiguration() {
    if (typeof listingConfig == 'undefined') {
        throw 'The listing display configuration was not defined';
    }
}

function fncInitListingConfig() {
    listingConfig = {};
    listingConfig.displayListingId = false;
    listingConfig.displayListingId2 = false;
    listingConfig.displayListingLinks = false;
    listingConfig.displayEventLinks = false;
    listingConfig.displayRBALinks = false;
    listingConfig.displaySalvageSaleLinks = false;
    listingConfig.displayABGLinks = false;
    listingConfig.displayRBAListingId = false;
    listingConfig.displayEventId = false;
    listingConfig.displayWatchlistControls = false;
    listingConfig.displayWatchlistSummary = false;
    listingConfig.displayNotesControls = false;
    listingConfig.displayNotesSummary = false;
    listingConfig.displayBuyersHighOffer = false;
    listingConfig.dispalyLeadOffer = false;
    listingConfig.displayTimeleft = false;
    listingConfig.displayBuyersConsole = false;
    listingConfig.displayRank = false;
    listingConfig.displayLocation = false;
    listingConfig.displayQandA = false;
    listingConfig.displayAskQuestion = false;
    listingConfig.displayBuyerQuestinsAnswersStyleChange = false;
    listingConfig.displayBuyingAwaitingRemoval = false;
    listingConfig.displayEventsCol = false;
    listingConfig.displayLocationMap = false;
    listingConfig.displayChangedText = false;
    listingConfig.displayLocationRBA = false;
    listingConfig.displayLotLocationMap = false;
    listingConfig.displayCloseDate = false;
    listingConfig.displayExpectedSellerCommission = false;
    listingConfig.displayEstimatedProceeds = false;
    listingConfig.displayTimeLeftForSellerAction = false;
    listingConfig.displayPaymentDueDate = false;
    listingConfig.displayRemovalDeadline = false;
    listingConfig.displayUnitNumber = false;
    listingConfig.displaySellerPhone = false;
    listingConfig.displaySellerName = false;
    listingConfig.displayBuyerPhone = false;
    listingConfig.displayBuyerName = false;
    listingConfig.displayVINorSerial = false;
    listingConfig.displayNotSoldReason = false;
    listingConfig.displayLateFee = false;
    listingConfig.displayPrice = false;
    listingConfig.displayPriceLateFee = false;
    listingConfig.displaySellersConsole = false;
    listingConfig.displayInvoiceDate = false;
    listingConfig.displayRemovalConfirmedDate = false;
    listingConfig.displayRejectionReason = false;
    listingConfig.displayDefiniteSale = false;
    listingConfig.displaySellerLogo = false;
    listingConfig.displayRemovalEventInfo = false;
    listingConfig.displayRemovalListing = false;
    listingConfig.displayApproveRejectForPendingListings = false;
    listingConfig.displayStartDate = false;
    listingConfig.displaySelfserviceLocation = false;
    listingConfig.displayDateSubitted = false;
    listingConfig.displayRelativeImage = false;
    listingConfig.displayAbsoluteImage = false;
    listingConfig.displayLotCount = false;
    listingConfig.displayHighlights = false;
    listingConfig.displayFirstLotClosing = false;
    listingConfig.displayFirstLotClosingEvent = false;
    listingConfig.displayFirstLotClosingRBA = false;
    listingConfig.displayNoNe1LotsCol = false;
    listingConfig.displayAuctionNumber = false;
    listingConfig.displayMakeModel = false;
    listingConfig.displayYear = false;
    listingConfig.displayCondition = false;
    listingConfig.displayMileage = false;
    listingConfig.displayCompare = false;
    listingConfig.AddtoCalender = false;
    listingConfig.displayEventHeader = false;
    listingConfig.displayEventInvoice = false;
    listingConfig.displayEventPaymentOptions = false;
    listingConfig.displayEventPrice = false;
    listingConfig.displayDefinitesaleConsole = false;
    listingConfig.displayCountOffers = false;
    listingConfig.displayCountViews = false;
    listingConfig.displayAcceprRejectForSellerAction = false;
    listingConfig.displaySellerQuestinsAnswersAction = false;
    listingConfig.displayCloseDateCol3 = false;
    listingConfig.displayfileDispute = false;
    listingConfig.displayInvoicePdf = false;
    listingConfig.displayReleaseDocument = false;
    listingConfig.displayListingCol3Data = false;
    listingConfig.displaySalesAgreement = false;
    listingConfig.displayClaimDeadline = false;
    listingConfig.displayTimeCounterForSellerAction = false;
    listingConfig.displayFirstListingClosing = false;
    listingConfig.displayDeleteDraftListingConsole = false;
}

function fncConfigureListing_displayBuyersConsole($listing, params, update) {
    if (params != undefined) {
        $listing.find('.jsOfferInput').attr('data-lotnum', params.LISTINGID);
        $listing.find('.jsOfferType input').attr('name', 'e1Bid_' + params.LISTINGID);
        $listing.find('.jsOfferType input[value="Max Bid"]').prop('checked', true);
        if (params.BUYITNOWFLAG == "Y") {
            $listing.find('.jsRadioBuyItNow').show();
            $listing.find('.e1BuyItNowPrice').autoNumeric('init', {
                aSign: '$',
                aSep: ',',
                aDec: '.',
                mDec: '2',
                mRound: 'D',
                lZero: 'deny'
            });
            $listing.find('.e1BuyItNowPrice').autoNumeric('set', params.BUYITNOWPRICE);
        }
        $listing.find('.jsOfferType input').on('click', function() {
            $listing.find('.jsOfferType .btn-toggle').removeClass('active');
            $('div:has(input:radio:checked)').addClass('active');
        });
        $listing.find('.jsWhatsthis span').tooltip({
            trigger: 'hover'
        });
        var nextOffer = params.NEXTOFFERAMOUNT;
        if (typeof params.CURRENCYLOCALESHORT != 'undefined') {
            $listing.find('.locale_short').val(params.CURRENCYLOCALESHORT);
        }
        if (typeof params.CURRENCYLOCALE != 'undefined') {
            $listing.find('.locale').val(params.CURRENCYLOCALE);
        }
        fncBuyersConsoleFormatCurrency($listing, '.jsMinOffer', nextOffer);
        $listing.data('highbidhash', params.HIGHBIDUSERID);
        if (update == false) {
            $listing.find('.e1BuyerConsole').removeClass('e1Hidden');
            fncInitBuyersConsole('#listing-' + params.LISTINGID + ' .e1OfferConsole', params);
        } else {
            fncUpdateBuyersConsole('#listing-' + params.LISTINGID + ' .e1OfferConsole', params);
        }
    }
}

function fncConfigureListing_displayfileDispute($listing, params) {
    var inputDate = params.DISPUTEDEADLINE;
    if (inputDate != undefined || inputDate != '') {
        var pattern = /(.*?)\/(.*?)\/(.*?)$/;
        var result = '';
        if (inputDate != undefined) {
            result = inputDate.replace(pattern, function(match, p1, p2, p3) {
                var p3 = parseInt(p3);
                var todayDate = new Date();
                var dateDsp = new Date(p3, p1 - 1, p2);
                if (todayDate < dateDsp) {
                    if (params.SALESTATUS != 'PF') {
                        $listing.find('.jsfileDisputeLink').attr('href', "/dispute?listingid=" + params.LISTINGID);
                        $listing.find('.jsfileDispute').show();
                    }
                } else {
                    $listing.find('.jsfileDispute').hide();
                }
            });
        }
    }
}

function fncConfigureListing_displayWatchlistControls($listing, params) {
    if (params.MARKETPLACE != undefined && params.MARKETPLACE != '') {
        var watchlistDisplayListingDetails = VENUE['allMarketPlaces'];
        for (var i = 0; i < watchlistDisplayListingDetails.length; i++) {
            watchlistDisplayListingDetails[i] = watchlistDisplayListingDetails[i].replace(" ", '-');
            var docMarketplace = (params.MARKETPLACE).replace(" ", '-');
            if ((docMarketplace == watchlistDisplayListingDetails[i]) || params.WATCHED == true) {
                $listing.find('.jsWatchlistToggle').show();
            }
        }
    }
    var config = {};
    var watched = false;
    config.data = {};
    config.data.curl = '/user/:userID/watchlist/venueID/:venueID';
    var postfields = {};
    postfields.listingID = params.LISTINGID;
    config.data.postfields = JSON.stringify(postfields);
    config.url = '/home?task=ajax';
    config.success = fncRenderListings;
    $listing.find('.jsWatchMsg').hide();
    var watchlist = UserListings.fncGetWatchlist();
    for (var i in watchlist) {
        if (watchlist[i] == params.LISTINGID) {
            watched = true;
        }
    }
    if (watched == true) {
        fncDisplayRemoveFromWatchlist($listing, params);
    } else {
        fncDisplayAddToWatchlist($listing, params);
    }
    $listing.find('.jsWatchMsg').hide();
}

function fncDisplayAddToWatchlist($listing, params) {
    var config = {};
    config.data = {};
    config.data.curl = '/user/:userID/watchlist/venueID/:venueID';
    config.data.method = 'POST';
    config.data.version = '1';
    var postfields = {};
    postfields.listingID = params.LISTINGID;
    config.data.postfields = JSON.stringify(postfields);
    config.url = '/home?task=ajax';
    $listing.find('.jsWatchlistToggle div').removeClass('icon-custom-watching');
    $listing.find('.jsWatchlistToggle div').addClass('icon_watchlist');
    $listing.find('.jsWatchlistToggle i').tooltip({
        trigger: 'hover'
    });
    $listing.find('.jsWatchlistToggle').unbind('click');
    $listing.find('.jsWatchMsg').show();
    $listing.find('.jsWatchlistToggle').on('click', function() {
        send2Monetate.addCartRow($listing, params);
        $('#enableCache').val(1);
        fncAjax(config);
        UserListings.fncAddListing(params.LISTINGID, 'WATCHLIST', 1);
        fncDisplayRemoveFromWatchlist($listing, params);
        $listing.find('.jswatchtitle').attr('data-original-title', '<div class="watchtext">Click to remove from <br/> Watchlist</div>');
        $listing.find('.jsWatchlistToggle i').tooltip({
            trigger: 'hover'
        });
        return false;
    })
}

function fncDisplayRemoveFromWatchlist($listing, params, callType) {
    var config = {};
    config.data = {};
    config.data.curl = '/user/:userID/watchlist/venueID/:venueID';
    config.data.method = 'DELETE';
    config.data.version = '1';
    var postfields = {};
    postfields.listingID = params.LISTINGID;
    config.data.postfields = JSON.stringify(postfields);
    config.url = '/home?task=ajax';
    $listing.find('.jsWatchMsg').hide();
    $listing.find('.jsWatchlistToggle div').removeClass('icon_watchlist');
    $listing.find('.jsWatchlistToggle div').addClass('icon-custom-watching');
    $listing.find('.jswatchtitle').attr('title', '');
    $listing.find('.jswatchtitle').attr('data-original-title', '<div class="watchtext">Click to remove from <br/> Watchlist</div>');
    $listing.find('.jsWatchlistToggle i').tooltip({
        trigger: 'hover'
    });
    $listing.find('.jsWatchlistToggle').unbind('click');
    $listing.find('.jsWatchlistToggle').on('click', function() {
        $('#enableCache').val(1);
        fncAjax(config);
        UserListings.fncRemoveListing("" + params.LISTINGID, 'WATCHLIST');
        fncDisplayAddToWatchlist($listing, params);
        $listing.find('.jsWatchMsg').show();
        $listing.find('.jswatchtitle').attr('data-original-title', '<div class="watchtext">Click to add to <br/> Watchlist</div>');
        $listing.find('.jsWatchlistToggle i').tooltip({
            trigger: 'hover'
        });
        $listing.find('.jsWatchers').html(params.COUNTWATCHERS);
        return false;
    })
}

function fncConfigureListing_displayWatchlistSummary() {}

function fncConfigureListing_displayDefiniteSale($listing, params) {
    if (params != undefined) {
        $('.jsDefiniteSaleBuyer').removeClass('e1Hidden');
        if (params.DEFINITESALE == 'Y' || params.DEFINITESALE == true) {
            if (params.COUNTOFFERS == '--' || params.COUNTOFFERS == 0) {
                $listing.find('.jsDefiniteSaleText').html('FIRST BID MEETS RESERVE');
            } else {
                $listing.find('.jsDefiniteSaleText').html('RESERVE MET');
            }
        } else {
            $listing.find('.jsDefiniteSaleText').html('RESERVE NOT MET');
        }
    }
    fncDisplayDefiniteSale($listing, params);
}

function fncConfigureListing_displaySellerLogo($listing, params) {
    if (params.SELLERCOMPANYLOGO != undefined && params.SELLERCOMPANYLOGO != '' && params.SELLERCOMPANYLOGO != null) {
        $listing.find('.jsSeller').show();
        $listing.find('.jsSellerLogo').attr('src', CDN_IMAGES_BASE + '/' + params.SELLERCOMPANYLOGO);
    }
}

function fncConfigureListing_displayDefinitesaleConsole($listing, params) {
    var page = $('#jsMyonePageName').val();
    $listing.find('.jsSellerDefinitesaleConsole').show();
    $listing.find('.jsSellerDefinitesaleConsole').addClass('e1BuyerConsole');
    if (params != undefined) {
        if (params.DEFINITESALE == true) {
            $listing.find('.jsSellerDefinitesaleActive').show();
        } else {
            $listing.find('.jsSellerDefinitesale').show();
            if (page == 'ListingDetails') {
                fncInitListingDefiniteSaleProcess('#listing-' + params.LISTINGID + ' .sellAsDefinitesale', params.LISTINGID, $listing);
            }
            fncInitMyoneDefiniteSaleProcess('#listing-' + params.LISTINGID + ' .sellAsDefinitesale', params.LISTINGID, $listing);
        }
    }
}

function fncInitNotesConsole(selector) {
    var characters = 250;
    $(this).closest('.listing-notes').find('.noteMode').val('0');
    $(selector).on('click', function() {
        $(this).closest('.listing-notes').find('.jsAddNote').hide();
        $(this).closest('.listing-notes').find('.jsmyNotes').hide();
        $(this).closest('.listing-notes').find('.jsNoteFields').show();
        $(this).closest('.listing-notes').find('.jsDisplayNotes').hide();
        $(this).closest('.listing-notes').find('.noteMode').val('1');
    });
    $(selector).on('keyup', '.jsNoteContent', function() {
        var remaining = characters - $(this).closest('.listing-notes').find(".jsNoteContent").val().length;
        $(this).closest('.listing-notes').find("#counter").html('<span class="rgbold">' + remaining + '</span> Remaining');
        $(this).closest('.listing-notes').find('.note-count').show();
    });
}

function fncDisplayAddNotes($listing, params) {
    $listing.find('.jsSaveNote span').addClass('fa-floppy-o');
    $listing.find('.jsSaveNote span').removeClass('fa-pencil-square-o');
    $listing.find('.jsSaveNote').unbind('click');
    $listing.find('.jsNoteContent').show();
    $listing.find('.jsListingNote').hide();
    $listing.find('.jsNoteContent').focus();
    var params = {};
    var postfields = {};
    params.data = {};
    $listing.find('.jsSaveNote').on('click', function() {
        var noteContent = $listing.find('.jsNoteContent').val();
        if (noteContent == '') {
            $listing.find('.jsNoteContent').show();
            $(this).closest('.listing-notes').find('.noteMode').val('1');
            $listing.find('.note-count').html('<span class="rgbold">250</span> Remaining').show();
            $listing.find('.jsListingNote').hide();
            $listing.find('.jsSaveNote span').removeClass('fa-pencil-square-o');
            $listing.find('.jsSaveNote span').addClass('fa-floppy-o');
            $listing.find('.jsSaveNote').css('visibility', 'visible');
            $listing.find('.jsNoteLoader').hide();
            $listing.find('.jsNoteContent').blur();
            $("#notesErrorMessage").modal('show');
            return false;
        }
        $listing.find('.noteMode').val('0');
        noteContent = noteContent.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        $listing.find('.jsListingNote').html(noteContent);
        $listing.find('.jsListingNote').show();
        $listing.find('.note-count').hide();
        var noteId = $listing.find('.jsNoteId').val();
        var currentFragment = $listing.closest('.jsListingRendered').attr('id');
        var listingIdAr = currentFragment.split('-');
        var listingId = listingIdAr[1];
        var params = {};
        var postfields = {};
        params.data = {};
        pageConfig = {};
        pageConfig.defaultAjaxController = '/home?task=ajax';
        params.data.curl = '/myone/:userID/offerNote/venueID/:venueID';
        params.url = pageConfig.defaultAjaxController;
        params.success = fncAddNoteResponse;
        params.data.method = 'POST';
        postfields.LISTINGID = listingId;
        postfields.NOTE = noteContent;
        if (noteId != '' && noteId != undefined) {
            postfields.ID = noteId;
        }
        params.data.postfields = JSON.stringify(postfields);
        fncAjax(params);
        UserListings.fncAddListing(postfields.LISTINGID, 'NOTESLIST', 1);
        fncDisplayEditNotes($listing, params);
        return false;
    })
}

function fncAddNoteResponse(response) {
    var params = {};
    params.source = 'fncAddNoteResponse';
    response = fncAjaxResponse(response, params);
    if (response == AJAX_ERROR) {
        return AJAX_ERROR;
    }
    $listing = $('#listing-' + response.OFFERNOTE.LISTINGID);
    $listing.find('.jsNoteId').val(response.OFFERNOTE.ID);
}

function fncDisplayEditNotes($listing, params) {
    var params = {};
    var postfields = {};
    params.data = {};
    $listing.find('.jsSaveNote span').addClass('fa-pencil-square-o');
    $listing.find('.jsSaveNote span').removeClass('fa-floppy-o');
    $listing.find('.jsNoteContent').hide();
    $listing.find('.jsListingNote').show();
    $listing.find('.note-count').hide();
    if ($listing.find(".jsNoteContent").val() != undefined) {
        var remaining = 250 - $listing.find(".jsNoteContent").val().length;
        $listing.find("#counter").html('<span class="rgbold">' + remaining + '</span> Remaining');
    }
    $listing.find('.jsSaveNote').unbind('click');
    $listing.find('.jsSaveNote').on('click', function() {
        if ($listing.find('.jsSaveNote .glyphicon').hasClass('fa-pencil-square-o')) {
            $listing.find('.jsNoteContent').show();
            $(this).closest('.listing-notes').find('.noteMode').val('1');
            $listing.find('.jsListingNote').hide();
            $listing.find('.note-count').show();
            $listing.find('.jsNoteContent').focus();
            $listing.find('.jsSaveNote span').removeClass('fa-pencil-square-o');
            $listing.find('.jsSaveNote span').addClass('fa-floppy-o');
            return false;
        } else {
            $listing.find('.jsNoteContent').hide();
            $listing.find('.jsListingNote').show();
            $listing.find('.jsSaveNote span').addClass('fa-pencil-square-o');
            $listing.find('.jsSaveNote span').removeClass('fa-floppy-o');
        }
        $listing.find('.jsSaveNote').css('visibility', 'hidden');
        $listing.find('.jsNoteLoader').show();
        var noteContent = $listing.find('.jsNoteContent').val();
        if (noteContent == '') {
            $listing.find('.jsNoteContent').show();
            $(this).closest('.listing-notes').find('.noteMode').val('1');
            $listing.find('.jsListingNote').hide();
            $listing.find('.note-count').show();
            $listing.find('.jsSaveNote span').removeClass('fa-pencil-square-o');
            $listing.find('.jsSaveNote span').addClass('fa-floppy-o');
            $listing.find('.jsSaveNote').css('visibility', 'visible');
            $listing.find('.jsNoteLoader').hide();
            $listing.find('.jsNoteContent').blur();
            $("#notesErrorMessage").modal('show');
            return false;
        }
        $listing.find('.noteMode').val('0');
        noteContent = noteContent.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        $listing.find('.jsListingNote').html(noteContent);
        var noteId = $listing.find('.jsNoteId').val();
        var currentFragment = $listing.closest('.jsListingRendered').attr('id');
        var listingIdAr = currentFragment.split('-');
        var listingId = listingIdAr[1];
        var params = {};
        var postfields = {};
        params.data = {};
        pageConfig = {};
        pageConfig.defaultAjaxController = '/home?task=ajax';
        params.data.curl = '/myone/:userID/offerNote/venueID/:venueID';
        params.url = pageConfig.defaultAjaxController;
        params.data.method = 'POST';
        postfields.LISTINGID = listingId;
        postfields.NOTE = noteContent;
        if (noteId != '' && noteId != undefined) {
            postfields.offerNoteID = noteId;
        }
        params.data.postfields = JSON.stringify(postfields);
        params.success = fncMoneSaveNoteRender;
        fncAjax(params);
        return false;
    })
}

function fncMoneSaveNoteRender(response) {
    $('.jsSaveNote').css('visibility', 'visible');
    $('.jsNoteLoader').hide();
}

function fncDisplayDeleteNotes($listing, params) {
    var params = {};
    var postfields = {};
    params.data = {};
    pageConfig.defaultAjaxController = '/home?task=ajax';
    $listing.find('.jsDeleteNote').unbind('click');
    $listing.find('.jsDeleteNote').on('click', function() {
        $listing.find('.jsListingNote').html('');
        var noteId = $listing.find('.jsNoteId').val();
        var currentFragment = $listing.closest('.jsListingRendered').attr('id');
        var listingIdAr = currentFragment.split('-');
        var listingId = listingIdAr[1];
        params.data.curl = '/myone/:userID/offerNote/venueID/:venueID';
        params.url = pageConfig.defaultAjaxController;
        params.data.method = 'DELETE';
        postfields.LISTINGID = listingId;
        if (noteId != '' && noteId != undefined) {
            postfields.offerNoteID = noteId;
        }
        params.data.postfields = JSON.stringify(postfields);
        fncAjax(params);
        if (typeof userNoteListings != 'undefined') {
            UserListings.fncRemoveListing(postfields.LISTINGID, 'NOTESLIST');
        }
        $listing.find('.jsAddNote').show();
        $listing.find('.jsmyNotes').show();
        $listing.find('.jsNoteContent').val('');
        $listing.find('.jsNoteId').val('');
        $listing.find('.jsNoteFields').hide();
        $listing.find('.note-count').hide();
        fncDisplayAddNotes($listing, params);
        fncInitNotesConsole('#listing-' + params.LISTINGID + ' .listing-notes');
        return false;
    })
}

function fncConfigureListing_displayDateSubitted($listing, params) {
    $listing.find('.jsDateSubmitted').html(params.DATESUBMITTED);
    $listing.find('.jsDateSubmittedCol3').show();
    $listing.find('.jsDateSubmitted').show();
}

function fncConfigureListing_displayNotesControls($listing, params) {
    $listing.find('.listing-notes').show();
    if ($listing.find('.noteMode').val() == '0') {
        var countNotes = 0;
        if (params['OFFERNOTES'] != undefined) {
            countNotes = params['OFFERNOTES'].length;
        }
        if (countNotes > 0) {
            var lastNote = countNotes - 1;
            $listing.find('.jsNoteContent').val(params['OFFERNOTES'][lastNote]['NOTE']);
            $listing.find('.jsListingNote').html(params['OFFERNOTES'][lastNote]['NOTE']);
            $listing.find('.jsNoteId').val(params['OFFERNOTES'][lastNote]['ID']);
            $listing.find('.jsAddNote').hide();
            $listing.find('.jsmyNotes').hide();
            $listing.find('.jsNoteFields').show();
            fncDisplayEditNotes($listing, params);
        } else {
            $listing.find('.jsAddNote').show();
            $listing.find('.jsmyNotes').show();
            $listing.find('.jsNoteContent').val('');
            $listing.find('.jsNoteId').val('');
            $listing.find('.jsNoteFields').hide();
            fncDisplayAddNotes($listing, params);
        }
        fncInitNotesConsole('#listing-' + params.LISTINGID + ' .listing-notes');
        fncDisplayDeleteNotes($listing, params);
    }
}

function fncRenderNotes(response) {
    var params = {};
    params.source = 'fncRenderNotes';
    $('.jsLoader').hide();
    $('.jsNoteFields').show();
    response = fncAjaxResponse(response, params);
    if (response == AJAX_ERROR) {}
    if (response.STATUS == 'OK') {
        $('.listing-notes').find('.jsDisplayNotes').show();
        $('.listing-notes').find('.jsNoteContent').hide();
    } else {
        $('.listing-notes').find('.jsNoteContent').hide();
        $('.listing-notes').find('.jsDisplayNotes').show();
    }
}

function fncConfigureListing_displayNotesSummary($listing, params) {
    if (params.NOTES != undefined)
        $listing.find('.jsNotesText').html(params.OFFERNOTES);
    else
        $listing.find('.jsNotesText').html('Here are the notes associated with this listing. Max is 250 characters');
    $listing.find('.jsNoteSummary').show();
}

function fncConfigureListing_displayBuyersHighOffer($listing, params) {
    var bidMaxOffer = fncCurrencyText('$', params.CURRENCYLOCALESHORT, params.CURRENTBIDDERMAXOFFER);
    $listing.find('.jsMyMaxOffer').html(bidMaxOffer);
    $listing.find('.jsMyMaxOfferLabel').show();
    $listing.find('.jsMyMaxOffer').show();
}

function fncConfigureListing_dispalyLeadOffer($listing, params, update) {
    if (update == false) {
        $listing.find('.jsCurrentOfferLabel').show();
        $listing.find('.jsCurrentOffer').show();
    }
    if (params != undefined) {
        var leardOfferAmount = fncCurrencyText('$', params.CURRENCYLOCALESHORT, params.LEADOFFERPRICE);
        $listing.find('.jsCurrentOffer').html(leardOfferAmount);
        $listing.find('.jsCurrentOffer').autoNumeric('init', {
            aSign: '$',
            aSep: ',',
            aDec: '.',
            mDec: '2',
            mRound: 'D',
            lZero: 'deny'
        });
        $listing.find('.jsCurrentOffer').autoNumeric('set', leardOfferAmount);
    }
}

function fncConfigureListing_displayFirstLotClosing($listing, params, update) {
    if (update != true) {
        $listing.find('.jsTimeLeftLabel').show();
        $listing.find('.jsTimeLeft').show();
        $('.jsExtendedBidding i').tooltip({
            trigger: 'hover'
        });
    }
    if (params != undefined) {
        if (params.EVENT == '1' || params.EVENT == '2') {
            $listing.find('.jsClosingTimeLabel').html('First Lot Closing');
        }
        if (params.AUCTIONENDDATE != undefined) {
            $listing.find('.jsTimeLeft').html(params.AUCTIONENDDATE);
        }
    }
}

function fncConfigureListing_displayFirstLotClosingEvent($listing, params, update) {
    if (update != true) {
        $listing.find('.jsTimeLeftLabel').show();
        $listing.find('.jsTimeLeft').show();
        $('.jsExtendedBidding i').tooltip({
            trigger: 'hover'
        });
    }
    if (params.EVENT == '1' || params.EVENT == '2') {
        $listing.find('.jsClosingTimeLabel').html('First Lot Closing');
    }
    if (params.AUCTIONENDDATE != undefined) {
        var actdate = params.AUCTIONENDDATE;
        var last2 = actdate.slice(-11);
        var replaced = last2.split(' ');
        if (params.AUCTIONENDDATE.indexOf("CT") > 0)
            var d = new Date(params.AUCTIONENDDATE.replace(" CT", ""));
        else
            var d = new Date(params.AUCTIONENDDATE);
        var months = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];
        $listing.find('.jsTimeLeft').html(months[d.getMonth()] + '/' + d.getDate() + '/' + d.getFullYear() + ' ' + replaced[0] + '' + replaced[1] + ' ' + replaced[2]);
    }
}

function fncConfigureListing_displayFirstLotClosingRBA($listing, params, update) {
    if (update != true) {
        $listing.find('.jsTimeLeftLabel').show();
        $listing.find('.jsTimeLeft').show();
        $('.jsExtendedBidding i').tooltip({
            trigger: 'hover'
        });
    }
    if (params.EVENT == '1' || params.EVENT == '2') {
        $listing.find('.jsClosingTimeLabel').html('First Lot Closing');
    }
    if (params.AUCTIONENDDATE != undefined) {
        if (params.AUCTIONENDDATE.indexOf("CT") > 0)
            var d = new Date(params.AUCTIONENDDATE.replace(" CT", ""));
        else
            var d = new Date(params.AUCTIONENDDATE);
        var months = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];
        $listing.find('.jsTimeLeft').html(months[d.getMonth()] + '/' + d.getDate() + '/' + d.getFullYear());
    }
}

function fncConfigureListing_displayTimeleft($listing, params, update) {
    if (update != true) {
        $listing.find('.jsTimeLeftLabel').show();
        $listing.find('.jsTimeLeft').show();
        $('.jsExtendedBidding i').tooltip({
            trigger: 'hover'
        });
        var changeDate;
        currentDate = fncGetCurrentDate();
        changeDate = new Date(params.AUCTIONSTARTDATETIMEUTC);
        if (params.ALLOWEDCOMMERCEMETHODS != undefined && params.ALLOWEDCOMMERCEMETHODS.NEXTCOMMERCEMETHODS == 'BIN') {
            $listing.find('.jsClosingTimeLabel').html('Time Until Auction');
        } else {
            $listing.find('.jsClosingTimeLabel').html('Time Remaining');
        }
    }
    if (params != undefined) {
        try {
            fncInitCountdown($listing, params);
        } catch (e) {
            fncLogAjaxError(params, e, 'LISTINGS PAGE');
        }
    }
}

function fncInitCountdown($listing, params) {
    if (params.ALLOWEDCOMMERCEMETHODS != undefined) {
        oldDate = $listing.find('.jsTimeLeft').data('countdown');
        $listing.find('.jsTimeLeft').attr('data-AssetID', params.LISTINGID);
        if (typeof params.ALLOWEDCOMMERCEMETHODS.CHANGEMETHODSAT != 'undefined' && params.ALLOWEDCOMMERCEMETHODS.CHANGEMETHODSAT != "") {
            endDate = new Date(params.ALLOWEDCOMMERCEMETHODS.CHANGEMETHODSAT);
        } else {
            endDate = new Date(params.AUCTIONENDDATETIMEUTC);
        }
        var $this = $listing.find('.jsTimeLeft');
        var endDateString = endDate.getFullYear() + '/' + (endDate.getMonth() + 1) + '/' + endDate.getDate() + ' ' + endDate.getHours() + ":" + endDate.getMinutes() + ':' + endDate.getSeconds();
        if (oldDate != undefined && endDateString != oldDate && params.TIMELEFT != "<span class='closed'>Closed</span>") {
            $listing.find('.jsExtendedBidding').removeClass('e1Hidden');
            $listing.find('.jsExtendedBidding').addClass('text-strong');
        }
        currentDate = fncGetCurrentDate();
        if (currentDate > endDate) {
            endDate = new Date(params.AUCTIONENDDATETIMEUTC);
        }
        endDateString = endDate.getFullYear() + '/' + (endDate.getMonth() + 1) + '/' + endDate.getDate() + ' ' + endDate.getHours() + ":" + endDate.getMinutes() + ':' + endDate.getSeconds();
        $listing.find('.jsTimeLeft').attr('data-countdown', endDateString);
        $listing.find('.jsTimeLeft').removeClass('jsClosing');
        fncCountdownSettings($this, params, endDateString);
    }
}

function fncCountdownSettings($this, params, endDateString) {
    $this.countdown(endDateString, function(event) {
        var $listing = $('#listing-' + params.LISTINGID);
        var closeDate = new Date($listing.find('.auctionEndDateUtc').val());
        closeDateString = closeDate.getFullYear() + '/' + (closeDate.getMonth() + 1) + '/' + closeDate.getDate() + ' ' + closeDate.getHours() + ":" + closeDate.getMinutes() + ':' + closeDate.getSeconds();
        currentDate = fncGetCurrentDate();
        currentDateString = currentDate.getFullYear() + '/' + (currentDate.getMonth() + 1) + '/' + currentDate.getDate() + ' ' + currentDate.getHours() + ":" + currentDate.getMinutes() + ':' + currentDate.getSeconds();
        if (params.SALESTATUS == 'S') {
            $this.text(event.strftime('Lot in Preview'));
        } else if (currentDate > closeDate) {
            $this.addClass('error');
            $listing.find('.jsTimeLeft').html('Closed');
            fncConfigureClosed($listing, params);
        } else if (event.offset.days == 0 && event.offset.hours == 0 && event.offset.minutes == 0 && event.offset.seconds == 0) {
            var $listing = $('#listing-' + params.LISTINGID);
            params.FORCECHANGE = true;
            endDateString = $listing.find('.jsTimeLeft').attr('data-countdown');
            if (endDateString == closeDateString) {
                $this.addClass('error');
                $listing.find('.jsTimeLeft').html('Closed');
                fncConfigureClosed($listing, params);
            } else {
                $this.removeClass('error');
                params.FORCECHANGE = true;
                fncDisplayPriceData(params, true);
            }
        } else if (event.offset.weeks == 0 && event.offset.days == 0 && event.offset.hours == 0 && event.offset.minutes < 30) {
            var minute = event.strftime('%M');
            minute = parseInt(minute);
            minute = minute + 1;
            $this.text(event.strftime('Less than ' + minute + 'm'));
            $this.addClass('error');
        } else {
            $this.removeClass('error');
            $this.text(event.strftime('%Dd %Hh %Mm'));
        }
    }).on('finish.countdown', function(event) {
        if ($this.hasClass('jsClosing')) {
            return;
        }
        $this.addClass('jsClosing');
        var $listing = $('#listing-' + params.LISTINGID);
        endDateString = $listing.find('.jsTimeLeft').attr('data-countdown');
        var salesStatus = $listing.find('.salesStatus').val();
        params.SALESTATUS = salesStatus;
        if (endDateString == closeDateString) {
            $this.addClass('error');
            $listing.find('.jsTimeLeft').html('Closed');
            fncConfigureClosed($listing, params);
        } else {
            $this.removeClass('error');
            params.FORCECHANGE = true;
            fncDisplayPriceData(params, true);
            $listing.find('.jsTimeLeft').attr('data-countdown', closeDateString);
            $this.countdown(closeDateString);
        }
    });
}

function fncConfigureListing_displayRank($listing, params, update) {
    if (params == undefined || params.TIMELEFT == undefined || params.YOURRANK == undefined) {
        return;
    }
    var isClosed = params.TIMELEFT.indexOf("Closed");
    if (isClosed != -1 || (params.SALESTATUS != undefined && params.SALESTATUS != 'A')) {
        return
    }
    if (update != true) {
        $listing.find('.jsRank').show();
        $listing.find('.jsRank').addClass('heavy-text');
    }
    if (params.YOURRANK == undefined) {
        if (params.CURRENTBIDDERRANK != undefined) {
            params.YOURRANK = params.CURRENTBIDDERRANK;
        }
    }
    if (params.YOURRANK == 1) {
        $listing.find('.jsRank').html('You\'re Winning');
        $listing.find('.e1BuyerConsole').addClass('green-offer-console');
    } else if (params.YOURRANK == 0 || params.YOURRANK == undefined) {
        $listing.find('.jsRank').html('Place First Bid');
        $listing.find('.e1BuyerConsole').addClass('orange-offer-console');
        $listing.find('.jsRank').removeClass('green-text');
        $listing.find('.jsRank').addClass('orange-text');
    } else if (params.YOURRANK > 10) {
        $listing.find('.jsRank').html('Make Higher Offer');
        $listing.find('.e1BuyerConsole').addClass('orange-offer-console');
        $listing.find('.jsRank').removeClass('green-text');
        $listing.find('.jsRank').addClass('orange-text');
    } else if (params.YOURRANK <= 10) {
        var appendStr = 'th'
        if (params.YOURRANK == 2) {
            appendStr = 'nd';
        } else if (params.YOURRANK == 3) {
            appendStr = 'rd';
        }
        $listing.find('.jsRank').html('You\'re in ' + params.YOURRANK + appendStr + ' Place');
        $listing.find('.jsRank').removeClass('green-text');
        $listing.find('.jsRank').addClass('red-text');
        $listing.find('.e1BuyerConsole').addClass('red-offer-console');
    }
}

function fncConfigureListing_displayLocation($listing, params, update) {
    $listing.find('.jsLocation').show();
    $listing.find('.jsLocationLabel').show();
    localeArray = params.LOCATION.split(", ");
    if (localeArray.length == 3) {
        if (localeArray[2] == 'MEX' || localeArray[2] == 'SWE')
            flagCountry = (localeArray[2].charAt(0) + localeArray[2].charAt(2)).toLowerCase();
        else
            flagCountry = localeArray[2].substring(0, 2).toLowerCase();
    } else {
        localeArray = params.LOCATION.split(" ");
        var localelength = localeArray.length;
        if (localeArray[localelength - 1] == 'MEX' || localeArray[localelength - 1] == 'SWE')
            flagCountry = (localeArray[localelength - 1].charAt(0) + localeArray[localelength - 1].charAt(2)).toLowerCase();
        else
            flagCountry = localeArray[localelength - 1].substring(0, 2).toLowerCase();
    }
    $listing.find('.jsLocation').html('<i class="img-thumbnail locale_flag flag flag-icon-background flag-icon-' + flagCountry + '" title="' + flagCountry + '"></i> ' + params.LOCATION);
    if (update != true) {
        $listing.find('.jsLocationLink').click(function() {
            fnclocateItem($listing, params);
        });
    }
}

function fncConfigureListing_displaySelfserviceLocation($listing, params) {
    $listing.find('.jsLocation').show();
    $listing.find('.jsLocationLabel').show();
    $listing.find('.jsLocation').html(params.LOCATION);
    $listing.find('.jsLocation').addClass('jsCursorDefault');
}

function fncGetLocationMarkerIconUrl(baseURL) {
    var pos = baseURL.indexOf("www");
    if (pos != -1) {
        baseURL = baseURL.replace("https", "http");
    } else {
        baseURL = '';
    }
    return baseURL;
}

function fnclocateItem($listing, params) {
    if ($listing.find('#e1ItemMapTemplate').css('display') == 'none') {
        var itemLocation = "";
        gLat = params.STORAGELOCATIONLAT;
        gLng = params.STORAGELOCATIONLNG;
        $base = fncGetLocationMarkerIconUrl(BASE_URL);
        if (params.STORAGELOCATIONLAT != undefined || params.STORAGELOCATIONLNG != undefined) {
            itemLocation = gLat + "," + gLng;
        } else {
            itemLocation = params.LOCATION;
        }
        $listing.find('.e1mapaddressValue').html(params.LOCATION);
        var sourcedtp = "//maps.googleapis.com/maps/api/staticmap?center=" + itemLocation + "&zoom=10&size=1024x300&maptype=roadmap&markers=icon:" + $base + "%2Fimages%2FE1-Listing.png%7C" + itemLocation;
        var sourcemobile = "//maps.googleapis.com/maps/api/staticmap?center=" + itemLocation + "&zoom=8&size=200x200&scale=2&maptype=roadmap&markers=icon:" + $base + "%2Fimages%2FE1-Listing.png%7C" + itemLocation;
        $listing.find('#e1ItemMapTemplate #maparea img').css('width', '100%');
        $listing.find('#e1ItemMapTemplate #maparea img').css('height', 'auto');
        $listing.find('.jsdtpmap #maparea img').attr('src', sourcedtp);
        $listing.find('.jsmobilemap #maparea img').attr('src', sourcemobile);
        $listing.find('#e1ItemMapTemplate').show();
        $listing.find('#e1ItemMapTemplate .e1CloseMap').click(function() {
            $listing.find('#e1ItemMapTemplate #maparea img').attr('src', '');
            $listing.find('#e1ItemMapTemplate').hide();
        });
    } else {
        $listing.find('#e1ItemMapTemplate #maparea img').attr('src', '');
        $listing.find('#e1ItemMapTemplate').hide();
    }
}

function fncConfigureListing_displayAskQuestion($listing, params, update) {
    if (update == true) {
        return;
    }
    $listing.find('.jsAskitnow').on('click', function() {
        $listing.find('.jsAskLotQuestion').removeClass('e1Hidden');
        $listing.find('.jsAskitnow').hide();
        $listing.find('.jsAskQuestionText').hide();
    });
    $listing.find('.jsAskQuescancel').on('click', function() {
        $listing.find('.jsAskLotQuestion').addClass('e1Hidden');
        $listing.find('#AskquesText').val('');
        $listing.find('.jsAskitnow').show();
        $listing.find('.jsAskQuestionText').show();
    });
    $("#AddQuesform").find("textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {},
        submitSuccess: function($form, event) {
            event.preventDefault();
            var listingID = params.LISTINGID;
            var lotID = params.LOTNUMBER;
            var AskquesText = $('#AskquesText').val();
            AskquesText = escape(trim_str(AskquesText));
            jQuery.ajax({
                url: "/index.php?option=com_e1listing&format=raw&task=askQuestion",
                type: "POST",
                data: 'listingID=' + listingID + '&lotID=' + lotID + '&listingQuestion=' + AskquesText + '&listingTitle=' + escape(params.TITLE),
                dataType: "json",
                success: function(responseText) {
                    console.log(responseText);
                    if ((responseText != null) && responseText.e1_status == 'success') {
                        var qStatus = responseText.e1_content['STATUS'];
                        if (qStatus == 'OK') {
                            $listing.find('.jsAskLotQuestion').addClass('e1Hidden');
                            $listing.find('.jsAskLotQuestionSucess').removeClass('e1Hidden');
                            $listing.find('.jsAskLotQuestionclose').on('click', function() {
                                if ($listing.find('#jsSendCopy').prop('checked')) {
                                    jQuery.ajax({
                                        url: "/index.php?option=com_e1listing&format=raw'",
                                        type: "POST",
                                        data: 'task=sendQuestionCopy&question=' + AskquesText + '&title=' + escape(params.TITLE) + '&listingId=' + listingID + '&lotID=' + lotID,
                                    });
                                }
                                $listing.find('#AskquesText').val('');
                                $listing.find('.jsAskLotQuestionSucess').addClass('e1Hidden');
                                $listing.find('.jsAskitnow').show();
                                $listing.find('.jsAskQuestionText').show();
                            });
                        }
                    }
                }
            });
        },
        filter: function() {
            return $(this).is(":visible");
        }
    });
}

function fncConfigureListing_displayCloseDate($listing, params) {
    $listing.find('.jsClosedDateLabel').show();
    $listing.find('.jsClosedDate').show();
    if (params != undefined) {
        if (params.MARKETPLACE == "SS" || params.MARKETPLACE == 'ABG')
            $listing.find('.jsClosedDateLabel .e1-label').html("Closing Date");
        else if (params.MARKETPLACE == "RBA")
            $listing.find('.jsClosedDateLabel .e1-label').html("Sale Date");
        if (params.CLOSEDDATE != undefined && params.CLOSEDDATE != '') {
            $listing.find('.jsClosedDate').html(getFormattedDate(params.CLOSEDDATE));
        } else if (params.CLOSEDATE != undefined && params.CLOSEDATE != '') {
            $listing.find('.jsClosedDate').html(getFormattedDate(params.CLOSEDATE));
        } else {
            if (params.AUCTIONENDDATE != undefined) {
                if (params.AUCTIONENDDATE.indexOf("CT") > 0)
                    var d = new Date(params.AUCTIONENDDATE.replace(" CT", ""));
                else
                    var d = new Date(params.AUCTIONENDDATE);
                var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                $listing.find('.jsClosedDate').html(months[d.getMonth()] + ' ' + d.getDate() + ', ' + d.getFullYear());
            }
        }
    }
}

function fncConfigureListing_displayCloseDateCol3($listing, params) {
    $listing.find('.jsClosedDateLabelCol3').show();
    $listing.find('.jsClosedDate').show();
    if (params != undefined) {
        $listing.find('.jsClosedDate').html(getFormattedDate(params.CLOSEDATE));
    }
}

function fncConfigureListing_displayExpectedSellerCommission($listing, params) {
    $listing.find('.jsExpCommissionLabel').show();
    $listing.find('.jsExpCommission').show();
    if (params != undefined) {
        var leardExpCommission = fncCurrencyText('$', params.CURRENCYLOCALESHORT, params.COMMISSION);
        $listing.find('.jsExpCommission').html(leardExpCommission);
        $listing.find('.jsExpCommission').autoNumeric('init', {
            aSign: '$',
            aSep: ',',
            aDec: '.',
            mDec: '2',
            mRound: 'D',
            lZero: 'deny'
        });
        $listing.find('.jsExpCommission').autoNumeric('set', leardExpCommission);
    }
}

function fncConfigureListing_displayEstimatedProceeds($listing, params) {
    $listing.find('.jsEstimatedProceedsLabel').show();
    $listing.find('.jsEstimatedProceeds').show();
    if (params != undefined) {
        var leardEstimatedProceeds = fncCurrencyText('$', params.CURRENCYLOCALESHORT, params.PROCEEDS);
        $listing.find('.jsEstimatedProceeds').html(leardEstimatedProceeds);
        $listing.find('.jsEstimatedProceeds').autoNumeric('init', {
            aSign: '$',
            aSep: ',',
            aDec: '.',
            mDec: '2',
            mRound: 'D',
            lZero: 'deny'
        });
        $listing.find('.jsEstimatedProceeds').autoNumeric('set', leardEstimatedProceeds);
    }
}

function fncConfigureListing_displayTimeLeftForSellerAction($listing, params) {
    $listing.find('.jsTimeRemainingForActionLabel').show();
    $listing.find('.jsTimeRemainingForAction').show();
    if (params != undefined) {
        $listing.find('.jsTimeRemainingForAction').html(getFormattedDate(params.SELLERBIDSELECTACTIONREQUIREDDATE));
    }
}

function fncConfigureListing_displayTimeCounterForSellerAction($listing, params) {
    $listing.find('.jsTimeRemainingForActionLabel').show();
    $listing.find('.jsTimeRemainingForAction').show();
    var inputDate = params.SELLERBIDSELECTACTIONREQUIREDDATE;
    $listing.find('.jsTimeRemainingForAction').html(inputDate);
    var seller_action_time = getFormattedDate_timer(inputDate);
    $listing.find('.jsTimeRemainingForAction').countdown(inputDate, function(event) {
        $(this).text(event.strftime('%Dd %Hh %Mm'));
    });
    $listing.find('.jsSellerApprovalDtTitle').html('Time Remaining to Take Action');
    $listing.find('.jsTimeRemainingForActionLabel').show();
    $listing.find('.jsSellerApproveOffer').css('visibility', 'visible');
    $listing.find('.jsApproveOfferTimeOver').hide();
    var remainingTime = $listing.find('.jsTimeRemainingForAction').html();
    var arRemainingTime = remainingTime.split(' ');
    var remainingMin = arRemainingTime[2].replace("m", "");
    if (arRemainingTime[0] == '00d' && arRemainingTime[1] == '00h' && parseInt(remainingMin) < 30) {
        $listing.find('.jsTimeRemainingForAction').addClass('red-text');
    }
}

function fncConfigureListing_displayPaymentDueDate($listing, params) {
    $listing.find('.jsPaymentDueDateLabel').show();
    if (params.PAYMENTDUEDATE != undefined) {
        $listing.find('.jsPaymentDueDate').html(getFormattedDate(params.PAYMENTDUEDATE));
        $listing.find('.jsPaymentDueDate').addClass('myone-black');
        $listing.find('.jsPaymentDueDate').show();
    }
}

function fncConfigureListing_displayRemovalDeadline($listing, params) {
    $listing.find('.jsRemovalDeadlineLabel').show();
    if (params.REMOVALDEADLINE != undefined) {
        $listing.find('.jsRemovalDeadline').html(getFormattedDate(params.REMOVALDEADLINE));
        $listing.find('.jsRemovalDeadline').addClass('myone-black');
        $listing.find('.jsRemovalDeadline').show();
    }
}

function fncConfigureListing_displayRemovalListing($listing, params) {
    $listing.find('.jsRemovalLot').show();
    $listing.find('.jsRemovalLabel').show();
}

function fncConfigureListing_displaySalesAgreement($listing, params) {
    if (params.EQUIPMENTSALESAGREEMENT != undefined && params.EQUIPMENTSALESAGREEMENT != '') {
        $listing.find('.jsSalesAgreementLabel').show();
        $listing.find('.jsSalesAgreement').attr('href', params.EQUIPMENTSALESAGREEMENT);
    }
}

function fncConfigureListing_displayClaimDeadline($listing, params) {
    if (params.DISPUTEDEADLINE != undefined && params.DISPUTEDEADLINE != '') {
        $listing.find('.jsClaimDeadlineLabel').show();
        $listing.find('.jsClaimDeadline').html(getFormattedDate(params.DISPUTEDEADLINE));
    }
}

function fncConfigureListing_displayUnitNumber($listing, params) {
    if (params.UNITNUMBER != undefined) {
        $listing.find('.jsUnitNumberLabel').show();
        $listing.find('.jsUnitNumber').html(params.UNITNUMBER);
        $listing.find('.jsUnitNumber').show();
    }
}

function fncConfigureListing_displaySellerPhone($listing, params) {
    $listing.find('.jsRemovalPhoneLabel').show();
    if (params.REMOVALPHONE != undefined) {
        $listing.find('.jsRemovalPhone').html(fncPhoneNumberFormate(params.REMOVALPHONE));
        $listing.find('.jsRemovalPhone').attr('href', 'tel:+' + fncPhoneNumbersOnly(params.REMOVALPHONE));
        $listing.find('.jsRemovalPhone').show();
    }
}

function fncConfigureListing_displayReleaseDocument($listing, params) {
    $listing.find('.jsReleasDcoumentLabel').show();
    if (params.POB != undefined && params.POB != '') {
        $listing.find('.jsReleasDcoument').attr('href', params.POB);
    } else {
        $listing.find('.jsReleasDcoument').hide();
    }
}

function fncConfigureListing_displayInvoicePdf($listing, params) {
    $listing.find('.jsInvoicePdfLabel').show();
    if (params.INVOICE != undefined) {
        $listing.find('.jsInvoicePdf').attr('href', params.INVOICE);
    }
}

function fncConfigureListing_displayListingCol3Data($listing, params) {
    $listing.find('.jsListingCol3Data').show();
}

function fncConfigureListing_displaySellerName($listing, params) {
    $listing.find('.jsRemovalContactLabel').show();
    if (params.REMOVALCONTACT != undefined) {
        $listing.find('.jsRemovalContact').html(params.REMOVALCONTACT);
        $listing.find('.jsRemovalContact').show();
    }
}

function fncConfigureListing_displayBuyerPhone($listing, params) {
    $listing.find('.jsBuyerPhoneLabel').show();
    if (params.BUYERPHONE != undefined) {
        $listing.find('.jsBuyerPhone').html(fncPhoneNumberFormate(params.BUYERPHONE));
        $listing.find('.jsBuyerPhone').removeClass('myone-green');
        $listing.find('.jsBuyerPhone').addClass('myone-black');
        $listing.find('.jsBuyerPhone').show();
    }
}

function fncConfigureListing_displayBuyerName($listing, params) {
    $listing.find('.jsBuyerContactLabel').show();
    $listing.find('.jsBuyerContact').html(params.BUYERCONTACT);
    $listing.find('.jsBuyerContact').removeClass('myone-green');
    $listing.find('.jsBuyerContact').addClass('myone-black');
    $listing.find('.jsBuyerContact').show();
}

function fncConfigureListing_displayDeleteDraftListingConsole($listing, params) {
    $listing.find('.jsDeleteDraftListingConsole').show();
    fncDeleteDtaftListing('#listing-' + params.LISTINGID + ' .jsDeleteDtaftListing', params.LISTINGID, $listing);
}

function fncDeleteDtaftListing(selector, listingID, $listing) {
    $(selector).on('click', function() {
        ANAjax.exec('/payment?task=service_listing_update', {
            data: {
                listingId: listingID,
                status: 'deleted'
            },
            success: function(response) {
                var response = JSON.parse(response);
                if (response.status == 'success') {
                    if ($('#listing-' + listingID).length) {
                        if ($('#listing-' + listingID).remove()) {
                            var jsDraftListingCount = parseInt($('#jsDraftListingCount').html());
                            $('#jsDraftListingCount').html(jsDraftListingCount - 1);
                            var myone_darft_listing_count = $('.myone_darft_listing_count').html();
                            myone_darft_listing_count = myone_darft_listing_count.replace('[', '');
                            myone_darft_listing_count = myone_darft_listing_count.replace(']', '')
                            myone_darft_listing_count = parseInt(myone_darft_listing_count);
                            myone_darft_listing_count = myone_darft_listing_count - 1
                            $('.myone_darft_listing_count').html('[' + myone_darft_listing_count + ']');
                        }
                    }
                }
            }
        });
    });
}

function getFormattedDate(input) {
    var pattern = /(.*?)\/(.*?)\/(.*?)$/;
    var result = '';
    if (input != undefined) {
        result = input.replace(pattern, function(match, p1, p2, p3) {
            var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            return p2 + "-" + months[(p1 - 1)] + "-" + p3;
        });
    }
    return result;
}

function getFormattedDate_timer(input) {
    var pattern = /(.*?)\/(.*?)\/(.*?)$/;
    var result = '';
    if (input != undefined) {
        result = input.replace(pattern, function(match, p1, p2, p3) {
            var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            var
                p1 = parseInt(p1);
            p1 = p1 - 1;
            return months[p1] + ', ' + p2 + ", " + p3;
        });
    }
    return result;
}

function fncConfigureListing_displayVINorSerial($listing, params) {
    $listing.find('.jsVINorSerialLabel').show();
    if (params.SERIALNUMBER != undefined) {
        $listing.find('.jsVINorSerial').html(params.SERIALNUMBER);
        $listing.find('.jsVINorSerial').removeClass('myone-red');
        $listing.find('.jsVINorSerial').addClass('myone-black');
        $listing.find('.jsVINorSerial').show();
    }
}

function fncConfigureListing_displayNotSoldReason($listing, params) {
    if (params != undefined) {
        $listing.find('.jsNotSoldReasonLabel').show();
        $listing.find('.jsNotSoldReason').html(params.LOSTREASON).show();
    }
}

function fncConfigureListing_displayLateFee($listing, params) {
    $listing.find('.jsLateFeeLabel').show();
    if (params.LATEFEE != undefined) {
        $listing.find('.jsLateFee').html(params.LATEFEE);
        $listing.find('.jsLateFee').show();
        $listing.find('.jsLateFee').autoNumeric('init', {
            aSign: '$',
            aSep: ',',
            aDec: '.',
            mDec: '2',
            mRound: 'D'
        });
    }
}

function fncConfigureListing_displayPrice($listing, params) {
    $listing.find('.jsPriceLabel').show();
    if (params.LEADOFFERPRICE != undefined) {
        $listing.find('.jsPrice').html(params.LEADOFFERPRICE);
        $listing.find('.jsPrice').addClass('myone-black');
        $listing.find('.jsPrice').show();
        $listing.find('.jsPrice').autoNumeric('init', {
            aSign: '$',
            aSep: ',',
            aDec: '.',
            mDec: '2',
            mRound: 'D'
        });
    }
}

function fncConfigureListing_displayPriceLateFee($listing, params) {
    $listing.find('.jsPriceLateFeeLabel').show();
    if (params.PRICELATEFEE != undefined) {
        var PriceLateFee = fncCurrencyText('$', params.CURRENCYLOCALESHORT, params.INVOICEAMOUNT);
        $listing.find('.jsPriceLateFee').html(PriceLateFee);
        $listing.find('.jsPriceLateFee').show();
        $listing.find('.jsPriceLateFee').autoNumeric('init', {
            aSign: '$',
            aSep: ',',
            aDec: '.',
            mDec: '2',
            mRound: 'D'
        });
        $listing.find('.jsPriceLateFee').autoNumeric('set', PriceLateFee);
    }
}

function fncConfigureListing_displaySellersConsole($listing, params) {
    if (params.SALESTATUS == 'P') {
        $listing.find('.jsSellerDefinitesale').hide();
        $listing.find('.jsSellerDefinitesaleActive').hide();
        $listing.find('.jsSellerDefinitesaleConsole').show();
        $listing.find('.jsSellerApproveReject').removeClass('e1Hidden');
        $listing.find('.jsSellerApproveReject').show();
        fncConfigureListing_displayApproveRejectForSellerAction($listing, params);
    }
}

function fncConfigureListing_displayInvoiceDate($listing, params) {
    $listing.find('.jsClosedDateCol2').hide();
    $listing.find('.jsInvoiceDtLabel').show();
    if (params != undefined) {
        $listing.find('.jsInvoiceDt').html(getFormattedDate(params.INVOICEDATE));
    }
}

function fncConfigureListing_displayRemovalConfirmedDate($listing, params) {
    $listing.find('.jsRemovalConfirmDtLabel').show();
    if (params != undefined) {
        $listing.find('.jsRemovalConfirmDt').html('TBD');
    }
}

function fncConfigureListing_displayRejectionReason() {}

function fncConfigureListing_displayCountOffers($listing, params) {
    $listing.find('.jsViewsOffers').show();
    if (params != undefined) {
        if (params.OFFERS == 1)
            $listing.find('.jsOffersCount').html(params.OFFERS + ' Offer');
        else if (params.OFFERS > 1)
            $listing.find('.jsOffersCount').html(params.OFFERS + ' Offers');
        else
            $listing.find('.jsViewsOffers').hide();
    }
}

function fncConfigureListing_displayCountViews($listing, params) {
    $listing.find('.jsViewWatchers').show();
    if (params != undefined) {
        $listing.find('.jsViews').html(params.COUNTVIEWS);
        $listing.find('.jsWatchers').html(params.COUNTWATCHERS);
    }
}

function fncConfigureListing_displayApproveRejectForSellerAction($listing, params) {
    $listing.find('.jsSellerApproveReject').show();
    if (params != undefined) {
        fncInitMyoneSellerApproveOfferProcess('#listing-' + params.LISTINGID + ' .jsSellerApproveOffer', params.LISTINGID, $listing);
        fncInitMyoneSellerSendToRbaProcess('#listing-' + params.LISTINGID + ' .jsSellerSendRba', params.LISTINGID, $listing);
        fncInitMyoneSellerRejectOfferProcess('#listing-' + params.LISTINGID + ' .jsSellerRejectOffer', params.LISTINGID, $listing);
        fncInitMyoneSellerRejectMessageCounter('#listing-' + params.LISTINGID);
        fncInitMyoneSellerAcceptRerenectCancel('#listing-' + params.LISTINGID + ' .cancelApproveReject', $listing);
    }
}

function fncConfigureListing_displayApproveRejectForPendingListings($listing, params) {
    if (params != undefined) {
        $listing.find('.jsPendingApproveReject').show();
        fncInitMyonePendingApproveOfferProcess('#listing-' + params.LISTINGID + ' .jsSellerApproveOffer', params.LISTINGID, $listing);
        fncInitMyonePendingRejectOfferProcess('#listing-' + params.LISTINGID + ' .jsSellerRejectOffer', params.LISTINGID, $listing);
    }
}

function fncTrimUnusedFragments(listingConfig) {
    if (listingConfig.displayDefiniteSale == false || listingConfig.displayDefiniteSale == undefined) {
        $('.jsSellerDefinitesaleConsole').remove();
        $('.jsBuyersConsole').find('.jsDefiniteSale').remove();
    }
    if (listingConfig.displayApproveRejectForSellerAction == false || listingConfig.displayApproveRejectForSellerAction == undefined) {
        $('.jsSellerApproveReject').remove();
        $('.sellerApprovedMessage').remove();
    }
    if (listingConfig.displayBuyersConsole == false || listingConfig.displayBuyersConsole == undefined) {
        $('.jsBuyersConsole').remove();
        $('.jsBuyersConsole').find('.jsDefiniteSale').remove();
        $('.e1OfferBox').remove();
    }
}

function fncConfigureListing_displayBuyerQuestinsAnswersAction($listing, params) {
    $listing.find('.buyerQandA').show();
    if (params.QANDAS.COUNT != undefined && params.QANDAS.COUNT >= 1) {
        var questionCount = params.QANDAS.COUNT;
        for (i = 0; i < questionCount; i++) {
            var $question = $("#buyerQuestion").clone();
            var $answer = $("#buyerAsnwers").clone();
            $question.find('.jsQuestionDate').html(formatQaDate(params.QANDAS.RESULTS[i].QUESTION.QUESTIONDATE));
            $question.find('.jsquestionText').html(params.QANDAS.RESULTS[i].QUESTION.QUESTIONTEXT);
            if (params.QANDAS.RESULTS[i].ANSWER.ANSWERDATE != undefined && params.QANDAS.RESULTS[i].ANSWER.ANSWERDATE != 'none') {
                $answer.find('.jsAnswerDate').html(formatQaDate(params.QANDAS.RESULTS[i].ANSWER.ANSWERDATE));
            }
            if (params.QANDAS.RESULTS[i].ANSWER.ANSWEREDBY != undefined && params.QANDAS.RESULTS[i].ANSWER.ANSWEREDBY != '' && params.QANDAS.RESULTS[i].ANSWER.ANSWEREDBY != 'none') {
                $answer.find('.jsBuyerAnswer').html(formatQaDate(params.QANDAS.RESULTS[i].ANSWER.ANSWEREDBY));
            }
            $answer.find('.jsAnswer').html(params.QANDAS.RESULTS[i].ANSWER.ANSWERTEXT);
            if (params.QANDAS.RESULTS[i].ANSWER.ANSWERDATE != undefined) {
                $answer.find('.jsSellerAnswerDate').html(formatQaDate(params.QANDAS.RESULTS[i].ANSWER.ANSWERDATE));
            }
            $listing.find('.qa').append($question);
            $listing.find('.qa').append($answer);
        }
        var $askQuestion = $("#jsAskQuestion").clone();
        $listing.find('.qa').append($askQuestion);
    }
    if (params != undefined) {
        fncDisplayQuestionArea('#listing-' + params.LISTINGID + ' .jsAskAnotherQuestion', params.LISTINGID, $listing);
        fncProcessMyoneBuyerCancelQuestion('#listing-' + params.LISTINGID + ' .jsCancelQuestion', params.LISTINGID);
    }
}

function fncConfigureListing_displayBuyerQuestinsAnswersStyleChange($listing, params) {
    $listing.find('.jsdivexpand').removeClass('col-lg-15 col-md-14 col-sm-15 col-xs-24');
    $listing.find('.jsdivexpand').addClass('col-lg-24 col-md-24 col-sm-24 col-xs-24');
    $listing.find('.jsimgcol').removeClass('col-lg-10 col-md-8 col-sm-8 col-xs-24');
    $listing.find('.jsimgcol').addClass('col-lg-11 col-md-11 col-sm-11 col-xs-24');
    $listing.find('.jslothidden').hide();
    $listing.find('.jsimgdisplay').removeClass('col-sm-24');
    $listing.find('.jsimgdisplay').addClass('col-sm-13 col-lg-16 col-xs-24');
    $listing.find('.jstitlehide').addClass('e1Hidden');
    $listing.find('.jsdisplaytitle').removeClass('e1Hidden');
}

function fncConfigureListing_displayBuyingAwaitingRemoval($listing, params) {
    $listing.find('.jsdivexpand').removeClass('col-lg-15 col-md-14 col-sm-15 col-xs-24');
    $listing.find('.jsdivexpand').addClass('col-lg-24 col-md-24 col-sm-24 col-xs-24');
    $listing.find('.jsimgcol').removeClass('col-lg-9 col-md-8 col-sm-8 col-xs-24');
    $listing.find('.jsimgcol').addClass('col-lg-7 col-md-6 col-sm-6 col-xs-24');
    $listing.find('.jsRemovalextend').removeClass('col-lg-7 col-md-8 col-sm-8 col-xs-12');
    $listing.find('.jsRemovalextend').addClass('col-lg-6 col-md-6 col-sm-6 col-xs-12');
    $listing.find('.jsRemovalextendColThree').removeClass('col-lg-8 col-md-8 col-sm-8 col-xs-12');
    $listing.find('.jsRemovalextendColThree').addClass('col-lg-5 col-md-5 col-sm-6 col-xs-12');
    $listing.find('.jsListingCol3Data').removeClass('col-lg-7 col-md-8 col-sm-8 col-xs-24');
    $listing.find('.jsListingCol3Data').addClass('col-lg-6 col-md-6 col-sm-6 col-xs-24');
    $listing.find('.jsimgdisplay').removeClass('col-sm-24');
    $listing.find('.jsimgdisplay').addClass('col-xs-24');
}

function fncConfigureListing_displayEventsCol($listing, params) {
    $listing.find('.jsdivexpand').removeClass('col-lg-15 col-md-14 col-sm-15 col-xs-24');
    $listing.find('.jsdivexpand').addClass('col-lg-24 col-md-24 col-sm-24 col-xs-24');
    $listing.find('.jsimgcol').removeClass('col-lg-9 col-md-8 col-sm-8 col-xs-24');
    $listing.find('.jsimgcol').addClass('col-lg-6 col-md-5 col-sm-5 col-xs-24');
    $listing.find('.jsRemovalextend').removeClass('col-lg-7 col-md-8 col-sm-8 col-xs-12');
    $listing.find('.jsRemovalextend').addClass('col-lg-5 col-md-5 col-sm-5 col-xs-12');
    $listing.find('.jsRemovalextendColThree').removeClass('col-lg-8 col-md-8 col-sm-8 col-xs-12');
    $listing.find('.jsRemovalextendColThree').addClass('col-lg-5 col-md-5 col-sm-5 col-xs-12');
    $listing.find('.jsListingCol3Data').removeClass('col-lg-7 col-md-8 col-sm-8 col-xs-24');
    $listing.find('.jsListingCol3Data').addClass('col-lg-8 col-md-9 col-sm-9 col-xs-24');
    $listing.find('.jsimgdisplay').removeClass('col-sm-24');
    $listing.find('.jsimgdisplay').addClass('col-xs-24');
}

function fncConfigureListing_displayNoNe1LotsCol($listing, params) {
    $listing.find('.jsdivexpand').removeClass('col-lg-15 col-md-14 col-sm-15 col-xs-24');
    $listing.find('.jsdivexpand').addClass('col-lg-24 col-md-24 col-sm-24 col-xs-24');
    $listing.find('.jsimgcol').removeClass('col-lg-9 col-md-8 col-sm-8 col-xs-24');
    $listing.find('.jsimgcol').addClass('col-lg-6 col-md-6 col-sm-6 col-xs-24');
    $listing.find('.jsRemovalextend').removeClass('col-lg-7 col-md-8 col-sm-8 col-xs-12');
    $listing.find('.jsRemovalextend').addClass('col-lg-4 col-md-5 col-sm-5 col-xs-12');
    $listing.find('.jsRemovalextendColThree').removeClass('col-lg-8 col-md-8 col-sm-8 col-xs-12');
    $listing.find('.jsRemovalextendColThree').addClass('col-lg-5 col-md-5 col-sm-5 col-xs-12');
    $listing.find('.jsListingCol3Data').removeClass('col-lg-7 col-md-8 col-sm-8 col-xs-24');
    $listing.find('.jsListingCol3Data').addClass('col-lg-9 col-md-8 col-sm-8 col-xs-24');
    $listing.find('.jsimgdisplay').removeClass('col-sm-24');
    $listing.find('.jsimgdisplay').addClass('col-xs-24');
}

function fncDisplayQuestionArea(selector, listingID, $listing) {
    $(selector).on('click', function() {
        var $questionArea = $("#jsAskQuestionArea").clone();
        var currentdatetime = fncGetCurrentDateTime();
        $questionArea.find('.jsCurrentTime').html(currentdatetime);
        $listing.find('.qa').append($questionArea);
        $listing.find('.jsAskAnotherQuestion').hide();
        fncProcessMyoneBuyerSubmitQuestion('#listing-' + listingID + ' .jsSubmitQuestion', listingID);
        fncProcessMyoneBuyerCancelQuestion('#listing-' + listingID + ' .jsCancelQuestion', listingID);
    });
}

function fncProcessMyoneBuyerCancelQuestion(selector, listingID) {
    $('.jsCancelQuestion').on('click', function() {
        $listing = $('#listing-' + listingID);
        $listing.find('.jsAskQuestionArea').remove();
        var $askQuestion = $("#jsAskQuestion").clone();
        var $askQuestion = $("#jsAskQuestion").clone();
        $askQuestion.find('.jsQuestinText').val('');
        $listing.find('.qa').append($askQuestion);
        fncDisplayQuestionArea('#listing-' + listingID + ' .jsAskAnotherQuestion', listingID, $listing);
    });
}

function fncProcessMyoneBuyerSubmitQuestion(selector, listingID) {
    $(selector).on('click', function() {
        $listing = $('#listing-' + listingID);
        var questionText = $listing.find('.jsQuestinText').val().trim();
        if (questionText == '') {
            $("#qaErrorMessage").modal('show');
            return false;
        }
        $listing.find('.jsAskAnotherQuestion').hide();
        $('#current_fragment').val('listing-' + listingID);
        var params = {};
        var postfields = {};
        params.data = {};
        params.data.curl = '/listing/:listingID/question';
        params.url = pageConfig.defaultAjaxController;
        params.data.method = 'POST';
        params.data.listingID = listingID;
        postfields.questionText = questionText
        postfields.userID = ':userID';
        params.data.version = '1';
        params.data.postfields = JSON.stringify(postfields);
        params.success = fncMoneBuyerSubmitQuestionRender;
        fncAjax(params);
    });
}

function fncMoneBuyerSubmitQuestionRender() {
    var current_fragment = $('#current_fragment').val();
    $listing = $('#' + current_fragment);
    var listingIdAr = current_fragment.split('-');
    var $askQuestion = $("#jsAskQuestion").clone();
    var $askedQuestion = $("#buyerQuestionSent").clone();
    $listing.find('.qa').append($askQuestion);
    var questinText = $listing.find('.jsQuestinText').val();
    var currentdatetime = fncGetCurrentDateTime();
    $listing.find('.jsAskQuestion').remove();
    $listing.find('.jsAskQuestionArea').remove();
    $listing.find('.qa').append($askedQuestion);
    $listing.find('.qa').append($askQuestion);
    fncDisplayQuestionArea('#listing-' + listingIdAr[1] + ' .jsAskAnotherQuestion', listingIdAr[1], $listing);
}

function fncConfigureListing_displaySellerQuestinsAnswersAction($listing, params) {
    $listing.find('.jsSellerQandA').show();
    if (params.QANDAS.COUNT != undefined && params.QANDAS.COUNT >= 1) {
        var questionCount = params.QANDAS.COUNT;
        for (i = 0; i < questionCount; i++) {
            var $answer = $("#sellerQnA").clone();
            $answer.find('.jsQuestionDate').html(formatQaDate(params.QANDAS.RESULTS[i].QUESTION.QUESTIONDATE));
            $answer.find('.jsquestionText').html(params.QANDAS.RESULTS[i].QUESTION.QUESTIONTEXT);
            $answer.find('.jsSellerQuestionID').val(params.QANDAS.RESULTS[i].QUESTION.ID);
            if (params.QANDAS.RESULTS[i].ANSWER.ANSWEREDBY == '') {
                $answer.find('.jsSellerAnswerArea').addClass('question-' + params.QANDAS.RESULTS[i].QUESTION.ID);
                $answer.find('.jsSellerAnswerArea').show();
            } else {
                $answer.find('.jsSellerAnswerDate').html(formatQaDate(params.QANDAS.RESULTS[i].ANSWER.ANSWERDATE));
                $answer.find('.jsAnswer').html(params.QANDAS.RESULTS[i].ANSWER.ANSWERTEXT);
                $answer.find('.jsAnswerBy').html('- By ' + params.QANDAS.RESULTS[i].ANSWER.ANSWEREDBY);
                $answer.find('.jsSellerAnswer').show();
            }
            $listing.find('.Sellerqa').append($answer);
        }
        fncProcessSellerAnswer('#listing-' + params.LISTINGID + ' .jsSendForReview', params.LISTINGID);
    }
}

function fncConfigureListing_displayListingId($listing, params, update) {
    if (update == false) {
        $listing.find('.jsLotIDContainer').show();
        $listing.find('.jsLotID').html(params.LOTNUMBER);
    }
}

function fncConfigureListing_displayListingId2($listing, params, update) {
    if (update == false) {
        $listing.find('.jsLotIDContainer2').show();
        $listing.find('.jsLotID').html(params.LOTNUMBER);
    }
}

function fncConfigureListing_displayRBAListingId($listing, params) {
    $listing.find('.jsRBAListingIDContainer').show();
    $listing.find('.jsRBAListingIDContainer').find('.e1-label').html('Equipment ID');
    $listing.find('.jsRBAListingID').html(params.LISTINGID);
}

function fncConfigureListing_displayEventId($listing, params) {
    if (params.MARKETPLACE != 'RBA' && params.MARKETPLACE != 'SS') {
        $listing.find('.jsEventIDContainer').show();
        $listing.find('.jsEventID').html(params.LOTNUMBER);
    }
}

function fncGetImageURL(params) {
    var imageUrl;
    if (params.EVENTID != undefined) {
        var eventId = params.EVENTID;
    } else if (params.LOTNUMBER != undefined) {
        var strLot = params.LOTNUMBER;
        var arrLot = strLot.split('-');
        var eventId = arrLot[0].replace('EQ', '');
    } else if (params.AUCTIONID != undefined) {
        var eventId = params.AUCTIONID;
    }
    if (params.MARKETPLACE == 'RBA') {
        return params.FEATUREDIMAGEURL;
    }
    if (params.FEATUREDIMAGEURL != '' && params.FEATUREDIMAGEURL != undefined) {
        imageUrl = params.FEATUREDIMAGEURL.replace('small', 'large');
        if (imageUrl.indexOf("https://") == -1 && imageUrl.indexOf("http://") == -1) {
            imageUrl = CDN_IMAGES_BASE + '/' + imageUrl;
        }
    } else if (params.FEATUREDIMAGE != '' && params.FEATUREDIMAGE != undefined) {
        imageUrl = CDN_IMAGES_BASE + "/auction_media/" + eventId + "/" + params.LISTINGID + "/large/" + params.FEATUREDIMAGE;
    } else {
        imageUrl = params.thumbnail;
    }
    return imageUrl;
}

function fncConfigureListing_displayRelativeImage($listing, params) {
    var imageUrl = fncGetImageURL(params);
    if ((params.YEAR != '') && (params.YEAR != 'Unknown') && (params.YEAR != undefined)) {
        var par_YEAR = params.YEAR;
    } else {
        var par_YEAR = '';
    }
    if (params.MAKE != '' && params.MAKE != 'Unknown' && params.MAKE != undefined) {
        var par_MAKE = params.MAKE;;
    } else {
        var par_MAKE = '';
    }
    if (params.MODEL != '' && params.MODEL != 'Unknown' && params.MODEL != undefined) {
        var par_MODEL = params.MODEL;
    } else {
        var par_MODEL = '';
    }
    var alt_build = par_YEAR + ' ' + par_MAKE + ' ' + par_MODEL;
    $listing.find('.jsListingImage').attr('alt', alt_build.trim());
    if (imageUrl != '' && imageUrl != undefined) {
        $listing.find('.jsListingImage').on('error', function() {
            $(this).attr('src', CDN_BASE_URL + '/images/na.png');
        });
        $listing.find('.jsListingImage').attr('src', imageUrl);
        $listing.find('.jsListingImage').addClass('img-responsive');
    } else {
        $listing.find('.jsListingImage').attr('src', CDN_BASE_URL + '/images/na.png');
        $listing.find('.jsListingImage').addClass('img-responsive');
    }
}

function fncConfigureListing_displayAbsoluteImage($listing, params) {
    if ((params.YEAR != '') && (params.YEAR != 'Unknown') && (params.YEAR != undefined)) {
        var par_YEAR = params.YEAR;
    } else {
        var par_YEAR = '';
    }
    if (params.MAKE != '' && params.MAKE != 'Unknown' && params.MAKE != undefined) {
        var par_MAKE = params.MAKE;;
    } else {
        var par_MAKE = '';
    }
    if (params.MODEL != '' && params.MODEL != 'Unknown' && params.MODEL != undefined) {
        var par_MODEL = params.MODEL;
    } else {
        var par_MODEL = '';
    }
    var altbuild = par_YEAR + ' ' + par_MAKE + ' ' + par_MODEL;
    $listing.find('.jsListingImage').attr('alt', altbuild.trim());
    if (params.FEATUREDIMAGEURL != '' && params.FEATUREDIMAGEURL != undefined) {
        if (params.FEATUREDIMAGEURL.indexOf("small") > -1) {
            params.FEATUREDIMAGEURL = params.FEATUREDIMAGEURL.replace("small", "large");
        } else if ((params.FEATUREDIMAGEURL.indexOf("medium") > -1)) {
            params.FEATUREDIMAGEURL = params.FEATUREDIMAGEURL.replace("medium", "large");
        }
        $listing.find('.jsListingImage').on('error', function() {
            $(this).attr('src', CDN_BASE_URL + '/images/na.png');
        });
        $listing.find('.jsListingImage').attr('src', params.FEATUREDIMAGEURL);
    } else {
        $listing.find('.jsListingImage').attr('src', CDN_BASE_URL + '/images/na.png');
        $listing.find('.jsListingImage').addClass('img-responsive');
    }
}

function fncConfigureListing_displayListingLinks($listing, params) {
    $listing.find('.jsTitle').attr('href', "/listing?listingid=" + params.LISTINGID);
    $listing.find('.jsImageLink').attr('href', "/listing?listingid=" + params.LISTINGID);
}

function fncConfigureListing_displayEventLinks($listing, params) {
    $listing.find('.jsTitle').attr('href', "/event?eventid=" + params.AUCTIONID);
    $listing.find('.jsImageLink').attr('href', "/event?eventid=" + params.AUCTIONID);
}

function fncConfigureListing_displayRBALinks($listing, params) {
    var rbaLOGO = 'images/rba_logo.png';
    $listing.find('.jsTitle').on('click', function() {
        fncLeavingTheSite($listing, params.URL, rbaLOGO);
        return false;
    });
    $listing.find('.jsImageLink').on('click', function() {
        fncLeavingTheSite($listing, params.URL, rbaLOGO);
        return false;
    });
}

function fncConfigureListing_displaySalvageSaleLinks($listing, params) {
    var url = "http://www.salvagesale.com/general/LotDetail/LotNumber/" + params.LOTNUMBER;
    var ssLOGO = 'images/salvagesale-logo.png';
    $listing.find('.jsTitle').on('click', function() {
        fncLeavingTheSite($listing, url, ssLOGO);
        return false;
    });
    $listing.find('.jsImageLink').on('click', function() {
        fncLeavingTheSite($listing, url, ssLOGO);
        return false;
    });
}

function fncConfigureListing_displayABGLinks($listing, params) {
    var url = "http://auctionsbygov.com/General/LotDetail/LotNumber/" + params.LOTNUMBER;
    var abgLOGO = 'templates/ja_playmag/images/abgov-logo.png';
    $listing.find('.jsTitle').on('click', function() {
        fncLeavingTheSite($listing, url, abgLOGO);
        return false;
    });
    $listing.find('.jsImageLink').on('click', function() {
        fncLeavingTheSite($listing, url, abgLOGO);
        return false;
    });
}

function fncLeavingTheSite($listing, url, logo) {
    $listing.find('.jsLeavingTheSiteConfirmation').removeClass('e1Hidden hidden-lg hidden-sm hidden-xs hidden-md');
    $listing.find('.jsPOPupShow').removeClass('e1Hidden');
    $listing.find('.jsPOPupHide').hide();
    $('.div-disable').addClass('e1Hidden');
    $listing.find('.jsLOGO').html('<img alt="" class="text-right" src="/' + logo + '">');
    $listing.find('.jsLeaveTheSite').on('click', function() {
        window.open(url);
    });
    $listing.find('.jsStayOnSite').on('click', function() {
        $listing.find('.jsLeavingTheSiteConfirmation').addClass('e1Hidden hidden-lg hidden-sm hidden-xs hidden-md');
        $listing.find('.jsPOPupShow').addClass('e1Hidden');
        $listing.find('.jsPOPupHide').show();
        $('.div-disable').removeClass('e1Hidden');
    });
}

function fncConfigureListing_displayLotCount($listing, params) {
    $listing.find('.jsLotCountContainer').removeClass('e1Hidden');
    $listing.find('.jsLotCount').html(params.ACTIVELOTCOUNT);
}

function fncConfigureListing_displayHighlights($listing, params) {
    $('.jsListingCol3Data').removeClass('e1Hidden');
    $listing.find('.jsHighlightContainer').removeClass('e1Hidden');
    $listing.find('.jsHighlights').html(params.AUCTIONHIGHLIGHTS);
}

function fncConfigureListing_displayMakeModel($listing, params) {
    $('.jsListingCol3Data').removeClass('e1Hidden');
    $listing.find('.jsMakeContainer').removeClass('e1Hidden');
    if (params.MODEL == '--' || params.MODEL == undefined) {
        params.MODEL = '';
    }
    if (params.MAKE == undefined) {
        params.MAKE = '';
    }
    if (params.MODEL == '' && params.MAKE == '') {
        params.MODEL = '&mdash;';
    }
    $listing.find('.jsMake').html(params.MAKE + " " + params.MODEL);
}

function fncConfigureListing_displayYear($listing, params) {
    $('.jsListingCol3Data').removeClass('e1Hidden');
    $listing.find('.jsYearContainer').removeClass('e1Hidden');
    if (params.YEAR == undefined) {
        params.YEAR = '';
    }
    if (params.YEAR == '') {
        params.YEAR = '&mdash;';
    }
    $listing.find('.jsYear').html("" + params.YEAR);
}

function fncConfigureListing_displayMileage($listing, params) {
    $('.jsListingCol3Data').removeClass('e1Hidden');
    $listing.find('.jsMileageContainer').removeClass('e1Hidden');
    $listing.find('.jsMileage').html(params.MILEAGE);
}

function fncConfigureListing_displayCompare($listing, params) {
    $listing.find('.jsCompareContainer').removeClass('e1Hidden');
    $listing.find('.jsCompare').val(params.LISTINGID);
    $listing.find('.jsCompare').attr('checked', false);
}

function fncConfigureListing_displayAuctionNumber($listing, params) {
    if (VENUE.displayAuctionPeriodCount == false) {
        return;
    }
    $listing.find('.jsAuctionNumberLabel').removeClass('e1Hidden');
    $listing.find('.jsAuctionNumber').html(params.LISTINGCLOSEITERATION);
    $listing.find('.jsAuctionNumberColor').removeClass('green-text');
    $listing.find('.jsAuctionNumberColor').removeClass('orange-text');
    $listing.find('.jsAuctionNumberColor').removeClass('red');
    params.LISTINGCLOSEITERATION = parseInt(params.LISTINGCLOSEITERATION);
    switch (params.LISTINGCLOSEITERATION) {
        case 0:
        case 1:
            $listing.find('.jsAuctionNumberColor').addClass('green-text');
            break;
        case 2:
            $listing.find('.jsAuctionNumberColor').addClass('orange-text');
            break;
        case 3:
            $listing.find('.jsAuctionNumberColor').addClass('red');
            break;
    }
}

function formatQaDate(dt) {
    if (dt.indexOf("-") > -1) {
        var arDt = dt.split('-');
        var qaDate = arDt[0].slice(0, -4);
        return qaDate;
    }
}

function fncProcessSellerAnswer(selector, listingID) {
    $(selector).on('click', function() {
        $listing = $('#listing-' + listingID);
        var answerText = $(this).closest('.jsSellerAnswerArea').find('.jsAnswerText').val();
        if (fncTrimString(answerText) == '') {
            $("#qaSellerErrorMessage").modal('show');
            return false;
        }
        var questionID = $(this).closest('.jsSellerAnswerArea').find('.jsSellerQuestionID').val();
        $('#current_question_id').val(questionID);
        $('#current_fragment').val('listing-' + listingID);
        var params = {};
        var postfields = {};
        params.data = {};
        params.data.curl = '/listing/:listingID/answer';
        params.url = pageConfig.defaultAjaxController;
        params.data.version = '1';
        params.data.method = 'POST';
        params.data.listingID = listingID;
        postfields.listingID = listingID;
        postfields.questionID = questionID;
        postfields.answerText = answerText;
        postfields.userID = ':userID';
        params.data.postfields = JSON.stringify(postfields);
        params.success = fncMoneSelerSubmitAnswerRender;
        fncAjax(params);
    });
}

function fncMoneSelerSubmitAnswerRender() {
    var current_question_fragment = $('#current_question_id').val();
    $question = $('.question-' + current_question_fragment);
    $question.find('.jsAnswerTextArea').hide();
    $question.find('.jsAnswerSuccessMsg').show();
}

function fncGetCurrentDateTime() {
    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var D = new Date();
    var date1 = D.getDate();
    var month1 = monthNames[D.getMonth()];
    var year1 = D.getFullYear();
    var hours = D.getHours();
    var minutes = D.getMinutes();
    minutes = (minutes < 10) ? ("0" + minutes) : minutes;
    delete D;
    return month1 + ', ' + date1 + ' ' + year1 + ' ' + hours + ":" + minutes;
}

function fncLogAjaxError(response, errorStack, page) {
    var pageConfig = {};
    pageConfig.defaultAjaxController = '/home?task=ajax';
    var params = {};
    params.data = {};
    params.data.log = "\n ***** " + page + " PROCESSING EXCEPTION **** \n " + "API Response : " + JSON.stringify(response) + "\n  " + "Error Message :" + errorStack.message + " \n  " + "Exception Stack :" + errorStack.stack + "\n " + "User Agent :" + JSON.stringify($.browser) + "\n  " + "Error Occured Page :" + page + "\n" + "*****EXCEPTION END ****";
    params.data.version = 1;
    params.data.curl = '/log';
    params.url = pageConfig.defaultAjaxController;
    fncAjax(params);
}

/*===============================
https://www.equipmentone.com/e1_2/js/buyersConsoleUtils.js
================================================================================*/
;

function fncShowLogin(parms) {
    var $previousListing = $('.e1LoginBox.jsLiveLogin').closest('.listing-item');
    $previousListing.find('.e1OfferButton').removeClass('e1Hidden');
    $previousListing.find('.e1MinOffer').removeClass('e1Hidden');
    if ($previousListing.find('.jsHighBid').html() != '$0.00') {
        $previousListing.find('.jsHighOffer2').removeClass('e1Hidden');
    }
    $previousListing.find('.jsDefiniteSaleBuyer').removeClass('e1Hidden');
    $previousListing.find('.jsHr').removeClass('e1Hidden');
    if (typeof hiddenLoginBoxText != "undefined") {
        $previousListing.find('.jsRibbonText').html(hiddenLoginBoxText);
    }
    $('.e1LoginBox.jsLiveLogin').remove();
    var loginTemplate = $('#e1LoginTemplate').clone();
    loginTemplate.removeAttr('id');
    loginTemplate.removeClass('e1Hidden');
    loginTemplate.removeClass('jsTemplate');
    loginTemplate.addClass('jsLiveLogin');
    $(this).closest('.e1OfferButton').addClass('e1Hidden');
    $(this).closest('.e1OfferButton').after(loginTemplate);
    var $listing = $(this).closest('.listing-item');
    hiddenLoginBoxText = $listing.find('.jsRibbonText').html();
    var params = parms.data.event_data;
    if (params == undefined) {
        params = {};
    }
    params.LOGINFORM = true;
    fncSetBuyersConsoleRibbonText($listing, params);
}

function fncAddBuySell() {
    $('.e1BuySellMessage.jsLiveAddBuySell').closest('.listing-item').find('.e1OfferButton').removeClass('e1Hidden');
    $('.e1BuySellMessage.jsLiveAddBuySell').remove();
    var buySellTemplate = $('#e1BuySellTemplate').clone();
    buySellTemplate.removeAttr('id');
    buySellTemplate.removeClass('e1Hidden');
    buySellTemplate.removeClass('jsTemplate');
    buySellTemplate.addClass('jsLiveAddBuySell');
    $(this).closest('.e1OfferButton').addClass('e1Hidden');
    $(this).closest('.e1OfferButton').after(buySellTemplate);
    var $listing = $(this).closest('.listing-item');
    $listing.find('.jsBidMessage').removeClass('e1Hidden');
    var currentRibbon = $listing.find('.jsRibbonText').text();
    $listing.find('.jsRibbonText').html('MEMBERS ONLY');
    $listing.find('.e1MinOffer').addClass('e1Hidden');
    $listing.find('.jsHighOffer2').addClass('e1Hidden');
    $listing.find('.jsBuyersConsoleMessage').addClass('e1Hidden');
    $listing.find('.jsHr').addClass('e1Hidden');
    $listing.find('.jsStayAsLiteUser').on('click', function() {
        $listing.find('.jsBidMessage').addClass('e1Hidden');
        $listing.find('.e1OfferButton').removeClass('e1Hidden');
        $listing.find('.e1BuyerConsole').removeClass('e1Hidden');
        $listing.find('.e1MinOffer').removeClass('e1Hidden');
        $listing.find('.jsHighOffer2').removeClass('e1Hidden');
        $listing.find('.jsBuyersConsoleMessage').removeClass('e1Hidden');
        $listing.find('.jsRibbonText').html(currentRibbon);
        $listing.find('.jsHr').removeClass('e1Hidden');
    })
    $listing.find('.jsConvertLiteUser').attr('href', "/registration?view=fullregistration");
}

function fncCancelAddBuySell() {
    $('.e1BuySellMessage.jsLiveAddBuySell').closest('.e1ListingOffer').find('.e1OfferButton').removeClass('e1Hidden');
    $('.e1BuySellMessage.jsLiveAddBuySell').remove();
}

function fncUpdateBuyersConsole(selector, params, skipPriceUpdate) {
    if (typeof skipPriceUpdate == 'undefined') {
        skipPriceUpdate = false;
    }
    fncDisplayPriceData(params, skipPriceUpdate);
}

function fncInitBuyersConsole(selector, params) {
    $(selector).find('.salesStatus').val(params.SALESTATUS);
    $(selector).find('.auctionEndDateUtc').val(params.AUCTIONENDDATETIMEUTC);
    $(selector).find('.e1LotId').val(params.LOTNUMBER);
    if ($('input#isLiteUser').val() === 'true') {
        $(selector).find('jsMinOffer').addClass('e1Hidden');
    } else {
        $(selector).find('jsMinOffer').removeClass('e1Hidden');
    }
    userTermsAgreed = false;
    $(selector).on('focusin', '.e1OfferButton.jsOfferLive > .jsOfferInput', fncSetPrice);
    $(selector).on('keyup', '.e1OfferButton input', fncLockPriceInput);
    $(selector).on('focusout', '.e1OfferButton.jsOfferLive > .jsOfferInput', fncRemovePrice);
    $(selector).off('click', '.jsMakeOffer');
    $(selector).off('click', '.jsBuyItNow');
    $(selector).on('click', '.jsMakeOffer', fncMakeOffer);
    $(selector).on('click', '.jsBuyItNow', fncMakeOffer);
    $(selector).on('click', '.jsConfirmOffer', fncConfirmOffer);
    $(selector).on('click', '.jsCancelOffer', fncCancelOffer);
    $(selector).on('click', '.jsSubmitOffer', fncSubmitBid);
    $(selector).on('click', '.jsLogin', {
        event_data: params
    }, fncShowLogin);
    $(selector).on('click', '.jsAddBuySell', fncAddBuySell);
    $(selector).on('click', '.jsCancelAddBuySell', fncCancelAddBuySell);
    $(selector).on('click', '.jsLogin', function() {
        setCookie('CURRENT_LISTING_SPAN', $(this).parents('.e1ListingItem')[1]['id'], 1);
    });
    $(selector).find('.e1OfferButton').find('.jsPlaceholder').html($(selector).find('.jsMinOffer').text().substring(0, $(selector).find('.jsMinOffer').text().length - 6));
    $(selector).on('click', '.e1OfferButton .jsPlaceholder', fncRemovePlacehoder);
    $(selector).on('keyup', '.jsOfferInput', function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            $(this).parent().find('input:button').click()
        }
    });
    var salesStatus = params.SALESTATUS;
    var $listing = $(selector).closest('.listing-item');
    fncDisplayPriceData(params, false);
}

function fncDisplayDefiniteSale($listing, params) {
    if (params != undefined) {
        $('.jsDefiniteSaleBuyer').removeClass('e1Hidden');
        if (params.DEFINITESALE == 'Y' || params.DEFINITESALE == true || params.DEFINITESALE == 'Seller Will Sell' || params.SELLERBIDSELECTION == 'Seller Will Sell') {
            if (params.COUNTOFFERS == '--' || params.COUNTOFFERS == 0 || params.COUNTOFFERS == undefined) {
                $listing.find('.jsDefiniteSaleText').html('FIRST BID MEETS RESERVE');
            } else {
                $listing.find('.jsDefiniteSaleText').html('RESERVE MET');
            }
        } else {
            $listing.find('.jsDefiniteSaleText').html('RESERVE NOT MET');
        }
    }
}

function fncConfigureClosed($listing, params) {
    objStatusText = {
        A: 'Closed',
        E: "Closed",
        P: "Closed [Sale Pending]",
        PF: "Closed [Sold]",
        PL: "Closed [Sold]",
        SC: "Closed [Sold]",
        CS: "Closed [Sold]",
        L: "Closed [Not Available]",
        D: "Closed [Not Available]",
        S: "Pending &#150; Offer Period Begins Soon"
    }
    $listing.find('.e1BuyerConsole').removeClass('default');
    $listing.find('.e1BuyerConsole').removeClass('winning');
    $listing.find('.e1BuyerConsole').removeClass('outbid');
    $listing.find('.e1BuyerConsole').addClass('closed');
    $listing.find('.e1MinOffer').addClass('e1Hidden');
    $listing.find('.e1OfferButton').addClass('e1Hidden');
    $listing.find('.jsOfferType').addClass('e1Hidden');
    $listing.find('.jsHr').addClass('e1Hidden');
    $listing.find('.jsHighOffer2').closest('.row').find('.jsHr').remove();
    $listing.find('.jsHighOffer2').closest('.row').find('.jsHrClosed').remove();
    $listing.find('.jsHighOffer2').closest('.row').append('<div class="col-xs-24 jsHrClosed"><div class="jsHr"></div></div>');
    $listing.find('.jsClosingTimeLabel').html('Time Remaining');
    $listing.find('.jsTimeLeft').html('Closed');
    $listing.find('.jsTimeLeft').addClass('error');
    $listing.find('.jsBidMessage').addClass('e1Hidden');
    $listing.find('.e1OfferDisclaimer').addClass('e1Hidden');
    if (typeof params.SALESTATUS == 'undefined') {
        $listing.find('.jsRibbonText').html('CLOSED');
    } else {
        $listing.find('.jsRibbonText').html(objStatusText[params.SALESTATUS]);
    }
}

function fncSetBuyersConsoleRibbonText($listing, params) {
    $listing.find('.ribbon').removeClass('e1Hidden');
    $listing.find('.e1MinOffer').addClass('e1Hidden');
    $listing.find('.jsHighOffer2').addClass('e1Hidden');
    if (params.LOGINFORM == true) {
        $listing.find('.e1BuyerConsole').addClass('default');
        if (params.COUNTOFFERS == '--' || params.COUNTOFFERS == '0') {
            $listing.find('.jsRibbonText').html('<span class="e1signin-bid">SIGN IN TO PLACE THE FIRST BID</span>');
        } else {
            $listing.find('.jsRibbonText').html('SIGN IN TO PLACE A BID');
        }
        $listing.find('.jsBuyersConsoleMessage').addClass('e1Hidden');
        $listing.find('.jsHr').html('');
        $listing.find('.jsHr').addClass('e1Hidden');
    }
}

function fncRemovePlacehoder() {
    $(this).closest('.e1ListingItem').find('.e1OfferButton').find('.jsOfferInput').focus();
    $(this).closest('.e1ListingItem').find('.e1OfferButton').find('.jsPlaceholder').addClass('e1Hidden');
}

function fncSetPrice(evnt) {
    var listingId = $(this).closest('.e1ListingItem').find('.jsOfferInput').attr('data-lotnum');
    if (evnt.type == "focusin" || (evnt.type == "click" && $(this).hasClass('jsBuyItNow'))) {
        $(this).closest('.e1ListingItem').find('.e1OfferButton').find('.jsPlaceholder').addClass('e1Hidden');
    } else {
        $(this).closest('.e1ListingItem').find('.e1OfferButton').find('.jsPlaceholder').removeClass('e1Hiden');
    }
    $(this).closest('.e1ListingItem').find('.jsOfferInput').autoNumeric('init', {
        aSign: '$',
        aSep: ',',
        aDec: '.',
        mDec: '2',
        mRound: 'D',
        lZero: 'deny'
    });
    if ($(this).hasClass('jsBuyItNow')) {
        $(this).closest('.e1ListingItem').find('.e1OfferButton').removeClass('jsOfferLive');
        var buyItNowPrice = Number($(this).closest('.e1ListingItem').find('.e1BuyItNowPrice').html().replace(/[^0-9\.]+/g, ""));
        $(this).closest('.e1ListingItem').find('.jsOfferInput').not('.e1ConfirmInput').autoNumeric('set', buyItNowPrice);
        $(this).closest('.e1ListingItem').find('.e1OfferButton').find('.jsOfferInput').prop('disabled', true);
        $(this).closest('.e1ListingItem').find('.e1OfferButton').find('.jsPlaceholder').addClass('e1Hidden');
    } else {
        $(this).closest('.e1ListingItem').find('.jsOfferInput').val('');
        $(this).closest('.e1ListingItem').find('.e1OfferButton').addClass('jsOfferLive');
        $(this).closest('.e1ListingItem').find('.e1OfferButton').find('.jsOfferInput').prop('disabled', false);
    }
}

function fncRemovePrice() {
    $(this).closest('.e1ListingItem').find('.e1OfferButton').addClass('jsOfferLive');
    $(this).closest('.e1ListingItem').find('.e1OfferButton').find('.jsPlaceholder').removeClass('e1Hidden');
    $(this).val('');
}

function fncLockPriceInput() {
    if (typeof($(this).val()) != 'undefined' && $(this).val() != 0 && $(this).val() != '') {
        $(this).closest('.e1OfferButton').removeClass('jsOfferLive');
    } else {
        $(this).closest('.e1OfferButton').addClass('jsOfferLive');
    }
}

function fncMakeOffer() {
    $(this).closest('.e1ListingItem').find('.jsOfferInput').autoNumeric('init', {
        aSign: '$',
        aSep: ',',
        aDec: '.',
        mDec: '0',
        mRound: 'D',
        lZero: 'deny',
        zCent: '1'
    });
    if ($(this).val() == 'PLACE A BID') {
        if ($(this).closest('.e1ListingItem').find('.e1OfferButton').hasClass('jsOfferLive') && $(this).closest('.e1ListingItem').find('.jsOfferInput').val() == "") {
            offerAmt = Number($(this).closest('.e1ListingItem').find('.e1OfferButton').find('.jsPlaceholder').html().replace(/[^0-9\.]+/g, ""));
            formattedAmt = $(this).closest('.e1ListingItem').find('.e1OfferButton').find('.jsPlaceholder').html();
        } else {
            offerAmt = $(this).closest('.e1ListingItem').find('.jsOfferInput').autoNumeric('get');
            formattedAmt = $(this).closest('.e1ListingItem').find('.jsOfferInput').val();
        }
    } else {
        offerAmt = Number($(this).closest('.e1ListingItem').find('.e1BuyItNowPrice').html().replace(/[^0-9\.]+/g, ""));
        formattedAmt = $(this).closest('.e1ListingItem').find('.e1BuyItNowPrice').html();
        $(this).closest('.e1ListingItem').find('[data-bidtype="buyitnow"]').prop("checked", true);
    }
    $(this).closest('.e1ListingItem').find('.e1OfferButton').find('.jsPlaceholder').addClass('e1Hidden');
    updateID = [$(this).closest('.e1ListingItem').find('.jsOfferInput').attr('data-lotNum')];
    confirmTemplate = $('.e1OfferBox.jsTemplate').clone(true);
    var blnHasChild = false;
    objUserAccounts = {};
    fncGetParentChild();
    if (objUserAccounts != null && $.keys(objUserAccounts).length > 1) {
        blnHasChild = true;
        var objTmpUserAccounts = {};
        for (var k in objUserAccounts)
            objTmpUserAccounts[k] = objUserAccounts[k];
        userListDiv = $('<div class="e1UserSelect"><div class="e1UserSelectPrompt">Submit on behalf of company:</div></div>');
        userList = $('<ul class="list-unstyled behalflist">');
        $('<li><span class="behalf-company"><input type="radio" name="userSelectID" value="loggeduser" checked="checked"><label>' + objTmpUserAccounts.loggeduser + '</label></span></li>').appendTo(userList);
        delete objTmpUserAccounts.loggeduser;
        $.each(objTmpUserAccounts, function(accountID, accountName) {
            userAccount = $('<li><span class="behalf-company"><input type="radio" name="userSelectID" value="' + accountID + '#-#' + accountName + '"><label>' + accountName + '</label></span></li>');
            userAccount.appendTo(userList);
        });
        userList.appendTo(userListDiv);
    }
    if (offerAmt > 0) {
        $(this).closest('.e1OfferConsole').find('.jsOfferType input').prop('disabled', true);
        $(this).closest('.e1OfferConsole').find('.jsOfferType input').addClass('compare-disable');
        $(this).closest('.e1ListingItem').find('.e1OfferConsole').find('.jsBidMessage').find('span').text('');
        $(this).closest('.e1ListingItem').find('.e1OfferConsole').find('.jsBidMessage').removeClass('e1BidError').addClass('e1Hidden');
        $(this).closest('.e1ListingItem').find('.jsOfferType').after(confirmTemplate);
        confirmTemplate.find('.e1ConfirmInput').autoNumeric('init', {
            aSign: '$',
            aSep: ',',
            aDec: '.',
            mDec: '0',
            mRound: 'D',
            lZero: 'deny',
            zCent: '1'
        });
        confirmTemplate.find('.e1ConfirmInput').attr('data-lotNum', $(this).closest('.e1ListingItem').find('.jsOfferInput').attr('data-lotNum'));
        confirmTemplate.removeClass('jsTemplate');
        confirmTemplate.removeClass('e1Hidden');
        confirmTemplate.find('.jsConfirmAmt').html(formattedAmt);
        confirmTemplate.find('.jsOfferInput').focus();
        $(this).closest('.e1ListingItem').find('.e1OfferButton').find('.jsOfferInput').autoNumeric('set', offerAmt);
        $(this).closest('.e1ListingItem').find('.e1OfferButton').find('.jsOfferInput').prop('disabled', true);
        $(this).closest('.e1ListingItem').find('.e1OfferButton').find('.jsMakeOffer').addClass('e1ButtonNegative e1GrayShadow jsCancelOffer');
        $(this).closest('.e1ListingItem').find('.jsConfirmOffer').find('.jsMakeOffer').width($(this).closest('.e1ListingItem').find('.e1OfferButton').find('.jsMakeOffer').width());
        $(this).closest('.e1ListingItem').find('.e1OfferButton').find('.jsMakeOffer').val('UNDO');
        $(this).closest('.e1ListingItem').find('.e1OfferButton').find('.jsMakeOffer').removeClass('btn-primary');
        $(this).closest('.e1ListingItem').find('.e1OfferButton').find('.jsMakeOffer').addClass('btn-info');
        $(this).closest('.e1ListingItem').find('.e1OfferButton').find('.jsMakeOffer').removeClass('e1Buttons e1OrangeShadow jsMakeOffer');
        $(this).closest('.e1ListingItem').find('.e1OfferButton').find('.jsOfferInput').autoNumeric('set', formattedAmt);
        $(this).closest('.e1ListingItem').find('.e1OfferButton').addClass('disabledShaded');
        if (blnHasChild) {
            confirmTemplate.find('.e1ConfirmButton').after(userListDiv);
        }
        confirmAmt = 0;
    } else if (offerAmt > 0 && offerAmt < priceDataUpdate.nextOffer) {
        $(this).closest('.e1ListingItem').find('.e1OfferConsole').find('.jsBidMessage').find('span').text('Offer amount is lower than minimum amount required');
        $(this).closest('.e1ListingItem').find('.e1OfferConsole').find('.jsBidMessage').removeClass('e1Hidden e1BidSuccess').addClass('e1BidError');
        $(this).closest('.e1ListingItem').find('.e1OfferButton').addClass('jsOfferLive');
    } else {
        $(this).closest('.e1ListingItem').find('.e1OfferConsole').find('.jsBidMessage').find('span').text('Offer amount cannot be empty and must be greater than 0');
        $(this).closest('.e1ListingItem').find('.e1OfferConsole').find('.jsBidMessage').removeClass('e1Hidden e1BidSuccess').addClass('e1BidError');
        $(this).closest('.e1ListingItem').find('.e1OfferButton').addClass('jsOfferLive');
    }
}

function fncConfirmOffer() {
    $('.jsBidMessage').addClass('e1Hidden');
    confirmAmt = $(this).closest('.e1ConfirmButton').find('.jsOfferInput').autoNumeric('get');
    if (objUserAccounts != null && $.keys(objUserAccounts).length > 1) {
        selectedUserID = $(this).closest('.e1OfferBox').find('input[name="userSelectID"]:checked').val();
    } else {
        selectedUserID = 'loggeduser';
    }
    if (typeof selectedUserID == 'undefined') {
        $(this).closest('.e1OfferBox').find('.jsConfirmError').find('span').text('Must select a company!');
        $(this).closest('.e1OfferBox').find('.jsConfirmError').removeClass('e1Hidden');
        $(this).closest('.e1OfferBox').find('.jsConfirmError').addClass('e1BidError');
        $(this).closest('.e1OfferBox').find('input[name="userSelectID"]:first').focus();
    } else if (offerAmt == confirmAmt && $(this).closest('.e1ConfirmButton').find('.jsOfferInput').val() == formattedAmt) {
        var listingID = $(this).closest('.e1ConfirmButton').find('.jsOfferInput').attr('data-lotNum');
        var bidType = $(this).closest('.e1OfferConsole').find('[name="e1Bid_' + listingID + '"]:checked').attr('data-bidtype');
        var selector = '#listing-' + listingID;
        $(selector).find('input[type=button][value=UNDO]').prop('disabled', true);
        $(selector).find('input[type=button][value=UNDO]').addClass('compare-disable')
        $(selector).find('.jsProcessBidVal').removeClass('e1Hidden');
        $(selector).find('.jsSubmitBidButtons').removeClass('e1Hidden');
        fncValidateBid(listingID, bidType, selectedUserID);
    } else {
        $(this).closest('.e1OfferBox').find('.jsConfirmError').find('span').text('Amounts do not match!');
        $(this).closest('.e1OfferBox').find('.jsConfirmError').removeClass('e1Hidden');
        $(this).closest('.e1OfferBox').find('.jsConfirmError').addClass('e1BidError');
        $(this).closest('.e1OfferBox').find('.jsOfferInput').val('');
        $(this).closest('.e1OfferBox').find('.jsOfferInput').focus();
    }
}

function fncCancelOffer() {
    objOfferConsole = $(this).closest('.e1OfferConsole');
    objE1ListingItem = $(this).closest('.e1ListingItem');
    objOfferConsole.find('.jsOfferType input').prop('disabled', false);
    objOfferConsole.find('.jsOfferType input').removeClass('compare-disable');
    objOfferConsole.find('.jsOfferType').removeClass('e1Hidden');
    var currentMethods = objE1ListingItem.find('.jsSelectedMethods').val();
    objE1ListingItem.find('.e1OfferBox').remove();
    if (currentMethods.indexOf('MAX') != -1 || currentMethods.indexOf('EXACT') != -1) {
        objOfferConsole.find('.e1MinOffer').removeClass('e1Hidden');
        objOfferConsole.find('.e1OfferButton').removeClass('e1Hidden');
        objE1ListingItem.find('.e1OfferAmt').removeClass('activeBid');
        objE1ListingItem.find('.e1OfferButton').find('.jsOfferInput').prop('disabled', false);
        objE1ListingItem.find('.e1OfferButton').addClass('jsOfferLive');
        objE1ListingItem.find('.e1OfferButton').find('.jsOfferInput').val('');
        objE1ListingItem.find('.e1OfferButton').find('.jsPlaceholder').removeClass('e1Hidden');
        objE1ListingItem.find('.e1OfferButton').find('.jsCancelOffer').addClass('e1Buttons e1OrangeShadow jsMakeOffer');
        objE1ListingItem.find('.e1OfferButton').find('.jsCancelOffer').removeClass('e1ButtonNegative e1GrayShadow jsCancelOffer');
        objE1ListingItem.find('.e1OfferButton').find('.jsMakeOffer').val('PLACE A BID');
        objE1ListingItem.find('.e1OfferButton').find('.jsMakeOffer').removeClass('btn-info');
        objE1ListingItem.find('.e1OfferButton').find('.jsMakeOffer').addClass('btn-primary');
        objE1ListingItem.find('.e1OfferButton').find('.jsMakeOffer').css('width', '');
        objE1ListingItem.find(':radio[value="Max Bid"]').prop('checked', 'checked');
        objE1ListingItem.find('.jsBidMessage').removeClass('e1BidError').addClass('e1Hidden');
        objE1ListingItem.find('.e1OfferButton').focusin();
        objE1ListingItem.find('.e1OfferButton').removeClass('disabledShaded');
    }
    if (objE1ListingItem.find('.jsRadioBuyItNow input').prop('checked') == true) {
        objE1ListingItem.find('.e1OfferButton').removeClass('jsOfferLive');
        objE1ListingItem.find('.e1OfferButton').find('.jsPlaceholder').addClass('e1Hidden');
        var buyItNowPrice = Number(objE1ListingItem.find('.e1BuyItNowPrice').html().replace(/[^0-9\.]+/g, ""));
        objE1ListingItem.find('.jsOfferInput').autoNumeric('set', buyItNowPrice);
        objE1ListingItem.find('.e1OfferButton').find('.jsOfferInput').prop('disabled', true);
    }
}

function fncValidateBid(listingID, bidType, selectedUserID) {
    var selector = '#listing-' + listingID;
    var objData = {
        e1bid_listingid: listingID,
        e1bid_bidamount: offerAmt,
        e1bid_bidtype: bidType,
        e1bid_bidAmountPer: null
    };
    if (selectedUserID != null) {
        objData.userid = selectedUserID;
    }
    userIDSelected = selectedUserID;
    var strData = $.param(objData);
    fncAjax({
        url: "/index.php?option=com_e1listing&format=raw&task=validateBid",
        type: "POST",
        data: strData,
        success: function(bidData) {
            $('.e1UserSelect').remove();
            $(selector).find('.jsProcessBidVal').addClass('e1Hidden');
            $(selector).find('input[type=button][value=UNDO]').prop('disabled', false);
            $(selector).find('input[type=button][value=UNDO]').removeClass('compare-disable');
            fncShowPremium(bidData, listingID);
        },
        error: function(bidData) {
            $(selector).find('.jsProcessBidVal').addClass('e1Hidden');
            $(selector).find('input[type=button][value=UNDO]').prop('disabled', false);
            $(selector).find('input[type=button][value=UNDO]').removeClass('compare-disable');
            $(selector).find('.jsBidMessage').find('span').html('Unable to validate offer at this time.');
            $(selector).find('.jsBidMessage').removeClass('e1Hidden');
            $(selector).find('.jsBidMessage').addClass('e1BidError').removeClass('e1BidSuccess');
        },
        dataType: 'json'
    });
}

function fncShowPremium(bidData, listingID) {
    var selector = '#listing-' + listingID + ' .e1BuyerConsole';
    fncGetUserTermsStatus(updateID, true);
    if (userTermsAgreed == false) {
        bidData.e1_status = 'error';
        bidData.e1_error = "Unable to validate offer. Please try again.";
    } else {
        userAgreed = userTermsAgreed.userAgreed;
        showSSA = userTermsAgreed.showSSA;
        salesAgreement = userTermsAgreed.salesAgreement;
        defSale = userTermsAgreed.definiteSale;
    }
    if (bidData.e1_status == 'error') {
        $(selector).find('.jsCancelOffer').click();
        $(selector).find('.jsBidMessage').find('span').html(bidData.e1_error);
        $(selector).find('.jsBidMessage').removeClass('e1Hidden');
        $(selector).find('.jsBidMessage').addClass('e1BidError');
    } else {
        $(selector).find('.e1OfferAmt').addClass('activeBid');
        $(selector).find('.e1OfferBox').find('.e1ConfirmText').addClass('e1Hidden');
        $(selector).find('.e1OfferConsole').find('.jsOfferType').addClass('e1Hidden');
        $(selector).find('.e1OfferConsole').find('.e1MinOffer').addClass('e1Hidden');
        $(selector).find('.e1OfferButton').addClass('e1Hidden');
        $(selector).find('.e1OfferBox').find('.e1OfferDisclaimer').removeClass('e1Hidden');
        $(selector).find('.e1OfferBox').find('.e1ConfirmButton').addClass('e1Hidden');
        $(selector).find('.e1OfferBox').find('.jsConfirmError').addClass('e1Hidden');
        $(selector).find('.e1OfferBox').find('.e1OfferConsole').addClass('e1Hidden');
        $(selector).find('.e1OfferBox').find('.e1OfferConsole').find('.jsOfferType input').prop('disabled', false);
        $(selector).find('.e1OfferBox').find('.e1ListingItem').find('.e1OfferButton').removeClass('disabledShaded');
        if (showSSA) {
            $(selector).find('.e1OfferBox').find('.jsShowSSA').closest('.textTwo').removeClass('e1Hidden');
            $(selector).find('.e1OfferBox').find('.jsShowSSA').closest('.textTwo').find('a').attr('href', salesAgreement);
        } else {
            $(selector).find('.e1OfferBox').find('.jsShowSSA').closest('.textTwo').addClass('e1Hidden');
        }
        if (userAgreed) {
            $(selector).find('.e1OfferBox').find('.jsShowTerms').closest('.textTwo').addClass('e1Hidden');
        } else {
            $(selector).find('.e1OfferBox').find('.jsShowTerms').closest('.textTwo').removeClass('e1Hidden');
        }
        if (defSale) {
            $(selector).find('.e1OfferBox').find('.jsDefSale').closest('.textOne').removeClass('e1Hidden');
            $(selector).find('.e1OfferBox').find('.jsSellerSelect').closest('.textOne').addClass('e1Hidden');
            $(selector).find('.e1OfferBox').find('.jsAutoBidSelect').removeClass('e1Hidden');
            $(selector).find('.e1OfferBox').find('.jsSellerBidSelect').addClass('e1Hidden');
        } else {
            $(selector).find('.e1OfferBox').find('.jsDefSale').closest('.textOne').addClass('e1Hidden');
            $(selector).find('.e1OfferBox').find('.jsSellerSelect').closest('.textOne').removeClass('e1Hidden');
            $(selector).find('.e1OfferBox').find('.jsAutoBidSelect').addClass('e1Hidden');
            $(selector).find('.e1OfferBox').find('.jsSellerBidSelect').removeClass('e1Hidden');
        }
        var bidAmount = parseInt(bidData.e1_content.BIDAMOUNT);
        var buyerPremium = parseFloat(bidData.e1_content.BUYERPREMIUMAMOUNT);
        var totalBid = (parseInt(bidData.e1_content.BIDAMOUNT) + parseFloat(bidData.e1_content.BUYERPREMIUMAMOUNT));
        fncBuyersConsoleFormatCurrency($(selector), '.jsBidAmtFinal', bidAmount);
        fncBuyersConsoleFormatCurrency($(selector), '.jsPremiumAmt', buyerPremium, undefined, '2');
        fncBuyersConsoleFormatCurrency($(selector), '.jsBidTotal', totalBid, undefined, '2');
        send2Monetate.conversionAmount = totalBid;
    }
}

function fncSubmitBid() {
    var console = $(this);
    if (($(this).closest('ul').find('.jsTermsAgree').prop('checked') === true && $(this).closest('ul').find('.jsTermsAgree').closest('.textTwo').hasClass('e1Hidden') == false) || $(this).closest('ul').find('.jsTermsAgree').closest('.textTwo').hasClass('e1Hidden') == true) {
        agreedToSSA = true;
    } else {
        agreedToSSA = false;
    }
    if (($(this).closest('ul').find('.jsSiteTermsAgree').prop('checked') === true && $(this).closest('ul').find('.jsSiteTermsAgree').closest('.textTwo').hasClass('e1Hidden') == false) || $(this).closest('ul').find('.jsSiteTermsAgree').closest('.textTwo').hasClass('e1Hidden') == true) {
        agreedToTerms = true;
    } else {
        agreedToTerms = false;
    }
    if (agreedToTerms === true && agreedToSSA === true) {
        $(this).closest('.e1ListingItem').find('.e1OfferConsole').find('.jsBidMessage').removeClass('e1BidError').addClass('e1Hidden');
        var listingID = $(this).closest('.e1OfferConsole').find('.e1OfferButton').find('.jsOfferInput').attr('data-lotNum');
        var bidType = $(this).closest('.e1OfferConsole').find('[name="e1Bid_' + listingID + '"]:checked').attr('data-bidtype');
        fncPlaceBid(listingID, bidType, console);
    } else {
        $(this).closest('.listing-item').find('.jsBidMessage').addClass('e1BidError').removeClass('e1Hidden e1BidSuccess');
        $(this).closest('.listing-item').find('.jsBidMessage').find('span').html('You must agree to all terms to place a bid.');
    }
}

function fncPlaceBid(listingID, bidType, console) {
    var objData = {
        e1bid_listingid: listingID,
        e1bid_bidamount: offerAmt,
        e1bid_bidtype: bidType,
        e1bid_userId: userIDSelected,
        e1bid_bidAmountPer: null,
        isTerms: 'yes'
    };
    var strData = $.param(objData);
    listingBidID = listingID;
    $('.jsAjaxSpinnerContainer').removeClass('e1Hidden');
    $('.jsSubmitBidButtons').addClass('e1Hidden');
    fncAjax({
        url: "/index.php?option=com_e1listing&format=raw&task=placeBid",
        type: "POST",
        data: strData,
        success: function(bidResponse) {
            fncBidComplete(bidResponse, listingBidID, console);
        },
        error: function(bidResponse) {
            var errorString = {
                e1_status: 'error',
                e1_content: {
                    ERRORMESSAGE: 'Unable to place offer at this time.'
                }
            };
            fncBidComplete(errorString, listingBidID, console);
        },
        dataType: 'json'
    });
}

function fncBidComplete(bidResponse, listingID, console) {
    var selector = '#listing-' + listingID;
    var $listing = $(selector);
    var listingId = $(console).closest('.e1ListingItem').find('.e1ListingID span.e1ListingIDText').html();
    var lotID = $(console).closest('.e1ListingItem').find('.e1LotId').val();
    if (bidResponse.e1_status == 'success') {
        var bidamount = bidResponse.e1_content.CURRENTOFFERAMT;
        var premium = bidResponse.e1_content.COA_BUYERPREMIUMAMT;
    }
    dataLayer.push({
        "lotid": lotID,
        "bidamount": bidamount,
        "premium": premium,
        "grosstransvalue": (bidamount + premium),
        "event": "Make an Offer"
    });
    send2Monetate.objConversion.itemId = listingID;
    send2Monetate.objConversion.unitPrice = String(send2Monetate.conversionAmount);
    send2Monetate.objConversion.quantity = 1;
    send2Monetate.addConversionRows();
    $(selector).find('.jsBidMessage').find('span').html(bidResponse.e1_error);
    $(selector).find('.jsBidMessage').removeClass('e1Hidden');
    var updateID = [listingID];
    $(selector).find('.jsCancelOffer').click();
    $('.jsSubmitBidButtons').removeClass('e1Hidden');
    $('.jsAjaxSpinnerContainer').addClass('e1Hidden');
    if (bidResponse.e1_status != 'success') {
        $(selector).find('.jsBidMessage').addClass('e1BidError').removeClass('e1BidSuccess').removeClass('e1Hidden');
    } else {
        var params = {};
        params.source = 'fncBidComplete';
        var response = fncAjaxResponse(bidResponse, params);
        response.LISTINGID = listingID;
        UserListings.fncAddListing(listingID, 'BUYINGACTIVE', response.MYMAXBID);
        currentDate = fncGetCurrentDate();
        var closeDate = new Date(response.AUCTIONENDDATETIMEUTC);
        if (currentDate > closeDate) {
            fncConfigureClosed($(selector), response);
        } else {
            fncSetColor($listing, response.MYRANK);
        }
        fncSetMaxBid($listing, response.MYMAXBID);
    }
}

function fncUpdateWinning(selector, rank) {
    var objCurrentListing = $(selector).closest('.e1ListingItem');
    if (rank == 1) {
        objCurrentListing.find('.e1BuyerConsole').removeClass('red-offer-console');
        objCurrentListing.find('.e1BuyerConsole').addClass('green-offer-console');
        objCurrentListing.find('.offer-heading').text("You're Winning");
        objCurrentListing.find('.offer-heading').removeClass('red-text');
        objCurrentListing.find('.offer-heading').addClass('green-text');
    } else {
        var strRank = $.numToOrd(rank) + " Place";
        objCurrentListing.find('.e1BuyerConsole').removeClass('green-offer-console');
        objCurrentListing.find('.e1BuyerConsole').addClass('red-offer-console');
        objCurrentListing.find('.offer-heading').text(strRank);
        objCurrentListing.find('.offer-heading').removeClass('green-text');
        objCurrentListing.find('.offer-heading').addClass('red-text');
    }
}

function fncStripTags(strText) {
    if (typeof(strText) != 'undefined') {
        var cleanText = strText.replace(/<\/?[^>]+(>|$)/g, "");
        return cleanText;
    }
}

function fncGetSolrTime(callback) {
    fncAjax({
        url: '/util.php',
        data: {
            action: 'getSolrTime'
        },
        type: 'post',
        async: false,
        success: function(solrTime) {
            if (typeof callback != 'undefined') {
                callback(solrTime);
            }
            serverTime = solrTime;
        }
    });
}

function fncGetUserTermsStatus(strListingID, returnData) {
    returnData = (typeof(returnData) == 'undefined') ? false : true;
    asyncCall = (typeof(returnData) == 'undefined' || returnData == false) ? true : false;
    fncAjax({
        url: '/index.php?option=com_e1listing&format=raw&task=loadBidDetails&listingid=' + strListingID,
        type: "POST",
        data: null,
        async: asyncCall,
        success: function(response) {
            var defSale = false;
            if (response.e1_status == 'success') {
                defSale = (typeof(response.e1_content.SELLERBIDSELECTION) != 'undefined' && response.e1_content.SELLERBIDSELECTION == 'Seller Will Sell') ? true : false;
                userTermsAgreed = {
                    userAgreed: response.e1_content.TERMSACCEPTED,
                    showSSA: response.e1_content.COESA,
                    salesAgreement: response.e1_content.EQUIPMENTSALESAGREEMENT,
                    definiteSale: defSale
                };
            } else {
                defSale = false;
                userTermsAgreed = false;
            }
        },
        error: function(bidData) {
            userTermsAgreed = false;
        },
        dataType: 'json'
    });
}

function fncGetParentChild() {
    fncAjax({
        url: '/index.php?option=com_e1home&format=raw&task=check_for_parent',
        type: "POST",
        data: null,
        async: false,
        success: function(userAccounts) {
            objUserAccounts = userAccounts;
        },
        error: function(bidData) {
            objUserAccounts = null;
        },
        dataType: 'json'
    });
}

function doRefreshListing(bidpopupStatus) {
    if ($('#pagename').val() == 'listing') {
        var txt_bidAmount = $('#txt_bidAmount').val();
        var ofTotal = parseInt($('#ofTotal').val());
        if (ofTotal > 10 && $('#ofCount').val() < $('#ofTotal').val()) {
            $('.offMore').show();
        }
        loadDynamicdetails(bidpopupStatus);
        if ($('#ofCount').val() == $('#ofTotal').val()) {
            $('.offMore').slideUp('slow');
        }
    }
}

function fncGetCurrentDate() {
    currentDate = new Date(parseInt(dbTime));
    currentDate.setSeconds(currentDate.getSeconds() + Math.ceil((Math.ceil(new Date().getTime()) - parseInt(dbTime)) / 1000 + timeOffsetSecs))
    return currentDate;
}

function fncGetCommerceMethods(listingItem, data) {
    priceData = {};
    if (data.ALLOWEDCOMMERCEMETHODS != undefined) {
        changeDate = new Date(data.ALLOWEDCOMMERCEMETHODS.CHANGEMETHODSAT);
        currentDate = fncGetCurrentDate();
        endDate = new Date(data.AUCTIONENDDATETIMEUTC);
        if ((typeof data.FORCECHANGE != undefined && data.FORCECHANGE == true) || currentDate > changeDate) {
            priceData.commerceMethods = data.ALLOWEDCOMMERCEMETHODS.NEXTCOMMERCEMETHODS;
        } else {
            priceData.commerceMethods = data.ALLOWEDCOMMERCEMETHODS.CURRENTMETHODS;
        }
        if (currentDate >= endDate) {
            priceData.commerceMethods = "";
        }
        listingItem.find('.jsSelectedMethods').val(priceData.commerceMethods);
        listingItem.find('.jsCommerceMethods').val(data.ALLOWEDCOMMERCEMETHODS.CURRENTMETHODS);
        listingItem.find('.jsChangeMethodsAt').val(data.ALLOWEDCOMMERCEMETHODS.CHANGEMETHODSAT);
        listingItem.find('.jsNextCommerceMethods').val(data.ALLOWEDCOMMERCEMETHODS.NEXTCOMMERCEMETHODS);
    }
    if (data.SELLERBIDSELECTION == 'Seller Will Sell') {
        priceData.defSale = 'Y';
    } else {
        priceData.defSale = 'N';
    }
    priceData.currentOffer = parseInt(data.LEADOFFERPRICE);
    priceData.nextOffer = parseInt(data.NEXTOFFERAMOUNT);
    if (priceData.commerceMethods != undefined && priceData.commerceMethods.search('BIN') == -1) {
        priceData.buyItNow = 'N';
    } else {
        priceData.buyItNow = 'Y';
    }
    if (priceData.commerceMethods != undefined && priceData.commerceMethods.search('EXACT') == -1) {
        priceData.exactOffer = 'N';
    } else {
        priceData.exactOffer = 'Y';
    }
    if (priceData.commerceMethods != undefined && priceData.commerceMethods.search('MAX') == -1) {
        priceData.maxOffer = 'N';
    } else {
        priceData.maxOffer = 'Y';
    }
    priceData.buyItNowPrice = (data.BUYITNOWPRICE != null) ? parseInt(data.BUYITNOWPRICE) : null;
    priceData.saleStatusText = objStatusText[data.SALESTATUS];
    priceData.timeLeft = fncStripTags(data.TIMELEFT);
    return priceData;
}

function fncDisplayPriceData(data, skipPriceUpdate) {
    objStatusText = {
        A: 'Closed',
        E: "Closed",
        P: "Closed [Sale Pending]",
        PF: "Closed [Sold]",
        PL: "Closed [Sold]",
        SC: "Closed [Sold]",
        CS: "Closed [Sold]",
        L: "Closed [Not Available]",
        D: "Closed [Not Available]",
        S: "Pending &#150; Offer Period Begins Soon"
    }
    if (data != null) {
        var index = data.LISTINGID;
        var listingItem = $('#listing-' + index);
        priceData = fncGetCommerceMethods(listingItem, data);
    }
    var lastUpdated = $('#' + index).attr('data-lastUpdated');
    if (typeof lastUpdated != 'undefined' && intTimeStamp < lastUpdated) {
        return true;
    } else {
        $('#' + index).attr('data-lastUpdated', intTimeStamp);
    }
    if (data != null && data.COUNTOFFERS != '--' && data.COUNTOFFERS > 0) {
        listingItem.find('.jsHighOffer2').removeClass('e1Hidden');
    }
    if (data != null && skipPriceUpdate == false) {
        var leadOfferPrice = data.LEADOFFERPRICE;
        var locale = data.CURRENCYLOCALE;
        var country = 'us';
        if (locale != undefined)
            country = locale.substring(locale.length - 2).toLowerCase()
        fncBuyersConsoleFormatCurrency(listingItem, '.jsHighBid', leadOfferPrice, country);
    }
    listingItem.find('.jsMinOffer').autoNumeric('init', {
        aSign: '$',
        aSep: ',',
        aDec: '.',
        mDec: '2',
        mRound: 'D',
        lZero: 'deny'
    });
    listingItem.find('.e1CurrentOffer').removeClass('e1Invisible');
    listingItem.find('.e1OfferButton > button').removeClass('e1Invisible');
    listingItem.find('.e1OfferConsole').removeClass('e1Invisible');
    listingItem.find('.e1OfferConsole').find('.jsOfferType').find('input').attr('name', 'e1Bid_' + index);
    if (priceData.buyItNow == 'Y') {
        listingItem.find('.jsRadioBuyItNow').removeClass('e1Hidden');
        listingItem.find('.e1BuyItNowPrice').autoNumeric('init', {
            aSign: '$',
            aSep: ',',
            aDec: '.',
            mDec: '2',
            mRound: 'D',
            lZero: 'deny'
        });
        listingItem.find('.e1BuyItNowPrice').autoNumeric('set', priceData.buyItNowPrice);
        fncBuyitnowCurrency(listingItem, priceData.buyItNow);
    } else {
        listingItem.find('.buy-btn-section').addClass('e1Hidden');
    }
    var timeRemainingLabel = 'Time Remaining';
    if (data != null) {
        if (data.ALLOWEDCOMMERCEMETHODS != undefined) {
            if (typeof data.ALLOWEDCOMMERCEMETHODS.CHANGEMETHODSAT != 'undefined' && data.ALLOWEDCOMMERCEMETHODS.CHANGEMETHODSAT != "")
                changeDate = new Date(data.ALLOWEDCOMMERCEMETHODS.CHANGEMETHODSAT);
            currentDate = fncGetCurrentDate();
        }
        if ((data.FORCECHANGE == true) || (data.ALLOWEDCOMMERCEMETHODS != undefined && data.ALLOWEDCOMMERCEMETHODS.CURRENTMETHODS != 'BIN')) {
            if (data.FORCECHANGE == true) {
                listingItem.find('.buyersConsoleOverlay').removeClass('largeOverlay');
                listingItem.find('.buyersConsoleOverlay').removeClass('smallOverlay');
                listingItem.find('.e1BuyerConsole').addClass('default');
                listingItem.find('.e1OfferButton').find('.jsPlaceholder').html(listingItem.find('.jsMinOffer').text());
            }
        }
        if (priceData.commerceMethods == "" || data.ALLOWEDCOMMERCEMETHODS == undefined) {
            listingItem.find('.e1BuyerConsole').addClass('closed');
            fncConfigureClosed(listingItem, data);
            if (skipPriceUpdate == false) {
                fncDisplayDefiniteSale(listingItem, data);
            }
        } else if ((RESTRICT_BUYITNOW == true && (priceData.commerceMethods == 'BIN')) || data.SALESTATUS == 'S') {
            if (RESTRICT_BUYITNOW == true) {
                var previewData = jQuery.extend(true, {}, data);
                previewData.ALLOWEDCOMMERCEMETHODS.CURRENTMETHODS = previewData.ALLOWEDCOMMERCEMETHODS.NEXTCOMMERCEMETHODS;
                previewData.ALLOWEDCOMMERCEMETHODS.NEXTCOMMERCEMETHODS = "";
                previewData.CHANGEMETHODSAT = "";
                priceData = fncGetCommerceMethods(listingItem, previewData);
                listingItem.find('.jsHrClosed').remove();
                timeRemainingLabel = 'Time Until Auction';
                listingItem.find('.e1OfferButton').find('.jsPlaceholder').removeClass('e1Hidden');
                listingItem.find('.jsOfferType').removeClass('e1Hidden');
                listingItem.find('.jsClosingTimeLabel').html(timeRemainingLabel);
                listingItem.find('.jsBidControls').removeClass('e1Hidden');
                listingItem.find('.e1MinOffer').removeClass('e1Hidden');
                listingItem.find('.e1OfferButton').removeClass('e1Hidden');
                listingItem.find('.jsHr').removeClass('e1Hidden');
                listingItem.find('.e1OfferBox').addClass('e1Hidden');
                listingItem.find('.jsSubmitBidButtons').removeClass('e1Hidden');
                if (priceData.maxOffer == 'N') {
                    listingItem.find('.jsMaxBidOption').addClass('e1Hidden');
                }
                if (priceData.exactOffer == 'N') {
                    listingItem.find('.jsExactBidOption').addClass('e1Hidden');
                }
            }
            listingItem.find('.e1BuyerConsole').removeClass('closed');
            listingItem.find('.e1BuyerConsole').addClass('default');
            listingItem.find('.jsRibbonText').html('LOT IN PREVIEW');
            listingItem.find('.e1OfferButton').find('.jsOfferInput').attr({
                'data-lotNum': index,
                'readonly': true
            });
            if (priceData.buyItNow == 'Y') {
                listingItem.find('.buyersConsoleOverlay').addClass('largeOverlay');
                listingItem.find('.buyersConsoleOverlay').css("height", "247px");
            } else {
                listingItem.find('.buy-btn-section').addClass('e1Hidden');
                if (E1_USERID != '') {
                    listingItem.find('.buyersConsoleOverlay').addClass('largeOverlay');
                } else {
                    listingItem.find('.buyersConsoleOverlay').addClass('smallOverlay');
                }
            }
            listingItem.find('.buyersConsoleOverlay').show();
            fncDisplayDefiniteSale(listingItem, data);
        } else {
            listingItem.find('.jsHrClosed').remove();
            listingItem.find('.e1BuyerConsole').addClass('default');
            listingItem.find('.e1OfferButton').find('.jsPlaceholder').html(listingItem.find('.jsMinOffer').text().substring(0, listingItem.find('.jsMinOffer').text().length - 6));
            listingItem.find('.e1OfferButton').find('.jsOfferInput').attr({
                'data-lotNum': index,
                'readonly': false
            });
            if (IS_SELLER == true) {
                listingItem.find('.jsRibbonText').html('THIS IS YOUR LOT');
            } else if (data.COUNTOFFERS == '--' || data.COUNTOFFERS == '0' || data.COUNTOFFERS == undefined) {
                if (E1_USERID != '')
                    listingItem.find('.jsRibbonText').html('PLACE FIRST BID');
                else
                    listingItem.find('.jsRibbonText').html('<span class="e1signin-bid">SIGN IN TO PLACE THE FIRST BID</span>');
            } else {
                if (E1_USERID != '')
                    listingItem.find('.jsRibbonText').html('PLACE A BID');
                else
                    listingItem.find('.jsRibbonText').html('SIGN IN TO PLACE A BID');
            }
            if (E1_USERID != '') {
                var isBidding = false;
                var activeListings = UserListings.fncGetBuyingActive();
                for (i in activeListings) {
                    if (activeListings[i].LISTINGID == index) {
                        isBidding = true;
                        myMaxBid = activeListings[i].MYMAXBID;
                    }
                }
                var rank = UserListings.fncGetUserRank(data.HIGHBIDUSERID);
                if (isBidding) {
                    fncSetColor(listingItem, rank);
                    fncSetMaxBid(listingItem, myMaxBid);
                } else if (IS_SELLER == false) {
                    listingItem.find('.e1BuyerConsole').removeClass('winning');
                    listingItem.find('.e1BuyerConsole').removeClass('closed');
                    listingItem.find('.e1BuyerConsole').removeClass('outbid');
                    listingItem.find('.e1BuyerConsole').addClass('default');
                    if (data.COUNTOFFERS == '--' || data.COUNTOFFERS == '0' || data.COUNTOFFERS == undefined) {} else {
                        listingItem.find('.jsRibbonText').html('PLACE YOUR FIRST BID');
                    }
                }
            }
            fncDisplayDefiniteSale(listingItem, data);
            if (E1_USERID != '') {
                var myMaxBid = 0;
                if (priceData.buyItNow == 'Y') {
                    listingItem.find('.jsOfferType').removeClass('e1Hidden');
                    listingItem.find('.jsRadioBuyItNow').removeClass('e1Hidden');
                    listingItem.find('.e1BuyItNowPrice').autoNumeric('init', {
                        aSign: '$',
                        aSep: ',',
                        aDec: '.',
                        mDec: '2',
                        mRound: 'D',
                        lZero: 'deny'
                    });
                    listingItem.find('.e1BuyItNowPrice').autoNumeric('set', priceData.buyItNowPrice);
                    listingItem.find('.buy-btn-section').removeClass('e1Hidden');
                } else {
                    listingItem.find('.buy-btn-section').addClass('e1Hidden');
                }
                if (priceData.maxOffer == 'N' && priceData.exactOffer == 'N') {
                    listingItem.find('.e1MinOffer').addClass('e1Hidden');
                    listingItem.find('.e1OfferButton').addClass('e1Hidden');
                    listingItem.find('.jsBidControls').addClass('e1Hidden');
                    listingItem.find('.jsHr').addClass('e1Hidden');
                    listingItem.find('.jsSubmitBidButtons').addClass('e1Hidden');
                    if (priceData.buyItNow == 'Y') {
                        listingItem.find('.jsClosingTimeLabel').html('Time Until Auction');
                    }
                } else {
                    listingItem.find('.jsOfferType').removeClass('e1Hidden');
                    listingItem.find('.jsClosingTimeLabel').html(timeRemainingLabel);
                    listingItem.find('.jsBidControls').removeClass('e1Hidden');
                    listingItem.find('.e1MinOffer').removeClass('e1Hidden');
                    listingItem.find('.e1OfferButton').removeClass('e1Hidden');
                    listingItem.find('.jsHr').removeClass('e1Hidden');
                    listingItem.find('.e1OfferBox').addClass('e1Hidden');
                    listingItem.find('.jsSubmitBidButtons').removeClass('e1Hidden');
                    if (priceData.maxOffer == 'N') {
                        listingItem.find('.jsMaxBidOption').addClass('e1Hidden');
                    }
                    if (priceData.exactOffer == 'N') {
                        listingItem.find('.jsExactBidOption').addClass('e1Hidden');
                    }
                }
            }
        }
    }
    listingItem.find('.e1OfferButton').removeClass('e1Invisible');
    if (priceData.currentOffer == 0) {
        var locale = data.CURRENCYLOCALE;
        var country = 'us';
        if (locale != undefined)
            country = locale.substring(locale.length - 2).toLowerCase();
        fncBuyersConsoleFormatCurrency(listingItem, '.jsMinOffer', priceData.nextOffer, country);
    } else {
        fncBuyersConsoleFormatCurrency(listingItem, '.jsMinOffer', priceData.nextOffer);
    }
    listingItem.find('.e1OfferButton').find('input').autoNumeric('init', {
        aSign: '$',
        aSep: ',',
        aDec: '.',
        mDec: '0',
        mRound: 'D',
        lZero: 'deny',
        zCent: '1'
    });
    listingItem.find('.e1OfferButton').find('input').attr('data-lotNum', index);
    listingItem.closest('.e1ListingDetails').find('.e1TimeLeftText').html(priceData.timeLeft);
}

function fncSetColor(listingItem, rank) {
    var ribbonTxt = listingItem.find('.jsRibbonText').text();
    if (ribbonTxt != undefined && ribbonTxt.indexOf('Closed') != -1) {
        return;
    }
    listingItem.find('.e1BuyerConsole').removeClass('default');
    listingItem.find('.e1BuyerConsole').removeClass('winning');
    listingItem.find('.e1BuyerConsole').removeClass('outbid');
    if (rank == 1) {
        listingItem.find('.e1BuyerConsole').addClass('winning');
        listingItem.find('.jsRibbonText').html("YOU'RE WINNING");
    } else {
        listingItem.find('.e1BuyerConsole').addClass('outbid');
        listingItem.find('.jsRibbonText').html("YOU'VE BEEN OUTBID");
    }
}

function fncSetMaxBid(listingItem, myMaxBid) {
    if (myMaxBid != 0) {
        listingItem.find('.jsMyMaxBidText').removeClass('e1Hidden');
    }
    fncBuyersConsoleFormatCurrency(listingItem, '.jsMyMaxBidValue', myMaxBid);
}

function fncBuyersConsoleFormatCurrency(listingItem, selector, amount, flagCountry, allowDecimalPlaces) {
    locale = listingItem.find('.locale_short').val();
    if (typeof allowDecimalPlaces == 'undefined') {
        allowDecimalPlaces = '0';
    }
    if (amount > 0) {
        listingItem.find(selector).html(amount);
        listingItem.find(selector).autoNumeric('init', {
            aSign: '$',
            aSep: ',',
            aDec: '.',
            mDec: allowDecimalPlaces,
            mRound: 'D',
            lZero: 'deny',
            zCent: '1'
        });
        listingItem.find(selector).autoNumeric('set', amount);
    } else {
        listingItem.find(selector).html("$0");
    }
    listingItem.find(selector).append('<span> (' + locale + ')</span>');
    if (flagCountry != undefined) {
        listingItem.find(selector).append('<div class="flag-wrapper" style="display:inline;"><div class="img-thumbnail locale_flag flag flag-icon-background flag-icon-' + flagCountry + '" title="' + flagCountry + '" id="ca"></div></div>');
    }
    if (locale == '') {
        locale = 'USD';
        flagCountry = 'us';
    }
}

function fncBuyitnowCurrency(listingItem, buyItNowValue) {
    if (buyItNowValue == 'Y') {
        var locale = listingItem.find('.locale_short').val();
        if (locale != undefined || locale != '')
            listingItem.find('.jsBuyItNowLocale').html('(' + locale + ')');
        else
            listingItem.find('.jsBuyItNowLocale').html('(USD)');
    }
}

function fncSetRibbon(objItem, ribbonColor, ribbonText) {
    objItem.find('.ribbon').find('.text').html(ribbonText);
    objItem.find('.ribbon').attr('class', 'ribbon ribbon-' + ribbonColor);
    objItem.find('.ribbon').removeClass('e1Hidden');
}

/*===============================
https://www.equipmentone.com/e1_2/js/searchUtils.js
================================================================================*/
;
listingConfig = {};

function fncInitSearch() {
    $('.t3-content-mast-top').show();
    blnFiltersModified = false;
    curDate = new Date();
    objMonths = {
        0: 'January',
        1: 'February',
        2: 'March',
        3: 'April',
        4: 'May',
        5: 'June',
        6: 'July',
        7: 'August',
        8: 'September',
        9: 'October',
        10: 'November',
        11: 'December'
    };
    objEncodedQuery = fnGetURLParameter('eqs');
    objStoredQuery = (typeof sessionStorage.getItem('EQ1_query') != 'undefined' && sessionStorage.getItem('EQ1_query') != null) ? sessionStorage.getItem('EQ1_query') : {};
    objStoredPosition = (typeof sessionStorage.getItem('scrollPos') != 'undefined' && sessionStorage.getItem('scrollPos') != null) ? sessionStorage.getItem('scrollPos') : $('#e1CompareBar').offset().top;
    var keyword = fnGetURLParameter('kw');
    keyword = (keyword == false) ? fnGetURLParameter('q') : keyword;
    keyword = (keyword == false) ? fnGetURLParameter('qterm') : keyword;
    objLegacySavedQuery = location.hash;
    venuekey = VENUE['venueKey'];
    browseAllLotsUrl = getVenuePage('browsealllots');
    querySite = (typeof sessionStorage.getItem('sourceSite') != 'undefined' && sessionStorage.getItem('sourceSite') != null) ? sessionStorage.getItem('sourceSite') : venuekey;
    objCompareList = {};
    objSiteSelected = VENUE['searchResultsSiteIdSelect'];
    for (var k in objSiteSelected) {
        if (k.replace(/\s/g, '') != k) {
            objSiteSelected[k.replace(/\s/g, '-')] = objSiteSelected[k];
            delete objSiteSelected[k];
        }
    }
    displayType = 'grid';
    facetTitles = {
        category_multi: 'Category',
        make: 'Make',
        model: 'Model',
        country: 'Country',
        region: 'Region',
        state: 'State/Province',
        year: 'Year',
        zip: 'Postal Code'
    };
    mapType = 'search';
    geoDataParams = '';
    objMapClusterOpts = {
        styles: [{
            height: 32,
            width: 32,
            url: '/images/m1.png',
            shadow: true,
            opt_anchor: [16, 0],
            opt_textColor: '#FF00FF'
        }, {
            height: 39,
            width: 38,
            url: '/images/m3.png',
            opt_anchor: [24, 0],
            opt_textColor: '#FF0000'
        }, {
            height: 48,
            width: 43,
            url: '/images/m5.png',
            opt_anchor: [32, 0]
        }],
        maxZoom: 8,
        fitToMarkers: true
    };
    objSearchMap = {
        map: null,
        MarkerClusterer: null,
        infoWindow: null,
        markers: [],
        options: objMapClusterOpts
    };
    objItemMap = {};
    objGeoCoder = {};
    objQuery = {};
    pageConfig = {};
    strMapAsset = '';
    showROW = true;
    intTimeStamp = +new Date;
    $(".jsClickableFacets").on('click', 'li.jsCheckboxFilters ul li', function() {
        fncAddFilter($(this));
        fncBuildSolrQuery();
    });
    $('.e1FilterBar').on('click', '.e1FBCloseIcon', function() {
        fncRemoveFilter($(this));
        fncBuildSolrQuery();
        if ($(this).closest('li').data().filtername == 'zip') {
            $('.zip_code').val('');
            $('.postal-section').removeClass('active');
            $('.postal-section ul').removeClass('in');
        }
    });
    $('.e1TermList').on('click', '.e1FBCloseIcon', function() {
        objQuery.q = '*';
        fncBuildSolrQuery();
        if (!window.location.origin) {
            var targetUrl = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
        } else {
            var targetUrl = window.location.origin;
        }
        window.location.assign(targetUrl + browseAllLotsUrl);
    });
    $('.zip_search').click(function() {
        var postalCode = $(this).closest('.searchlist-heading').find('.zip_code');
        var range = $(this).closest('.searchlist-heading').find('.zip_range');
        fncZipSearch(postalCode);
    });
    $("select[name='sort']").change(function() {
        $("select[name='sort'] option").filter(function() {
            return $.trim($(this).text()) == "User Search"
        }).remove();
        fncAddSorting($(this).val());
        fncBuildSolrQuery();
    });
    $(".e1SaveSearch").click(function() {
        fncSaveSearch(objQuery, this);
    });
    $('.e1SearchPagination').on('click', 'a', function() {
        fncChangePage($(this).attr('data-startVal'));
        fncBuildSolrQuery();
        return false;
    });
    $('#e1ResultsList').on('change', '.jsCompare', function() {
        fncAddRemoveCompare($(this));
    });
    $('#e1ResultsList').on('click', '.e1AddWatchList', fncWatchListing);
    $('#e1CompareBar').on('click', '.e1FBCloseIcon', function() {
        var itemID = $(this).closest('.jsCompareRmv').attr('name');
        var selectedItem = $('.jsCompare[value="' + itemID + '"]');
        if (selectedItem.length == 0) {
            delete objCompareList[itemID];
            fncAddRemoveCompareImages();
        } else {
            $('.jsCompare[value="' + itemID + '"]').trigger('click');
        }
        if (jQuery.keys(objCompareList).length == 0) {
            $('.e1CompareButton').addClass('compare-disable');
            $('.e1CompareButton').removeClass('btn-primary');
        }
    });
    $('#e1CompareTemplate').on('click', '.jsCompareClose', fncCloseCompare)
    $('#e1CompareBar').on('click', '.e1CompareButton', function() {
        fncCompareItems();
    });
    $('#e1CompareBar').on('click', '.e1MapSelect', function() {
        $('.e1GridSelected').removeClass('e1GridSelected e1OrangeShadow').addClass('e1GridSelect e1GrayShadow');
        $('.e1MapSelect').removeClass('e1MapSelect e1GrayShadow').addClass('e1MapSelected e1OrangeShadow');
        $('#e1ResultsList').addClass('e1Hidden');
        $('.e1MapContainer').removeClass('e1Hidden');
        $('.e1ResultsFrom').addClass('e1Hidden');
        $('.e1SiteToggle').addClass('e1Hidden');
        mapType = 'search';
        if (typeof blnMapLoaded == "undefined") {
            fncLoadMapScript()
        } else {
            fncInitalizeMap();
        }
    });
    $('#e1CompareBar').on('click', '.e1GridSelect', function() {
        fncShowResultsGrid();
    });
    $(".searchform").css('border-radius', 0).addClass('e1GrayShadow');
    $('.e1CompareTable').on('click', '.e1CompareRemove', function() {
        fncCompareQuickRemove($(this));
    });
    $('a.sendEmail').click(function() {
        fncSendSearch(objQuery);
    });
    $('.scrollToResults').on('click', 'a', fncScrollToResults);
    $('#jsListingsPerPage select').on('change', function() {
        fncBuildSolrQuery();
    });
    if (E1_USERID == 'Eq1User') {
        userSpecificData = UserListings.fncInitUserListingData();
    }
    if (objLegacySavedQuery.length > 0) {
        objQuery = {};
        if (typeof(objEventQuery) == 'undefined') {
            objQuery.filters = {};
            objQuery.params = {};
        } else {
            objParsedStoredQuery = (!$.isEmptyObject(objStoredQuery) && typeof objStoredQuery != 'undefined') ? JSON.parse(objStoredQuery) : {};
            strStoredFilters = JSON.stringify(objParsedStoredQuery.filters);
            strEventFilters = JSON.stringify(objEventQuery.filters);
            if (strStoredFilters == strEventFilters) {
                objQuery.filters = objParsedStoredQuery.filters;
                objQuery.params = objParsedStoredQuery.params;
            } else {
                objQuery.filters = objEventQuery.filters;
                objQuery.params = objEventQuery.params;
            }
        }
        arrGetVars = location.hash.split('?');
        objSplitHash = arrGetVars[0].replace("%2526amp%3B", "%26amp;").replace('#', '').split('&');
        fncParseSavedQuery(objSplitHash);
        fncBuildSolrQuery();
    } else if ((typeof(objEncodedQuery) == 'undefined' || objEncodedQuery === false) && ($.isEmptyObject(objStoredQuery) && typeof(objStoredQuery) == 'undefined' || objStoredQuery === false) && (typeof(objEventQuery) == 'undefined' || objEventQuery === false)) {
        objQuery = {};
        objQuery.filters = {
            site_id: VENUE['solrSiteIdFilter']
        };
        objQuery.params = {};
        fncBuildSolrQuery();
    } else if (typeof(objEncodedQuery) != 'undefined' && objEncodedQuery !== false && typeof objQuery == 'undefined') {
        objQuery = JSON.parse(objEncodedQuery);
        fncBuildSolrQuery();
        fncParseQueryFilters(objQuery.filters);
    } else if (typeof(objEventQuery) != 'undefined') {
        if ($('#listbackSeachUrl').val() == 1) {
            sessionStorage.setItem('EQ1_query_pre', sessionStorage.getItem('EQ1_query'));
            if ((typeof sessionStorage.getItem('EQ1_query_pre') != 'undefined' && sessionStorage.getItem('EQ1_query_pre') != 'null')) {
                tmpParsedStoredQuery = (typeof sessionStorage.getItem('EQ1_query_pre') != 'undefined') ? JSON.parse(sessionStorage.getItem('EQ1_query_pre')) : {};
                objStoredQuery = JSON.stringify(tmpParsedStoredQuery);
                objEventQuery = tmpParsedStoredQuery;
            }
        }
        objParsedStoredQuery = (typeof objStoredQuery != 'undefined' && objStoredQuery != null && !$.isEmptyObject(objStoredQuery)) ? JSON.parse(objStoredQuery) : {};
        strStoredFilters = JSON.stringify(objParsedStoredQuery.filters);
        if ($('#listbackSeachUrl').val() == 1) {
            if (typeof objParsedStoredQuery.filters.category_multi != 'undefined' && objParsedStoredQuery.filters.category_multi.length > 0) {
                for (var i = 0; i < objParsedStoredQuery.filters.category_multi.length; i++) {
                    var filterlist = '<li data-filtervalue="' + objParsedStoredQuery.filters.category_multi[i] + '" data-filtername="category_multi" class="filter">' + objParsedStoredQuery.filters.category_multi[i] + '<span class="e1FBCloseIcon glyphicon glyphicon-remove glip-cancel"></span></li>';
                    $(".filterListClass").append(filterlist.replace(/%20/g, " ").replace(/%26/g, "&"));
                }
            }
            if (typeof objParsedStoredQuery.filters.make != 'undefined' && objParsedStoredQuery.filters.make.length > 0) {
                for (var i = 0; i < objParsedStoredQuery.filters.make.length; i++) {
                    var filterlistmake = '<li data-filtervalue="' + objParsedStoredQuery.filters.make[i] + '" data-filtername="make" class="filter">' + objParsedStoredQuery.filters.make[i] + '<span class="e1FBCloseIcon glyphicon glyphicon-remove glip-cancel"></span></li>';
                    $(".filterListClass").append(filterlistmake.replace(/%20/g, " ").replace(/%26/g, "&"));
                }
            }
            if (typeof objParsedStoredQuery.filters.model != 'undefined' && objParsedStoredQuery.filters.model.length > 0) {
                for (var i = 0; i < objParsedStoredQuery.filters.model.length; i++) {
                    var filterlistmodel = '<li data-filtervalue="' + objParsedStoredQuery.filters.model[i] + '" data-filtername="model" class="filter">' + objParsedStoredQuery.filters.model[i] + '<span class="e1FBCloseIcon glyphicon glyphicon-remove glip-cancel"></span></li>';
                    $(".filterListClass").append(filterlistmodel.replace(/%20/g, " ").replace(/%26/g, "&"));
                }
            }
            if (typeof objParsedStoredQuery.filters.country != 'undefined' && objParsedStoredQuery.filters.country.length > 0) {
                for (var i = 0; i < objParsedStoredQuery.filters.country.length; i++) {
                    var filterlistcountry = '<li data-filtervalue="' + objParsedStoredQuery.filters.country[i] + '" data-filtername="country" class="filter">' + objParsedStoredQuery.filters.country[i] + '<span class="e1FBCloseIcon glyphicon glyphicon-remove glip-cancel"></span></li>';
                    $(".filterListClass").append(filterlistcountry.replace(/%20/g, " ").replace(/%26/g, "&"));
                }
            }
            if (objParsedStoredQuery.filters.year != null) {
                var filterlistyr = '<li data-filtervalue="' + objParsedStoredQuery.filters.year + '" data-filtername="year" class="filter">' + objParsedStoredQuery.filters.year + '<span class="e1FBCloseIcon glyphicon glyphicon-remove glip-cancel"></span></li>';
                $(".filterListClass").append(filterlistyr.replace(/%20/g, " ").replace(/%26/g, "&"));
            }
            if (typeof objParsedStoredQuery.filters.region != 'undefined' && objParsedStoredQuery.filters.region.length > 0) {
                for (var i = 0; i < objParsedStoredQuery.filters.region.length; i++) {
                    var filterlistregion = '<li data-filtervalue="' + objParsedStoredQuery.filters.region[i] + '" data-filtername="region" class="filter">' + objParsedStoredQuery.filters.region[i] + '<span class="e1FBCloseIcon glyphicon glyphicon-remove glip-cancel"></span></li>';
                    $(".filterListClass").append(filterlistregion.replace(/%20/g, " ").replace(/%26/g, "&"));
                }
            }
            if (typeof objParsedStoredQuery.filters.state != 'undefined' && objParsedStoredQuery.filters.state.length > 0) {
                for (var i = 0; i < objParsedStoredQuery.filters.state.length; i++) {
                    var filterliststate = '<li data-filtervalue="' + objParsedStoredQuery.filters.state[i] + '" data-filtername="state" class="filter">' + objParsedStoredQuery.filters.state[i] + '<span class="e1FBCloseIcon glyphicon glyphicon-remove glip-cancel"></span></li>';
                    $(".filterListClass").append(filterliststate.replace(/%20/g, " ").replace(/%26/g, "&"));
                }
            }
            if (objParsedStoredQuery.filters.geofilt != null) {
                var filterlistgeofilt = '<li data-filtervalue="' + objParsedStoredQuery.filters.geofilt + '" data-filtername="zip" class="filter">' + objParsedStoredQuery.filters.geofilt + '<span class="e1FBCloseIcon glyphicon glyphicon-remove glip-cancel"></span></li>';
                $(".filterListClass").append(filterlistgeofilt.replace(/%20/g, " ").replace(/%26/g, "&"));
            }
        }
        if (typeof(marketingLocalFilters) != 'undefined' && marketingLocalFilters == true && typeof(marketingLocalFilterArray) != 'undefined') {
            if (!E1_USERID) {
                $("#jsRegisterFree").removeClass('e1Hidden');
            }
            var validFiltersKey = $.keys(facetTitles);
            for (var i = 0; i < validFiltersKey.length; i++) {
                filterValue = marketingLocalFilterArray[validFiltersKey[i]];
                if (typeof(filterValue) != 'undefined' && filterValue != false) {
                    objEventQuery.filters[validFiltersKey[i]] = [filterValue];
                    fncFilterbarAddRemove(validFiltersKey[i], filterValue, filterValue, 'add');
                }
            }
        } else if (typeof(marketingLocalFilters) != 'undefined' && marketingLocalFilters == true) {
            var validFiltersKey = $.keys(facetTitles);
            for (var i = 0; i < validFiltersKey.length; i++) {
                filterValue = fnGetURLParameter(validFiltersKey[i]);
                if (filterValue != false) {
                    filterValueArr = filterValue.toString().split(',');
                    objEventQuery.filters[validFiltersKey[i]] = filterValueArr;
                    fncFilterbarAddRemove(validFiltersKey[i], filterValue, filterValue, 'add');
                }
            }
        }
        strEventFilters = JSON.stringify(objEventQuery.filters);
        qTerm = JSON.stringify(objEventQuery.qterm);
        if (strStoredFilters == strEventFilters && qTerm == objParsedStoredQuery.q) {
            objQuery = objParsedStoredQuery;
        } else {
            objQuery = objEventQuery;
        }
        if (typeof objQuery.filters.seller_id != 'undefined' || typeof objQuery.filters.tags != 'undefined' || typeof objQuery.filters.seller_company_id != 'undefined') {
            showROW = false;
        } else {
            showROW = true;
        }
        fncBuildSolrQuery();
    } else {
        objParsedStoredQuery = $.isEmptyObject(objStoredQuery) ? {} : JSON.parse(objStoredQuery);
        if (keyword != false && keyword != objParsedStoredQuery.q) {
            objQuery = {};
            objQuery.filters = {
                site_id: VENUE['solrSiteIdFilter']
            };
            objQuery.params = {};
        } else {
            objQuery = objParsedStoredQuery;
        }
        fncBuildSolrQuery();
        fncParseQueryFilters(objQuery.filters);
    }
    var itemsString = $('#items').html();
    $('#dvSecondaryContainer').removeClass('container_secondary');
    $('#dvSecondaryContainer').addClass('search_page_wraper');
    $('.search_name').on('keyup', function() {
        var $this = $(this);
        if ($this.val() != '') {
            $('.jsNameSearch button').removeClass('compare-disable');
            $('.jsNameSearch button').addClass('btn-warning');
            $('.e1SaveSearch').prop("disabled", false);
        } else {
            $('.jsNameSearch button').addClass('compare-disable');
            $('.jsNameSearch button').removeClass('btn-warning');
        }
    });
}

function fncConfigureListing(page) {
    listingConfig.displayListingId = true;
    listingConfig.displayBuyersConsole = true;
    listingConfig.displayTimeleft = true;
    listingConfig.displayRank = true;
    listingConfig.displayLocation = true;
    listingConfig.displayDefiniteSale = true;
    listingConfig.displaySellerLogo = true;
    listingConfig.displayRelativeImage = true;
    listingConfig.displayListingLinks = true;
    listingConfig.displayCompare = true;
    listingConfig.displayDefiniteSale = true;
    listingConfig.dispalyLeadOffer = true;
    listingConfig.displayAuctionNumber = true;
}

function fncLoadImagesCarousel() {
    $('.jcarousel').on('jcarousel:create jcarousel:reload', function() {
        var element = $(this),
            width = element.innerWidth();
        if (width >= 900) {
            width = width / 4;
            $('.jcarousel-control-prev').jcarouselControl({
                target: '-=3'
            });
            $('.jcarousel-control-next').jcarouselControl({
                target: '+=3'
            });
        } else if (width >= 600) {
            width = width / 3;
            $('.jcarousel-control-next').jcarouselControl({
                target: '+=3'
            });
            $('.jcarousel-control-next').jcarouselControl({
                target: '-=3'
            });
        } else if (width >= 350) {
            width = width / 2;
            $('.jcarousel-control-next').jcarouselControl({
                target: '+=2'
            });
            $('.jcarousel-control-next').jcarouselControl({
                target: '-=2'
            });
        } else {
            $('.jcarousel-control-prev').jcarouselControl({
                target: '-=1'
            });
            $('.jcarousel-control-next').jcarouselControl({
                target: '+=1'
            });
        }
        element.jcarousel('items').css('width', width + 'px');
    }).jcarousel({
        wrap: 'circular'
    });
    $('.jcarousel-pagination').on('jcarouselpagination:active', 'a', function() {
        $(this).addClass('active');
    }).on('jcarouselpagination:inactive', 'a', function() {
        $(this).removeClass('active');
    }).on('click', function(e) {
        e.preventDefault();
    }).jcarouselPagination({
        item: function(page) {
            return '<a href="#' + page + '">' + page + '</a>';
        }
    });
}

function fncSearcOnLoad() {
    if (E1_USERID != "") {
        fncMyoneSearchSummary(true, true);
    }
    fncLoadImagesCarousel();
    var $searchResult = $('#e1SearchResultsContainer');
    if (isEvent == true && $searchResult.find('#collapseread').length >= 1) {
        fncSetMarketingContentDisplay();
    }
    var $categoryContent = $('.e1ArticleContent');
    if ($categoryContent.find('#collapseExample').length >= 1) {
        fncSetCategoryContentDisplay();
    }
}
$(window).on('load', function() {
    fncSearcOnLoad();
});
$(window).on('beforeunload', function() {
    sessionStorage.setItem('scrollPos', $(document).scrollTop());
});

function fncUpdateData(itemIDs, returnData) {
    if (typeof itemIDs == 'undefined') {
        return;
    }
    returnData = (typeof(returnData) == 'undefined') ? false : true;
    asyncCall = (typeof(returnData) == 'undefined' || returnData == false) ? true : false;
    itemString = itemIDs.join(',');
    intTimeStamp = +new Date;
    fncInitListingConfig();
    listingConfig.displayListingId = true;
    listingConfig.displayBuyersConsole = true;
    listingConfig.displayTimeleft = true;
    listingConfig.displayRank = true;
    listingConfig.displayLocation = true;
    listingConfig.displayDefiniteSale = true;
    listingConfig.displaySellerLogo = true;
    listingConfig.displayRelativeImage = true;
    listingConfig.displayListingLinks = true;
    listingConfig.displayCompare = true;
    listingConfig.displayDefiniteSale = true;
    listingConfig.dispalyLeadOffer = true;
    listingConfig.displayAuctionNumber = true;
    $.ajax({
        url: "/update.php?ids=" + itemString,
        dataType: "json",
        timeout: 12000,
        async: asyncCall,
        itemIDs: itemIDs,
        success: function(data) {
            delete data.STATUS;
            if (returnData) {
                priceDataUpdate = {
                    currentOffer: parseInt(data.LEADOFFERPRICE),
                    nextOffer: parseInt(data.NEXTOFFERAMOUNT)
                };
            } else {
                if (this.itemIDs.length > 1) {
                    $.each(data, function(index, data) {
                        fncUpdateListing(data);
                    });
                    fncGetActiveBidding();
                } else {
                    fncUpdateListing(data);
                    fncGetActiveBidding();
                }
            }
        }
    });
}

function fncShowHideFilters(filterList) {
    if ($(filterList).find('.renderedFacet').hasClass('active')) {
        $(filterList).find('ul').removeClass('in');
        $(filterList).removeClass('active');
    } else {
        $(filterList).find('ul').addClass('in');
        $(filterList).addClass('active');
    }
    var facetName = filterList.closest('.e1FilterBar').find('li').attr('data-filtername');
    if (typeof objQuery.filters[facetName] != 'undefined' && $.keys(objQuery.filters[facetName]).length > 0) {
        var facetValue = decodeURIComponent(objQuery.filters[facetName][$.keys(objQuery.filters[facetName]).length - 1]);
        var facetOffset = filterList.closest('.e1FacetOuter').find('.jsClickableFacets').find('li[data-filtervalue="' + facetValue + '"]').position().top - 80;
        filterList.closest('.e1FacetOuter').find('.jsClickableFacets').scrollTop(filterList.closest('.e1FacetOuter').find('.jsClickableFacets').scrollTop() + facetOffset);
    }
}

function fncAddRemoveCompare(listingItemChkBox) {
    cmpListingItem = listingItemChkBox.closest('.e1ListingItem');
    var itemID = listingItemChkBox.val();
    $('.jsCompareWarning').remove();
    if (jQuery.keys(objCompareList).length >= 4 && listingItemChkBox.prop('checked') === true) {
        $("#jsmodelerrorMessage").html('You can only compare 4 lots at a time.');
        $("#modelerrorMessage").modal('show');
        listingItemChkBox.prop('checked', false);
    } else {
        if (listingItemChkBox.prop('checked') === true) {
            if (jQuery.keys(objCompareList).length >= 1) {
                $('.e1CompareButton').removeClass('compare-disable');
                $('.e1CompareButton').addClass('btn-primary');
            }
            cmpListingItem.find('.jsListingImage').addClass('e1ListingCompared');
            cmpListingItem.find('.jsCompareContainer').addClass('e1comparedChecked');
            objCompareList[itemID] = cmpListingItem.find('.jsListingImage').attr('src');
        } else {
            if (jQuery.keys(objCompareList).length <= 2) {
                $('.e1CompareButton').addClass('compare-disable');
                $('.e1CompareButton').removeClass('btn-primary');
            }
            cmpListingItem.find('.jsListingImage').removeClass('e1ListingCompared');
            cmpListingItem.find('.jsCompareContainer').removeClass('e1comparedChecked');
            delete objCompareList[itemID];
        }
    }
    fncAddRemoveCompareImages();
}

function fncAddRemoveCompareImages() {
    var intIterator = 0;
    $(".e1CompareItem").each(function() {
        $(this).html('');
    });
    if (objCompareList.length != 0) {
        $.each(objCompareList, function(index, thumbSource) {
            $(".e1CompareItem").eq(intIterator).html('<a class="jsCompareRmv" name="' + index + '"><img onerror="ANUtils.defaultImage(this);" src="' + thumbSource + '" class="compareImage" /><span class="e1FBCloseIcon glyphicon glyphicon-remove glip-cancel"></span></a>');
            intIterator++;
        });
    } else {
        $(".e1CompareItem").each(function() {
            $(this).html('');
        });
    }
    if (intIterator > 1) {
        $('.e1CompareButton').prop("disabled", false);
    } else {
        $('.e1CompareButton').prop("disabled", true);
    }
}

function fncCompareItems() {
    if (jQuery.keys(objCompareList).length < 2) {
        $("#jsmodelerrorMessage").html('You must select at least two lots to compare.');
        $("#modelerrorMessage").modal('show');
    } else {
        var queryURL = solrURL + solrComparePath;
        var e1IDs = [];
        var otherIDs = [];
        $.each(objCompareList, function(itemID, thumbSource) {
            if (itemID.indexOf('E1@') != -1) {
                e1IDs.push(itemID.replace('E1@', ''));
            } else {
                otherIDs.push(itemID);
            }
        });
        var queryArr = [];
        if (e1IDs.length > 0) {
            queryArr.push('lot_number:("' + e1IDs.join('" OR "') + '")');
        }
        if (otherIDs.length > 0) {
            queryArr.push('asset_id:("' + otherIDs.join('" OR "') + '")');
        }
        var searchQuery = 'fq=' + queryArr.join(' OR ');
        $.ajax({
            'url': queryURL,
            'data': searchQuery,
            'success': function(data) {
                fncDisplayCompare(data.response.docs)
            },
            'error': function() {
                $("#jsmodelerrorMessage").html('Unable to load listings for compare. Please try again later.');
                $("#modelerrorMessage").modal('show');
            },
            'dataType': 'jsonp',
            'jsonp': 'json.wrf',
            'processData': false
        });
    }
}

function fncDisplayCompare(docs) {
    var compareTable = $('#e1CompareTemplate').clone(true);
    compareTable.removeClass('e1Hidden');
    compareTable.removeClass('jsTemplate');
    compareTable.attr('id', 'jsCompareContent')
    var colWidth = 80 / docs.length;
    $.each(docs, function(index, doc) {
        var columnClass = 'jsCompareCol-' + index;
        var colGroup = $('<colgroup width="' + colWidth + '%;" class="dataColumn ' + columnClass + '"></colgroup>');
        colGroup.prependTo(compareTable.find('.e1CompareTable'));
        var objEventDate = new Date(doc.AUCTIONENDTIME);
        var dateText = (typeof(doc.AUCTIONENDTIME) != 'undefined') ? objMonths[objEventDate.getMonth()] + ' ' + objEventDate.getDate() + ', ' + objEventDate.getFullYear() : '&mdash;';
        var marketPlace = VENUE['allMarketPlaces'];
        var siteLogo = '';
        for (var i = 0; i < (marketPlace).length; i++) {
            marketPlace[i] = marketPlace[i].replace(" ", '-');
            var docMarketplace = (doc.MARKETPLACE).replace(" ", '-');
            if (docMarketplace == marketPlace[i]) {
                siteLogo = "<div class='refinevalue filter" + marketPlace[i] + "'></div>";
                var listingID = doc.LOTNUMBER;
                var compareKey = doc.LOTNUMBER;
                $('.e1AddWatchList').attr('data-AssetID', doc.LOTNUMBER);
                if (marketPlace[i] == "RBA") {
                    var listingID = doc.LISTINGID;
                    var compareKey = doc.LISTINGID;
                    $('.e1AddWatchList').attr('data-AssetID', doc.LISTINGID);
                }
                if (marketPlace[i] == "SS") {
                    var listingID = doc.LISTINGID;
                    var compareKey = doc.LISTINGID;
                    $('.e1AddWatchList').attr('data-AssetID', doc.LISTINGID);
                }
                var showWatchIcon = '';
            }
        }
        if (typeof(doc.thumbnail) == 'undefined') {
            doc['thumbnail'] = 'https://eq1-assetnation.netdna-ssl.com/images/na.png'
        }
        var imageUrl = fncGetImageURL(doc);
        var conditionText = (typeof(doc.CONDITION) != 'undefined') ? doc.CONDITION : '&mdash;';
        var locationText = (typeof(doc.LOCATION) != 'undefined') ? doc.LOCATION : '&mdash;';
        var makeText = (typeof(doc.MAKE) != 'undefined') ? doc.MAKE : '&mdash;';
        var modelText = (typeof(doc.MODEL) != 'undefined') ? doc.MODEL : '&mdash;';
        var yearText = (typeof(doc.YEAR) != 'undefined') ? doc.YEAR : '&mdash;';
        var mileageText = (typeof(doc.MILEAGE) != 'undefined') ? doc.MILEAGE : '&mdash;';
        var descriptionText = (typeof(doc.DESCRIPTION) != 'undefined' && doc.DESCRIPTION != '<p></p>') ? fncRemoveIframe(doc.DESCRIPTION) : '&mdash;';
        var listingIDText = (typeof(listingID) != 'undefined') ? listingID : '&mdash;';
        compareTable.find('.jsCompareThumb').append('<td class="' + columnClass + '"><span class="e1CompareImg"><span class="e1ListingThumb ' + columnClass + '"></span><a class="e1CompareRemove" data-compareKey="' + doc.LISTINGID + '"> <span class="e1FBCloseIcon glyphicon glyphicon-remove glip-cancel ' + columnClass + '"></span></a></span><div class="e1ComareTitle">' + doc.TITLE + '</div>' + showWatchIcon + '</td>');
        compareTable.find('.e1ListingThumb.' + columnClass).css('background-image', 'url(' + imageUrl + ')');
        compareTable.find('.jsCompareSite').append('<td class="' + columnClass + '">' + siteLogo + '</td>');
        compareTable.find('.jsCompareID').append('<td class="' + columnClass + '">' + listingID + '</td>');
        compareTable.find('.jsCompareDate').append('<td class="' + columnClass + '">' + dateText + '</td>');
        compareTable.find('.jsCompareCondition').append('<td class="' + columnClass + '">' + conditionText + '</td>');
        compareTable.find('.jsCompareLocation').append('<td class="' + columnClass + '">' + locationText + '</td>');
        compareTable.find('.jsCompareMake').append('<td class="' + columnClass + '">' + makeText + '</td>');
        compareTable.find('.jsCompareModel').append('<td class="' + columnClass + '">' + modelText + '</td>');
        compareTable.find('.jsCompareYear').append('<td class="' + columnClass + '">' + yearText + '</td>');
        compareTable.find('.jsCompareMileage').append('<td class="' + columnClass + '">' + mileageText + '</td>');
        compareTable.find('.jsCompareDescription').append('<td class="' + columnClass + '">' + descriptionText + '</td>');
    });
    var colGroup = $('<colgroup width="15%;" class="e1CompareHeader"></colgroup>');
    colGroup.prependTo(compareTable.find('.e1CompareTable'));
    $('.t3-mainbody').append(compareTable);
    $('.e1CompareContainer').on('click', '.e1AddWatchList', fncWatchListing);
    $('.e1CompareContainer').removeClass('e1Hidden');
    $('#e1SearchResultsContainer').hide();
    $('.t3-sidebar').hide();
    $('.t3-content').hide();
    $('.e1OffCanvasContainer').addClass('hidden-xs hidden-sm');
    $('.e1OffCanvasContainer').hide();
}

function fncCompareQuickRemove(item) {
    var itemID = item.attr('data-comparekey');
    var columnID = item.closest('td').attr('class');
    if (jQuery.keys(objCompareList).length > 2) {
        $('.e1CompareTable').find('.' + columnID).remove();
        $('.e1CompareTable').find('.' + columnID).remove();
        fncAddRemoveCompareImages();
        var selectedItem = $('.jsCompare[value="' + itemID + '"]');
        if (selectedItem.length == 0) {
            delete objCompareList[itemID];
            fncAddRemoveCompareImages();
        } else {
            $('.jsCompare[value="' + itemID + '"]').trigger('click');
        }
    } else {
        $('#jsCompareContent').find('.e1CompareError').find('span').html('<div class="errormsg-global"><i class="fa fa-exclamation-circle"></i> <span class="error-txt">Minimum of two lots are required for compare.</span></div>')
    }
    var colWidth = 80 / $('.dataColumn').length;
    $('.dataColumn').width(colWidth + '%');
}

function fncCloseCompare() {
    $('#jsCompareContent').remove();
    $('.e1CompareContainer').addClass('e1Hidden');
    $('#e1SearchResultsContainer').show();
    $('.t3-sidebar').show();
    $('.t3-content').show();
    $('#e1SearchNav').show();
    $('.e1OffCanvasContainer').removeClass('hidden-xs hidden-sm');
    $('.e1OffCanvasContainer').show();
}

function fncAddFilter(filterItem) {
    $('body,html').scrollTop($('#e1CompareList').offset().top);
    var filterName = $(filterItem).attr('data-filterName');
    var filterValue = $(filterItem).attr('data-filterValue');
    var filterDisplay = $(filterItem).attr('data-filterDisplay');
    if (typeof(objQuery.params.start) != 'undefined') {
        delete objQuery.params.start;
    }
    if ($(filterItem).hasClass('jsFilterSelected')) {
        if (filterName == 'site_id') {
            objSiteSelected[filterValue] = false;
            blnFiltersModified = true;
        }
        fncRemoveFilter(filterItem);
    } else {
        if (filterName == 'site_id') {
            objSiteSelected[filterValue] = true;
            blnFiltersModified = true;
        }
        if (filterName == 'year') {
            if (typeof(objQuery.filters[filterName]) == 'undefined') {
                objQuery.filters[filterName] = [];
            }
            if ($.inArray(filterValue, objQuery.filters[filterName]) == -1) {
                objQuery.filters[filterName][0] = encodeURIComponent(filterValue);
            }
        } else if (filterName != 'zip') {
            if (typeof(objQuery.filters[filterName]) == 'undefined') {
                objQuery.filters[filterName] = [];
            }
            if ($.inArray(filterValue, objQuery.filters[filterName]) == -1) {
                objQuery.filters[filterName].push(encodeURIComponent(filterValue));
            }
        } else if (filterName == 'zip') {
            objQuery.filters['geofilt'] = filterValue;
        }
        if (filterName != 'year') {
            $(filterItem).addClass('jsFilterSelected');
            $(filterItem).find('input:checkbox').prop('checked', true);
        }
        fncFilterbarAddRemove(filterName, filterValue, filterDisplay, 'add');
    }
}

function fncRemoveFilter(filterItem) {
    if ($(filterItem).is("span")) {
        filterBarItem = $(filterItem).closest('li.filter');
        var filterName = $(filterBarItem).attr('data-filterName');
        var filterValue = $(filterBarItem).attr('data-filterValue');
        var resetYear = true;
    } else {
        var filterName = $(filterItem).attr('data-filterName');
        var filterValue = $(filterItem).attr('data-filterValue');
    }
    if (filterName == 'zip') {
        filterItem = $('input[name="zip_code"]');
        delete objQuery.params.pt;
        delete objQuery.params.d;
        delete objQuery.params.sfield;
        delete objQuery.filters.geofilt;
    } else {
        if (filterName == 'year' && resetYear) {
            fncResetYearSlider();
        } else {
            filterItem = $(" li[data-filterName='" + filterName + "'][data-filterValue='" + filterValue + "']");
            $(filterItem).find('input:checkbox').prop('checked', false);
        }
        if ($.inArray(encodeURIComponent(filterValue), objQuery.filters[filterName]) != -1) {
            objQuery.filters[filterName].splice($.inArray(encodeURIComponent(filterValue), objQuery.filters[filterName]), 1);
        }
        if (typeof(objQuery.filters[filterName]) != 'undefined' && objQuery.filters[filterName].length <= 0) {
            delete objQuery.filters[filterName];
        }
    }
    $(filterItem).removeClass('jsFilterSelected');
    fncFilterbarAddRemove(filterName, filterValue, null, 'remove');
}

function fncChangePage(intStart) {
    objQuery.params.start = intStart;
}

function fncResetYearSlider() {
    sliderOptions = $(".e1YearSlider").slider('option');
    $(".e1YearText").html(sliderOptions.min + " - " + sliderOptions.max);
    $('div.e1FacetOuter[id="Year"]').find('.jsClickableFacets').hide();
}

function fncFilterbarAddRemove(filterName, filterValue, filterDisplay, action) {
    var validFilters = $.keys(facetTitles);
    if (filterName == "site_id" || $.inArray(filterName, validFilters) == -1) {
        return false;
    }
    var filterList = $('.e1FilterBar').find('ul.e1FilterList');
    if (action == 'add') {
        if (filterName == 'year') {
            filterBarText = filterValue.replace('TO', '-');
            $('.e1YearSlider .ui-slider-range').addClass('ui-widget-header');
            $('#slideYearStatus').val(1);
        } else {
            filterBarText = filterValue;
        }
        var filterBarItem = $('<li class="filter" data-filterName="' + filterName + '" data-filterValue="' + filterValue + '"></li>');
        filterBarItem.html(decodeURIComponent(filterBarText) + '<span class="e1FBCloseIcon glyphicon glyphicon-remove glip-cancel"></span>');
        if (filterName == 'year' && $(filterList).find('[data-FilterName="' + filterName + '"]').size() > 0) {
            $(filterList).find('[data-FilterName="' + filterName + '"]').replaceWith(filterBarItem);
        } else {
            $(filterList).append(filterBarItem);
        }
    } else if (action == 'remove') {
        if (filterName == 'year') {
            $('.e1YearSlider .ui-slider-range').removeClass('ui-widget-header');
            $('#slideYearStatus').val(0);
        }
        if (filterName == 'zip') {
            var filterBarItem = $("ul.e1FilterList li[data-filterName='zip']");
        } else {
            var filterBarItem = $("ul.e1FilterList li[data-filterName='" + filterName + "'][data-filterValue='" + filterValue + "']");
        }
        $(filterBarItem).remove();
    }
    if (objQuery.lenght < 0) {
        objQuery.params.start = 0;
    }
}

function yearSlider(minYear, maxYear) {
    if (typeof objQuery.filters.year != 'undefined') {
        yearArray = objQuery.filters.year[0].split('%20TO%20');
        minYear = yearArray[0];
        maxYear = yearArray[1];
    }
    if (isNaN(minYear) || isNaN(maxYear) || (minYear == '0' && maxYear == '0')) {
        $('div.e1FacetOuter[id="Year"]').find('.jsClickableFacets').hide();
    } else {
        if ($.inArray('year', jQuery.keys(objQuery.filters)) != -1) {
            $('div.e1FacetOuter[id="Year"]').find('.jsClickableFacets').show();
        }
        minYear = isNaN(minYear) ? 1950 : minYear;
        maxYear = isNaN(maxYear) ? curDate.getFullYear() : maxYear;
        var minYear1 = minYear;
        var maxYear1 = maxYear;
        $(function() {
            $(".e1YearSlider").slider({
                range: true,
                min: 1950,
                max: curDate.getFullYear(),
                values: [minYear, maxYear],
                slide: function(event, objSlider) {
                    minYear = objSlider.values[0];
                    maxYear = objSlider.values[1];
                    $(".e1YearText").html(objSlider.values[0] + " - " + objSlider.values[1]);
                    $('.e1YearSlider .ui-slider-range').addClass('ui-widget-header');
                    $('#slideYearStatus').val(1);
                },
                stop: function() {
                    fncRemoveFilter($(".e1YearText"));
                    $(".e1YearText").attr("data-filterName", "year");
                    $(".e1YearText").attr("data-filterValue", minYear + " TO " + maxYear);
                    fncAddFilter($(".e1YearText"));
                    fncBuildSolrQuery();
                    $(".e1YearText").html($(".e1YearSlider").slider("values", 0) + " - " + $(".e1YearSlider").slider("values", 1));
                }
            });
            if ($('#slideYearStatus').val() == '1') {
                $('.e1YearSlider .ui-slider-range').addClass('ui-widget-header');
            } else {
                $('.e1YearSlider .ui-slider-range').removeClass('ui-widget-header');
            }
            $(".e1YearText").html($(".e1YearSlider").slider("values", 0) + " - " + $(".e1YearSlider").slider("values", 1));
        });
    }
}

function fncZipSearch(postalCode) {
    if (postalCode.val() == '') {
        return false;
    }
    fncGetGeoData();
}

function fncGetGeoData() {
    mapType = 'geoCoder';
    if (typeof blnMapLoaded == "undefined") {
        fncLoadMapScript();
    } else {
        fncInitalizeMap();
    }
}

function fncGeoCodeAddress(postalCode, range) {
    objGeoCoder.geocode({
        'address': $(postalCode).val()
    }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            fncSetGeoFilter(results, postalCode, range);
        } else {
            return false;
        }
    });
}

function fncReverseGeoData(geoPoint) {
    mapType = 'reverseGeoCoder';
    strGeoPoint = geoPoint;
    if (typeof blnMapLoaded == "undefined") {
        fncLoadMapScript()
    } else {
        fncInitalizeMap();
    }
}

function fncGetZip() {
    objGeoCoder.geocode({
        'latlng': strGeoPoint
    }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            fncParseGeoData(results);
        } else {
            return false;
        }
    });
}

function fncSetGeoFilter(geoData, postalCode, range) {
    var latitude = geoData[0].geometry.location.lat();
    var longitude = geoData[0].geometry.location.lng();
    var distanceRadius = $(range).val() * 1.60934;
    $('input[name="zip_code"]').attr("data-filterName", "zip");
    $('input[name="zip_code"]').attr("data-filterValue", "Within " + $(range).val() + " miles of " + geoData[0].address_components[0]['long_name']);
    fncRemoveFilter($('input[name="zip_code"]'));
    objQuery.params.pt = latitude + ',' + longitude;
    objQuery.params.d = distanceRadius;
    objQuery.params.sfield = 'geo_loc';
    fncAddFilter($(postalCode));
    fncBuildSolrQuery();
}

function fncParseGeoData(geoData) {
    $.each(geoData[0].address_components, function(index, geoValue) {
        if (geoValue.types[0] == 'postal_code') {
            fncSetPostalCode(geoValue.long_name);
        }
    });
}

function fncSetPostalCode(postalCode) {
    $('input[name="zip_code"]').val(postalCode);
    $('#Zip').show()
    fncZipSearch(postalCode);
}

function fncReverseGeoFilter(paramName, paramValue) {
    if (paramName == 'pt') {
        fncReverseGeoData(paramValue);
    } else if (paramName == 'd') {
        var range = paramValue / 1.60934;
        $('#zip_range').val(range);
    }
}

function fncAddSorting(sortCriteria) {
    objSortParams = JSON.parse(sortCriteria);
    tmpSort = []
    $.each(objSortParams, function(sortField, sortDirection) {
        tmpSort.push(sortField + ' ' + sortDirection);
    });
    objQuery.params.sort = tmpSort.join(',');
}

function fncBuildSolrQuery() {
    var kws = false;
    if ($.find('.e1ArticleContent')) {
        arrCateogories = $(location).attr('pathname').substr(1).split('/');
        send2Monetate.addCategory(arrCateogories);
    }
    if (typeof objQuery.q == 'undefined' && fnGetURLParameter('kw') != false) {
        var q = fnGetURLParameter('kw');
        var kws = true;
    } else if (typeof objQuery.q == 'undefined' && fnGetURLParameter('q') != false) {
        var q = fnGetURLParameter('q');
    } else if (typeof objQuery.qterm == 'undefined' && fnGetURLParameter('qterm') != false) {
        var q = fnGetURLParameter('qterm');
    } else {
        var q = objQuery.q;
    }
    isSellerId = /^(\(seller_id:)/i.test(q);
    if (isSellerId) {
        var splitQ = q.split(':');
        var seller_id = splitQ[1].replace(/[{()}]/g, '');
        seller_id = ($.isNumeric(seller_id) ? seller_id : $.base64.atob(seller_id));
        q = "(seller_id:" + seller_id + ")";
    }
    var searchQuery = '';
    serverTime = '';
    fncGetSolrTime();
    var auctionEndDate = '[NOW TO *]';
    switch (querySite) {
        case "e1ss":
            queryURL = solrURL + solrQueryPath;
            break;
        case "rowss":
            queryURL = solrURL + solrROWPath;
            break;
        default:
            queryURL = solrURL + solrQueryPath;
            break;
    }
    if (typeof(q) == 'undefined' || q == "") {
        q = "*";
    }
    if ((querySite == venuekey && showROW === false) || displayType == 'map') {
        $('.e1ResultsFrom').hide();
        $('.e1SiteToggle').hide();
    } else {
        $('.e1ResultsFrom').show();
        $('.e1SiteToggle').show();
    }
    searchQuery += 'wt=json&q=' + q;
    objQuery.q = q;
    if ($(objQuery.filters).length > 0) {
        $.each(objQuery.filters, function(filterName, filterArray) {
            filterTag = filterName.substr(0, 2);
            if (filterName == 'geofilt') {
                searchQuery += "&fq={!geofilt}";
            } else if (filterName == 'year') {
                searchQuery += "&fq=" + filterName + ":" + '[' + filterArray.join('" OR "') + ']';
            } else if (filterName == 'auction_end_date' || filterName == 'auction_start_date') {
                searchQuery += "&fq=" + filterName + ":" + encodeURIComponent(filterArray.join('" OR "'));
            } else {
                searchQuery += "&fq={!tag=" + filterTag + "}" + filterName + ":" + '("' + filterArray.join('" OR "') + '")';
            }
        });
        if (querySite == venuekey && isEvent === false && searchQuery.indexOf('auction_end_date') == -1) {
            searchQuery += "&fq=" + "auction_end_date:" + '(' + auctionEndDate + ')';
        }
    } else {
        if (querySite == venuekey && isEvent === false && searchQuery.indexOf('auction_end_date') == -1) {
            searchQuery += "&fq=" + "auction_end_date:" + '(' + auctionEndDate + ')';
        }
    }
    resultsPerPage = $('#jsListingsPerPage select').val();
    if ($.keys(objQuery.params).length > 0) {
        var totalLotcount = 0,
            pageLotcount = 0;
        if ($(".e1NumFound").html() != '' && $("#e1ResultsFound").html() != '') {
            var LotCount = $(".e1NumFound").html().replace(",", "");
            var EventCount = $("#e1ResultsFound").html().replace(",", "");
            var AuctionsCount = $("#e1AuctionsFound").html().replace(",", "");
            totalLotcount = parseInt(LotCount) + parseInt(EventCount) + parseInt(AuctionsCount);
        }
        $.each(objQuery.params, function(paramName, paramValue) {
            if (totalLotcount != 0)
                pageLotcount = parseInt(paramValue) + parseInt(resultsPerPage);
            if (totalLotcount < pageLotcount && paramName != "NOW") {
                var pageCount = Math.ceil(totalLotcount / resultsPerPage),
                    paramValuecal = (pageCount - 1) * resultsPerPage;
                if (paramName == "pt" || paramName == "d")
                    searchQuery += "&" + paramName + "=" + paramValue;
                else
                    searchQuery += "&" + paramName + "=" + paramValuecal;
            } else {
                searchQuery += "&" + paramName + "=" + paramValue;
            }
        });
    } else {
        if (kws == false) {
            searchQuery += "&sort=site_sort asc,auction_end_date asc,high_bid_amount desc";
            objQuery.params.sort = "site_sort asc,auction_end_date asc,high_bid_amount desc";
        } else {
            searchQuery += "&sort=site_sort asc, score desc";
            objQuery.params.sort = "site_sort asc, score desc";
        }
    }
    if (typeof resultsPerPage != 'undefined') {
        searchQuery += "&rows=" + resultsPerPage;
    }
    sessionStorage.setItem('EQ1_query', JSON.stringify(objQuery));
    sessionStorage.setItem('sourceSite', querySite);
    sessionStorage.setItem('EQ1_search_query1', searchQuery);
    
    alert(searchQuery);
    $.ajax({
        'url': queryURL,
        'data': searchQuery,
        'success': function(data) {
            arrUpdateIDs = [];
            objPriceData = {};
            var searchData = {
                response: data.response,
                facets: data.facet_counts.facet_fields,
                stats: (typeof data.stats != "undefined") ? data.stats.stats_fields : "",
                callStatus: 'success',
                querySite: querySite
            }
            sessionStorage.setItem('EQ1_searchData', JSON.stringify(searchData));
            fncDisplayResults(searchData);
        },
        'error': function(data, status, error) {
            fncDisplayResults(null, null, null, error, null)
        },
        'dataType': 'jsonp',
        'jsonp': 'json.wrf',
        'processData': false
    });
}

function fncDisplayResults(searchData) {
    if (searchData != null) {
        if (searchData.callStatus != 'success') {} else {
            var siteList = new Array();
            var site_ids = VENUE['solrSiteIdFilter'];
            var siteTextName = VENUE['siteTextName'];
            var siteIdTextMap = VENUE['siteIdTextMap'];
            var facetsSiteid = searchData.facets.site_id;
            for (var i = 0; i < (site_ids).length; i++) {
                siteList[i] = (facetsSiteid[site_ids[i]] > 0 && $.inArray(site_ids[i], objQuery.filters.site_id) != -1) ? siteIdTextMap[site_ids[i]] : null;
            }
            var siteText = jQuery.grep(siteList, function(val) {
                return val !== null;
            }).join(', ');
            if ((siteText.match(/,/g) || []).length == 2) {
                siteText = siteText.replace(/,(?=[^,]*$)/, ' and');
            }
            if (siteText == '') {
                siteText = siteTextName;
                siteText = siteText.join(', ');
                if ((siteText.match(/,/g) || []).length == 2) {
                    siteText = siteText.replace(/,(?=[^,]*$)/, ' and');
                }
            }
            $('#e1SitesFound').html(siteText);
            if (querySite == venuekey) {
                var numEvents = (typeof searchData.facets.event[1] != 'undefined') ? searchData.facets.event[1] : 0;
                var numAuctions = (typeof searchData.facets.event[2] != 'undefined') ? searchData.facets.event[2] : 0;
                $('#e1ResultsFound').autoNumeric('init', {
                    aSign: '',
                    aSep: ',',
                    aDec: '.',
                    mDec: '0',
                    mRound: 'D',
                    lZero: 'deny'
                });
                $('#e1ResultsFound').autoNumeric('set', numEvents);
                $('#e1AuctionsFound').autoNumeric('init', {
                    aSign: '',
                    aSep: ',',
                    aDec: '.',
                    mDec: '0',
                    mRound: 'D',
                    lZero: 'deny'
                });
                $('#e1AuctionsFound').autoNumeric('set', numAuctions);
            } else {
                numEvents = 0;
                numAuctions = 0;
            }
            var numFound = searchData.response.numFound - numEvents - numAuctions;
            $('.e1NumFound').autoNumeric('init', {
                aSign: '',
                aSep: ',',
                aDec: '.',
                mDec: '0',
                mRound: 'D',
                lZero: 'deny'
            });
            $('.e1NumFound').autoNumeric('set', numFound);
            $('.e1SiteToggle').removeClass('e1SiteToggleEvent');
            if (numEvents <= 0 && numFound <= 0 && numAuctions <= 0) {
                $('.e1EventsText').addClass('e1Hidden');
                $('.e1AuctionsText').addClass('e1Hidden');
                var browseAllLotsUrl = getVenuePage('browsealllots');
                var howTosearchUrl = getVenuePage('howtosearch');
                if (VENUE['hidehowtosearchbutton'] == 'true') {
                    var noResultsTemp = '<div class="row compare-section zero-border-top">' + '<div class="col-xs-24"><h4>Your search did not return any results. Please try another search keyword or browse all our lots. You can also see instructions on How to Search.</h4></div>' + '<div class="col-lg-24 text-right"><div class="row"><div class=" col-lg-10 col-md-13 col-sm-13 col-xs-24 pull-right"><div class="row">' + '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group pull-right"><a href="' + browseAllLotsUrl + '" class="btn btn-primary btn-block rmCache searchnoresultp-btn">Browse All Lots</a></div>' + '</div></div></div></div></div>';
                } else {
                    var noResultsTemp = '<div class="row compare-section zero-border-top">' + '<div class="col-xs-24"><h4>Your search did not return any results. Please try another search keyword or browse all our lots. You can also see instructions on How to Search.</h4></div>' + '<div class="col-lg-24 text-right"><div class="row"><div class=" col-lg-10 col-md-13 col-sm-13 col-xs-24 pull-right"><div class="row">' + '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group"><a href="' + browseAllLotsUrl + '" class="btn btn-primary btn-block rmCache searchnoresultp-btn">Browse All Lots</a></div>' + '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a href="' + howTosearchUrl + '" class="btn btn-primary btn-block searchnoresultp-btn e1hidden">How to Search?</a></div>' + '</div></div></div></div></div>';
                }
                $('#e1ResultsList').empty();
                $(noResultsTemp).appendTo('#e1ResultsList');
                $('.selectpages').addClass('e1Hidden');
                if (display_lot_counts == 'false') {
                    $('.e1ResultsText').addClass('e1Hidden');
                    $('.e1EventsText').addClass('e1Hidden');
                    $('.e1AuctionsText').addClass('e1Hidden');
                }
            } else {
                $('.e1ResultsText').addClass('e1Hidden');
                $('.e1EventsText').addClass('e1Hidden');
                $('.e1AuctionsText').addClass('e1Hidden');
                $('.selectpages').removeClass('e1Hidden');
                if (numFound > 0 && display_lot_counts != 'false') {
                    $('.e1ResultsText').removeClass('e1Hidden');
                }
                if (numEvents > 0 && display_lot_counts != 'false') {
                    $('.e1EventsText').removeClass('e1Hidden');
                }
                if (numAuctions > 0 && display_lot_counts != 'false') {
                    $('.e1AuctionsText').removeClass('e1Hidden');
                }
            }
            var currentpageType = $('#currentEvent').val();
            if (currentpageType == 'eventresult' || currentpageType == 'events') {
                $('.e1ResultsFrom').hide();
                $('.e1SiteToggle').hide();
            }
            if (currentpageType == 'events') {
                $('#e1ResultType').html('Events on ');
                $('#e1SitesFound').html('EquipmentOne ');
            }
            fncBuildFacets(searchData.facets, searchData.stats);
            fncBuildResults(searchData.response);
        }
        var cListingSpan = ReadCookie('CURRENT_LISTING_SPAN');
        var isSearchPage = ReadCookie('IS_SEARCHPAGE');
        if (isSearchPage == 'true' && window.location.href.indexOf('login_error') != -1) {
            $('#' + cListingSpan + ' .jsLogin').click();
            $('#' + cListingSpan + ' .e1InlineLoginMess').html('<span style="color:#F00;">Either your email or password is incorrect or you do not have an account yet.</span>');
            $('#' + cListingSpan + '  input:text').first().val('');
            $('#' + cListingSpan + '  input:text').first().focus();
        }
        $('.jsCompareRmv').each(function() {
            var cmpListingItem = $('#listing-' + $(this).attr("name"))
            if (cmpListingItem.find(".jsCompare").prop('checked') == false) {
                cmpListingItem.find('.jsCompare').attr('checked', true);
                cmpListingItem.find('.jsListingImage').addClass('e1ListingCompared');
                cmpListingItem.find('.jsCompareContainer').addClass('e1comparedChecked');
            }
        });
        UserListings.fncInitUserListingData();
    }
}

function fncGetParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function isEncoded(str) {
    return decodeURIComponent(str) !== str;
}

function fncBuildFacets(facets, stats) {
    $('.renderedFacet').remove();
    $('.myone-menu').metisMenu('destroy');
    $('.myone-menu').metisMenu({
        toggle: false
    });
    tmpParsedStoredQuery = (typeof sessionStorage.getItem('EQ1_query') != 'undefined') ? JSON.parse(sessionStorage.getItem('EQ1_query')) : {};
    $.each(facets, function(facetName, facetArr) {
        facetArr = $.sortByKeys(facetArr);
        if (facetName != 'site_id' && facetName != 'event' && $.isEmptyObject(facetArr) !== true) {
            var tmpFacet = $('#facetTemplate').clone(true);
            tmpFacet.removeClass('e1Hidden');
            tmpFacet.addClass('renderedFacet');
            dataFilterName = facetName;
            tmpFacet.attr('id', facetTitles[facetName]);
            tmpFacet.find('.jsFacetTitle').before(facetTitles[facetName]);
            if (objQuery.filters[dataFilterName] != 'undefined' && objQuery.filters[dataFilterName] != undefined) {
                for (i = 0; i < objQuery.filters[dataFilterName].length; i++) {
                    if (isEncoded(objQuery.filters[dataFilterName][i]) == false) {
                        objQuery.filters[dataFilterName][i] = encodeURIComponent(objQuery.filters[dataFilterName][i]);
                    }
                }
            }
            $.each(facetArr, function(filterName, objItemDetails) {
                var filterSelected = ($.inArray(encodeURIComponent(objItemDetails.searchKey), objQuery.filters[dataFilterName]) != -1) ? 'checked' : '';
                var listItem = ($.inArray(encodeURIComponent(filterName), objQuery.filters[dataFilterName]) != -1) ? $('<li class="jsFilterSelected" />') : $('<li />');
                var currentList = tmpFacet.find('.sidenav');
                listItem.append('<span class="leftcheckbox">\
         <input type="checkbox" ' + filterSelected + ' \
          data-filterName="' + dataFilterName + '" \
          data-filterValue="' + objItemDetails.searchKey + '">\
        </span>' + filterName + ' (' + objItemDetails.count + ')');
                listItem.attr('data-filterName', dataFilterName);
                listItem.attr('data-filterValue', objItemDetails.searchKey);
                listItem.attr('data-filterDisplay', filterName);
                listItem.find('.jsFacetTitle').html(filterName);
                listItem.appendTo(currentList);
            });
            tmpFacet.removeClass('jsTemplate');
            $('.jsFacetPlaceholder').before(tmpFacet);
        } else if (facetName != 'event') {
            $.each(facetArr, function(filterName, objItemDetails) {
                filterNameClass = filterName.replace(" ", '-');
                var objSite = $('.' + filterNameClass + 'Count');
                if (objSite != undefined && objSite != '') {
                    objSite.text('(' + objItemDetails.count + ')');
                    if (objItemDetails !== 0) {
                        if (typeof(objQuery.filters['site_id']) == 'undefined') {
                            objQuery.filters['site_id'] = [];
                        }
                        if ($.inArray(filterName, objQuery.filters['site_id']) == -1 && $.inArray(filterName, tmpParsedStoredQuery.filters.site_id) != -1) {
                            objQuery.filters['site_id'].push(filterName);
                        }
                        objSite.closest('li').find('input:checkbox').prop('disabled', false);
                        var objQueryFilterSiteid = objQuery.filters.site_id;
                        for (var i = 0; i < (objQueryFilterSiteid).length; i++) {
                            if (objSiteSelected[filterNameClass] && $.inArray(filterName, objQueryFilterSiteid) != -1) {
                                objSite.closest('li').find('input:checkbox').prop('checked', true);
                                objSite.closest('li').addClass('jsFilterSelected');
                                objSite.closest('li').addClass('jsFilterRemove');
                            } else {
                                objSite.closest('li').removeClass('jsFilterSelected');;
                                objSite.closest('li').find('input:checkbox').prop('checked', false);
                                if (display_source_site_filters == 'false' && objSite.closest('li').hasClass('jsFilterRemove') == false) {
                                    objSite.closest('li').addClass('e1Hidden');
                                }
                            }
                        }
                    } else {
                        objSite.closest('li').removeClass('jsFilterSelected');
                        objSite.closest('li').find('input:checkbox').prop('disabled', true);
                        objSite.closest('li').find('input:checkbox').prop('checked', false);
                    }
                }
            });
        }
    });
    if (objQuery.q == '*') {
        strPathName = decodeURIComponent(window.location.pathname);
        arrPathNameArray = strPathName.substring(1).split('/');
        strParentPath = arrPathNameArray[0];
        strPathText = "";
        isCompPage = false;
        if (strParentPath != 'search') {
            if (arrPathNameArray.length > 1) {
                strChildPath = arrPathNameArray[1].replace("listings", "");
                strPathTextArray = strChildPath.split(/[_\/ -]/);
                if (strPathTextArray)
                    $.each(strPathTextArray, function(index, text) {
                        if (text == 'equipmentone') {
                            strPathText = strPathText + " EquipmentOne";
                        } else if (strPathText.indexOf("Salvage") >= 0 && text == 'sale') {
                            strPathText = strPathText + text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
                        } else if (text != 'all') {
                            strPathText = strPathText + " " + text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
                        }
                    });
            } else if (arrPathNameArray.length == 1) {
                if (arrPathNameArray[0] == 'all-equipment')
                    strPathText = strPathText + " Equipment";
                else
                    strPathText = strPathText + " " + arrPathNameArray[0].charAt(0).toUpperCase() + arrPathNameArray[0].substr(1).toLowerCase();
                isCompPage = true;
            }
            if (arrPathNameArray[1] == 'ritchie-bros-auctions' || arrPathNameArray[1] == 'salvage-sale-equipment' || arrPathNameArray[1] == 'events') {
                strPathText = "All " + strPathText;
            }
        }
        if (strPathText == "") {
            var title = fncGetParameterByName('title');
            if (title != "") {
                strPathText = title;
            } else {
                strPathText = "All";
            }
        }
        if (typeof strChildPath != 'undefined' && strChildPath != 'ritchie-bros-auctions' && strChildPath != 'events' && strChildPath != 'salvage-sale-equipment') {
            if (strPathText != 'All') {
                if (typeof strChildPath != 'undefined' && strChildPath == 'closing-today') {
                    strPathText = 'All Lots ' + strPathText;
                } else {
                    strPathText = 'All ' + strPathText + ' Lots';
                }
            } else {
                strPathText = strPathText + ' Lots';
            }
            strPathText = strPathText.replace("Definite Sale", "Reserve Met");
        } else if (isCompPage == true) {
            if (strPathText != 'All') {
                if (typeof strChildPath != 'undefined' && strChildPath == 'closing-today') {
                    strPathText = 'All Lots ' + strPathText;
                } else {
                    strPathText = 'All ' + strPathText + ' Lots';
                }
            } else {
                strPathText = strPathText + ' Lots';
            }
        }
        $('.e1TermList li').html(strPathText);
    } else if (objQuery.q == '(event:1)') {
        $('.e1TermList li').html('All Events<a class="e1FBCloseIcon">x</a>');
    } else if (isSellerId == false) {
        $('.e1TermList li').html(strip_tags(objQuery.q) + '<span class="e1FBCloseIcon glyphicon glyphicon-remove glip-cancel"></span>');
    }
    var tmpFacet = $('#facetTemplate').clone(true);
    tmpFacet.removeClass('e1Hidden');
    tmpFacet.addClass('renderedFacet');
    tmpFacet.attr('id', 'Year');
    tmpFacet.addClass('jsYear');
    tmpFacet.find('.jsFacetTitle').html('Year');
    var objYearSlider = $('<div id="yearSlider" class="e1YearSlider"/><div class="yearvalue"><div class="legendyear">1950</div><div class="currentyear">' + curDate.getFullYear() + '</div></div>');
    var objYearText = $('<div id="yearText" class="e1YearText"/>');
    var objYearLegend = $('<div class="yearLegend"><span class="yearLegendMin">1950</span><span class="yearLegendMax">' + curDate.getFullYear() + '</span></div>');
    $(objYearText).attr("data-filterName", "year");
    var currentList = tmpFacet.find('.sidenav');
    $(currentList).find('.sidenav div').remove();
    $(objYearSlider).appendTo(currentList);
    $(objYearText).appendTo(currentList);
    $('.filtersMenu>li:nth-last-child(5)').before(tmpFacet);
    if (typeof(stats.year) != 'undefined' && stats.year != null) {
        yearSlider(stats.year.min, stats.year.max);
        $(objYearText).attr("data-filterValue", stats.year.min + " TO " + stats.year.max);
        $(objYearSlider).children().attr("data-filterName", "year");
        $(objYearSlider).children().attr("data-filterValue", stats.year.min + " TO " + stats.year.max);
        var minYear = 0;
        var maxYear = curDate.getFullYear();
        if (objQuery.filters['year'] != undefined && $.keys(objQuery.filters['year']).length > 0) {
            fncShowHideFilters($('.jsYear'));
        }
    } else {
        yearSlider(0, 0);
        tmpFacet.addClass('e1Hidden')
    }
    fncShowHideFilters($('.filtersMenu li.searchlist-heading:nth-child(2)'));
    hasSelected = $('.filtersMenu').find('li.jsFilterSelected').closest('.searchlist-heading');
    $('.zip_code').on('keyup', function() {
        var $this = $(this);
        if ($this.val() != '') {
            $('.zip_search').removeClass('compare-disable');
            $('.zip_search').addClass('btn-primary');
        } else {
            $('.zip_search').addClass('compare-disable');
            $('.zip_search').removeClass('btn-primary');
        }
    });
    $('.myone-menu').metisMenu('destroy');
    $('.myone-menu').metisMenu({
        toggle: false
    });
    $.each(hasSelected, function(index, facetContainer) {
        if ($(facetContainer).find('.jsFacetTitle').html() != 'Source Site' || blnFiltersModified == true) {
            fncShowHideFilters($(facetContainer));
        }
    });
    tmpFacet.removeClass('jsTemplate');
}

function strip_tags(input, allowed) {
    allowed = (((allowed || '') + '').toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('');
    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
        commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
    return input.replace(commentsAndPhpTags, '').replace(tags, function($0, $1) {
        return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
    }).replace(/\+/g, ' ');
}

function fncBuildResults(data) {
    fncBuildPagination(data.numFound, data.start);
    if (data.numFound == 0) {
        return true;
    }
    $('#e1ResultsList').empty();
    $.each(data.docs, function(index, listingItem) {
        if (typeof listingItem['ALLOWEDCOMMERCEMETHODS'] != 'undefined') {
            listingItem['ALLOWEDCOMMERCEMETHODS'] = JSON.parse(unescape(listingItem['ALLOWEDCOMMERCEMETHODS']));
        }
        var listingTemplate = $('.jsListingTemplate').clone();
        listingTemplate.removeClass('jsListingTemplate');
        listingTemplate.removeAttr('id');
        $.each(listingItem, function(index, value) {
            if (value == "") {
                listingItem[index] = "--";
            }
            if (index == 'LEADOFFERPRICE' && value == "") {
                listingItem[index] = 0;
            }
        });
        var apiIndex = '';
        if (typeof listingItem.NEXTBID == 'undefined') {
            listingItem.NEXTBID = listingItem.NEXTOFFERAMOUNT;
        }
        $container = $('#e1ResultsList');
        if (listingItem.EVENT != 1 && listingItem.EVENT != 2) {
            switch (listingItem.MARKETPLACE) {
                case 'RBA':
                    fncInitListingConfig();
                    listingConfig.displayRBAListingId = true;
                    listingConfig.displayLocation = true;
                    listingConfig.displaySellerLogo = true;
                    listingConfig.displayAbsoluteImage = true;
                    listingConfig.displayRBALinks = true;
                    listingConfig.displayVINorSerial = true;
                    listingConfig.displayMakeModel = true;
                    listingConfig.displayYear = true;
                    listingConfig.displayModel = true;
                    listingConfig.displayCondition = true;
                    listingConfig.displayMileage = true;
                    listingConfig.displayCompare = true;
                    listingConfig.displayCloseDate = true;
                    listingConfig.displayEventsCol = true;
                    fncRenderListing(listingItem, index, $container);
                    break;
                case 'SS':
                    fncInitListingConfig();
                    listingConfig.displayListingId = true;
                    listingConfig.displaySalvageSaleLinks = true;
                    listingConfig.displayVINorSerial = true;
                    listingConfig.displayMakeModel = true;
                    listingConfig.displayYear = true;
                    listingConfig.displayMileage = true;
                    listingConfig.displayDefiniteSale = true;
                    listingConfig.displaySellerLogo = true;
                    listingConfig.displayRelativeImage = true;
                    listingConfig.displayListingLinks = true;
                    listingConfig.displayCompare = true;
                    listingConfig.displayLocation = true;
                    listingConfig.displayCloseDate = true;
                    listingConfig.displayEventsCol = true;
                    fncRenderListing(listingItem, index, $container);
                    break;
                default:
                    fncInitListingConfig();
                    listingConfig.displayListingId = true;
                    listingConfig.displayBuyersConsole = true;
                    listingConfig.displayTimeleft = true;
                    listingConfig.displayRank = true;
                    listingConfig.displayLocation = true;
                    listingConfig.displayDefiniteSale = true;
                    listingConfig.displaySellerLogo = true;
                    listingConfig.displayRelativeImage = true;
                    listingConfig.displayListingLinks = true;
                    listingConfig.displayCompare = true;
                    listingConfig.displayDefiniteSale = true;
                    listingConfig.displayAuctionNumber = true;
                    arrUpdateIDs.push(listingItem.LISTINGID);
                    fncRenderListing(listingItem, index, $container);
                    break;
            }
        } else {
            fncInitListingConfig();
            listingConfig.displayEventId = true;
            listingConfig.displaySellerLogo = true;
            listingConfig.displayEventLinks = true;
            listingConfig.displayLotCount = true;
            listingConfig.displayHighlights = true;
            listingConfig.displayEventsCol = true;
            if (listingItem.MARKETPLACE == 'RBA') {
                listingConfig.displayAbsoluteImage = true;
                listingConfig.displayDescription = true;
                listingConfig.displayFirstLotClosingRBA = true;
            } else {
                listingConfig.displayRelativeImage = true;
                listingConfig.displayFirstLotClosingEvent = true;
            }
            fncRenderListing(listingItem, index, $container);
        }
    });
    if (data.docs.length % 2 == 0) {
        $('.jsBottomPagination').addClass('white-bg');
        $('.selectpages').addClass('white-bg');
    } else {
        $('.jsBottomPagination').removeClass('white-bg');
        $('.selectpages').removeClass('white-bg');
    }
    send2Monetate.pageType = 'search'
    send2Monetate.addProducts();
}

function fncBuildPagination(totalListings, intStart) {
    if (typeof resultsPerPage != 'undefined') {
        var numPerPage = resultsPerPage;
    } else {
        var numPerPage = 25;
    }
    var numPages = Math.ceil(totalListings / numPerPage);
    var currentPage = (intStart / numPerPage == 0) ? 1 : Math.ceil(intStart / numPerPage + 1);
    if (currentPage > numPages) {
        currentPage = numPages;
    }
    var previousPage = ((intStart - numPerPage) <= 0) ? 0 : intStart - numPerPage;
    var nextPage = intStart + numPerPage;
    var lastPage = Math.ceil(totalListings / numPerPage);
    var paginationText = $('<li>' + currentPage + '</li>');
    var tmpQueryString = $.extend(true, {}, objQuery);
    tmpQueryString.params.start = previousPage;
    var seoQueryString = JSON.stringify(tmpQueryString);
    $('.jsPaginationRendered').remove();
    var paginationControls = $('.jsPaginationTemplate2 .e1SearchPagination').clone(true);
    paginationControls.empty();
    paginationControls.addClass('jsPaginationRendered')
    var includePreviousElipses = false;
    var includeNextElipses = false;
    paginationControls.append('<div class="col-md-24"><div class="row"></div></div>');
    if (numPages > 3) {
        if (currentPage < lastPage - 2) {
            includeNextElipses = true;
        }
        if (currentPage > 3) {
            includePreviousElipses = true;
        }
    }
    if (numPages > 1) {
        $('.page-section').removeClass('e1Hidden');
        $('.selectpages').removeClass('e1Hidden');
        paginationControls.find('.row').append('<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3 previous-page pagePrev" ></div>');
        if (currentPage > 1) {
            var pageStart = ((currentPage - 1) * numPerPage) - numPerPage;
            tmpQueryString.params.start = pageStart;
            seoQueryString = JSON.stringify(tmpQueryString);
            var previousLink = $('<a href="/search?eq=' + seoQueryString + '" data-startVal="' + pageStart + '"><i class="fa fa-arrow-left">&nbsp;</i> <span class="hidden-xs">Previous Page</span></a>');
            paginationControls.find('.previous-page').append(previousLink);
        }
        paginationControls.find('.row').append('<div class="col-lg-12 col-md-12 col-sm-12 col-xs-18 text-center page-number"></div>');
        if (currentPage != 1) {
            paginationControls.find('.text-center').append('<a href="#" data-startVal="' + 0 + '"">1</a> ');
        }
        if (includePreviousElipses == true) {
            paginationControls.find('.text-center').append('<span class="page-dots">...</span> ');
        }
        if (currentPage > 2) {
            paginationControls.find('.text-center').append('<a href="#" data-startVal="' + ((currentPage - 2) * numPerPage) + '">' + (currentPage - 1) + '</a> ');
        }
        paginationControls.find('.text-center').append('<a href="javascript:void(0)" class="page-active" data-startVal="' + ((currentPage - 1) * numPerPage) + '">' + currentPage + '</a> ');
        if (currentPage < (lastPage - 1)) {
            paginationControls.find('.text-center').append('<a href="#" data-startVal="' + ((currentPage) * numPerPage) + '">' + (currentPage + 1) + '</a> ');
        }
        if (includeNextElipses == true) {
            paginationControls.find('.text-center').append('<span class="page-dots">...</span> ');
        }
        if (currentPage != lastPage) {
            paginationControls.find('.text-center').append('<a href="javascript:void(0)" data-startVal="' + ((lastPage - 1) * numPerPage) + '">' + lastPage + '</a> ');
        }
        if (currentPage != lastPage) {
            tmpQueryString.params.start = nextPage;
            seoQueryString = JSON.stringify(tmpQueryString);
            var nextPageLink = $('<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3 pull-right text-right next-page pageNext" ><a href="/search?eq=' + seoQueryString + '" data-startVal="' + ((currentPage) * numPerPage) + '"><span class="hidden-xs">Next Page</span> <i class="fa fa-arrow-right">&nbsp;</i></a></div>');
            paginationControls.find('.row').append(nextPageLink);
        }
    } else {
        $('.page-section').addClass('e1Hidden');
        $('.selectpages').addClass('e1Hidden');
    }
    $('.jsTopPagination').prepend(paginationControls);
    var lowerPaginationControls = $(paginationControls).clone(true);
    lowerPaginationControls.find('a').on('click', function() {
        $('body,html').scrollTop($('#e1CompareList').offset().top);
    });
    $('.jsBottomPagination').prepend(lowerPaginationControls);
}

function fncShowResultsMap() {
    displayType = 'map';
    fncBuildSolrQuery();
}

function fncLoadMapScript() {
    var strClusterScript = document.createElement('script');
    var strMapScript = document.createElement('script');
    blnMapLoaded = true;
    strClusterScript.type = 'text/javascript';
    strMapScript.type = 'text/javascript';
    strMapScript.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=flase&callback=fncInitalizeMap';
    var host = window.location.host;
    strClusterScript.src = 'http://' + host + '/js/markerclusterer.js';
    $('head').append(strMapScript);
    $('head').append(strClusterScript);
}

function fncInitalizeMap() {
    if (mapType == 'search') {
        var mapOptions = {
            zoom: 3,
            center: new google.maps.LatLng(41.241190, -77.001079),
            gridSize: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        objSearchMap = new google.maps.Map($('#map-canvas')[0], mapOptions);
        objSearchMap.infoWindow = new google.maps.InfoWindow();
        fncShowResultsMap();
    } else if (mapType == 'item') {
        var mapOptions = {
            zoom: 6,
            center: new google.maps.LatLng(41.241190, -77.001079),
            gridSize: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        objItemMap = new google.maps.Map($('#item-map-canvas')[0], mapOptions);
        objItemMap.data.addListener('addfeature', function(e) {
            objItemMap.panTo(e.feature.getGeometry().get())
        });
        objItemMap.data.loadGeoJson("/geo_data.php?fq=asset_id:" + strMapAsset);
    } else if (mapType == 'geoCoder') {
        var postalCode, range;
        var postalForm = $('.zip_code');
        var ranageForm = $('.zip_range');
        for (i = 0; i < postalForm.length; i++) {
            if ($(postalForm[i]).val() != '') {
                postalCode = postalForm[i];
                range = ranageForm[i];
            }
        }
        objGeoCoder = new google.maps.Geocoder();
        fncGeoCodeAddress(postalCode, range);
    } else if (mapType == 'reverseGeoCoder') {
        objGeoCoder = new google.maps.Geocoder();
        fncReverseGeoData();
    }
}

function fncAddCluster(objFeature) {
    var geoFeature = objFeature.feature.getGeometry();
    var isEvent = objFeature.feature.getProperty('event');
    var siteFlag = objFeature.feature.getProperty('site_id');
    if (isEvent) {
        imageUrl = '/images/E1-Event.png';
    } else {
        if (siteFlag == 2) {
            imageUrl = '/images/E1-Listing.png';
        } else {
            imageUrl = '/images/RBA-Listing.png';
        }
    }
    var markerImage = new google.maps.MarkerImage(imageUrl, new google.maps.Size(32, 32));
    if (geoFeature.getType() === "Point") {
        var objMarker = new google.maps.Marker({
            position: geoFeature.get(),
            title: objFeature.feature.getProperty('asset_title'),
            description: objFeature.feature.getProperty('description'),
            icon: markerImage
        });
        objMarker.featureInfo = objFeature;
        google.maps.event.addListener(objMarker, 'click', function(e) {
            var objPosition = this.featureInfo.feature.getGeometry().get();
            var strTimeZone = this.featureInfo.feature.getProperty('time_zone');
            var latLng = this.featureInfo.feature.getProperty('lat_lng')
            $.ajax({
                url: solrURL + solrMapPath,
                data: 'fq=(longitude:"' + latLng[0] + '" AND latitude:"' + latLng[1] + '")',
                type: 'post',
                markerPosition: objPosition,
                timeZone: strTimeZone,
                async: true,
                success: function(locationResults) {
                    fncBuildInfoWindow(locationResults.response, this.markerPosition, this.timeZone);
                },
                'dataType': 'jsonp',
                'jsonp': 'json.wrf',
                'processData': false
            });

            function fncBuildInfoWindow(objResults, objPosition, strTimeZone) {
                var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                infoHTMLTemplate = $('#e1InfoWindowTemplate').clone();
                infoHTMLTemplate.removeClass('jsTemplate');
                infoHTMLTemplate.removeAttr('id');
                if (objResults.numFound == 1) {
                    var objListing = objResults.docs[0];
                    var strCityState = objListing.location[0] + ', ' + objListing.location[1];
                    var strCountry = objListing.location[3];
                    var objDate = new Date(Date.parse(objResults.docs[0].auction_end_date));
                    if (objDate.getUTCHours() > 12) {
                        strHour = objDate.getUTCHours() - 12;
                        strMeridiem = ' PM ' + strTimeZone;
                    } else {
                        strHour = objDate.getUTCHours();
                        strMeridiem = ' AM ' + strTimeZone;
                    }
                    var strMinutes = ('0' + objDate.getUTCMinutes()).slice(-2);
                    var strDate = monthNames[objDate.getMonth()] + ' ' + objDate.getDate() + ', ' + objDate.getFullYear();
                    var strTime = strHour + ':' + strMinutes + strMeridiem;
                    if (objListing.site_id == 'RBA') {
                        infoHTMLTemplate.find('.e1InfoWindowLotNum').addClass('e1Hidden');
                        infoHTMLTemplate.find('.jsLeaveURL').attr('href', objListing.url);
                        infoHTMLTemplate.find('.jsLeaveURL').attr('target', 'blank');
                        infoHTMLTemplate.find('.e1InfoWindowLeaving').removeClass('e1Hidden');
                    } else {
                        infoHTMLTemplate.find('.e1InfoWindowLotNumText').text(objListing.lot_number);
                        infoHTMLTemplate.find('.e1InfoWindowTitleLink').attr('href', '/listing?listingid=' + objListing.asset_id);
                    }
                    if (objListing.thumbnail == null) {
                        strThumbnail = 'https://eq1-assetnation.netdna-ssl.com/images/na.png'
                    } else {
                        strThumbnail = objListing.thumbnail;
                    }
                    infoHTMLTemplate.find('.e1InfoWindowLotNumText').text(objListing.lot_number);
                    infoHTMLTemplate.find('.e1InfoWindowTitleText').text(objListing.asset_title);
                    infoHTMLTemplate.find('.e1InfoWindowCityStateText').text(strCityState);
                    infoHTMLTemplate.find('.e1InfoWindowCountryText').text(strCountry);
                    infoHTMLTemplate.find('.e1InfoWindowDateText').text(strDate);
                    infoHTMLTemplate.find('.e1InfoWindowTimeText').text(strTime);
                    infoHTMLTemplate.find('.e1InfoWindowImgSrc').attr('src', strThumbnail);
                    strHTML = $('<div>').append(infoHTMLTemplate).html();
                } else {
                    infoHTMLTemplate.find('.e1InfoWindowScrollingTable').removeClass('e1Hidden');
                    $.each(objResults.docs, function(index, objListing) {
                        var siteFlag = objListing.site_id;
                        var tableRowTemplate = $('#e1InfoRowTemplate').clone();
                        tableRowTemplate.removeClass('jsTemplate');
                        tableRowTemplate.removeAttr('id');
                        var strLocation = objListing.location[0] + ', ' + objListing.location[1] + ', ' + objListing.location[3];
                        var objDate = new Date(Date.parse(objResults.docs[0].auction_end_date));
                        if (objDate.getUTCHours() > 12) {
                            strHour = objDate.getUTCHours() - 12;
                            strMeridiem = ' PM ' + strTimeZone;
                        } else {
                            strHour = objDate.getUTCHours();
                            strMeridiem = ' AM ' + strTimeZone;
                        }
                        var strMinutes = ('0' + objDate.getUTCMinutes()).slice(-2);
                        var strDate = monthNames[objDate.getMonth()] + ' ' + objDate.getDate() + ', ' + objDate.getFullYear();
                        var strTime = strHour + ':' + strMinutes + strMeridiem;
                        if (siteFlag == 1) {
                            var siteIcon = '/images/RBA-Listing.png';
                            var strID = '&mdash;';
                            var strLink = objListing.url;
                            tableRowTemplate.find('.e1InfoWindowMutliTitleLink').attr('target', 'blank');
                            infoHTMLTemplate.find('.e1InfoWindowLeavingMulti').removeClass('e1Hidden');
                        } else if (objListing.event) {
                            var siteIcon = '/images/E1-Event.png';
                            var strID = objListing.asset_id;
                            var strLink = '/listing?listingid=' + objListing.asset_id;
                        } else {
                            var siteIcon = '/images/E1-Listing.png';
                            var strID = objListing.asset_id;
                            var strLink = '/listing?listingid=' + objListing.asset_id;
                        }
                        tableRowTemplate.find('.e1InfoWindowIconSrc').attr('src', siteIcon);
                        tableRowTemplate.find('.e1InfoWindowMultiID').text(strID);
                        tableRowTemplate.find('.e1InfoWindowMutliTitleLink').attr('href', strLink);
                        tableRowTemplate.find('.e1InfoWindowMutliTitleLink').text(objListing.asset_title);
                        tableRowTemplate.find('.e1InfoWindowMutliLocation').text(strLocation);
                        tableRowTemplate.find('.e1InfoWindowDateText').text(strDate);
                        tableRowTemplate.find('.e1InfoWindowTimeText').text(strTime);
                        infoHTMLTemplate.find('.e1InfoWindowMultipleTable').find('tbody').append(tableRowTemplate);
                    });
                    infoHTMLTemplate.find('.e1InfoWindowImage').remove();
                    infoHTMLTemplate.find('.e1InfoWindowDetails').remove();
                    infoHTMLTemplate.find('.e1InfoWindowLeaving').remove();
                    infoHTMLTemplate = $('<div>').append(infoHTMLTemplate)
                    infoHTMLTemplate.find('.e1InfoWindow').css('width', '425px')
                    strHTML = infoHTMLTemplate.html();
                }
                objSearchMap.infoWindow.setContent(strHTML);
                objSearchMap.infoWindow.setPosition(objPosition);
                objSearchMap.infoWindow.open(objSearchMap);
            }
        });
        objSearchMap.MarkerClusterer.addMarker(objMarker);
        objSearchMap.data.remove(objFeature.feature);
    }
}

function fncShowResultsGrid() {
    displayType = 'grid';
    $('.e1GridSelect').removeClass('e1GridSelect e1GrayShadow').addClass('e1GridSelected e1OrangeShadow');
    $('.e1MapSelected').removeClass('e1MapSelected e1OrangeShadow').addClass('e1MapSelect e1GrayShadow');
    $('#e1ResultsList').removeClass('e1Hidden');
    $('.e1MapContainer').addClass('e1Hidden');
    $('.e1ResultsFrom').removeClass('e1Hidden');
    $('.e1SiteToggle').removeClass('e1Hidden');
    fncBuildSolrQuery();
}

function fnGetURLParameter(urlParam) {
    var pageURL = window.location.search.substring(1);
    var pageVars = pageURL.split('&');
    for (var i = 0; i < pageVars.length; i++) {
        var paramArray = pageVars[i].split('=');
        if (paramArray[0] == urlParam) {
            return decodeURIComponent(paramArray[1]);
        }
    }
    return false;
}

function fncCheckDST(strTime) {
    $.ajax({
        url: '/util.php',
        data: {
            action: 'getDST',
            time: strTime
        },
        type: 'post',
        async: false,
        success: function(isDST) {
            return isDST;
        }
    });
}

function fncParseSavedQuery(objSplitHash) {
    $.each(objSplitHash, function(index, objHashPart) {
        fncParseQueryParts(objHashPart);
    });
}

function fncParseQueryParts(objHashPart) {
    objSplitHashPart = objHashPart.split('=');
    objStrName = objSplitHashPart[0];
    objQueryParts = objSplitHashPart[1].split('+AND+');
    if (objStrName == "filter") {
        $.each(objQueryParts, function(index, objQueryPart) {
            objQueryPart = objQueryPart.replace(/["'()]/g, "")
            if (objQueryPart.indexOf('+OR+') != -1) {
                tmpParts = objQueryPart.split('+OR+');
                $.each(tmpParts, function(index, tmpPart) {
                    tmpFilter = tmpPart.split(':');
                    objQueryPart = tmpFilter[1];
                    objQueryPart = objQueryPart.replace(/\+/g, "%20");
                    objQueryPart = decodeURIComponent(objQueryPart);
                    objQueryPart = objQueryPart.replace(/["'()\[\]]/g, "")
                    tmpFilterItem = $('<div />').attr({
                        'data-filterName': tmpFilter[0]
                    }).attr({
                        'data-filterValue': objQueryPart
                    });
                    if (tmpFilter[0] == 'site_id' && tmpFilter[1] == '0') {
                        querySite = "rowss";
                    }
                    fncAddFilter(tmpFilterItem);
                });
            } else {
                objQueryPart = objQueryPart.replace(/\+/g, "%20");
                objQueryPart = decodeURIComponent(objQueryPart);
                objQueryPart = objQueryPart.replace(/["'()\[\]]/g, "")
                objFilterParts = objQueryPart.split(':');
                tmpFilterItem = $('<div />').attr({
                    'data-filterName': objFilterParts[0]
                }).attr({
                    'data-filterValue': objFilterParts[1]
                });
                if (objFilterParts[0] == 'site_id' && objFilterParts[1] == '0') {
                    querySite = "rowss";
                }
                fncAddFilter(tmpFilterItem);
            }
        });
    } else {
        $.each(objQueryParts, function(index, filterValues) {
            fncParseQueryParams(objStrName, filterValues)
        });
    }
}

function fncParseQueryFilters(objFilters) {
    $.each(objFilters, function(index, filterValues) {
        if (index != 'year' && $.inArray(index, $.keys(facetTitles)) != -1) {
            filterName = index;
            $.each(filterValues, function(index, filterValue) {
                fncFilterbarAddRemove(filterName, decodeURIComponent(filterValue), decodeURIComponent(filterValue), 'add');
            });
        } else if (index == 'year') {
            filterItem = $(".e1YearText");
            yearArray = decodeURIComponent(filterValues[0]).split(' TO ');
            $(".e1YearText").attr("data-filterName", index);
            $(".e1YearText").attr("data-filterValue", filterValues[0]);
            yearSlider(yearArray[0], yearArray[1]);
            $(".e1YearText").html(decodeURIComponent(filterValues[0]));
            fncFilterbarAddRemove(index, decodeURIComponent(filterValues[0]), null, 'add');
        }
    });
}

function fncParseQueryParams(paramName, paramValue) {
    switch (paramName) {
        case "sort":
            objSortParams = paramValue.split(',');
            tmpSort = {};
            $.each(objSortParams, function(index, paramValue) {
                tmpParam = paramValue.split(':');
                sortDirection = (tmpParam[1] == 1) ? 'desc' : 'asc';
                tmpSort[tmpParam[0]] = sortDirection;
            });
            tmpSort = JSON.stringify(tmpSort);
            fncAddSorting(tmpSort);
            break;
        case "pt":
        case "d":
            fncReverseGeoFilter(paramName, paramValue);
            break;
    }
}

function fncSaveSearch(objQuery, input) {
    var $form = $(input).closest('.jsClickableFacets');
    if ($form.find('.search_name').val() == '') {
        return;
    }
    if (!loggedIn) {
        $('.jsSaveSearchVisitor').show();
        return;
    }
    $('.jsSaveSearchVisitor').hide();
    $('.jsEmptySavedSearch').hide();
    $.ajax({
        url: '/index.php?task=saveSearch',
        data: {
            query: 'eqs=' + JSON.stringify(objQuery),
            title: $form.find('.search_name').val()
        },
        success: function() {
            $('.search_name').hide();
            $('.e1SaveSearch').hide();
            $('.jsSaveSuccess').show();
        },
        error: function() {
            $('.jsSaveFailed').show();
        }
    });
}

function fncSendSearch(query) {
    var currentURL = window.location.href;
    var urlArray = currentURL.split("?");
    var searchURL = urlArray[0] + "?eqs=" + encodeURI(JSON.stringify(query));
    window.location.href = "mailto:?body=" + encodeURI(searchURL);
}

function fncScrollToResults() {
    $("html, body").animate({
        scrollTop: $("#e1CompareBar").offset().top - 30
    }, "slow");
}

function fncLeaveSite() {
    $('.e1LeavingMessage.jsLive').remove();
    siteURL = $(this).attr('data-siteURL');
    leavingTemplate = $('#e1LeavingTemplate').clone();
    leavingTemplate.addClass('jsLive');
    leavingTemplate.find('.jsLeaveURL').attr('href', siteURL);
    leavingTemplate.find('.jsLeaveURL').attr('target', 'blank');
    leavingTemplate.find('.jsLeaveURL').on('click', '.jsLeaveSite', fncCancelLeaveSite);
    leavingTemplate.on('click', '.jsCancelLeaveSite', fncCancelLeaveSite);
    $(this).closest('.e1ListingDetails').append(leavingTemplate);
}

function fncCancelLeaveSite() {
    $('.e1LeavingMessage.jsLive').remove();
}

function fncWatchListing() {
    var listingid = $(this).attr('data-AssetID');
    $.ajax({
        url: "/index.php?option=com_e1listing&format=raw&task=addToWatchList&listingID=" + listingid,
        type: "POST",
        dataType: "json",
        itemWatch: $(this),
        success: function(responseText) {
            var params = {};
            params.source = 'fncWatchListing';
            var response = fncAjaxResponse(response, params);
            this.itemWatch.replaceWith('\
    <span class="e1WatchingThis e1GrayIcon"></span>\
    <span class="e1WatchingThisText">Watching</span>');
            if (response.MESSAGE.search('already watching') != -1) {
                $.colorbox({
                    html: response.MESSAGE + '<div style="text-align:center;padding-top:20px;"><input id="btnClose" class="primary_80" type="button" value="OK" onclick="javascript:$.colorbox.close();"></div>',
                    width: '20%'
                });
            }
        },
        error: function() {
            $.colorbox({
                html: "<div>Unable to add listing to Watchlist, please try again later</div>"
            });
        }
    });
}

function fncValidateLogin(formArray) {
    var emailField = '';
    var pwField = '';
    for (i = 0; i < 7; i++) {
        if (formArray[i].name == 'josso_username') {
            emailField = $.trim(formArray[i].value);
        }
        if (formArray[i].name == 'josso_password') {
            pwField = $.trim(formArray[i].value);
        }
    }
    var currentListing = ReadCookie('CURRENT_LISTING_SPAN');
    if (emailField == '' && pwField == '') {
        $('#' + currentListing + ' .e1InlineLoginMess').html('<span style="color:#F00;">Please enter your email address and password.</span>');
        return false;
    } else if (emailField == '') {
        $('#' + currentListing + ' .e1InlineLoginMess').html('<span style="color:#F00;">Please enter your email address.</span>');
        return false;
    } else if (pwField == '') {
        $('#' + currentListing + ' .e1InlineLoginMess').html('<span style="color:#F00;">Please enter your password.</span>');
        return false;
    }
}

function fncRemoveIframe(desc) {
    var str = desc.replace(/?/g, '');
    str = str.replace(/\r?\n/g, '');
    str = str.replace(/(<iframe.*?>.*?<\/iframe>)/g, '');
    str = str.replace(/(&lt;iframe.*?&gt;.*?&lt;\/iframe&gt;)/g, '');
    if (str == '<p></p>' || str == '') {
        str = '&mdash;';
    }
    return str;
}

function trim_string(s) {
    if (s != undefined)
        return s.replace(/^\s+|\s+$/, '');
}

function fncAddWatchlistIntoUserObject() {
    jQuery.ajax('/home?task=addWatchlistIntoUserObj', {
        type: 'GET',
        data: 'maxresult=1000',
        dataType: "json",
        success: function(responseText) {
            jQuery.each(responseText, function() {
                var currentListingId = this;
                $listing = $('#listing-' + currentListingId);
                var config = {};
                config.data = {};
                config.data.curl = '/user/:userID/watchlist/venueID/:venueID';
                config.data.method = 'DELETE';
                config.data.version = '1';
                var postfields = {};
                postfields.listingID = currentListingId;
                config.data.postfields = JSON.stringify(postfields);
                config.url = '/home?task=ajax';
                $listing.find('.jsWatchMsg').hide();
                $listing.find('.jsWatchlistToggle div').removeClass('icon_watchlist');
                $listing.find('.jsWatchlistToggle div').addClass('icon-custom-watching');
                $listing.find('.jsWatchlistToggle div').removeClass('icon_watchlist');
                $listing.find('.jsWatchlistToggle div').addClass('icon-custom-watching');
                $listing.find('.jsWatchlistToggle').unbind('click');
                $listing.find('.jsWatchlistToggle').on('click', function() {
                    fncAjax(config);
                    fncDisplayAddToWatchlistInSearch(currentListingId);
                    $listing.find('.jsWatchMsg').show();
                    fncAddRemoveWatchlistItemFromUserObj(currentListingId, 0);
                    return false;
                })
            });
        },
        error: function(e) {}
    });
}

function fncDisplayRemoveFromWatchlistInSearch(currentListingId) {
    var $listing = $('#listing-' + currentListingId);
    var config = {};
    config.data = {};
    config.data.curl = '/user/:userID/watchlist/venueID/:venueID';
    config.data.method = 'DELETE';
    config.data.version = '1';
    var postfields = {};
    postfields.listingID = currentListingId;
    config.data.postfields = JSON.stringify(postfields);
    config.url = '/home?task=ajax';
    $listing.find('.jsWatchMsg').hide();
    $listing.find('.jsWatchlistToggle div').removeClass('icon_watchlist');
    $listing.find('.jsWatchlistToggle div').addClass('icon-custom-watching');
    $listing.find('.jsWatchlistToggle').unbind('click');
    $listing.find('.jsWatchlistToggle').on('click', function() {
        $('#enableCache').val(1);
        fncAjax(config);
        fncDisplayAddToWatchlistInSearch($listing, currentListingId);
        $listing.find('.jsWatchMsg').show();
        fncAddRemoveWatchlistItemFromUserObj(currentListingId, 0);
        return false;
    })
}

function fncRenderMyoneData(response) {
    var params = {};
    params.source = 'fncMyoneSearchSummary';
    var response = fncAjaxResponse(response, params);
    var overviewContentHtml = '';
    if (response != null && response.STATUS == 'OK') {
        if (response['SAVEDSEARCHCOUNT'] > 0) {
            overviewContentHtml += '<ul class="current-srch-sectiion"><li class="searchlist-heading">';
            overviewContentHtml += '<div class="search-list-head">Saved Searches';
            overviewContentHtml += '<div class="search-tags">';
            overviewContentHtml += '<ul class="list-block small-text">';
            var ssearches = response['SAVEDSEARCHES'];
            var scount = response['SAVEDSEARCHCOUNT'];
            if (scount > 3)
                scount = 3;
            for (var i = 0; i < scount; i++) {
                overviewContentHtml += ' <li style="word-wrap: break-word; word-break: break-all;"><a href="/search?' + encodeURI(ssearches[i].QUERYSTRING).replace(/%25/g, "%").replace(/%20AND%20/g, "%20") + '"> ' + ssearches[i].TITLE + '</a></li>';
            }
            overviewContentHtml += '</ul></div></div></li></ul>';
        }
        if (response['RECENTSEARCHCOUNT'] > 0) {
            overviewContentHtml += '<ul class="current-srch-sectiion"><li class="searchlist-heading" id="bottom_section">';
            overviewContentHtml += '<div class="search-list-head">Recent Searches';
            overviewContentHtml += '<div class="search-tags">';
            overviewContentHtml += '<ul class="list-block small-text">';
            var rsearches = response['RECENTSEARCHES'];
            var rcount = response['RECENTSEARCHCOUNT'];
            if (rcount > 3) {
                rcount = 3;
            }
            for (var i = 0; i < rcount; i++) {
                if (rsearches[i].QUERYSTRING.split('=')[1] !== '*' && ((rsearches[i].QUERYSTRING.split('=')[1]).substring(0, 1) !== "(")) {
                    overviewContentHtml += ' <li style="word-wrap: break-word; word-break: break-all;"><a href="/search?' + rsearches[i].QUERYSTRING + '"> Search results for \"' + rsearches[i].QUERYSTRING.split('=')[1] + '\"</a></li>';
                }
            }
            overviewContentHtml += '</ul></div></div></li></ul>';
        }
    }
    if (!E1_EMAIL_PREFERENCES) {
        var setprefcls = "";
        if (typeof rcount == 'undefined' && typeof scount == 'undefined') {
            setprefcls = ' custemail-button';
        }
        if (VENUE['venuekey'] == 'e1ss') {
            overviewContentHtml += '<div class="col-lg-24' + setprefcls + '"><a href="/myone?view=preferences"><button class="btn btn-primary btn-sm" type="submit" value="Set Equipment Preferences">Set Equipment Preferences</button></a></div>';
        }
    }
    if (!E1_USER_IS_LITE && !E1_PHONE_VERIFIED) {
        overviewContentHtml += '<a href="registration?view=verifyaccount"><button class="btn btn-primary" type="submit" value="Verify Phone">Verify Phone</button></a>';
    }
    $('.myoneupdates_recent_searches').html(overviewContentHtml);
}

function fncMyoneSearchSummary() {
    jQuery.ajax({
        url: "/index.php?option=com_e1home&format=raw&task=myoneupdates",
        type: "GET",
        dataType: "json",
        data: {
            request_json: "true"
        },
        success: fncRenderMyoneData
    });
};

function fncSetMarketingContentDisplay() {
    var cUrl = $(location).attr('href');
    var $searchResult = $('#e1SearchResultsContainer');
    if (fncGetE1LocalStorage(cUrl) == 'open') {
        $searchResult.find('#collapseread').addClass('in');
        $searchResult.find('.collapseread').addClass('collapsed');
        $searchResult.find('.collapseread').html('Read Less');
    } else if (fncGetE1LocalStorage(cUrl) == 'collapsed') {
        $searchResult.find('#collapseread').removeClass('in');
        $searchResult.find('.collapseread').removeClass('collapsed');
        $searchResult.find('.collapseread').html('Read More');
    }
    $searchResult.find('.collapseread').click(function() {
        if ($searchResult.find('.collapseread').hasClass("collapsed") == true) {
            fncSetE1LocalStorage(cUrl, 'collapsed');
        } else {
            fncSetE1LocalStorage(cUrl, 'open');
        }
    });
}

function fncSetCategoryContentDisplay() {
    var cUrl = $(location).attr('href');
    var $categoryContent = $('.e1ArticleContent');
    if (fncGetE1LocalStorage(cUrl) == 'open') {
        $categoryContent.find('#collapseExample').addClass('in');
        $categoryContent.find('.collapselink').addClass('collapsed');
        $categoryContent.find('.collapselink').html('Less');
    } else if (fncGetE1LocalStorage(cUrl) == 'collapsed') {
        $categoryContent.find('#collapseExample').removeClass('in');
        $categoryContent.find('.collapselink').removeClass('collapsed');
        $categoryContent.find('.collapselink').html('More');
    }
    $categoryContent.find('.collapselink').click(function() {
        if ($categoryContent.find('.collapselink').hasClass("collapsed") == true) {
            fncSetE1LocalStorage(cUrl, 'collapsed');
        } else {
            fncSetE1LocalStorage(cUrl, 'open');
        }
    });
}

/*===============================
https://www.equipmentone.com/e1_2/js/jqBootstrapValidation.js
================================================================================*/
;
(function($) {
    var createdElements = [];
    var defaults = {
        options: {
            prependExistingHelpBlock: false,
            sniffHtml: true,
            preventSubmit: true,
            submitError: false,
            submitSuccess: false,
            semanticallyStrict: false,
            autoAdd: {
                helpBlocks: true
            },
            filter: function() {
                return true;
            }
        },
        methods: {
            init: function(options) {
                var settings = $.extend(true, {}, defaults);
                settings.options = $.extend(true, settings.options, options);
                var $siblingElements = this;
                var uniqueForms = $.unique($siblingElements.map(function() {
                    return $(this).parents("form")[0];
                }).toArray());
                $(uniqueForms).bind("submit", function(e) {
                    var $form = $(this);
                    var warningsFound = 0;
                    var $inputs = $form.find("input,textarea,select").not("[type=submit],[type=image]").filter(settings.options.filter);
                    $inputs.trigger("submit.validation").trigger("validationLostFocus.validation");
                    $inputs.each(function(i, el) {
                        var $this = $(el),
                            $controlGroup = $this.parents(".control-group").first();
                        if ($controlGroup.hasClass("warning")) {
                            $controlGroup.removeClass("warning").addClass("field-error");
                            warningsFound++;
                        }
                    });
                    $inputs.trigger("validationLostFocus.validation");
                    if (warningsFound) {
                        if (settings.options.preventSubmit) {
                            e.preventDefault();
                        }
                        $form.addClass("error");
                        if ($.isFunction(settings.options.submitError)) {
                            settings.options.submitError($form, e, $inputs.jqBootstrapValidation("collectErrors", true));
                        }
                    } else {
                        $form.removeClass("error");
                        if ($.isFunction(settings.options.submitSuccess)) {
                            settings.options.submitSuccess($form, e);
                        }
                    }
                });
                return this.each(function() {
                    var $this = $(this),
                        $controlGroup = $this.parents(".control-group").first(),
                        $helpBlock = $controlGroup.find(".help-block").first(),
                        $form = $this.parents("form").first(),
                        validatorNames = [];
                    if (!$helpBlock.length && settings.options.autoAdd && settings.options.autoAdd.helpBlocks) {
                        $helpBlock = $('<div class="help-block" />');
                        $controlGroup.find('.controls').append($helpBlock);
                        createdElements.push($helpBlock[0]);
                    }
                    if (settings.options.sniffHtml) {
                        var message = "";
                        if ($this.attr("pattern") !== undefined) {
                            message = "Not in the expected format<!-- data-validation-pattern-message to override -->";
                            if ($this.data("validationPatternMessage")) {
                                message = $this.data("validationPatternMessage");
                            }
                            $this.data("validationPatternMessage", message);
                            $this.data("validationPatternRegex", $this.attr("pattern"));
                        }
                        if ($this.attr("max") !== undefined || $this.attr("aria-valuemax") !== undefined) {
                            var max = ($this.attr("max") !== undefined ? $this.attr("max") : $this.attr("aria-valuemax"));
                            message = "Too high: Maximum of '" + max + "'<!-- data-validation-max-message to override -->";
                            if ($this.data("validationMaxMessage")) {
                                message = $this.data("validationMaxMessage");
                            }
                            $this.data("validationMaxMessage", message);
                            $this.data("validationMaxMax", max);
                        }
                        if ($this.attr("min") !== undefined || $this.attr("aria-valuemin") !== undefined) {
                            var min = ($this.attr("min") !== undefined ? $this.attr("min") : $this.attr("aria-valuemin"));
                            message = "Too low: Minimum of '" + min + "'<!-- data-validation-min-message to override -->";
                            if ($this.data("validationMinMessage")) {
                                message = $this.data("validationMinMessage");
                            }
                            $this.data("validationMinMessage", message);
                            $this.data("validationMinMin", min);
                        }
                        if ($this.attr("maxlength") !== undefined) {
                            message = "Too long: Maximum of '" + $this.attr("maxlength") + "' characters<!-- data-validation-maxlength-message to override -->";
                            if ($this.data("validationMaxlengthMessage")) {
                                message = $this.data("validationMaxlengthMessage");
                            }
                            $this.data("validationMaxlengthMessage", message);
                            $this.data("validationMaxlengthMaxlength", $this.attr("maxlength"));
                        }
                        if ($this.attr("minlength") !== undefined) {
                            message = "Too short: Minimum of '" + $this.attr("minlength") + "' characters<!-- data-validation-minlength-message to override -->";
                            if ($this.data("validationMinlengthMessage")) {
                                message = $this.data("validationMinlengthMessage");
                            }
                            $this.data("validationMinlengthMessage", message);
                            $this.data("validationMinlengthMinlength", $this.attr("minlength"));
                        }
                        if ($this.attr("required") !== undefined || $this.attr("aria-required") !== undefined) {
                            message = settings.builtInValidators.required.message;
                            if ($this.data("validationRequiredMessage")) {
                                message = $this.data("validationRequiredMessage");
                            }
                            $this.data("validationRequiredMessage", message);
                        }
                        if ($this.attr("type") !== undefined && $this.attr("type").toLowerCase() === "number") {
                            message = settings.builtInValidators.number.message;
                            if ($this.data("validationNumberMessage")) {
                                message = $this.data("validationNumberMessage");
                            }
                            $this.data("validationNumberMessage", message);
                        }
                        if ($this.attr("type") !== undefined && $this.attr("type").toLowerCase() === "email") {
                            message = "Please check email format<!-- data-validator-validemail-message to override -->";
                            if ($this.data("validationValidemailMessage")) {
                                message = $this.data("validationValidemailMessage");
                            } else if ($this.data("validationEmailMessage")) {
                                message = $this.data("validationEmailMessage");
                            }
                            $this.data("validationValidemailMessage", message);
                        }
                        if ($this.attr("minchecked") !== undefined) {
                            message = "Not enough options checked; Minimum of '" + $this.attr("minchecked") + "' required<!-- data-validation-minchecked-message to override -->";
                            if ($this.data("validationMincheckedMessage")) {
                                message = $this.data("validationMincheckedMessage");
                            }
                            $this.data("validationMincheckedMessage", message);
                            $this.data("validationMincheckedMinchecked", $this.attr("minchecked"));
                        }
                        if ($this.attr("maxchecked") !== undefined) {
                            message = "Too many options checked; Maximum of '" + $this.attr("maxchecked") + "' required<!-- data-validation-maxchecked-message to override -->";
                            if ($this.data("validationMaxcheckedMessage")) {
                                message = $this.data("validationMaxcheckedMessage");
                            }
                            $this.data("validationMaxcheckedMessage", message);
                            $this.data("validationMaxcheckedMaxchecked", $this.attr("maxchecked"));
                        }
                    }
                    if ($this.data("validation") !== undefined) {
                        validatorNames = $this.data("validation").split(",");
                    }
                    $.each($this.data(), function(i, el) {
                        var parts = i.replace(/([A-Z])/g, ",$1").split(",");
                        if (parts[0] === "validation" && parts[1]) {
                            validatorNames.push(parts[1]);
                        }
                    });
                    var validatorNamesToInspect = validatorNames;
                    var newValidatorNamesToInspect = [];
                    do {
                        $.each(validatorNames, function(i, el) {
                            validatorNames[i] = formatValidatorName(el);
                        });
                        validatorNames = $.unique(validatorNames);
                        newValidatorNamesToInspect = [];
                        $.each(validatorNamesToInspect, function(i, el) {
                            if ($this.data("validation" + el + "Shortcut") !== undefined) {
                                $.each($this.data("validation" + el + "Shortcut").split(","), function(i2, el2) {
                                    newValidatorNamesToInspect.push(el2);
                                });
                            } else if (settings.builtInValidators[el.toLowerCase()]) {
                                var validator = settings.builtInValidators[el.toLowerCase()];
                                if (validator.type.toLowerCase() === "shortcut") {
                                    $.each(validator.shortcut.split(","), function(i, el) {
                                        el = formatValidatorName(el);
                                        newValidatorNamesToInspect.push(el);
                                        validatorNames.push(el);
                                    });
                                }
                            }
                        });
                        validatorNamesToInspect = newValidatorNamesToInspect;
                    } while (validatorNamesToInspect.length > 0)
                    var validators = {};
                    $.each(validatorNames, function(i, el) {
                        var message = $this.data("validation" + el + "Message");
                        var hasOverrideMessage = (message !== undefined);
                        var foundValidator = false;
                        message = (message ? message : "'" + el + "' validation failed <!-- Add attribute 'data-validation-" + el.toLowerCase() + "-message' to input to change this message -->");
                        $.each(settings.validatorTypes, function(validatorType, validatorTemplate) {
                            if (validators[validatorType] === undefined) {
                                validators[validatorType] = [];
                            }
                            if (!foundValidator && $this.data("validation" + el + formatValidatorName(validatorTemplate.name)) !== undefined) {
                                validators[validatorType].push($.extend(true, {
                                    name: formatValidatorName(validatorTemplate.name),
                                    message: message
                                }, validatorTemplate.init($this, el)));
                                foundValidator = true;
                            }
                        });
                        if (!foundValidator && settings.builtInValidators[el.toLowerCase()]) {
                            var validator = $.extend(true, {}, settings.builtInValidators[el.toLowerCase()]);
                            if (hasOverrideMessage) {
                                validator.message = message;
                            }
                            var validatorType = validator.type.toLowerCase();
                            if (validatorType === "shortcut") {
                                foundValidator = true;
                            } else {
                                $.each(settings.validatorTypes, function(validatorTemplateType, validatorTemplate) {
                                    if (validators[validatorTemplateType] === undefined) {
                                        validators[validatorTemplateType] = [];
                                    }
                                    if (!foundValidator && validatorType === validatorTemplateType.toLowerCase()) {
                                        $this.data("validation" + el + formatValidatorName(validatorTemplate.name), validator[validatorTemplate.name.toLowerCase()]);
                                        validators[validatorType].push($.extend(validator, validatorTemplate.init($this, el)));
                                        foundValidator = true;
                                    }
                                });
                            }
                        }
                        if (!foundValidator) {
                            $.error("Cannot find validation info for '" + el + "'");
                        }
                    });
                    $helpBlock.data("original-contents", ($helpBlock.data("original-contents") ? $helpBlock.data("original-contents") : $helpBlock.html()));
                    $helpBlock.data("original-role", ($helpBlock.data("original-role") ? $helpBlock.data("original-role") : $helpBlock.attr("role")));
                    $controlGroup.data("original-classes", ($controlGroup.data("original-clases") ? $controlGroup.data("original-classes") : $controlGroup.attr("class")));
                    $this.data("original-aria-invalid", ($this.data("original-aria-invalid") ? $this.data("original-aria-invalid") : $this.attr("aria-invalid")));
                    $this.bind("validation.validation", function(event, params) {
                        var value = getValue($this);
                        var errorsFound = [];
                        $.each(validators, function(validatorType, validatorTypeArray) {
                            if (value || value.length || (params && params.includeEmpty) || (!!settings.validatorTypes[validatorType].blockSubmit && params && !!params.submitting)) {
                                $.each(validatorTypeArray, function(i, validator) {
                                    if (settings.validatorTypes[validatorType].validate($this, value, validator)) {
                                        errorsFound.push(validator.message);
                                    }
                                });
                            }
                        });
                        return errorsFound;
                    });
                    $this.bind("getValidators.validation", function() {
                        return validators;
                    });
                    $this.bind("submit.validation", function() {
                        return $this.triggerHandler("change.validation", {
                            submitting: true
                        });
                    });
                    $this.bind(["focus", "blur", "click", "change"].join(".validation ") + ".validation", function(e, params) {
                        var value = getValue($this);
                        var errorsFound = [];
                        $controlGroup.find("input,textarea,select").each(function(i, el) {
                            var oldCount = errorsFound.length;
                            $.each($(el).triggerHandler("validation.validation", params), function(j, message) {
                                errorsFound.push(message);
                            });
                            if (errorsFound.length > oldCount) {
                                $(el).attr("aria-invalid", "true");
                            } else {
                                var original = $this.data("original-aria-invalid");
                                $(el).attr("aria-invalid", (original !== undefined ? original : false));
                            }
                        });
                        $form.find("input,select,textarea").not($this).not("[name=\"" + $this.attr("name") + "\"]").trigger("validationLostFocus.validation");
                        errorsFound = $.unique(errorsFound.sort());
                        if (errorsFound.length) {
                            $controlGroup.removeClass("success error").addClass("warning");
                            if (settings.options.semanticallyStrict && errorsFound.length === 1) {
                                $helpBlock.html(errorsFound[0] +
                                    (settings.options.prependExistingHelpBlock ? $helpBlock.data("original-contents") : ""));
                            } else {
                                $helpBlock.html("<ul role=\"alert\"><li>" + errorsFound.join("</li><li>") + "</li></ul>" +
                                    (settings.options.prependExistingHelpBlock ? $helpBlock.data("original-contents") : ""));
                            }
                        } else {
                            $controlGroup.removeClass("warning field-error success");
                            if (value.length > 0) {
                                $controlGroup.addClass("success");
                            }
                            $helpBlock.html($helpBlock.data("original-contents"));
                        }
                        if (e.type === "blur") {
                            $controlGroup.removeClass("success");
                        }
                    });
                    $this.bind("validationLostFocus.validation", function() {
                        $controlGroup.removeClass("success");
                    });
                });
            },
            destroy: function() {
                return this.each(function() {
                    var
                        $this = $(this),
                        $controlGroup = $this.parents(".control-group").first(),
                        $helpBlock = $controlGroup.find(".help-block").first();
                    $this.unbind('.validation');
                    $helpBlock.html($helpBlock.data("original-contents"));
                    $controlGroup.attr("class", $controlGroup.data("original-classes"));
                    $this.attr("aria-invalid", $this.data("original-aria-invalid"));
                    $helpBlock.attr("role", $this.data("original-role"));
                    if (createdElements.indexOf($helpBlock[0]) > -1) {
                        $helpBlock.remove();
                    }
                });
            },
            collectErrors: function(includeEmpty) {
                var errorMessages = {};
                this.each(function(i, el) {
                    var $el = $(el);
                    var name = $el.attr("name");
                    var errors = $el.triggerHandler("validation.validation", {
                        includeEmpty: true
                    });
                    errorMessages[name] = $.extend(true, errors, errorMessages[name]);
                });
                $.each(errorMessages, function(i, el) {
                    if (el.length === 0) {
                        delete errorMessages[i];
                    }
                });
                return errorMessages;
            },
            hasErrors: function() {
                var errorMessages = [];
                this.each(function(i, el) {
                    errorMessages = errorMessages.concat($(el).triggerHandler("getValidators.validation") ? $(el).triggerHandler("validation.validation", {
                        submitting: true
                    }) : []);
                });
                return (errorMessages.length > 0);
            },
            override: function(newDefaults) {
                defaults = $.extend(true, defaults, newDefaults);
            }
        },
        validatorTypes: {
            callback: {
                name: "callback",
                init: function($this, name) {
                    return {
                        validatorName: name,
                        callback: $this.data("validation" + name + "Callback"),
                        lastValue: $this.val(),
                        lastValid: true,
                        lastFinished: true
                    };
                },
                validate: function($this, value, validator) {
                    if (validator.lastValue === value && validator.lastFinished) {
                        return !validator.lastValid;
                    }
                    if (validator.lastFinished === true) {
                        validator.lastValue = value;
                        validator.lastValid = true;
                        validator.lastFinished = false;
                        var rrjqbvValidator = validator;
                        var rrjqbvThis = $this;
                        executeFunctionByName(validator.callback, window, $this, value, function(data) {
                            if (rrjqbvValidator.lastValue === data.value) {
                                rrjqbvValidator.lastValid = data.valid;
                                if (data.message) {
                                    rrjqbvValidator.message = data.message;
                                }
                                rrjqbvValidator.lastFinished = true;
                                rrjqbvThis.data("validation" + rrjqbvValidator.validatorName + "Message", rrjqbvValidator.message);
                                setTimeout(function() {
                                    rrjqbvThis.trigger("change.validation");
                                }, 1);
                            }
                        });
                    }
                    return false;
                }
            },
            ajax: {
                name: "ajax",
                init: function($this, name) {
                    return {
                        validatorName: name,
                        url: $this.data("validation" + name + "Ajax"),
                        lastValue: $this.val(),
                        lastValid: true,
                        lastFinished: true
                    };
                },
                validate: function($this, value, validator) {
                    if (validator.lastValue === value && validator.lastFinished === true) {
                        return validator.lastValid === false;
                    }
                    if (validator.lastFinished === true) {
                        validator.lastValue = value;
                        validator.lastValid = true;
                        validator.lastFinished = false;
                        $.ajax({
                            url: validator.url,
                            data: "value=" + value + "&field=" + $this.attr("name"),
                            dataType: "json",
                            success: function(data) {
                                if (validator.lastValue === data.value) {
                                    validator.lastValid = (data.valid);
                                    if (data.message) {
                                        validator.message = data.message;
                                    }
                                    validator.lastFinished = true;
                                    $this.data("validation" + validator.validatorName + "Message", validator.message);
                                    setTimeout(function() {
                                        $this.trigger("change.validation");
                                    }, 1);
                                }
                            },
                            failure: function() {
                                validator.lastValid = true;
                                validator.message = "ajax call failed";
                                validator.lastFinished = true;
                                $this.data("validation" + validator.validatorName + "Message", validator.message);
                                setTimeout(function() {
                                    $this.trigger("change.validation");
                                }, 1);
                            }
                        });
                    }
                    return false;
                }
            },
            regex: {
                name: "regex",
                init: function($this, name) {
                    return {
                        regex: regexFromString($this.data("validation" + name + "Regex"))
                    };
                },
                validate: function($this, value, validator) {
                    return (!validator.regex.test(value) && !validator.negative) || (validator.regex.test(value) && validator.negative);
                }
            },
            required: {
                name: "required",
                init: function($this, name) {
                    return {};
                },
                validate: function($this, value, validator) {
                    return !!(value.length === 0 && !validator.negative) || !!(value.length > 0 && validator.negative);
                },
                blockSubmit: true
            },
            match: {
                name: "match",
                init: function($this, name) {
                    var element = $this.parents("form").first().find("[name=\"" + $this.data("validation" + name + "Match") + "\"]").first();
                    element.bind("validation.validation", function() {
                        $this.trigger("change.validation", {
                            submitting: true
                        });
                    });
                    return {
                        "element": element
                    };
                },
                validate: function($this, value, validator) {
                    return (value !== validator.element.val() && !validator.negative) || (value === validator.element.val() && validator.negative);
                },
                blockSubmit: true
            },
            max: {
                name: "max",
                init: function($this, name) {
                    return {
                        max: $this.data("validation" + name + "Max")
                    };
                },
                validate: function($this, value, validator) {
                    return (parseFloat(value, 10) > parseFloat(validator.max, 10) && !validator.negative) || (parseFloat(value, 10) <= parseFloat(validator.max, 10) && validator.negative);
                }
            },
            min: {
                name: "min",
                init: function($this, name) {
                    return {
                        min: $this.data("validation" + name + "Min")
                    };
                },
                validate: function($this, value, validator) {
                    return (parseFloat(value) < parseFloat(validator.min) && !validator.negative) || (parseFloat(value) >= parseFloat(validator.min) && validator.negative);
                }
            },
            maxlength: {
                name: "maxlength",
                init: function($this, name) {
                    return {
                        maxlength: $this.data("validation" + name + "Maxlength")
                    };
                },
                validate: function($this, value, validator) {
                    return ((value.length > validator.maxlength) && !validator.negative) || ((value.length <= validator.maxlength) && validator.negative);
                }
            },
            minlength: {
                name: "minlength",
                init: function($this, name) {
                    return {
                        minlength: $this.data("validation" + name + "Minlength")
                    };
                },
                validate: function($this, value, validator) {
                    return ((value.length < validator.minlength) && !validator.negative) || ((value.length >= validator.minlength) && validator.negative);
                }
            },
            maxchecked: {
                name: "maxchecked",
                init: function($this, name) {
                    var elements = $this.parents("form").first().find("[name=\"" + $this.attr("name") + "\"]");
                    elements.bind("click.validation", function() {
                        $this.trigger("change.validation", {
                            includeEmpty: true
                        });
                    });
                    return {
                        maxchecked: $this.data("validation" + name + "Maxchecked"),
                        elements: elements
                    };
                },
                validate: function($this, value, validator) {
                    return (validator.elements.filter(":checked").length > validator.maxchecked && !validator.negative) || (validator.elements.filter(":checked").length <= validator.maxchecked && validator.negative);
                },
                blockSubmit: true
            },
            minchecked: {
                name: "minchecked",
                init: function($this, name) {
                    var elements = $this.parents("form").first().find("[name=\"" + $this.attr("name") + "\"]");
                    elements.bind("click.validation", function() {
                        $this.trigger("change.validation", {
                            includeEmpty: true
                        });
                    });
                    return {
                        minchecked: $this.data("validation" + name + "Minchecked"),
                        elements: elements
                    };
                },
                validate: function($this, value, validator) {
                    return (validator.elements.filter(":checked").length < validator.minchecked && !validator.negative) || (validator.elements.filter(":checked").length >= validator.minchecked && validator.negative);
                },
                blockSubmit: true
            }
        },
        builtInValidators: {
            email: {
                name: "Email",
                type: "shortcut",
                shortcut: "validemail"
            },
            validemail: {
                name: "Validemail",
                type: "regex",
                regex: "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\\.[A-Za-z]{2,4}",
                message: "Please use a valid email format<!-- data-validator-validemail-message to override -->"
            },
            passwordagain: {
                name: "Passwordagain",
                type: "match",
                match: "password",
                message: "Does not match the given password<!-- data-validator-paswordagain-message to override -->"
            },
            positive: {
                name: "Positive",
                type: "shortcut",
                shortcut: "number,positivenumber"
            },
            negative: {
                name: "Negative",
                type: "shortcut",
                shortcut: "number,negativenumber"
            },
            number: {
                name: "Number",
                type: "regex",
                regex: "([+-]?\\\d+(\\\.\\\d*)?([eE][+-]?[0-9]+)?)?",
                message: "Must be a number<!-- data-validator-number-message to override -->"
            },
            integer: {
                name: "Integer",
                type: "regex",
                regex: "[+-]?\\\d+",
                message: "No decimal places allowed<!-- data-validator-integer-message to override -->"
            },
            positivenumber: {
                name: "Positivenumber",
                type: "min",
                min: 0,
                message: "Must be a positive number<!-- data-validator-positivenumber-message to override -->"
            },
            negativenumber: {
                name: "Negativenumber",
                type: "max",
                max: 0,
                message: "Must be a negative number<!-- data-validator-negativenumber-message to override -->"
            },
            required: {
                name: "Required",
                type: "required",
                message: "This is required<!-- data-validator-required-message to override -->"
            },
            checkone: {
                name: "Checkone",
                type: "minchecked",
                minchecked: 1,
                message: "Check at least one option<!-- data-validation-checkone-message to override -->"
            }
        }
    };
    var formatValidatorName = function(name) {
        return name.toLowerCase().replace(/(^|\s)([a-z])/g, function(m, p1, p2) {
            return p1 + p2.toUpperCase();
        });
    };
    var getValue = function($this) {
        var value = $this.val();
        var type = $this.attr("type");
        if (type === "checkbox") {
            value = ($this.is(":checked") ? value : "");
        }
        if (type === "radio") {
            value = ($('input[name="' + $this.attr("name") + '"]:checked').length > 0 ? value : "");
        }
        return value;
    };

    function regexFromString(inputstring) {
        return new RegExp("^" + inputstring + "$");
    }

    function executeFunctionByName(functionName, context) {
        var args = Array.prototype.slice.call(arguments).splice(2);
        var namespaces = functionName.split(".");
        var func = namespaces.pop();
        for (var i = 0; i < namespaces.length; i++) {
            context = context[namespaces[i]];
        }
        return context[func].apply(this, args);
    }
    $.fn.jqBootstrapValidation = function(method) {
        if (defaults.methods[method]) {
            return defaults.methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return defaults.methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.jqBootstrapValidation');
            return null;
        }
    };
    $.jqBootstrapValidation = function(options) {
        $(":input").not("[type=image],[type=submit]").jqBootstrapValidation.apply(this, arguments);
    };
})(jQuery);

/*===============================
https://www.equipmentone.com/e1_2/js/postalcodeValidator.js
================================================================================*/
;
$(function() {
    $("input,select,textarea").not('input.tt-hint,input#search').jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {},
        submitSuccess: function($form, event) {
            event.preventDefault();
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });
    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});