/*
Template: External DMP
Author: Amy Manson
version: 6.6.15

*/
(function( window, undefined ) {
	
	var MDVDATA = {} || MDVDATA;
	

	/* Revised function that can be repurposed to grab URI and break in to different parts */ 
	
	MDVDATA.fn = MDVDATA.prototype = {	
	    debug:function(){
					this.collections.debug = true;
					return this
					},
	    trace:function(a){
					try{
						if(this.collections.debug == true){
						console.log("MEDIATIVE UTAG LOGS: " + a);
						}
						}catch(e){}
					
					
					},
		
		collections:{
			debug:false,
			mseg:[],
			lang:"en",
			sitename:"RB Auction"
			
			
			},
			addseg :function(k,v){
if(window.MUTAG_RANDOM == undefined){
window.MUTAG_RANDOM = Math.round(Math.random()*100000000);
}
var mseg = {};
mseg[k]=v;
MDVDATA.collections.mseg.push(mseg);
return this;
},
populate:function(){
	
	var img = document.createElement('img');
	var tempURL = document.URL;
	var search = checkSearch();
	//var tempURL = "https://www.rbauction.com/home-v3?utm_expid=331893-19.ih29hH4JTNS0Z1CbR8OXAA.1&utm_referrer=https%3A%2F%2Fwww.rbauction.com%2FGRANDE-PRAIRIE-AB%3Fid%3Dci%26s%3DY2l8Tj00Mjk0MzY3OTc1KzQyOTQ5NjY4NTgmTnM9TG90TnVtYmVyfDB8fExvdE51bWJlck1vZGlmaWVyfDA";
	var urlParts = parseUri(tempURL);
	var newQuery = findQuery(urlParts['query']);
	urlParts['query'] = newQuery;
	var textimg = ""; 
	var string = "";
	var myTarget = findUTM();

	
	
	function categoryCleaner(w){
	        var MKEY =  {
	            "é": "e",
	            "Ã¨":"e",    
	            "Ã ":"a",
	            "Ã©":"e",
	            "Ã‰":"E",
	            "Ãˆ":"E",
	            "Ã€":"A",
	            "Ã‡":"C",
	            "Ã§":"c",
	            "â":"a",
	            "ú":"u",
	            "ö":"o",
	            "المتحدة العربية الامارات":"United Arab Emirates",
	            "belgië":"belgium",
	            "ελληνικά":"greek",
	            "ελλάς":"greece",
	            "españa":"spain",
	            "україна":"ukrainse",
	            "беларусь":"belarus",
	         	"romană":"romanian",
	         	"azərbaycan":"azerbaijan",
	            "българия":"bulgaria",
	            "magyarország":"hungary",
	            "italia":"italy",
	            "Français":"french",
	            "français":"french",
	            "繁體漢字":"chinese",
	            "español":"spanish",
	            "schweiz":"switzerland",
	            "italiano":"italian",
	            "bosna i hercegovina":"bosnia",
	            "česká republika":"czech republic",
	            "suomi":"finland",
	            "россия":"russia",
	            "русскии":"russian",
	            "português":"portuguese",
	            "عربي":"arabic",
	            "bahasa (indonesia)":"indonesia",
	            "deutsch":"german",
	            "română":"romanian",
	            "中国":"china",
	            "香港":"hong kong",
	            "简体汉字":"chinese",
	            "日本":"japan",
	            "日本語":"japanese",
	            "台灣":"taiwan",
	            "việt nam":"viet nam",
	            "tiếng việt":"vietnamese",
	            "বাংলাদেশ":"bangladesh",
	            "türkçe":"turkish",
	            "қазақстан":"kazakhstan",
	            "नेपाल":"nepal",
	            "ประเทศไทย":"thailand",
	            "ภาษาไทย":"thai",
	            "საქართველო":"georgia",
	            "대한민국":"korea",
	            "한국어/조선말":"korean",
	            "افغانستان":"afghanistan",
	            "السعودية":"saudi arabia",
	            "البحرين":"bahrain",
	            "قطر":"diameter",
	            "الأردن":"jordan",
	            "عُمان":"amman",
	            "türkiye":"turkey",
	            "مصر":"egypt",
	            "ليبيــا":"lybia"



	        };
	        for(var i in MKEY){
	            do{
	                w = w.replace(i, MKEY[i]);
	            }while(w.indexOf(i) !=-1);
	        }
	        return w;
	       
	    }


	//lang = lang.substring(0, lang.indexOf('-'));
	var getNav = findOnPage();
	//var cleanPath = languageCleaner(urlParts['path']).slice(1);
	//urlParts['path'] = cleanPath;
	img.style.display = "none";
	textimg  += "//bcp.crwdcntrl.net/5/c=2726/";
	textimg  += "rand=" + String(Math.floor((Math.random()*100000000)+1));
	textimg  += "/pv=y/";
	textimg  += "/ctax=Advertisers";
	textimg += "^";
	textimg += this.collections.sitename;

	

	function findQuery(getQuery) {
	var a = getQuery.split("&");
	var b = {}
	for (var i = 0; i < a.length; i++) {
		var d=a[i].split("=");
		b[d[0]]=d[1];
	}
	return b;
}

function findUTM() {

	if (urlParts['query'] != undefined) {

		if (urlParts['query']['utm_campaign'] == "rtb-mediative-orlando-seller-feb-prospecting") {

			 var myUTM = "prospecting";

		}	if (urlParts['query']['utm_campaign'] == "rtb-mediative-orlando-seller-feb-retargeting") {

			myUTM = "retargeting";
		}


	}

	return myUTM;
}

		function findOnPage() {

			var myNav = document.querySelector('.active a');

			if (myNav != null) {

				var navItem = document.querySelector('.active a').title.replace(/\s/g,"-").toLowerCase();

			} else if (myNav == null) {

				var navItem = "account-login-page";

			}

			return navItem;

		}

//timeBread();


	function parseUri(sourceUri){
    var uriPartNames = ["source","protocol","authority","domain","port","path","directoryPath","fileName","query","anchor"];
    var uriParts = new RegExp("^(?:([^:/?#.]+):)?(?://)?(([^:/?#]*)(?::(\\d*))?)?((/(?:[^?#](?![^?#/]*\\.[^?#/.]+(?:[\\?#]|$)))*/?)?([^?#/]*))?(?:\\?([^#]*))?(?:#(.*))?").exec(sourceUri);
    var uri = {};
    
    for(var i = 0; i < 10; i++){
        uri[uriPartNames[i]] = (uriParts[i] ? uriParts[i] : "");
    }
    
    // Always end directoryPath with a trailing backslash if a path was present in the source URI
    // Note that a trailing backslash is NOT automatically inserted within or appended to the "path" key
    if(uri.directoryPath.length > 0){
        uri.directoryPath = uri.directoryPath.replace(/\/?$/, "/");
    }
    
    return uri;
}



	function checkSearch() {

		var checkSearch = document.getElementById('search-terms');

		if (checkSearch) {

			var innerSearch =  document.getElementById('search-terms').innerHTML;

			}

			return innerSearch;

		}

var addEvent = (function () {
   var filter = function(el, type, fn) {
      for ( var i = 0, len = el.length; i < len; i++ ) {
         addEvent(el[i], type, fn);
      }
   };
   if ( document.addEventListener ) {
      return function (el, type, fn) {
         if ( el && el.nodeName || el === window ) {
            el.addEventListener(type, fn, false);
         } else if (el && el.length) {
            filter(el, type, fn);
         }
      };
   }
 
   return function (el, type, fn) {
      if ( el && el.nodeName || el === window ) {
         el.attachEvent('on' + type, function () { return fn.call(el, window.event); });
      } else if ( el && el.length ) {
         filter(el, type, fn);
      }
   };
})();






function breadcrumbs () {

    
    
    var bread = document.getElementById("breadcrumb-container");
    var search = document.getElementById("rba-search-results-breadcrumb");
    var test = document.getElementsByClassName('rba-bc-search-term');




    if (bread) {
        var mediative_getCat = document.getElementById("breadcrumb-container").innerHTML.replace(/<[^>]*|&nbsp;|&ldash;\s  /g, "").trim();
    
      } else if (search && test != -1 ) {

        var mediative_getCat = document.getElementById("rba-search-results-breadcrumb").innerHTML.replace(/<[^>]*|&nbsp;|&ldash;\s  /g, "").trim();     
        

     }


    else if (search) {
   
        var mediative_getCat = document.getElementById("rba-search-results-breadcrumb").innerHTML.replace(/<[^>]*|&nbsp;|&ldash;\s  /g, "").trim();
    } else if (!bread || !search || !test) {
    	mediative_CatLevel = "null";
    	return mediative_CatLevel;

    }

 	var cleanCat = mediative_getCat.replace(/>> |> /g, "");
    var stripCat = cleanCat.replace(/(>)(?=\1)/gi,"").slice(1, -1).trim();
    var myArray = stripCat.replace(/ &amp; |[, ]+/g, "-").toLowerCase();
    var fixMyArray = myArray.replace(/--/g,"");
    //var cleanLang = MUNIVERSAL.kws_cleaner(myArray);
    var arraySplit = fixMyArray.split(">");
    mediative_CatLevel = {};
    for (i = 0; i < arraySplit.length; i++) {
     mediative_CatLevel["l" + (i + 1)] = arraySplit[i].trim();

    }
    
    return mediative_CatLevel;
}


	function timeBread() {
    var myVar = setTimeout(breadcrumbs, 1000);
    var myParts = setTimeout(compileParts, 3000);
   	//var myCompile = setTimeout(MDVDATA, 3000);
}

timeBread();



function checkDate(string, expressions) {

	

	var expression = '[/[0-9]/]';

	var myBreadArray = mediative_CatLevel;


    for (var i = 0; i < myBreadArray.length; i++) {

        if (myBreadArray.indexOf(expression) >= 0) {
            return true;
        }

    return false;

}
}



	var countPath = urlParts["path"].split('/');
	var newPath = countPath.slice(1);
	
	//need some URL specific details
	

// empty directory path is home


function compileParts() {

	var lang = categoryCleaner(document.querySelector('.rba-localeselector-text').innerHTML.split(' - ')[1].toLowerCase());
	var region = categoryCleaner(document.querySelector('.rba-localeselector-text').innerHTML.split(' - ')[0].toLowerCase());



	var string = "";

	if (urlParts['directoryPath'] == "/" || urlParts['directoryPath'].indexOf('home-v3') != -1) {
		//code
		
		
		textimg += "^";
		textimg += region;
		textimg += "^";
		textimg += lang;
		textimg += "^";
		var string = "Home";	

// not empty directory path 

	

	} else if (tempURL == "https://www.rbauction.com/heavy-equipment-auctions/orlando-fl"){
		//code
		textimg += "^"
		textimg += "orlando-fl-campaign";	
		textimg += "^";
		textimg += region;
		textimg += "^";
		textimg += lang;
		textimg += "^";
		textimg += "auctions";	
		textimg += "^";
		textimg += "Eheavy-equipment-auctions";

// not empty directory path 

	

	}	else if (myTarget != undefined) {
		
			
			//code
		
			var slashes = urlParts['path'].split("/");
			var newSlashes = slashes.slice(1, -1);

			textimg += "^";
			textimg += myTarget;
			textimg += "^";
			textimg += region;
			textimg += "^";
			textimg += lang;

		if (newSlashes[0] != getNav) {

			textimg += "^";
			textimg += getNav;

		}	

		if (newSlashes[2]) {
			var slashCategory = newSlashes[2].replace(/[0-9]|-+$/g, "");
			var cleanCategory = slashCategory.replace(/-+$/g, "");
			newSlashes[2] = cleanCategory;
		}

		//slashes=slashes.substring(0, (slashes.length -1));
		

		if (newSlashes.length == 1){
			
			//string += "^";
		
		
			textimg += "^";
			var string = newSlashes[0];

		} 

	}else if (getNav == "current-inventory") {
		
	
		var getKeys = Object.keys(mediative_CatLevel).length;

	

		delete mediative_CatLevel['l1'];
		delete mediative_CatLevel['l5'];
		delete mediative_CatLevel['l6'];
		delete mediative_CatLevel['l7'];

		textimg += "^";
		textimg += region;
		textimg += "^";
		textimg += lang;

		if (search != undefined && search != "") {

			textimg += "^";
			textimg += "search";

		}


		for (var prop in mediative_CatLevel)
			{
				textimg += "^";
				textimg += mediative_CatLevel[prop];
			}


	} 	else if (urlParts['directoryPath'] != "/" && mediative_CatLevel['l2'] == "auctions") {
		
			
			//code
		
			var slashes = urlParts['path'].split("/");
			var newSlashes = slashes.slice(1, -1);

			
			textimg += "^";
			textimg += region;
			textimg += "^";
			textimg += lang;

		if (newSlashes[0] != getNav) {

			textimg += "^";
			textimg += getNav;

		}	

		if (newSlashes[2]) {
			var slashCategory = newSlashes[2].replace(/[0-9]|-+$/g, "");
			var cleanCategory = slashCategory.replace(/-+$/g, "");
			newSlashes[2] = cleanCategory;
		}

		//slashes=slashes.substring(0, (slashes.length -1));
		

		if (newSlashes.length == 1){
			
			//string += "^";
		
		
			textimg += "^";
			var string = newSlashes[0];
		}	
		else {
		
		
			//string+= newSlashes[0];

			for(i=0;i<newSlashes.length;i++)
		{
			//textimg += "test";
			
			string += "^";
			string += newSlashes[i];
			}
			
		}	

	}else if (urlParts['directoryPath'] != "/") {
		
			
			//code
		
			var slashes = urlParts['path'].split("/");
			var newSlashes = slashes.slice(1);

			textimg += "^";
			textimg += region;	
			textimg += "^";
			textimg += lang;

		if (newSlashes[0] != getNav) {

			textimg += "^";
			textimg += getNav;

		}	

		if (newSlashes[2]) {
			var slashCategory = newSlashes[2].replace(/[0-9]|-+$/g, "");
			var cleanCategory = slashCategory.replace(/-+$/g, "");
			newSlashes[2] = cleanCategory;
		}

		//slashes=slashes.substring(0, (slashes.length -1));
		

		if (newSlashes.length == 1){
			
			//string += "^";
		
		
			textimg += "^";
			var string = newSlashes[0];
		}	
		else {
		
		
			//string+= newSlashes[0];

			for(i=0;i<newSlashes.length;i++)
		{
			//textimg += "test";
			
			string += "^";
			string += newSlashes[i];
			}
			
		}
	
	}

	
for(i=0;i<MDVDATA.collections.mseg.length;i++){
 for(k in MDVDATA.collections.mseg[i]){
textimg += String(k) + "=" + String(MDVDATA.collections.mseg[i][k]) + "/";
 }

}
textimg += string;
textimg +="//rt=ifr";
img.src = textimg;

document.getElementsByTagName("body")[0].appendChild(img);
return this;
}
},

MLoadEvent:function(){	
try{
var section = document.getElementById("rot_ctr1_bod_ctr3_bod_wrp1_blk2_custom").innerHTML;

string=section.substring(section.lastIndexOf("<font color=\"#b9101b\">"),section.lastIndexOf("</font>"));


if (string.indexOf("<br>") > 0){
	var string=string.substring(string.lastIndexOf("<font color=\"#b9101b\">")+22,string.lastIndexOf("<br>"));
}
else{
	var string=string.substring(string.lastIndexOf("<font color=\"#b9101b\">")+22,string.lastIndexOf("</font>"));
}

string = string.replace(/<\/a>/gi,"_");
string = string.replace(/<[^>]*>/g,"");
string = string.replace(/\s/gi,"");
string = string.replace(/_/,"");
string = string.replace(/amp;/,"");
string = string.replace(/Home/gi,"");


//MDVDATA.addseg("seg","language=" + this.collections.sitename + "-" + this.collections.lang + "-" + section);


}
  
catch(e){
MDVDATA.trace(e.message);
}


 try{
window.addEventListener('load', MDVDATA.MLoadEvent, false);
}catch(e){
window.attachEvent('onload', MDVDATA.MLoadEvent);
}}
	
		
		};	
					
		
		
	
MDVDATA = MDVDATA.fn;
                return (window.MDVDATA = window.$UTAG = MDVDATA);		
	
})(window);

//MDVDATA.MLoadEvent();	

MDVDATA.populate();